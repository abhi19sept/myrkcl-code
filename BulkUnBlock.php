<?php
$title="UnBlock Center";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
			<div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Bulk UnBlock Center From Excel</div>
					<form name="frmblock" id="frmblock" class="form-inline" role="form" enctype="multipart/form-data"> 
						<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-6 form-group">     
                                <label for="learnercode">Upload Batch ID:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtUploadId" id="txtUploadId" disabled="disabled"/>								
                            </div> 
							<div class="col-sm-4 form-group">  
								<label for="file">Download Sample File:</label>
								<a href="UnBlockCenterSampleFileFormat.xls"> <input type="button" value="Download File" name="download">  </a>
							</div>		
						</div> 	

						<div class="container">                            
                            <div class="col-sm-4 form-group"> 
                                <label for="excel"> Browse Center List From Your Computer </label>
								<input type="file" id="_file" name="_file" class="form-control" accept=".xls">                               
                            </div>

							<div class="col-sm-4 form-group"> 
							<span class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"/></span> 
							<span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
								<div class='progress_outer' style="display:none;">
                                    <div id='_progress' class='progress'></div>
                                </div>
							</div>							
						</div>
						
					<div id="tblBlock" style="display:none;">
						<div class="container">                            
                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Category:</label>
								 <select id="ddlBlockCategory" name="ddlBlockCategory" class="form-control">

                                 </select>
							</div>					
							
							<div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Date:</label>
								<input type="text" id="txtBlockDate" name="txtBlockDate" class="form-control" placeholder="YYYY-MM-DD">                               
                            </div>							
						</div>		
              
                         <div class="container">
							<div class="col-sm-5">     
									<label for="address">Block Category:</label> <br/> 
								  <label class="radio-inline"> <input type="radio" onclick="toggle_visibility1('ddlcourse');" id="center" name="blockmethod" checked="checked"/> Block Center </label>
								  <label class="radio-inline"> <input type="radio" onclick="toggle_visibility('ddlcourse');" id="course" name="blockmethod"  /> Block Course </label>
							</div>
								
							<div class="col-sm-6 form-group" id="ddlcourse" style="display:none;"> 
							  <label for="photo">Select Course:</label> 
							   <select id="ddlCourse" name="ddlCourse" class="form-control">
										
								</select>						  									  
							</div>									
						</div>
						
							<div class="container" style="margin-top:15px;">
								<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Unblock All"/>    
							</div>						
									<div id="grid" style="margin-top:15px;"> </div>
                     </div>		
						
				 </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>     
                  
    <script src="scripts/blockfileupload.js"></script>
	<script type="text/javascript">
<!--
    function toggle_visibility(id) {
       var e = document.getElementById(id); 
		var f = document.getElementById('excel')
          e.style.display = 'block';
		  f.style["display"]='none';
		  //f.style.display = 'none';
    }
	
	function toggle_visibility1(id) {
       var e = document.getElementById(id); 
		var f = document.getElementById('excel')
          e.style.display = 'none';
		  f.style["display"]='block';
    }
//-->
</script>  
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            function GenerateUploadId()
            {

                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {
                        //alert(data);
                        txtUploadId.value = data;
                    }
                });
            }
            GenerateUploadId();

            function FillBlockCategory() {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=FILLUNBLOCKCATEGORY",
                    success: function (data) {
                        $("#ddlBlockCategory").html(data);
                    }
                });
            }
            FillBlockCategory();
			
			function GetCourse(){
				$.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GETBulkUnBlockCourse",
                    success: function (data) { 
						//alert(data);
					//data = $.parseJSON(data);
						$('#ddlCourse').html(data);
					}
                });
			}
			GetCourse();


            $("#btnReset").click(function () {
                window.location.href = "bulkunblock.php";
            });

            $("#btnSubmit").click(function () {
				$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfBlockUnblock.php"; // the script where you handle the form input.
				if (document.getElementById('center').checked) //for radio button
					{
						var radiocheck = 'no';
					}
					else {
					radiocheck = 'yes';
					}
				 var data;
				 data =  "action=BULKUNBLOCK&UploadId=" + txtUploadId.value + "&BlockCategory=" + ddlBlockCategory.value + "&coursename=" + ddlCourse.value + "&radiovaue=" + radiocheck + "&BlockDate=" + txtBlockDate.value + "";
                $.ajax({
                   type: "POST",
					url: url,
					data: data,
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {

                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "bulkunblock.php";
                            }, 1000);

                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                    }
                });
				 return false; 
            });
        });

    </script>
</html>
