<?php
$title = "EOI Configuration";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var EoiConfigCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var EoiConfigCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
?>
<link rel="stylesheet" href="css/datepicker.css"/>
<script src="scripts/datepicker.js"></script>
<div class="container"> 


    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">EOI Configuration</div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> --> 
            <form name="frmEoiConfig" id="frmEoiConfig" class="form-inline" role="form" enctype="multipart/form-data">    
                <div class="container">
                    <div class="container">
                        <div id="response"></div>
                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="EOIName">Name of the EOI:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="500" name="txtEoiName" id="txtEoiName" placeholder="Eoi Name"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="Course">Select Course:<span class="star">*</span></label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control" >

                        </select>    
                    </div>  

                    <div class="col-sm-4 form-group">     
                        <label for="EOIName">Start Date:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="YYYY-MM-DD"/>
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="EOIName">End Date:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtenddate" id="txtenddate" readonly="true" placeholder="YYYY-MM-DD"/>
                    </div>
                </div>						

                <div class="container">  
                    <div class="col-sm-4 form-group"> 
                        <label for="t_marks">Processing Fees (In Rs):<span class="star">*</span></label>
                        <input type="text" class="form-control" name="txtPFees" id="txtPFees" placeholder="Processing Fees"/>    
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="email">Course Fees:<span class="star">*</span></label>
                        <input type="text" class="form-control" id="txtCFees" name="txtCFees" placeholder="Course Fees"/>
                    </div>

                    <div class="col-sm-4 form-group">  
                        <label for="email">Download Sample File:</label>
                        <a href="sample_file.xls"> <input type="button" value="Download File" name="download">  </a>
                    </div>						
                </div>						

                <div class="container"> 
                    <div class="col-sm-4 form-group"> 
                        <label for="ecl">Upload Eligible Centre List:<span class="star">*</span></label>
                        <input type="file" class="form-control" id="_file" name="_file" accept=".csv"/>
                        <span style="font-size:10px;">Note : csv Allowed Max Size =1MB</span>	
                    </div>

<!--                    <div class="col-sm-4 form-group"> 
                        <span class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"/></span> 
                        <span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
                        <div class='progress_outer'>
                            <div id='_progress' class='progress'></div>
                        </div>
                    </div>-->

                    <div class="col-sm-4 form-group"> 
                        <label for="rs_cit_certficate">Upload Terms and Condition:<span class="star">*</span></label>
                        <input type="file" class="form-control" id="tncdoc" name="tncdoc"/>
                      
                        <span style="font-size:10px;"> Note : JPG,JPEG Allowed Max Size =50KB</span>
                    </div> 
                </div>

                <div class="container">      
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Launch EOI"/>
                </div>                  
            </form>
        </div>
    </div>
</div>
</body>

<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<!--<script src="scripts/fileupload.js"></script>-->
<!--<script src="scripts/eoitnc.js"></script>-->

<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        orientation: "bottom auto",
        todayHighlight: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        orientation: "bottom auto",
        todayHighlight: true
    });
</script>	

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $("#txtstartdate, #txtenddate").datepicker();
    $("#txtenddate").change(function () {
        var txtstartdate = document.getElementById("txtstartdate").value;
        var txtenddate = document.getElementById("txtenddate").value;
        if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
            alert("End date should be greater than Start date");
            document.getElementById("txtenddate").value = "";
        }
    });


    $(document).ready(function () {
        //alert("hii");

		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCourseName",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();
        $("#frmEoiConfig").on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('action', 'ADD');
                $.ajax({
                    url: "common/cfEoiConfig.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                //window.location.href = 'frmeoiconfig.php';
                            }, 3000);
                        } else {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> " + data + " </span></p>");
                        }
                    },
                    error: function (e)
                    {
                        $("#errmsg").html(e).fadeIn();
                    }
                });
            }));

//        $("#btnSubmit").click(function () {
//            if ($("#frmEoiConfig").valid())
//            {
//                $('#response').empty();
//                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
//
//                //var eclist=$('#eclist').val(); 
//                //var tncdoc=$('#tncdoc').val(); 
//                //alert(filename);    
//                var startdate = $('#txtstartdate').val();
//                var enddate = $('#txtenddate').val();
//                //alert(enddate);
//                var url = "common/cfEoiConfig.php"; // the script where you handle the form input.
//                var data;
//                var forminput=$("#frmEoiConfig").serialize();
//                if (Mode == 'Add')
//                {
//                     data = "action=ADD&" + forminput; // serializes the form's elements.
////                    data = "action=ADD&name=" + txtEoiName.value + "&course=" + ddlCourse.value +
////                            "&startdate=" + startdate + "&enddate=" + enddate +
////                            "&pfees=" + txtPFees.value + "&cfees=" + txtCFees.value +
////                            ""; // serializes the form's elements.
//                    //alert(data);
//                }
//                else
//                {
//                    data = "action=UPDATE&code=" + EoiConfigCode +
//                            "&name=" + txtEoiName.value + "&course=" + ddlCourse.value +
//                            "&startdate=" + txtstartdate.value + "&enddate=" + txtenddate.value +
//                            "&pfees=" + txtPFees.value + "&cfees=" + txtCFees.value +
//                            "&eclist=" + eclist + ""; // serializes the form's elements.
//                }
//
//                $.ajax({
//                    type: "POST",
//                    url: url,
//                    data: data,
//                    success: function (data)
//                    {
//                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
//                        {
//                            $('#response').empty();
//                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
//                            window.setTimeout(function () {
//                                window.location.href = "frmeoiconfig.php";
//                            }, 1000);
//
//                            Mode = "Add";
//                            resetForm("frmEoiConfig");
//                        }
//                        else
//                        {
//                            $('#response').empty();
//                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
//                        }
//                        //showData();
//
//
//                    }
//                });
//            }
//            return false; // avoid to execute the actual submit of the form.
//        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });
</script>
</html> 


<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmeoiconfig_validation.js" type="text/javascript"></script>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>	