<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPaymentRefund
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsPaymentRefund {
    //put your code here
    
    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {

                $_SelectQuery2 = "Select * From tbl_payment_refund WHERE Payment_Refund_Timestamp >= '" . $_StartDate . "' AND Payment_Refund_Timestamp <= '" . $_EndDate . "'";
            } 
            elseif ($_loginflag == "7") {

                $_SelectQuery2 = "Select * From tbl_payment_refund WHERE Payment_Refund_Timestamp >= '" . $_StartDate . "' AND Payment_Refund_Timestamp <= '" . $_EndDate . "' AND Payment_Refund_ITGK = '" . $_SESSION['User_LoginId'] . "' ";
            } 
            else {

                $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission_transaction AS b ON a.Centercode = b.Admission_Transaction_CenterCode WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response1[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";
                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");
                if ($CenterCode3) {
                     $_SelectQuery2 = "Select * From tbl_payment_refund WHERE Payment_Refund_Timestamp >= '" . $_StartDate . "' AND Payment_Refund_Timestamp <= '" . $_EndDate . "' AND Payment_Refund_ITGK IN ($CenterCode3)";
                }
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }
}
