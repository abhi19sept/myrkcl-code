<?php

/**
 * Description of clsManageEvent
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsEvents {

    public function FILLEvent($_EventCat) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventCat = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventCat);
				
            $_SelectQuery = "Select * From tbl_event_type WHERE Event_Type_Category = '" . $_EventCat . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SHOWAdmissionPay($_Batch, $_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				
             $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id WHERE a.Event_Course = '".$_Course."' AND a.Event_Batch = '".$_Batch."' AND Event_Name='3' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate AND Event_Payment != '2'";
           // $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id INNER JOIN tbl_ddmode_center AS c ON a.Event_ID = c.Ddmode_Center_EventId WHERE a.Event_Course = '" . $_Course . "' AND a.Event_Batch = '" . $_Batch . "' AND Event_Name='3' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate AND c.Ddmode_Center_ITGK = '" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SHOWAdmissionPayDD($_Batch, $_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				
             $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id INNER JOIN tbl_ddmode_center AS c ON a.Event_ID = c.Ddmode_Center_EventId WHERE a.Event_Course = '" . $_Course . "' AND a.Event_Batch = '" . $_Batch . "' AND Event_Name='3' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate AND c.Ddmode_Center_ITGK = '" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SHOWReexamPay($_Examevent, $_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Examevent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Examevent);
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				
            $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id WHERE a.Event_ReexamEvent = '" . $_Examevent . "' AND Event_Name='7' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SHOWEOIPay($_Courseeoi) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Courseeoi = mysqli_real_escape_string($_ObjConnection->Connect(),$_Courseeoi);
				
            $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id WHERE a.Event_CourseEOI = '" . $_Courseeoi . "' AND Event_Name='5' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SHOWCorrectionPay($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id WHERE a.Event_CorrectionEvent = '" . $_Code . "' AND Event_Name='8' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}

?>