<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsRspReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRspReport {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "SELECT a.Rspitgk_Rspname,a.Rspitgk_Rspdistrict,a.Rspitgk_ItgkCode,a.Rspitgk_Rspcode,"
                    . "b.User_LoginId,c.Organization_name, b.User_code, d.User_LoginId, e.District_Name"
                    . " FROM tbl_rspitgk_mapping as a INNER JOIN tbl_user_master as b "
                    . "ON a.Rspitgk_ItgkCode=b.User_LoginId INNER JOIN tbl_user_master as d "
                    . "ON a.Rspitgk_Rspcode=d.User_Code INNER JOIN tbl_organization_detail as c "
                    . "ON a.Rspitgk_ItgkCode=b.User_LoginId and b.User_code= c.Organization_User"
                    . " INNER JOIN tbl_district_master as e ON c.Organization_District=e.District_Code ORDER BY e.District_Name, d.User_LoginId";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
