<?php

require 'DAL/classconnectionNEW.php';
require 'DAL/upload_ftp_doc.php';

$_ObjFTPConnection = new ftpConnection();
$_ObjConnection = new _Connection();
$_Response = array();

class clsBatchMaster {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Batch_Code,Batch_Name,Batch_Start,Batch_End,Status_Name From tbl_batch_master as a Inner Join Status_Master as b on a.Batch_Status=B.Status_Code";
            $_Data = $_ObjConnection->ExecuteQuery($_SelectQuery);
            $_Response[0] = $_Data;
            $_Response[1] = Message::Success;
            return $_Response;
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
    }

    public function Add($_BatchName, $_Course, $_Status, $_Sdate, $_Edate, $_Install, $_Certif, $_Finance, $_Year, $_Fees, $_Share, $_VMOUShare) {
        global $_ObjConnection;
        global $_ObjFTPConnection;
        $_ObjConnection->Connect();

        try {
            $_BatchName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchName);
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_Sdate = mysqli_real_escape_string($_ObjConnection->Connect(),$_Sdate);
            $_Edate = mysqli_real_escape_string($_ObjConnection->Connect(),$_Edate);
            $_User_Code = $_SESSION['User_Code'];

            $ftpaddress = $_ObjFTPConnection->ftpPathIp();
            $ftpconn = $_ObjFTPConnection->ftpConnect();

            $_InsertQuery = "Insert Into tbl_batch_master(Batch_Code,Batch_Name,Batch_StartDate,Batch_EndDate,Batch_Status,Course_Code,Admission_Installation_Mode,User_Code,Certificate_Code,Financial_Year,Year,Course_Fee,RKCL_Share,VMOU_Share) 
					Select Case When Max(Batch_Code) Is Null Then 1 Else Max(Batch_Code)+1 End as Batch_Code,'" . $_BatchName . "' as Batch_Name,'" . $_Sdate . "' as Batch_StartDate,'" . $_Edate . "' as Batch_EndDate,'" . $_Status . "' as Batch_Status,
					'" . $_Course . "' as Course_Code,'" . $_Install . "' as Admission_Installation_Mode,'" . $_User_Code . "' as User_Code,'" . $_Certif . "' as Certificate_Code,'" . $_Finance . "' as Financial_Year,
					'" . $_Year . "' as Year,'" . $_Fees . "' as Course_Fee,'" . $_Share . "' as RKCL_Share, '" . $_VMOUShare . "' as VMOU_Share From tbl_batch_master";
            $_DuplicateQuery = "Select * From tbl_batch_master Where Batch_Name='" . $_BatchName . "' and Course_Code = '" . $_Course . "' and Batch_StartDate='" . $_Sdate . "' and Batch_EndDate='" . $_Edate . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

                //GST Code  added
                $selectquerybatchid = "select max(Batch_Code) as Batch from tbl_batch_master";
                $Gst_Invoce_Category_Id = $_ObjConnection->ExecuteQuery($selectquerybatchid, Message::SelectStatement);
                $Gst_Invoce_Category_row = mysqli_fetch_array($Gst_Invoce_Category_Id[2]);
                $batchcode = $Gst_Invoce_Category_row['Batch'];
                if ($_Course == 5) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';
                    //$Gst_Invoce_BaseFee = '678';
                    $Gst_Invoce_VMOU = '0';
                    // $Gst_Invoce_TutionFee = '2200';
                    // $Gst_Invoce_TotalFee = '3000';
                     //from 1 Aug 2019 fees changes :-
                     $Gst_Invoce_BaseFee = '847.46';
                    $Gst_Invoce_TutionFee = '5000';
                    $Gst_Invoce_TotalFee = '6000';
                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);                    
//                    $Gst_Invoce_BaseFee = '855';
//                    $Gst_Invoce_VMOU = '145';
                    // $Gst_Invoce_BaseFee = '815';
                    // $Gst_Invoce_VMOU = '185';
                    $Gst_Invoce_BaseFee = '799';
                    $Gst_Invoce_VMOU = '201';
                    // $Gst_Invoce_TutionFee = '1850';
                    // $Gst_Invoce_TotalFee = '2850';
                    //from 1 Aug 2019 fees changes :-
                       $Gst_Invoce_TutionFee = '2350';
                    $Gst_Invoce_TotalFee = '3350';
                } elseif ($_Course == 4) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    
//                    $Gst_Invoce_BaseFee = '855';
//                    $Gst_Invoce_VMOU = '145';
                     // $Gst_Invoce_BaseFee = '815';
                    // $Gst_Invoce_VMOU = '185';
                    $Gst_Invoce_BaseFee = '799';
                    $Gst_Invoce_VMOU = '201';
                    $Gst_Invoce_TutionFee = '1700';
                    $Gst_Invoce_TotalFee = '2700';
                }
                elseif ($_Course == 3) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }				
				
				elseif ($_Course == 23) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';
                    $Gst_Invoce_BaseFee = '97.46';
                    $Gst_Invoce_VMOU = '185';
                    $Gst_Invoce_TutionFee = '300';
                    $Gst_Invoce_TotalFee = '350';
                }   


           //     $Gst_Invoce_CGST = ($Gst_Invoce_BaseFee) * 0.09;
           //     $Gst_Invoce_SGST = ($Gst_Invoce_BaseFee) * 0.09;
					elseif ($_Course == 21) {

                    $Gst_Invoce_BaseFee = '847.45';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '1000';
                    $Gst_Invoce_TotalFee = '6500';
                    $Gst_Invoce_CGST = '76.27';
                    $Gst_Invoce_SGST = '76.27';
                }
                 elseif ($_Course == 25) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';
                    $Gst_Invoce_BaseFee = '2203.38';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '2600';
                    $Gst_Invoce_TotalFee = '4000';
                    $Gst_Invoce_CGST = '198.31';
                    $Gst_Invoce_SGST = '198.31';
                }
                elseif ($_Course == 28) {

                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';
                    $Gst_Invoce_BaseFee = '3389.84';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '4000';
                    $Gst_Invoce_TotalFee = '5500';
                    $Gst_Invoce_CGST = '305.08';
                    $Gst_Invoce_SGST = '305.08';
                }
                elseif ($_Course == 29) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';
                    $Gst_Invoce_BaseFee = '4661.00';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '5500';
                    $Gst_Invoce_TotalFee = '7500';
                    $Gst_Invoce_CGST = '419.50';
                    $Gst_Invoce_SGST = '419.50';
                }
                elseif ($_Course == 30) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';
                    $Gst_Invoce_BaseFee = '4661.00';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '5500';
                    $Gst_Invoce_TotalFee = '7500';
                    $Gst_Invoce_CGST = '419.50';
                    $Gst_Invoce_SGST = '419.50';
                }
                elseif ($_Course == 31) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';
                    $Gst_Invoce_BaseFee = '4661.00';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '5500';
                    $Gst_Invoce_TotalFee = '7500';
                    $Gst_Invoce_CGST = '419.50';
                    $Gst_Invoce_SGST = '419.50';
                }
                elseif ($_Course == 32) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';
                    $Gst_Invoce_BaseFee = '3389.84';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '4000';
                    $Gst_Invoce_TotalFee = '5500';
                    $Gst_Invoce_CGST = '305.08';
                    $Gst_Invoce_SGST = '305.08';
                }
				
				else {

                    $Gst_Invoce_BaseFee = '0';
                    $Gst_Invoce_VMOU = '0';
                    $Gst_Invoce_TutionFee = '0';
                    $Gst_Invoce_TotalFee = '0';
                }
				$Gst_Invoce_CGST = ($Gst_Invoce_BaseFee) * 0.09;
                $Gst_Invoce_SGST = ($Gst_Invoce_BaseFee) * 0.09;
                $_InsertQuery_Invoice = "insert into tbl_gst_invoice_master 
	(Gst_Invoce_Category_Id, 
	Gst_Invoce_Category_Type, 
	Gst_Invoce_SAC, 
	Gst_Invoce_BaseFee, 
	Gst_Invoce_CGST, 
	Gst_Invoce_SGST, 
	Gst_Invoce_VMOU, 
	Gst_Invoce_TutionFee, 
	Gst_Invoce_TotalFee
	
	)
	values
	('" . $batchcode . "', 
	'Admission', 
	'999294', 
	'" . $Gst_Invoce_BaseFee . "', 
	'" . $Gst_Invoce_CGST . "', 
	'" . $Gst_Invoce_SGST . "', 
	'" . $Gst_Invoce_VMOU . "', 
	'" . $Gst_Invoce_TutionFee . "', 
	'" . $Gst_Invoce_TotalFee . "'
	)";
                $_Response1 = $_ObjConnection->ExecuteQuery($_InsertQuery_Invoice, Message::InsertStatement);


                // GST Code ended
                
            $photodir = 'admission_photo/'.$_LearnerBatchNameFTP; 
            $signdir = 'admission_sign/'.$_LearnerBatchNameFTP; 
            // $nest = $ftpaddress.'admission_photo/'.$_LearnerBatchNameFTP.'/'; 
            if (!(ftp_mkdir($ftpconn, $photodir)))
              {
              echo "Error while creating $photodir";
              }
            if (!(ftp_mkdir($ftpconn, $signdir)))
              {
              echo "Error while creating $signdir";
              }
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetMaxBatchCode($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select Max(Batch_Code) as Code from tbl_batch_master where Course_Code = '" . $_Course . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetMaxIntakeBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select Max(Intake_Batch) as IntakeCode from tbl_intake_master where Intake_Course = '" . $_Course . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Addintake($_Course, $_BatchCode, $_IntakeCode) {
        global $_ObjConnection;
        $_Response1 = array();
        $_ObjConnection->Connect();
        try {
             $_IntakeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_IntakeCode);
             $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_Course = "1";
            $_Insert = "INSERT INTO tbl_intake_master (Intake_User, Intake_Center, Intake_Course, Intake_Batch, 
                        Intake_Base_Intake, Intake_Available, Intake_Consumed,Intake_NOS)
                        SELECT Intake_User, Intake_Center,
                        Intake_Course,'" . $_BatchCode . "',Intake_Base_Intake,Intake_Available,Intake_Consumed,Intake_NOS
                        FROM tbl_intake_master where Intake_Batch = '" . $_IntakeCode . "' and Intake_Course = '" . $_Course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
            
            $_UpdateQuery = "UPDATE tbl_intake_master set Intake_Available = ABS(Intake_Available) where Intake_Batch = '" . $_BatchCode . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Batch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $course . "' AND CURDATE() >= Batch_StartDate AND CURDATE() <= Batch_EndDate";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ALLBatch() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
         //   $_SelectQuery = "Select a.Event_Batch AS Batch_Code, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '".$course."' AND CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='3' AND Event_Payment='1'";
           
		   $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "' ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ModifyAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "Select DISTINCT Admission_Batch, Batch_Name From tbl_admission as a INNER JOIN tbl_batch_master as b ON a.Admission_Batch = b.Batch_Code WHERE Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLEventBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $_Course . "' AND Now() >= Event_Startdate AND
			Now() <= Event_Enddate AND Event_Name='1'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLModifyEventBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS 
			b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $_Course . "' AND Now() >= Event_Startdate
			AND Now() <= Event_Enddate AND Event_Name='2'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionBatchName($_BatchCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_BatchCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchCode);
            $_SelectQuery = "Select Batch_Name From tbl_batch_master WHERE Batch_Code = '" . $_BatchCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetBatchDetail($_BatchCode, $_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_BatchCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchCode);
            $_SelectQuery = "Select Batch_Code,Batch_Name,Batch_StartDate,Batch_EndDate,Batch_Status,Admission_Installation_Mode,Certificate_Code,Financial_Year,Year,Course_Fee,RKCL_Share,Course_Name From tbl_batch_master as a Inner Join tbl_course_master as b on a.Course_Code=b.Course_Code WHERE a.Batch_Code = '" . $_BatchCode . "' AND a.Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_BatchName, $_Course, $_Sdate, $_Edate, $_Fees, $_Share) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_BatchName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchName);
            $_UpdateQuery = "Update tbl_batch_master set Batch_StartDate='" . $_Sdate . "',"
                    . "Batch_EndDate='" . $_Edate . "'," . "Course_Fee='" . $_Fees . "', " . "RKCL_Share='" . $_Share . "'"
                    . " Where Course_Code='" . $_Course . "' AND Batch_Code='" . $_BatchName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLPhotoSignProcessEventBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $_Course . "' AND CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='9'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLPhotoSignReuploadEventBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select DISTINCT a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='10'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
		

    }
	 public function FILLAdmissionBatchCourse($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code, Course_Name FROM tbl_batch_master  as a inner join "
                    . " tbl_course_master as b on a.Course_Code=b.Course_Code WHERE a.Course_Code='" . $course . "' ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

   public function FILLModifyForAadhar($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $_Course . "' AND Now() >= Event_Startdate AND
			Now() <= Event_Enddate AND Event_Name='17'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}

?>