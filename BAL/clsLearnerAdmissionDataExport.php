<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsLearnerAdmissionDataExport.php
 *
 * @author Mayank
 */

require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsLearnerAdmissionDataExport 
{
    //put your code here
  
  public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','3','4','5','22','23','25')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAllBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
   public function GetAll($course,$batch) 
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
				$_SelectQuery = "select Admission_Code, Admission_LearnerCode, Admission_ITGK_Code, BioMatric_Status,
				Admission_Aadhar_Status, Admission_Date, Admission_Date_LastModified, Admission_Date_Payment, Admission_Course,
				Admission_Batch,Admission_Advance_CourseCode,Admission_Course_Category,Admission_Fee,a.Admission_Installation_Mode,
				Admission_PhotoUpload_Status,Admission_SignUpload_Status,Admission_PhotoProcessing_Status,rejection_reason,
				rejection_type,Admission_Payment_Status,Admission_ReceiptPrint_Status,Admission_TranRefNo,Admission_RKCL_Trnid,
				Admission_Name,Admission_Fname,Admission_DOB,Admission_MTongue,Admission_Scan,Admission_Photo,Admission_Sign,
				Admission_Gender,Admission_MaritalStatus,Admission_Medium,Admission_PH,Admission_PID,Admission_UID,
				District_Name,Tehsil_Name,Admission_Address,Admission_PIN,Admission_Mobile,Admission_Phone,
				Admission_Email,Qualification_Name,LearnerType_Name,Admission_GPFNO,a.User_Code,
				Admission_RspName,a.Timestamp,Admission_yearid,a.IsNewRecord,User_device_id,g.Course_Name as course, h.Batch_Name as batch
				from tbl_admission as a inner join tbl_course_master as g on a.Admission_Course=g.Course_Code
				inner join tbl_batch_master as h on  a.Admission_Batch=h.Batch_Code left join tbl_district_master as c
				on a.Admission_District=c.District_Code left join tbl_tehsil_master as d on a.Admission_Tehsil=d.Tehsil_Code
				left join tbl_qualification_master as e on a.Admission_Qualification=e.Qualification_Code left join
				tbl_learnertype_master as l on a.Admission_Ltype=l.LearnerType_Code
				where a.Admission_Course='".$course."' and a.Admission_Batch='".$batch."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
}
