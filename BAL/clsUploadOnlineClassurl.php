<?php

/**
 * Description of clsUploadOnlineClassurl
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsUploadOnlineClassurl {

    //put your code here

    public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('3','24') ORDER BY Course_Code ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
           $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "' and Batch_Code>= '249' ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function Add($course,$batch,$slotime,$classurl)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

					$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
					$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
					
                $_InsertQuery = "insert into tbl_itgk_onlineclassurl_wcd (Course_Code,Batch_Code,Slote_Time,Class_Url,ITGKCode,RSPCode,DistrictCode)values('".$course."','".$batch."','".$slotime."','".$classurl."','".$_SESSION['User_LoginId']."','".$_SESSION['User_Rsp']."','".$_SESSION['Organization_District']."')";
                $ChkSlot = "select * from tbl_itgk_onlineclassurl_wcd where Course_Code='".$course."' and Batch_Code = '".$batch."' and Slote_Time = '".$slotime."' and ITGKCode='".$_SESSION['User_LoginId']."'";
                $_ResponseChkSlot = $_ObjConnection->ExecuteQuery($ChkSlot, Message::SelectStatement);
                if ($_ResponseChkSlot[0] == Message::NoRecordFound) {
                    $_chkDuplicate = "Select * from tbl_itgk_onlineclassurl_wcd where Course_Code='".$course."' and Batch_Code = '".$batch."' and Slote_Time = '".$slotime."' and Class_Url='".$classurl."' and ITGKCode='".$_SESSION['User_LoginId']."'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
                    if ($_Response2[0] == Message::NoRecordFound) {
                        $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    } else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                    }
                }
                else{
                    $_Response[0] = "duplicatslot";
                    $_Response[1] = Message::Error;
                 
                }

            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

            }
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Chkslottime($course,$batch,$slotime)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$slotime = mysqli_real_escape_string($_ObjConnection->Connect(),$slotime);
				
            $_SelectQuery = "select * from tbl_itgk_onlineclassurl_wcd where Course_Code='".$course."' and Batch_Code = '".$batch."' and Slote_Time = '".$slotime."' and ITGKCode='".$_SESSION['User_LoginId']."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }


    public function GetListITGK($course,$batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

					
                 $_SelectQuery3 = "SELECT Course_Name,Batch_Name,Slote_Time,Class_Url 
                 from tbl_itgk_onlineclassurl_wcd as a inner join tbl_course_master as c 
                 on a.Course_Code=c.Course_Code inner join tbl_batch_master as b on 
                 a.Batch_Code=b.Batch_Code where a.ITGKCode='".$_SESSION['User_LoginId']."'";

                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

            }

        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function getAllEmployeeForOneViewAttence($course,$batch,$mmyy)
        {
          
            $mmyys=$mmyy."-00 00:00:00";
            $mmyye=$mmyy."-31 23:59:59";
            global $_ObjConnection;
            $_ObjConnection->Connect();
			
			$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
			$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
			$mmyy = mysqli_real_escape_string($_ObjConnection->Connect(),$mmyy);
			
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                    if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 28){

                         $Query="select distinct ITGKCode from tbl_itgk_onlineclassurl_wcd where Course_Code='".$course."' and Batch_Code = '".$batch."' and Slote_Time >='".$mmyys."' and Slote_Time <='".$mmyye."'";
                            $_Response3 = $_ObjConnection->ExecuteQuery($Query, Message::SelectStatement);
                            return $_Response3;
                    }
                    elseif($_SESSION['User_UserRoll'] == 14 ){
                         $Query="select distinct ITGKCode from tbl_itgk_onlineclassurl_wcd where Course_Code='".$course."' and Batch_Code = '".$batch."' and RSPCode='".$_SESSION['User_Code']."' and Slote_Time >='".$mmyys."' and Slote_Time <='".$mmyye."'";
                            $_Response3 = $_ObjConnection->ExecuteQuery($Query, Message::SelectStatement);
                            return $_Response3;
                    }
                    elseif($_SESSION['User_UserRoll'] == 17 ){
                         $Query="select distinct ITGKCode from tbl_itgk_onlineclassurl_wcd where Course_Code='".$course."' and Batch_Code = '".$batch."' and DistrictCode='".$_SESSION['Organization_District']."' and Slote_Time >='".$mmyys."' and Slote_Time <='".$mmyye."'";

                    }
           
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

            }
        }

    public function AttendanceView($date,$empID,$course,$batch)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
			
			$date = mysqli_real_escape_string($_ObjConnection->Connect(),$date);
			$empID = mysqli_real_escape_string($_ObjConnection->Connect(),$empID);
			$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
			$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
			
            $sdate = $date." 00:00:00";
            $edate = $date." 23:59:59";
            $Query="select * from tbl_itgk_onlineclassurl_wcd where ITGKCode='".$empID."' and Slote_Time >='".$sdate."' and Slote_Time <='".$edate."' and Course_Code='".$course."' and Batch_Code = '".$batch."'";
            $_Response3 = $_ObjConnection->ExecuteQuery($Query, Message::SelectStatement);
            return $_Response3;
        }
    public function GetURL($autoid)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
           
		   $autoid = mysqli_real_escape_string($_ObjConnection->Connect(),$autoid);
		   
            $Query="select * from tbl_itgk_onlineclassurl_wcd where ClassUrlId in ($autoid)";
            $_Response3 = $_ObjConnection->ExecuteQuery($Query, Message::SelectStatement);
            return $_Response3;
        }

}
