<?php
/**
 * Description of clsWcdItgkApprovedCount
 *
 * @author Mayank
 */
require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsWcdItgkApprovedCount {
    //put your code here

public function GetAllCount($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
			$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			
			if($_LoginUserRole == '1' || $_LoginUserRole == '4' || $_LoginUserRole == '8'){
				if($_course=='24'){
					$_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, 
							sum(case when Oasis_Admission_LearnerStatus in ('Pending','Approved','Rejected') then 1 else 0 end) AS total,
							sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS approved,
							sum(case when Oasis_Admission_LearnerStatus in ('Rejected') then 1 else 0 end) AS reject
							from tbl_oasis_admission as b inner join tbl_district_master as c on 
							b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND
							b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_Eligibility='eligible' 
							group by b.Oasis_Admission_Final_Preference";
				}
				else{
					$_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, 
							sum(case when Oasis_Admission_LearnerStatus in ('Pending','Approved','Rejected') then 1 else 0 end) AS total,
							sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS approved,
							sum(case when Oasis_Admission_LearnerStatus in ('Rejected') then 1 else 0 end) AS reject
							from tbl_oasis_admission as b inner join tbl_district_master as c on 
							b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND
							b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_Eligibility='eligible' 
							group by b.Oasis_Admission_Final_Preference having total>4";
				}
				
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}			
			else if ($_LoginUserRole == 17) {
					if($_course=='24'){
						$_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, 
							sum(case when Oasis_Admission_LearnerStatus in ('Pending','Approved','Rejected') then 1 else 0 end) AS total,
							sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS approved,
							sum(case when Oasis_Admission_LearnerStatus in ('Rejected') then 1 else 0 end) AS reject
							from tbl_oasis_admission as b inner join tbl_district_master as c on 
							b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND
							b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_Eligibility='eligible'
							AND b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "'
							group by b.Oasis_Admission_Final_Preference";
					}
					else{
						$_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, 
							sum(case when Oasis_Admission_LearnerStatus in ('Pending','Approved','Rejected') then 1 else 0 end) AS total,
							sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS approved,
							sum(case when Oasis_Admission_LearnerStatus in ('Rejected') then 1 else 0 end) AS reject
							from tbl_oasis_admission as b inner join tbl_district_master as c on 
							b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND
							b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_Eligibility='eligible'
							AND b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "'
							group by b.Oasis_Admission_Final_Preference having total>4";
					}
                
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
               }					
		}
		catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}