<?php

/**
 * Description of clsRedHatAdmission
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRedHatAdmission {
	
//put your code here
    public function Add($_LearnerName, $_LearnerCode, $_ParentName, $_DOB, $_Mobile) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_SMS = "Dear Learner, " . $_LearnerName . " Your Admission in Red Hat Advance Course has been Uploaded to RKCL Server.Please 
		Note your Learner Code for further Communication with RKCL " . $_LearnerCode;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
						$_ITGK_Code = $_SESSION['User_LoginId'];
                        $_User_Code = $_SESSION['User_Code'];
                        $_User_Rsp = $_SESSION['User_Rsp'];
						
						$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
						
                $_InsertQuery = "Insert Into tbl_admission_rhls(Admission_RHLS_Code,Admission_RHLS_LearnerCode,
                                Admission_RHLS_ITGK_Code,Admission_RHLS_Name,Admission_RHLS_Fname,Admission_RHLS_DOB,
                                Admission_RHLS_Mobile,User_Code,Admission_RHLS_RspName) 
                                Select Case When Max(Admission_RHLS_Code) Is Null Then 1 Else Max(Admission_RHLS_Code)+1 End as 
								Admission_RHLS_Code, '" . $_LearnerCode . "' as Admission_RHLS_LearnerCode,
								'" . $_ITGK_Code . "' as Admission_RHLS_ITGK_Code, '" . $_LearnerName . "' as Admission_RHLS_Name,
								'" . $_ParentName . "' as Admission_RHLS_Fname,  '" . $_DOB . "' as Admission_RHLS_DOB,
								'" . $_Mobile . "' as Admission_RHLS_Mobile,'" . $_User_Code . "' as User_Code,
								'" . $_User_Rsp . "' as Admission_RHLS_RspName From tbl_admission_rhls";


                        $_chkDuplicate_learnerCode = "Select Admission_RHLS_LearnerCode From tbl_admission_rhls Where
														Admission_RHLS_LearnerCode='" . $_LearnerCode . "'";
                        $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate_learnerCode, Message::SelectStatement);
                        if ($_Response2[0] == Message::NoRecordFound) {
                            $_DuplicateQuery = "Select * From tbl_admission_rhls Where Admission_RHLS_Name='" . $_LearnerName . "' 
								AND Admission_RHLS_Fname = '" . $_ParentName . "' AND Admission_RHLS_DOB = '" . $_DOB . "' 
								AND Admission_RHLS_ITGK_Code = '" . $_ITGK_Code . "'";
                            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                            if ($_Response[0] == Message::NoRecordFound) {                                
                                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                    SendSMS($_Mobile, $_SMS);                              
                            } else {
                                $_Response[0] = Message::DuplicateRecord;
                                $_Response[1] = Message::Error;
                            }
                        } else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
                    
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    

    public function GetAdmissionCount() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LoginId = $_SESSION['User_LoginId'];
				
				$_SelectQuery3 = "select sum(rhls_qty) as qty from tbl_buy_rhls where rhls_code='".$_LoginId."' and rhls_pay_status='1'";
				$_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
					
						$_Row1 = mysqli_fetch_array($_Response2[2]);							
						$_admissionQty=$_Row1['qty'];
							if($_admissionQty==''){
								echo "00";
								return;
							}
							
				
				$_SelectQuery = "Select count(Admission_RHLS_LearnerCode) as lcode FROM tbl_admission_rhls WHERE 
								Admission_RHLS_ITGK_Code = '" . $_LoginId . "'"; 
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				$_Row = mysqli_fetch_array($_Response[2]);
				$_adcount=$_Row['lcode'];				
				
				$totaladmissioncount=$_admissionQty*20;
				
				if($totaladmissioncount > $_adcount){
					echo $_adcount;
					return;
				}
				else {
					echo "10000";
					return;
				}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        //return $_Response;
    }
	
	public function SentSms($mobileno)
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_Response = $this->SendOTPNEW($mobileno);
         } 
		 catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	   public function ResendOTP($mobileno)
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {                   
                $_Response = $this->ResendOTPNEW($mobileno);                
         } 
		 catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function GenerateOTP($length = 6, $chars = '1234567890') {
    $chars_length = (strlen($chars) - 1);
    $string = $chars{rand(0, $chars_length)};
    for ($i = 1; $i < $length; $i = strlen($string)) {
        $r = $chars{rand(0, $chars_length)};
        if ($r != $string{$i - 1})
            $string .= $r;
    }
    return $string;
}
	
	public function SendOTPNEW($mobileno = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_OTP = $this->GenerateOTP();

        try {
            $_Mobile = $mobileno;
            
            date_default_timezone_set('Asia/Kolkata');
            $_OTP_StartOn=date("Y-m-d h:i:s");
            $_OTP_EndOn=date("Y-m-d h:i:s", strtotime("+10 minutes"));

            $_InsertQuery = "Insert Into tbl_admission_rhls_otp(RHLS_Otp_Code,RHLS_Otp_Mobile,
								RHLS_Otp_OTP,OTP_StartOn,OTP_EndOn) 
				Select Case When Max(RHLS_Otp_Code) Is Null Then 1 Else Max(RHLS_Otp_Code)+1 End as RHLS_Otp_Code, 
				'" . $_Mobile . "' as RHLS_Otp_Mobile, 
				'" . $_OTP . "' as RHLS_Otp_OTP, 
				'" . $_OTP_StartOn . "' as OTP_StartOn,
				'" . $_OTP_EndOn . "' as OTP_EndOn 
            From tbl_admission_rhls_otp";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_admission_rhls_otp Where RHLS_Otp_Mobile='" . $_Mobile . "' AND
								RHLS_Otp_Status = 0";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            }
            else
            {
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                $RHLS_Otp_Code=$_row['RHLS_Otp_Code'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d h:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);
                if($datetime1 > $datetime2)
                {
                     //echo 'IF NOT EXPIRE';die;
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
                else
                {   
                    //echo 'IF EXPIRE';die;
                    $_UpdateQuery = "Update tbl_admission_rhls_otp set RHLS_Otp_Status='1' WHERE 
										RHLS_Otp_Code='" . $RHLS_Otp_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }
	
	public function ResendOTPNEW($mobileno = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_OTP = $this->GenerateOTP();

        try {            
            $_Mobile = $mobileno;
            
            date_default_timezone_set('Asia/Kolkata');
            $_OTP_StartOn=date("Y-m-d h:i:s");
            $_OTP_EndOn=date("Y-m-d h:i:s", strtotime("+10 minutes"));
           
            $_InsertQuery = "Insert Into tbl_admission_rhls_otp(RHLS_Otp_Code,RHLS_Otp_Mobile,
								RHLS_Otp_OTP,OTP_StartOn,OTP_EndOn) 
				Select Case When Max(RHLS_Otp_Code) Is Null Then 1 Else Max(RHLS_Otp_Code)+1 End as RHLS_Otp_Code, 
				'" . $_Mobile . "' as RHLS_Otp_Mobile, 
				'" . $_OTP . "' as RHLS_Otp_OTP, 
				'" . $_OTP_StartOn . "' as OTP_StartOn,
				'" . $_OTP_EndOn . "' as OTP_EndOn 
            From tbl_admission_rhls_otp";
            $_DuplicateQuery = "Select * From tbl_admission_rhls_otp Where RHLS_Otp_Mobile='" . $_Mobile . "' 
									AND RHLS_Otp_Status = 0";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            }
            else
            {
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                $RHLS_Otp_Code=$_row['RHLS_Otp_Code'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d h:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);
                if($datetime1 > $datetime2)
                {
                     //echo 'IF NOT EXPIRE';die;
                    $_UpdateQuery = "Update tbl_admission_rhls_otp set RHLS_Otp_Status='1' WHERE 
										RHLS_Otp_Code='" . $RHLS_Otp_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
                else
                {   
                    //echo 'IF EXPIRE';die;
                    $_UpdateQuery = "Update tbl_admission_rhls_otp set RHLS_Otp_Status='1' 
										WHERE RHLS_Otp_Code='" . $RHLS_Otp_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }
	
	 public function VerifyOTPNEW($oid, $otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_admission_rhls_otp WHERE RHLS_Otp_Code='" . $oid . "' AND 
								RHLS_Otp_OTP = '" . $otp . "' AND RHLS_Otp_Status = '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if($_Response[0]=='Success'){
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d h:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);


                if($datetime1 > $datetime2)
                {
                    if ($_Response[0] != Message::NoRecordFound) {
                        $_UpdateQuery = "Update tbl_admission_rhls_otp set RHLS_Otp_Status='1' WHERE 
											RHLS_Otp_Code='" . $oid . "'";
                        $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    }
                }
                else
                {   
                    $_Response[0]='EXPIRE';
                }
            }
            else
                {   
                    $_Response[0]='NORECORD';
                }
            
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function getVeriFyOTPForm($id, $_Mobile) {
        return '
        <div class="locksmall"><img src="images/lock_icon_small.png"></div>
                            <div class="panelforgot">Red Hat Learning Subscription(RHLS) Admission </div>
                         
        <div class="validate"><b>Validate OTP: (One time password)</b></div>
            <div class="goto"><a href="frmredhatadmission.php">Go Back</a></div>
            <div style="clear: both;"></div>
            <hr>
        <div style="margin-left:25px;"><span style="color: green;">Dear Learner a one time password has been sent to your 
			registered Mobile No. '.$_Mobile.'.</span><p></p></div>
            <div id="response" style="padding-left: 23px;"></div>  
            <div style="float:left; margin-left:24px;"><span>Please Enter OTP to Verify Your Mobile No.</span><p></p></div>
        <div class="col-lg-12 col-md-12"><input maxlength="8" type="text" id="txtOTP" name="txtOTP" class="inputsearch" onkeypress="javascript:return allownumbers(event);" style="margin-left:8px;" Placeholder="Enter OTP"/>
        <input type="hidden" value="' . $id . '" name="oid" id="oid">
		</div>';
    }
	
	public function TrackStatus($txtacknownumber)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4'){
					$_SelectQuery = "select rhls_id from tbl_buy_rhls where rhls_id='".$txtacknownumber."'";
					$_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
						if ($_Response1[0] == Message::NoRecordFound) {
							$er='Acknowledgement Number Not Exist With Us';
							$_Response[0] = $er;
						} else {
								$_SelectQuery = "select * from tbl_buy_rhls where rhls_id='".$txtacknownumber."'";
								$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
								return $_Response;
							}
				}
				else if($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '7'){
					$_SelectQuery = "select rhls_id from tbl_buy_rhls where rhls_id='".$txtacknownumber."' 
						and rhls_code='".$_SESSION['User_LoginId']."'";
					$_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
						if ($_Response1[0] == Message::NoRecordFound) {
							$er='Acknowledgement Number Not Exist With Us';
							$_Response[0] = $er;
						} else {
							$_SelectQuery = "select * from tbl_buy_rhls where rhls_id='".$txtacknownumber."'";
							$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
							return $_Response;
						}
				}           
            
           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }

}
