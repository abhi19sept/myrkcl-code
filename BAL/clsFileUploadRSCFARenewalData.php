<?php

/**
 * Description of clsFileUploadRSCFARenewalData
 *
 * @author Mayank
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsFileUploadRSCFARenewalData {

    public function Addrscfsrenewaldata($EOI_Code, $itgkcode, $_fname) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {  
            $EOI_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$EOI_Code);
            $itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkcode);
            $_fname = mysqli_real_escape_string($_ObjConnection->Connect(),$_fname);
				if($EOI_Code=='19' || $EOI_Code=='20'){
					date_default_timezone_set('Asia/Kolkata');
					$_Date = date("Y-m-d");
				
					$_SelectQuery = "Select EXTRACT(Year FROM CourseITGK_ExpireDate) as CourseITGK_ExpireDate,
									datediff('" . $_Date . "', CourseITGK_ExpireDate) as DateDiffernce From tbl_courseitgk_mapping 
									WHERE Courseitgk_ITGK = '" . $itgkcode . "' AND Courseitgk_EOI in ('19','20') AND EOI_Fee_Confirm='1'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					
					if($_Response[0]== Message::NoRecordFound) return;
					
					$row=mysqli_fetch_array($_Response[2]);
						$expire_year=$row['CourseITGK_ExpireDate'];
						$EoiDate=$row['DateDiffernce'];
						$previous_year=($expire_year-1);
				
							if($EoiDate<=60){
								$_DuplicateQuery = "Select * From tbl_eoi_centerlist Where EOI_Code='" . $EOI_Code . "' AND
													EOI_ECL = '" . $itgkcode . "' AND EOI_Year='" . $previous_year . "' AND EOI_Status_Flag='Y'";
								$_Responsess = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
					
								if ($_Responsess[0] == Message::NoRecordFound) {							 	
									$_InsertQuery = "INSERT INTO tbl_eoi_centerlist (EOI_ID, EOI_Code, EOI_ECL, EOI_Year,"
												. "EOI_DateTime, EOI_Filename, EOI_Status_Flag) "
												. "Select Case When Max(EOI_ID) Is Null Then 1 Else Max(EOI_ID)+1 End as EOI_ID,"
												. "'" . $EOI_Code . "' as EOI_Code,'" . $itgkcode . "' as EOI_ECL,'" . $previous_year . "' as EOI_Year,"
												. "'" . $_Date . "' as EOI_DateTime, '" . $_fname . "' as EOI_Filename, 'Y' as EOI_Status_Flag"
												. " From tbl_eoi_centerlist";
									$_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								
									$_UpdateQuery = "Update tbl_courseitgk_mapping set CourseITGK_Renewal_Status = 'Y' 
														where Courseitgk_ITGK='" . $itgkcode . "' AND Courseitgk_EOI IN('5','6','7')";
									$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
								
									$_InsertQuery1 = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
												. "EOI_Log_DateTime, EOI_Log_Filename,EOI_Log_Status_Flag) "
												. "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
												. "'" . $EOI_Code . "' as EOI_Log_Code,'" . $itgkcode . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
												. "'" . $_Date . "' as EOI_Log_DateTime, '" . $_fname . "' as EOI_Log_Filename, 'Updated' as EOI_Log_Status_Flag"
												. " From tbl_eoi_centerlist_Log";
									$_ResponseLog = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);								
								}
								else {
									$_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
																. "EOI_Log_DateTime, EOI_Log_Filename) "
																. "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
																. "'" . $EOI_Code . "' as EOI_Log_Code,'" . $itgkcode . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
																. "'" . $_Date . "' as EOI_Log_DateTime, '" . $_fname . "' as EOI_Log_Filename"
																. " From tbl_eoi_centerlist_Log";
									$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								}
							}
							else {
									$_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
																. "EOI_Log_DateTime, EOI_Log_Filename) "
																. "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
																. "'" . $EOI_Code . "' as EOI_Log_Code,'" . $itgkcode . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
																. "'" . $_Date . "' as EOI_Log_DateTime, '" . $_fname . "' as EOI_Log_Filename"
																. " From tbl_eoi_centerlist_Log";
									$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								}
				}		
			} catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
