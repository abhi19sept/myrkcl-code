<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrStatusReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrStatusReport {
    //put your code here
    
     public function GetITGK_NCR_Details() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '11' || $_LoginRole == '30') {
                 
                $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,
                                d.Tehsil_Name as Organization_Tehsil, e.Organization_Name as RSP_Name, f.User_LoginId as ITGKCODE,
                                g.UserProfile_FirstName
                                FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b
                                ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c 
                                ON a.Organization_District=c.District_Code INNER JOIN tbl_tehsil_master as d 
                                ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_organization_detail as e
                                ON a.Org_RspCode=e.Organization_User LEFT JOIN tbl_user_master as f
                                ON a.Org_Ack=f.User_Ack LEFT JOIN tbl_userprofile as g 
                                ON f.User_Code=g.UserProfile_User WHERE a.Org_Visit='Yes'";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '14') {
                $_SelectQuery = "Select DISTINCT a.*, b.Courseitgk_Course, b.Courseitgk_ITGK AS ITGKCODE, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
				c.Organization_Name as ITGK_Name, c.*, j.Block_Name, k.GP_Name,
				g.Organization_Name as RSP_Name, Ncr_Transaction_Amount From tbl_user_master as a 
                                INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
                                INNER join tbl_organization_detail as c on a.User_Code=c.Organization_User
				
				INNER join tbl_user_master as f on a.User_Rsp=f.User_Code
				INNER join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_org_master as h on a.User_Ack=h.Org_Ack
                                LEFT JOIN tbl_ncr_transaction as i on h.Org_TranRefNo=i.Ncr_Transaction_Txtid
                                LEFT JOIN tbl_panchayat_samiti as j on c.Organization_Panchayat=j.Block_Code
                                LEFT JOIN tbl_gram_panchayat as k on c.Organization_Gram=k.GP_Code
				WHERE b.Courseitgk_Course='RS-CIT' AND CURDATE() > b.CourseITGK_ExpireDate AND f.User_LoginId='" . $_SESSION['User_LoginId'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
         public function Get_Faculty_Details($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "select * from tbl_staff_detail where Staff_User='" . $_CenterCode . "' and Staff_Designation='2'";
        
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRenewalPenaltyAmount() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Renewal_Penalty_Amount from tbl_renewal_penalty_master where Renewal_Penalty_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
