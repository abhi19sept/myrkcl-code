<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsWcdVisit
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdVisit {
    //put your code here
    
    public function GetAOList() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "select DISTINCT a.Wcd_Intake_Center from tbl_wcd_intake_master as a inner join tbl_user_master as b
on a.Wcd_Intake_Center=b.User_LoginId where b.User_Rsp = '" . $_SESSION['User_Code'] . "' and Wcd_Intake_Batch in (249,250)";
                } else if ($_SESSION['User_UserRoll'] == 23){
            $_SelectQuery = "select DISTINCT Wcd_Intake_Center from tbl_wcd_intake_master where Wcd_Center_District='" . $_SESSION['Organization_District'] . "' and Wcd_Intake_Batch in (249,250)";        
                }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
             } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code "
                    . " WHERE e.User_LoginId = '" . $_CenterCode . "'";
                    //. " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function AddVisit($_AOCode,$_Date) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_AOCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AOCode);
				$_Date = mysqli_real_escape_string($_ObjConnection->Connect(),$_Date);
				
            $_InsertQuery = "Insert Into tbl_wcd_visit(WCDVisit_Code,WCDVisit_UserCode,WCDVisit_UserRoll,"
                    . "WCDVisit_LoginId,WCDVisit_Center_Code,WCDVisit_Selected_Date) "
                    . "Select Case When Max(WCDVisit_Code) Is Null Then 1 Else Max(WCDVisit_Code)+1 End as WCDVisit_Code,"
                    . "'" . $_SESSION['User_Code'] . "' as WCDVisit_UserCode,"
                    . "'" . $_SESSION['User_UserRoll'] . "' as WCDVisit_UserRoll,"
                    . "'" . $_SESSION['User_LoginId'] . "' as WCDVisit_LoginId,"
                    . "'" . $_AOCode . "' as WCDVisit_Center_Code,"
                    . "'" . $_Date . "' as WCDVisit_Selected_Date"
                    . " From tbl_wcd_visit";
            $_DuplicateQuery = "Select * From tbl_wcd_visit Where WCDVisit_Center_Code='" . $_AOCode . "' AND
				WCDVisit_UserCode = '" . $_SESSION['User_Code'] . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
