
<?php

/**
 * Description of clsGenerateLearnerReceipt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGenerateLearnerReceipt {

    //put your code here
    public function GetDatabyCode($lcode) {
        //echo $userid;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
            $_User_Code = $_SESSION['User_Code'];
            if ($_User_Code == '1' || $_User_Code == '14' || $_User_Code == '4258' || $_User_Code == '4257') {
                $_SelectQuery = "Select * From tbl_Admission WHERE Admission_LearnerCode='" . $lcode . "' and Admission_Batch>='99'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                //print_r($_Response);
            }
            else {
                $_Response[0] = '0';
                $_Response[1] = 'error';
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


    public function GetAllPrintRecpDwnld($lid) { /// for new invoice system GST
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $lid = mysqli_real_escape_string($_ObjConnection->Connect(),$lid);
           $_SelectQuery = "Select Admission_Code,Admission_LearnerCode, Admission_Name, Admission_Batch, 
                Admission_Course,Admission_Fname, Admission_DOB,Admission_Mobile, Gst_Invoce_SAC,
                Admission_ITGK_Code,Admission_Photo, Admission_Sign, Admission_Fee, Admission_Address,
                Admission_Payment_Status,Admission_Date,b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
                b.amount,b.addtime,c.Organization_Name,Course_Name,Course_Duration,Course_DurationType, District_Name,
                Batch_Name, Gst_Invoce_TotalFee,Gst_Invoce_VMOU, RKCL_Share, VMOU_Share FROM tbl_admission 
                as a inner join tbl_admission_invoice as b on a.Admission_Code=b.invoice_ref_id inner join tbl_organization_detail 
                as c on a.User_Code=c.Organization_User inner join tbl_batch_master as d on a.Admission_Batch=d.Batch_Code
                inner join tbl_course_master as e on a.Admission_Course=e.Course_Code inner join tbl_district_master as g on
                Organization_District=District_Code inner join tbl_gst_invoice_master as f on a.Admission_Batch=f.Gst_Invoce_Category_Id WHERE  
                Admission_Code = '" . $lid . "' AND Admission_Payment_Status='1' AND f.Gst_Invoce_Category_Type='admission'";
            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
            //WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function addPhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        if ($coursecode == '5') {
            $Imagepath = '../upload/DownloadPrintRecpt/rscfa.jpg';
        } else {
            $Imagepath = '../upload/DownloadPrintRecpt/rscit.jpg';
        }

        $pdf->Image($Imagepath, 8, 107, 50, 25);
        $pdf->Image($Imagepath, 8, 256, 50, 25);

        $pdf->Output($path, "F");
    }
 public function addNotePhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        if ($coursecode == '1' || $coursecode == '4') {
            $Imagepath = '../upload/DownloadPrintRecpt/GovEmpNote.jpg';
        } 

        $pdf->Image($Imagepath, 63, 123, 140, 10);
        $pdf->Image($Imagepath, 63, 272, 140, 10);
        //$pdf->Image($Imagepath, 8, 256, 50, 25);

        $pdf->Output($path, "F");
    }

}