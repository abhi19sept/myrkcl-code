<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsddAdmissionPayment {

    //put your code here

    public function Update($_Code, $_PayTypeCode, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName, $_txtRKCLTranId) {
        global $_ObjConnection;
        //print_r($_SESSION);

        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_admission_ddpay (admission_ddpay_code, dd_Transaction_Status, dd_Transaction_Name, dd_amount,"
                    . "dd_Transaction_Txtid,dd_Transaction_RKCL_Txnid, dd_no, dd_date,"
                    . "dd_centercode,dd_bank,dd_branch,dd_Transaction_Email) "
                    . "Select Case When Max(admission_ddpay_code) Is Null Then 1 Else Max(admission_ddpay_code)+1 End as admission_ddpay_code,"
                    . "'Intermediate' as dd_Transaction_Status,'" . $_firstname . "' as dd_Transaction_Name,'" . $_amount . "' as dd_amount,"
                    . "'" . $_TranRefNo . "' as dd_Transaction_Txtid,'".$_txtRKCLTranId."' as dd_Transaction_RKCL_Txnid,"
                    . "'" . $_ddno . "' as dd_no,'" . $_dddate . "' as dd_date,'" . $_Code . "' as dd_centercode,'" . $_ddlBankName . "' as dd_bank,'" . $_txtBranchName . "' as dd_branch,'" . $_email . "' as dd_Transaction_Email"
                    . " From tbl_admission_ddpay";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);



            $_InsertQuery1 = "INSERT INTO tbl_admission_transaction (Admission_Transaction_Code, Admission_Transaction_Status, Admission_Transaction_Fname, Admission_Transaction_Amount,"
                    . "Admission_Transaction_Txtid,Admission_Payment_Mode, Admission_Transaction_ProdInfo, Admission_Transaction_Email,"
                    . "Admission_Transaction_CenterCode,Admission_Transaction_RKCL_Txid) "
                    . "Select Case When Max(Admission_Transaction_Code) Is Null Then 1 Else Max(Admission_Transaction_Code)+1 End as Admission_Transaction_Code,"
                    . "'Intermediate' as Admission_Transaction_Status,'" . $_firstname . "' as Admission_Transaction_Fname,'" . $_amount . "' as Admission_Transaction_Amount,"
                    . "'" . $_TranRefNo . "' as Admission_Transaction_Txtid,'DD Mode' as Admission_Payment_Mode,"
                    . "'" . $_PayTypeCode . "' as Admission_Transaction_ProdInfo,'" . $_email . "' as Admission_Transaction_Email,'" . $_Code . "' as Admission_Transaction_CenterCode,'" . $_txtRKCLTranId . "' as Admission_Transaction_RKCL_Txid"
                    . " From tbl_admission_transaction";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);

            $_UpdateQuery = "Update tbl_admission set Admission_Payment_Status = 8, Admission_TranRefNo='" . $_TranRefNo . "' "
                    . "Where Admission_ITGK_Code='" . $_Code . "' AND Admission_RKCL_Trnid = '" . $_txtRKCLTranId . "'";

            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
            $_UpdateQuery1 = "Update tbl_payment_transaction set Pay_Tran_PG_Trnid='" . $_TranRefNo . "' "
                    . "Where Pay_Tran_ITGK='" . $_Code . "' AND Pay_Tran_RKCL_Trnid = '" . $_txtRKCLTranId . "'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select RKCL_Share FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCenterCode($_batchcode, $_coursecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT Admission_ITGK_Code FROM tbl_admission WHERE Admission_Batch = '" . $_batchcode . "' AND Admission_Course = '" . $_coursecode . "' AND Admission_Payment_Status = '8'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDDno($_centercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_admission_ddpay WHERE dd_centercode = '" . $_centercode . "' AND dd_Transaction_Status='Intermediate'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllDdData($_refno) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_admission_ddpay WHERE dd_Transaction_Txtid = '" . $_refno . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateDDPaymentStatus($course, $batch, $_DDtransactionid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $Transactionid = $_SESSION['LearnerAdmissionCodes'];
            // print_r($_SESSION);
             $_UpdateQuery = "Update tbl_admission_ddpay set dd_Transaction_Status = 'Success'"
                    . " Where dd_Transaction_Txtid = '".$_DDtransactionid."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			

            $_UpdateQuery1 = "Update tbl_admission_transaction set Admission_Transaction_Status = 'Success'"
                    . " Where Admission_Transaction_Txtid = '".$_DDtransactionid."'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
			
			$_UpdateQuery3 = "Update tbl_payment_transaction set Pay_Tran_Status = 'PaymentReceiveByDD'"
                    . " Where Pay_Tran_PG_Trnid = '".$_DDtransactionid."'";
            $_Response3 = $_ObjConnection->ExecuteQuery($_UpdateQuery3, Message::UpdateStatement);
			

          echo $_UpdateQuery2 = "Update tbl_admission set Admission_Payment_Status = '1'"
                    . " Where Admission_TranRefNo = '".$_DDtransactionid."'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
			print_r($_Response2);

            

//            $_SMS = "Dear Learner Your Admission Payment for " . $course . " Course in " . $batch . " has been Confirmed. Please start study at your ITGK.";
//            $Mobile = $MobileNo3;
//            SendSMS($Mobile, $_SMS);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    function GetLearnerMobile($_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Admission_Mobile FROM tbl_admission WHERE Admission_TranRefNo IN ($_AdmissionCode)";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
