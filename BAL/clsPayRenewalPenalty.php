<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPayRenewalPenalty
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPayRenewalPenalty extends paymentFunctions {

    //put your code here

    public function GetITGK_SLA_Details() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                    if (isset($_SESSION['User_Rsp']) && !empty($_SESSION['User_Rsp']) && $_SESSION['User_Rsp'] != '0') {
                        $_SelectQuery = "Select DISTINCT a.User_EmailId as ITGKEMAIL, a.User_MobileNo AS ITGKMOBILE,
                                b.Courseitgk_Course, b.Courseitgk_ITGK AS ITGKCODE,
                                b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
				c.Organization_Name as ITGK_Name, c.*, j.Block_Name, k.GP_Name,
				g.Organization_Name as RSP_Name From tbl_user_master as a 
                                INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
                                INNER join tbl_organization_detail as c on a.User_Code=c.Organization_User
				
				INNER join tbl_user_master as f on a.User_Rsp=f.User_Code
				INNER join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_panchayat_samiti as j on c.Organization_Panchayat=j.Block_Code
                                LEFT JOIN tbl_gram_panchayat as k on c.Organization_Gram=k.GP_Code
				WHERE b.Courseitgk_Course='RS-CIT' AND CURDATE() > b.CourseITGK_ExpireDate AND a.User_LoginId='" . $_SESSION['User_LoginId'] . "'";
                        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                        return $_Response;
                    } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "index.php";</script> 
                        <?php

                    }
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script> 
                    <?php

                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }

    public function GetITGK_Penalty_Details() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            date_default_timezone_set('Asia/Kolkata');
            $_Year = date("Y");
            
            $_SelectQuery = "select * from tbl_renewal_penalty where RenewalPenalty_ItgkCode= '" . $_SESSION['User_LoginId'] . "' and RenewalPenalty_Payment_Status='1' and RenewalPenalty_Year = '" . $_Year . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetITGK_Admission_Details($_CenterCode, $_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				
            $SDate = $_StartDate . '00:00:00';
            $EDate = $_EndDate . '00:00:00';
			
			
            $_SelectQuery = "SELECT count(Admission_Code) as Admission_Count from tbl_admission where Admission_ITGK_Code='" . $_CenterCode . "' AND Admission_Date_Payment>'" . $SDate . "' AND Admission_Date_Payment<='" . $EDate . "'  AND Admission_Payment_Status='1'"
                    . " AND Admission_Course in (1,4)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetRenewalPenaltyAmount() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Renewal_Penalty_Amount from tbl_renewal_penalty_master where Renewal_Penalty_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Renewal_Payment($_ITGKName, $_Panchayat, $_GramPanchayat, $_CreatedDate, $_LastRenewal, $_ExpireDate, $_CreationAmount, $_RequiedAdmission, $_AdmissionCount, $_Shortfall, $_Penalty) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
//        date_default_timezone_set('Asia/Kolkata');
//        $_Year = date("Y");
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                    if (isset($_SESSION['User_Rsp']) && !empty($_SESSION['User_Rsp']) && $_SESSION['User_Rsp'] != '0') {
                        $_Year = date('Y', strtotime($_ExpireDate));
                        $_InsertQuery = "Insert IGNORE Into tbl_renewal_penalty(RenewalPenalty_ItgkCode, RenewalPenalty_Year, RenewalPenalty_ItgkName,"
                                . "RenewalPenalty_Panchayat,RenewalPenalty_Gram,RenewalPenalty_Created_Date,RenewalPenalty_LastRenewal_Date,"
                                . "RenewalPenalty_Expire_Date, RenewalPenalty_AreaType, RenewalPenalty_Required_Admission, "
                                . "RenewalPenalty_AdmissionCount, RenewalPenalty_Shortfall, RenewalPenalty_Penalty) "
                                . "values('" . $_SESSION['User_LoginId'] . "', '" . $_Year . "', '" . $_ITGKName . "', '" . $_Panchayat . "', '" . $_GramPanchayat . "', '" . $_CreatedDate . "', "
                                . "'" . $_LastRenewal . "', '" . $_ExpireDate . "',"
                                . " '" . $_CreationAmount . "', '" . $_RequiedAdmission . "', '" . $_AdmissionCount . "', "
                                . "'" . $_Shortfall . "', '" . $_Penalty . "')";
                        $_DuplicateQuery = "Select * From tbl_renewal_penalty Where RenewalPenalty_ItgkCode='" . $_SESSION['User_LoginId'] . "' AND RenewalPenalty_Year = '" . $_Year . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                
            } else {
                $_Response[0] = "Please Proceed To Payment"; //Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
                        
                    } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "index.php";</script> 
                        <?php

                    }
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script> 
                    <?php

                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getUnpaidPaneltyInfo($itgkCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
$itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkCode);
        $_SelectQuery = "SELECT * FROM tbl_renewal_penalty WHERE RenewalPenalty_ItgkCode = '" . $itgkCode . "' AND RenewalPenalty_Payment_Status = '0' ORDER BY RenewalPenalty_Code DESC LIMIT 1";

        return $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
    }


    public function AddPayTran($productinfo) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_Response = $this->getUnpaidPaneltyInfo($_SESSION['User_LoginId']);
                if (mysqli_num_rows($_Response[2])) {
                    $row = mysqli_fetch_assoc($_Response[2]);
                    $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $row['RenewalPenalty_Penalty'], [$row['RenewalPenalty_Code']], 0, 0, 0);
                }
            } else {
                session_destroy();
            ?>
                <script> window.location.href = "index.php";</script> 
            <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }
		public function ShowPenaltyPay() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b
							ON a.Event_Payment = b.payment_id WHERE a.Event_Category = '13' AND Event_Name='16' AND
							Event_Payment='1' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
