<?php


/**
 * Description of clsAdmission
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsAdmission {
    //put your code here
    public function Add($_LearnerName,$_ParentName,$_IdProof,$_IdNo,$_DOB,$_MotherTongue,$_Medium,$_Gender,$_MaritalStatus,$_District,$_Tehsil,$_Address,$_ResiPh,$_Mobile,
						$_Qualification,$_LearnerType,$_GPFno,$_PhysicallyChallenged,$_Email,$_PIN,$_LearnerCourse,$_LearnerBatch,$_LearnerFee,$_LearnerInstall, 
						$_Photo, $_Sign, $_Scan) {
	 
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LearnerCode = $_ObjConnection->learnercodegen();
	$_SMS = "Dear Learner, ".$_LearnerName." Your Admission has been Uploaded to RKCL Server. Please ask your ITGK to Confirm your payment to RKCL. Please Note your Learner Code for further Communication with RKCL " . $_LearnerCode;
        try {
          
			$_photoname	=	$_Photo .'_photo' . '.png';
			$_signname	=	$_Sign  .'_sign'  . '.png';
			$_scanname	=	$_Scan  .'_scan'  . '.png';
			$_ITGK_Code =   $_SESSION['User_LoginId'];
			$_User_Code =   $_SESSION['User_Code'];
			
			
			 
           $_InsertQuery = "Insert Into tbl_admission(Admission_Code,Admission_LearnerCode,"
                    . "Admission_ITGK_Code,Admission_Course,Admission_Batch,"
                    . "Admission_Fee,Admission_Installation_Mode,Admission_Name,Admission_Fname,Admission_DOB,"
                    . "Admission_MTongue,Admission_Photo,Admission_Sign,Admission_Gender,Admission_Scan,"
                    . "Admission_MaritalStatus,Admission_Medium,Admission_PH,Admission_PID,"
                    . "Admission_UID,Admission_District,Admission_Tehsil,Admission_Address,"
                    . "Admission_PIN,Admission_Mobile,Admission_Phone,Admission_Email,User_Code,"
                    . "Admission_Qualification,Admission_Ltype,Admission_GPFNO) "
                    . "Select Case When Max(Admission_Code) Is Null Then 1 Else Max(Admission_Code)+1 End as Admission_Code,"
                    . "'".$_LearnerCode."' as Admission_LearnerCode,'" .$_ITGK_Code  . "' as Admission_ITGK_Code,"
                    . "'" . $_LearnerCourse . "' as Admission_Course,  '" . $_LearnerBatch . "' as Admission_Batch,"
                    . "'" . $_LearnerFee  . "' as Admission_Fee,'" . $_LearnerInstall  . "' as Admission_Installation_Mode,'" .$_LearnerName. "' as Admission_Name,'" .$_ParentName. "' as Admission_Fname,"
                    . "'" .$_DOB. "' as Admission_DOB,'" .$_MotherTongue. "' as Admission_MTongue,'" .$_photoname. "' as Admission_Photo,'" .$_signname. "' as Admission_Sign,"  
                    . "'" .$_Gender. "' as Admission_Gender,'" .$_scanname. "' as Admission_Scan,"
                    . "'" .$_MaritalStatus. "' as Admission_MaritalStatus,'" .$_Medium. "' as Admission_Medium,"
                    . "'" .$_PhysicallyChallenged. "' as Admission_PH,'" .$_IdProof. "' as Admission_PID,"
                    . "'" .$_IdNo. "' as Admission_UID,'" .$_District. "' as Admission_District,"
                    . "'" .$_Tehsil. "' as Admission_Tehsil,'" . $_Address . "' as Admission_Address,"
                    . "'" .$_PIN. "' as Admission_PIN,'" . $_Mobile . "' as Admission_Mobile,'" . $_ResiPh . "' as Admission_Phone,"
                    . "'" .$_Email. "' as Admission_Email,'" . $_User_Code . "' as User_Code,'" . $_Qualification . "' as Admission_Qualification,"
                    . "'" .$_LearnerType. "' as Admission_Ltype,'" . $_GPFno . "' as Admission_GPFNO"
                    . " From tbl_admission";            
           
            $_DuplicateQuery = "Select * From tbl_admission Where Admission_Name='" . $_LearnerName . "' AND Admission_DOB = '" .$_DOB. "' AND Admission_Fname = '" .$_ParentName. "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            
            //print_r($_Response);
            
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				
                SendSMS($_Mobile, $_SMS);
				 $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                    . "Admission_Log_ITGK_Code,"                 
                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                    . "Select Case When Max(Admission_Log_Code) Is Null Then 1 Else Max(Admission_Log_Code)+1 End as Admission_Log_Code,"
                    . "'" .$_ITGK_Code  . "' as Admission_Log_ITGK_Code,"                                     
                    . "'" .$_photoname. "' as Admission_Log_Photo,'" .$_signname. "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"                   
                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                    . " From tbl_admission_log";
				 $_Response1=$_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
				
				
				$_UpdateQuery = "Update tbl_intake_master set Intake_Available=Intake_Available - 1 ,Intake_Consumed = Intake_Consumed + 1 Where Intake_Center = '" .$_ITGK_Code  . "' AND Intake_Batch = '" . $_LearnerBatch . "' ";
				$_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        //print_r($_Response);
        return $_Response;
    }
    
    public function Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code =   $_SESSION['User_LoginId'];
			$_User_Code =   $_SESSION['User_Code'];			
			
           $_UpdateQuery = "Update tbl_admission set Admission_Name='" . $_LearnerName . "',"
                    . "Admission_Fname='" . $_ParentName . "'," . "Admission_DOB='" . $_DOB . "', " . "Admission_Photo='" . $newpicture . "',"
                    . "Admission_Sign='" . $newsign . "'"
                    . " Where Admission_Code='" . $_Code . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           
           $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                    . "Admission_Log_ITGK_Code,"                 
                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                    . "'" . $_Code . "' as Admission_Log_Code,"
                    . "'" .$_ITGK_Code  . "' as Admission_Log_ITGK_Code,"                                     
                    . "'" .$newpicture. "' as Admission_Log_Photo,'" .$newsign. "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"                   
                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                    . " From tbl_admission_log";
		  $_Response1=$_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
     public function GetDatabyCode($_AdmissionCode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_admission Where Admission_Code='" . $_AdmissionCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	 public function GetAdmissionFee($_batchcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select Course_Fee FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	 public function GetAdmissionInstall($_batchcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
            $_SelectQuery="Select Admission_Installation_Mode FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
           // echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetIntakeAvailable($_batchcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
            $_SelectQuery="Select Intake_Available FROM tbl_intake_master WHERE Intake_Batch = " . $_batchcode . "";
           // echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
   
}
