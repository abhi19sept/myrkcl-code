<?php

/**
 * Description of clsCorrectionTransRpt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsCorrectionTransRpt {

    //put your code here

    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_LoginRole = $_SESSION['User_UserRoll'];
			$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
			$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
			
            // echo $_LoginRole;

            if ($_LoginRole == '1') {

                $_LoginUserType = "1";
            } elseif ($_LoginRole == '3') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '4') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '9') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } 
			elseif ($_LoginRole == '11') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            }
			elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
            } else {
                echo "hello";
            }
            
           // $_SESSION['UserType'] = $_LoginUserType;
            $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_correction_transaction AS b ON a.Centercode = b.Correction_Transaction_CenterCode WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_i = 0;
            $CenterCode2 = '';
            while ($_Row = mysqli_fetch_array($_Response1[2])) {

                $CenterCode2.=$_Row['Centercode'] . ",";
                $_i = $_i + 1;
            }
            $CenterCode3 = rtrim($CenterCode2, ",");
            if ($CenterCode3) {
                $_SelectQuery2 = "Select a.*, b.* From tbl_correction_copy as a INNER JOIN tbl_correction_transaction as b ON a.Correction_TranRefNo = b.Correction_Transaction_Txtid WHERE b.Correction_Transaction_timestamp >= '" . $_StartDate . "' AND b.Correction_Transaction_timestamp <= '" . $_EndDate . "' AND b.Correction_Transaction_CenterCode IN ($CenterCode3)";
                //echo $_SelectQuery;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

    public function ShowSummary($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_LoginRole = $_SESSION['User_UserRoll'];
			$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
			$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
			
            // echo $_LoginRole;

            if ($_LoginRole == '1') {

                $_LoginUserType = "1";
            } elseif ($_LoginRole == '3') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '4') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '9') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } 
			elseif ($_LoginRole == '11') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            }
			elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
            } else {
                echo "hello";
            }
            // $_SESSION['UserType'] = $_LoginUserType;
             $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_correction_transaction AS b ON a.Centercode = b.Correction_Transaction_CenterCode WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_i = 0;
            $CenterCode2 = '';
            while ($_Row = mysqli_fetch_array($_Response1[2])) {

                $CenterCode2.=$_Row['Centercode'] . ",";
                $_i = $_i + 1;
            }
            $CenterCode3 = rtrim($CenterCode2, ",");
            if ($CenterCode3) {
                $_SelectQuery2 = "Select b.Correction_Transaction_ProdInfo, b.Correction_Transaction_CenterCode, b.Correction_Transaction_Fname, b.Correction_Transaction_Status, b.Correction_Transaction_Txtid, b.Correction_Transaction_timestamp, count(a.lcode) as lcount, b.Correction_Transaction_Amount as totalamount From tbl_correction_copy as a INNER JOIN tbl_correction_transaction as b ON a.Correction_TranRefNo = b.Correction_Transaction_Txtid WHERE b.Correction_Transaction_timestamp >= '" . $_StartDate . "' AND b.Correction_Transaction_timestamp <= '" . $_EndDate . "' AND b.Correction_Transaction_CenterCode IN ($CenterCode3)  GROUP BY b.Correction_Transaction_Txtid";
                //echo $_SelectQuery;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

}
