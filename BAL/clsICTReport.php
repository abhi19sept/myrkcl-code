<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$_ObjConnection = new _Connection();
$_Response = array();


class clsICTReport {

    //put your code here

public function GetAllDetails($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '4'){
				 $_SelectQuery = "select * from
										(select 										
										c.exam_held_date,a.Admission_LearnerCode,a.Admission_Name,a.Admission_ITGK_Code,
										b.ITGK_Name,b.District_Name,c.result,d.LearnerType_Name from tbl_admission as a inner 
										join vw_itgkname_distict_rsp as b on a.Admission_ITGK_Code=b.ITGKCODE left join 
										tbl_result as c on a.Admission_LearnerCode=c.scol_no inner join 
										tbl_learnertype_master as d on a.Admission_Ltype=d.LearnerType_Code
										where a.Admission_Batch='". $_batch . "' and Admission_Course='4' and 
										Admission_Payment_Status='1' order by exam_held_date DESC ) as b
										group  by b.Admission_LearnerCode";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}			
		}
		catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FillCourse($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectQuery = "Select Course_Name From tbl_course_master WHERE Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
