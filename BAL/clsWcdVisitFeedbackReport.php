<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsWcdVisitFeedbackReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
class clsWcdVisitFeedbackReport {
    //put your code here
    
    public function GetITGK_NCR_Details() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8') {
                 
                $_SelectQuery = "SELECT distinct a.*,b.*,d.WCDVisit_Selected_Date,d.WCDVisit_Confirm_date,fullname,
                                SUM(LENGTH(WCDFeedback_AdmissionCodes) - LENGTH(REPLACE(WCDFeedback_AdmissionCodes, ',', '')) + 1) as Admission_Count
                                FROM tbl_wcdvisit_feedback as a inner join vw_itgkname_distict_rsp as b on a.WCDFeedback_ITGKCode=b.ITGKCODE
                                inner join tbl_wcd_visit as d on a.WCDFeedback_VisitCode=d.WCDVisit_Code
                                inner join tbl_biomatric_enrollments as e on d.confirm_visiter_id=e.id group by WCDFeedback_AdmissionCodes";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '14') {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
 public function GetDatabyCode($_CenterCode,$_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				 
				 $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				 $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				 
            $_SelectQuery = "select Admission_Code,Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB 
                from tbl_admission where Admission_Code in ($_LearnerCode) and Admission_ITGK_Code='" . $_CenterCode . "'";
                    //. " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
