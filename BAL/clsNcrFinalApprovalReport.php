<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrFinalApprovalReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsNcrFinalApprovalReport {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
                $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code "
                    . " WHERE e.User_Type = 'New'";
//                WHERE e.User_Status = 'New'
    
            } else {
                $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code "
                    . " WHERE e.User_Type = 'New' AND e.User_UserRoll = '15' AND e.User_Rsp='" .$_SESSION['User_Code'] . "'";
            }
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
