<?php

/**
 * Description of clsgraphmaster
 *
 * @author Yogendra
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response2 = array();
$_Response3 = array();
$_Response4= array();
class clsgraphmaster {
    //put your code here
     public function GetAll($_actionvalue) {
        global $_ObjConnection;
		
        $_ObjConnection->Connect();
        try 
		{
           
		   
		   $_SelectQuery = "Select Admission_yearid,count(Admission_LearnerCode) from tbl_admission  GROUP BY Admission_yearid  ORDER BY count(Admission_LearnerCode) DESC limit ".$_actionvalue." ";
		  
           $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			

        } 
		  catch (Exception $_ex) 
		  {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
          }
          return $_Response;
		  
    }
	
	public function GetTotal() {
        global $_ObjConnection;
		
        $_ObjConnection->Connect();
        try 
		{
           
            
			 
			
            $_SelectQuery = "Select DISTINCT Admission_ITGK_Code,count(Admission_LearnerCode) from tbl_admission   GROUP BY Admission_ITGK_Code ORDER BY count(Admission_LearnerCode) DESC  limit 4" ;
			
			
		  
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
		

			

        } 
		  catch (Exception $_ex) 
		  {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
          }
          return $_Response;
		  
    }
	
	
	 public function GetDistrict($_actionvalue,$_actionvalue1) {
        global $_ObjConnection;
		//echo $_actionvalue;
        $_ObjConnection->Connect();
        try 
		{
			
		
		  // $_SelectQuery = "Select District_Name,count(*) from tbl_admission  as a inner join tbl_district_master as b on a.Admission_District=b.District_Code where EXTRACT(YEAR FROM Timestamp)='".$_actionvalue."' OR Admission_Batch='".$_actionvalue1."' OR Admission_ITGK_Code IN ($CenterCode3)  GROUP BY Admission_ITGK_Code ORDER BY count(*) DESC LIMIT 5" ;
		  
		  
		  
		 //$_SelectQuery="SELECT a.Admission_District,d.District_Name As district_name, count(Admission_LearnerCode) as admission_count from tbl_admission AS a INNER JOIN tbl_user_master AS b ON a.Admission_Code = b.User_Code inner join tbl_organization_detail as c ON b.User_Code = c.Organization_User inner join tbl_district_master as d ON c.Organization_District = d.District_Code WHERE Admission_ITGK_Code IN ($CenterCode3) AND EXTRACT(YEAR FROM Timestamp)='".$_actionvalue."' AND Admission_Batch='".$_actionvalue1."'";
        
		 
		 $_SelectQuery="SELECT District_Name ,count(Admission_LearnerCode) as admission_count from tbl_admission as a  inner join tbl_district_master as b ON a.Admission_District = b.District_Code WHERE  Admission_yearid='".$_actionvalue."' AND Admission_Batch='".$_actionvalue1."' group  by b.District_Code ORDER BY count(Admission_LearnerCode) DESC limit 20"; 
		 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
		

        } 
		  catch (Exception $_ex) 
		  {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
          }
          return $_Response;
		  
    }


	
	public function Getyear() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct Admission_yearid from tbl_admission";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
   
   
   
   
   
}
