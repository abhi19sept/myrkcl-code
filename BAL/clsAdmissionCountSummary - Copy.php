<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionCountSummary {

    //put your code here

    public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            $_SESSION['UserType'] = $_LoginUserType;

            if ($_loginflag == "1") {

                $_SelectQuery = "SELECT ( SELECT Count(`Admission_LearnerCode`) FROM tbl_admission where Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' )
								AS Total_Learner, ( SELECT Count(Admission_Payment_Status) FROM tbl_admission Where Admission_Course='" . $_course . "' AND
								Admission_Batch='" . $_batch . "' AND `Admission_Payment_Status` = '1' )  AS Confirmed_Learner";
            } elseif ($_loginflag == "7") {
                $_SelectQuery = "SELECT ( SELECT Count(`Admission_LearnerCode`) FROM tbl_admission where Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "' AND
							 Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' ) AS Total_Learner, 
							( SELECT Count(Admission_Payment_Status) FROM tbl_admission Where Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "' AND
							Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' AND `Admission_Payment_Status` = '1' ) AS Confirmed_Learner";
            } else {
                $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);

                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";

                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");
                $_SelectQuery = "SELECT ( SELECT Count(`Admission_LearnerCode`) FROM tbl_admission where Admission_ITGK_Code IN ($CenterCode3) AND
							 Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' ) AS Total_Learner, 
							( SELECT Count(Admission_Payment_Status) FROM tbl_admission Where Admission_ITGK_Code IN ($CenterCode3) AND
							Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' AND `Admission_Payment_Status` = '1' ) AS Confirmed_Learner";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
