<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */
	
require 'DAL/classconnectionNEW.php' ;
	
$_ObjConnection=new _Connection();
$_Response=array();

class clsPhotoSignProcess {		
		
    
	    public function GetAll($_batch,$_course)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
						$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
						$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
						
					$_LoginRole = $_SESSION['User_UserRoll'];
					if ($_LoginRole == '1') {
						$_LoginUserType = $_LoginRole;
					} 
					elseif ($_LoginRole == '5') {
						$_LoginUserType = "PSAUserCode";
					}
					else {
						echo "Not Valid";
						return;
					}
					$_SESSION['UserType'] = $_LoginUserType;


            if ($_LoginRole == '1') {

                $_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code";
            } else {

                $_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            $_i = 0;
            $CenterCode2 = '';
            while ($_Row = mysqli_fetch_array($_Response[2])) {
                $CenterCode2.=$_Row['Centercode'] . ",";
                $_i = $_i + 1;
            }
            $CenterCode3 = rtrim($CenterCode2, ",");
            
            //$_SelectQuery = "CALL `proc_photosignstatus`(". $_course.", ". $_batch .", ". $array[$k] .")";
			$_SelectQuery = "Select Admission_ITGK_Code,Sum(TotalStudent) as TotalStudent,Sum(Approved) as Approved,Sum(Pending) as Pending,Sum(Rejected) as Rejected,Sum(Reprocess)
							 as Reprocess, Admission_Course,Admission_Batch  From ( select admission_itgk_code,Admission_Course,Admission_Batch,Count(*) as TotalStudent,0 as
							 Approved,0 as Pending,0 as Rejected,0 as Reprocess From tbl_admission Group By Admission_itgk_code,Admission_Course,Admission_Batch
							 Union all select admission_itgk_code,Admission_Course,Admission_Batch,0 as TotalStudent,Count(*) as Approved,0 as Pending,0 as Rejected,0 as Reprocess 
							 From tbl_admission Where Admission_PhotoProcessing_Status='Approved' Group By Admission_itgk_code,Admission_Course,Admission_Batch 
							 Union all select admission_itgk_code,Admission_Course,Admission_Batch,0 as TotalStudent,0 as Approved,Count(*) as Pending,0 as Rejected,0 as Reprocess
							 From tbl_admission Where Admission_PhotoProcessing_Status='Pending' Group By Admission_itgk_code,Admission_Course,Admission_Batch
							 Union all select admission_itgk_code,Admission_Course,Admission_Batch,0 as TotalStudent,0 as Approved,0 as Pending,Count(*) as Rejected,0 as Reprocess
							 From tbl_admission Where Admission_PhotoProcessing_Status='Rejected' Group By Admission_itgk_code,Admission_Course,Admission_Batch
							 Union all select admission_itgk_code,Admission_Course,Admission_Batch,0 as TotalStudent,0 as Approved,0 as Pending,0 as Rejected,Count(*) as Reprocess
							 From tbl_admission Where Admission_PhotoProcessing_Status='Reprocess' Group By Admission_itgk_code,Admission_Course,Admission_Batch ) as a 
							 where Admission_ITGK_Code IN ($CenterCode3) AND Admission_Course='" . $_course . "' and Admission_Batch='" . $_batch . "' Group By Admission_ITGK_Code,
							 Admission_Course,Admission_Batch";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			//return $_Response;
			
		 //return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response1;
    }   
		
		public function GetAllCourse() {
			global $_ObjConnection;
			$_ObjConnection->Connect();
			 try {
					$_SelectQuery = "Select * From tbl_course_master";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
				} catch (Exception $_ex) {
					$_Response[0] = $_ex->getLine() . $_ex->getTrace();
					$_Response[1] = Message::Error;           
				}			
			return $_Response;
		}
		
		public function GetBatchName($_coursename) {
			global $_ObjConnection;
			$_ObjConnection->Connect();
			 try {
					$_coursename = mysqli_real_escape_string($_ObjConnection->Connect(),$_coursename);
					
					$_SelectQuery = "Select * From tbl_batch_master where Course_Code = '" . $_coursename . "' AND CURDATE() >= `Batch_StartDate` AND CURDATE() <= `Batch_EndDate`";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           //print_r($_Response);
				} catch (Exception $_ex) {
					$_Response[0] = $_ex->getLine() . $_ex->getTrace();
					$_Response[1] = Message::Error;           
					}			
			return $_Response;
		}
		
		 public function GetAllLearner($code,$course,$batch,$mode) {
			global $_ObjConnection;
			$_ObjConnection->Connect();
        try {
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
						$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
						$code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
						$mode = mysqli_real_escape_string($_ObjConnection->Connect(),$mode);
						
            $_SelectQuery = "SELECT * FROM tbl_admission where Admission_ITGK_Code = '" . $code . "' AND Admission_Course = '". $course . "' AND Admission_Batch = '" . $batch . "' AND Admission_PhotoProcessing_Status = '" . $mode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }  

 public function Update_System($_UserPermissionArray) {
         //print_r($_UserPermissionArray);		 
        global $_ObjConnection;
        $_ObjConnection->Connect();
         try {
			 $_ITGK_Code =   $_SESSION['User_LoginId'];
			$_User_Code =   $_SESSION['User_Code'];
            $_InsertQuery="";
            $_CountLength=count($_UserPermissionArray);
            for($i=0;$i<$_CountLength;$i++)
            {
             $_UpdateQuery = "Update tbl_admission set Admission_PhotoProcessing_Status ='" . $_UserPermissionArray[$i]['Values'] . "', rejection_reason = '" . $_UserPermissionArray[$i]['Reason'] . "', rejection_type = '" . $_UserPermissionArray[$i]['Type'] . "' WHERE Admission_Code = '" . $_UserPermissionArray[$i]['Function'] . "'"; 
             $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			 
			 $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                    . "Admission_Log_ITGK_Code,"                 
                    . "Admission_Log_ProcessingStatus,Admission_Log_Reason,Admission_Log_Type,Admission_Log_User_Code) "
                    . "'" . $_UserPermissionArray[$i]['Function'] . "' as Admission_Log_Code,"
                    . "'1234567890' as Admission_Log_LearnerCode,'" .$_ITGK_Code  . "' as Admission_Log_ITGK_Code,"                                     
                    . "'" . $_UserPermissionArray[$i]['Values'] . "' as Admission_Log_ProcessingStatus, '" . $_UserPermissionArray[$i]['Reason'] . "' as Admission_Log_Reason,"                   
                    . "'" . $_UserPermissionArray[$i]['Type'] . "' as Admission_Log_Type, '" . $_User_Code . "' as Admission_Log_User_Code"
                    . " From tbl_admission_log";
		  $_Response1=$_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		  
            }
                      
        }
        
        catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }  
	
		 public function GetAllRejected($batch,$course)
			{
				global $_ObjConnection;
				$_ObjConnection->Connect();
			try {			
					//$_SelectedRole = $_Role;
					$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
						$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
						
					$_LoginRole = $_SESSION['User_UserRoll'];

					if ($_LoginRole == '1') {

						$_LoginUserType = $_LoginRole;
					} elseif ($_LoginRole == '5') {

						$_LoginUserType = "PSAUserCode";
					} 
					 elseif ($_LoginRole == '7') {

						$_LoginUserType = "CenterUserCode";
					}else {
						echo "Not Valid";
						return;
					}
					$_SESSION['UserType'] = $_LoginUserType;


					if ($_LoginRole == '1') {

						$_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code";
					}
					else if ($_LoginRole == '7') {

						$_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
					} else {

						$_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
					}
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

					$_i = 0;
					$CenterCode2 = '';
					while ($_Row = mysqli_fetch_array($_Response[2])) {
						$CenterCode2.=$_Row['Centercode'] . ",";
						$_i = $_i + 1;
					}
					$CenterCode3 = rtrim($CenterCode2, ",");
					
					
					$_SelectQuery = "SELECT * FROM tbl_admission where Admission_ITGK_Code IN ($CenterCode3) AND Admission_Course = '". $course . "' AND Admission_Batch = '" . $batch . "' AND Admission_PhotoProcessing_Status = 'Rejected'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    } 
}
?>