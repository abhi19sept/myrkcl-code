<?php


/**
 * Description of clsSupportStatusUpdate
 * @author Hariom Awasthi
 */

    require 'DAL/classconnectionNEW.php';
    require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsSupportStatusUpdate {
	
	public function updateITGKStausForSupport($itgkCode, $columnForUpdate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkCode);
				$columnForUpdate = mysqli_real_escape_string($_ObjConnection->Connect(),$columnForUpdate);
				
            if($columnForUpdate == 'IsNewRecord'){

                /* Update Status For MyRKCL*/

                /* Get User_Code*/
                $_SelectQuery = "SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '".$itgkCode."'";
                $_Response_User_Master = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_row = mysqli_fetch_row($_Response_User_Master[2]);
                $_User_Code = $_row['0'];

                /* User Master Status Update*/
                $update_status_user_master = "UPDATE tbl_user_master SET ".$columnForUpdate." = 'Y' WHERE User_LoginId = '".$itgkCode."'";
                $update_status_user_master_Response = $_ObjConnection->ExecuteQuery($update_status_user_master, Message::UpdateStatement);
                /* Update User Profile Status */
                $update_status_UserProfile = "UPDATE tbl_userprofile SET ".$columnForUpdate." = 'Y' WHERE UserProfile_User = '".$_User_Code."'";
                $_Response = $_ObjConnection->ExecuteQuery($update_status_UserProfile, Message::UpdateStatement);

                /* END */

            }
            elseif($columnForUpdate == 'IsNewCRecord'){

                /* User Master Status Update*/
                $update_status_user_master = "UPDATE tbl_user_master SET ".$columnForUpdate." = 'Y' WHERE User_LoginId = '".$itgkCode."'";
                $_update_user_master_Response = $_ObjConnection->ExecuteQuery($update_status_user_master, Message::UpdateStatement);

                /* Getting User Code For update Organisation detail*/
                $_SelectQuery = "SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '".$itgkCode."'";
                $_Response_User_Master = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_row = mysqli_fetch_row($_Response_User_Master[2]);
                $_User_Code = $_row['0'];

                /* Organisation Details Status Update*/
                $update_status_organisation_detail = "UPDATE tbl_organization_detail SET ".$columnForUpdate." = 'Y' WHERE Organization_User = '".$_User_Code."'";
                $_Response = $_ObjConnection->ExecuteQuery($update_status_organisation_detail, Message::UpdateStatement);

            }else{

                /* User Master Status Update*/
                $update_status_user_master = "UPDATE tbl_user_master SET ".$columnForUpdate." = 'Y' WHERE User_LoginId = '".$itgkCode."'";
                $_update_user_master_Response = $_ObjConnection->ExecuteQuery($update_status_user_master, Message::UpdateStatement);

                /* Getting User Code For update Organisation detail*/
                $_SelectQuery = "SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '".$itgkCode."'";
                $_Response_User_Master = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_row = mysqli_fetch_row($_Response_User_Master[2]);
                $_User_Code = $_row['0'];

                /* Update User Profile Status */
                $update_status_UserProfile = "UPDATE tbl_userprofile SET ".$columnForUpdate." = 'Y' WHERE UserProfile_User = '".$_User_Code."'";
                $update_status_UserProfile_Response = $_ObjConnection->ExecuteQuery($update_status_UserProfile, Message::UpdateStatement);

                /* Organisation Details Status Update*/
                $update_status_organisation_detail = "UPDATE tbl_organization_detail SET ".$columnForUpdate." = 'Y' WHERE Organization_User = '".$_User_Code."'";
                $_Response = $_ObjConnection->ExecuteQuery($update_status_organisation_detail, Message::UpdateStatement);

            }

        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

}
