<?php

/**
 * Description of clsEventWiseResultData
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsEventWiseResultData {

    //put your code here

    

    public function GetResultData($_Event_Id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_Event_Id = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event_Id);
				
				$select = "select concat('',Affilate_FreshBaches,Affilate_ReexamBatches) as batchcodes from
								tbl_exammaster where Affilate_Event='".$_Event_Id."'";
				$_Responses = $_ObjConnection->ExecuteQuery($select, Message::SelectStatement);
					if($_Responses[0] == "Success"){				
							$_Rows=mysqli_fetch_array($_Responses[2]);
								$batchcodes=$_Rows['batchcodes'];
					$_SelectQuery = "Select scol_no, exam_event_name, exam_date, rollno, upper(name) as lname, upper(fname) as fname, dob,
								study_cen, net_marks, inte_marks, tot_marks, percentage, result, b.Admission_Mobile
								 From tbl_result as a inner join tbl_admission as b on a.scol_no=b.Admission_LearnerCode
								 WHERE exameventnameID = '".$_Event_Id."' and  Admission_Batch in ($batchcodes)";		
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			
					}
					else{
						$_SelectQuery = "Select scol_no, exam_event_name, exam_date, rollno, upper(name) as lname, upper(fname) as
								fname, dob, study_cen, net_marks, inte_marks, tot_marks, percentage, result, 'NA' as Admission_Mobile
								 From tbl_result WHERE exameventnameID = '".$_Event_Id."'";		
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			
					}
				 
			}  else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
