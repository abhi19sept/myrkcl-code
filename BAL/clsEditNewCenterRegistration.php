<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsEditNewCenterRegistration
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsEditNewCenterRegistration {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code WHERE a.Org_Course_Allocation_Status = 'Pending' "
                    . " AND a.Org_RspLoginId='" . $_SESSION['User_LoginId'] . "' AND a.Org_NCR_App_Date is not null";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function GetOrgDatabyCode($_OrgCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
				
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code WHERE a.Organization_Code = '" . $_OrgCode . "' "
                    . " AND a.Org_Course_Allocation_Status = 'Pending'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}
