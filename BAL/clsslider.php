<?php
/**
 * Description of clsAreaMaster
 *
 * @author Abhi
 */
require('DAL/classconnectionNEW.php');
$_ObjConnection = new _Connection();
$_Response = array();
class clsSliderForm{
    public function ImageSliderNew($_imageTittle,$_sliderStatus,$_newfilename,$link)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "insert into tbl_sliderimage (imagetittle,status,photo,link)values('".$_imageTittle."','".$_sliderStatus."','".$_newfilename."','".$link."')";
            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function ShowSlideImages($_Status)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_sliderimage";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteSliderImage($_deleteid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_deleteid = mysqli_real_escape_string($_ObjConnection->Connect(),$_deleteid);
				
            $_DeleteQuery = "Delete From tbl_sliderimage Where id='" . $_deleteid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function ShowSlideImagesForEdit($_editid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_editid = mysqli_real_escape_string($_ObjConnection->Connect(),$_editid);
				
            $_SelectQuery = "select * from tbl_sliderimage Where id='" . $_editid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function SliderImageUpdate($_sliderid,$_imageTittleupdate,$_sliderStatusupdate,$_newfilename,$link)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_sliderid = mysqli_real_escape_string($_ObjConnection->Connect(),$_sliderid);
				
            $_UpdateQuery = "Update tbl_sliderimage set imagetittle = '".$_imageTittleupdate."', status = '".$_sliderStatusupdate."', photo = '".$_newfilename."' , link = '".$link."' where id = '".$_sliderid."'";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}  
?>