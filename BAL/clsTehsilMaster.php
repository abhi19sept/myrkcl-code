<?php

/*
 * @author Abhi

 */

/**
 * Description of clsTehsilMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsTehsilMaster {
    //put your code here
    public function GetAll($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$district = mysqli_real_escape_string($_ObjConnection->Connect(),$district);
				
            if(!$district)
            {

            $_SelectQuery = "Select Tehsil_Code, Tehsil_Name, District_Name, Region_Name, State_Name, Country_Name, Status_Name From tbl_Tehsil_master as a 
inner join tbl_district_master as b on a.Tehsil_District=b.District_Code 
inner join tbl_region_master as c on b.District_Region=c.Region_Code 
inner join tbl_state_master as d on c.Region_State=d.State_Code 
inner join tbl_country_master as f on d.State_Country=f.Country_Code 
inner join tbl_status_master as e on a.Tehsil_Status=e.Status_Code ORDER BY State_Name, Region_Name, District_Name, Tehsil_Name";
            }
            else {
                 $_SelectQuery = "Select Tehsil_Code,Tehsil_Name,District_Name,Region_Name,State_Name,"
                    . "Status_Name,Country_Name From tbl_Tehsil_master as a inner join tbl_district_master as b "
                    . "on a.Tehsil_District=b.District_Code inner join tbl_region_master as c "
                    . "on b.District_Region=c.Region_Code inner join tbl_state_master as d "
                    . "on c.Region_State=d.State_Code inner join tbl_status_master as e "
                    . "on a.Tehsil_Status=e.Status_Code inner join tbl_country_master as f "
                    . "on d.State_Country=f.Country_Code and a.Tehsil_District='" . $district . "' ORDER BY State_Name, Region_Name, District_Name, Tehsil_Name";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Tehsil_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Tehsil_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil_Code);
				
            $_SelectQuery = "Select Tehsil_Code,Tehsil_Name,Tehsil_District,Tehsil_Status"
                    . " From tbl_tehsil_master Where Tehsil_Code='" . $_Tehsil_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Tehsil_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Tehsil_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil_Code);
				
            $_DeleteQuery = "Delete From tbl_tehsil_master Where Tehsil_Code='" . $_Tehsil_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_TehsilName,$_District,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_TehsilName = mysqli_real_escape_string($_ObjConnection->Connect(),$_TehsilName);
				$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
				
            $_InsertQuery = "Insert Into tbl_tehsil_master(Tehsil_Code,Tehsil_Name,"
                    . "Tehsil_District,Tehsil_Status) "
                    . "Select Case When Max(Tehsil_Code) Is Null Then 1 Else Max(Tehsil_Code)+1 End as Tehsil_Code,"
                    . "'" . $_TehsilName . "' as Tehsil_Name,"
                    . "'" . $_District . "' as Tehsil_District,'" . $_Status . "' as Tehsil_Status"
                    . " From tbl_tehsil_master";
            $_DuplicateQuery = "Select * From tbl_tehsil_master Where Tehsil_Name='" . $_TehsilName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_TehsilName,$_District,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_TehsilName = mysqli_real_escape_string($_ObjConnection->Connect(),$_TehsilName);
				$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
				
            $_UpdateQuery = "Update tbl_tehsil_master set Tehsil_Name='" . $_TehsilName . "',"
                    . "Tehsil_District='" . $_District . "',"
                    . "Tehsil_Status='" . $_Status . "' Where Tehsil_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_tehsil_master Where Tehsil_Name='" . $_TehsilName . "' "
                    . "and Tehsil_Code <> '" . $_Code . "'";
           
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	 public function GetAllLegislativeAssembly($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$district = mysqli_real_escape_string($_ObjConnection->Connect(),$district);
				
                $_SelectQuery = "SELECT la.* FROM tbl_legislative_assembly_constituency_list la INNER JOIN tbl_district_master dm ON la.district = dm.District_Name WHERE dm.District_Code = '" . $district . "' ORDER BY la.name";
            
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
