<?php

/*
 *  author Viveks

 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsAddSystem {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select System_Code,System_Type,"
                    . "System_Processor,System_RAM,System_HDD,System_Status,Processor_Name From tbl_system_info as a inner join tbl_processor_master as b on a.System_Processor = b.Processor_Code where System_User='". $_SESSION['User_LoginId']."'" ;
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_System_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_System_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_System_Code);
            $_SelectQuery = "Select System_Code,System_Type,"
                    . "System_Processor,System_RAM,System_HDD From  tbl_system_info Where System_Code='" . $_System_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_System_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_System_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_System_Code);
            $_DeleteQuery = "Delete From tbl_system_info Where System_Code='" . $_System_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_Type,$_Processor,$_RAM,$_HDD) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Type = mysqli_real_escape_string($_ObjConnection->Connect(),$_Type);
            $_Processor = mysqli_real_escape_string($_ObjConnection->Connect(),$_Processor);
            $_RAM = mysqli_real_escape_string($_ObjConnection->Connect(),$_RAM);
            $_HDD = mysqli_real_escape_string($_ObjConnection->Connect(),$_HDD);
            
            $_ITGK_Code =   $_SESSION['User_LoginId'];
            
            $_InsertQuery = "Insert Into tbl_system_info(System_Code,System_User,System_Type,"
                    . "System_Processor,System_RAM,System_HDD) "
                    . "Select Case When Max(System_Code) Is Null Then 1 Else Max(System_Code)+1 End as System_Code,"
                    . "'" . $_ITGK_Code . "' as System_User,'" . $_Type . "' as System_Type,'" . $_Processor . "' as System_Processor,"
                    . "'" . $_RAM . "' as System_RAM,'" . $_HDD . "' as System_HDD"
                    . " From tbl_system_info";
            //print_r($_InsertQuery);
            
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            
         catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_Type,$_Processor,$_RAM,$_HDD) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            $_Type = mysqli_real_escape_string($_ObjConnection->Connect(),$_Type);
            $_Processor = mysqli_real_escape_string($_ObjConnection->Connect(),$_Processor);
            $_RAM = mysqli_real_escape_string($_ObjConnection->Connect(),$_RAM);
            $_HDD = mysqli_real_escape_string($_ObjConnection->Connect(),$_HDD);
            $_UpdateQuery = "Update tbl_system_info set System_Type='" . $_Type . "',"
                    . "System_Processor='" . $_Processor . "', System_RAM='" . $_RAM . "',"
                    . "System_HDD='" . $_HDD . "' Where System_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_bank_master Where Bank_Name='" . $_BankName . "' "
                    . "and Bank_Code <> '" . $_BankCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
