<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgUpdate
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOrgUpdate {
    //put your code here
    
    public function GetDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                
                $_SelectQuery = "Select a.Organization_Name, a.Organization_District, a.Organization_Tehsil, b.District_Name, c.Tehsil_Name "
                        . " FROM tbl_organization_detail "
                        . " AS a INNER JOIN tbl_district_master AS b ON a.Organization_District=b.District_Code "
                        . " INNER JOIN tbl_tehsil_master as c ON a.Organization_Tehsil=c.Tehsil_Code "
                        . " WHERE Organization_User = '" . $_SESSION['User_Code'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FillMunicipalType()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_municipality_type Where Municipality_Type_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function FillMunicipalName($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_District_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_District_Code);
				
            $_SelectQuery = "Select * From tbl_municipality_master Where Municipality_District='" . $_District_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function FILLWardno($_Municipal_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Municipal_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Municipal_Code);
				
            $_SelectQuery = "Select * From tbl_ward_master Where Ward_Municipality='" . $_Municipal_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function UpdateOrg($_Areatype, $_WardNo, $_village, $_GramPanch, $_Panchayat, $_MName, $_MType) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		$_User=$_SESSION['User_Code'];
        try {	
				if($_Areatype == 'Urban') {
					if($_MName == '0' || $_MType == '0' || $_WardNo == '0') {
						 echo " Please select all fields";
						 return;
					}
					else {
						$_SelectQuery = "Select * From tbl_organization_detail Where Organization_Status='1' AND
							Organization_User='" . $_User . "'";
						$_SelectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement); 
						if($_SelectResponse[0]==Message::NoRecordFound) {
							$_UpdateQuery = "Update tbl_organization_detail set Organization_AreaType='" . $_Areatype . "', Organization_WardNo='" . $_WardNo . "',Organization_Municipal='" . $_MName . "',
											Organization_Municipal_Type='" . $_MType . "', Organization_Gram='',Organization_Panchayat='',
											Organization_Village='', Organization_Status='1' Where Organization_User='" . $_User . "'";
						}
						else {
							echo " You have already submitted your details";
							return;
						}					
					}
				}
				else {
					if($_GramPanch =='00' || $_Panchayat =='0' || $_village =='0') {
						 echo " Please select all fields";
						 return;
					}
					else {
							$_SelectQuery = "Select * From tbl_organization_detail Where Organization_Status='1' AND Organization_User='" . $_User . "'";
							$_SelectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement); 
							if($_SelectResponse[0]==Message::NoRecordFound) {
								$_UpdateQuery = "Update tbl_organization_detail set Organization_AreaType='" . $_Areatype . "', Organization_Gram='" . $_GramPanch . "',Organization_Panchayat='" . $_Panchayat . "',
											Organization_Village='" . $_village . "', Organization_WardNo='',Organization_Municipal='',
											Organization_Municipal_Type='', Organization_Status='1' Where Organization_User='" . $_User . "'";
							}
							else {
								echo " You have already submitted your details";
								return;
							}	
					}
				}
           
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
}
