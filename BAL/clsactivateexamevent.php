<?php

/**
 * Description of clsgetpassword
 *
 * @author yogendra soni
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsactivateexamevent {

    //put your code here

    public function FillEvent() {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT Event_Id,Event_Name from tbl_events as a inner join tbl_exammaster as b on a.Event_Id=b.Affilate_Event   ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getdata() {

        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
			if($_SESSION['User_UserRoll'] == '1' )
			{
				 $_SelectQuery = "Select Event_Name,Status_Name From tbl_events as a inner join tbl_exammaster as b on a.Event_Id=b.Affilate_Event inner join tbl_status_master as c on b.Event_Status=c.Status_Code ";
			}
			
		$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	public function Add($_Event,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
           
            
               $_UpdateQuery = "Update tbl_exammaster set 
						Event_Status='".$_Status."'
						Where  Affilate_Event='".$_Event."'";
             $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            
         catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
}
