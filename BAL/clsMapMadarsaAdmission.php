<?php

/**
 * Description of clsMapMadarsaAdmission
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsMapMadarsaAdmission {

    //put your code here
    public function GetAllLearner($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
			$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
			
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_SelectQuery = "Select Admission_Code, Admission_LearnerCode, Admission_Name, Admission_Batch, Admission_Fname,"
                        . " Admission_DOB,Admission_ITGK_Code,Admission_Gender,Admission_Mobile FROM tbl_admission as a left join "
                        . "tbl_admission_madarsa_map as b on a.Admission_LearnerCode = b.Admission_Madarsa_Lcode "
                        . "WHERE Admission_Batch = '" . $batch . "' AND  Admission_Course = '" . $course . "'"
                        . "  order by Admission_ITGK_Code";
                
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "Select Admission_Code, Admission_LearnerCode, Admission_Name, Admission_Batch, Admission_Fname,"
                        . " Admission_DOB, Admission_Photo, Admission_Sign FROM tbl_admission as a left join "
                        . "tbl_admission_madarsa_map as b on a.Admission_LearnerCode = b.Admission_Madarsa_Lcode "
                        . "WHERE Admission_Batch = '" . $batch . "' AND  Admission_Course = '" . $course . "'  AND "
                        . " Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' order by Admission_Madarsa_Mcode";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLmadarsa() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            // $_SelectQuery = "Select * FROM tbl_madarsa_master WHERE Madarsa_Tehsil = '" . $_tehsilcode . "' AND Madarsa_Status = 1";
            $_SelectQuery = "Select * FROM tbl_madarsa_master WHERE Madarsa_Status = 1 order by Madarsa_Regd_code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Addtoadmissionmadarsamap($lcode, $mcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$mcode = mysqli_real_escape_string($_ObjConnection->Connect(),$mcode);
				
            $_InsertQuery = "Insert Into tbl_admission_madarsa_map(Admission_Madarsa_Lcode,Admission_Madarsa_Mcode) "
                    . "values ('" . $lcode . "','" . $mcode . "')";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function chkadmissionmadarsamap($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            // $_SelectQuery = "Select * FROM tbl_madarsa_master WHERE Madarsa_Tehsil = '" . $_tehsilcode . "' AND Madarsa_Status = 1";
            $_SelectQuery = "Select Madarsa_Regd_code,Madarsa_Name FROM tbl_madarsa_master as a inner join tbl_admission_madarsa_map as b on "
                    . "a.Madarsa_Code=b.Admission_Madarsa_Mcode WHERE Admission_Madarsa_Lcode = '" . $lcode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
