<?php

/**
 * Description of clsCenterActiveTerminate
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCenterActiveTerminate {

    //put your code here

    public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '11' || $_SESSION['User_UserRoll'] == '8') {
                    $_SelectQuery = "SELECT a.*, b.User_Active_Status,b.User_Active_Timestamp "
                            . " from vw_itgkname_distict_rsp as a inner join tbl_user_master as b on "
                            . "a.ITGKCODE=b.User_LoginId where a.ITGKCODE='" . $_CenterCode . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    echo "You are not Auhorized to use this functionality on MYRKCL.";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_ddlActiveStatus, $_ItgkCode) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            $_ddlActiveStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_ddlActiveStatus);
            $_ItgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ItgkCode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

                $_SelectQuery = "SELECT * from tbl_user_master where User_LoginId='" . $_ItgkCode . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                $_Row = mysqli_fetch_array($_Response1[2]);
                $_User_Code = $_Row['User_Code'];
                $_UserOldActiveStatus = $_Row['User_Active_Status'];


                date_default_timezone_set('Asia/Calcutta');
                $_Date = date("Y-m-d h:i:s");

                $_InsertQuery = "Insert into tbl_user_master_active_log (User_Active_Log_UserCode, 
	User_Active_Log_ITGKCode, User_Active_Log_Status,User_Active_Log_Timestamp) 
                values ('" . $_User_Code . "','" . $_ItgkCode . "','" . $_UserOldActiveStatus . "','" . $_Date . "')";
                //echo $_InsertQuery;
                $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

                $_UpdateQuery = "Update tbl_user_master set User_Active_Status = '" . $_ddlActiveStatus . "' "
                        . " WHERE User_LoginId = '" . $_ItgkCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }

}
