<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsexamdatamaster {
	
	public function Add($_sel,$_event)
	{	global $_ObjConnection;
		$_Response1 = array();
        $_ObjConnection->Connect();
        try {			
				
			    $_SelectQuery = "Select Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB,Admission_Course,Admission_Batch,Admission_ITGK_Code,Admission_Name,Organization_District,Organization_Tehsil From tbl_admission as a inner join tbl_user_master as b on a.Admission_ITGK_Code=b.User_LoginId inner join tbl_organization_detail as c on b.User_Code=c.Organization_User where Admission_Batch IN ($_sel) AND Admission_Payment_Status=1 AND Admission_PhotoUpload_Status=1 AND Admission_SignUpload_Status=1 AND Admission_PhotoProcessing_Status='Approved'";            
				$_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				
			//$num_rows = mysqli_num_rows($_Response1[2]);              
        }
		catch (Exception $_e) 
		{
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
  
		
        return $_Response1;
    }
	
	
	
	
	public function Add1($_sel,$_event)
	{	global $_ObjConnection;
		$_Response1 = array();
        $_ObjConnection->Connect();
        try {		
				
				$_event = mysqli_real_escape_string($_ObjConnection->Connect(),$_event);
				
			    $_SelectQuery = "Select Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB,Admission_Course,Admission_Batch,Admission_ITGK_Code,Admission_Name,Organization_District,Organization_Tehsil From tbl_admission as a inner join tbl_user_master as b on a.Admission_ITGK_Code=b.User_LoginId inner join tbl_organization_detail as c on b.User_Code=c.Organization_User where Admission_Batch IN ($_sel) AND Admission_Payment_Status=1 AND Admission_PhotoUpload_Status=1 AND Admission_SignUpload_Status=1 AND Admission_PhotoProcessing_Status='Approved'";            
				$_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				
			//$num_rows = mysqli_num_rows($_Response1[2]);
			
			
				 $_SelectQuery2 = "Select Affilate_Event from tbl_exammaster where Affilate_Event='".$_event."'";
          
            $_Response2=$_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
			$_data = mysqli_fetch_array($_Response2[2]);
			//print_r($_data);
            	 while ($_Row = mysqli_fetch_array($_Response1[2])) {
					 
					 $_InsertQuery = "Insert Into examdata(examid, learnercode,learnername,fathername,dob,coursename,batchname,itgkcode,itgkname,itgkdistrict,itgktehsil,remark,status) 
												 VALUES ('" .$_data['Affilate_Event']. "',
												 '" .$_Row['Admission_LearnerCode']. "',
												 '" .$_Row['Admission_Name']. "',
												  '" .$_Row['Admission_Fname']. "',
												  '" .$_Row['Admission_DOB']. "',
												  '" .$_Row['Admission_Course']. "',
												  '" .$_Row['Admission_Batch']. "',
												  '" .$_Row['Admission_ITGK_Code']. "',
												   '" .$_Row['Admission_Name']. "',
												   '" .$_Row['Organization_District']. "',
												    '" .$_Row['Organization_Tehsil']. "',
													'Active',
													'1'
												)";
												
        $_DuplicateQuery = "Select * From examdata Where learnercode='" .$_Row['Admission_LearnerCode']. "' and batchname='".$_Row['Admission_Batch']."'";
        $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
         //print_r($_Response);
            
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				
				
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }								  
											  
		
        } 
		 
              
        }
		catch (Exception $_e) 
		{
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
  
		
        return $_Response;
    }
	public function Getexception() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT  Event_Id,Event_Name ,Affilate_Event From tbl_exammaster as a inner join tbl_events as b on a.Affilate_Event=b.Event_Id ";
			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	public function Getexambatch()
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_SelectQuery = "Select DISTINCT Affilate_FreshBaches FROM tbl_exammaster";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			   
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 return $_Response;
		}
		
		
		public function GeteselectedEvents($_Event)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
					$_Event = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event);
					
				$_SelectQuery = "Select DISTINCT Affilate_FreshBaches  FROM tbl_exammaster where Affilate_Event='".$_Event."'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			   
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 return $_Response;
		}
		
		
		

		public function Getexambatchname($_Batch_Code)
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
         echo  $_SelectQuery = "Select DISTINCT Event_Id,Event_Name From tbl_exammaster as a INNER JOIN tbl_events as b ON a. Affilate_Event = b.Event_Id inner join tbl_admission as c where a.Affilate_FreshBaches IN (" . $_Batch_Code . ") and c.Admission_Batch IN (" . $_Batch_Code . ") ";            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }		

	
	
	
	
	
}
