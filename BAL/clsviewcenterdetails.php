<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBankAccount
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsviewcenterdetails {
    //put your code here
    
	
	
	
	
	
    public function GetAll($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
            $_SelectQuery = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_state_master as d on a.Organization_State=d.State_Code inner join tbl_district_master as f on a.Organization_District=f.District_Code inner join tbl_tehsil_master as g on a.Organization_Tehsil=g.Tehsil_Code where b.User_LoginId = '".$_activate."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	public function Getprofile($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
            $_SelectQuery = "Select * from tbl_userprofile as a inner join tbl_user_master as b on a.UserProfile_User=b.User_Code
					where b.User_LoginId ='".$_activate."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	public function Gethr($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
            $_SelectQuery = "Select * from tbl_staff_detail where Staff_User = '".$_activate."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	public function GetItperiferals($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
            $_SelectQuery = "Select IT_Peripherals_Code,IT_UserCode,IT_Peripherals_Device_Type,IT_Peripherals_Availablity,"
                    ."IT_Peripherals_Make,IT_Peripherals_Model,IT_Peripherals_Quantity,IT_Peripherals_Detail,"
                    . "Device_Name From tbl_it_peripherals as a inner join tbl_device_master as b "
                    . "on a.IT_Peripherals_Device_Type=b.Device_Code where IT_UserCode = '".$_activate."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	public function Getpremise($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
            $_SelectQuery = "Select * from tbl_premises_details where Premises_User='".$_activate."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	
	
	
	
	
	public function Getcomputing($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
            $_SelectQuery = "Select System_Code,System_Type,"
                    . "System_Processor,System_RAM,System_HDD From tbl_system_info where System_User='". $_activate."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	
	
	
	
	
	public function Getintake($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
             $_SelectQuery = "Select *  from tbl_intake_master where Intake_Center='". $_activate."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
}
