<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrFinalApproval
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrFinalApproval {
    //put your code here
    
    public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code "
                    . " WHERE e.User_LoginId = '" . $_CenterCode . "' AND e.User_UserRoll ='15' "
                    . " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
//            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,
                            d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b
                            ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c 
                            ON a.Organization_District=c.District_Code
                            INNER JOIN tbl_tehsil_master as d
                            ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e 
                            ON a.Org_Ack=e.User_Ack inner join tbl_ncr_feedback as f on e.User_LoginId=f.NCRFeedback_AOCode
                            inner join tbl_ncr_visit as g on e.User_LoginId=g.NCRVisit_Center_Code
                            inner join tbl_ncrvisit_photo as h on e.User_LoginId=h.NCRVisit_CenterCode 
                            where a.Org_Visit = 'Yes' AND e.User_PaperLess = 'Yes' AND g.NCRVisit_LoginId='" . $_SESSION['User_LoginId'] . "' AND e.User_Rsp = '" . $_SESSION['User_Code'] . "'";
//            } else {
//            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
//                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
//                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
//                    . " ON a.Organization_District=c.District_Code"
//                    . " INNER JOIN tbl_tehsil_master as d"
//                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
//                    . " ON a.Organization_User=e.User_Code "
//                    . " INNER JOIN tbl_intake_master as f "
//                    . " ON e.User_LoginId=f.Intake_Center INNER JOIN tbl_staff_detail as g "
//                    . " ON e.User_LoginId=g.Staff_User INNER JOIN tbl_bank_account as h "
//                    . " ON e.User_LoginId=h.Bank_User_Code INNER JOIN tbl_it_peripherals as i "
//                    . " ON e.User_LoginId=i.IT_UserCode INNER JOIN tbl_premises_details as j "
//                    . " ON e.User_LoginId=j.Premises_User INNER JOIN tbl_userprofile as k "
//                    . " ON e.User_Code=k.UserProfile_User"
//                    . " WHERE e.User_Type = 'New' AND e.User_Rsp='" .$_SESSION['User_Code'] . "'";
//            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Upgrade($_CenterCode, $_Ack) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_Ack = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ack);
				
            date_default_timezone_set('Asia/Kolkata');
            $_Date = date("Y-m-d h:i:s");
            $_UpdateQuery = "Update tbl_user_master set User_UserRoll='7', User_SPFinalApprovalDate='" . $_Date . "'"
                    . "Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
            $_UpdateQuery1 = "Update tbl_org_master set Org_Final_Approval_Status='Approved', Org_NCR_Final_Approval_Date='" . $_Date . "'"
                    . "Where Org_Ack='" . $_Ack . "'";
            
            $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            
//            $_Year = date("Y");
//            $_PreviousYear = ($_Year - 1);
//            $_DateOnly = date("Y-m-d");
//            $_InsertQuery = "Insert Into tbl_eoi_centerlist"
//                    . "(EOI_Code,EOI_ECL,EOI_Year,EOI_DateTime,EOI_Filename) values "                   
//                    . "('1','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
//                    . "('2','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
//                    . "('9','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
//                    . "('6','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR')";
//            $_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response1;
    }
    
    public function SHOWBANKACCOUNTDATA($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
                $_SelectQuery = "Select * from tbl_bank_account where Bank_User_Code ='" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function SHOWOWNERDATA($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
                $_SelectQuery = "Select max(User_Code) as User_Code FROM tbl_user_master WHERE User_LoginId = '" . $_CenterCode . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Row = mysqli_fetch_array($_Response1[2]);                
                $_CenterUserCode = $_Row['User_Code']; 
                
             $_SelectQuery1 = "Select * From tbl_userprofile where UserProfile_User ='". $_CenterUserCode ."'" ;
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            //print_r($_Response);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }  
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function SHOWHRDETAIL($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
			
            $_SelectQuery = "Select * from tbl_staff_detail where Staff_User = '" . $_CenterCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function SHOWSIDATA($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
			
             $_SelectQuery = "Select System_Code,System_Type,"
                    . "System_Processor,System_RAM,System_HDD,System_Status,Processor_Name From tbl_system_info as a inner join tbl_processor_master as b on a.System_Processor = b.Processor_Code where System_User='". $_CenterCode ."'" ;
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     public function SHOWPREMISESDATA($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "Select * from tbl_premises_details where Premises_User = '" . $_CenterCode . "'";              
             
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function SHOWITDATA($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
							
                $_SelectQuery = "Select IT_Peripherals_Code,IT_UserCode,IT_Peripherals_Device_Type,IT_Peripherals_Availablity,"
                ."IT_Peripherals_Make,IT_Peripherals_Model,IT_Peripherals_Quantity,IT_Peripherals_Detail,"
                . "Device_Name From tbl_it_peripherals as a inner join tbl_device_master as b "
                . "on a.IT_Peripherals_Device_Type=b.Device_Code where IT_UserCode = '" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
      public function SHOWSPVISITDATA($_CenterCode,$_VisitorCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                     $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					 $_VisitorCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitorCode);
					 
                $_SelectQuery = "select a.* from tbl_ncr_feedback as a inner join tbl_user_master as b 
            on a.NCRFeedback_User=b.User_LoginId where b.User_UserRoll='" . $_VisitorCode . "' and
			a.NCRFeedback_AOCode= '" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
      public function SHOWFACULTYRESULT($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                     $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					 
                $_SelectQuery = "SELECT b.Staff_Name,a.* from tbl_facultyexamresultreport as a inner join tbl_staff_detail as b on
                a.ResExam_learner = concat (b.Staff_User,'_',b.Staff_Code) where Staff_User= '" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
