<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGovLearnerStatusReport {
    //put your code here
    public function GetAll($_EID) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_EID = mysqli_real_escape_string($_ObjConnection->Connect(),$_EID);
			if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == '11'){
				$_SelectQuery = "Select a.*, b.lotname, c.cstatus FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid
									inner join tbl_capcategory as c on a.trnpending=c.capprovalid where empid='" . $_EID . "'";
			}
			else if($_SESSION['User_UserRoll'] == 7) {
				$_SelectQuery = "Select a.*, b.lotname, c.cstatus FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid
									inner join tbl_capcategory as c on a.trnpending=c.capprovalid where
									Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND empid='" . $_EID . "'";
			}
			else if($_SESSION['User_UserRoll'] == 19) {
				$_SelectQuery = "Select a.*, b.lotname, c.cstatus FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid
									inner join tbl_capcategory as c on a.trnpending=c.capprovalid where
									learnercode = '".$_SESSION['User_LearnerCode']."' AND empid='" . $_EID . "'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
}
