<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsDisplaySms
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
class clsDisplaySms {

    //put your code here
    public function ShowCat() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
               
                    $_SelectQuery = "select distinct SMS_Log_SmsTpye from tbl_sms_log order by SMS_Log_SmsTpye";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function SHOWrpt($sdate, $edate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$sdate = mysqli_real_escape_string($_ObjConnection->Connect(),$sdate);
				$edate = mysqli_real_escape_string($_ObjConnection->Connect(),$edate);
				
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {

                    $_SelectQuery = "select b.UserRoll_Name ,a.SMS_Log_UserLoginId,SMS_Log_Mobile,SMS_Log_SmsTpye,
SMS_Log_SmsText as msg,a.SMS_Log_SentDate
 from tbl_sms_log as a inner join tbl_userroll_master as b on 
a.SMS_Log_UserRole=b.UserRoll_Code where   SMS_Log_SentDate
 between '$sdate' and '$edate'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    $_SelectQuery = "Select * From tbl_sms_log where SMS_Log_SmsSentStaus = '1' and "
                            . "SMS_Log_UserLoginId='" . $_SESSION['User_LoginId'] . "' and SMS_Log_SmsTpye='".$ddlCategory."' order by SMS_Log_SentDate DESC";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

        public function SHOWrpt1($sdate, $edate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$sdate = mysqli_real_escape_string($_ObjConnection->Connect(),$sdate);
				$edate = mysqli_real_escape_string($_ObjConnection->Connect(),$edate);
				
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {

                    $_SelectQuery = "select SMS_Log_SmsTpye,count(SMS_Log_SmsTpye) as cnt,SMS_Log_SmsText as msg
,SMS_Log_SentDate, 
 (CASE  WHEN SMS_Log_UserRole = '19' THEN 'Learner'
        WHEN SMS_Log_UserRole = '7' THEN 'ITGK' END) as UserType, SMS_Log_UserRole
 from tbl_sms_log as a
 inner join tbl_userroll_master as b on 
a.SMS_Log_UserRole=b.UserRoll_Code where SMS_Log_SentDate
 between '$sdate' and '$edate' group by SMS_Log_SmsTpye,SMS_Log_SentDate";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    $_SelectQuery = "Select * From tbl_sms_log where SMS_Log_SmsSentStaus = '1' and "
                            . "SMS_Log_UserLoginId='" . $_SESSION['User_LoginId'] . "' and SMS_Log_SmsTpye='".$ddlCategory."' order by SMS_Log_SentDate DESC";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetData($ddlCategory) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$ddlCategory = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlCategory);
				
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {

                    $_SelectQuery = "Select * From tbl_sms_log where SMS_Log_SmsSentStaus = '1' and SMS_Log_SmsTpye='".$ddlCategory."' order by SMS_Log_SentDate DESC";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    $_SelectQuery = "Select * From tbl_sms_log where SMS_Log_SmsSentStaus = '1' and "
                            . "SMS_Log_UserLoginId='" . $_SESSION['User_LoginId'] . "' and SMS_Log_SmsTpye='".$ddlCategory."' order by SMS_Log_SentDate DESC";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function DwldSmsLogList($_smstype,$_senddate,$_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$_smstype = mysqli_real_escape_string($_ObjConnection->Connect(),$_smstype);
				$_senddate = mysqli_real_escape_string($_ObjConnection->Connect(),$_senddate);
				$_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
				
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {

                    $_SelectQuery = "select b.UserRoll_Name ,a.SMS_Log_UserLoginId,SMS_Log_Mobile,SMS_Log_SmsTpye,
SMS_Log_SmsText as msg,a.SMS_Log_SentDate 
 from tbl_sms_log as a inner join tbl_userroll_master as b on 
a.SMS_Log_UserRole=b.UserRoll_Code where SMS_Log_SmsTpye='".$_smstype."' and SMS_Log_SentDate='".$_senddate."' and SMS_Log_UserRole=$_rolecode";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    $_SelectQuery = "Select * From tbl_sms_log where SMS_Log_SmsSentStaus = '1' and "
                            . "SMS_Log_UserLoginId='" . $_SESSION['User_LoginId'] . "' and SMS_Log_SmsTpye='".$ddlCategory."' order by SMS_Log_SentDate DESC";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
