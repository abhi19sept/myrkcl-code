<?php
//require_once 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsforgotpassword {

   public function ResendOTP($_center)
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_SelectQuery = "SELECT * FROM `tbl_user_master` WHERE `User_LoginId` = '" . $_center . "' LIMIT 1";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if (mysqli_num_rows($_Response[2])) {
                    $_row = mysqli_fetch_array($_Response[2]);
                    $_Response = $this->ResendOTPNEW($_row['User_LoginId']);
                }
         } 
		 catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
   public function Forgot($_center)
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_SelectQuery = "SELECT * FROM `tbl_user_master` WHERE `User_LoginId` = '" . $_center . "' LIMIT 1";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if (mysqli_num_rows($_Response[2])) {
                    $_row = mysqli_fetch_array($_Response[2]);
                    $_Response = $this->SendOTPNEW($_row['User_LoginId']);
                }
         } 
		 catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }


public function GenerateOTP($length = 6, $chars = '1234567890') {
    $chars_length = (strlen($chars) - 1);
    $string = $chars{rand(0, $chars_length)};
    for ($i = 1; $i < $length; $i = strlen($string)) {
        $r = $chars{rand(0, $chars_length)};
        if ($r != $string{$i - 1})
            $string .= $r;
    }
    return $string;
}


    public function SendOTP($userId = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_OTP = $this->GenerateOTP();

        try {
            $_UserM = (!empty($userId)) ? $userId : $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_LoginId='" . $_UserM . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_Mobile = $_Row1['User_MobileNo'];
            $_Email = $_Row1['User_EmailId'];


            $_InsertQuery = "Insert Into tbl_ao_register(AO_Code,AO_Email,AO_Mobile,AO_OTP)"
                    . "Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,"
                    . "'" . $_Email . "' as AO_Email,'" . $_Mobile . "' as AO_Mobile,'"
                    . "" . $_OTP . " as AO_OTP '"
                    . " From tbl_ao_register";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_ao_register Where AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_Status = 0";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function VerifyOTP($oid, $otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Code='" . $oid . "' AND AO_OTP = '" . $otp . "' AND AO_Status = '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if ($_Response[0] != Message::NoRecordFound) {
                $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Code='" . $oid . "'";
                $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function updateUserPassword($pass, $User_LoginId, $oid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_SelectQuery = "Select * FROM tbl_ao_register ot INNER JOIN tbl_user_master um ON um.User_EmailId = ot.AO_Email WHERE ot.AO_Code='" . $oid . "' AND um.User_LoginId = '" . $User_LoginId . "' AND AO_Status = '1'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            $_UpdateQuery = "UPDATE tbl_user_master SET User_Password = '" . $pass . "', encrypted = 0 WHERE User_LoginId = '" . $User_LoginId . "'";
            return $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } else {
            return false;
        }
    }

    public function getVeriFyOTPForm($id, $_Center, $_Mobile) {
        return '
        <div class="locksmall"><img src="images/lock_icon_small.png"></div>
                            <div class="panelforgot">Forgot / Reset Password</div>
                         
        <div class="validate"><b>Validate OTP: (One time password)</b></div>
            <div class="goto"><a href="forgot_password.php">Go Back</a></div>
            <div style="clear: both;"></div>
            <hr>
        <div style="float:left; margin-left:25px;"><span style="color: green;">Dear MYRKCL a one time password has been sent to your registered Mobile No. '.$_Mobile.'.</span><p></p></div>
            <div id="response" style="padding-left: 23px;"></div>  
            <div style="float:left; margin-left:25px;"><span>Please enter OTP to Verify your User Name.</span><p></p></div>
        <div class="col-lg-12 col-md-12"><input maxlength="8" type="text" id="txtOTP" name="txtOTP" class="inputsearch" onkeypress="javascript:return allownumbers(event);"/>
        <input type="hidden" value="' . $id . '" name="oid" id="oid">
        <input type="hidden" value="' . $_Center . '" name="ccode" id="ccode"></div>';
    }
    
    

    public function getNewPasswordForm($id, $ccode) {
        return '</div></div><div class="container"><div class="form-group"><label for="email">New Password: </label><input type="password" id="newPass" name="newPass" class="form-control"/></div></div>
            <div class="container"><div class="form-group"> <label for="email">Confirm Password: </label><input type="password" id="CnPass" name="CnPass" class="form-control"/> </div><input type="hidden" value="' . $id . '" name="oid" id="oid"><input type="hidden" value="' . $ccode . '" name="ccode" id="ccode"></div>';
    }
    
    
    /* Added By : Sunil Kuamr Baindara 26-10-2018*/
    
    public function updateUserPasswordNew($pass, $User_LoginId, $oid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        
         function encryptPasswordFinal($password, $hashkey) {
                    return md5(sha1($password . $hashkey));
            } 
        $_SelectQuery = "Select * FROM tbl_ao_register ot INNER JOIN tbl_user_master um ON um.User_EmailId = ot.AO_Email WHERE ot.AO_Code='" . $oid . "' AND um.User_LoginId = '" . $User_LoginId . "' AND AO_Status = '1'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2]) > 0) {
            
            $_SelectQuery = "select a.*,b.* from tbl_into_s a
            LEFT JOIN tbl_phrase_ke B ON a.Phrase_Ke = b.Phrase_ID where a.User_LoginId = '" . $User_LoginId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //echo "<pre>"; print_r($_Response);die;
            if($_Response[0]=='Success'){                
                $_row = mysqli_fetch_array($_Response[2]);            
                $Phrase_Ke=$_row['Phrase_Ke'];            
                $Open_Sesame = encryptPasswordFinal($pass, $Phrase_Ke);
                $Open_Sesame_En = password_hash($Open_Sesame, PASSWORD_BCRYPT); 
                
                /* Checking the password should not match with last 3 password*/
                $_SelectQuery2 = "Select * from tbl_into_s_log where Open_Sesame='".$Open_Sesame."' and User_LoginId = '" . $User_LoginId . "' order by In_ID desc limit 3";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                $_row2 = mysqli_num_rows($_Response2[2]); 
                /* Checking the password should not match with last 3 password*/
                if($_row2 > 0){
                    $responceNew[0]='MORE3';
                    return $responceNew;
                }
                else{
                    $_SelectQuery1 = "Select * FROM tbl_phrase_ke WHERE Phrase_Status = 1";
                    $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                    $_row1 = mysqli_fetch_array($_Response1[2]);            
                    $Phrase_ID=$_row1['Phrase_ID'];            
                    $Phrase_Ke=$_row1['Phrase_Ke'];
                
                    $_UpdateQuery = "UPDATE tbl_into_s SET Open_Sesame = '" . $Open_Sesame . "', Open_Sesame_En = '" . $Open_Sesame_En . "', Phrase_Ke = '" . $Phrase_ID . "' WHERE User_LoginId = '" . $User_LoginId . "'";            
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    
                    $_UpdateQuery2 = "UPDATE tbl_ps SET PS_Status = '0' WHERE User_LoginId = '" . $User_LoginId . "'";            
                    $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
                    
                    $_InsertQuery3 = "INSERT INTO `tbl_ps`(`User_LoginId`, `PS`, `PS_Status`) 
                                 VALUES ('" . $User_LoginId . "', '" . $pass . "',  '1')";
                    $_ObjConnection->ExecuteQuery($_InsertQuery3, Message::InsertStatement);

                    $_InsertQuery2 = "INSERT INTO `tbl_into_s_log`(`User_Code`, `User_LoginId`, `Open_Sesame`, `Open_Sesame_En`, `Phrase_Ke`) 
                                     VALUES ('" . $_row['User_Code'] . "', '" . $User_LoginId . "', '" . $Open_Sesame . "',  '" . $Open_Sesame_En . "', '" . $Phrase_ID . "')";
                    return $_ObjConnection->ExecuteQuery($_InsertQuery2, Message::InsertStatement); 
                }
                
            }else{
                $_SelectQuery = "Select User_Code,User_LoginId FROM tbl_user_master WHERE User_LoginId = '" . $User_LoginId . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_row = mysqli_fetch_array($_Response[2]);            
                $User_Code=$_row['User_Code'];            
                $User_LoginId=$_row['User_LoginId'];
                
                $_SelectQuery1 = "Select * FROM tbl_phrase_ke WHERE Phrase_Status = 1";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_row1 = mysqli_fetch_array($_Response1[2]);            
                $Phrase_ID=$_row1['Phrase_ID'];            
                $Phrase_Ke=$_row1['Phrase_Ke'];            
                         
                $Open_Sesame = encryptPasswordFinal($pass, $Phrase_Ke);
                $Open_Sesame_En = password_hash($Open_Sesame, PASSWORD_BCRYPT);
                
                $_InsertQuery = "INSERT INTO `tbl_into_s`(`User_Code`, `User_LoginId`, `Open_Sesame`, `Open_Sesame_En`, `Phrase_Ke`) 
                                 VALUES ('" . $User_Code . "', '" . $User_LoginId . "', '" . $Open_Sesame . "',  '" . $Open_Sesame_En . "', '" . $Phrase_ID . "')";
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                
                $_InsertQuery3 = "INSERT INTO `tbl_ps`(`User_LoginId`, `PS`, `PS_Status`) 
                                 VALUES ('" . $User_LoginId . "', '" . $pass . "',  '1')";
                $_ObjConnection->ExecuteQuery($_InsertQuery3, Message::InsertStatement);
                
                $_InsertQuery2 = "INSERT INTO `tbl_into_s_log`(`User_Code`, `User_LoginId`, `Open_Sesame`, `Open_Sesame_En`, `Phrase_Ke`) 
                                 VALUES ('" . $User_Code . "', '" . $User_LoginId . "', '" . $Open_Sesame . "',  '" . $Open_Sesame_En . "', '" . $Phrase_ID . "')";
                return $_ObjConnection->ExecuteQuery($_InsertQuery2, Message::InsertStatement);     
                
            }
            
            
        } else {
            return false;
        }
    }
    
    public function ResendOTPNEW($userId = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_OTP = $this->GenerateOTP();

        try {
            $_UserM = (!empty($userId)) ? $userId : $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_LoginId='" . $_UserM . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_Mobile = $_Row1['User_MobileNo'];
            $_Email = $_Row1['User_EmailId'];
            
            date_default_timezone_set('Asia/Kolkata');
            $_OTP_StartOn=date("Y-m-d H:i:s");
            $_OTP_EndOn=date("Y-m-d H:i:s", strtotime("+10 minutes"));
            //$_OTP_StartOn= date('Y-m-d m:i:s');
           // $_OTP_EndOn= date('Y-m-d m:i:s', strtotime("+10 minutes"));

            $_InsertQuery = "Insert Into tbl_ao_register(AO_Code,AO_Email,AO_Mobile,AO_OTP,OTP_StartOn,OTP_EndOn) 
            Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code, 
            '" . $_Email . "' as AO_Email, 
            '" . $_Mobile . "' as AO_Mobile, 
            '" . $_OTP . "' as AO_OTP, 
            '" . $_OTP_StartOn . "' as OTP_StartOn,
            '" . $_OTP_EndOn . "' as OTP_EndOn 
            From tbl_ao_register";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_ao_register Where AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_Status = 0";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            }
            else
            {
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                $AO_Code=$_row['AO_Code'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d H:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);
                if($datetime1 > $datetime2)
                {
                     //echo 'IF NOT EXPIRE';die;
                    $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Code='" . $AO_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
                else
                {   
                    //echo 'IF EXPIRE';die;
                    $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Code='" . $AO_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }
    
    public function SendOTPNEW($userId = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_OTP = $this->GenerateOTP();

        try {
            $_UserM = (!empty($userId)) ? $userId : $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_LoginId='" . $_UserM . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_Mobile = $_Row1['User_MobileNo'];
            $_Email = $_Row1['User_EmailId'];
            
            date_default_timezone_set('Asia/Kolkata');
            $_OTP_StartOn=date("Y-m-d H:i:s");
            $_OTP_EndOn=date("Y-m-d H:i:s", strtotime("+10 minutes"));
            //$_OTP_StartOn= date('Y-m-d m:i:s');
           // $_OTP_EndOn= date('Y-m-d m:i:s', strtotime("+10 minutes"));

            $_InsertQuery = "Insert Into tbl_ao_register(AO_Code,AO_Email,AO_Mobile,AO_OTP,OTP_StartOn,OTP_EndOn) 
            Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code, 
            '" . $_Email . "' as AO_Email, 
            '" . $_Mobile . "' as AO_Mobile, 
            '" . $_OTP . "' as AO_OTP, 
            '" . $_OTP_StartOn . "' as OTP_StartOn,
            '" . $_OTP_EndOn . "' as OTP_EndOn 
            From tbl_ao_register";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_ao_register Where AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_Status = 0";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            }
            else
            {
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                $AO_Code=$_row['AO_Code'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d H:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);
                if($datetime1 > $datetime2)
                {
                     //echo 'IF NOT EXPIRE';die;
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
                else
                {   
                    //echo 'IF EXPIRE';die;
                    $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Code='" . $AO_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }
    
    public function VerifyOTPNEW($oid, $otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Code='" . $oid . "' AND AO_OTP = '" . $otp . "' AND AO_Status = '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //echo "<pre>"; print_r($_Response);die;
            if($_Response[0]=='Success'){
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d H:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);


                if($datetime1 > $datetime2)
                {
                    if ($_Response[0] != Message::NoRecordFound) {
                        $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Code='" . $oid . "'";
                        $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    }
                }
                else
                {   
                    $_Response[0]='EXPIRE';
                }
            }
            else
                {   
                    $_Response[0]='NORECORD';
                }
            
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
//echo "<pre>"; print_r($_Response);die;
        return $_Response;
    }
    
    
    public function GetSecurityQuestion() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select SQ_ID, Security_Question From tbl_security_q where Security_Status = 1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
    public function GetSecurityQuestionITGK($ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_DuplicateQuery = "Select * From tbl_security_q_user Where User_LoginId='" . $ccode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
    
    
    
    /// // theser part is used for login security questions//
    public function setSecurityQueAns($SQ_ID, $securityqueans, $ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_security_q_user 
                ( User_LoginId, SQ_ID, Security_Question_Answer, Security_Status)
                VALUES ('".$ccode."', ".$SQ_ID.", '".$securityqueans."', 1)";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_security_q_user Where User_LoginId='" . $ccode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];die;
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                //echo "sunil";echo "<pre>"; print_r($_Response);
                return $_Response;
                
            }
            else
            {
                $_DuplicateQuery1 = "Select * From tbl_security_q_user Where User_LoginId='" . $ccode . "' AND SQ_ID = '" . $SQ_ID . "' AND Security_Question_Answer = '" . $securityqueans . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);
                //echo "sunil2";echo "<pre>"; print_r($_Response);
                return $_Response;
                
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            //echo "sunil33";echo "<pre>"; print_r($_Response);
            return $_Response;
        }

        
    }
    public function getVeriFyOTPForm2($id, $_Center, $_Mobile) {
        return '
        <div class="locksmall"><img src="images/lock_icon_small.png"></div>
                            <div class="panelforgot">Forgot / Reset Password</div>
                         
        <div class="validate"><b>Validate OTP: (One time password)</b></div>
            
            <div style="clear: both;"></div>
            <hr>
        <div style="float:left; margin-left:25px;"><span style="color: green;">Dear '.$_Center.' a one time password has been sent to your registered Mobile No. '.$_Mobile.'.</span><p></p></div>
            <div id="response"  style="padding-left: 23px;"></div>  
            <div style="float:left; margin-left:25px;"><span>Please enter OTP to Verify your User Name.</span><p></p></div>
        <div class="col-lg-12 col-md-12"><input type="text" id="txtOTP" name="txtOTP" class="inputsearch" onkeypress="javascript:return allownumbers(event);"/>
        <input type="hidden" value="' . $id . '" name="oid" id="oid">
        <input type="hidden" value="' . $_Center . '" name="ccode" id="ccode"></div>';
    }
    /// // theser part is used for login security questions//
    
    
    //// for squerty queations 9-1-2019/////
    
    public function getVeriFyOTPFormSecurity($id, $_Center, $_Mobile) {
        return '
        <div class="locksmall"><img src="images/security_small.png"></div>
                            <div class="panelforgot">Reset Security Question</div>
                         
        <div class="validate"><b>Validate OTP: (One time password)</b></div>
            <div class="goto"><a href="reset_security_question.php">Go Back</a></div>
            <div style="clear: both;"></div>
            <hr>
        <div style="float:left; margin-left:25px;"><span style="color: green;">Dear MYRKCL a one time password has been sent to your registered Mobile No. '.$_Mobile.'.</span><p></p></div>
            <div id="response" style="padding-left: 23px;"></div>  
            <div style="float:left; margin-left:25px;"><span>Please enter OTP to Verify your User Name.</span><p></p></div>
        <div class="col-lg-12 col-md-12"><input maxlength="8" type="text" id="txtOTP" name="txtOTP" class="inputsearch" onkeypress="javascript:return allownumbers(event);"/>
        <input type="hidden" value="' . $id . '" name="oid" id="oid">
        <input type="hidden" value="' . $_Center . '" name="ccode" id="ccode"></div>';
    }
    
    public function setSecurityQueAnsUpdate($SQ_ID, $securityqueans, $ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_DuplicateQuery = "Select * From tbl_security_q_user Where User_LoginId='" . $ccode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];die;
            if ($_Response[0] == Message::NoRecordFound) 
             {
                $_InsertQuery = "INSERT INTO tbl_security_q_user 
                ( User_LoginId, SQ_ID, Security_Question_Answer, Security_Status)
                VALUES ('".$ccode."', ".$SQ_ID.", '".$securityqueans."', 1)";
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                
                $_InsertQuery_log = "INSERT INTO tbl_security_q_user_log 
                ( User_LoginId, SQ_ID, Security_Question_Answer, Security_Status)
                VALUES ('".$ccode."', ".$SQ_ID.", '".$securityqueans."', 1)";
                $_Response_log = $_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);
                //echo "sunil";echo "<pre>"; print_r($_Response);
                return $_Response;
                
            }
            else
            {
                
                $_UpdateQuery = "Update tbl_security_q_user set SQ_ID='".$SQ_ID."',Security_Question_Answer = '".$securityqueans."' WHERE User_LoginId='" . $ccode . "'";
                $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                
                $_InsertQuery_log = "INSERT INTO tbl_security_q_user_log 
                ( User_LoginId, SQ_ID, Security_Question_Answer, Security_Status)
                VALUES ('".$ccode."', ".$SQ_ID.", '".$securityqueans."', 1)";
                $_Response_log = $_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);
                return $_Response;
                
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            //echo "sunil33";echo "<pre>"; print_r($_Response);
            return $_Response;
        }

        
    }

}

?>