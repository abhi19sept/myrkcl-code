<?php
/**
 * Description of clsParentFunctionMaster
 *
 *  Author Name:  Sunil Kumar Baindara
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRkclApiFDetail {
    //put your code here
    
    //*/Get all data record form function table//*//
    
    
     public function GetAllFunctionName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_rkcl_api_functions Order By functionname asc";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    //*/Get all data record form function table//*//
    
    //*/Get all data record form function table//*//
    
    
     public function GetTableFiledByTabName($ftablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='database_rkclcoin_1' AND `TABLE_NAME`='" . $ftablename . "' ";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    //*/Get all data record form function table//*//
    
    public function GetAll($client_id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$client_id = mysqli_real_escape_string($_ObjConnection->Connect(),$client_id);
				
            $_SelectQuery = "Select * From tbl_rkcl_api_fun_details where `client_rkclAPi_id`='" . $client_id . "' Order By client_fun_id desc";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_rkclAPi_id)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_rkclAPi_id = mysqli_real_escape_string($_ObjConnection->Connect(),$_rkclAPi_id);
				
            $_SelectQuery = "Select Functionname, Functionfileds From tbl_rkcl_api_fun_details Where
				client_fun_id='" . $_rkclAPi_id . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Root_Menu_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Root_Menu_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Root_Menu_Code);
				
            $_DeleteQuery = "Delete From tbl_rkcl_api_fun_details Where client_fun_id='" . $_Root_Menu_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ClientApiId,$_Functionname,$_Functionfileds) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ClientApiId = mysqli_real_escape_string($_ObjConnection->Connect(),$_ClientApiId);
				$_Functionname = mysqli_real_escape_string($_ObjConnection->Connect(),$_Functionname);
				$_Functionfileds = mysqli_real_escape_string($_ObjConnection->Connect(),$_Functionfileds);
				
            $_Status=1;    
            $_InsertQuery = "INSERT INTO tbl_rkcl_api_fun_details (client_rkclAPi_id, Functionname, Functionfileds, Status)
                                      VALUES ('".$_ClientApiId."','".$_Functionname."','".$_Functionfileds."','".$_Status."');";
            $_DuplicateQuery = "Select * From tbl_rkcl_api_fun_details Where client_rkclAPi_id='" . $_ClientApiId . "' AND Functionname='" . $_Functionname . "' ";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                 $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }       
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_RootCode,$_RootName,$_RootStatus,$_RootDisplay) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootCode);
				$_RootName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootName);
				$_RootStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootStatus);
				$_RootDisplay = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootDisplay);
				
            $_UpdateQuery = "Update tbl_root_menu set Root_Menu_Name='" . $_RootName . "',"
                    . "Root_Menu_Status='" . $_RootStatus . "',Root_Menu_Display='" . $_RootDisplay . "' Where Root_Menu_Code='" . $_RootCode . "'";
            $_DuplicateQuery = "Select * From tbl_parent_function_master Where Parent_Function_Name='" . $_RootName . "' "
                    . "and Parent_Function_Code <> '" . $_RootCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
