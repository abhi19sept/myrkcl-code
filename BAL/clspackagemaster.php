<?php


/**
 * Description of clsOrgDetail
 *
 * @author 
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clspackagemaster {
	
	 
	
	public function Add($_package,$_lable,$_price) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			 $_ITGK_Code =   $_SESSION['User_LoginId'];
			 $_package = mysqli_real_escape_string($_ObjConnection->Connect(),$_package);
			 
             $_InsertQuery = "Insert Into tbl_package(id,package,lable,price,Status) 
			 Select Case When Max(id) Is Null Then 1 Else Max(id)+1 End as id,
			 '" . $_package. "' as package,
			 '" . $_lable. "' as lable,
			 '" .$_price. "' as price,
			 '1' as Status
			 From tbl_package";
			
                    
            $_DuplicateQuery = "Select * From tbl_package Where package='".$_package."'";
					
                    
             $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
             if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				//$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
       //print_r($_Response);
        return $_Response;
    }
	
	
	public function GetAll() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code =   $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select * from tbl_package";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	public function Update($_Status,$_id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				$_id = mysqli_real_escape_string($_ObjConnection->Connect(),$_id);
				
			if($_Status==1)
			{
				$_Status='0';
			}
			else
			{
				$_Status='1';
			}
            $_UpdateQuery = "Update tbl_package set Status='" . $_Status . "'
                     Where id='" . $_id. "'";
           
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	

	
	
}
