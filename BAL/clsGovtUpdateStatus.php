<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGovtUpdateStatus {
    //put your code here
    public function GetAll($_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select a.*, b.lotname FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid where trnpending='" . $_Status . "'";
			}
			else{
				$_SelectQuery = "Select a.*, b.lotname FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid where Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND trnpending='" . $_Status . "'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function FILLOTFORUPDATESTATUS() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where govrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLSTATUSFORUPDATE() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where govrole='Yes' AND capprovalid in ('3','0','4','5','8','9','6','7','10') ORDER BY 
								FIELD(capprovalid,'3','0','4','5','8','9','10','7','6')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function ShowDetailsToUpdate($_Status,$_Lotid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            $_Lotid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lotid);
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select * FROM `tbl_govempentryform` where lot='" . $_Lotid . "' AND trnpending='" . $_Status . "'";
			}
			else{
				$_SelectQuery = "Select * FROM `tbl_govempentryform` where lot='" . $_Lotid . "' AND trnpending='" . $_Status . "'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function Update_LearnerStatus($_LearnerCode, $_Update_Status) {
         //print_r($_LearnerCode);
        global $_ObjConnection;
        $_ObjConnection->Connect();
         try {            
             $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
             $_Update_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Update_Status);
              $_UpdateQuery = "Update tbl_govempentryform set trnpending='" . $_Update_Status . "' WHERE learnercode IN ($_LearnerCode)"; 
			  $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);                     
        }        
        catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
 
}
