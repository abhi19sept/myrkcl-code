<?php

/**
 * Description of clsAadharLinkCountReport
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsAadharLinkCountReport {

    //put your code here
	public function FillEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Event_Id,Event_Name from tbl_events where Event_Status='1' and Event_Id >= '1220' order by Event_Id";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					if($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8' || 
						$_SESSION['User_UserRoll'] == '9'){							
						$_SelectQuery = "select Admission_LearnerCode,Admission_ITGK_Code,upper(Admission_Name) as name,
											upper(Admission_Fname) as father,Admission_Mobile 
											from tbl_admission as a inner join tbl_aadhar_details as b on 
											a.Admission_LearnerCode=b.Aadhar_Detail_Lcode
											where Aadhar_Detail_DateTime>='".$_StartDate."' AND Aadhar_Detail_DateTime<='".$_EndDate."'";
							$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);							
					}
					else if($_SESSION['User_UserRoll'] == '7'){
						$_SelectQuery = "select Admission_LearnerCode,Admission_ITGK_Code,upper(Admission_Name) as name,
											upper(Admission_Fname) as father,Admission_Mobile 
											from tbl_admission as a inner join tbl_aadhar_details as b on 
											a.Admission_LearnerCode=b.Aadhar_Detail_Lcode
											where Aadhar_Detail_DateTime>='".$_StartDate."' AND Aadhar_Detail_DateTime<='".$_EndDate."'
											and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);	
					}
				}
				else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;           
			}
        return $_Response;
    }

	public function GetEventBatch($eventid,$batchtype) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if($batchtype=='1'){
					$column='Affilate_FreshBaches';
				}
				else{
					$column='Affilate_ReexamBatches';
				}
            $_SelectQuery = "select $column as batchcode from tbl_exammaster where Affilate_Event='".$eventid."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GeneratedCertificateCount($batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select  sum(case when Admission_Aadhar_Status in ('2','4') and Admission_Payment_Status='1' 
								 then 1 else 0 end) as codes, b.Batch_Name, c.Course_Name, Admission_Batch from tbl_admission as a inner join
							tbl_batch_master as b on a.Admission_Batch=b.Batch_Code inner join tbl_course_master as c 
							on a.Admission_Course=c.Course_Code where Admission_Batch in ($batch) group by Admission_Batch";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetLearnerDetails($batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Admission_LearnerCode,Admission_ITGK_Code,upper(Admission_Name) as name,Admission_Mobile,
							upper(Admission_Fname) as fname,Admission_UID from tbl_admission where Admission_Batch='".$batchcode."'
							and Admission_Aadhar_Status in ('2','4') and Admission_Payment_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
