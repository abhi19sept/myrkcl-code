<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsUpdateBankDetails
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRenewedItgkList {
    //put your code here   

	
	public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Distinct Courseitgk_Course From tbl_courseitgk_mapping order by Courseitgk_EOI";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	
	public function GetITGKDetails($_Course)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_LoginRole = $_SESSION['User_UserRoll'];
				
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				
				 if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '11') {
						$_SelectQuery = "Select a.Courseitgk_Course, a.Courseitgk_ITGK, a.Courseitgk_EOI, a.CourseITGK_UserCreatedDate, a.CourseITGK_ExpireDate,
										c.Organization_Name as ITGK_Name, d.District_Name,e.Tehsil_Name,f.User_LoginId as RSP_Code,
										g.Organization_Name as RSP_Name, b.User_MobileNo as ITGK_Mobile From tbl_courseitgk_mapping as a INNER JOIN tbl_user_master as b
										 ON a.Courseitgk_ITGK = b.User_LoginId inner join tbl_organization_detail as c on b.User_Code=c.Organization_User
										inner join tbl_district_master as d on c.Organization_District=d.District_Code
										inner join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
										inner join tbl_user_master as f on b.User_Rsp=f.User_Code
										inner join tbl_organization_detail as g on f.User_Code=g.Organization_User
										WHERE Courseitgk_Course='" . $_Course . "' AND CURDATE() <= `CourseITGK_ExpireDate` AND EOI_Fee_Confirm='1'";
						$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
						return $_Response;
				 }
				 else if ($_LoginRole == '7') {
					 $_SelectQuery = "Select a.Courseitgk_Course, a.Courseitgk_ITGK, a.Courseitgk_EOI, a.CourseITGK_UserCreatedDate, a.CourseITGK_ExpireDate,c.Organization_Name as ITGK_Name,
										d.District_Name,e.Tehsil_Name,f.User_LoginId as RSP_Code,g.Organization_Name as RSP_Name, b.User_MobileNo as ITGK_Mobile
										 From tbl_courseitgk_mapping as a INNER JOIN tbl_user_master as b
										 ON a.Courseitgk_ITGK = b.User_LoginId inner join tbl_organization_detail as c on b.User_Code=c.Organization_User
										inner join tbl_district_master as d on c.Organization_District=d.District_Code
										inner join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
										inner join tbl_user_master as f on b.User_Rsp=f.User_Code
										inner join tbl_organization_detail as g on f.User_Code=g.Organization_User
										WHERE b.User_LoginId = '" . $_SESSION['User_LoginId'] . "' AND Courseitgk_Course='" . $_Course . "'
										 AND CURDATE() <= `CourseITGK_ExpireDate` AND EOI_Fee_Confirm='1'";
					 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					 return $_Response;
				 }
				 else if ($_LoginRole == '14') {
					 $_SelectQuery = "Select a.Courseitgk_Course, a.Courseitgk_ITGK, a.Courseitgk_EOI, a.CourseITGK_UserCreatedDate,a.CourseITGK_ExpireDate,c.Organization_Name as ITGK_Name,
										d.District_Name,e.Tehsil_Name,f.User_LoginId as RSP_Code,g.Organization_Name as RSP_Name, b.User_MobileNo as ITGK_Mobile
										 From tbl_courseitgk_mapping as a INNER JOIN tbl_user_master as b
										 ON a.Courseitgk_ITGK = b.User_LoginId inner join tbl_organization_detail as c on b.User_Code=c.Organization_User
										inner join tbl_district_master as d on c.Organization_District=d.District_Code
										inner join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
										inner join tbl_user_master as f on b.User_Rsp=f.User_Code
										inner join tbl_organization_detail as g on f.User_Code=g.Organization_User
										WHERE b.`User_Rsp` = '" . $_SESSION['User_Code'] . "' AND Courseitgk_Course='" . $_Course . "' AND 
										 CURDATE() <= `CourseITGK_ExpireDate` AND EOI_Fee_Confirm='1'";
					 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					 return $_Response;
				 }
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 
		}	 
		
}
