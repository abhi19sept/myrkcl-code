<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Mayank
 */

require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNameAddressFeePayment extends paymentFunctions {

    //put your code here

public function Check_Center() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "select * from tbl_courseitgk_mapping where Courseitgk_Course = 'RS-CIT Women' and Courseitgk_EOI='32' and
                            CourseITGK_BlockStatus='unblock' and EOI_Fee_Confirm='1' and Courseitgk_ITGK='" . $_SESSION['User_LoginId'] . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
    public function GetPayEligibility($_ITGK, $payfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
				$payfor = mysqli_real_escape_string($_ObjConnection->Connect(),$payfor);
				
            $_SelectQuery = "Select * FROM tbl_change_address_itgk WHERE fld_ITGK_Code = '" . $_ITGK . "'" . " AND fld_requestChangeType='" . $payfor . "' AND fld_status='3'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetOrgRuralDetails($payfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$payfor = mysqli_real_escape_string($_ObjConnection->Connect(),$payfor);
				
                $_SelectQuery = "select b.Block_Name as old_block, c.Block_Name as new_block,
                                d.GP_Name as old_gram, e.GP_Name as new_gram, f.Village_Name as old_village, 
                                g.Village_Name as new_village, b1.Municipality_Type_Name as old_mtype, c1.Municipality_Type_Name as
                                new_mtype, d1.Municipality_Name as old_mname, e1.Municipality_Name as new_mname, 
                                f1.Ward_Name as old_ward, g1.Ward_Name as new_ward
                                
                                from tbl_change_address_itgk as a, 
                                tbl_panchayat_samiti as b , tbl_panchayat_samiti as c ,
                                tbl_gram_panchayat as d, tbl_gram_panchayat as e,
                                tbl_village_master as f, tbl_village_master as g,
                                
                                tbl_change_address_itgk as a1, 
                                tbl_municipality_type as b1 , tbl_municipality_type as c1, tbl_municipality_master as d1, 
                                tbl_municipality_master as e1, tbl_ward_master as f1, tbl_ward_master as g1
                                
                                where a.fld_ITGK_Code='" . $_SESSION['User_LoginId'] . "' AND 
                                a.fld_requestChangeType='" . $payfor . "' AND a.fld_status='3' and 
                                a.Organization_Panchayat_old=b.Block_Code and a.Organization_Panchayat=c.Block_Code 
                                and a.Organization_Gram_old=d.GP_Code and a.Organization_Gram=e.GP_Code and 
                                a.Organization_Village_old=f.Village_Code and a.Organization_Village=g.Village_Code and
                                
                                a1.Organization_Municipal_Type_old=b1.Municipality_Type_Code and
                                a1.Organization_Municipal_Type=c1.Municipality_Type_Code and 
                                a1.Organization_Municipal_old=d1.Municipality_Code and
                                a1.Organization_Municipal=e1.Municipality_Code and 
                                a1.Organization_WardNo_old=f1.Ward_Code and
                                a1.Organization_WardNo=g1.Ward_Code";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetOrgUrbanDetails($payfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$payfor = mysqli_real_escape_string($_ObjConnection->Connect(),$payfor);
				
                $_SelectQuery = "select b.Municipality_Type_Name as old_mtype, c.Municipality_Type_Name as
                                new_mtype, d.Municipality_Name as old_mname, e.Municipality_Name as new_mname, 
                                f.Ward_Name as old_ward, g.Ward_Name as new_ward from tbl_change_address_itgk as a, 
                                tbl_municipality_type as b , tbl_municipality_type as c , tbl_municipality_master as d, 
                                tbl_municipality_master as e, tbl_ward_master as f, tbl_ward_master as g 
                                where a.fld_ITGK_Code='" . $_SESSION['User_LoginId'] . "' AND 
                                a.fld_requestChangeType='" . $payfor . "' AND fld_status='3'  and 
                                a.Organization_Municipal_Type_old=b.Municipality_Type_Code and
                                a.Organization_Municipal_Type=c.Municipality_Type_Code and 
                                a.Organization_Municipal_old=d.Municipality_Code and
                                a.Organization_Municipal=e.Municipality_Code and a.Organization_WardNo_old=f.Ward_Code and
                                a.Organization_WardNo=g.Ward_Code";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
/*	public function GetBothAreaType() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Organization_AreaType, Organization_AreaType_old from tbl_change_address_itgk where "
                    . "fld_ITGK_Code='" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    } */
	
	public function GetBothAreaType() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery1 = "select fld_ref_no from tbl_change_address_itgk where "
                    . "fld_ITGK_Code='" . $_SESSION['User_LoginId'] . "' AND fld_status='3'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_rows = mysqli_fetch_array($_Response1[2]);
            $RefNo = $_rows['fld_ref_no'];
            
            $_SelectQuery = "select Organization_AreaType, Organization_AreaType_old from tbl_change_address_itgk where "
                    . "fld_ref_no='" . $RefNo . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function Getamtncr() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select NCRamount_Amount from tbl_ncramount_master where "
                    . "NCRamount_ITGK='" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Getamturban($AreaTypeNew) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$AreaTypeNew = mysqli_real_escape_string($_ObjConnection->Connect(),$AreaTypeNew);
			
            $_SelectQuery = "Select Ncr_Fee_Amount FROM tbl_ncrfee_master WHERE Ncr_Fee_Status = 'Active' "
                    . "AND Ncr_Fee_Category='" . $AreaTypeNew . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetPayForAmt($_Type) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Type = mysqli_real_escape_string($_ObjConnection->Connect(),$_Type);
			
            $_SelectQuery = "Select NameAddress_Fee_Amount FROM tbl_nameaddressfee_master WHERE "
                    . "NameAddress_Fee_Status = 'Active' AND NameAddress_Fee_Category='" . $_Type . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
        public function ChkPayEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Event_NameAddressPayment,Event_Startdate,Event_Enddate FROM tbl_event_management WHERE CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='11'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	/**
	  public function ChkPaymentStatus($refId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_address_name_transaction WHERE fld_ref_no = '" . $refId . "' AND fld_Transaction_Status = 'Success'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	**/
	
	    public function ChkPaymentStatus($refId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$refId = mysqli_real_escape_string($_ObjConnection->Connect(),$refId);
			
            $_SelectQuery = "Select * FROM tbl_address_name_transaction WHERE fld_ref_no = '" . $refId . "' AND fld_Transaction_Status = 'Success'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
            if ($_Response[0] == "Success") {
            $_UpdateQuery = "UPDATE tbl_change_address_itgk SET fld_status = '5' WHERE fld_ref_no = '" . $refId . "' AND fld_status = '3'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


    public function AddNameAddressPayTran($amount, $postValues) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $payFor = ucwords($postValues['payforchange']) . ' Change Fee Payment';
                $return = parent::insertPaymentTransaction($payFor, $_SESSION['User_LoginId'], $amount, [$postValues['refid']], 0, 0);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }

    public function GetDetails($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
            $_SelectQuery = "Select UserProfile_FirstName,UserProfile_Mobile "
                    . "From tbl_userprofile  "
                    . "Where UserProfile_User='" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
