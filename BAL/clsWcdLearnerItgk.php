<?php

/**
 * Description of clsWcdLearnerItgk
 *
 * @author Abhishek
 */
require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdLearnerItgk {

    //put your code here

    public function getdatawcd($_course, $_batch) {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
            $_LoginUserRole = $_SESSION['User_UserRoll'];
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4'){
				 $_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, 
									sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS pref,
									sum(case when Oasis_Admission_ReportedStatus = 'Reported' then 1 else 0 end) Reported_Learner,
									sum(case when Oasis_Admission_ReportedStatus = 'Not Reported' then 1 else 0 end) NotReported_Learner
									from tbl_user_master as a
									inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.user_loginid inner join tbl_district_master as c on 
									b.Oasis_Admission_District = c.District_Code WHERE a.User_UserRoll='7' and
									b.Oasis_Admission_Course='".$_course."' AND 
									b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible'
									group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_LoginUserRole == '17') {
				//print_r($_SESSION);
				  $_SelectQuery1 = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference,
								sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS pref,
								sum(case when Oasis_Admission_ReportedStatus = 'Reported' then 1 else 0 end) Reported_Learner,
								sum(case when Oasis_Admission_ReportedStatus = 'Not Reported' then 1 else 0 end) NotReported_Learner
								from tbl_user_master as a
								inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.User_LoginId inner join tbl_district_master as c on
								b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND 
								b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible' AND
								b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "'
								 group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			}
           } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function GetLearnerList($_mode, $_batch, $_districtcode, $_Itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_mode = mysqli_real_escape_string($_ObjConnection->Connect(),$_mode);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				$_districtcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_districtcode);
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
			   if($_mode == 'one') {
				 $_SelectQuery3 = "SELECT b.*,c.Wcd_Intake_Available,d.Category_Name,e.Qualification_Name FROM tbl_oasis_admission as b
									inner join tbl_wcd_intake_master as c on b.Oasis_Admission_Final_Preference = c.Wcd_Intake_Center
									INNER JOIN tbl_category_master as d on b.Oasis_Admission_Category = d.Category_Code
									INNER JOIN tbl_qualification_master as e on b.Oasis_Admission_Qualification = e.Qualification_Code
									WHERE b.Oasis_Admission_Batch = '" . $_batch . "' AND b.Oasis_Admission_District='" . $_districtcode . "'
									AND b.Oasis_Admission_Final_Preference='" . $_Itgk . "' AND b.Oasis_Admission_Eligibility='eligible' AND c.Wcd_Intake_Batch='" . $_batch . "'
									AND b.Oasis_Admission_LearnerStatus in ('Approved') Order by Oasis_Admission_Final_Priority";   
			   }
				else if($_mode == 'two') {
					$_SelectQuery3 = "SELECT b.*,c.Wcd_Intake_Available,d.Category_Name,e.Qualification_Name FROM tbl_oasis_admission as b
									inner join tbl_wcd_intake_master as c on b.Oasis_Admission_Final_Preference = c.Wcd_Intake_Center
									INNER JOIN tbl_category_master as d on b.Oasis_Admission_Category = d.Category_Code
									INNER JOIN tbl_qualification_master as e on b.Oasis_Admission_Qualification = e.Qualification_Code
									WHERE b.Oasis_Admission_Batch = '" . $_batch . "' AND b.Oasis_Admission_District='" . $_districtcode . "'
									AND b.Oasis_Admission_Final_Preference='" . $_Itgk . "' AND b.Oasis_Admission_Eligibility='eligible' AND c.Wcd_Intake_Batch='" . $_batch . "'
									AND b.Oasis_Admission_LearnerStatus in ('Approved') AND b.Oasis_Admission_ReportedStatus='Reported' Order by Oasis_Admission_Final_Priority"; 
				}   
				else if($_mode == 'three') {
					$_SelectQuery3 = "SELECT b.*,c.Wcd_Intake_Available,d.Category_Name,e.Qualification_Name FROM tbl_oasis_admission as b
									inner join tbl_wcd_intake_master as c on b.Oasis_Admission_Final_Preference = c.Wcd_Intake_Center
									INNER JOIN tbl_category_master as d on b.Oasis_Admission_Category = d.Category_Code
									INNER JOIN tbl_qualification_master as e on b.Oasis_Admission_Qualification = e.Qualification_Code
									WHERE b.Oasis_Admission_Batch = '" . $_batch . "' AND b.Oasis_Admission_District='" . $_districtcode . "'
									AND b.Oasis_Admission_Final_Preference='" . $_Itgk . "' AND b.Oasis_Admission_Eligibility='eligible' AND c.Wcd_Intake_Batch='" . $_batch . "'
									AND b.Oasis_Admission_LearnerStatus in ('Approved') AND b.Oasis_Admission_ReportedStatus='Not Reported' Order by Oasis_Admission_Final_Priority"; 
				}   
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }


public function GetDatabyCode($_Batch, $_Lcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				$_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				
             $_SelectQuery = "Select a.*,b.Category_Name,c.Qualification_Name From tbl_oasis_admission as a INNER JOIN tbl_category_master as b 
								on a.Oasis_Admission_Category = b.Category_Code INNER JOIN tbl_qualification_master as c
								on a.Oasis_Admission_Qualification = c.Qualification_Code Where Oasis_Admission_Batch='" . $_Batch . "' 
								AND Oasis_Admission_LearnerCode='" . $_Lcode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
   
public function UpdateWcdNotReported($_LCode, $_ProcessStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	
				$_LCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LCode);
				
           $_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_ReportedStatus='Not Reported'"                   
                    . " Where Oasis_Admission_LearnerCode='" . $_LCode . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
public function UpdateWcdReported($_Batch, $_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
				
				$_SelectQuery2 = "SELECT Oasis_Admission_ReportedStatus from tbl_oasis_admission where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $_Batch . "'
								AND Oasis_Admission_ReportedStatus = 'Reported'";   
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
				$_count = mysqli_num_rows($_Response2[2]);
				//$_Row1=mysql_fetch_array($_Response2[2]);
				if($_count){
					echo "Already Reported";
					return;
				}
			else {
				
					$_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_ReportedStatus='Reported'"                   
							. " Where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $_Batch . "'"
							. " AND Oasis_Admission_LearnerStatus = 'Approved' AND Oasis_Admission_Eligibility='eligible' AND Oasis_Admission_ReportedStatus is NULL";
				   $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement); 
				}		   
//print_r($_Response);		   
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }

	public function getdatawcditgk($_course,$batch) {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
				$_LoginUserRole = $_SESSION['User_UserRoll'];
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
				if ($_LoginUserRole == '7'){
					$_SelectQuery = "select Oasis_Admission_LearnerCode,Oasis_Admission_Name,Oasis_Admission_Fname,Oasis_Admission_Mobile,
							Oasis_Admission_Photo,Oasis_Admission_Sign from tbl_oasis_admission where
							Oasis_Admission_Final_Preference='" . $_SESSION['User_LoginId'] . "' and Oasis_Admission_Batch='".$batch."'
							and Oasis_Admission_Course='".$_course."' and Oasis_Admission_LearnerStatus='Approved'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}              
			}					
			else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
            }      
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
