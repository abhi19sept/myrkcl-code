<?php
/**
 * Description of clsWcdAllocateBatchTime
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
include('DAL/smtp_class.php');
$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsWcdAllocateBatchTime {
    //put your code here

public function GetAllLearner($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
				$_LoginUserRole = $_SESSION['User_UserRoll'];
				if($_LoginUserRole == '7'){
						$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
						$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
						
					$_ITGK_Code = $_SESSION['User_LoginId'];
					$_SelectQuery = "select Oasis_Admission_LearnerCode,Oasis_Admission_Name,Oasis_Admission_Fname,Oasis_Admission_DOB,
									Oasis_Admission_Mobile,b.Batch_Time_lcode,Batch_Time_time,Batch_Time_email
									from tbl_oasis_admission as a left join
									tbl_wcd_allocate_batch_time as b on a.Oasis_Admission_LearnerCode=b.Batch_Time_lcode
									where Oasis_Admission_Course='".$_course."' and
									Oasis_Admission_Final_Preference='".$_ITGK_Code."' and Oasis_Admission_Batch='".$_batch."'
									and Oasis_Admission_LearnerStatus='Approved' order by Oasis_Admission_Final_Priority";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
			else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
            }		
			}
			else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
            }		
		}
		catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
							order by Batch_Code DESC LIMIT 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function updaterscitbatchtime($_appid,$_allottime,$_batch) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_appid = mysqli_real_escape_string($_ObjConnection->Connect(),$_appid);
					$_allottime = mysqli_real_escape_string($_ObjConnection->Connect(),$_allottime);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					
                $_InsertQuery = "insert into tbl_wcd_allocate_batch_time (Batch_Time_lcode, Batch_Time_ITGK_Code, Batch_Time_Learner_Name,
								Batch_Time_Father_Name, Batch_Time_dob, Batch_Time_course, Batch_Time_batch, Batch_Time_mobile, 
								Batch_Time_time) select Oasis_Admission_LearnerCode, Oasis_Admission_Final_Preference,
								Oasis_Admission_Name, Oasis_Admission_Fname, Oasis_Admission_DOB, Oasis_Admission_Course,
								Oasis_Admission_Batch, Oasis_Admission_Mobile, '".$_allottime."' from tbl_oasis_admission
								where Oasis_Admission_LearnerCode='".$_appid."' and Oasis_Admission_Batch='".$_batch."'
					and Oasis_Admission_LearnerStatus='Approved' and Oasis_Admission_Final_Preference='".$_SESSION['User_LoginId']."'";
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			 } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function Updaterscfabatchtime($_appid,$_allottime,$_batch,$_email) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
		
		$_appid = mysqli_real_escape_string($_ObjConnection->Connect(),$_appid);
		$_allottime = mysqli_real_escape_string($_ObjConnection->Connect(),$_allottime);
		$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
		$_email = mysqli_real_escape_string($_ObjConnection->Connect(),$_email);
		
		function OTP($length = 6, $chars = '1234567890') {
			$chars_length = (strlen($chars) - 1);
			$string = $chars{rand(0, $chars_length)};
			for ($i = 1; $i < $length; $i = strlen($string)) {
				$r = $chars{rand(0, $chars_length)};
				if ($r != $string{$i - 1})
					$string .= $r;
			}
			return $string;
		}
        try {
				
              $_chkDuplicate = "Select Batch_Time_lcode From tbl_wcd_allocate_batch_time Where
									Batch_Time_email='" . $_email . "' and Batch_Time_batch='".$_batch."'";
                        $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
				if ($_Response2[0] == Message::NoRecordFound){							
							$_OTP = OTP();
							$_SMS = "OTP for E-mail Verification in RS-CFA Women Course Batch Time Registration is " . $_OTP;
							
				$_InsertQuery = "Insert Into tbl_wcd_email_otp(Wcd_Email_Lcode,Wcd_Email_Email,Wcd_Email_OTP)
                    values('" .  $_appid . "','" .  $_email . "','" .  $_OTP . "')";
            
				$_DuplicateQuery = "Select * From tbl_wcd_email_otp Where Wcd_Email_Email='" . $_email . "' AND
									Wcd_Email_OTP = '" . $_OTP . "'";
				$_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
					if ($_Response[0] == Message::NoRecordFound) {
						$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
							$mail_data = array();
							$Admin_Name='RKCL';
							$mail_data['email'] = $_email;
							$mail_data['admin'] = $Admin_Name;
										
							$mail_data['Msg'] = $_SMS;						
							$subject = "OTP for E-mail Verification";						
							$sendMail = new sendMail();
							$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
							
					}
					else 
					{
						$_Response[0] = Message::DuplicateRecord;
						$_Response[1] = Message::Error;
					}	
				}
						else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
                //$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function Veifyemailotp($_Otp,$_Email,$_Batch,$_Lcode,$_AllotTime) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_Otp);
				$_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email);
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				$_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				$_AllotTime = mysqli_real_escape_string($_ObjConnection->Connect(),$_AllotTime);
				
            $_SelectQuery = "Select * FROM tbl_wcd_email_otp WHERE Wcd_Email_OTP = '" . $_Otp . "' AND Wcd_Email_Status = '0'
							AND Wcd_Email_Email='".$_Email."'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid Verification Details. Please Try Again";
                return;
            } else {
				$_InsertQuery = "insert into tbl_wcd_allocate_batch_time (Batch_Time_lcode, Batch_Time_ITGK_Code, Batch_Time_Learner_Name,
								Batch_Time_Father_Name, Batch_Time_dob, Batch_Time_course, Batch_Time_batch, Batch_Time_mobile, 
								Batch_Time_time,Batch_Time_email) select Oasis_Admission_LearnerCode, Oasis_Admission_Final_Preference,
								Oasis_Admission_Name, Oasis_Admission_Fname, Oasis_Admission_DOB, Oasis_Admission_Course,
								Oasis_Admission_Batch, Oasis_Admission_Mobile, '".$_AllotTime."', '".$_Email."'
								from tbl_oasis_admission
								where Oasis_Admission_LearnerCode='".$_Lcode."' and Oasis_Admission_Batch='".$_Batch."'
					and Oasis_Admission_LearnerStatus='Approved' and Oasis_Admission_Final_Preference='".$_SESSION['User_LoginId']."'";
				$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				
                $_UpdateQuery = "Update tbl_wcd_email_otp set Wcd_Email_Status='1' WHERE  Wcd_Email_OTP = '" . $_Otp . "'
									AND Wcd_Email_Status = '0' AND Wcd_Email_Email='".$_Email."'";
                $_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}