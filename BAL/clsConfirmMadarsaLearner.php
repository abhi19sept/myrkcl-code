<?php

/**
 * Description of clsConfirmMadarsaLearner
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsConfirmMadarsaLearner {

    //put your code here

    public function GetAll($batch, $course, $district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $district = mysqli_real_escape_string($_ObjConnection->Connect(),$district);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

                $_SelectQuery = "SELECT a.Admission_Code,a.Admission_Name,a.Admission_Fname,a.Admission_Gender,"
                        . " a.Admission_Photo,a.Admission_ITGK_Code,a.Admission_Payment_Status,c.Organization_Name,"
                        . " e.Madarsa_Regd_code,e.Madarsa_Name,f.District_Name FROM tbl_admission as a"
                        . " inner join tbl_user_master as b on a.Admission_ITGK_Code=b.User_LoginId inner join "
                        . " tbl_organization_detail as c on b.User_Code=c.Organization_User inner join "
                        . " tbl_admission_madarsa_map as d on a.Admission_LearnerCode=d.Admission_Madarsa_Lcode "
                        . " inner join tbl_madarsa_master as e on d.Admission_Madarsa_Mcode=e.Madarsa_Code  inner join "
                        . " tbl_district_master as f on e.Madarsa_District=f.District_Code "
                        . " WHERE Admission_Batch = '" . $batch . "' AND Admission_Course = '" . $course . "' "
                        . " And Admission_District='" . $district . "' And Admission_Payment_Status='0' order by Admission_ITGK_Code, Admission_Name ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllRpt($batch, $course, $district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $district = mysqli_real_escape_string($_ObjConnection->Connect(),$district);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

                $_SelectQuery = "SELECT a.Admission_Code,a.Admission_Name,a.Admission_Fname,a.Admission_Gender,"
                        . " a.Admission_Photo,a.Admission_ITGK_Code,a.Admission_Payment_Status,c.Organization_Name,"
                        . " e.Madarsa_Regd_code,e.Madarsa_Name,f.District_Name FROM tbl_admission as a"
                        . " inner join tbl_user_master as b on a.Admission_ITGK_Code=b.User_LoginId inner join "
                        . " tbl_organization_detail as c on b.User_Code=c.Organization_User inner join "
                        . " tbl_admission_madarsa_map as d on a.Admission_LearnerCode=d.Admission_Madarsa_Lcode "
                        . " inner join tbl_madarsa_master as e on d.Admission_Madarsa_Mcode=e.Madarsa_Code  inner join "
                        . " tbl_district_master as f on e.Madarsa_District=f.District_Code "
                        . " WHERE Admission_Batch = '" . $batch . "' AND Admission_Course = '" . $course . "' "
                        . " And Admission_District='" . $district . "' order by Admission_ITGK_Code, Admission_Name ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ConfirmMadAdmission($batch, $course, $_AdmissionCode) {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_AdmissionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AdmissionCode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

                $_UpdateQuery = "Update tbl_admission set Admission_Payment_Status='1',"
                        . " Admission_RKCL_Trnid = 'ConfirmWithoutFee', Admission_TranRefNo='ConfirmWithoutFee'"
                        . " Where Admission_Code IN ($_AdmissionCode)";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

}
