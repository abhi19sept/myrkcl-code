<?php

/**
 * Description of clsgetpassword
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$_ObjConnection = new _Connection();
$_Response = array();

class clsexamdetails {

    //put your code here

    public function FillEvent() {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT eventname,eventid From tbl_eligiblelearners as a inner join tbl_exammaster as b 
							on a.eventid=b.Affilate_Event where b.Event_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillExamEvent($examid = 0) {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
            $filter = ($examid) ? " AND a.Event_Id = '" . $examid . "'" : '';
            $_SelectQuery = "SELECT DISTINCT a.Event_Name AS eventname, a.Event_Id AS eventid, b.Exam_Date, b.Affilate_FreshBaches, b.Affilate_ReexamBatches FROM tbl_events a INNER JOIN tbl_exammaster b ON a.Event_Id = b.Affilate_Event WHERE b.Event_Status = 1 $filter ORDER BY a.Event_Id DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function liveSelectedExamSchedule($examid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		$examid = mysqli_real_escape_string($_ObjConnection->Connect(),$examid);
		
        $insertSql = "INSERT IGNORE INTO tbl_eligiblelearners (eventname, eventid, learnercode, learnername, fathername, dob, coursename, batchname, itgkcode, itgkname, itgkdistrict, itgktehsil, remark, status, examdate, Reason, districtId, thesilId, location_rkcl_fixed, location_rkcl_district, location_rkcl, location_vmou_district, location_vmou) SELECT eventname, eventid, learnercode, learnername, fathername, dob, coursename, batchname, itgkcode, itgkname, itgkdistrict, itgktehsil, remark, status, examdate, Reason, districtId, thesilId, location_rkcl_fixed, location_rkcl_district, location_rkcl, location_vmou_district, location_vmou FROM tbl_eligiblelearners_temp WHERE eventid = '" . $examid . "'";
        $_Response = $_ObjConnection->ExecuteQuery($insertSql, Message::InsertStatement);
        $return = (isset($_Response[1]) && $_Response[1] == Message::Success) ? 1 : 0;

        return $return;
    }

    public function getschedule($_id) {

        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
				$_id = mysqli_real_escape_string($_ObjConnection->Connect(),$_id);
				
			if($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '2' || $_SESSION['User_UserRoll'] == '3' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '9' || $_SESSION['User_UserRoll'] == '10' || $_SESSION['User_UserRoll'] == '11') {
				$_SelectQuery = "SELECT count(*) as n, `status`, remark FROM tbl_eligiblelearners_temp WHERE eventid = '" . $_id . "' GROUP BY `status`, remark ORDER BY `status`, remark";
			} else {
                $_SelectQuery = "SELECT el.eventname, el.examdate, IF(el.status = 'Eligible', tm.Tehsil_Name, 'N/A') AS Tehsil_Name, IF(el.status = 'Eligible', dm.District_Name, 'N/A') AS District_Name, el.learnercode, el.learnername, el.fathername, el.dob, el.coursename, el.batchname, el.status, el.remark, el.Reason, el.itgkcode, el.itgkname, el.itgktehsil, el.itgkdistrict FROM tbl_eligiblelearners el INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = el.location_rkcl INNER JOIN tbl_district_master dm ON dm.District_Code = tm.Tehsil_District WHERE el.itgkcode = '" . $_SESSION['User_LoginId'] . "' AND el.eventid = '" . $_id . "' ORDER BY el.itgkcode, el.status, el.learnername";
		    }
		    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function syncRecordsForEligibleLearners($examId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
			
			$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
			
        $drop = "DROP TABLE IF EXISTS tbl_eligiblelearners_temp";
        $_DropResponse = $_ObjConnection->ExecuteQuery($drop, Message::MultipleStatement);

        $create = "CREATE TABLE tbl_eligiblelearners_temp LIKE tbl_eligiblelearners";
        $_CreateResponse = $_ObjConnection->ExecuteQuery($create, Message::MultipleStatement);

        $response = $this->FillExamEvent($examId);
        $return = 0;
        if ($response) {
            $event = mysqli_fetch_array($response[2]);

            $examDate = $event['Exam_Date']; //&examdate=2017-07-16

            $_SelectQuery = "CALL `insert_eligibleLearners_for_exam`(" . $examId . ", '" . $examDate . "')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::MultipleStatement);
            $return = (isset($_Response[1]) && $_Response[1] == Message::Success) ? 1 : 0;
        }

        return $return;
    }

    public function syncRecordsForEligibleLearners_old($examId, $examDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
		$examDate = mysqli_real_escape_string($_ObjConnection->Connect(),$examDate);
		
        $_SelectQuery = "CALL `insert_eligibleLearners_for_exam`(" . $examId . ", '" . $examDate . "')";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::MultipleStatement);

        die('Procedure Called!!');

        $userId = (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 7) ? $_SESSION['User_LoginId'] : '';
        //Insert Re Exam Learners
        $this->insertReExamLearners($examId, $examDate, $userId);

        $allowedFreshBatches = "SELECT Affilate_FreshBaches, Affilate_ReexamBatches FROM tbl_exammaster WHERE Affilate_Event = '" . $examId . "'";
        $_Response = $_ObjConnection->ExecuteQuery($allowedFreshBatches, Message::SelectStatement);
        $allowedBatches = mysqli_fetch_array($_Response[2]);

        //Process to Insert Previous Batches Learners
        $_Response = $this->getPreviousBatches($examId, $allowedBatches['Affilate_ReexamBatches']);
        $filter = (!empty($userId)) ? " AND ta.Admission_ITGK_Code = '" . $userId . "'" : '';
        while ($batch = mysqli_fetch_array($_Response[2])) {
        print $detailsSql = "SELECT '', ( SELECT Event_Name FROM tbl_events WHERE Event_Id = '" . $examId . "' ) AS Event_Name, '" . $examId . "' AS examId , ta.Admission_LearnerCode, ta.Admission_Name, ta.Admission_Fname, ta.Admission_DOB, cm.Course_Name, bm.Batch_Name, ta.Admission_ITGK_Code, od.Organization_Name, dm.District_Name, tm.Tehsil_Name, 'PreviousBatch' AS batchType, 'Eligible' AS status, '". $examDate . "' AS examDate, 'Eligible' AS remark
            FROM tbl_admission ta INNER JOIN tbl_learner_score ls ON ls.Learner_Code = ta.Admission_LearnerCode 
            INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course 
            INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ta.Admission_Batch 
            INNER JOIN tbl_user_master um ON um.User_LoginId = ta.Admission_ITGK_Code 
            INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code 
            INNER JOIN tbl_district_master dm ON dm.District_Code = ta.Admission_District 
            INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = ta.Admission_Tehsil 
            WHERE ta.Admission_Payment_Status = 1 AND ta.Admission_LearnerCode NOT IN (SELECT learnercode FROM tbl_eligiblelearners WHERE eventid = '" . $examId . "') AND ls.Score >= 11 AND ta.Admission_Batch = " . $batch['Admission_Batch'] . " " . $filter;
            die;
            $_AdmResponse = $_ObjConnection->ExecuteQuery($detailsSql, Message::SelectStatement);
            while ($row = mysqli_fetch_array($_AdmResponse[2])) {
                $countOldMapping = "SELECT count(*) AS n FROM tbl_finalexammapping_collective WHERE learnercode = '" . $row['Admission_LearnerCode'] . "'";
                $_MapResponse = $_ObjConnection->ExecuteQuery($countOldMapping, Message::SelectStatement);
                $mapCount = mysqli_fetch_array($_MapResponse[2]);
                if (empty($mapCount['n'])) {
                    $values = "('" . $row['Event_Name'] . "', '" . $row['examId'] . "', '" . $row['Admission_LearnerCode'] . "', '" . $row['Admission_Name'] . "', '" . $row['Admission_Fname'] . "', '" . $row['Admission_DOB'] . "', '" . $row['Course_Name'] . "', '" . $row['Batch_Name'] . "', '" . $row['Admission_ITGK_Code'] . "', '" . $row['Organization_Name'] . "', '" . $row['District_Name'] . "', '" . $row['Tehsil_Name'] . "', '" . $row['batchType'] . "', '" . $row['status'] . "', '" . $row['examDate'] . "', '" . $row['remark'] . "')";
                    print $insertFreshLearners = "INSERT IGNORE INTO tbl_eligiblelearners (eventname, eventid, learnercode, learnername, fathername, dob, coursename, batchname, itgkcode, itgkname, itgkdistrict, itgktehsil, remark, status, examdate, Reason) VALUES " . $values;
                    $_ObjConnection->ExecuteQuery($insertFreshLearners, Message::InsertStatement);
                }
            }
        }

        die('m here!!');

        //Process to Insert Fresh Batches Learners
        $_Response = $this->getPreviousBatches($examId, $allowedBatches['Affilate_FreshBaches']);
        if (mysqli_num_rows($_Response[2])) {
            while ($batch = mysqli_fetch_array($_Response[2])) {
                $this->insertFreshLearners($examId, $examDate, $batch['Admission_Batch'], $userId);
            }
        }
    }

    private function insertReExamLearners($examId, $examDate, $userId = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
		$examDate = mysqli_real_escape_string($_ObjConnection->Connect(),$examDate);
		
        $filter = (!empty($userId)) ? " AND ed.itgkcode = '" . $userId . "'" : '';
        $insertReExamLearners = "INSERT IGNORE INTO tbl_eligiblelearners (
                SELECT '', ev.Event_Name, ed.examid, ed.learnercode, ed.learnername, ed.fathername, ed.dob,
    cm.Course_Name, bm.Batch_Name, ed.itgkcode, od.Organization_Name, dm.District_Name, tm.Tehsil_Name, 'Re-exam', 'Eligible', '". $examDate . "', 'Eligible' FROM examdata ed 
                INNER JOIN tbl_events ev ON ev.Event_Id = ed.examId
                INNER JOIN tbl_course_master cm ON cm.Course_Code = ed.coursename
                INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ed.batchname
                INNER JOIN tbl_user_master um ON um.User_LoginId = ed.itgkcode
                INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code
                INNER JOIN tbl_district_master dm ON dm.District_Code = ed.itgkdistrict
                INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = ed.itgktehsil
                WHERE ed.examid = '" . $examId . "' AND ed.paymentstatus = 1 AND ev.Event_Status = 1 ". $filter . "
        )";
        $_ObjConnection->ExecuteQuery($insertReExamLearners, Message::InsertStatement);
    }

    private function insertFreshLearners($examId, $examDate, $batchId, $userId = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
		$examDate = mysqli_real_escape_string($_ObjConnection->Connect(),$examDate);
		
        $filter = (!empty($userId)) ? " AND ta.Admission_ITGK_Code = '" . $userId . "'" : '';
        $insertFreshBatchLearners = "INSERT IGNORE INTO tbl_eligiblelearners (
            SELECT '', ( SELECT Event_Name FROM tbl_events WHERE Event_Id = '" . $examId . "' ) AS Event_Name, " . $examId . " AS examId , ta.Admission_LearnerCode, ta.Admission_Name, ta.Admission_Fname, ta.Admission_DOB, cm.Course_Name, bm.Batch_Name, ta.Admission_ITGK_Code, od.Organization_Name, dm.District_Name, tm.Tehsil_Name, 'FreshBatch' AS batchType, 'Eligible' AS status, '". $examDate . "' AS examDate, 'Eligible' AS remark
            FROM tbl_admission ta INNER JOIN tbl_learner_score ls ON ls.Learner_Code = ta.Admission_LearnerCode 
            INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course 
            INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ta.Admission_Batch 
            INNER JOIN tbl_user_master um ON um.User_LoginId = ta.Admission_ITGK_Code 
            INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code 
            INNER JOIN tbl_district_master dm ON dm.District_Code = ta.Admission_District 
            INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = ta.Admission_Tehsil 
            WHERE ta.Admission_Batch = " . $batchId . " " . $filter . " AND ta.Admission_Payment_Status = 1 AND ls.Score >= 11
        )";
        $_ObjConnection->ExecuteQuery($insertFreshBatchLearners, Message::InsertStatement);
    }

    private function getPreviousBatches($examId, $batchIds) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
		$batchIds = mysqli_real_escape_string($_ObjConnection->Connect(),$batchIds);
		
        $previousAdmissionBatches = "SELECT distinct Admission_Batch FROM tbl_admission WHERE Admission_Batch IN (0" . $batchIds . ") AND Admission_LearnerCode NOT IN (SELECT learnercode FROM tbl_eligiblelearners WHERE eventid = '" . $examId . "') ORDER BY Admission_Batch";
        
        return $_ObjConnection->ExecuteQuery($previousAdmissionBatches, Message::SelectStatement);
    }

    public function syncLearnerScores($examId, $type) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        /**
            1st Sql section for Fresh batches learners
            2nd section for previous batch learners who applied for reexam
            3rd section for previous event not eligible learners
        */
        if ($type == 'freshbatch') {
            $getExamBatchLearners = "SELECT ta.Admission_Code, ta.Admission_LearnerCode, ta.Admission_ITGK_Code, ls.Score FROM tbl_admission ta LEFT JOIN tbl_learner_score ls ON ls.Learner_Code = ta.Admission_LearnerCode WHERE ta.Admission_Payment_Status = 1 AND ta.Admission_LearnerCode LIKE('%789') AND FIND_IN_SET(ta.Admission_Batch, (SELECT Affilate_FreshBaches FROM tbl_exammaster WHERE Affilate_Event = '" . $examId . "')) > 0 HAVING (Score IS NULL OR Score < 11)";
        } else if ($type == 'reexam') {
            $getExamBatchLearners = "SELECT ta.Admission_Code, ta.Admission_LearnerCode, ta.Admission_ITGK_Code, ls.Score FROM examdata ed INNER JOIN tbl_admission ta ON ed.learnercode = ta.Admission_LearnerCode LEFT JOIN tbl_learner_score ls ON ls.Learner_Code = ta.Admission_LearnerCode WHERE ed.examid = '" . $examId . "' AND ta.Admission_Payment_Status = 1 AND ta.Admission_LearnerCode LIKE('%789') AND FIND_IN_SET(ta.Admission_Batch, (SELECT Affilate_ReexamBatches FROM tbl_exammaster WHERE  Affilate_Event = '" . $examId . "')) > 0 HAVING (Score IS NULL OR Score < 11)";
        } else if ($type == 'previous') {
            $getExamBatchLearners = "SELECT ta.Admission_Code, ta.Admission_LearnerCode, ta.Admission_ITGK_Code, ls.Score FROM tbl_eligiblelearners ed INNER JOIN tbl_admission ta ON ed.learnercode = ta.Admission_LearnerCode LEFT JOIN tbl_learner_score ls ON ls.Learner_Code = ta.Admission_LearnerCode WHERE ed.eventid = '" . ($examId - 1) . "' AND ed.status = 'Not Eligible' AND ta.Admission_Payment_Status = 1 AND ta.Admission_LearnerCode LIKE('%789') AND FIND_IN_SET(ta.Admission_Batch, (SELECT Affilate_ReexamBatches FROM tbl_exammaster WHERE  Affilate_Event = '" . $examId . "')) > 0 HAVING (Score IS NULL OR Score < 11)";
        } else {
            die('Invalid request!!');
        }
        echo $getExamBatchLearners;
        die;
        $result = $_ObjConnection->ExecuteQuery($getExamBatchLearners, Message::SelectStatement);
        $this->fetchScoreAndSync($result);
    }

    public function syncBatchScores($batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $getBatchLearners = "SELECT ta.Admission_Code, ta.Admission_LearnerCode, ta.Admission_ITGK_Code FROM tbl_admission ta WHERE ta.Admission_Payment_Status = 1 AND ta.Admission_LearnerCode LIKE('%789') AND ta.Admission_Batch = '" . $batch . "'";

        $result = $_ObjConnection->ExecuteQuery($getBatchLearners, Message::SelectStatement);
        $this->fetchScoreAndSync($result);
    }

    private function fetchScoreAndSync($result) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $res = [];
        if (mysqli_num_rows($result[2])) {
            while ($data = mysqli_fetch_assoc($result[2])) {
                $score = $this->fetchLearnerScore($data['Admission_LearnerCode']);
                //$res[$data['Admission_LearnerCode']] = $score;
                $score_per = (($score / 30) * 100);
                if ($score && $score > 0) {
                    $insertScore = "INSERT INTO tbl_learner_score (Admission_Code, Learner_Code, Score, Score_Per, ITGK_code, AddEditTime) VALUES ('" . $data['Admission_Code'] . "', '" . $data['Admission_LearnerCode'] . "', '" . $score . "', '" . $score_per . "', '" . $data['Admission_ITGK_Code'] . "', '" . date("Y-m-d h:i:s") . "') ON DUPLICATE KEY UPDATE tbl_learner_score.Score = IF(tbl_learner_score.Score > " . $score . ", tbl_learner_score.Score, " . $score . "), tbl_learner_score.Score_Per = IF(tbl_learner_score.Score_Per > " . $score_per . ", tbl_learner_score.Score_Per, " . $score_per . ")";
                    $_Response = $_ObjConnection->ExecuteQuery($insertScore, Message::InsertStatement);
                }
            }
            //print_r($res);
        }
    }


    function fetchLearnerScore($learnerCode) {
        $score = 0;
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        $url = 'http://server2/ScoreService/ScoreService.svc/GetScore/' . $learnerCode;
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_VERBOSE => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Simple cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources

        $info = curl_getinfo($curl);

        curl_close($curl);

        ob_start();
        print($resp);
        $output = ob_get_contents();
        ob_end_clean();

        if(!empty($output)) {
            $dom = new DOMDocument;
            @$dom->loadHTML($output);
            foreach ($dom->getElementsByTagName('string') as $node) {
                //echo $learnerCode . ' in ' . $node->nodeValue."\n"; 
                $score = (isset($node->nodeValue) && is_numeric($node->nodeValue)) ? $node->nodeValue : 0;
            }
        }
        //echo $learnerCode . ' out ' . $score."\n"; 

        return $score;
    }

    public function GetAllBatch() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT CONCAT(bm.Batch_Name,' (', cm.Course_Name ,')') AS Batch_Name, bm.Batch_Code FROM tbl_batch_master bm INNER JOIN tbl_course_master cm ON cm.Course_Code = bm.Course_Code ORDER BY bm.Batch_Code DESC, cm.Course_Name DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

}
