<?php
/**
 * Description of clsCategoryMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCategoryMaster {
    //put your code here
    
     public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Category_Code,Category_Name,"
                    . "Status_Name From tbl_category_master as a inner join tbl_status_master as b "
                    . "on a.Category_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Category_Code)
    {   //echo $_Category_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Category_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Category_Code);
            $_SelectQuery = "Select Category_Code,Category_Name,Category_Status"
                    . " From tbl_category_master Where Category_Code='" . $_Category_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Category_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Category_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Category_Code);
            $_DeleteQuery = "Delete From tbl_category_master Where Category_Code='" . $_Category_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_CategoryName,$_CategoryStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CategoryName = mysqli_real_escape_string($_ObjConnection->Connect(),$_CategoryName);
            
            $_InsertQuery = "Insert Into tbl_category_master(Category_Code,Category_Name,"
                    . "Category_Status) "
                    . "Select Case When Max(Category_Code) Is Null Then 1 Else Max(Category_Code)+1 End as Category_Code,"
                    . "'" . $_CategoryName . "' as Category_Name,"
                    . "'" . $_CategoryStatus . "' as Category_Status"
                    . " From tbl_category_master";
            $_DuplicateQuery = "Select * From tbl_category_master Where Category_Name='" . $_CategoryName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_CategoryCode,$_CategoryName,$_CategoryStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CategoryName = mysqli_real_escape_string($_ObjConnection->Connect(),$_CategoryName);
            $_CategoryCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CategoryCode);
            $_UpdateQuery = "Update tbl_category_master set Category_Name='" . $_CategoryName . "',"
                    . "Category_Status='" . $_CategoryStatus . "' Where Category_Code='" . $_CategoryCode . "'";
            $_DuplicateQuery = "Select * From tbl_category_master Where Category_Name='" . $_CategoryName . "' "
                    . "and Category_Code <> '" . $_CategoryCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
