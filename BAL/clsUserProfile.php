<?php


/**
 * Description of clsUserProfile
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUserProfile {
    //put your code here
    public function Add($_UserInitial,$_UserFname,$_UserMname,$_UserLname,$_UserGender,$_UserTagName,$_UserParentName,$_UserOccupation,$_UserMobile,$_UserSTDcode,$_UserPhone,$_UserWebsite,$_UserFBID,$_UserTWID,$_UserHouseno,$_UserStreet,$_UserRoad,$_UserLandmark,$_UserArea,$_UserPhoto,$_UsePANno,$_UserAadharNo,$_UserKYC) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
$_mobile=$_SESSION['User_MobileNo'];
            $_SMS = "Dear  Applicant, Your Owner Detail Submit has been submitted to RKCL. Kindly complete entry of all other details for reaching next stage of NCR.";
            
            $_InsertQuery = "Insert Into tbl_userprofile(UserProfile_Code, UserProfile_User, UserProfile_Initial, UserProfile_FirstName, UserProfile_MiddleName, UserProfile_LastName, UserProfile_HouseNo, UserProfile_Street, UserProfile_Road, UserProfile_Landmark, UserProfile_Area, UserProfile_Image, UserProfile_Gender, UserProfile_TagName, UserProfile_ParentName, UserProfile_Occupation, UserProfile_PanNo, UserProfile_AadharNo, UserProfile_Mobile, UserProfile_STDCode, UserProfile_PhoneNo, UserProfile_Website, UserProfile_FaceBookId, UserProfile_TwitterId, UserProfile_KycScan) Select Case When Max(UserProfile_Code) Is Null Then 1 Else Max(UserProfile_Code)+1 End as UserProfile_Code, '1' as UserProfile_User,'" .$_UserInitial. "' as UserProfile_Initial,'" . $_UserFname . "' as UserProfile_FirstName,'" .$_UserMname. "' as UserProfile_MiddleName,'" .$_UserLname. "' as UserProfile_LastName,'" .$_UserHouseno. "' as UserProfile_HouseNo,'" .$_UserStreet. "' as UserProfile_Street,'" .$_UserRoad. "' as UserProfile_Road,'" .$_UserLandmark. "' as UserProfile_Landmark,'" .$_UserArea. "' as UserProfile_Area,'" .$_UserPhoto. "' as UserProfile_Image,'" .$_UserGender. "' as UserProfile_Gender,'" .$_UserTagName. "' as UserProfile_TagName,'" .$_UserParentName. "' as UserProfile_ParentName,'" .$_UserOccupation. "' as UserProfile_Occupation,'" .$_UsePANno. "' as UserProfile_PanNo,'" .$_UserAadharNo. "' as UserProfile_AadharNo,'" .$_UserMobile. "' as UserProfile_Mobile,'" .$_UserSTDcode. "' as UserProfile_STDCode,'" .$_UserPhone. "' as UserProfile_PhoneNo,'" .$_UserWebsite. "' as UserProfile_Website,'" .$_UserFBID. "' as UserProfile_FaceBookId,'" .$_UserTWID. "' as UserProfile_TwitterId,'" .$_UserKYC. "' as UserProfile_KycScan From tbl_userprofile";         
            $_DuplicateQuery = "Select * From tbl_userprofile Where UserProfile_User='" . $_SESSION['User_Code'] . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_mobile, $_SMS);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }  
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        //print_r($_Response);
        return $_Response;
    }
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
             $_SelectQuery = "Select * From tbl_userprofile where UserProfile_User ='". $_SESSION['User_Code']."'" ;
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }  
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
