<?php
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsthoughts {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_thoughts";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	public function Get() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			mysqli_set_charset('utf8');
            $_SelectQuery = "Select * From tbl_thoughts";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
	
	
	
    
    public function GetDatabyCode($_Status_Code)
    {
		
		
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			mysqli_set_charset('utf8');
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_SelectQuery = "Select thoughts From tbl_thoughts Where id='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function DeleteRecord($_values)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_values = mysqli_real_escape_string($_ObjConnection->Connect(),$_values);
				
           $_DeleteQuery = "Delete From tbl_thoughts Where id='" . $_values . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Add($_thoughts,$_thoughtimage) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
		mysqli_set_charset('utf8');
			$_thoughts = mysqli_real_escape_string($_ObjConnection->Connect(),$_thoughts);
			
		$_fileBirth1   =	$_thoughtimage.'_thought' .'.jpg';
		$_SelectQuery = "Select thoughts From tbl_thoughts";
        $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
	    $num_rows = mysqli_num_rows($_Response[2]);
		if($num_rows==0)
	    {
			
			$_InsertQuery = "Insert Into tbl_thoughts(thoughts,thoughtsimage) VALUES ('" . $_thoughts . "','" . $_fileBirth1 . "')";
            $_DuplicateQuery = "Select * From tbl_thoughts Where thoughts='" . $_thoughts . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
		}
		else
		{
			 $_UpdateQuery = "Update tbl_thoughts set thoughts='" . $_thoughts . "',thoughtsimage='" . $_fileBirth1 . "'";
			 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			
		}
		
		
		
		
            
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_code,$_thoughts) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			mysqli_set_charset('utf8');
			
			$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
			$_thoughts = mysqli_real_escape_string($_ObjConnection->Connect(),$_thoughts);
			
            $_UpdateQuery = "Update tbl_thoughts set 
						thoughts='" . $_thoughts . "'
                     Where id='" . $_code . "'";
            $_DuplicateQuery = "Select * From tbl_thoughts Where id='" . $_thoughts . "' and "
                    . "id <> '" . $_code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
    

}

?>