<?php
require 'DAL/classconnection.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsgetotpforpassword {

    public function GetOtpPassword($loginid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $loginid = mysqli_real_escape_string($_ObjConnection->Connect(),$loginid);
            $_SelectQuery = "select User_Code,User_MobileNo,User_UserRoll from tbl_user_master where User_LoginId='".$loginid."' and User_UserRoll in ('7','17')";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if($_Response1[0] == "Success"){
                $row = mysqli_fetch_array($_Response1[2]);
                $mobile = $row["User_MobileNo"];
                $userroll = $row["User_UserRoll"];
                if($userroll == '17' && $_SESSION['User_UserRoll'] == '4'){
                    $_SelectQuery = "select AO_Code,AO_Mobile,AO_OTP from tbl_ao_register where AO_Mobile='".$mobile."' and AO_Status='0'
                                        order by AO_Code DESC LIMIT 1";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement); 
                }else if($_SESSION['User_UserRoll'] == '28' || $_SESSION['User_UserRoll'] == '1'){
                    $_SelectQuery = "select AO_Code,AO_Mobile,AO_OTP from tbl_ao_register where AO_Mobile='".$mobile."' and AO_Status='0'
                                        order by AO_Code DESC LIMIT 1";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }else{
                    $_Response[0] = "No Record Found.";
                }
            }else{
                $_Response[0] = "You are not authorized to get OTP.";
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


}
