<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsVillageMaster
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsVillageMaster {
    //put your code here
    
    public function GetAll($country) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
       //echo $country;
        try {
				$country = mysqli_real_escape_string($_ObjConnection->Connect(),$country);
				
            if ($country && is_array($country)) {
                $filter = '';
                foreach($country as $field => $val) {
                    $filter .= ' AND ' . $field . ' ' . $val;
                }
                $_SelectQuery = "Select Village_Code,Village_Name,GP_Name,"
                    . "Status_Name From tbl_village_master as a inner join tbl_gram_panchayat as b "
                    . "on a.Village_GP=b.GP_Code inner join tbl_status_master as c "
                    . "on a.Village_Status=c.Status_Code" . $filter;
            } elseif($country) {
                 $_SelectQuery = "Select Village_Code,Village_Name,GP_Name,"
                    . "Status_Name From tbl_village_master as a inner join tbl_gram_panchayat as b "
                    . "on a.Village_GP=b.GP_Code inner join tbl_status_master as c "
                    . "on a.Village_Status=c.Status_Code and a.Village_GP='" . $country . "'";
            } else {
                $_SelectQuery = "Select Village_Code,Village_Name,GP_Name,"
                    . "Status_Name From tbl_village_master as a inner join tbl_gram_panchayat as b "
                    . "on a.Village_GP=b.GP_Code inner join tbl_status_master as c "
                    . "on a.Village_Status=c.Status_Code"; 
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Village_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Village_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Village_Code);
				
            $_SelectQuery = "Select Village_Code,Village_Name,Village_GP,Village_Status"
                    . " From tbl_village_master Where Village_Code='" . $_Village_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Village_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Village_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Village_Code);
				
            $_DeleteQuery = "Delete From tbl_village_master Where Village_Code='" . $_Village_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_VillageName,$_VillageParent,$_VillageStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_VillageName = mysqli_real_escape_string($_ObjConnection->Connect(),$_VillageName);
				$_VillageParent = mysqli_real_escape_string($_ObjConnection->Connect(),$_VillageParent);
				$_VillageStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_VillageStatus);
				
            $_InsertQuery = "Insert Into tbl_village_master(Village_Code,Village_Name,"
                    . "Village_GP,Village_Status) "
                    . "Select Case When Max(Village_Code) Is Null Then 1 Else Max(Village_Code)+1 End as Village_Code,"
                    . "'" . $_VillageName . "' as Village_Name,"
                    . "'" . $_VillageParent . "' as Village_GP,'" . $_VillageStatus . "' as Village_Status"
                    . " From tbl_village_master";
            $_DuplicateQuery = "Select * From tbl_village_master Where Village_Name='" . $_VillageName . "' AND Village_GP = '" . $_VillageParent . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_VillageCode,$_VillageName,$_VillageParent,$_VillageStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_VillageCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_VillageCode);
				$_VillageName = mysqli_real_escape_string($_ObjConnection->Connect(),$_VillageName);
				$_VillageParent = mysqli_real_escape_string($_ObjConnection->Connect(),$_VillageParent);
				$_VillageStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_VillageStatus);
				
            $_UpdateQuery = "Update tbl_village_master set Village_Name='" . $_VillageName . "',"
                    . "Village_GP='" . $_VillageParent . "',"
                    . "Village_Status='" . $_VillageStatus . "' Where Village_Code='" . $_VillageCode . "'";
            $_DuplicateQuery = "Select * From tbl_village_master Where Village_Name='" . $_VillageName . "' "
                    . " AND Village_GP = '" . $_VillageParent . "' AND Village_Code <> '" . $_VillageCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
