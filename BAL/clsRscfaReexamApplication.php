<?php

/**
 * Description of clsRscfaReexamApplication
 *
 * @author Abhishek
 */

require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRscfaReexamApplication {

    //put your code here   
public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master WHERE Course_Code='24'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
				$_SelectQueryGetEvent1 = "SELECT Event_Name FROM tbl_event_management WHERE Event_Category = '15' AND
										Event_Name = '21' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
				if ($_getEvent['Event_Name'] == '21'){
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
								order by Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
				else {
                    //echo "Invalid User Input";
                    return;
                }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function GetExamEventManage($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
//            $_SelectQuery = "Select a.Course_Code b.Course_Name FROM tbl_exammaster as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.a.Course_Code WHERE a.";
            $_SelectQuery = "Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " WHERE NOW() >= Affilate_Astart AND NOW() <= Affilate_End";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    

    public function GetAll($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_Batch = '" . $batch . "' AND Admission_Course = '" . $course . "' AND Admission_Payment_Status = '1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /* Added By: Sunil for single learner */

    public function GetAllRscfaLearner($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                
                $courseName = "RS-CFA";
				$_LoginRole = $_SESSION['User_UserRoll'];
                $_ITGK_Code = $_SESSION['User_LoginId'];
				
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
					if ($_LoginRole == '7') {
						$_SelectQuery = "select id,lcode,name,fname,dob,study_cen,percentage,course,batch,rscfa_reexam_lcode
							from tbl_result_rscfa as a left join tbl_rscfa_reexam_application as b
							on a.lcode=b.rscfa_reexam_lcode where course='".$course."' and batch='".$batch."' and
							study_cen='".$_SESSION['User_LoginId']."' and result='FAIL'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					}               
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ApplyForReexamApplication($lcode, $batch, $course){
	global $_ObjConnection;
        $_ObjConnection->Connect();
	try {
          if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_ITGK_Code = $_SESSION['User_LoginId'];
				$_LoginRole = $_SESSION['User_UserRoll'];
				
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
			if ($_LoginRole == '7') {	
				$_InsertQuery = "insert into tbl_rscfa_reexam_application 	
								(rscfa_reexam_lcode,rscfa_reexam_lname,rscfa_reexam_fname,rscfa_reexam_dob,coursename,batchname,itgkcode)
								select lcode,name,fname,dob,course,batch,study_cen from tbl_result_rscfa
								where lcode='".$lcode."' and course='".$course."' and batch='".$batch."'";
				$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			}
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
}

    
}
