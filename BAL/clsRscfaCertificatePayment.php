<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsRscfaCertificatePayment
 *
 *  author Mayank
 */
require 'DAL/classconnection.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRscfaCertificatePayment extends paymentFunctions {

    //put your code here
public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code='5'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
								order by Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function GetAll($batch, $course, $paymode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK = $_SESSION['User_LoginId'];
					$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
					$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
					$paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$paymode);
					
                $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_Category = '14' AND 
				Event_Payment = '" . $paymode . "' AND Event_Name = '18' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
                if ($_getEvent['Event_Payment'] == '1') {
                    $_SelectQuery = "SELECT * FROM tbl_apply_rscfa_certificate WHERE apply_rscfa_cert_batch = '" . $batch . "' 
									AND apply_rscfa_cert_course = '" . $course . "' And apply_rscfa_cert_itgk='" . $_ITGK . "'
									And apply_rscfa_cert_payment_status='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } 
				else {
                    //echo "Invalid User Input";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
				
            $_SelectQuery = "Select RscfaCertificate_Fee_Amount FROM tbl_apply_rscfa_certificate_fee_master WHERE
							RscfaCertificate_Fee_Category = 'RS-CFA Certificate' and RscfaCertificate_Fee_Status='Active'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function ShowRscfaCertificateEvent($_Batch, $_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				
             $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON
							a.Event_Payment = b.payment_id WHERE a.Event_Category = '14' AND Event_Name='18' AND
							NOW() >= Event_Startdate AND NOW() <= Event_Enddate AND Event_Payment != '2'";
           
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }



    public function AddPayTran($postValues, $productinfo) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $postValues['amounts'], $postValues['AdmissionCodes'], $postValues['ddlCourse'], 0, $postValues['ddlBatch']);
            } else {
                session_destroy();
            ?>
                <script> window.location.href = "logout.php";</script> 
            <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }
	
	public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}