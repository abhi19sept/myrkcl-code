<?php


/**
 * Description of clsDesignationMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsDesignationMaster {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Designation_Code,Designation_Name,"
                    . "Status_Name From tbl_designation_master as a inner join tbl_status_master as b "
                    . "on a.Designation_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Designation_Code)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Designation_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Designation_Code);
				
            $_SelectQuery = "Select Designation_Code,Designation_Name,Designation_Status"
                    . " From tbl_designation_master Where Designation_Code='" . $_Designation_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Designation_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Designation_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Designation_Code);
				
            $_DeleteQuery = "Delete From tbl_designation_master Where Designation_Code='" . $_Designation_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_DesignationName,$_DesignationStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DesignationName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DesignationName);
				$_DesignationStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_DesignationStatus);
				
            $_InsertQuery = "Insert Into tbl_designation_master(Designation_Code,Designation_Name,"
                    . "Designation_Status) "
                    . "Select Case When Max(Designation_Code) Is Null Then 1 Else Max(Designation_Code)+1 End as Designation_Code,"
                    . "'" . $_DesignationName . "' as Designation_Name,"
                    . "'" . $_DesignationStatus . "' as Designation_Status"
                    . " From tbl_designation_master";
            $_DuplicateQuery = "Select * From tbl_designation_master Where Designation_Name='" . $_DesignationName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_DesignationCode,$_DesignationName,$_DesignationStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DesignationCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DesignationCode);
				$_DesignationName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DesignationName);
				
            $_UpdateQuery = "Update tbl_designation_master set Designation_Name='" . $_DesignationName . "',"
                    . "Designation_Status='" . $_DesignationStatus . "' Where Designation_Code='" . $_DesignationCode . "'";
            $_DuplicateQuery = "Select * From tbl_designation_master Where Designation_Name='" . $_DesignationName . "' "
                    . "and Designation_Code <> '" . $_DesignationCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
