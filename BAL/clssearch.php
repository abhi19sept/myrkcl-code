<?php
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();
class clssearch {
    public function Search($request) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$request = mysqli_real_escape_string($_ObjConnection->Connect(),$request);
					
                if ($request != 'undefined') {
                    $sear = "Function_Name like '%" . $request . "%' and ";
                } else {
                    $sear = '';
                }
                $_SelectQuery = "SELECT Function_URL,Function_Name FROM tbl_function_master as a  inner Join 
                                tbl_permission_master as b on a.Function_Code=b.Permission_Function
                                inner join tbl_parent_function_master as c on a.Function_Parent=c.Parent_Function_Code
                                WHERE  " . $sear . "  b.Permission_UserRoll='" . $_SESSION['User_UserRoll'] . "'  ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
?>