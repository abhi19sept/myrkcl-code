<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsEoiConfig
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsEoiConfig {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_eoi_master";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Code)
    {
        //echo $_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_SelectQuery = "Select a.*, b.Course_Name From tbl_eoi_master as a INNER JOIN tbl_course_master as b on
								a.EOI_Course = b.Course_Code Where EOI_Code ='" . $_Code . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_FeeType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_FeeType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_FeeType_Code);
				
            $_DeleteQuery = "Delete From tbl_feetype_master Where FeeType_Code='" . $_FeeType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_EoiName,$_Course,$_SDate,$_EDate,$_PFees,$_CFees,$_TncDoc,$newfilenamepng)
	{	global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EoiName = mysqli_real_escape_string($_ObjConnection->Connect(),$_EoiName);
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				
            $_TncDoc = $_TncDoc.'_tnc.jpg';
				 $_InsertQuery = "Insert Into tbl_eoi_master(EOI_Code,EOI_Name,"
                    . "EOI_Course,EOI_StartDate,EOI_EndDate,EOI_ProcessingFee,EOI_CourseFee,EOI_TNC) "
                    . "Select Case When Max(EOI_Code) Is Null Then 1 Else Max(EOI_Code)+1 End as EOI_Code,"
                    . "'" . $_EoiName . "' as EOI_Name,'" . $_Course . "' as EOI_Course,"
                    . "'" . $_SDate . "' as EOI_StartDate,'" . $_EDate . "' as EOI_EndDate,"
                    . "'" . $_PFees . "' as EOI_ProcessingFee,'" . $_CFees . "' as EOI_CourseFee,"
                    . "'" . $newfilenamepng . "' as EOI_TNC"
                    . " From tbl_eoi_master";
					//print_r($_InsertQuery);
				$_DuplicateQuery = "Select * From tbl_eoi_master Where EOI_Name='" . $_EoiName . "' AND EOI_Course = '" . $_Course . "' ";
				$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
				if($_Response[0]==Message::NoRecordFound)
				{
					$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);					
					//$_lastid = $_ObjConnection->last_id();				
				}
				else {
						$_Response[0] = Message::DuplicateRecord;
						$_Response[1] = Message::Error;
					 }
			} catch (Exception $_e) {
										$_Response[0] = $_e->getTraceAsString();
										$_Response[1] = Message::Error;
            
									}
        return $_Response;
    }
	public function GetMaxCode()
	{
		global $_ObjConnection;
		$_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select Max(EOI_Code) as Code from tbl_eoi_master";
			//echo $_SelectQuery;
			$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);					
			
		}
		catch (Exception $_e) {
										$_Response[0] = $_e->getTraceAsString();
										$_Response[1] = Message::Error;
            
									}
        return $_Response;
	}
	 public function Addlist($rows)
	{	global $_ObjConnection;
        $_ObjConnection->Connect();
        try {								
			$_Insert = "Insert Into tbl_eoi_centerlist (EOI_Code, EOI_ECL, EOI_Year,EOI_Status_Flag,EOI_DateTime,EOI_Filename,timestamp,IsNewCRecord) "
                                . "Values " . $rows . " ON DUPLICATE KEY UPDATE EOI_Code = VALUES(EOI_Code), EOI_ECL = VALUES(EOI_ECL)";
			$_Response=$_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
		catch (Exception $_e) {
										$_Response[0] = $_e->getTraceAsString();
										$_Response[1] = Message::Error;
            
									}
        return $_Response;
	}
    
    public function Update($_Code,$_EoiName,$_Course,$_SDate,$_EDate,$_PFees,$_CFees,$_ECL,$_TncDoc) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_EoiName = mysqli_real_escape_string($_ObjConnection->Connect(),$_EoiName);
				
            $_UpdateQuery = "Update tbl_eoi_master set EOI_Name='" . $_EoiName . "',"
                    . "FeeType_Status='" . $_FunctionStatus . "' Where EOI_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_eoi_master Where EOI_Name='" . $_EoiName . "' "
                    . "and EOI_Code <> '" . $_Code . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
