<?php
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();
class clssitemap {
    public function GetRootMenu() {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_SelectQuery = "Select RootName,RootCode From vw_userrolewiserootmenu Where
						UserRole='" . $_SESSION['User_UserRoll'] . "' Order By DisplayOrder";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /* Get Parent Menu */

    public function GetParentMenu($_RootMenu) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_RootMenu = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootMenu);
					
                $_SelectQuery = "Select ParentCode,ParentName From vw_userrolewiseparentmenu Where
					UserRole='" . $_SESSION['User_UserRoll'] . "' and RootMenu='" . $_RootMenu . "' Order By DisplayOrder";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /* get child Menu */

    public function GetChildMenu($_Parent) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_Parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent);
					
                $_SelectQuery = "Select FunctionName,FunctionURL From vw_userrolewisefunction where "
                        . "UserRole='" . $_SESSION['User_UserRoll'] . "' and Parent='" . $_Parent . "' Order by Display";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
?>