<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionModify {
    //put your code here
    
    public function GetAll($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
         $_SelectQuery = "Select Admission_Code,Admission_LearnerCode,Admission_Mobile, Admission_Name, Admission_Batch,
							Admission_Fname, Admission_DOB, Admission_Photo, Admission_Sign, Admission_Fee, Admission_Payment_Status,
							IsNewRecord FROM tbl_admission WHERE  Admission_Batch = '" . $batch . "' AND
							Admission_Course = '" . $course . "' AND Admission_ITGK_Code = '".$_SESSION['User_LoginId']."'";  
				//$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
				//WHERE b.Course_Name = '" . $course . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

    public function GetAllForModify($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
         $_SelectQuery = "Select bm.Batch_Name, ad.Admission_Code, ad.Admission_LearnerCode, ad.Admission_Mobile, ad.Admission_Name,ad.IsNewRecord, ad.Admission_Course, ad.Admission_Batch, ad.Admission_Fname, ad.Admission_DOB, ad.Admission_Photo, ad.Admission_Sign, ad.Admission_Fee, ad.Admission_Payment_Status FROM tbl_admission ad INNER JOIN tbl_event_management em ON em.Event_Batch = ad.Admission_Batch 
         inner join tbl_batch_master as bm on ad.Admission_Batch=bm.Batch_Code WHERE ad.Admission_Batch = '" . $batch . "' AND ad.Admission_Course = '" . $course . "' AND ad.Admission_ITGK_Code = '".$_SESSION['User_LoginId']."' AND CURDATE() >= em.Event_Startdate AND CURDATE() <= em.Event_Enddate AND em.Event_Name='2'";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
                //WHERE b.Course_Name = '" . $course . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	 public function DeleteRecord($_Admission_Code , $_batch)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_Response = [];
        try {
            $_Admission_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Admission_Code);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_SelectQuery = "Select ad.Admission_Code FROM tbl_admission ad INNER JOIN tbl_event_management em ON em.Event_Batch = ad.Admission_Batch WHERE ad.Admission_Batch = '" . $_batch . "'  AND ad.Admission_ITGK_Code = '" . $_ITGK_Code . "' AND ad.Admission_Code = '" . $_Admission_Code . "' AND CURDATE() >= em.Event_Startdate AND CURDATE() <= em.Event_Enddate AND em.Event_Name='2' LIMIT 1";
            $_LResponse = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_LResponse[2])) {
				$_Row = mysqli_fetch_array($_LResponse[2]);
                if ($_Row['Admission_Code'] == $_Admission_Code) {
    				$_DeleteQuery = "Delete From tbl_admission Where Admission_Code= " . $_Admission_Code . " AND Admission_ITGK_Code = '" . $_ITGK_Code . "' AND Admission_Batch = '" . $_batch . "'";
    				$_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);

    				$_UpdateQuery = "Update tbl_intake_master set Intake_Consumed=Intake_Consumed - 1,"
                          . "Intake_Available=Intake_Available + 1 Where Intake_Center = '" .$_ITGK_Code  . "' AND Intake_Batch = " . $_batch . " "; 
    				$_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                } else {
                    $_Response[0] = 'Unable to process.';
                }
	        } else {
                $_Response[0] = 'Unable to process.';
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
}
