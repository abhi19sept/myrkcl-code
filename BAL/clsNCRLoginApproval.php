<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNCRLoginApproval
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNCRLoginApproval {
    //put your code here
 
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code WHERE a.Org_Visit='Yes' AND Org_Role = '22' AND Org_Login_Approval_Status = 'Pending' AND Org_RspLoginId='" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function GetOrgDatabyCode($_OrgCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
				
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code WHERE Organization_Code = '" . $_OrgCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function SPCreateCenter($_OrgCode, $_DistrictCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
				$_DistrictCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DistrictCode);
				
            $_SelectQuery = "Select * FROM tbl_org_master WHERE Organization_Code = '" . $_OrgCode . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response1[2]);
			$_OrgAck = $_Row['Org_Ack'];
            $_Email = $_Row['Org_Email'];
            $_mobile = $_Row['Org_Mobile'];
            $_roll = $_Row['Org_Role'];
            $_RspLoginId = $_Row['Org_RspLoginId'];
            $_Status = '1';

            $_OrgName = $_Row['Organization_Name'];
            $_OrgRegno = $_Row['Organization_RegistrationNo'];
            $_OrgEstDate = $_Row['Organization_FoundedDate'];
            $_OrgType = $_Row['Organization_Type'];
            $_doctype = $_Row['Organization_DocType'];
            $_photoname = $_Row['Organization_ScanDoc'];
            
            $_aadhar = $_Row['Organization_UID'];
            $_addproof = $_Row['Organization_AddProof'];
            $_appform = $_Row['Organization_AppForm'];
            $_docid = $_Row['Organization_TypeDocId'];
            
            $_State = $_Row['Organization_State'];
            $_Region = $_Row['Organization_Region'];
            $_District = $_Row['Organization_District'];
            $_Tehsil = $_Row['Organization_Tehsil'];
            $_Landmark = $_Row['Organization_Landmark'];
            $_Road = $_Row['Organization_Road'];
            $_Street = $_Row['Organization_Street'];
            $_HouseNo = $_Row['Organization_HouseNo'];
            $_OrgCountry = $_Row['Organization_Country'];
            
            $_PAN = $_Row['Org_PAN'];
            $_AreaType = $_Row['Org_AreaType'];
            $_Mohalla = $_Row['Org_Mohalla'];
            $_WardNo = $_Row['Org_WardNo'];
            $_Police = $_Row['Org_Police'];
            $_Municipal = $_Row['Org_Municipal'];
            $_Village = $_Row['Org_Village'];
            $_GramPanchayat = $_Row['Org_Gram'];
            $_Panchayat = $_Row['Org_Panchayat'];
            $_PinCode = $_Row['Org_PinCode'];
            
            $_Rsp_Code=$_Row['Org_RspCode'];
//            
//            $_SelectQuery11 = "Select a.User_Code,b.*,c.Org_Type_Name,d.District_Name,e.Tehsil_Name FROM tbl_user_master as a INNER JOIN tbl_organization_detail as b"
//                    . " ON a.User_Code=b.Organization_User INNER JOIN tbl_org_type_master as c"
//                    . " ON b.Organization_Type=c.Org_Type_Code INNER JOIN tbl_district_master as d"
//                    . " ON b.Organization_District=d.District_Code INNER JOIN tbl_tehsil_master as e"
//                    . " ON b.Organization_Tehsil=e.Tehsil_Code WHERE User_LoginId = '" . $_RspLoginId . "'";
//            $_Response11 = $_ObjConnection->ExecuteQuery($_SelectQuery11, Message::SelectStatement);
//            $_Row1 = mysqli_fetch_array($_Response11[2]);
//            $_Code = $_Row1['User_Code'];
//            $_Rspname = $_Row1['Organization_Name'];
//            $_Rspregno = $_Row1['Organization_RegistrationNo'];
//            $_Rspestdate = $_Row1['Organization_FoundedDate'];
//            $_Rsptype = $_Row1['Org_Type_Name'];
//            $_Rspdistrict = $_Row1['District_Name'];
//            $_Rsptehsil = $_Row1['Tehsil_Name'];
//            $_Rspstreet = $_Row1['Organization_Street'];
//            $_Rsproad = $_Row1['Organization_Road'];
//            
//                $_InsertQuery11 = "Insert Into tbl_rspitgk_mapping(Rspitgk_Code,Rspitgk_ItgkCode,Rspitgk_Rspcode,Rspitgk_Rspname,"
//                        . "Rspitgk_Rspregno,Rspitgk_Rspestdate,Rspitgk_Rsporgtype,Rspitgk_Rspdistrict,"
//                        . "Rspitgk_Rsptehsil,Rspitgk_Rspstreet,Rspitgk_Rsproad,Rspitgk_Date,Rspitgk_Status,Rspitgk_UserType) "
//                        . "Select Case When Max(Rspitgk_Code) Is Null Then 1 Else Max(Rspitgk_Code)+1 End as Rspitgk_Code,"
//                        . "'" . $_USERNAME . "' as Rspitgk_ItgkCode,'" . $_Rsp_Code . "' as Rspitgk_Rspcode,'" . $_Rspname . "' as Rspitgk_Rspname,"
//                        . "'" . $_Rspregno . "' as Rspitgk_Rspregno,'" . $_Rspestdate . "' as Rspitgk_Rspestdate,"
//                        . "'" . $_Rsptype . "' as Rspitgk_Rsporgtype,'" . $_Rspdistrict . "' as Rspitgk_Rspdistrict,"
//                        . "'" . $_Rsptehsil . "' as Rspitgk_Rsptehsil,'" . $_Rspstreet . "' as Rspitgk_Rspstreet,"
//                        . "'" . $_Rsproad . "' as Rspitgk_Rsproad,'" . $_Date . "' as Rspitgk_Date,'Approved' as Rspitgk_Status,'New_Center' as Rspitgk_UserType"
//                        . " From tbl_rspitgk_mapping";
//                $_Response61 = $_ObjConnection->ExecuteQuery($_InsertQuery11, Message::InsertStatement);
//           
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
                    

            function PASSWORD($length = 6, $chars = 'abcdefghijklmnopqrstuvwxyz1234567890') {
                $chars_length = (strlen($chars) - 1);
                $string = $chars{rand(0, $chars_length)};
                for ($i = 1; $i < $length; $i = strlen($string)) {
                    $r = $chars{rand(0, $chars_length)};
                    if ($r != $string{$i - 1})
                        $string .= $r;
                }
                return $string;
            }
             if ($_Row['Org_Role'] == '22') {
                $_SelectQuery2 = "Select CenterCode_LoginId FROM tbl_center_code WHERE CenterCode_Status = '0' AND CenterCode_District = '". $_DistrictCode ."' ORDER BY CenterCode_Id ASC LIMIT 1";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                $_Row1 = mysqli_fetch_array($_Response2[2]);
                $_USERNAME = $_Row1['CenterCode_LoginId'];
                $_UpdateQuery1 = "Update tbl_center_code set CenterCode_Status = '1'"
                        . " Where CenterCode_LoginId='" . $_USERNAME . "'";
                $_Response3 = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            }
            $_PASSWORD = PASSWORD();

            $_SMS = "Your Login details for submitted application in MYRKCL is User Name: " . $_USERNAME . " Password: " . $_PASSWORD . "";

            $_InsertQuery = "Insert Into tbl_user_master(User_Code,User_EmailId,User_MobileNo,User_LoginId,User_Password,"
                    . " User_UserRoll,User_ParentId,User_Rsp,User_Status,User_CreatedDate,User_PaperLess,User_Ack)"
                    . "Select Case When Max(User_Code) Is Null Then 1 Else Max(User_Code)+1 End as User_Code,"
                    . "'" . $_Email . "' as User_EmailId,'" . $_mobile . "' as User_MobileNo,"
                    . "'" . $_USERNAME . "' as User_LoginId,'" . $_PASSWORD . "' as User_Password,"
                    . "'" . $_roll . "' as User_UserRoll,'1' as User_ParentId,'" . $_Rsp_Code . "' as User_Rsp,"
                    . "'" . $_Status . "' as User_Status,'" . $_Date . "' as User_CreatedDate,'Yes' as User_PaperLess,'" . $_OrgAck . "' as User_Ack From tbl_user_master";
            //echo $_InsertQuery;
            $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            $_SelectQuery1 = "Select User_Code FROM tbl_user_master WHERE User_EmailId = '" . $_Email . "' AND User_MobileNo = '" . $_mobile . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row2 = mysqli_fetch_array($_Response5[2]);
            $_usercode = $_Row2['User_Code'];

            $_InsertQuery1 = "Insert Into  tbl_organization_detail(Organization_Code,Organization_User,Organization_Name,"
                    . "Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,"
                    . "Organization_DocType,Organization_ScanDoc,Organization_UID,Organization_AddProof,Organization_AppForm,Organization_TypeDocId,"
                    . "Organization_State,Organization_Region,Organization_District,"
                    . "Organization_Tehsil,Organization_Landmark,Organization_Address,Organization_Street,"
                    . "Organization_HouseNo,Organization_Country,Organization_PAN,Organization_AreaType,Organization_Mohalla,"
                    . "Organization_WardNo,Organization_Police,Organization_Municipal,Organization_Village,Organization_Gram,"
                    . "Organization_Panchayat,Organization_PinCode)"
                    . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                    . "'" . $_usercode . "' as Organization_User,'" . $_OrgName . "' as Organization_Name,'" . $_OrgRegno . "' as Organization_RegistrationNo,"
                    . "'" . $_OrgEstDate . "' as Organization_FoundedDate,  '" . $_OrgType . "' as Organization_Type,"
                    . "'" . $_doctype . "' as Organization_DocType,'" . $_photoname . "' as Organization_ScanDoc,"
                    . "'" . $_aadhar . "' as Organization_UID,'" . $_addproof . "' as Organization_AddProof,"
                    . "'" . $_appform . "' as Organization_AppForm,'" . $_docid . "' as Organization_TypeDocId,"
                    . "'" . $_State . "' as Organization_State,'" . $_Region . "' as Organization_Region,'" . $_District . "' as Organization_District,"
                    . "'" . $_Tehsil . "' as Organization_Tehsil,"
                    . "'" . $_Landmark . "' as Organization_Landmark,'" . $_Road . "' as Organization_Address,"
                    . "'" . $_Street . "' as Organization_Street,'" . $_HouseNo . "' as Organization_HouseNo,"
                    . "'" . $_OrgCountry . "' as Organization_Country,"
                    . "'" . $_PAN . "' as Organization_PAN,'" . $_AreaType . "' as Organization_AreaType,"
                    . "'" . $_Mohalla . "' as Organization_Mohalla,'" . $_WardNo . "' as Organization_WardNo,"
                    . "'" . $_Police . "' as Organization_Police,'" . $_Municipal . "' as Organization_Municipal,"
                    . "'" . $_Village . "' as Organization_Village,'" . $_GramPanchayat . "' as Organization_Gram,"
                    . "'" . $_Panchayat . "' as Organization_Panchayat,'" . $_PinCode . "' as Organization_PinCode"
                    . " From  tbl_organization_detail";
            $_Response6 = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);


            $_UpdateQuery = "Update tbl_org_master set Org_Login_Approval_Status='Approved', Org_NCR_Login_Approval_Date='" . $_Date . "'"
                    . " Where Organization_Code='" . $_OrgCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            SendSMS($_mobile, $_SMS);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
