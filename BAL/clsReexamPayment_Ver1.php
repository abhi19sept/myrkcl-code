<?php

/**
 * Description of clsReexamPayment
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsReexamPayment {

    //put your code here

    public function GetExamEvent($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $_SelectQuery = "Select a.Course_Code b.Course_Name FROM tbl_exammaster as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.a.Course_Code WHERE a.";
            $_SelectQuery = "Select DISTINCT a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " INNER JOIN tbl_event_management as c ON b.Event_Id = c.Event_ReexamEvent "
                    . " WHERE c.Event_Name='7'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetExamEventDDConfirm($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $_SelectQuery = "Select a.Course_Code b.Course_Name FROM tbl_exammaster as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.a.Course_Code WHERE a.";
            $_SelectQuery = "Select DISTINCT a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " WHERE a.Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCenterCodeRexamdd($_examevent, $_coursecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select reexxam_TranRefNo,itgkcode FROM examdata WHERE examid = '" . $_examevent . "' AND coursename = '" . $_coursecode . "' AND paymentstatus = '8' AND learnertype = 'reexam'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll($paymode, $course, $event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK = $_SESSION['User_LoginId'];
                $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_ReexamEvent = '" . $event . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '7' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);

                $filter = ($_getEvent['Event_Payment'] == '2') ? "AND coursename = '" . $course . "'" : "AND examid = '" . $event . "'" ;
                $filter .= (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 19) ? " AND a.learnercode='" . $_SESSION['User_LearnerCode'] . "'" : '';
                $_SelectQuery = "SELECT a.*, b.Batch_Name, tr.ReExam_fee as fee FROM examdata as a INNER JOIN tbl_batch_master as b ON a.batchname = b.Batch_Code INNER JOIN tbl_exammaster ex ON ex.Affilate_Event = a.examid INNER JOIN tbl_reexamfeemaster tr ON tr.ReExam_Id = ex.Affilate_Fee WHERE itgkcode = '" . $_SESSION['User_LoginId'] . "' AND learnertype = 'reexam' AND paymentstatus = '0' " . $filter . " ORDER BY a.learnername";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                if (empty($paymode) || $_Response[0] == Message::NoRecordFound) {
                    //echo "Invalid User Input";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllDdDataReexam($_refno) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_reexam_ddpay WHERE dd_Transaction_Txtid = '" . $_refno . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    function GetLearnerMobile($_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Admission_Mobile FROM tbl_admission AS a INNER JOIN examdata AS b ON a.Admission_LearnerCode = b.learnercode WHERE b.reexxam_TranRefNo IN ($_AdmissionCode)";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateReexamDDPaymentConfirm($course, $examevent, $MobileNo3) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $Transactionid = $_SESSION['LearnerAdmission'];
            // print_r($_SESSION);
            $_UpdateQuery = "Update tbl_reexam_ddpay set dd_Transaction_Status = 'Success'"
                    . " Where dd_Transaction_Txtid IN ($Transactionid) ";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_UpdateQuery1 = "Update tbl_reexam_transaction set Reexam_Transaction_Status = 'Success'"
                    . " Where Reexam_Transaction_Txtid IN ($Transactionid) ";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);

            $_UpdateQuery2 = "Update examdata set paymentstatus = '1'"
                    . " Where reexxam_TranRefNo IN ($Transactionid) ";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);

            $_SMS = "Dear Learner Your Re-Exam Payment for '$examevent' has been Confirmed. Please contact to your ITGK for further instruction.";
            $Mobile = $MobileNo3;
            SendSMS($Mobile, $_SMS);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetEventName($_eventid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Event_Name FROM tbl_events WHERE Event_Id = '" . $_eventid . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddPayTran($event, $course, $amount, $examCodes, $txtGenerateId, $txtGeneratePayUId) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_RKCL_Id = $txtGenerateId . '_RKCLtranid';

                $_ITGK_Code = $_SESSION['User_LoginId'];
                //$_Fname = $_SESSION['User_Code'];
                $Lcount = count($examCodes);
				$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "'";
                $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                if(! mysqli_num_rows($response[2])) {
					
                $_InsertQuery = "INSERT IGNORE INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray, Pay_Tran_LCount, Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_Course, Pay_Tran_ReexamEvent, Pay_Tran_PG_Trnid) VALUES ('" . $_ITGK_Code . "', '" . $_RKCL_Id . "', '" . implode(',', $examCodes) . "', '" . $Lcount . "', '" . ($amount * $Lcount) . "', 'PaymentInProcess', 'ReexamPayment', '" . $course . "', '" . $event . "', '" . $txtGeneratePayUId . "')";

                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

				} else {
					$_UpdateQuery = "UPDATE tbl_payment_transaction SET 
                        Pay_Tran_ITGK = '" . $_ITGK_Code . "', 
                        Pay_Tran_RKCL_Trnid = '" . $_RKCL_Id . "', 
                        Pay_Tran_AdmissionArray = '" . implode(',', $examCodes) . "',
                        Pay_Tran_LCount = '" . $Lcount . "', 
                        Pay_Tran_Amount = '" . ($amount * $Lcount) . "', 
                        Pay_Tran_ProdInfo = 'ReexamPayment',
                        Pay_Tran_Course = '" . $course . "',
                        Pay_Tran_ReexamEvent = '" . $event . "'
                    WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "' AND Pay_Tran_Status = 'PaymentInProcess'";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				}
                $_SelectQuery_PaymentTrnx = "SELECT Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "' AND Pay_Tran_ProdInfo = 'ReexamPayment' AND Pay_Tran_Status = 'PaymentInProcess'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery_PaymentTrnx, Message::SelectStatement);
                if (mysqli_num_rows($_Response1[2])) {
                    $getStoredTrnxId = mysqli_fetch_array($_Response1[2]);
                    $_UpdateQuery = "UPDATE examdata SET reexam_TranRefNo = '" . $getStoredTrnxId['Pay_Tran_PG_Trnid'] . "', reexam_RKCL_Trnid = '" . $_RKCL_Id . "' WHERE examdata_code IN (" . implode(',', $examCodes) . ")";
                    $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $return = $amount * $Lcount;
			    }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $return;
    }
	
	public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
