<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFacultyResultReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsFacultyResultReport {
    //put your code here
    
     public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "select c.ITGK_Code,c.Staff_Name, sum(c.attempt1) as a1, sum(c.attempt2) as a2 from(
                            select a.ITGK_Code,b.Staff_Name,
                            CASE WHEN a.ResExam_attempt = '1' THEN a.ResExam_obtainmarks End as attempt1,
                            Case WHEN a.ResExam_attempt = '2' THEN a.ResExam_obtainmarks END as attempt2
                            from tbl_facultyexamresultreport as a inner join 
                            tbl_staff_detail as b on concat(b.Staff_User,'_',b.Staff_Code)=a.ResExam_learner) as c group by Staff_Name";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "select c.ITGK_Code,c.Staff_Name, sum(c.attempt1) as a1, sum(c.attempt2) as a2 from(
                            select a.ITGK_Code,b.Staff_Name,
                            CASE WHEN a.ResExam_attempt = '1' THEN a.ResExam_obtainmarks End as attempt1,
                            Case WHEN a.ResExam_attempt = '2' THEN a.ResExam_obtainmarks END as attempt2
                            from tbl_facultyexamresultreport as a inner join 
                            tbl_staff_detail as b on concat(b.Staff_User,'_',b.Staff_Code)=a.ResExam_learner) as c group by Staff_Name";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}
