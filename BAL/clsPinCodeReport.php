<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPinCodeReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPinCodeReport {
    //put your code here
    
         public function GetAll($pincode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$pincode = mysqli_real_escape_string($_ObjConnection->Connect(),$pincode);
				
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "SELECT a.Organization_User,a.Organization_Code, a.Organization_Name, a.Organization_Address, 
                            d.User_LoginId, d.User_EmailId, d.User_MobileNo, b.District_Name, c.Tehsil_Name, a.Organization_PinCode
                            from tbl_organization_detail as a INNER JOIN tbl_district_master as b on a.Organization_District=b.District_Code
                            INNER JOIN tbl_tehsil_master as c on a.Organization_Tehsil=c.Tehsil_Code
                            INNER JOIN tbl_user_master as d on a.Organization_User=d.User_Code
                            where a.Organization_PinCode='" . $pincode . "' AND d.User_UserRoll='7'";
            
            } elseif($_SESSION['User_UserRoll'] == 23){
            $_SelectQuery = "SELECT a.Organization_User,a.Organization_Code, a.Organization_Name, a.Organization_Address, 
                            d.User_LoginId, d.User_EmailId, d.User_MobileNo, b.District_Name, c.Tehsil_Name, a.Organization_PinCode
                            from tbl_organization_detail as a INNER JOIN tbl_district_master as b on a.Organization_District=b.District_Code
                            INNER JOIN tbl_tehsil_master as c on a.Organization_Tehsil=c.Tehsil_Code
                            INNER JOIN tbl_user_master as d on a.Organization_User=d.User_Code
                            where a.Organization_PinCode='" . $pincode . "' AND d.User_UserRoll='7' AND a.Organization_District='" . $_SESSION['Organization_District'] . "'";
                
            }
            else {
                $_SelectQuery = "";

            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
            
             
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
