<?php

/**
 * Description of clsReexamApplication
 *
 * @author Deep
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsExamSLAReport {

    private function getDbConnect() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        return $_ObjConnection;
    }

    public function GetExamEvent() {
        $_ObjConnection = $this->getDbConnect();
        try {
            $_SelectQuery = "SELECT a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id GROUP BY a.Affilate_Event ORDER BY a.Affilate_Event DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function getItgkExamSlaReport($examid) {
        $_ObjConnection = $this->getDbConnect();
        try {
				$examid = mysqli_real_escape_string($_ObjConnection->Connect(),$examid);
				
            $filter = ($_SESSION['User_UserRoll'] == 7) ? "AND ss.itgkcode = '" . $_SESSION['User_LoginId'] . "'" : (($_SESSION['User_UserRoll'] == 14) ? "AND um.User_Rsp = '" . $_SESSION['User_Code'] . "'" : '');
            $_SelectQuery = "SELECT ss.*, a.vmou_result, b.Event_Name FROM tbl_examevent_itgk_sla_status ss INNER JOIN tbl_exammaster as a ON ss.examid = a.Affilate_Event INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id INNER JOIN tbl_user_master um ON um.User_LoginId = ss.itgkcode WHERE ss.examid = '" . $examid . "' $filter ORDER BY ss.itgkcode ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;    
    }

    public function getSPExamSlaReport($examid) {
        $_ObjConnection = $this->getDbConnect();
        try {
            $filter = ($_SESSION['User_UserRoll'] == 14) ? "AND um.User_Rsp = '" . $_SESSION['User_Code'] . "'" : '';
			
			$examid = mysqli_real_escape_string($_ObjConnection->Connect(),$examid);
			
            $_SelectQuery = "SELECT um.User_Rsp, od.Organization_Name, a.vmou_result, b.Event_Name, SUM(ss.totalAppear) AS totalAppear, SUM(ss.absentCount) AS absentCount, SUM(ss.eventitgklearners) AS eventitgklearners, SUM(ss.passCount) AS passCount, SUM(ss.failCount) AS failCount, COUNT(ss.itgkcode) AS faileditgks
                FROM tbl_examevent_itgk_sla_status ss INNER JOIN tbl_exammaster as a ON ss.examid = a.Affilate_Event INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id INNER JOIN tbl_user_master um ON um.User_LoginId = ss.itgkcode INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Rsp WHERE ss.examid = '" . $examid . "' AND ss.panelty LIKE ('yes') $filter GROUP BY um.User_Rsp ORDER BY od.Organization_Name ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response; 
    }

    public function getExamEventSummary($examid) {
        $_ObjConnection = $this->getDbConnect();
        try {
            $filter = ($_SESSION['User_UserRoll'] == 14) ? "AND um.User_Rsp = '" . $_SESSION['User_Code'] . "'" : '';
			
			$examid = mysqli_real_escape_string($_ObjConnection->Connect(),$examid);
			
            //event name(id), Exam date, Total ITGK, Total Appered, Total Pass, Total Fail, Total Absent, vmou_result
            $_SelectQuery = "SELECT ss.examid, b.Event_Name, a.Exam_Date, count(ss.itgkcode) AS Total_ITGK, SUM(ss.eventitgklearners) AS TotalAppear, SUM(ss.totalAppear) AS permissionLetters, SUM(ss.passCount) AS passCount, SUM(ss.failCount) AS failCount, SUM(ss.absentCount) AS absentCount, a.vmou_result 
                FROM tbl_examevent_itgk_sla_status ss INNER JOIN tbl_exammaster as a ON ss.examid = a.Affilate_Event INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id WHERE ss.examid = '" . $examid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function getlearnerresults($examid) {
        $_ObjConnection = $this->getDbConnect();
        try {
				$examid = mysqli_real_escape_string($_ObjConnection->Connect(),$examid);
				
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 4) {

                $_SelectQuery = "SELECT rs.scol_no, rs.study_cen, rs.name, rs.net_marks, rs.tot_marks, rs.percentage, rs.result FROM tbl_result rs INNER JOIN tbl_user_master as um ON rs.study_cen = um.User_LoginId  WHERE rs.exameventnameID = '" . $examid . "' ORDER BY rs.study_cen";
            } 
            elseif ($_SESSION['User_UserRoll'] == 14) {
                
                $_SelectQuery = "SELECT rs.scol_no, rs.study_cen, rs.name, rs.net_marks, rs.tot_marks, rs.percentage, rs.result FROM tbl_result rs INNER JOIN tbl_user_master as um ON rs.study_cen = um.User_LoginId  WHERE rs.exameventnameID = '" . $examid . "' AND um.User_Rsp = '".$_SESSION['User_Code']."' ORDER BY rs.study_cen";
            } 
			  elseif ($_SESSION['User_UserRoll'] == 7) {
                
                $_SelectQuery = "SELECT rs.scol_no, rs.study_cen, rs.name, rs.net_marks, rs.tot_marks, rs.percentage, rs.result FROM tbl_result rs INNER JOIN tbl_user_master as um ON rs.study_cen = um.User_LoginId  WHERE rs.exameventnameID = '" . $examid . "' AND um.User_Code = '".$_SESSION['User_Code']."' ORDER BY rs.study_cen";
            } 
			
			$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

}
