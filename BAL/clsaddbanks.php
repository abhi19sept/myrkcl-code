<?php


/**
 * Description of clsHRDetail
 *
 * @author yogi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response1 = array();

class clsaddbanks {
    
	 public function GetAll() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_rajbank_master limit 10000" ;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
          //  print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Add($_Bankname,$_txtifsc,$_txtMicr,$_txtBranch,$_txtAddress,$_txtContact,$_txtCity,$_txtDistrict,$_txtState,$_ddlStatus) 
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			
			 	 $_InsertQuery = "Insert Into tbl_rajbank_master(bankid,bankname,ifsccode,micrcode,branchname,bankaddress,bankcontact,bankcity,bankdistrict,bankstate,active) Select Case When Max(bankid) Is Null Then 1 Else Max(bankid)+1 End as bankid,'" .$_Bankname. "' as bankname,'" .$_txtifsc. "' as ifsccode,'" .$_txtMicr. "' as micrcode,'" . $_txtBranch. "' as branchname,'" .$_txtAddress. "' as bankaddress,'" .$_txtContact. "' as bankcontact,'" .$_txtCity. "' as bankcity,'" .$_txtDistrict. "' as bankdistrict,'" .$_txtState. "' as bankstate,'" .$_ddlStatus. "' as active From tbl_rajbank_master";
				
		   
		        $_DuplicateQuery = "Select * From tbl_rajbank_master Where ifsccode='".$_txtifsc."'";
				$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
         
			if($_Response[0]==Message::NoRecordFound)
            {
               $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			
            }
            else 
			{
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } 
		catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	

    public function DeleteRecord($_bankid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_DeleteQuery = "Delete From tbl_rajbank_master Where bankid='" . $_bankid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	
	public function GetDistrict() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT District_Name from tbl_district_master where District_StateCode='29'";
            
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
          //  print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function GetBanks() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT bankname from tbl_rajbank_master";
            
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
          //  print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
}
