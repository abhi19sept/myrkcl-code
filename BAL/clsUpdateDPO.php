<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsUpdateDPO
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUpdateDPO {
    //put your code here
    
    public function GetAllDPO() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_user_master where User_UserRoll='23'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetDatabyCode($_DpoUserCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_DpoUserCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DpoUserCode);
					
            $_SelectQuery = "SELECT A.*,B.* FROM tbl_biomatric_enrollments AS A RIGHT JOIN tbl_user_master AS B 
                            ON A.userId=B.User_Code WHERE B.User_Code  = '" . $_DpoUserCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
      public function UpdateDPO($_DPOCode,$_Mobile,$_Email) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
			
			$_DPOCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DPOCode);
			$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
			$_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email);
			
            $_SMSDPO = "Dear DPO, Your details has been UPDATED on MYRKCL. Kindly use FORGOT PASSWORD link available on MYRKCL login page to generate password.";
            $_UpdateQuery = "Update tbl_user_master set User_MobileNo='" . $_Mobile . "', "
                    . "User_EmailId='" . $_Email . "'"
                    . "Where User_Code='" . $_DPOCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            SendSMS($_Mobile, $_SMSDPO);
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
      public function DeRegister($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
            
			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
            $_InsertQuery = "INSERT INTO tbl_biomatric_enrollments_log
                            (id,userId,addedby,fullname,employee_type,
                            gender,mobile,email_id,photo,BioMatric_Pic,
                            status,BioMatric_NFIQ,BioMatric_Quality,BioMatric_ISO_Template,BioMatric_ISO_Image,
                            BioMatric_Raw_Data,BioMatric_WSQ_Image,addtime,logtime)
                            SELECT id,userId,addedby,fullname,employee_type,
                            gender,mobile,email_id,photo,BioMatric_Pic,
                            status,BioMatric_NFIQ,BioMatric_Quality,BioMatric_ISO_Template,BioMatric_ISO_Image,
                            BioMatric_Raw_Data,BioMatric_WSQ_Image,addtime,'" . $_Date . "' FROM tbl_biomatric_enrollments
                            WHERE userId='" . $_Code . "'";  
            $_ResponseI=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
            $query = "DELETE FROM tbl_biomatric_enrollments WHERE userId = '" . $_Code . "'";
            
            if($_ResponseI[0] == 'Successfully Inserted'){
            $_Response=$_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
            } else {
                echo "Something went wrong. Please check data again.";
                return;
            }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
}
