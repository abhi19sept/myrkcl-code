<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAllotedExamLocationReport.php
 *
 * @author Yogendra soni
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAllotedExamLocationReport 
{
    //put your code here
  
   public function GetAll($event) 
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
            $event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
		    $_SelectQuery = "SELECT el.itgkcode, dm.District_Name AS EXAM_DIST, th.Tehsil_Name AS EXAM_THESIL,
						IF (el.location_rkcl = ch.choice1, 'Choice1', (IF (el.location_rkcl = ch.choice2, 'Choice2',
						(IF (el.location_rkcl = ch.choice3, 'Choice3', (IF (el.location_rkcl = ch.choice4, 'Choice4',
						(IF (el.location_rkcl = ch.choice5, 'Choice5', 'No Choice'))))))))) AS Mapped_Choice, COUNT(*) as n
						FROM tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master th ON th.Tehsil_Code = el.location_rkcl 
						INNER JOIN tbl_district_master dm ON dm.District_Code = th.Tehsil_District
						LEFT JOIN tbl_examchoicemaster_temp ch ON el.itgkcode = ch.centercode
						WHERE eventid='".$event."' AND el.status = 'Eligible' GROUP BY el.itgkcode, th.Tehsil_Name";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}