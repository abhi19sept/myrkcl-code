<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsClassTypeMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsClassTypeMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select ClassType_Code,ClassType_Name,"
                    . "Status_Name From tbl_classtype_master as a inner join tbl_status_master as b "
                    . "on a.ClassType_Status"
                    . "=b.Status_Code";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_ClassType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ClassType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ClassType_Code);
            $_SelectQuery = "Select ClassType_Code,ClassType_Name,"
                    . "ClassType_Status From tbl_classtype_master Where ClassType_Code='" . $_ClassType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_ClassType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ClassType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ClassType_Code);
            $_DeleteQuery = "Delete From tbl_classtype_master Where ClassType_Code='" . $_ClassType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ClassTypeName,$_ClassTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ClassTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ClassTypeName);
            $_InsertQuery = "Insert Into tbl_classtype_master(ClassType_Code,ClassType_Name,"
                    . "ClassType_Status) "
                    . "Select Case When Max(ClassType_Code) Is Null Then 1 Else Max(ClassType_Code)+1 End as ClassType_Code,"
                    . "'" . $_ClassTypeName . "' as ClassType_Name,'" . $_ClassTypeStatus . "' as ClassType_Status"
                    . " From tbl_classtype_master";
            $_DuplicateQuery = "Select * From tbl_classtype_master Where ClassType_Name='" . $_ClassTypeName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_ClassTypeCode,$_ClassTypeName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ClassTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ClassTypeCode);
            $_ClassTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ClassTypeName);
            $_UpdateQuery = "Update tbl_classtype_master set ClassType_Name='" . $_ClassTypeName . "',"
                    . "ClassType_Status='" . $_FunctionStatus . "' Where ClassType_Code='" . $_ClassTypeCode . "'";
            $_DuplicateQuery = "Select * From tbl_classtype_master Where ClassType_Name='" . $_ClassTypeName . "' "
                    . "and ClassType_Code <> '" . $_ClassTypeCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
