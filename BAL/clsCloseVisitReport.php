<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsCloseVisitReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');

$_ObjConnection = new _Connection();
$_Response = array();

class clsCloseVisitReport {
    //put your code here
    
    public function GetDetails() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
                
                  $_SelectQuery = "select a.*,b.*,c.fullname from tbl_visit_requests as a 
                      inner join vw_itgkname_distict_rsp as b on a.itgk_code=b.ITGKCODE
                      left join tbl_biomatric_enrollments as c on a.spid=c.id 
                      where a.status=2";  
               
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
