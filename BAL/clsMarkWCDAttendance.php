
<?php

/**
 * Description of clsMarkWCDAttendance
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
//require 'DAL/upload_ftp_doc.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsMarkWCDAttendance {

    //put your code here
    public function GetAllLearner($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(), $batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(), $course);

        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_SelectQuery = "";
                
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "Select a.Admission_Code, a.Admission_LearnerCode, Admission_Name, Admission_Batch, Admission_Fname,"
                        . " Admission_DOB, Admission_Photo, Admission_Sign FROM tbl_admission as a "
                        . "WHERE Admission_Batch = '" . $batch . "' AND  Admission_Course = '".$course."'  AND "
                        . " Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
 public function markWCDAttendance($lcode, $batch, $course, $date, $atype, $classmode){
          global $_ObjConnection;
        $_ObjConnection->Connect();
                $lcode = mysqli_real_escape_string($_ObjConnection->Connect(), $lcode);
                $batch = mysqli_real_escape_string($_ObjConnection->Connect(), $batch);
                $course = mysqli_real_escape_string($_ObjConnection->Connect(), $course);
                $course = mysqli_real_escape_string($_ObjConnection->Connect(), $course);
                $atype = mysqli_real_escape_string($_ObjConnection->Connect(), $atype);
                $classmode = mysqli_real_escape_string($_ObjConnection->Connect(), $classmode);

        try {
            if(isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){

            $chkduplicate = "select * from tbl_learner_attendance_wcd where Attendance_ITGK_Code='" . $_SESSION['User_LoginId'] . "' and Attendance_Admission_Code='" . $lcode . "' and DATE(Attendance_In_Time)= '" . $date . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($chkduplicate, Message::SelectStatement);
                if ($_Response2[0] == Message::NoRecordFound) {
                    $_InsertQuery = "INSERT INTO  tbl_learner_attendance_wcd (Attendance_ITGK_Code, Attendance_Admission_Code, Attendance_Batch,Attendance_Course,Attendance_In_Time,Attendance_Type,Attendance_Mode) 
                VALUES ('" . $_SESSION['User_LoginId'] . "','" . $lcode . "','" . $batch . "','" . $course . "','" . $date . "','" . $atype . "','" . $classmode . "')";



                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                }
                 else {
                                $_Response[0] = Message::DuplicateRecord;
                                $_Response[1] = Message::Error;
                            }
             } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

                    }        
        }
        catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
     }
 public function CheckAttendance($lcode, $batch, $attendate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
            $lcode = mysqli_real_escape_string($_ObjConnection->Connect(), $lcode);
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(), $batch);
            $attendate = mysqli_real_escape_string($_ObjConnection->Connect(), $attendate);

        try {
			// date_default_timezone_set('Asia/Kolkata');
   //          $_Date = date("Y-m-d");
            $_SelectQuery = "Select Attendance_Admission_Code ,Attendance_Type,Attendance_In_Time FROM tbl_learner_attendance_wcd WHERE Attendance_Admission_Code='".$lcode."' and Attendance_Batch = '" . $batch . "' and Attendance_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' and Attendance_In_Time='".$attendate."' limit 1";
            

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
public function CheckCaptureEvent($coursecode,$batchcode)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_SelectQuery = "SELECT Event_LearnerAttendance from tbl_event_management where Event_Course='".$coursecode."'
				 and Event_Batch='".$batchcode."' and Event_Category='10' and Event_Name='13' and Event_LearnerAttendance='1'
				 AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
public function FILLEventBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $_Course . "' AND Now() >= Event_Startdate AND
			Now() <= Event_Enddate AND Event_Name='13'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function ADD($_classdur, $_ddlBatch, $_ddlCourse, $_classDate, $_txtclassurl, $_filename){
          global $_ObjConnection;
        $_ObjConnection->Connect();
                $_classdur = mysqli_real_escape_string($_ObjConnection->Connect(), $_classdur);
                $_ddlBatch = mysqli_real_escape_string($_ObjConnection->Connect(), $_ddlBatch);
                $_ddlCourse = mysqli_real_escape_string($_ObjConnection->Connect(), $_ddlCourse);
                $_classDate = mysqli_real_escape_string($_ObjConnection->Connect(), $_classDate);
                $_txtclassurl = mysqli_real_escape_string($_ObjConnection->Connect(), $_txtclassurl);
                $_filename = mysqli_real_escape_string($_ObjConnection->Connect(), $_filename);
        try {

            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_InsertQuery = "INSERT INTO  tbl_wcd_onlineclass_details (OnlineClass_Course, OnlineClass_Batch, OnlineClass_Date, OnlineClass_URL, OnlineClass_Duration, 
    OnlineClass_ITGK, OnlineClass_Photo) 
        VALUES ('" . $_ddlCourse . "','" . $_ddlBatch . "','" . $_classDate."','" . $_txtclassurl . "','" . $_classdur . "','" . $_SESSION['User_LoginId']. "','" . $_filename . "')";



            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

            }
                    
        }
        catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
     }
    public function CheckOnlineClass($batch, $attendate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
            
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(), $batch);
            $attendate = mysqli_real_escape_string($_ObjConnection->Connect(), $attendate);

        try {
            // date_default_timezone_set('Asia/Kolkata');
   //          $_Date = date("Y-m-d");
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select OnlineClass_ITGK FROM tbl_wcd_onlineclass_details WHERE  OnlineClass_Batch = '" . $batch . "' and OnlineClass_ITGK = '" . $_SESSION['User_LoginId'] . "' and OnlineClass_Date='".$attendate."' limit 1";
            

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
public function Chkslottime($course,$batch,$slotime)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
             
                $_SelectQuery = "select * from tbl_wcd_onlineclass_details where OnlineClass_Course='".$course."' and OnlineClass_Batch = '".$batch."' and OnlineClass_Date = '".$slotime."' and OnlineClass_ITGK='".$_SESSION['User_LoginId']."'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}