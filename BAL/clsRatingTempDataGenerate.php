<?php

/**
 * Description of clsRatingTempDataGenerate
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsRatingTempDataGenerate {

    //put your code here

	public function GetRatingParameters() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {         
            $_SelectQuery = "select ranking_weightage_id,ranking_weightage_parameter from tbl_ranking_weightage
								where ranking_weightage_status='Active' order by ranking_weightage_id";           
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetBatchforRating() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_SelectQuery = "select Batch_Code,Batch_Name From tbl_batch_master where Course_Code='1' and 
								Batch_Code>='205' order by Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetExamEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select * From tbl_events where Event_Status = '1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
	
    public function Add($_Batch, $_Parameter, $_Event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
				if($_Parameter=='1'){
					$deletequery="TRUNCATE TABLE tbl_rating_admission_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);
					
					$_SelectQuery = "INSERT INTO
							tbl_rating_admission_temp(rating_admission_Itgk_code,rating_admission_count,rating_admission_batch)
							select Admission_ITGK_Code,count(Admission_LearnerCode) as admission_count,Admission_Batch
							From tbl_admission where Admission_Batch='".$_Batch."' and Admission_Payment_Status='1'
							group by Admission_ITGK_Code";					
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);						
				}
				else if($_Parameter=='2'){
					$deletequery="TRUNCATE TABLE tbl_rating_enrollment_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);
					
					$_SelectQuery = "INSERT INTO
							tbl_rating_enrollment_temp(rating_enrollment_Itgk_code,rating_enrollment_count,
							rating_admission_count,rating_enrollment_batch)
							Select Admission_ITGK_Code, enroll_cnt, admision_cnt,'".$_Batch."'
							from (SELECT BioMatric_ITGK_Code,
							count(a.BioMatric_Admission_LCode) as enroll_cnt FROM tbl_biomatric_registration as a right join
							tbl_admission as b on a.BioMatric_Admission_LCode=b.Admission_LearnerCode
							WHERE Admission_Batch = '".$_Batch."' AND Admission_Payment_Status = '1'
							group by a.BioMatric_ITGK_Code order by BioMatric_ITGK_Code) as q1 right JOIN 
							(SELECT Admission_ITGK_Code, count(Admission_LearnerCode) as admision_cnt  
							FROM tbl_admission WHERE Admission_Batch = '".$_Batch."' AND  Admission_Payment_Status = '1'  
							group by Admission_ITGK_Code order by Admission_ITGK_Code) as q2 on
							q1.BioMatric_ITGK_Code=q2.Admission_ITGK_Code";					
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);
				}
				else if($_Parameter=='3'){
					$deletequery="TRUNCATE TABLE tbl_rating_attendance_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);
					
					$_SelectQuery = "INSERT INTO
							tbl_rating_attendance_temp(rating_attendance_count,rating_attendance_Itgk_code,
							rating_admission_count,rating_attendance_batch)
							select sum(t) as attcount,q2.Admission_ITGK_Code,admision_cnt,'".$_Batch."'
							from (select
							count(distinct date(Attendance_In_Time)) as t,Admission_LearnerCode,Admission_ITGK_Code,
							Attendance_Admission_Code,Attendance_ITGK_Code,
							Admission_Batch,Admission_Course,Admission_Name,Admission_Fname , BioMatric_Status
							from tbl_learner_attendance as a right join tbl_admission as b on a.Attendance_Admission_Code=
							b.Admission_LearnerCode where Admission_Payment_Status='1' and Admission_Batch='".$_Batch."'
							group by Admission_LearnerCode,Attendance_ITGK_Code) as tb right join 
							(SELECT Admission_ITGK_Code, count(Admission_LearnerCode) as admision_cnt,
							Admission_Batch as batch FROM tbl_admission 
							WHERE Admission_Batch = '".$_Batch."' AND  Admission_Payment_Status = '1'
							group by Admission_ITGK_Code order by Admission_ITGK_Code) as q2
							on tb.Attendance_ITGK_Code=q2.Admission_ITGK_Code group by Admission_ITGK_Code";					
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);
				}
				else if($_Parameter=='4'){
					$deletequery="TRUNCATE TABLE tbl_rating_DeRegister_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);
					
					$_SelectQuery = "INSERT INTO
							tbl_rating_DeRegister_temp(rating_DeRegister_Itgk_code,rating_DeRegister_count,
							rating_admission_count,rating_DeRegister_batch)							
							Select Admission_ITGK_Code, deregister_cnt, admision_cnt,'".$_Batch."'
							from (SELECT Adm_Data_Log_ITGK,
							count(a.Adm_Data_Log_Lcode) as deregister_cnt FROM tbl_adm_data_log as a right join 
							tbl_admission as b on a.Adm_Data_Log_Lcode=b.Admission_LearnerCode
							WHERE Admission_Course = '1' AND Admission_Batch = '".$_Batch."' AND Admission_Payment_Status = '1'
							group by a.Adm_Data_Log_ITGK order by Adm_Data_Log_ITGK) as q1 right JOIN (SELECT Admission_ITGK_Code, 
							count(Admission_LearnerCode) as admision_cnt  FROM tbl_admission WHERE Admission_Course = '1'
							AND Admission_Batch = '".$_Batch."' AND  Admission_Payment_Status = '1'  
							group by Admission_ITGK_Code
							order by Admission_ITGK_Code) as q2 on q1.Adm_Data_Log_ITGK=q2.Admission_ITGK_Code";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);
				}
				else if($_Parameter=='5'){
					$deletequery="TRUNCATE TABLE tbl_rating_blockunblock_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);
					
					$_SelectBatchDate = "select Batch_StartDate,Batch_EndDate from tbl_batch_master 
									where Batch_Code='".$_Batch."'";
					$_SelectBatchDate=$_ObjConnection->ExecuteQuery($_SelectBatchDate, Message::SelectStatement);
						if($_SelectBatchDate[0] == "Success"){				
							$_Rows=mysqli_fetch_array($_SelectBatchDate[2]);
								$sdate=$_Rows['Batch_StartDate'];
								$edate=$_Rows['Batch_EndDate'];
						
					$_SelectQuery = "INSERT INTO
							tbl_rating_blockunblock_temp(rating_blockunblock_Itgk_code,rating_blockunblock_count,
							rating_admission_count,rating_blockunblock_batch)								
							Select Admission_ITGK_Code, cnt, admision_cnt,'".$_Batch."' from (
							select count(id) as cnt, centercode from block where activity='Block'
							and activitydate>='".$sdate."' and activitydate<='".$edate."' group by centercode)
							as q1 right JOIN (SELECT Admission_ITGK_Code, 
							count(Admission_LearnerCode) as admision_cnt  FROM tbl_admission WHERE Admission_Course = '1'
							AND Admission_Batch = '".$_Batch."' AND  Admission_Payment_Status = '1'
							group by Admission_ITGK_Code
							order by Admission_ITGK_Code) as q2 on q1.centercode=q2.Admission_ITGK_Code";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);
						}
				}
				else if($_Parameter=='6'){
					$deletequery="TRUNCATE TABLE tbl_rating_result_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);					
						
					$_SelectQuery = "INSERT INTO
							tbl_rating_result_temp(rating_result_Itgk_code,rating_admission_count,rating_result_count,
							rating_result_batch)								
							SELECT study_cen as Admission_ITGK_Code,count(scol_no) as ad_cnt,
						sum(case when exameventnameID='".$_Event."' AND result like '%pass%' then 1 else 0 END)
						as pass_cnt,'".$_Batch."' from tbl_result where exameventnameID='".$_Event."' group by study_cen";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);						
				}
				else if($_Parameter=='7'){
					$deletequery="TRUNCATE TABLE tbl_rating_infra_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);					
						
					$_SelectQuery = "INSERT INTO
							tbl_rating_infra_temp(rating_infra_Itgk_code,rating_infra_count,
							rating_infra_batch)								
							select System_User as ITGK,count(System_Code) as total_comp,'".$_Batch."'
							from tbl_system_info where System_Status='Confirmed By Center' and System_User!='0'
							group by System_User";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);						
				}
				else if($_Parameter=='8'){
					$deletequery="TRUNCATE TABLE tbl_rating_dpofeedback_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);					
						
					$_SelectQuery = "INSERT INTO
							tbl_rating_dpofeedback_temp(rating_dpofeedback_Itgk_code,rating_dpofeedback_count,
							rating_dpofeedback_batch)								
							SELECT c1.DPO_To_ITGK_code,c1.DPO_To_ITGK_rate,'".$_Batch."'
							FROM tbl_rate_dpo_to_itgk c1 LEFT JOIN tbl_rate_dpo_to_itgk c2
							ON (c1.DPO_To_ITGK_code = c2.DPO_To_ITGK_code AND c1.DPO_To_ITGK_id < c2.DPO_To_ITGK_id)
							WHERE c2.DPO_To_ITGK_id IS NULL";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);						
				}
				else if($_Parameter=='9'){
					$deletequery="TRUNCATE TABLE tbl_rating_learnerfeedback_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);					
						
					$_SelectQuery = "INSERT INTO
							tbl_rating_learnerfeedback_temp(rating_learnerfeedback_Itgk_code,rating_learnerfeedback_count,
							rating_learnerfeedback_batch)								
							select Learner_To_ITGK_code,ROUND(AVG(Learner_To_ITGK_rate),1) as avg,'".$_Batch."'
							from tbl_rate_learner_to_itgk
							where Learner_To_ITGK_batch='".$_Batch."' group by Learner_To_ITGK_code";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);						
				}
				else if($_Parameter=='10'){
					$deletequery="TRUNCATE TABLE tbl_rating_ilearnmarks_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);
					
					$_SelectQuery = "INSERT INTO
							tbl_rating_ilearnmarks_temp(rating_ilearnmarks_Itgk_code,rating_ilearnmarks_count,
							rating_admission_count,rating_ilearnmarks_batch)
							select Fresult_itgk,Round(AVG(Fresult_obtainmarks),1) as avg,'0','".$_Batch."'
							from tbl_assessementfinalresult where Fresult_batch='".$_Batch."' and Fresult_obtainmarks>11
							group by Fresult_itgk";					
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);
				}
				else if($_Parameter=='11'){
					$deletequery="TRUNCATE TABLE tbl_rating_digital_cert_temp";
					$_Responses=$_ObjConnection->ExecuteQuery($deletequery, Message::DeleteStatement);
					
					$_SelectQuery1 = "select Affilate_FreshBaches from tbl_exammaster where Affilate_Event='".$_Event."'";
					$_selectResponses=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
						if($_selectResponses[0] == "Success"){
							$rows=mysqli_fetch_array($_selectResponses[2]);
							$batches=$rows['Affilate_FreshBaches'];
							$_SelectQuery = "INSERT INTO
							tbl_rating_digital_cert_temp(rating_digital_cert_count,rating_admission_count,
							rating_digital_cert_Itgk_code,rating_digital_cert_batch)
							select sum(case when Admission_Aadhar_Status in ('2','4') then 1 else 0 end) as
							generated_cnt, count(Admission_LearnerCode) as ad_cnt, Admission_ITGK_Code,'".$_Batch."'
							from tbl_admission where Admission_Batch in ($batches) and 
							Admission_Payment_Status='1' group by Admission_ITGK_Code";					
							$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::InsertStatement);
						}
					
				}
				
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
	
}
