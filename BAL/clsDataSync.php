<?php

/**
 * Description of clsDataSync
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';



$_ObjConnection = new _Connection();

$_Response = array();


ini_set("memory_limit", "-1");
ini_set('max_execution_time', 0);

class clsDataSync {
// Code for Data Sync from iLearn tbl_assessementfinalresult to MyRKCL tbl_assessementfinalresult Starts Here 
    /**
     * 
     * @global IlearnConfig $ilearnConnection
     * @return SELECT DATA WITH STATUS Y
     */
    /**
     * 
     * @global IlearnConfig $ilearnConnection
     * @return AFTER SELECT CHNAGE STATUS 'P'
     */

    /**
     * 
     * @global _Connection $_ObjConnection
     * @param type $smsdetails FOR COMEE VALES STRING
     * @return DATA INSERT AND UPDATE 
     */
	 
	    public function AssmenttoScore($batches,$examid) {
        global $_ObjConnection;
        $conn = $_ObjConnection->Connect();
        try {
           $trunquery = "TRUNCATE tbl_learner_score_temp";
            $_Response = $_ObjConnection->ExecuteQuery($trunquery, Message::OnUpdateStatement);

          $_InsertQuery = "INSERT INTO tbl_learner_score_temp (Admission_Code, Learner_Code, Score, Score_Per, ITGK_code,ExamID) 
                (SELECT ad.Admission_Code, us.Fresult_learner, us.Fresult_obtainmarks, ((us.Fresult_obtainmarks/30)*100) as Score_Per, us.Fresult_itgk , '".$examid."' 
                FROM tbl_assessementfinalresult AS us inner join tbl_admission as ad on us.Fresult_learner =ad.Admission_LearnerCode  WHERE us.Fresult_obtainmarks > 0 
                and ad.Admission_Batch in(".$batches.")) 
                ON DUPLICATE KEY UPDATE tbl_learner_score_temp.Score = IF(tbl_learner_score_temp.Score > us.Fresult_obtainmarks, tbl_learner_score_temp.Score,               us.Fresult_obtainmarks), 
                tbl_learner_score_temp.Score_Per = IF(tbl_learner_score_temp.Score_Per > ((us.Fresult_obtainmarks/30)*100) , 
                tbl_learner_score_temp.Score_Per, ((us.Fresult_obtainmarks/30)*100))";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {

            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function AssmenttoScoreFinal() {
        global $_ObjConnection;
        $conn = $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_learner_score (Admission_Code, Learner_Code, Score, Score_Per, ITGK_code,ExamID) 
                (SELECT us.Admission_Code, us.Learner_Code, us.Score, us.Score_Per, us.ITGK_code, us.ExamID FROM tbl_learner_score_temp AS us WHERE us.Score > 0) ON DUPLICATE KEY UPDATE tbl_learner_score.Score = IF(tbl_learner_score.Score > us.Score, tbl_learner_score.Score, us.Score), 
                tbl_learner_score.Score_Per = IF(tbl_learner_score.Score_Per > us.Score_Per, tbl_learner_score.Score_Per, us.Score_Per)";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {

            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    } 
	 
    public function Add_final_result($scoredetails) {
        global $_ObjConnection;
        $conn = $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_learner_score_temp "
                    . "(Admission_Code, Learner_Code, Score, Score_Per, ITGK_code, AddEditTime) "
                    . "VALUES " . $scoredetails . " ON DUPLICATE KEY UPDATE 
            Score = IF(Score > values(Score),Score,values(Score)),
            Score_Per = IF(Score_Per > values(Score_Per),Score_Per,values(Score_Per))";



            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {

            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add_final_result2($scoredetails) {
        global $_ObjConnection;
        $conn = $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_assessementfinalresult "
                    . "(Fresult_learner,Fresult_itgk,Fresult_rsp,Fresult_district,Fresult_batch,Fresult_course,Fresult_noofassessment,Fresult_obtainmarks,Fresult_timestamp,IsNewMRecord,Re_exam_status) "
                    . "VALUES " . $scoredetails . " ON DUPLICATE KEY UPDATE "
                    . "Fresult_learner = VALUES(Fresult_learner),"
                    . "Fresult_itgk = VALUES(Fresult_itgk),"
                    . "Fresult_rsp = VALUES(Fresult_rsp),"
                    . "Fresult_district = VALUES(Fresult_district),"
                    . "Fresult_batch = VALUES(Fresult_batch),"
                    . "Fresult_course = VALUES(Fresult_course),"
                    . "Fresult_noofassessment = VALUES(Fresult_noofassessment),"
                    . "Fresult_obtainmarks = VALUES(Fresult_obtainmarks),"
                    . "Fresult_timestamp = VALUES(Fresult_timestamp),"
                    . "Re_exam_status = VALUES(Re_exam_status)";


            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {

            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /**
     * 
     * @global IlearnConfig $ilearnConnection
     * @param type $ids
     * @return type
     */

    /**
     * 
     * @global IlearnConfig $_ObjConnection
     * @return SELECT DATA WITH STATUS Y
     */
    public function GetAll() {
        global $_ObjConnection;
        // GET ALL DATA IN SMS TABLE
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Admission_Code,Fresult_id,Fresult_learner,Fresult_itgk,Fresult_rsp,Fresult_district,Fresult_batch,Fresult_course,Fresult_noofassessment,Fresult_obtainmarks,Fresult_timestamp,IsNewMRecord,Re_exam_status From tbl_assessementfinalresult AS a "
                    . "LEFT JOIN tbl_admission b on a.Fresult_learner=b.Admission_LearnerCode  where Fresult_obtainmarks > '11' and IsNewMRecord = 'Y' limit 100000";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $this->update_status();
        } catch (Exception $_exp) {

            $_Response[0] = $_exp->getLine() . $_exp->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /**
     * 
     * @global IlearnConfig $ilearnConnection
     * @return AFTER SELECT CHNAGE STATUS 'P'
     */
    public function update_status() {
        global $_ObjConnection;
        // GET ALL DATA IN SMS TABLE
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Update tbl_assessementfinalresult a SET a.IsNewMRecord = 'P' where a.Fresult_id in (SELECT * FROM (SELECT c.Fresult_id from tbl_assessementfinalresult c where c.IsNewMRecord = 'Y' and Fresult_obtainmarks > '11' limit 100000) as b)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_exp) {

            $_Response[0] = $_exp->getLine() . $_exp->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /**
     * 
     * @global IlearnConfig $ilearnConnection
     * @param type $ids
     * @return type
     */
    public function update_result($ids) {
        global $_ObjConnection;
        // GET ALL DATA IN SMS TABLE
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Update tbl_assessementfinalresult a SET a.IsNewMRecord = 'N' where a.Fresult_id in ('$ids') and a.IsNewMRecord = 'P'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_exp) {

            $_Response[0] = $_exp->getLine() . $_exp->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    // Code for Data Sync from iLearn tbl_assessementfinalresult to MyRKCL tbl_assessementfinalresult Ends Here 

    public function GetExamEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $_SelectQuery = "Select a.Course_Code b.Course_Name FROM tbl_exammaster as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.a.Course_Code WHERE a.";
            $_SelectQuery = "Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetReexamBatch($_ExamEvent, $batchId = 0) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelBatch = "Select concat(Affilate_ReexamBatches, ',',Affilate_FreshBaches) as totalbatch  from tbl_exammaster where Affilate_Event = '" . $_ExamEvent . "'";
            $_a = $_ObjConnection->ExecuteQuery($_SelBatch, Message::SelectStatement);
            $_b = mysqli_fetch_array($_a[2]);
            $_Batch = $_b['totalbatch'];

            $filter = ($batchId) ? " AND b.Batch_Code = '" . $batchId . "'" : '';

            $_SelectQuery = "Select a.Affilate_ReexamBatches, b.Batch_Name, b.Batch_Code, c.Course_Name, c.Course_Code FROM tbl_exammaster"
                    . " as a INNER JOIN tbl_batch_master as b ON b.Batch_Code IN ($_Batch) inner join tbl_course_master"
                    . " as c on c.Course_Code= b.Course_Code  where "
                    . " a.Affilate_Event = '" . $_ExamEvent . "' " . $filter;

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    //Hitesh

    public function GetLearnerCountSynch($Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(Fresult_learner) as LearnerCount from tbl_assessementfinalresult where Fresult_batch in ($Batch)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerCountSynch_N_Number($Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(Fresult_learner) as LearnerCount from tbl_assessementfinalresult where IsNewMRecord='N' and Fresult_batch in ($Batch)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetLearnerCountSynch_P_Number($Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(Fresult_learner) as LearnerCountP from tbl_assessementfinalresult where IsNewMRecord='P' and Fresult_batch in ($Batch)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateStatusLearnerSynch($Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "update tbl_assessementfinalresult set IsNewMRecord='Y' where Fresult_batch in ($Batch)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::OnUpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
  //hitesh
    public function UpdateStatusTable($tblname,$batches=NULL) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if($tblname=='tbl_admission'){
                $_SelectQuery = "update $tblname set IsNewRecord='Y' where Admission_Batch in ($batches)";
            }
            else{
               $_SelectQuery = "update $tblname set IsNewOnlineLMSRecord='Y'";
            }
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::OnUpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GettblCountSynch($tblname, $batches=NULL) {
         global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if($tblname=='tbl_admission'){
                $_SelectQuery = "select count(*) as RecordCount from $tblname  where Admission_Batch in ($batches) and Admission_Payment_Status='1'";
            }
            else{
                $_SelectQuery = "select count(*) as RecordCount from $tblname";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GettblCountSynchNstatus($tblname, $batches=NULL) {
         global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if($tblname=='tbl_admission'){
                $_SelectQuery = "select count(*) as RecordCountN from $tblname where IsNewRecord = 'N' and Admission_Batch in ($batches) and  Admission_Payment_Status='1'";
            }
            else{
                $_SelectQuery = "select count(*) as RecordCountN from $tblname where IsNewOnlineLMSRecord = 'N'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;

    }
  
public function GettblCountSynchPstatus($tblname, $batches=NULL) {
     global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if($tblname=='tbl_admission'){
                $_SelectQuery = "select count(*) as RecordCountP from $tblname where IsNewRecord = 'P' and Admission_Batch in ($batches) and  Admission_Payment_Status='1'";
            }
            else{
                $_SelectQuery = "select count(*) as RecordCountP from $tblname where IsNewOnlineLMSRecord = 'P'";
            }
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;

    }
    /**
     * 
     * @global MYRKCL $_ObjConnection
     * @return SELECT DATA WITH STATUS Y
     */
    public function GetAllMyrkclTbl($Tblname, $batches) {
        global $_ObjConnection;
        // GET ALL DATA IN SMS TABLE
        $_ObjConnection->Connect();
        try {
            if($Tblname=='tbl_admission'){
			
			       $_SelectQuery = "Select * from $Tblname where IsNewRecord = 'Y' and Admission_Batch in ($batches) and Admission_Payment_Status='1' Limit 50000";
		/**	 if($batches >= '258')
                {
                    $_SelectQuery = "Select * from $Tblname where IsNewRecord = 'Y' and Admission_Batch in ($batches) Limit 50000";
                }
                else{
                    $_SelectQuery = "Select * from $Tblname where IsNewRecord = 'Y' and Admission_Batch in ($batches) and Admission_Payment_Status='1' Limit 50000";
				}  **/
            }
            else{
                $_SelectQuery = "Select * from $Tblname where IsNewOnlineLMSRecord = 'Y'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $this->TblStatusUpdtoP($Tblname, $batches);
			
        } catch (Exception $_exp) {

            $_Response[0] = $_exp->getLine() . $_exp->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    /**
     * 
     * @global MYRKCL table 
     * @return AFTER SELECT CHNAGE STATUS 'P'
     */
    public function TblStatusUpdtoP($tblname, $batches) {
        global $_ObjConnection;
        // GET ALL DATA IN SMS TABLE
        $_ObjConnection->Connect();
        try {
            if($tblname=='tbl_admission'){
               $_SelectQuery = "Update tbl_admission a SET a.IsNewRecord = 'P' where a.Admission_Code in (SELECT * FROM (SELECT c.Admission_Code from tbl_admission c where c.IsNewRecord = 'Y' and Admission_Payment_Status='1' and Admission_Batch in ($batches) limit 50000) as b)";
            
            }
            else{
                 $_SelectQuery = "Update $tblname a SET a.IsNewOnlineLMSRecord = 'P' where a.IsNewOnlineLMSRecord = 'Y";
          
            }
 
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_exp) {

            $_Response[0] = $_exp->getLine() . $_exp->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function TblStatusUpdtoN($tblname, $batches) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if($tblname=='tbl_admission'){
                $_SelectQuery = "update $tblname set IsNewRecord='N' where Admission_Batch in ($batches) and IsNewRecord='P'";
            }
            else{
               $_SelectQuery = "update $tblname set IsNewOnlineLMSRecord='N' where  IsNewRecord='P'";
            }
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::OnUpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAdmBatch($tblname) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select b.Batch_Name, b.Batch_Code, c.Course_Name, c.Course_Code FROM tbl_batch_master as b inner join tbl_course_master  as c on c.Course_Code= b.Course_Code  where b.Batch_Status='1' order by Batch_Code DESC";


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	public function FILLCourse()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct a.Courseitgk_Course as Course_Name, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' ORDER BY Course_Code ASC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }
public function GetBatch($CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $CourseCode . "'
                            order by Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
