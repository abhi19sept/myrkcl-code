<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsRenewalReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRenewalReport {
    //put your code here
    
    public function GetITGK_SLA_Details($ExpDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            $ExpDate = mysqli_real_escape_string($_ObjConnection->Connect(),$ExpDate);
            if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '11' || $_LoginRole == '30') {
//                $r = explode("-", $ExpDate);
//                $year = $r[0];
//                $month = $r[1];
//                $Date2 = date("$year-$month-05");
                $SDate = $ExpDate . '-01-01';
                $EDate = $ExpDate . '-12-31';
                date_default_timezone_set('Asia/Kolkata');
                $_Date = date("Y-m-d");
                if($EDate > $_Date){
                    $EDate = $_Date;
                } else{
                    $EDate = $EDate;
                }
                $_SelectQuery = "Select DISTINCT b.Courseitgk_Course, b.Courseitgk_ITGK AS ITGKCODE, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
                                c.Organization_Name as ITGK_Name, c.Organization_AreaType, l.Block_Name, m.GP_Name,
                                g.Organization_Name as RSP_Name From tbl_user_master as a 
                                INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
                                INNER join tbl_organization_detail as c on a.User_Code=c.Organization_User

                                INNER join tbl_user_master as f on a.User_Rsp=f.User_Code
                                INNER join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_panchayat_samiti as l on c.Organization_Panchayat=l.Block_Code
                                LEFT JOIN tbl_gram_panchayat as m on c.Organization_Gram=m.GP_Code
                                WHERE b.Courseitgk_Course='RS-CIT'  AND b.CourseITGK_ExpireDate >='" . $SDate . "' AND b.CourseITGK_ExpireDate <='" . $EDate . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }  else if ($_LoginRole == '14') {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }

    public function GetITGK_Admission_Details($_CenterCode, $_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
            $_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
            $SDate = $_StartDate . '00:00:00';
            $EDate = $_EndDate . '00:00:00';
            $_SelectQuery = "select count(Admission_Code) as Admission_Count from tbl_admission where Admission_ITGK_Code='" . $_CenterCode . "' 
                                and Admission_Date_Payment>'" . $SDate . "' and Admission_Date_Payment<='" . $EDate . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetRenewalPenaltyAmount() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Renewal_Penalty_Amount from tbl_renewal_penalty_master where Renewal_Penalty_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetSLAUploadStatus($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select EOI_Status_Flag from tbl_eoi_centerlist where EOI_ECL='" . $_CenterCode . "' "
                    . "and EOI_Code ='1' ORDER BY EOI_ID DESC LIMIT 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

//    public function Upload_SLA($_UserPermissionArray) {
//        global $_ObjConnection;
//        $_ObjConnection->Connect();
////        print_r($_UserPermissionArray['1']['Function']);
////        die;
//        try {
//            date_default_timezone_set('Asia/Kolkata');
//            $_Date = date("Y-m-d");
//
//            $_CountLength=count($_UserPermissionArray);
//            
//            for($i=1;$i<$_CountLength;$i++)
//            {
//            
//            $_SelectQuery = "Select EXTRACT(Year FROM CourseITGK_ExpireDate) as CourseITGK_ExpireDate,datediff(CourseITGK_ExpireDate,'" . $_Date . "') as DateDiffernce From tbl_courseitgk_mapping 
//                                        WHERE Courseitgk_ITGK = '" . $_UserPermissionArray[$i]['Function'] . "' AND Courseitgk_EOI='1'";
//            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
//
//            if ($_Response[0] == Message::NoRecordFound)
//                return;
//
//            $row = mysqli_fetch_array($_Response[2]);
//            $expire_year = $row['CourseITGK_ExpireDate'];
//            $EoiDate = $row['DateDiffernce'];
//            $previous_year = ($expire_year - 1);
//            if ($EoiDate <= 60) {
//                $_DuplicateQuery = "Select * From tbl_eoi_centerlist Where EOI_Code='1' AND
//                                                                        EOI_ECL = '" . $_UserPermissionArray[$i]['Function'] . "' AND EOI_Year='" . $previous_year . "' AND EOI_Status_Flag='Y'";
//                $_Responsess = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
//
//                if ($_Responsess[0] == Message::NoRecordFound) {
//                    $_InsertQuery = "INSERT INTO tbl_eoi_centerlist (EOI_ID, EOI_Code, EOI_ECL, EOI_Year,"
//                            . "EOI_DateTime, EOI_Filename, EOI_Status_Flag) "
//                            . "Select Case When Max(EOI_ID) Is Null Then 1 Else Max(EOI_ID)+1 End as EOI_ID,"
//                            . "'1' as EOI_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_ECL,'" . $previous_year . "' as EOI_Year,"
//                            . "'" . $_Date . "' as EOI_DateTime, 'ONLINE' as EOI_Filename, 'Y' as EOI_Status_Flag"
//                            . " From tbl_eoi_centerlist";
//                    $_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//
//                    $_UpdateQuery1 = "Update tbl_courseitgk_mapping set CourseITGK_Renewal_Status = 'Y' where
//                                                                                Courseitgk_ITGK='" . $_UserPermissionArray[$i]['Function'] . "' AND Courseitgk_EOI IN('1','2')";
//                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
//
//                    $_InsertQuery1 = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
//                            . "EOI_Log_DateTime, EOI_Log_Filename,EOI_Log_Status_Flag) "
//                            . "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
//                            . "'1' as EOI_Log_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
//                            . "'" . $_Date . "' as EOI_Log_DateTime, 'ONLINE' as EOI_Log_Filename, 'Updated' as EOI_Log_Status_Flag"
//                            . " From tbl_eoi_centerlist_Log";
//                    $_ResponseLog = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
//                } else {
//                    $_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
//                            . "EOI_Log_DateTime, EOI_Log_Filename) "
//                            . "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
//                            . "'1' as EOI_Log_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
//                            . "'" . $_Date . "' as EOI_Log_DateTime, 'ONLINE' as EOI_Log_Filename"
//                            . " From tbl_eoi_centerlist_Log";
//                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//                }
//            } else {
//                $_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
//                        . "EOI_Log_DateTime, EOI_Log_Filename) "
//                        . "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
//                        . "'1' as EOI_Log_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
//                        . "'" . $_Date . "' as EOI_Log_DateTime, 'ONLINE' as EOI_Log_Filename"
//                        . " From tbl_eoi_centerlist_Log";
//                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//            }
//            }
//        } catch (Exception $_e) {
//            $_Response[0] = $_e->getTraceAsString();
//            $_Response[1] = Message::Error;
//        }
//        return $_Response;
//    }

    
        public function Upload_SLA($_UserPermissionArray) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
//        print_r($_UserPermissionArray['1']['Function']);
//        print_r($_UserPermissionArray['1']['yr']);
//        print_r($_UserPermissionArray['2']['Function']);
//        print_r($_UserPermissionArray['2']['yr']);
//        die;
        try {
            date_default_timezone_set('Asia/Kolkata');
            $_Date = date("Y-m-d");

            $_CountLength=count($_UserPermissionArray);
            
            for($i=1;$i<$_CountLength;$i++)
            {
//            
//            $_SelectQuery = "Select EXTRACT(Year FROM CourseITGK_ExpireDate) as CourseITGK_ExpireDate,datediff(CourseITGK_ExpireDate,'" . $_Date . "') as DateDiffernce From tbl_courseitgk_mapping 
//                                        WHERE Courseitgk_ITGK = '" . $_UserPermissionArray[$i]['Function'] . "' AND Courseitgk_EOI='1'";
//            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
//
//            if ($_Response[0] == Message::NoRecordFound)
//                return;
//
//            $row = mysqli_fetch_array($_Response[2]);
//            $expire_year = $row['CourseITGK_ExpireDate'];
//            $EoiDate = $row['DateDiffernce'];
//            $previous_year = ($expire_year - 1);
//            if ($EoiDate <= 60) {
//                $_DuplicateQuery = "Select * From tbl_eoi_centerlist Where EOI_Code='1' AND
//                                                                        EOI_ECL = '" . $_UserPermissionArray[$i]['Function'] . "' AND EOI_Year='" . $previous_year . "' AND EOI_Status_Flag='Y'";
//                $_Responsess = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

//                if ($_Responsess[0] == Message::NoRecordFound) {
                    $_InsertQuery = "INSERT INTO tbl_eoi_centerlist (EOI_ID, EOI_Code, EOI_ECL, EOI_Year,"
                            . "EOI_DateTime, EOI_Filename, EOI_Status_Flag) "
                            . "Select Case When Max(EOI_ID) Is Null Then 1 Else Max(EOI_ID)+1 End as EOI_ID,"
                            . "'1' as EOI_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_ECL,'" . $_UserPermissionArray[$i]['yr'] . "' as EOI_Year,"
                            . "'" . $_Date . "' as EOI_DateTime, 'ONLINE' as EOI_Filename, 'Y' as EOI_Status_Flag"
                            . " From tbl_eoi_centerlist";
                    $_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

                    $_UpdateQuery1 = "Update tbl_courseitgk_mapping set CourseITGK_Renewal_Status = 'Y' where
                                                                                Courseitgk_ITGK='" . $_UserPermissionArray[$i]['Function'] . "' AND Courseitgk_EOI IN('1','2')";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
                 
                    $_InsertQuery1 = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
                            . "EOI_Log_DateTime, EOI_Log_Filename,EOI_Log_Status_Flag) "
                            . "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
                            . "'1' as EOI_Log_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_Log_ECL,'" . $_UserPermissionArray[$i]['yr'] . "' as EOI_Log_Year,"
                            . "'" . $_Date . "' as EOI_Log_DateTime, 'ONLINE' as EOI_Log_Filename, 'Updated' as EOI_Log_Status_Flag"
                            . " From tbl_eoi_centerlist_Log";
                    $_ResponseLog = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
//                } else {
//                    $_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
//                            . "EOI_Log_DateTime, EOI_Log_Filename) "
//                            . "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
//                            . "'1' as EOI_Log_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
//                            . "'" . $_Date . "' as EOI_Log_DateTime, 'ONLINE' as EOI_Log_Filename"
//                            . " From tbl_eoi_centerlist_Log";
//                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//                }
//            } else {
//                $_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year,"
//                        . "EOI_Log_DateTime, EOI_Log_Filename) "
//                        . "Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID,"
//                        . "'1' as EOI_Log_Code,'" . $_UserPermissionArray[$i]['Function'] . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year,"
//                        . "'" . $_Date . "' as EOI_Log_DateTime, 'ONLINE' as EOI_Log_Filename"
//                        . " From tbl_eoi_centerlist_Log";
//                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//            }
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
