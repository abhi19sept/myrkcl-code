<?php

//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsItgkRating {

    public function GetBatch() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Batch_Code,Batch_Name From tbl_batch_master where Course_Code='1' and 
							Batch_Code>='205' order by Batch_Code DESC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }

    public function GetBatchstatus($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select * From tbl_ranking_master where ITGK_Rating_Batch_Code='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == Message::NoRecordFound){
					$_CurrentYear=date("Y");
					$_InsertQuery = "INSERT INTO tbl_ranking_master (ITGK_Rating_ITGK_code, ITGK_Rating_Rsp_code, ITGK_Rating_Batch_Code,
									ITGK_Rating_Year) SELECT User_LoginId, User_Rsp, '".$_Batch_Code."', '".$_CurrentYear."'
									FROM tbl_user_master WHERE User_UserRoll='7'";
					$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		
    }
	
	
	 public function AdmissionWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_admission_count as admission_count,rating_admission_Itgk_code as Admission_ITGK_Code 
							From tbl_rating_admission_temp where rating_admission_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					$_SelectWeightage = "select ranking_weightage_percentage From tbl_ranking_weightage where 
								ranking_weightage_parameter='Number of Admission' and ranking_weightage_status='Active'";
					$_select_Weightage=$_ObjConnection->ExecuteQuery($_SelectWeightage, Message::SelectStatement);
						$_Row=mysqli_fetch_array($_select_Weightage[2]);
						$per=$_Row['ranking_weightage_percentage'];
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$cnt=$row['admission_count'];
						$ITGK=$row['Admission_ITGK_Code'];
						$rating=$cnt*$per/100;					
							
						$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Admission_Count='".$rating."',
										a.ITGK_Rating_Admission_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
										AND ranking_range_parameter='Admission' AND ITGK_Rating_Batch_Code='".$_Batch_Code."'";
						$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
					}
					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		
    }
	
	public function AttendanceWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_attendance_Itgk_code as Admission_ITGK_Code,
							rating_attendance_count as attcount,rating_admission_count as admision_cnt
							from tbl_rating_attendance_temp where rating_attendance_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					$_SelectWeightage = "select ranking_weightage_percentage From tbl_ranking_weightage where 
								ranking_weightage_parameter='Learners Unique Attendance' and ranking_weightage_status='Active'";
					$_select_Weightage=$_ObjConnection->ExecuteQuery($_SelectWeightage, Message::SelectStatement);
						$_Row=mysqli_fetch_array($_select_Weightage[2]);
						$per=$_Row['ranking_weightage_percentage'];
					while($row=mysqli_fetch_array($_selectResponse[2])){
						
						$attendance_cnt=$row['attcount'];						
						
						if($attendance_cnt=='' || $enroll_cnt=='NULL'){
											
							}
						else{
								$admission_cnt=$row['admision_cnt']*66;
								$ITGK=$row['Admission_ITGK_Code'];
								$ratio=$attendance_cnt/$admission_cnt*100;
								$rating=$ratio*$per/100;
						
							$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Attendance='".$rating."',
										a.ITGK_Rating_Attendance_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
										AND ranking_range_parameter='Attendance' AND ITGK_Rating_Batch_Code='".$_Batch_Code."'";
						$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
						}	
						
					}
					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		
    }
	
	public function LearnersEnrollWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_enrollment_Itgk_code as Admission_ITGK_Code,
							rating_enrollment_count as enroll_cnt,rating_admission_count as admision_cnt
							from tbl_rating_enrollment_temp where rating_enrollment_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					$_SelectWeightage = "select ranking_weightage_percentage From tbl_ranking_weightage where 
								ranking_weightage_parameter='Biometric Learner Enrollment' and 
								ranking_weightage_status='Active'";
					$_select_Weightage=$_ObjConnection->ExecuteQuery($_SelectWeightage, Message::SelectStatement);
						$_Row=mysqli_fetch_array($_select_Weightage[2]);
						$per=$_Row['ranking_weightage_percentage'];
					while($row=mysqli_fetch_array($_selectResponse[2])){						
						$enroll_cnt=$row['enroll_cnt'];					
						$admision_cnt=$row['admision_cnt'];
						$ITGK=$row['Admission_ITGK_Code'];
					
							if($enroll_cnt=='' || $enroll_cnt=='NULL'){
											
							}
							else{
								$cnt=($enroll_cnt/$admision_cnt)*100;
								$rating=$cnt*$per/100;
								
								$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Biometric_Enrollment='".$rating."',
										a.ITGK_Rating_Enrollment_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
									AND ranking_range_parameter='Enrollment' AND ITGK_Rating_Batch_Code='".$_Batch_Code."'";
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}
						
					}					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		
    }
	
	public function DeregisterWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_DeRegister_Itgk_code as Admission_ITGK_Code,
							rating_DeRegister_count as deregister_cnt,rating_admission_count as admision_cnt
							from tbl_rating_deregister_temp where rating_DeRegister_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					$_SelectWeightage = "select ranking_weightage_percentage From tbl_ranking_weightage where 
								ranking_weightage_parameter='Learner De-Register Request' and 
								ranking_weightage_status='Active'";
					$_select_Weightage=$_ObjConnection->ExecuteQuery($_SelectWeightage, Message::SelectStatement);
						$_Row=mysqli_fetch_array($_select_Weightage[2]);
						$per=$_Row['ranking_weightage_percentage'];
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$deregister_cnt=$row['deregister_cnt'];
						$admision_cnt=$row['admision_cnt'];
						$ITGK=$row['Admission_ITGK_Code'];
							if($deregister_cnt=='' || $deregister_cnt=='NULL'){
								$updateQuery = "UPDATE tbl_ranking_master SET ITGK_Rating_Deregister_Star='5'
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' AND
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);		
							}
							else{
								$cnt=($deregister_cnt/$admision_cnt)*100;
								$rating=$cnt*$per/100;
								
								$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Deregister='".$rating."',
										a.ITGK_Rating_Deregister_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
									AND ranking_range_parameter='DeRegister' AND ITGK_Rating_Batch_Code='".$_Batch_Code."'";
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}						
					}
					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		
    }
	
	
	public function BlockUnBlockWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_blockunblock_Itgk_code as Admission_ITGK_Code,
							rating_blockunblock_count as cnt,rating_admission_count as admision_cnt
							from tbl_rating_blockunblock_temp where rating_blockunblock_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$cnt=$row['cnt'];
						$ITGK=$row['Admission_ITGK_Code'];
						if($cnt=='' || $cnt=='NULL'){
								$updateQuery = "UPDATE tbl_ranking_master SET ITGK_Rating_Block_Star='5'
												WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' AND
												ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);		
							}
							else{
									//$rating=$cnt*$per/100;						
									$rating=$cnt;						
									$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
													a.ITGK_Rating_Block_Count='".$rating."',
													a.ITGK_Rating_Block_Star=b.ranking_range_star_rating
													WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
													('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
													AND ranking_range_parameter='Block' AND
													ITGK_Rating_Batch_Code='".$_Batch_Code."'";									
									$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}
					}
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
		
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }		
    }
	
	public function ResultWiseRank($_Batch_Code,$_Exam_Event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_Exam_Event = mysqli_real_escape_string($_ObjConnection->Connect(),$_Exam_Event);
            $_SelectQuery = "select rating_result_Itgk_code as Admission_ITGK_Code,rating_result_count as pass_cnt,
					rating_admission_count as ad_cnt from tbl_rating_result_temp where rating_result_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					$_SelectWeightage = "select ranking_weightage_percentage From tbl_ranking_weightage where 
								ranking_weightage_parameter='RS-CIT Results' and ranking_weightage_status='Active'";
					$_select_Weightage=$_ObjConnection->ExecuteQuery($_SelectWeightage, Message::SelectStatement);
						$_Row=mysqli_fetch_array($_select_Weightage[2]);
						$per=$_Row['ranking_weightage_percentage'];
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$pass_cnt=$row['pass_cnt'];
						$admision_cnt=$row['ad_cnt'];
						$ITGK=$row['Admission_ITGK_Code'];
							if($pass_cnt=='' || $pass_cnt=='NULL'){
								$updateQuery = "UPDATE tbl_ranking_master SET ITGK_Rating_Result_Star='1'
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' AND 
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}
							else{
								$cnt=($pass_cnt/$admision_cnt)*100;
								$rating=$cnt*$per/100;
									
								$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Result='".$rating."',
										a.ITGK_Rating_Result_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
										AND ranking_range_parameter='Result' AND 
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}						
					}
					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		
    }
	
	public function iLearnMarksWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_ilearnmarks_Itgk_code as Admission_ITGK_Code,
							rating_ilearnmarks_count as eligible_cnt,rating_admission_count as ad_cnt
							from tbl_rating_ilearnmarks_temp where rating_ilearnmarks_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){					
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$eligible_cnt=$row['eligible_cnt'];
						$admision_cnt=$row['ad_cnt'];
						$ITGK=$row['Admission_ITGK_Code'];							
							$rating=$eligible_cnt;									
							$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
								a.ITGK_Rating_iLearn_Marks='".$rating."',
								a.ITGK_Rating_iLearn_Marks_Star=b.ranking_range_star_rating
								WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
								('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
								AND ranking_range_parameter='iLearnMarks' AND ITGK_Rating_Batch_Code='".$_Batch_Code."'";
							$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);				
					}					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }		
    }
	
	
	/*public function DigitalCertificateWiseRank($_Batch_Code,$_Exam_Event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
			$_SelectQuery = "select rating_digital_cert_Itgk_code as Admission_ITGK_Code,
							rating_digital_cert_count as generated_cnt,rating_admission_count as ad_cnt
							from tbl_rating_digital_cert_temp where rating_digital_cert_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					$_SelectWeightage = "select ranking_weightage_percentage From tbl_ranking_weightage where 
								ranking_weightage_parameter='Digital Certificate' and ranking_weightage_status='Active'";
					$_select_Weightage=$_ObjConnection->ExecuteQuery($_SelectWeightage, Message::SelectStatement);
						$_Row=mysqli_fetch_array($_select_Weightage[2]);
						$per=$_Row['ranking_weightage_percentage'];
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$generated_cnt=$row['generated_cnt'];
						$admision_cnt=$row['ad_cnt'];
						$ITGK=$row['Admission_ITGK_Code'];
							if($generated_cnt=='0' || $generated_cnt=='NULL'){
								$updateQuery = "UPDATE tbl_ranking_master SET ITGK_Rating_Digital_Certificate_Star='1'
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' AND 
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}
							else{
								$cnt=($generated_cnt/$admision_cnt)*100;
								$rating=$cnt*$per/100;
									
								$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Digital_Certificate='".$rating."',
										a.ITGK_Rating_Digital_Certificate_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
										AND ranking_range_parameter='DigitalCertificate' AND 
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}						
					}
					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }		
    }*/
	
	public function InfraWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_infra_Itgk_code as ITGK,rating_infra_count as total_comp
							from tbl_rating_infra_temp where rating_infra_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){					
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$generated_cnt=$row['total_comp'];
						$ITGK=$row['ITGK'];
							$rating=$generated_cnt;
									
							$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Infrastructure='".$rating."',
										a.ITGK_Rating_Infrastructure_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
										AND ranking_range_parameter='Infrastructure' AND
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
							$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);				
					}					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }		
    }
	
	public function LearnerFeedbackWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_learnerfeedback_Itgk_code as Learner_To_ITGK_code,
							rating_learnerfeedback_count as avg from tbl_rating_learnerfeedback_temp where
							rating_learnerfeedback_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$generated_cnt=$row['avg'];
						$ITGK=$row['Learner_To_ITGK_code'];
							if($generated_cnt=='0' || $generated_cnt=='NULL'){
								$updateQuery = "UPDATE tbl_ranking_master SET ITGK_Rating_Learner_Feedback_Star='1'
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' AND
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}
							else{
								$rating=$generated_cnt;
									
								$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_Learner_Feedback='".$rating."',
										a.ITGK_Rating_Learner_Feedback_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
										AND ranking_range_parameter='LearnerFeedback' AND
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}						
					}
					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }		
    }
	
	public function DpoFeedbackkWiseRank($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $_SelectQuery = "select rating_dpofeedback_Itgk_code as DPO_To_ITGK_code,
							rating_dpofeedback_count as DPO_To_ITGK_rate from tbl_rating_dpofeedback_temp
							where rating_dpofeedback_batch='".$_Batch_Code."'";
            $_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_selectResponse[0] == "Success"){
					
					while($row=mysqli_fetch_array($_selectResponse[2])){
						$generated_cnt=$row['DPO_To_ITGK_rate'];
						$ITGK=$row['DPO_To_ITGK_code'];
							if($generated_cnt=='0' || $generated_cnt=='NULL'){
								$updateQuery = "UPDATE tbl_ranking_master SET ITGK_Rating_DPO_Feedback_Star='1'
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' AND
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}
							else{
								$rating=$generated_cnt;
									
								$updateQuery = "UPDATE tbl_ranking_master as a,tbl_ranking_range as b SET
										a.ITGK_Rating_DPO_Feedback='".$rating."',
										a.ITGK_Rating_DPO_Feedback_Star=b.ranking_range_star_rating
										WHERE ITGK_Rating_ITGK_code = '" . $ITGK . "' and
										('".$rating."' BETWEEN ranking_range_start AND ranking_range_end)
										AND ranking_range_parameter='DPOFeedback' AND
										ITGK_Rating_Batch_Code='".$_Batch_Code."'";										
								$_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
							}						
					}
					
					return $_Response;
				}
				else{
					$_Response="0";
					return $_Response;
				}
				
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }		
    }
	
	public function FinalRatingUpdate($_Batch_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
            $updateQuery = "update tbl_ranking_master as t1,(select ITGK_Rating_ITGK_code,
							ROUND(SUM(ITGK_Rating_Admission_Star+ITGK_Rating_Enrollment_Star+ITGK_Rating_Attendance_Star+
							 ITGK_Rating_Result_Star+ITGK_Rating_Block_Star+ITGK_Rating_Infrastructure_Star+
							 ITGK_Rating_Deregister_Star+ITGK_Rating_iLearn_Marks_Star							 
							 +ITGK_Rating_DPO_Feedback_Star+ITGK_Rating_Learner_Feedback_Star)/
							(select count(ranking_weightage_id) as cnt from tbl_ranking_weightage
							 where ranking_weightage_status='Active'),1) as avg
							 from tbl_ranking_master where ITGK_Rating_Batch_Code='".$_Batch_Code."'
							 group by ITGK_Rating_ITGK_code) as t2 set t1.ITGK_Rating_Final_Rating = t2.avg 
							 where t1.ITGK_Rating_ITGK_code=t2.ITGK_Rating_ITGK_code 
							 and ITGK_Rating_Batch_Code='".$_Batch_Code."'";
            $_Response=$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetITGK() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select User_LoginId From tbl_user_master where User_UserRoll='7'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetDataForITGKCode($_ITGKCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ITGKCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGKCode);
			$Current_Year=date("Y");
            $_SelectQuery = "select ITGK_Rating_Final_Rating,ITGK_Rating_Batch_Code From tbl_ranking_master 
							where ITGK_Rating_ITGK_code='".$_ITGKCode."' and 
							ITGK_Rating_Year='".$Current_Year."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetDataForITGKRating() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_ITGK_Code = $_SESSION['User_LoginId'];
				$Current_Year=date("Y");
				$_SelectQuery = "select ITGK_Rating_Final_Rating,ITGK_Rating_Batch_Code From tbl_ranking_master 
								 where ITGK_Rating_ITGK_code='".$_ITGK_Code."' and 
								 ITGK_Rating_Year='".$Current_Year."'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetAll($itgkcode,$mnth) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkcode);
            $mnth = mysqli_real_escape_string($_ObjConnection->Connect(),$mnth);
			$Current_Year=date("Y");
           $_SelectQuery = "select ITGK_Rating_Admission_Count as a1,ITGK_Rating_Biometric_Enrollment a2,
							ITGK_Rating_Attendance as a3,ITGK_Rating_Result as a6,ITGK_Rating_Block_Count as a5,
							ITGK_Rating_Infrastructure as a7,ITGK_Rating_Deregister as a4,
							ITGK_Rating_iLearn_Marks as a10,ITGK_Rating_DPO_Feedback as a8,
							ITGK_Rating_Learner_Feedback as a9,
							ITGK_Rating_Admission_Star as s1,ITGK_Rating_Enrollment_Star as s2,
							ITGK_Rating_Attendance_Star as s3,ITGK_Rating_Result_Star as s6,ITGK_Rating_Block_Star as s5,
							ITGK_Rating_Infrastructure_Star as s7,ITGK_Rating_Deregister_Star as s4,
							ITGK_Rating_iLearn_Marks_Star as s10,ITGK_Rating_DPO_Feedback_Star as s8,
							ITGK_Rating_Learner_Feedback_Star as s9
							from tbl_ranking_master as a inner join tbl_batch_master as b on 
							a.ITGK_Rating_Batch_Code=b.Batch_Code where ITGK_Rating_ITGK_code='".$itgkcode."' and
							Batch_Name='".$mnth."' and ITGK_Rating_Year='".$Current_Year."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function Search($request)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $request = mysqli_real_escape_string($_ObjConnection->Connect(),$request);
            if($request!='undefined'){
                                
                $sear="User_LoginId like '$request%'  ";            
            }else{
                $sear='';
            }
            $_SelectQuery = "select User_LoginId From tbl_user_master where User_UserRoll='7' AND $sear";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

	public function GetAllParameters() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "select ranking_weightage_parameter from tbl_ranking_weightage
								where ranking_weightage_status='Active'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetDataForSPRating() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_SP_Code = $_SESSION['User_LoginId'];
				$Current_Year=date("Y");
				$_SelectQuery = "select ITGK_Rating_ITGK_code, ROUND(AVG(ITGK_Rating_Final_Rating),1) as avg
									From tbl_ranking_master where ITGK_Rating_Year='".$Current_Year."' and 
									ITGK_Rating_Rsp_code='".$_SESSION['User_Code']."' group by ITGK_Rating_ITGK_code";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetDetailsForSPRating($_ITGK_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$Current_Year=date("Y");
				$_SelectQuery = "select ITGK_Rating_Final_Rating,ITGK_Rating_Batch_Code From tbl_ranking_master 
								 where ITGK_Rating_ITGK_code='".$_ITGK_Code."' and 
								 ITGK_Rating_Year='".$Current_Year."'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetAvgRatingForSP() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$Current_Year=date("Y");
				$_SelectQuery = "select ROUND(Avg(avg),1) as final_avg from (
									select ROUND(AVG(ITGK_Rating_Final_Rating),1) as avg
									From tbl_ranking_master where ITGK_Rating_Year='".$Current_Year."' and 
									ITGK_Rating_Rsp_code='".$_SESSION['User_Code']."' group by ITGK_Rating_ITGK_code) t";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
		public function GETAllSPRating() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$Current_Year=date("Y");
				$_SelectQuery = "select ITGK_Rating_Rsp_code,User_LoginId,ROUND(AVG(ITGK_Rating_Final_Rating),1) as avg
									from tbl_ranking_master as a inner join tbl_user_master as b 
									on a.ITGK_Rating_Rsp_code=b.User_Code where ITGK_Rating_Year='".$Current_Year."'
									group by ITGK_Rating_Rsp_code ";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetSpItgkWiseRatingDetails($_SP_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SP_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_SP_Code);
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$Current_Year=date("Y");
				$_SelectQuery = "select ITGK_Rating_ITGK_code, ROUND(AVG(ITGK_Rating_Final_Rating),1) as avg
									From tbl_ranking_master where ITGK_Rating_Year='".$Current_Year."' and 
									ITGK_Rating_Rsp_code='".$_SP_Code."' group by ITGK_Rating_ITGK_code";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetDistrictWiseData($_district_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_district_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_district_Code);
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$Current_Year=date("Y");
				$_SelectQuery = "select ITGK_Rating_ITGK_code,District_Name,ROUND(AVG(ITGK_Rating_Final_Rating),1) as avg 
					from tbl_ranking_master as a inner join tbl_user_master as b on a.ITGK_Rating_ITGK_code=b.User_LoginId
					inner join tbl_organization_detail as c on b.User_Code=c.Organization_User
					inner join tbl_district_master as d on c.Organization_District=d.District_Code
					where ITGK_Rating_Year='".$Current_Year."' and Organization_District='".$_district_Code."'
					group by ITGK_Rating_ITGK_code order by avg DESC";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
		 return $_Response;
    }
	
	public function GetExamEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select * From tbl_events where Event_Status = '1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
	
}

?>