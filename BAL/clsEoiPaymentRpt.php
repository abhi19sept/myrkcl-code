<?php

/**
 * Description of clsEoiPaymentRpt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response2 = array();

class clsEoiPaymentRpt {

    //put your code here
    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_LoginRole = $_SESSION['User_UserRoll'];
			$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
			$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
			
            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            }
			elseif ($_LoginRole == '30') {

                $_LoginUserType = "Reception";
                $_loginflag = "8";
            }
			else {
                echo "hello";
            }

            if ($_loginflag == "1") {

                $_SelectQuery2 = "Select a.*, b.*, c.EOI_Name From tbl_courseitgk_mapping as a INNER JOIN tbl_eoi_transaction as b ON a.Courseitgk_TranRefNo = b.EOI_Transaction_Txtid INNER JOIN tbl_eoi_master as c ON b.EOI_Transaction_ProdInfo = c.EOI_Code WHERE b.EOI_Transaction_timestamp >= '" . $_StartDate . "' AND b.EOI_Transaction_timestamp <= '" . $_EndDate . "'";
            }
			else if ($_loginflag == "8") {
// show only RS-CFA Reports
                $_SelectQuery2 = "Select a.*, b.*, c.EOI_Name From tbl_courseitgk_mapping as a INNER JOIN tbl_eoi_transaction as b ON a.Courseitgk_TranRefNo = b.EOI_Transaction_Txtid INNER JOIN tbl_eoi_master as c ON b.EOI_Transaction_ProdInfo = c.EOI_Code WHERE b.EOI_Transaction_timestamp >= '" . $_StartDate . "' AND b.EOI_Transaction_timestamp <= '" . $_EndDate . "' and Courseitgk_Course='RS-CFA'";
            } 
			else {
                $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_eoi_transaction AS b ON a.Centercode = b.EOI_Transaction_CenterCode WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response1[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";
                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");

                if ($CenterCode3) {
                    $_SelectQuery2 = "Select a.*, b.*, c.EOI_Name From tbl_courseitgk_mapping as a INNER JOIN tbl_eoi_transaction as b ON a.Courseitgk_TranRefNo = b.EOI_Transaction_Txtid INNER JOIN tbl_eoi_master as c ON b.EOI_Transaction_ProdInfo = c.EOI_Code WHERE b.EOI_Transaction_timestamp >= '" . $_StartDate . "' AND b.EOI_Transaction_timestamp <= '" . $_EndDate . "' AND b.EOI_Transaction_CenterCode IN ($CenterCode3)";
                }
            }
            $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response2;
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response1;
    }

    public function Showeoiapply($_StartDate, $_EndDate, $_Course, $_EOIname) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_EOIname = mysqli_real_escape_string($_ObjConnection->Connect(),$_EOIname);
				
            $_LoginRole = $_SESSION['User_UserRoll'];

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } 
			elseif ($_LoginRole == '14') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "8";
            } 
			
			else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;

            if ($_loginflag == "1") {

                $_SelectQuery2 = "Select a.*, b.* From tbl_courseitgk_mapping as a INNER JOIN tbl_eoi_master as b ON a.Courseitgk_EOI = b.EOI_Code WHERE a.Courseitgk_timestamp >= '" . $_StartDate . "' AND a.Courseitgk_timestamp <= '" . $_EndDate . "' AND a.Courseitgk_EOI='" . $_EOIname . "'";
            } 
			
			elseif ($_loginflag == "8") 
			{
				
				
				
				

             $_SelectQuery2 = "Select a.*, c.* From tbl_courseitgk_mapping as a inner join tbl_user_master as b on a.Courseitgk_ITGK=b.User_LoginId inner join tbl_eoi_master as c on a.Courseitgk_EOI=c.EOI_Code WHERE a.Courseitgk_timestamp >=  '" . $_StartDate . "' AND a.Courseitgk_timestamp <= '" . $_EndDate . "' AND a.Courseitgk_EOI='" . $_EOIname . "' AND b.User_UserRoll='7' AND b.User_Rsp='".$_SESSION['User_Code']."'";
			  
			  
			 
            } 
			else {
                $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_courseitgk_mapping AS b ON a.Centercode = b.Courseitgk_ITGK WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response1[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";
                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");

                if ($CenterCode3) {

                    $_SelectQuery2 = "Select a.*, b.* From tbl_courseitgk_mapping as a INNER JOIN tbl_eoi_master as b ON a.Courseitgk_EOI = b.EOI_Code WHERE a.Courseitgk_timestamp >= '" . $_StartDate . "' AND a.Courseitgk_timestamp <= '" . $_EndDate . "' AND a.Courseitgk_EOI='" . $_EOIname . "' AND a.Courseitgk_ITGK IN ($CenterCode3)";
                    //echo $_SelectQuery;
                }
            }

            $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response2;
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response1;
    }

}
