<?php

require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();
class clsmyrkcltostagingFileCreate {

    public function gentxtfilephoto($_Course,$_Batch)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {         
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				
            $_SelectQuery = "select Admission_Photo from tbl_admission where Admission_Course='".$_Course."' and Admission_Batch='".$_Batch."'";            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

    public function gentxtfilesign($_Course,$_Batch)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {    
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				
            $_SelectQuery = "select Admission_Sign from tbl_admission where Admission_Course='".$_Course."' and Admission_Batch='".$_Batch."'";            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}