<?php

/*
 * @author Abhi

 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCheckTxnStatus {
    //put your code here
    public function GetData()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Pay_Tran_PG_Trnid From tbl_payment_transaction Where (Pay_Tran_Status='PaymentInProcess' OR Pay_Tran_Status='PaymentFailure') AND Pay_Tran_ProdInfo='LearnerFeePayment' AND Pay_Tran_Reconcile_status='Not Reconciled' AND Pay_Tran_PG_Trnid !='' ORDER BY Pay_Tran_Code LIMIT 50";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}