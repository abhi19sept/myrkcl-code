<?php


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsProfileMaster {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select UserProfile_Code,UserProfile_User,UserProfile_FatherName,UserProfile_FirstName,UserProfile_HouseNo,UserProfile_DOB,UserProfile_MTongue,UserProfile_Address,UserProfile_Image,UserProfile_Sign,UserProfile_Gender,UserProfile_FatherName,UserProfile_Qualification,UserProfile_Occupation,UserProfile_PhysicallyChallenged,UserProfile_AadharNo,UserProfile_STDCode,UserProfile_Mobile,UserProfile_Email,UserProfile_FaceBookId,UserProfile_TwitterId From tbl_userprofile where UserProfile_Code='".$_SESSION['User_Code']."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
    
   
    
    
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_SelectQuery = "Select  * From 
                    tbl_userprofile Where UserProfile_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function DeleteRecord($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_DeleteQuery = "Delete From tbl_userprofile Where UserProfile_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Add($_txtFirstName,$_txtHouseNo,$_txtDOB,$_ddlMTongue,$_txtAddress,$_ddlGender,$_txtFatherName,$_ddlQualification,$_txtOccupation,
			$_ddlChallenged,$_AadharNo,$_txtSTDCode,$_txtMobile,$_txtEmail,$_txtFaceBookId,$_txtTwitterId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
$_InsertQuery = "Insert Into tbl_userprofile(UserProfile_Code,UserProfile_User,UserProfile_FirstName,
UserProfile_HouseNo,UserProfile_DOB,UserProfile_MTongue,UserProfile_Address,UserProfile_Gender,UserProfile_FatherName,
UserProfile_Qualification,UserProfile_Occupation,UserProfile_PhysicallyChallenged,UserProfile_AadharNo,UserProfile_STDCode,UserProfile_Mobile,UserProfile_Email,UserProfile_FaceBookId,UserProfile_TwitterId) 
			 Select Case When Max(UserProfile_Code) Is Null Then 1 Else Max(UserProfile_Code)+1 End as UserProfile_Code,
			 
			 '".$_SESSION['User_Code']."' as UserProfile_User,
			 '".$_txtFirstName."' as UserProfile_FirstName,
			 '" .$_txtHouseNo. "' as UserProfile_HouseNo,
			 '" . $_txtDOB. "' as UserProfile_DOB,
			 '" .$_ddlMTongue. "' as UserProfile_MTongue,
			 '" .$_txtAddress. "' as UserProfile_Address,
			 '" .$_ddlGender. "' as UserProfile_Gender,
			 '" .$_txtFatherName. "' as UserProfile_FatherName,
			 '" .$_txtOccupation. "' as UserProfile_Occupation,
			 '" .$_ddlChallenged. "' as UserProfile_PhysicallyChallenged,
			 '" .$_ddlQualification. "' as UserProfile_Qualification,
			 
			  '" .$_AadharNo. "' as UserProfile_AadharNo,
			 '" .$_txtSTDCode. "' as UserProfile_STDCode,
			 '" .$_txtMobile. "' as UserProfile_Mobile,
			 '" .$_txtEmail. "' as UserProfile_Email,
			 '" .$_txtFaceBookId. "' as UserProfile_FaceBookId,
			 '" .$_txtTwitterId. "' as UserProfile_TwitterId
			 From tbl_userprofile";
            $_DuplicateQuery = "Select * From tbl_userprofile Where UserProfile_User='" . $_SESSION['User_Code'] . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				//$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }  
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
       //print_r($_Response);
        return $_Response;
    }
    
    public function Update($_code,$_txtFirstName,$_txtHouseNo,$_txtDOB,$_ddlMTongue,$_txtAddress,$_ddlGender,$_txtFatherName,$_ddlQualification,$_txtOccupation,
			$_ddlChallenged,$_AadharNo,$_txtSTDCode,$_txtMobile,$_txtEmail,$_txtFaceBookId,$_txtTwitterId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_userprofile set UserProfile_FirstName='" . $_txtFirstName . "',UserProfile_HouseNo='" . $_txtHouseNo . "',UserProfile_DOB='" . $_txtDOB . "',UserProfile_Address='" . $_txtAddress . "',UserProfile_FatherName='" . $_txtFatherName . "',
                    UserProfile_MTongue='" . $_ddlMTongue . "',
					UserProfile_Qualification='" . $_ddlQualification . "',
					UserProfile_Occupation='" . $_txtOccupation . "',
					UserProfile_PhysicallyChallenged='" . $_ddlChallenged . "',
					UserProfile_AadharNo='" . $_AadharNo . "',
					UserProfile_STDCode='" . $_txtSTDCode . "',
					UserProfile_Mobile='" . $_txtMobile . "',
					UserProfile_Gender='" . $_ddlGender . "',
					UserProfile_Email='" . $_txtEmail . "',
					UserProfile_FaceBookId='" . $_txtFaceBookId . "',
					
					 	UserProfile_TwitterId='" . $_txtTwitterId . "'
					Where UserProfile_Code='" . $_code . "'";
            $_DuplicateQuery = "Select * From tbl_userprofile Where UserProfile_Code='" . $_code . "' and "
                    . "UserProfile_Code <> '" . $_code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
    

}

?>