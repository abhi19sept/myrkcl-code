<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrDDPayment
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrDDPayment {

    //put your code here

    public function Update($_AckCode, $_Code, $_PayTypeCode, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if(isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
		if(isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
            $_ddimage = $_TranRefNo.'_ddpayment.png';
            $_InsertQuery = "INSERT INTO tbl_ncr_ddpay (ncr_ddpay_code, dd_Transaction_Status, dd_Transaction_Name, dd_amount,"
                    . "dd_Transaction_Txtid, dd_no, dd_date,"
                    . "dd_rspcode,dd_bank,dd_branch,dd_image,dd_Transaction_Email) "
                    . "Select Case When Max(ncr_ddpay_code) Is Null Then 1 Else Max(ncr_ddpay_code)+1 End as ncr_ddpay_code,"
                    . "'Intermediate' as dd_Transaction_Status,'" . $_firstname . "' as dd_Transaction_Name,'" . $_amount . "' as dd_amount,"
                    . "'" . $_TranRefNo . "' as dd_Transaction_Txtid,"
                    . "'" . $_ddno . "' as dd_no,'" . $_dddate . "' as dd_date,'" . $_Code . "' as dd_rspcode,'" . $_ddlBankName . "' as dd_bank,'" . $_txtBranchName . "' as dd_branch,'" . $_ddimage . "' as dd_image,'" . $_email . "' as dd_Transaction_Email"
                    . " From tbl_ncr_ddpay";
            

            $_InsertQuery1 = "INSERT INTO tbl_ncr_transaction (Ncr_Transaction_Code, Ncr_Transaction_Status, Ncr_Transaction_Fname, Ncr_Transaction_Amount,"
                    . "Ncr_Transaction_Txtid, Ncr_Payment_Mode, Ncr_Transaction_ProdInfo, Ncr_Transaction_Email,"
                    . "Ncr_Transaction_CenterCode) "
                    . "Select Case When Max(Ncr_Transaction_Code) Is Null Then 1 Else Max(Ncr_Transaction_Code)+1 End as Ncr_Transaction_Code,"
                    . "'Intermediate' as Ncr_Transaction_Status,'" . $_firstname . "' as Ncr_Transaction_Fname,'" . $_amount . "' as Ncr_Transaction_Amount,"
                    . "'" . $_TranRefNo . "' as Ncr_Transaction_Txtid, 'DD Mode' as Ncr_Payment_Mode,"
                    . "'" . $_PayTypeCode . "' as Ncr_Transaction_ProdInfo,'" . $_email . "' as Ncr_Transaction_Email,'" . $_Code . "' as Ncr_Transaction_CenterCode"
                    . " From tbl_ncr_transaction";
            
            
            $ddscan = $_SERVER['DOCUMENT_ROOT'] . '/upload/Ncr_dd_payment/' . $_ddimage;
            
                if (file_exists($ddscan)) {
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response1 = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);

                    $_UpdateQuery = "Update tbl_org_master set Org_PayStatus = 8, Org_TranRefNo='" . $_TranRefNo . "'"
                            . " Where Org_RspLoginId='" . $_Code . "' AND Org_Ack =  '" . $_AckCode . "'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);                    
                } else {
                    echo "DD Image not attached successfully ,Please Re-Upload DD image";
                    return;
                }
            
                }else {
			  session_destroy();
				?>
				<script> window.location.href = "index.php"; </script> 
		<?php 
		  }
	
            }  else {
			  session_destroy();
				?>
				<script> window.location.href = "index.php"; </script> 
		<?php 
		  }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCenterCode($_code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT Correction_ITGK_Code, Correction_TranRefNo FROM tbl_correction_copy WHERE applicationfor = '" . $_code . "' AND Correction_Payment_Status = '8'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GETDD($_ackno) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.*, b.* FROM tbl_ncr_ddpay as a INNER JOIN tbl_org_master as b"
                    . " ON a.dd_Transaction_Txtid=b.Org_TranRefNo WHERE b.Org_Ack = '" . $_ackno . "' AND"
                    . " b.Org_PayStatus='8'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);            
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateDDPaymentStatus($Transactionid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_UpdateQuery = "Update tbl_ncr_ddpay set dd_Transaction_Status = 'Success'"
                    . " Where dd_Transaction_Txtid = '" . $Transactionid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_UpdateQuery1 = "Update tbl_ncr_transaction set Ncr_Transaction_Status = 'Success'"
                    . " Where Ncr_Transaction_Txtid = '" . $Transactionid . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);

            $_UpdateQuery2 = "Update tbl_org_master set Org_PayStatus = '1'"
                    . " Where Org_TranRefNo = '" . $Transactionid . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddCorrectionPayTran($_txtapplicationfor, $amount1, $_AdmissionCode, $txtGenerateId, $Lcount) {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {

            $_RKCL_Id = $txtGenerateId . '_RKCLtranid';

            $_ITGK_Code = $_SESSION['User_LoginId'];
            //$_Fname = $_SESSION['User_Code'];

            $_InsertQuery = "INSERT INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray,Pay_Tran_LCount,"
                    . "Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo) "
                    . "values('" . $_ITGK_Code . "' ,'" . $_RKCL_Id . "','" . $_AdmissionCode . "','" . $Lcount . "','" . $amount1 . "','PaymentInProcess','" . $_txtapplicationfor . "')";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            $_UpdateQuery = "Update tbl_correction_copy set Correction_RKCL_Trnid = '" . $_RKCL_Id . "' Where lcode IN ($_AdmissionCode)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }

}
