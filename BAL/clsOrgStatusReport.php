<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgStatusReport
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsOrgStatusReport {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {    
                $_SelectQuery = "select b.User_LoginId,a.*, c.Rspitgk_Rspname, d.District_Name,e.Tehsil_Name, f.Block_Name, g.GP_Name, h.Village_Name
                                    from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code 
                                    inner join tbl_rspitgk_mapping as c on c.Rspitgk_Itgkcode=b.User_LoginId 
                                    inner join tbl_district_master as d on a.Organization_District=d.District_Code
                                    inner join tbl_tehsil_master as e on a.Organization_Tehsil=e.Tehsil_Code
                                    left join tbl_panchayat_samiti as f on a.Organization_Panchayat=f.Block_Code
                                    left join tbl_gram_panchayat as g on a.Organization_Gram=g.GP_Code
                                    left join tbl_village_master as h on a.Organization_Village=h.Village_Code
                                    where  Rspitgk_Status='Approved' and NOW()<= Rspitgk_End_Date
                                    ";
                 } else {
                     $_SelectQuery = "select b.User_LoginId,a.*, c.Rspitgk_Rspname, d.District_Name,e.Tehsil_Name, f.Block_Name, g.GP_Name, h.Village_Name
                                    from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code 
                                    inner join tbl_rspitgk_mapping as c on c.Rspitgk_Itgkcode=b.User_LoginId 
                                    inner join tbl_district_master as d on a.Organization_District=d.District_Code
                                    inner join tbl_tehsil_master as e on a.Organization_Tehsil=e.Tehsil_Code
                                    left join tbl_panchayat_samiti as f on a.Organization_Panchayat=f.Block_Code
                                    left join tbl_gram_panchayat as g on a.Organization_Gram=g.GP_Code
                                    left join tbl_village_master as h on a.Organization_Village=h.Village_Code
                                    where  Rspitgk_Status='Approved' and NOW()<= Rspitgk_End_Date
                                    AND b.User_Rsp='" .$_SESSION['User_Code'] . "' GROUP BY b.User_LoginId
                                    ";
                 }
    
            
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
