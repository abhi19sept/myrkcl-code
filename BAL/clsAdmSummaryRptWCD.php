<?php

/**
 * Description of clsAdmSummaryRptWCD
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmSummaryRptWCD {

    //put your code here
	/* for course function */
	
	public function Getcourse() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select Course_Code, Course_Name From tbl_course_master where Course_Code='3' or Course_Code='24' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	/* for Batch function */
	
	public function Getbatch($_course) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
		    $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '".$_course."' order by Batch_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function GetDatafirst($batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
            $batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
            
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11') {
                $_SelectQuery3 = "select Oasis_Admission_Final_Preference,ITGK_Name,District_Name ,
                                            sum(case when Oasis_Admission_Category = '2' then 1 else 0 end) SCcount ,
                                            sum(case when Oasis_Admission_Category = '3' then 1 else 0 end) STcount ,
                                            sum(case when Oasis_Admission_Category NOT IN (2,3) then 1 else 0 end) Othcount from 
                                            tbl_oasis_admission as a inner join vw_itgkname_distict_rsp as b on 
                                            a.Oasis_Admission_Final_Preference=b.ITGKCODE  
                                            where Oasis_Admission_Transfer_Status='Transferred' AND
											 Oasis_Admission_LearnerStatus='Approved' and
											Oasis_Admission_Batch = '" . $batchcode . "'
                                            group by Oasis_Admission_Final_Preference";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDataPO($batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
            $batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
            
            if ($_LoginUserRole == '17') {
                $_SelectQuery3 = "select Oasis_Admission_Final_Preference,ITGK_Name,b.District_Name ,
                                                sum(case when Oasis_Admission_Category = '2' then 1 else 0 end) SCcount ,
                                                sum(case when Oasis_Admission_Category = '3' then 1 else 0 end) STcount ,
                                                sum(case when Oasis_Admission_Category NOT IN (2,3) then 1 else 0 end) Othcount from 
                                                tbl_oasis_admission as a inner join vw_itgkname_distict_rsp as b on 
                                                a.Oasis_Admission_Final_Preference=b.ITGKCODE inner join tbl_district_master as c on
                                                b.District_Name=c.District_Name where Oasis_Admission_Transfer_Status='Transferred' 
												and Oasis_Admission_LearnerStatus='Approved'
                                                AND Oasis_Admission_Batch = '" . $batchcode . "' AND c.District_Code='" . $_SESSION['Organization_District'] . "'
                                                group by Oasis_Admission_Final_Preference";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
