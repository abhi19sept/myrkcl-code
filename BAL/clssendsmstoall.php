<?php


/**
 * Description of clsOrgDetail
 *
 * @author  yogendra soni
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php'; 

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clssendsmstoall {
	
	 
		
    public function ADD($_mobile,$_Msg,$_date) 
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
			$countno=explode(',', $_mobile);
			
			$countno1 = count($countno);
			$_ITGK_Code =   $_SESSION['User_LoginId'];
			$_InsertQuery = "Insert Into tbl_sms(id,Mobile,sms,status,centercode,Date,count) 
			 Select Case When Max(id) Is Null Then 1 Else Max(id)+1 End as id,
			
			 '" . $_mobile. "' as Mobile,
			 '" .$_Msg. "' as sms,
			 '1' as status,
			 '" .$_ITGK_Code. "' as centercode,
			 '" .$_date. "' as Date,
			 '" .$countno1. "' as count
			 From tbl_sms";
			 if($_date==0)
			 {
				 SendSMS($_mobile, $_Msg);
			 }
			 /* $arr_length = count($_mobile); 
			 echo $arr_length; */
			 //SendSMS($_mobile, $_Msg);
			 
			
			 
			 $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					
				
               
            
        }
		catch (Exception $_e) 
		{
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	
	
	public function GetMsg() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code =   $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='".$_ITGK_Code."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAll($_ITGK_Code) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
				
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$totalpackage=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$totalpackage = $totalpackage + $_Row1['package'];
		      } 
			
			$temp=0;
			$_SelectQuery1 = "Select count from tbl_sms where centercode='$_ITGK_Code'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			while ($_data = mysqli_fetch_array($_Response1[2])) 
			{
				$temp=$temp+$_data['count'];
				
				
		    }
			//print_r($temp);
			$_Response[2]=$totalpackage-$temp;
			$_Response[3]=$temp;
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function GetTotalpackage($_ITGK_Code) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
				
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$tot=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$tot = $tot + $_Row1['package'];
		      } 	
			
			
			$_Response[2]=$tot;
			
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	

	
	public function GetRsp() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "select User_Rsp,User_LoginId from tbl_user_master where User_UserRoll='14' ORDER BY User_UserRoll DESC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function GetDistrict() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "Select * from tbl_district_master order by District_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    } 
	
	
	
	
	public function GetcenterList($_district,$_tehsil,$_Rspcode) 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			if($_Rspcode=='14')
			{
				 $_SelectQuery = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											where User_UserRoll='".$_Rspcode."' AND Organization_District='" . $_district . "' AND Organization_Tehsil IN(" . $_tehsil . ")
											order by a.Organization_Name";
				
			}
			else if($_Rspcode=='7')
			{
			 $_SelectQuery = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='".$_Rspcode."' AND Organization_District='" . $_district . "' AND Organization_Tehsil IN(" . $_tehsil . ") AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock'
											order by a.Organization_Name";
					    
			}
			else{ "Not done";}
			$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	public function GetMobilenumber($_activate,$_rspcode) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				$_rspcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rspcode);
				
		    $_SelectQuery = "Select distinct User_MobileNo from tbl_user_master where User_LoginId IN (".$_activate.") AND  User_UserRoll='".$_rspcode."' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function GetMobilenumberrsp($_activate,$_rspcode) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				$_rspcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rspcode);
				
		    $_SelectQuery = "Select distinct User_MobileNo from tbl_user_master where User_LoginId IN ('".$_activate."') AND  User_UserRoll='".$_rspcode."' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetCentermobile($_center,$_rspcode) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
				$_center = mysqli_real_escape_string($_ObjConnection->Connect(),$_center);
				$_rspcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rspcode);
				
		   $_SelectQuery = "Select distinct User_MobileNo from tbl_user_master where User_LoginId IN (".$_center.") AND 
								User_UserRoll='".$_rspcode."' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function GetCentermobilersp($_rsp,$_rspcode) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
				$_rspcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rspcode);
				
			$a = "'" . implode("','", explode(',', $_rsp)) . "'";
		  $_SelectQuery = "Select distinct User_MobileNo from tbl_user_master where User_LoginId IN ($a) AND  User_UserRoll='".$_rspcode."' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	public function Getstatus() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_ITGK_Code = $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select Payment_Status from tbl_package_transections where  centercode='".$_ITGK_Code."'  ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetalldistrictCenter($_district) 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
				 $_SelectQuery3 = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='7' AND Organization_District IN(" . $_district . ") AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock'
											order by a.Organization_Name";
					    
						
						
			$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	
	
	
	public function GetalldistrictRsp($_district,$_Rsp) 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			$_Rsp = mysqli_real_escape_string($_ObjConnection->Connect(),$_Rsp);
			
				 $_SelectQuery3 = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											where User_UserRoll='".$_Rsp."' AND Organization_District IN(" . $_district . ")
											order by a.Organization_Name";
					    
						
						
			$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	public function GetalltehsilCenter($_district,$_tehsil,$_rspcode) 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try 
	{		
		$_district = mysqli_real_escape_string($_ObjConnection->Connect(),$_district);
		$_rspcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rspcode);
		$_tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_tehsil);
		
	if($_rspcode=='14'){
				 $_SelectQuery3 = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											where User_UserRoll='".$_rspcode."' AND Organization_District='" . $_district . "' AND Organization_Tehsil IN(" . $_tehsil . ") ";
					    
				}
				else if($_rspcode=='7')
				{
				 $_SelectQuery3 = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='".$_rspcode."' AND Organization_District='" . $_district . "' AND Organization_Tehsil IN(" . $_tehsil . ") AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock' 
											order by a.Organization_Name ";
					
				}
				else
				{ echo "not defined";}
						
				$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
				return $_Response3;
	}
	catch (Exception $_ex) 
	{

		$_Response[0] = $_ex->getLine() . $_ex->getTrace();
		$_Response[1] = Message::Error;
	}
        return $_Response;
    }
	
	
	
	
	public function Getallmobile() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			
		   $_SelectQuery = "Select distinct User_MobileNo from tbl_user_master";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
}
