<?php

/**
 * Description of clsClickAdmission
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

require 'DAL/upload_ftp_doc.php';

$_ObjFTPConnection = new ftpConnection();

$_ObjConnection = new _Connection();
$_Response = array();

class clsClickAdmission {

//put your code here

public function GetAdmissionCourseName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code, Course_Name From tbl_course_master WHERE `Course_Code` = '23'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function GetAdmissionBatchName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b 
								ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '23' AND 
								CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='1'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }	

public function Getclickadmissiondetails($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_SelectQuery = "Select * From tbl_click_admission_details WHERE CLICK_Admission_LearnerCode='".$lcode."'
										and CLICK_Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
				else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
            }           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }    
    
    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select Course_Fee FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionInstall($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select Admission_Installation_Mode FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetIntakeAvailable($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_ITGK_Code = $_SESSION['User_LoginId'];

            $_SelectQuery3 = "Select Max(Batch_Code) as bc from tbl_batch_master where Course_Code = '1'";

            $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response2[2]);
            $_batchcode=$_Row1['bc'];

            $_SelectQuery = "Select Intake_Available FROM tbl_intake_master WHERE Intake_Center = '" . $_ITGK_Code . "'
							AND Intake_Batch = '" . $_batchcode . "'";
 //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function Add($_LearnerName, $_LearnerCode, $_ParentName, $_IdProof, $_IdNo, $_DOB, $_MotherTongue, $_Medium,
						$_Gender, $_MaritalStatus, $_District, $_Tehsil, $_Address, $_ResiPh, $_Mobile, $_Qualification,
						$_LearnerType, $_GPFno, $_PhysicallyChallenged, $_Email, $_PIN, $_LearnerCourse, $_LearnerBatch, 
						$_LearnerFee, $_LearnerInstall,$_isaadhar, $_BatchName){
		global $_ObjFTPConnection;
		global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$_SMS = "प्रिय  " . $_LearnerName . ", आपका एड्मिशन RKCL सर्वर पर अपलोड हो गया है| एड्मिशन की फीस भुगतान के लिए आप अपने ज्ञान केंद्र से संपर्क करें |   RKCL से किसी भी प्रकार के संवाद के लिए आपका लर्नर कोड" . $_LearnerCode. " है|" ;
        try {
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                if (isset($_SESSION['User_Rsp']) && !empty($_SESSION['User_Rsp']) && $_SESSION['User_Rsp'] != '0') {
                        $_photoname = $_LearnerCode . '_photo' . '.png';
                        $_signname = $_LearnerCode . '_sign' . '.png';
                        $_ITGK_Code = $_SESSION['User_LoginId'];
                        $_User_Code = $_SESSION['User_Code'];
                        $_User_Rsp = $_SESSION['User_Rsp'];					

				    if ($_POST["ddlAadhar"] == "n") {
                            $aadharstatus = '0';
					} else {
						$aadharstatus = '1';
					}

                         $_InsertQuery = "Insert Into tbl_admission(Admission_Code,Admission_LearnerCode,Admission_Aadhar_Status,"
                                . "Admission_ITGK_Code,Admission_Course,Admission_Batch,"
                                . "Admission_Fee,Admission_Installation_Mode,Admission_Name,Admission_Fname,Admission_DOB,"
                                . "Admission_MTongue,Admission_Photo,Admission_Sign,Admission_Gender,"
                                . "Admission_MaritalStatus,Admission_Medium,Admission_PH,Admission_PID,"
                                . "Admission_UID,Admission_District,Admission_Tehsil,Admission_Address,"
                                . "Admission_PIN,Admission_Mobile,Admission_Phone,Admission_Email,User_Code,"
                                . "Admission_Qualification,Admission_Ltype,Admission_GPFNO,Admission_RspName) "
                                . "Select Case When Max(Admission_Code) Is Null Then 1 Else Max(Admission_Code)+1 End as Admission_Code,"
                                . "'" . $_LearnerCode . "' as Admission_LearnerCode, '" . $aadharstatus . "' as Admission_Aadhar_Status,'" . $_ITGK_Code . "' as Admission_ITGK_Code,"
                                . "'" . $_LearnerCourse . "' as Admission_Course,  '" . $_LearnerBatch . "' as Admission_Batch,"
                                . "'" . $_LearnerFee . "' as Admission_Fee,'" . $_LearnerInstall . "' as Admission_Installation_Mode,'" . $_LearnerName . "' as Admission_Name,'" . $_ParentName . "' as Admission_Fname,"
                                . "'" . $_DOB . "' as Admission_DOB,'" . $_MotherTongue . "' as Admission_MTongue,'" . $_photoname . "' as Admission_Photo,'" . $_signname . "' as Admission_Sign,"
                                . "'" . $_Gender . "' as Admission_Gender,"
                                . "'" . $_MaritalStatus . "' as Admission_MaritalStatus,'" . $_Medium . "' as Admission_Medium,"
                                . "'" . $_PhysicallyChallenged . "' as Admission_PH,'" . $_IdProof . "' as Admission_PID,"
                                . "'" . $_IdNo . "' as Admission_UID,'" . $_District . "' as Admission_District,"
                                . "'" . $_Tehsil . "' as Admission_Tehsil,'" . $_Address . "' as Admission_Address,"
                                . "'" . $_PIN . "' as Admission_PIN,'" . $_Mobile . "' as Admission_Mobile,'" . $_ResiPh . "' as Admission_Phone,"
                                . "'" . $_Email . "' as Admission_Email,'" . $_User_Code . "' as User_Code,'" . $_Qualification . "' as Admission_Qualification,"
                                . "'" . $_LearnerType . "' as Admission_Ltype,'" . $_GPFno . "' as Admission_GPFNO, '" . $_User_Rsp . "' as Admission_RspName"
                                . " From tbl_admission";


                         $_chkDuplicate_learnerCode = "Select Admission_LearnerCode From tbl_admission Where Admission_LearnerCode='" . $_LearnerCode . "'";
                        $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate_learnerCode, Message::SelectStatement);
                        if ($_Response2[0] == Message::NoRecordFound) {
                            $_DuplicateQuery = "Select * From tbl_admission Where Admission_Name='" . $_LearnerName . "' AND Admission_Fname = '" . $_ParentName . "' AND Admission_DOB = '" . $_DOB . "' AND Admission_Course = '" . $_LearnerCourse . "' AND Admission_Batch = '" . $_LearnerBatch . "' AND Admission_ITGK_Code = '" . $_ITGK_Code . "'";
                            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                            $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                                    . "Admission_Log_ITGK_Code,"
                                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                                    . "Select Case When Max(Admission_Log_Code) Is Null Then 1 Else Max(Admission_Log_Code)+1 End as Admission_Log_Code,"
                                    . "'" . $_ITGK_Code . "' as Admission_Log_ITGK_Code,"
                                    . "'" . $_photoname . "' as Admission_Log_Photo,'" . $_signname . "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"
                                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                                    . " From tbl_admission_log";
                            if ($_Response[0] == Message::NoRecordFound) {
								
								$_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
								$_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';
								
								$ftpaddress = $_ObjFTPConnection->ftpPathIp();
								
								$lphotodoc = $ftpaddress.'admission_photo/' . $_LearnerBatchNameFTP . '/' . $_photoname;
                                 $lsigndoc = $ftpaddress.'admission_sign/' . $_LearnerBatchNameFTP . '/' . $_signname;
								 
					
                                //$lphotodoc = 'http://49.50.64.199/upload/admission_photo/' . $_photoname;
                                //$lsigndoc = 'http://49.50.64.199/upload/admission_sign/' . $_signname;
								
								$ch = curl_init($lphotodoc);
                                curl_setopt($ch, CURLOPT_NOBODY, true);
                                curl_setopt($ch,CURLOPT_TIMEOUT,5000);
                                curl_exec($ch);
                                  $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                curl_close($ch);
                                
                                $chsign = curl_init($lsigndoc);
                                curl_setopt($chsign, CURLOPT_NOBODY, true);
                                curl_setopt($chsign,CURLOPT_TIMEOUT,5000);
                                curl_exec($chsign);
                                  $responseCodeSign = curl_getinfo($chsign, CURLINFO_HTTP_CODE);
                                curl_close($chsign);



                                if ($responseCodePhoto == 200 && $responseCodeSign == 200) {
									$_UpdateQuery = "Update tbl_click_admission_details set Myrkcl_Transfer_Status=1
													 Where CLICK_Admission_ITGK_Code = '" . $_ITGK_Code . "' AND 
													 CLICK_Admission_LearnerCode = '".$_POST['txtclcode']."'";
                                    $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                    $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
                                    SendSMS($_Mobile, $_SMS);
                                } else {
                                    echo "Photo and Sign not attached successfully ,Please Re-Upload Photo-Sign";
                                    return;
                                }
                            } else {								
                                $_Response[0] = Message::DuplicateRecord;
                                $_Response[1] = Message::Error;
                            }
                        } else {							
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
				} else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php

				}
			} else {
				session_destroy();
				?>
				<script> window.location.href = "logout.php";</script> 
				<?php

			}
		} else {
			session_destroy();
			?>
			<script> window.location.href = "logout.php";</script> 
			<?php

		}
		} catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
		
	}
	public function GetBatchName($_LearnerCourse,$_LearnerBatch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Batch_Name from tbl_batch_master where Course_Code='".$_LearnerCourse."' and 
							Batch_Code='".$_LearnerBatch."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
