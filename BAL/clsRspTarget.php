<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsRspTarget
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRspTarget {
    //put your code here
     public function Add($_District,$_TwoMonthRural,$_TwoMonthUrban,$_FourMonthRural,$_FourMonthUrban,$_SixMonthRural,$_SixMonthUrban,$_Total) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_User = $_SESSION['User_LoginId'];
            $_InsertQuery = "Insert Into tbl_rsptarget(Rsptarget_Code,Rsptarget_User,Rsptarget_District,Rsptarget_2MonthRural,"
                    . "Rsptarget_2MonthUrban,Rsptarget_4MonthRural,Rsptarget_4MonthUrban,Rsptarget_6MonthRural,"
                    . "Rsptarget_6MonthUrban,Rsptarget_Total) "
                    . "Select Case When Max(Rsptarget_Code) Is Null Then 1 Else Max(Rsptarget_Code)+1 End as Rsptarget_Code,"
                    . "'" . $_User . "' as Rsptarget_User,'" . $_District . "' as Rsptarget_District,'" . $_TwoMonthRural . "' as Rsptarget_2MonthRural,"
                    . "'" . $_TwoMonthUrban . "' as Rsptarget_2MonthUrban,'" . $_FourMonthRural . "' as Rsptarget_4MonthRural,"
                    . "'" . $_FourMonthUrban . "' as Rsptarget_4MonthUrban,'" . $_SixMonthRural . "' as Rsptarget_6MonthRural,"
                    . "'" . $_SixMonthUrban . "' as Rsptarget_6MonthUrban,'" . $_Total . "' as Rsptarget_Total"
                    . " From tbl_rsptarget";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_rsptarget Where Rsptarget_District='" . $_District . "' AND
							Rsptarget_User='" . $_User . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User = $_SESSION['User_LoginId'];
            $_SelectQuery = "Select a.*,b.District_Name"
                    . " From tbl_rsptarget as a inner join tbl_district_master as b "
                    . "on a.Rsptarget_District"
                    . "=b.District_Code WHERE Rsptarget_User='" . $_User . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
