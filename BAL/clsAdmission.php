<?php

/**
 * Description of clsAdmission
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/upload_ftp_doc.php';

$_ObjFTPConnection = new ftpConnection();

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmission {

//put your code here
    public function Add($_LearnerName, $_LearnerCode, $_ParentName, $_IdProof, $_IdNo, $_DOB, $_MotherTongue, $_Medium, $_Gender, $_MaritalStatus, $_District, $_Tehsil, $_Address, $_ResiPh, $_Mobile, $_Qualification, $_LearnerType, $_GPFno, $_PhysicallyChallenged, $_Email, $_PIN, $_LearnerCourse, $_LearnerBatch, $_LearnerFee, $_LearnerInstall, $_Photo, $_Sign,$_isaadhar) {

        global $_ObjFTPConnection;
        global $_ObjConnection;
        $_ObjConnection->Connect();
    //    $_SMS = "Dear Learner, " . $_LearnerName . " Your Admission has been Uploaded to RKCL Server. Please ask your ITGK to Confirm your payment to RKCL. Please Note your Learner Code for further Communication with RKCL " . $_LearnerCode;
		
        try {
            
            $_LearnerName = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerName); 
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode); 
            $_ParentName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentName); 
            $_IdProof = mysqli_real_escape_string($_ObjConnection->Connect(),$_IdProof); 
            $_IdNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_IdNo); 
            $_DOB = mysqli_real_escape_string($_ObjConnection->Connect(),$_DOB); 
            $_MotherTongue = mysqli_real_escape_string($_ObjConnection->Connect(),$_MotherTongue); 
            $_Medium = mysqli_real_escape_string($_ObjConnection->Connect(),$_Medium); 
            $_Gender = mysqli_real_escape_string($_ObjConnection->Connect(),$_Gender); 
            $_MaritalStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_MaritalStatus); 
            $_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District); 
            $_Tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil); 
            $_Address = mysqli_real_escape_string($_ObjConnection->Connect(),$_Address); 
            $_ResiPh = mysqli_real_escape_string($_ObjConnection->Connect(),$_ResiPh); 
            $_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile); 
            $_Qualification = mysqli_real_escape_string($_ObjConnection->Connect(),$_Qualification); 
            $_LearnerType = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerType); 
            $_GPFno = mysqli_real_escape_string($_ObjConnection->Connect(),$_GPFno); 
            $_PhysicallyChallenged = mysqli_real_escape_string($_ObjConnection->Connect(),$_PhysicallyChallenged); 
            $_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email); 
            $_PIN = mysqli_real_escape_string($_ObjConnection->Connect(),$_PIN); 
            $_LearnerCourse = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCourse); 
            $_LearnerBatch = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerBatch); 
            $_LearnerFee = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerFee); 
            $_LearnerInstall = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerInstall); 
            $_Photo = mysqli_real_escape_string($_ObjConnection->Connect(),$_Photo); 
            $_Sign = mysqli_real_escape_string($_ObjConnection->Connect(),$_Sign);
            $_isaadhar = mysqli_real_escape_string($_ObjConnection->Connect(),$_isaadhar);
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                    if (isset($_SESSION['User_Rsp']) && !empty($_SESSION['User_Rsp']) && $_SESSION['User_Rsp'] != '0') {
                        $_photoname = $_LearnerCode . '_photo' . '.png';
                        $_signname = $_LearnerCode . '_sign' . '.png';
                        $_ITGK_Code = $_SESSION['User_LoginId'];
                        $_User_Code = $_SESSION['User_Code'];
                        $_User_Rsp = $_SESSION['User_Rsp'];

                                               if ($_isaadhar == "n") {

                            $aadharstatus = '0';
                        } else {
                            $aadharstatus = '1';
                        }
					$_LearnerBatchName="";
					$_LearnerCourseName="";
					
					$select_Batch="select Batch_Name from tbl_batch_master where Batch_Code='".$_LearnerBatch."'";
					$_ResponseBatch = $_ObjConnection->ExecuteQuery($select_Batch, Message::SelectStatement);
						if($_ResponseBatch[0]=="Success"){
							$_Row3 = mysqli_fetch_array($_ResponseBatch[2]);
                            $_LearnerBatchName=$_Row3['Batch_Name'];
						}
                    
					$select_Course="select Course_Name from tbl_course_master where Course_Code='".$_LearnerCourse."'";
					$_ResponseCourse = $_ObjConnection->ExecuteQuery($select_Course, Message::SelectStatement);
						if($_ResponseCourse[0]=="Success"){
							$_Row4 = mysqli_fetch_array($_ResponseCourse[2]);
                            $_LearnerCourseName=$_Row4['Course_Name'];
						}
						
					if($_LearnerBatchName!="" && $_LearnerCourseName!=""){
						$_SMS = "प्रिय  " . $_LearnerName . ", " .$_LearnerCourseName." ". $_LearnerBatchName."  बैच मे आपका एड्मिशन RKCL सर्वर पर अपलोड हो गया है|   RKCL से किसी भी प्रकार के संवाद के लिए आपका लर्नर कोड " . $_LearnerCode. " है|" ;
					}
					else{
						$_SMS = "प्रिय  " . $_LearnerName . ", आपका एड्मिशन RKCL सर्वर पर अपलोड हो गया है| अब आप अपने ITGK से संपर्क कर ऑनलाइन प्रशिक्षण प्रारंभ करें |   RKCL से किसी भी प्रकार के संवाद के लिए आपका लर्नर कोड" . $_LearnerCode. " है|" ;
					}					
					
                        $_InsertQuery = "Insert Into tbl_admission(Admission_Code,Admission_LearnerCode,Admission_Aadhar_Status,"
                                . "Admission_ITGK_Code,Admission_Course,Admission_Batch,"
                                . "Admission_Fee,Admission_Installation_Mode,Admission_Name,Admission_Fname,Admission_DOB,"
                                . "Admission_MTongue,Admission_Photo,Admission_Sign,Admission_Gender,"
                                . "Admission_MaritalStatus,Admission_Medium,Admission_PH,Admission_PID,"
                                . "Admission_UID,Admission_District,Admission_Tehsil,Admission_Address,"
                                . "Admission_PIN,Admission_Mobile,Admission_Phone,Admission_Email,User_Code,"
                                . "Admission_Qualification,Admission_Ltype,Admission_GPFNO,Admission_RspName) "
                                . "Select Case When Max(Admission_Code) Is Null Then 1 Else Max(Admission_Code)+1 End as Admission_Code,"
                                . "'" . $_LearnerCode . "' as Admission_LearnerCode, '" . $aadharstatus . "' as Admission_Aadhar_Status,'" . $_ITGK_Code . "' as Admission_ITGK_Code,"
                                . "'" . $_LearnerCourse . "' as Admission_Course,  '" . $_LearnerBatch . "' as Admission_Batch,"
                                . "'" . $_LearnerFee . "' as Admission_Fee,'" . $_LearnerInstall . "' as Admission_Installation_Mode,'" . $_LearnerName . "' as Admission_Name,'" . $_ParentName . "' as Admission_Fname,"
                                . "'" . $_DOB . "' as Admission_DOB,'" . $_MotherTongue . "' as Admission_MTongue,'" . $_photoname . "' as Admission_Photo,'" . $_signname . "' as Admission_Sign,"
                                . "'" . $_Gender . "' as Admission_Gender,"
                                . "'" . $_MaritalStatus . "' as Admission_MaritalStatus,'" . $_Medium . "' as Admission_Medium,"
                                . "'" . $_PhysicallyChallenged . "' as Admission_PH,'" . $_IdProof . "' as Admission_PID,"
                                . "'" . $_IdNo . "' as Admission_UID,'" . $_District . "' as Admission_District,"
                                . "'" . $_Tehsil . "' as Admission_Tehsil,'" . $_Address . "' as Admission_Address,"
                                . "'" . $_PIN . "' as Admission_PIN,'" . $_Mobile . "' as Admission_Mobile,'" . $_ResiPh . "' as Admission_Phone,"
                                . "'" . $_Email . "' as Admission_Email,'" . $_User_Code . "' as User_Code,'" . $_Qualification . "' as Admission_Qualification,"
                                . "'" . $_LearnerType . "' as Admission_Ltype,'" . $_GPFno . "' as Admission_GPFNO, '" . $_User_Rsp . "' as Admission_RspName"
                                . " From tbl_admission";


                        $_chkDuplicate_learnerCode = "Select Admission_LearnerCode From tbl_admission Where Admission_LearnerCode='" . $_LearnerCode . "'";
                        $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate_learnerCode, Message::SelectStatement);
                        if ($_Response2[0] == Message::NoRecordFound) {
                            $_DuplicateQuery = "Select * From tbl_admission Where Admission_Name='" . $_LearnerName . "' AND Admission_Fname = '" . $_ParentName . "' AND Admission_DOB = '" . $_DOB . "' AND Admission_Course = '" . $_LearnerCourse . "' AND Admission_Batch = '" . $_LearnerBatch . "' AND Admission_ITGK_Code = '" . $_ITGK_Code . "'";
                            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                            $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                                    . "Admission_Log_ITGK_Code,"
                                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                                    . "Select Case When Max(Admission_Log_Code) Is Null Then 1 Else Max(Admission_Log_Code)+1 End as Admission_Log_Code,'".$_LearnerCode."' as Admission_Log_LearnerCode,"
                                    . "'" . $_ITGK_Code . "' as Admission_Log_ITGK_Code,"
                                    . "'" . $_photoname . "' as Admission_Log_Photo,'" . $_signname . "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"
                                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                                    . " From tbl_admission_log";
                            if ($_Response[0] == Message::NoRecordFound) {


//$_LearnerBatchNameFTP = str_replace(' ', '_', $_LearnerBatchName);

$_Course = $_LearnerCourse;
$_BatchName =  $_LearnerBatchName;

                if ($_Course == 5) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);                    

                } elseif ($_Course == 4) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {

                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
                                
 $ftpaddress = $_ObjFTPConnection->ftpPathIp();
                                 $AdmissionPhotoFile = $ftpaddress.'admission_photo/' . $_LearnerBatchNameFTP . '/' . $_photoname;
                                 $AdmissionSignFile = $ftpaddress.'admission_sign/' . $_LearnerBatchNameFTP . '/' . $_signname;
                                
                    //$AdmissionPhotoFile = 'http://49.50.64.199/docrkcl/admission_photo/' . $_photoname;
                            //  $AdmissionSignFile = 'http://49.50.64.199/docrkcl/admission_sign/' . $_signname;
                                // Initialize cURL
                                $ch = curl_init($AdmissionPhotoFile);
                                curl_setopt($ch, CURLOPT_NOBODY, true);
                                curl_setopt($ch,CURLOPT_TIMEOUT,5000);
                                curl_exec($ch);
                                  $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                curl_close($ch);
                                
                                $chsign = curl_init($AdmissionSignFile);
                                curl_setopt($chsign, CURLOPT_NOBODY, true);
                                curl_setopt($chsign,CURLOPT_TIMEOUT,5000);
                                curl_exec($chsign);
                                  $responseCodeSign = curl_getinfo($chsign, CURLINFO_HTTP_CODE);
                                curl_close($chsign);

                                // Check the response code
                                if($responseCodePhoto == 200 && $responseCodeSign == 200){
                                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                    $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
                                    SendSMS($_Mobile, $_SMS);



                                    if ($_LearnerCourse == "1" || $_LearnerCourse == "4") {

                                        if ($_LearnerCourse == "4") {
                                            $_GetRscitBatch = "Select max(Intake_Batch) as bc From tbl_intake_master Where Intake_Course='1'";
                                            $_Response3 = $_ObjConnection->ExecuteQuery($_GetRscitBatch, Message::SelectStatement);
                                            $_Row1 = mysqli_fetch_array($_Response3[2]);
                                             $_LearnerBatch=$_Row1['bc'];
                                        }
                                        $_UpdateQuery = "Update tbl_intake_master set Intake_Available=Intake_Available - 1 ,Intake_Consumed = Intake_Consumed + 1 Where Intake_Center = '" . $_ITGK_Code . "' AND Intake_Batch = '" . $_LearnerBatch . "' ";
                                        $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                                    }
                                }else{
                                    echo "Photo and Sign not attached successfully ,Please Re-Upload Photo-Sign";
                                    return;
                                }

                                // $lphotodoc = '../upload/admission_photo/' . $_photoname;
                                // $lsigndoc = '../upload/admission_sign/' . $_signname;


                                // if (file_exists($lphotodoc) && file_exists($lsigndoc)) {
                                //     $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                //     $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
                                //     SendSMS($_Mobile, $_SMS);



                                //     if ($_LearnerCourse == "1" || $_LearnerCourse == "4") {

                                //         if ($_LearnerCourse == "4") {
                                //             $_GetRscitBatch = "Select max(Intake_Batch) as bc From tbl_intake_master Where Intake_Course='1'";
                                //             $_Response3 = $_ObjConnection->ExecuteQuery($_GetRscitBatch, Message::SelectStatement);
                                //             $_Row1 = mysqli_fetch_array($_Response3[2]);
                                //              $_LearnerBatch=$_Row1['bc'];
                                //         }
                                //         $_UpdateQuery = "Update tbl_intake_master set Intake_Available=Intake_Available - 1 ,Intake_Consumed = Intake_Consumed + 1 Where Intake_Center = '" . $_ITGK_Code . "' AND Intake_Batch = '" . $_LearnerBatch . "' ";
                                //         $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                                //     }
                                // } else {
                                //     echo "Photo and Sign not attached successfully ,Please Re-Upload Photo-Sign";
                                //     return;
                                // }
                            } else {
                                $_Response[0] = Message::DuplicateRecord;
                                $_Response[1] = Message::Error;
                            }
                        } else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
                    } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

                    }
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_LearnerName = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerName);
            $_ParentName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentName);
            $_DOB = mysqli_real_escape_string($_ObjConnection->Connect(),$_DOB);
            $newpicture = mysqli_real_escape_string($_ObjConnection->Connect(),$newpicture);
            $newsign = mysqli_real_escape_string($_ObjConnection->Connect(),$newsign);
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_User_Code = $_SESSION['User_Code'];

            $_UpdateQuery = "Update tbl_admission set Admission_Name='" . $_LearnerName . "',"
                    . "Admission_Fname='" . $_ParentName . "'," . "Admission_DOB='" . $_DOB . "', " . "Admission_Photo='" . $newpicture . "',"
                    . "Admission_Sign='" . $newsign . "'"
                    . " Where Admission_Code='" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                    . "Admission_Log_ITGK_Code,"
                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                    . "'" . $_Code . "' as Admission_Log_Code,"
                    . "'" . $_ITGK_Code . "' as Admission_Log_ITGK_Code,"
                    . "'" . $newpicture . "' as Admission_Log_Photo,'" . $newsign . "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"
                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                    . " From tbl_admission_log";
            $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_AdmissionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AdmissionCode);
            $_SelectQuery = "Select * From tbl_admission Where Admission_Code='" . $_AdmissionCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCodeForUpdate($_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_AdmissionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AdmissionCode);
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_SelectQuery = "Select ad.*, bm.Batch_Name From tbl_admission ad INNER JOIN tbl_event_management em ON em.Event_Batch = ad.Admission_Batch inner join tbl_batch_master as bm on ad.Admission_Batch=bm.Batch_Code  Where ad.Admission_Code = '" . $_AdmissionCode . "' AND ad.Admission_ITGK_Code = '" . $_ITGK_Code . "' AND CURDATE() >= em.Event_Startdate AND CURDATE() <= em.Event_Enddate AND em.Event_Name='2'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select Course_Fee FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
//echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionInstall($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select Admission_Installation_Mode FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
// echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetIntakeAvailable($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_ITGK_Code = $_SESSION['User_LoginId'];

            $_SelectQuery3 = "Select Max(Batch_Code) as bc from tbl_batch_master where Course_Code = '1'";

            $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response2[2]);
            $_batchcode=$_Row1['bc'];

            $_SelectQuery = "Select Intake_Available FROM tbl_intake_master WHERE Intake_Center = '" . $_ITGK_Code . "' AND Intake_Batch = '" . $_batchcode . "'";
 //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
