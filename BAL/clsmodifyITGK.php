<?php

require_once 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/smtp_class.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsmodifyITGK {

    //put your code here

	

    public function SHOWDATA($_activate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginUserRole = $_SESSION['User_UserRoll'];
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11') {

                $_SelectQuery2 = "SELECT * FROM tbl_organization_detail AS a INNER JOIN tbl_user_master AS b ON a.Organization_User=b.User_Code INNER JOIN tbl_org_type_master AS c ON a.Organization_Type=c.Org_Type_Code INNER JOIN tbl_state_master AS d ON a.Organization_State=d.State_Code  INNER JOIN tbl_district_master AS f ON a.Organization_District=f.District_Code INNER JOIN tbl_tehsil_master AS g ON a.Organization_Tehsil=g.Tehsil_Code INNER JOIN tbl_country_master AS h ON a.Organization_Country=h.Country_Code  INNER JOIN tbl_courseitgk_mapping AS i
				    ON b.user_loginid=i.Courseitgk_ITGK WHERE User_LoginId=" . $_activate . " AND 
					Courseitgk_Course='RS-CIT' AND b.User_UserRoll='7'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UPDATE($_code, $_OrgName, $_OrgType, $_OrgRegno, $_State, $_Region, $_District, $_Tehsil, $_Landmark, $_Road, $_Street, $_HouseNo, $_OrgEstDate, $_OrgCountry, $_address) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_UpdateQuery = "UPDATE tbl_organization_detail SET 
						Organization_Name='" . $_OrgName . "',
						Organization_RegistrationNo='" . $_OrgRegno . "',
						Organization_FoundedDate='" . $_OrgEstDate . "',
						Organization_Type='" . $_OrgType . "',
						Organization_State='" . $_State . "',
						Organization_Region='" . $_Region . "',
						Organization_District='" . $_District . "',
						Organization_Tehsil='" . $_Tehsil . "',
						
						Organization_Landmark='" . $_Landmark . "',
						Organization_Road='" . $_Road . "',
						Organization_Street='" . $_Street . "',
						Organization_HouseNo='" . $_HouseNo . "',
						Organization_Country='" . $_OrgCountry . "',
						Organization_Address='" . $_address . "',
						IsNewRecord='Y'
						WHERE Organization_Code='" . $_code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRecord($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_DeleteQuery = "DELETE FROM tbl_organization_detail WHERE Organization_Code='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_Org_Code) {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT *  FROM tbl_organization_detail WHERE Organization_Code='" . $_Org_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode_NEW() {
        //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT tom.*, tsm.State_Code AS TSC, trm.Region_Code AS TRC, tdm.District_Code AS TDC, ttm.Tehsil_Code AS TTC, tcm.Country_Code AS TCC, tsm.State_Name AS Organization_State, trm.Region_Name AS Organization_Region, tdm.District_Name AS Organization_District, ttm.Tehsil_Name AS Organization_Tehsil, tcm.Country_Name AS Organization_Country, tmt.Municipality_Type_Code AS TMTC, tmm.Municipality_Raj_Code AS TMRC, twm.Ward_Code TWRC, tps.Block_District AS TBDC, tgp.GP_Block AS TGB, tgp.GP_Name AS TGN, tvm.Village_Code AS TVC FROM tbl_organization_detail tom LEFT JOIN tbl_region_master trm ON tom.Organization_Region = trm.Region_Code LEFT JOIN tbl_state_master tsm ON tom.Organization_State = tsm.State_Code LEFT JOIN tbl_district_master tdm ON tom .Organization_District = tdm.District_Code LEFT JOIN tbl_tehsil_master ttm ON ttm.Tehsil_Code = tom.Organization_Tehsil  LEFT JOIN tbl_country_master tcm ON tcm.Country_Code = tom .Organization_Country LEFT JOIN tbl_municipality_type tmt ON tom.Organization_Municipal_Type = tmt.Municipality_Type_Code LEFT JOIN tbl_municipality_master tmm ON tom.Organization_Municipal = tmm.Municipality_Raj_Code LEFT JOIN tbl_ward_master twm ON tom.Organization_WardNo = twm.Ward_Code LEFT JOIN tbl_panchayat_samiti tps ON tom.Organization_Panchayat = tps.Block_District LEFT JOIN tbl_gram_panchayat tgp ON tom.Organization_Gram = tgp.GP_Block LEFT JOIN tbl_village_master tvm ON tom.Organization_Village = tvm.Village_Code WHERE Organization_User ='" . $_SESSION['User_Code'] . "' GROUP BY Organization_Code ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getAddressUpdateStatus($_Org_Code, $fld_requestChangeType) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
      //      $_SelectQuery = "SELECT fld_status, fld_ref_no FROM tbl_change_address_itgk WHERE fld_ITGK_UserCode = '" . $_Org_Code . "' AND fld_status IN ('0','1','5','3') AND fld_requestChangeType = '" . $fld_requestChangeType . "' ";
	  
	  $_SelectQuery = "SELECT fld_status, fld_ref_no FROM tbl_change_address_itgk WHERE fld_ITGK_UserCode = '" . $_Org_Code . "' AND fld_status IN ('0','1','5','3') AND fld_addedOn> '2019-09-19 00:00:00' AND fld_requestChangeType = '" . $fld_requestChangeType . "' ";
	  
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function pullUpdateRequest($_requestChangeType, $_fld_ref_no) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_change_address_itgk WHERE fld_ref_no = '" . $_fld_ref_no . "' AND fld_requestChangeType = '" . $_requestChangeType . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function checkMobile($_mobile, $_User_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT count(*)  AS total_count FROM tbl_user_master WHERE User_MobileNo = '" . $_mobile . "' AND User_Code = '" . $_User_Code . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function insertOTP($_OTP, $_Mobile, $fld_requestChangeType) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_InsertQuery = "INSERT INTO tbl_change_address_itgk (fld_requestChangeType, fld_ITGK_Code, fld_ITGK_UserCode, fld_RSP, fld_ref_no, fld_status, fld_updatedBy) VALUES ('" . $fld_requestChangeType . "', '" . $_SESSION['User_LoginId'] . "', '" . $_SESSION['User_Code'] . "', '" . $_SESSION['User_Rsp'] . "', '" . $_OTP . "', '0', '" . $_SESSION['UserRoll_Name'] . "' )";
        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        $_SMS = "Dear " . $_SESSION['UserRoll_Name'] . ", OTP for " . ucfirst($fld_requestChangeType) . " Update Request is : " . $_OTP;
        SendSMS($_Mobile, $_SMS);
        return $_Response;
    }

    public function verifyOTP($_OTP) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "SELECT COUNT(*) AS total_count FROM tbl_change_address_itgk WHERE fld_ref_no = '" . $_OTP . "' AND fld_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND fld_ITGK_UserCode = '" . $_SESSION['User_Code'] . "' AND fld_status = '0' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function revokeReference($_OTP) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '6', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "' WHERE fld_ref_no = '" . $_OTP . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function updateOrgAddressRef($postData, $fileArray) {

        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            if ($postData['Organization_AreaType'] == "Rural") {
                $postData['ddlWardno'] = '';
                $postData['ddlMunicipalName'] = '';
                $postData['ddlMunicipalType'] = '';
            }

            if ($postData['Organization_AreaType'] == "Urban") {
                $postData['ddlVillage'] = '';
                $postData['ddlGramPanchayat'] = '';
                $postData['ddlPanchayat'] = '';
            }

            if($postData["Organization_OwnershipType"] == "Rent" || $_POST["Organization_OwnershipType"] == "Relative" || $_POST["Organization_OwnershipType"] == "Special"){
                $postData["fld_ownershipDocument"] = "";
                $postData["fld_rentAgreement"] = $fileArray["fld_rentAgreement"];
                $postData["fld_utilityBill"] = $fileArray["fld_utilityBill"];
            } else {
                $postData["fld_ownershipDocument"] = $fileArray["fld_ownershipDocument"];
                $postData["fld_rentAgreement"] = "";
                $postData["fld_utilityBill"] = "";
            }
            
             $_SelectQuery1 = "Select * from tbl_address_name_transaction Where fld_ref_no = '" . $postData['reference_number'] . "' and fld_Transaction_Status='Success'";
            
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            //$_Row = mysqli_fetch_array($_Response1[2]);
            if( mysqli_num_rows($_Response1[2])){                
                $Status = '5';
            } else {
                $Status = '3';
            }

            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET 
                                 Organization_Name_old = '" . $_SESSION['Organization_Name'] . "', 
                                 Organization_Name = '" . $_SESSION['Organization_Name'] . "',  
                                 fld_document = '" . $fileArray['fld_document'] . "', 
                                 fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "', 
                                 fld_status = '" . $Status . "',                                  
                                 Organization_Type='" . $postData['txtType'] . "',      
                                     AddressChange_Reason='" . $postData['ddlReason'] . "', 
                                     Organization_Landmark='" . $postData['landmark'] . "',    
                                 Organization_Address='" . $postData['txtAddress'] . "',
                                    Organization_Tehsil='" . $postData['ddlTehsilNew'] . "',
                                    Organization_Tehsil_old='" . $postData['ddlTehsil'] . "',
                                 Organization_AreaType='" . $postData['Organization_AreaType'] . "',
                                 Organization_OwnershipType = '".$postData["Organization_OwnershipType"]."',
                                 Organization_OwnershipType_old = '".$postData["Organization_OwnershipType_old"]."',
                                 fld_ownershipDocument = '".$postData["fld_ownershipDocument"]."',
                                 fld_rentAgreement = '".$postData["fld_rentAgreement"]."',
                                 fld_utilityBill = '".$postData["fld_utilityBill"]."',
                                 Organization_OwnershipType_old = '".$postData["Organization_OwnershipType_old"]."',
                                     Organization_Owner_Name = '".$postData["Owner_Name"]."',
                                 Organization_WardNo='" . $postData['ddlWardno'] . "',
                                 Organization_Municipal='" . $postData['ddlMunicipalName'] . "',
                                 Organization_Municipal_Type='" . $postData['ddlMunicipalType'] . "',
                                 Organization_Village='" . $postData['ddlVillage'] . "',
                                 Organization_Gram='" . $postData['ddlGramPanchayat'] . "',
                                 Organization_Panchayat='" . $postData['ddlPanchayat'] . "',                                  
                                 Organization_Type_old='" . $postData['Organization_Type_old'] . "',                                 
                                 Organization_Address_old='" . $postData['Organization_Address_old'] . "',
                                 Organization_AreaType_old='" . $postData['Organization_AreaType_old'] . "',
                                 Organization_WardNo_old='" . $postData['Organization_WardNo_old'] . "',
                                 Organization_Municipal_old='" . $postData['Organization_Municipal_old'] . "',
                                 Organization_Municipal_Type_old='" . $postData['Organization_Municipality_Type_old'] . "',
                                 Organization_Village_old='" . $postData['Organization_Village_old'] . "',
                                 Organization_Gram_old='" . $postData['Organization_Gram_old'] . "',
                                 Organization_Panchayat_old='" . $postData['Organization_Panchayat_old'] . "'                                   
                                 WHERE fld_ref_no = '" . $postData['reference_number'] . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch
        (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function updateOrgNameRef($postData, $fld_document) {

        $postData['reference_number'] = (isset($postData['reference_number']) && ($postData['reference_number'] <> "")) ? $postData['reference_number'] : $postData['reference_number_name'];

        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_document = '" . $fld_document . "', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "', fld_status = '1', Organization_Name='" . $postData['Organization_Name'] . "',Organization_Name_old='" . $postData['Organization_Name_old'] . "' WHERE fld_ref_no = '" . $postData['reference_number'] . "'  ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch
        (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getSPDetails($_sp) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "SELECT User_EmailId AS email, User_MobileNo AS mobile, User_LoginId AS username FROM tbl_user_master WHERE User_Code = '" . $_sp . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getRKCLDetails($_rkcl) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "SELECT User_EmailId AS email, User_MobileNo AS mobile, User_LoginId AS username FROM tbl_user_master WHERE User_Code = '" . $_rkcl . "' AND User_UserRoll = 11 ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function sendEmailtoITGK($email, $mobile, $username, $reference_number, $requestType) {
        $variableArray = [];
        $variableArray["centerID"] = $_SESSION["User_LoginId"];
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear ITGK Center (" . $variableArray["centerID"] . "), Your have successfully created an " . ucfirst($requestType) . " Update Request, the reference number is " . $reference_number . ", Kindly login at MyRKCL Portal for more details.";

        $variableArray["username"] = "ITGK Center - " . $username;
        $variableArray["htmlMessage"] = "You have successfully created " . ucfirst($requestType) . " Update Request (Ref. No. <span style='color: orangered;'>" . $reference_number . "</span>). Kindly login at MyRKCL Portal for more details.";

        SendSMS($mobile, $_SMS);

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Generated : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoSP($email, $mobile, $username, $reference_number, $requestType) {
        $variableArray = [];
        $variableArray["centerID"] = $_SESSION["User_LoginId"];
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["SP_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear Service Provider (" . $username . "), ITGK Center - " . $_SESSION['User_LoginId'] . " has raised " . $requestType . " Update Request: " . $reference_number . ", Kindly Login at MyRKCL Portal for more details.";
        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "Service Provider - " . $username;
        $variableArray["htmlMessage"] = "An ITGK Center (Center ID - <span style='color: #00A65A;'>" . $variableArray["centerID"] . "</span>) has raised an " . ucfirst($requestType) . " Update Request via MyRKCL Portal (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>). Kindly login at MyRKCL for more details.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Generated from ITGK : " . $variableArray["centerID"], "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoSPAccept($spusername, $email, $mobile, $username, $reference_number, $requestType) {

        $variableArray = [];
        $variableArray["centerID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear Service Provider, you have successfully approved the request " . $reference_number . " raised by ITGK Center (" . $username . ") for " . ucfirst($requestType) . " Update Request. Kindly login at MyRKCL Portal for more details.";

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "Service Provider - " . $spusername;
        $variableArray["htmlMessage"] = "You have successfully approved the " . ucfirst($requestType) . " Update Request raised by ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span> (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>). The request has been forwarded to RKCL for further verification, once done you will be notified.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Approved by You : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoSPAccept2($spusername, $email, $mobile, $username, $reference_number, $requestType) {
        $variableArray = [];
        $variableArray["centerID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear Service Provider, RKCL has successfully approved the request " . $reference_number . " raised by ITGK Center (" . $username . ") for " . ucfirst($requestType) . " Update Request. Kindly login at MyRKCL portal for more details.";

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "Service Provider - " . $spusername;
        $variableArray["htmlMessage"] = "RKCL has approved accepted the " . $variableArray["requestType"] . " Update Request raised by ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span> (Ref. No. <span style=\"color: orangered;\">" . $variableArray["ref_no"] . "</span>). Kindly login at MyRKCL Portal for more details.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Approved by RKCL : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoSPAccept3($spusername, $email, $mobile, $username, $reference_number, $requestType) {
        $variableArray = [];
        $variableArray["centerID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear Service Provider, ITGK Center (" . $username . ") has successfully paid the fee for " . ucfirst($requestType) . " Update Request. The reference number is " . $reference_number . ", Kindly login at MyRKCL portal for more details.";

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "Service Provider - " . $spusername;
        $variableArray["htmlMessage"] = "ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span> has successfully paid for " . $variableArray["requestType"] . " Update Request (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>). Kindly login at MyRKCL Portal for more details.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Paid by ITGK : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoSPReject($spusername, $email, $mobile, $username, $reference_number, $requestType) {
        $variableArray = [];
        $variableArray["centerID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear Service Provider, RKCL has rejected " . ucfirst($requestType) . " Update Request. The reference number is " . $reference_number . " raised by ITGK Center (" . $username . ") , Kindly login at MyRKCL portal for more detailed remarks.";

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "Service Provider - " . $spusername;
        $variableArray["htmlMessage"] = "RKCL has rejected the " . ucfirst($requestType) . " Update Request (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>) raised by ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span>. Kindly login at MyRKCL Portal for more detailed remarks.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Rejected by RKCL : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoRKCLAccept($spusername, $username, $reference_number, $requestType, $rkclData) {

        $variableArray = [];
        $variableArray["SPID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear RKCL, $spusername has approved request " . $reference_number . " raised by ITGK Center (" . $username . ") for " . ucfirst($requestType) . " Update Request. Kindly login at MyRKCL Portal for more details.";

        $mobile = $rkclData['mobile'];
        $email = $rkclData['email'];

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "RKCL Team Member";
        $variableArray["htmlMessage"] = "<span style=\"color: #00A65A;\">{SPID}</span> has accepted the " . ucfirst($requestType) . " Update Request (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>) raised by ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span>. Kindly login at MyRKCL Portal for more details.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Approved by Service Provider : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoRKCLAccept2($spusername, $username, $reference_number, $requestType, $rkclData) {

        $variableArray = [];
        $variableArray["SPID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear RKCL, You have successfully approved the request " . $reference_number . " raised by ITGK Center (" . $username . ") for " . ucfirst($requestType) . " Update Request. Kindly login at MyRKCL Portal for more details.";

        $mobile = $rkclData['mobile'];
        $email = $rkclData['email'];

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "RKCL Team Member";
        $variableArray["htmlMessage"] = "You have successfully approved the " . ucfirst($requestType) . " Update Request (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>) raised by ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span>. Kindly login at MyRKCL Portal for more details.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Approved by You : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoRKCLAccept3($spusername, $username, $reference_number, $requestType, $rkclData) {

        $variableArray = [];
        $variableArray["SPID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear RKCL, ITGK Center (" . $username . ") has been successfully paid the fee for " . ucfirst($requestType) . " Update Request. The request number is " . $reference_number . ", Kindly login at MyRKCL Portal for more details.";

        $mobile = $rkclData['mobile'];
        $email = $rkclData['email'];

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "RKCL Team Member";
        $variableArray["htmlMessage"] = "ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span> has successfully paid for " . ucfirst($requestType) . " Update Request (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>). Kindly login at MyRKCL Portal for more details.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request paid by ITGK : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function sendEmailtoRKCLReject($spusername, $username, $reference_number, $requestType, $rkclData) {

        $variableArray = [];
        $variableArray["SPID"] = $spusername;
        $variableArray["ref_no"] = $reference_number;
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $username;
        $variableArray["requestType"] = ucfirst($requestType);
        $sendEmail = new sendMail();

        $_SMS = "Dear RKCL, you have rejected the  " . ucfirst($requestType) . " Update Request raised by ITGK Center (" . $username . "). The request number is " . $reference_number . ", Kindly login at MyRKCL Portal for more details.";

        $mobile = $rkclData['mobile'];
        $email = $rkclData['email'];

        SendSMS($mobile, $_SMS);

        $variableArray["username"] = "RKCL Team Member";
        $variableArray["htmlMessage"] = "You have rejected the " . ucfirst($requestType) . " Update Request raised by ITGK Center - <span style=\"color: #00A65A;\">" . $variableArray["ITGK_Name"] . "</span>  (Ref. No. <span style=\"color: orangered;\">" . $reference_number . "</span>). Kindly login at MyRKCL Portal for more details.";

        $_Response = $sendEmail->sendEmailHTML($email, $username, ucfirst($requestType) . " Update Request Rejected by You : Ref. Id : " . $reference_number, "send_email_template.php", $variableArray);
        return $_Response;
    }

    public function showUpdateRequest($flag) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $params = $columns = $totalRecords = $data = array();

        $params = $_REQUEST;

        $where = $sqlTot = $sqlRec = "";

        // check search value exist
        if (!empty($params['search']['value'])) {
            $where .= " AND ( fld_ITGK_Code LIKE '" . $params['search']['value'] . "%' ";
            $where .= " OR fld_ref_no LIKE '" . $params['search']['value'] . "%' )";
        } else {
            $where = "";
        }

        try {

            $user_roll_condition = ($_SESSION['User_UserRoll'] == "14") ? "fld_RSP = '" . $_SESSION['User_Code'] . "' AND " : "";

            $_SelectQuery = "SELECT tcai.fld_ID AS fld_ID, tcai.fld_ITGK_Code AS fld_ITGK_Code, tcai.fld_updatedBy AS fld_updatedBy, tod.Organization_Name AS Organization_Name FROM tbl_change_address_itgk tcai LEFT JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode WHERE " . $user_roll_condition . " fld_requestChangeType = '" . $flag . "' " . $where . " AND fld_status !='0' GROUP BY fld_ITGK_Code ORDER BY fld_addedOn DESC";

            $sqlRec .= $_SelectQuery;
            $sqlRec .= "  LIMIT " . $params['start'] . " , 10 ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $totalRecords = mysqli_num_rows($_Response[2]);
            $queryRecords = $_ObjConnection->ExecuteQuery($sqlRec, Message::SelectStatement);

            //echo $_SelectQuery;

            $count = $params['start'] + 1;
            while ($row = mysqli_fetch_row($queryRecords[2])) {
                $row[0] = $count;
                $data[] = $row;
                $count++;
            }

            $json_data = array(
                "draw" => intval($params['draw']),
                "recordsTotal" => intval($totalRecords),
                "recordsFiltered" => intval($totalRecords),
                "data" => $data   // total data array
            );
            $_Response = $json_data;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function showNameUpdateRequestITGK($fld_RSP, $flag) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $params = $columns = $totalRecords = $data = array();

        $params = $_REQUEST;

        $where = $sqlTot = $sqlRec = "";

        // check search value exist
        if (!empty($params['search']['value'])) {
            $where .= " AND ( fld_ITGK_Code LIKE '" . $params['search']['value'] . "%' ";
            $where .= " OR fld_ref_no LIKE '" . $params['search']['value'] . "%' )";
        } else {
            $where = "";
        }

        try {

            /*** FOR RSP ***/
            if($_SESSION["User_UserRoll"] == "14"){
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i %p') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i %p') AS 'fld_updatedOn',tcai.fld_updatedBy,
                    if(tcai.fld_addedOn< '2019-09-19 00:00:00','Old Process','New Process') as Process_Type,CASE
 tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
 WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE tcai.fld_RSP = '".$fld_RSP."' AND fld_requestChangeType = '" . $flag . "' AND fld_status >= '1' " . $where . " ORDER BY fld_status ASC, fld_updatedOn DESC";
            } else if($_SESSION["User_UserRoll"] == "4" || $_SESSION["User_UserRoll"] == "11"){
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i') AS 'fld_updatedOn',tcai.fld_updatedBy,
                    if(tcai.fld_addedOn< '2019-09-19 00:00:00','Old Process','New Process') as Process_Type,CASE
 tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
 WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE fld_requestChangeType = '" . $flag . "' AND fld_status >= '2' " . $where . " ORDER BY tcai.fld_updatedOn DESC";
            } else {
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i %p') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i %p') AS 'fld_updatedOn',tcai.fld_updatedBy,
                    if(tcai.fld_addedOn< '2019-09-19 00:00:00','Old Process','New Process') as Process_Type,CASE
 tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
 WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai
 .fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE tcai.fld_ITGK_UserCode = '".$_SESSION["User_Code"]."' AND fld_requestChangeType = '" . $flag . "' AND fld_status <> '0' " . $where . " ORDER BY fld_status ASC, fld_updatedOn DESC";
            }



            $sqlRec .= $_SelectQuery;
            $sqlRec .= "  LIMIT " . $params['start'] . " , 10 ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $totalRecords = mysqli_num_rows($_Response[2]);
            $queryRecords = $_ObjConnection->ExecuteQuery($sqlRec, Message::SelectStatement);

            $count = $params['start'] + 1;
            while ($row = mysqli_fetch_row($queryRecords[2])) {
                $row[0] = $count;
                $data[] = $row;
                $count++;
            }

            $json_data = array(
                "draw" => intval($params['draw']),
                "recordsTotal" => intval($totalRecords),
                "recordsFiltered" => intval($totalRecords),
                "data" => $data   // total data array
            );
            $_Response = $json_data;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
    public function showUpdateRequestITGK($fld_RSP, $flag) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '11' || $_LoginRole == '30') {
                 
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i') AS 'fld_updatedOn',tcai.fld_updatedBy,
                                if(tcai.fld_addedOn< '2019-09-19 00:00:00','Old Process','New Process') as Process_Type,CASE
                                tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
                                WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE fld_requestChangeType = '" . $flag . "' AND fld_status >= '2'  ORDER BY tcai.fld_updatedOn DESC";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '14') {
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i %p') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i %p') AS 'fld_updatedOn',tcai.fld_updatedBy,
                                if(tcai.fld_addedOn< '2019-09-19 00:00:00','Old Process','New Process') as Process_Type,CASE
                                tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
                                WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE tcai.fld_RSP = '".$fld_RSP."' AND fld_requestChangeType = '" . $flag . "' AND fld_status >= '1' ORDER BY fld_status ASC, fld_updatedOn DESC";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
    public function SendSMS($_SMS, $_CenterCode, $RefId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d H:i:s");
            $_UpdateQuery = "Update tbl_change_address_itgk set fld_onhold_message='" . $_SMS . "', fld_status='0', "
                    . "fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "', fld_updatedOn = '" . $_Date . "'"
                    . " Where fld_ref_no='" . $RefId . "'";
            
            $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);   
                
            $_SelectQuery = "Select * from tbl_user_master Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response[2]);
            $_Mobile = $_Row['User_MobileNo'];
            $_Rsp_Code = $_Row['User_Rsp'];
            
            $_SelectQuery1 = "Select * from tbl_user_master Where User_Code='" . $_Rsp_Code . "'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_RSPMobile = $_Row1['User_MobileNo'];
            SendSMS($_Mobile,$_SMS);
            SendSMS($_RSPMobile,$_SMS);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
        public function Download_Excel($fld_RSP, $flag) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $params = $columns = $totalRecords = $data = array();

        $params = $_REQUEST;

        $where = $sqlTot = $sqlRec = "";

        // check search value exist
        if (!empty($params['search']['value'])) {
            $where .= " AND ( fld_ITGK_Code LIKE '" . $params['search']['value'] . "%' ";
            $where .= " OR fld_ref_no LIKE '" . $params['search']['value'] . "%' )";
        } else {
            $where = "";
        }

        try {

            /*** FOR RSP ***/
            if($_SESSION["User_UserRoll"] == "14"){
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i %p') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i %p') AS 'fld_updatedOn',tcai.fld_updatedBy,CASE
 tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
 WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE tcai.fld_RSP = '".$fld_RSP."' AND fld_requestChangeType = '" . $flag . "' AND fld_status >= '1' " . $where . " ORDER BY fld_status ASC, fld_updatedOn DESC";
            } else if($_SESSION["User_UserRoll"] == "4" || $_SESSION["User_UserRoll"] == "11"){
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i') AS 'fld_updatedOn',tcai.fld_updatedBy,CASE
 tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
 WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE fld_requestChangeType = '" . $flag . "' AND fld_status >= '2' " . $where . " ORDER BY fld_status ASC, fld_updatedOn DESC";
            } else {
                $_SelectQuery = "SELECT tcai.fld_ID, tcai.fld_ref_no, tcai.fld_ITGK_Code as fld_ITGK_Code,tod.Organization_Name, District_Name, date_format(tcai.fld_addedOn, '%d-%m-%Y %h:%i %p') AS 'fld_addedOn', date_format(tcai.fld_updatedOn, '%d-%m-%Y %h:%i %p') AS 'fld_updatedOn',tcai.fld_updatedBy,CASE
 tcai.fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted by ITGK' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Payment Pending' WHEN '4' THEN 'Rejected by RKCL' 
 WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' WHEN '7' THEN 'Rejected By SP' WHEN '8' THEN 'Old Process' WHEN '9' THEN 'RM Approved' END AS 'fld_status' FROM tbl_change_address_itgk tcai JOIN tbl_organization_detail tod ON tod.Organization_User = tcai
 .fld_ITGK_UserCode inner join tbl_district_master as tdm on tod.Organization_District=tdm.District_Code WHERE tcai.fld_ITGK_UserCode = '".$_SESSION["User_Code"]."' AND fld_requestChangeType = '" . $flag . "' AND fld_status <> '0' " . $where . " ORDER BY fld_status ASC, fld_updatedOn DESC";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillUpdateAddressDetails($_ref_no) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "SELECT tcai.Organization_Name_old AS Organization_Name_old, 
                                 tcai.Organization_Name AS Organization_Name_NEW,
                                  tcai.fld_remarks AS fld_remarks, 
                                  tcai.fld_remarks_rkcl AS fld_remarks_rkcl,
                                  tcai.Organization_OwnershipType as Organization_OwnershipType,
                                  tcai.Organization_OwnershipType_old as Organization_OwnershipType_old,
                                  tcai.fld_ownershipDocument as fld_ownershipDocument,
                                  tcai.fld_rentAgreement as fld_rentAgreement,
                                  tcai.fld_utilityBill as fld_utilityBill,
                                  tcai.fld_document AS fld_document, 
tcai.fld_requestChangeType AS fld_requestChangeType,                                  

tcai.fld_VisitDate AS fld_VisitDate, 
tcai.fld_FrontPhoto AS fld_FrontPhoto, 
tcai.fld_LeftPhoto AS fld_LeftPhoto, 
tcai.fld_RightPhoto AS fld_RightPhoto, 
tcai.fld_LabSpace AS fld_LabSpace, 
tcai.fld_MarkByRKCL AS fld_MarkByRKCL, 
                                  tcai.fld_ITGK_UserCode AS Organization_User_Code, 
                                  tcai.fld_status AS fld_status,                                   
                                  tcai.Organization_Address AS Organization_Address, 
                                  tcai.fld_ITGK_Code AS fld_ITGK_Code, 
                                  tcai.fld_addedOn AS submission_date,
                                  tcai.fld_updatedOn AS fld_updatedOn,
                                  tcai.fld_spActionDate AS submission_date_sp,
                                  tcai.fld_rkclActionDate AS submission_date_rkcl,
                                  tod.Organization_Name AS Organization_Name, 
                                  tod.Organization_RegistrationNo AS Organization_RegistrationNo, 
                                  tod.Organization_FoundedDate AS Organization_FoundedDate, 
                                  totm.Org_Type_Name AS Organization_Type, 
                                  tcai.Organization_Owner_Name AS Organization_Owner_Name,
                                  tcai.Organization_AreaType AS Organization_AreaType, 
                                  tmt.Municipality_Type_Name AS Organization_Municipal_Type, 
                                  tmm.Municipality_Name AS Organization_Municipal,
                                  twm.Ward_Name AS Organization_WardNo, 
                                  tps.Block_Name AS Organization_Panchayat, 
                                  tgp.GP_Name AS Organization_Gram, 
                                  tvm.Village_Name AS Organization_Village,                                   
                                  tcai.Organization_Address_old AS Organization_Address_old, 
                                  totm_old.Org_Type_Name AS Organization_Type_old,                                  
                                  tcai.Organization_AreaType_old AS Organization_AreaType_old, 
                                  tmt_old.Municipality_Type_Name AS Organization_Municipal_Type_old, 
                                  tmm_old.Municipality_Name AS Organization_Municipal_old,
                                  twm_old.Ward_Name AS Organization_WardNo_old, 
                                  tps_old.Block_Name AS Organization_Panchayat_old, 
                                  tgp_old.GP_Name AS Organization_Gram_old, 
                                  tvm_old.Village_Name AS Organization_Village_old                                   
                                  FROM tbl_change_address_itgk tcai                                   
                                  LEFT JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode 
                                  LEFT JOIN tbl_org_type_master totm ON tcai.Organization_Type = totm.Org_Type_Code 
                                  LEFT JOIN tbl_org_type_master totm_old ON tcai.Organization_Type_old = totm_old.Org_Type_Code                                   
                                  LEFT JOIN tbl_municipality_type tmt ON tcai.Organization_Municipal_Type = tmt.Municipality_Type_Code 
                                  LEFT JOIN tbl_municipality_type tmt_old ON tcai.Organization_Municipal_Type_old = tmt_old.Municipality_Type_Code 
                                  LEFT JOIN tbl_municipality_master tmm ON tcai.Organization_Municipal = tmm.Municipality_Raj_Code 
                                  LEFT JOIN tbl_municipality_master tmm_old ON tcai.Organization_Municipal_old = tmm_old.Municipality_Raj_Code 
                                  LEFT JOIN tbl_ward_master twm ON tcai.Organization_WardNo = twm.Ward_Code 
                                  LEFT JOIN tbl_ward_master twm_old ON tcai.Organization_WardNo_old = twm_old.Ward_Code 
                                  LEFT JOIN tbl_panchayat_samiti tps ON tcai.Organization_Panchayat = tps.Block_Code 
                                  LEFT JOIN tbl_panchayat_samiti tps_old ON tcai.Organization_Panchayat_old = tps_old.Block_Code 
                                  LEFT JOIN tbl_gram_panchayat tgp ON tcai.Organization_Gram = tgp.GP_Code 
                                  LEFT JOIN tbl_gram_panchayat tgp_old ON tcai.Organization_Gram_old = tgp_old.GP_Code 
                                  LEFT JOIN tbl_village_master tvm ON tcai.Organization_Village = tvm.Village_Code 
                                  LEFT JOIN tbl_village_master tvm_old ON tcai.Organization_Village_old = tvm_old.Village_Code                                   
                                  WHERE fld_ref_no = '" . $_ref_no . "' GROUP BY tcai.fld_ITGK_Code";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillLocationDetails($_Code, $_Ref_No) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "SELECT tcm.Country_Name AS Organization_Country,
                                 tsm.State_Name AS Organization_State,
                                 trm.Region_Name AS Organization_Region,
                                 tdm.District_Name AS Organization_District,
                                 ttm.Tehsil_Name AS Organization_Tehsil_Old,
                                 ttmo.Tehsil_Name AS Organization_Tehsil
                                 FROM tbl_organization_detail tod 
                                 LEFT JOIN tbl_country_master tcm ON tcm.Country_Code = tod.Organization_Country 
                                 LEFT JOIN tbl_state_master tsm ON tsm.State_Code = tod.Organization_State 
                                 LEFT JOIN tbl_region_master trm ON trm.Region_Code = tod.Organization_Region 
                                 LEFT JOIN tbl_district_master tdm ON tdm.District_Code = tod.Organization_District
                                 LEFT JOIN tbl_tehsil_master ttm ON ttm.Tehsil_Code = tod.Organization_Tehsil 
                                 LEFT JOIN tbl_change_address_itgk tca ON tod.Organization_User = tca.fld_ITGK_UserCode
                                 LEFT JOIN tbl_tehsil_master ttmo ON tca.Organization_Tehsil = ttmo.Tehsil_Code
                                 WHERE tod.Organization_User = '" . $_Code . "' and fld_requestChangeType='address'  and tca.fld_ref_no='" . $_Ref_No . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function commitChangesAccept($_Code, $fld_remarks, $VisitDate, $owntypefilename, $salpifilename, $panpifilename, $ccpifilename) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            
//            $query = "SELECT od.Organization_Code, od.Organization_User, ca.* FROM tbl_organization_detail od "
//                    . "INNER JOIN tbl_user_master um ON um.User_Code = od.Organization_User "
//                    . "INNER JOIN  tbl_change_address_itgk ca ON ca.fld_ITGK_Code = um.User_LoginId "
//                    . "WHERE ca.fld_ref_no = '" . $_Code . "'";
//		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
//		if (mysqli_num_rows($_Responses[2])) {
//			$nacRow = mysqli_fetch_array($_Responses[2]);
//			if ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Urban') {
//                            $_InsertQuery = "Insert into tbl_organization_detail_log select A.*,'Address_Change' from tbl_organization_detail as a "
//                    . "Where Organization_Code='" . $nacRow['Organization_Code'] . "'";
//            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//            
//				$updateQuery = "UPDATE tbl_organization_detail SET
//			Organization_Type = '" . $nacRow['Organization_Type'] . "',
//			Organization_Address = '" . $nacRow['Organization_Address'] . "',
//                            Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
//			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
//			Organization_Municipal_Type = '" . $nacRow['Organization_Municipal_Type'] . "',
//			Organization_Municipal = '" . $nacRow['Organization_Municipal'] . "', 
//			Organization_WardNo = '" . $nacRow['Organization_WardNo'] . "' 
//			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
//			} elseif ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Rural') {
//                            $_InsertQuery = "Insert into tbl_organization_detail_log select A.*,'Address_Change' from tbl_organization_detail as a "
//                    . "Where Organization_Code='" . $nacRow['Organization_Code'] . "'";
//            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//            
//				$updateQuery = "UPDATE tbl_organization_detail SET
//			Organization_Type = '" . $nacRow['Organization_Type'] . "',
//			Organization_Address = '" . $nacRow['Organization_Address'] . "',
//                             Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
//			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
//			Organization_Village = '" . $nacRow['Organization_Village'] . "',
//			Organization_Gram = '" . $nacRow['Organization_Gram'] . "', 
//			Organization_Panchayat = '" . $nacRow['Organization_Panchayat'] . "' 
//			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
//			}
//			if ($updateQuery) {
//				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
//			}
//		}
                
            //$_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '2', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "', fld_remarks = '" . mysqli_real_escape_string($fld_remarks) . "', fld_spActionDate = CURRENT_TIMESTAMP() WHERE fld_ref_no = '" . $_Code . "'  ";
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '2', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "', fld_remarks = '" . mysqli_real_escape_string($_ObjConnection->Connect(),$fld_remarks) . "', fld_spActionDate = CURRENT_TIMESTAMP(), fld_VisitDate = '" . $VisitDate . "', fld_FrontPhoto = '" . $owntypefilename . "', fld_LeftPhoto = '" . $salpifilename . "', fld_RightPhoto = '" . $panpifilename . "', fld_LabSpace = '" . $ccpifilename . "' WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

     public function commitChangesAcceptName($_Code, $fld_remarks) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '2', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "', fld_remarks = '" . mysqli_real_escape_string($fld_remarks) . "', fld_spActionDate = CURRENT_TIMESTAMP() WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
      public function MarkIncorrectApplication($_Code, $fld_remarks) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_MarkByRKCL = 'RKCL Marked Incorrect Approval', fld_rkclActionDate = CURRENT_TIMESTAMP() WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function commitChangesAccept_rkcl($_Code, $fld_remarks) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
//            $query = "SELECT od.Organization_Code, od.Organization_User, ca.* FROM tbl_organization_detail od "
//                    . "INNER JOIN tbl_user_master um ON um.User_Code = od.Organization_User "
//                    . "INNER JOIN  tbl_change_address_itgk ca ON ca.fld_ITGK_Code = um.User_LoginId "
//                    . "WHERE ca.fld_ref_no = '" . $_Code . "'";
//		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
//		if (mysqli_num_rows($_Responses[2])) {
//			$nacRow = mysqli_fetch_array($_Responses[2]);
//			if ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Urban') {
//                            $_InsertQuery = "Insert into tbl_organization_detail_log select A.*,'Address_Change' from tbl_organization_detail as a "
//                    . "Where Organization_Code='" . $nacRow['Organization_Code'] . "'";
//            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//            
//				$updateQuery = "UPDATE tbl_organization_detail SET
//			Organization_Type = '" . $nacRow['Organization_Type'] . "',
//			Organization_Address = '" . $nacRow['Organization_Address'] . "',
//                            Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
//			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
//			Organization_Municipal_Type = '" . $nacRow['Organization_Municipal_Type'] . "',
//			Organization_Municipal = '" . $nacRow['Organization_Municipal'] . "', 
//			Organization_WardNo = '" . $nacRow['Organization_WardNo'] . "' 
//			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
//			} elseif ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Rural') {
//                            $_InsertQuery = "Insert into tbl_organization_detail_log select A.*,'Address_Change' from tbl_organization_detail as a "
//                    . "Where Organization_Code='" . $nacRow['Organization_Code'] . "'";
//            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//            
//				$updateQuery = "UPDATE tbl_organization_detail SET
//			Organization_Type = '" . $nacRow['Organization_Type'] . "',
//			Organization_Address = '" . $nacRow['Organization_Address'] . "',
//                             Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
//			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
//			Organization_Village = '" . $nacRow['Organization_Village'] . "',
//			Organization_Gram = '" . $nacRow['Organization_Gram'] . "', 
//			Organization_Panchayat = '" . $nacRow['Organization_Panchayat'] . "' 
//			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
//			}
//			if ($updateQuery) {
//				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
//			}
//		}
                
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '3', fld_updatedBy = '" . $_SESSION['User_LoginId'] . "', fld_remarks_rkcl = '" . mysqli_real_escape_string($_ObjConnection->Connect(),$fld_remarks) . "', fld_rkclActionDate = CURRENT_TIMESTAMP() WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
        public function commitChangesAccept_rkcl_address($_Code, $fld_remarks) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $query = "SELECT od.Organization_Code, od.Organization_User, ca.* FROM tbl_organization_detail od "
                    . "INNER JOIN tbl_user_master um ON um.User_Code = od.Organization_User "
                    . "INNER JOIN  tbl_change_address_itgk ca ON ca.fld_ITGK_Code = um.User_LoginId "
                    . "WHERE ca.fld_ref_no = '" . $_Code . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$nacRow = mysqli_fetch_array($_Responses[2]);
			if ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Urban') {
                            $_InsertQuery = "Insert into tbl_organization_detail_log select A.*,'Address_Change' from tbl_organization_detail as a "
                    . "Where Organization_Code='" . $nacRow['Organization_Code'] . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
				$updateQuery = "UPDATE tbl_organization_detail SET
			Organization_Type = '" . $nacRow['Organization_Type'] . "',
			Organization_Address = '" . $nacRow['Organization_Address'] . "',
                            Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
			Organization_Municipal_Type = '" . $nacRow['Organization_Municipal_Type'] . "',
			Organization_Municipal = '" . $nacRow['Organization_Municipal'] . "', 
			Organization_WardNo = '" . $nacRow['Organization_WardNo'] . "' 
			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			} elseif ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Rural') {
                            $_InsertQuery = "Insert into tbl_organization_detail_log select A.*,'Address_Change' from tbl_organization_detail as a "
                    . "Where Organization_Code='" . $nacRow['Organization_Code'] . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
				$updateQuery = "UPDATE tbl_organization_detail SET
			Organization_Type = '" . $nacRow['Organization_Type'] . "',
			Organization_Address = '" . $nacRow['Organization_Address'] . "',
                             Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
			Organization_Village = '" . $nacRow['Organization_Village'] . "',
			Organization_Gram = '" . $nacRow['Organization_Gram'] . "', 
			Organization_Panchayat = '" . $nacRow['Organization_Panchayat'] . "' 
			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			}
			if ($updateQuery) {
				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
			}
		}
                
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '9', fld_updatedBy = '" . $_SESSION['User_LoginId'] . "', fld_remarks_rkcl = '" . mysqli_real_escape_string($_ObjConnection->Connect(),$fld_remarks) . "', fld_rkclActionDate = CURRENT_TIMESTAMP() WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function commitChangesPaid($_Code) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '5', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "' WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function commitChangesReject($_Code, $fld_remarks) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '4', fld_updatedBy = '" . $_SESSION['User_LoginId'] . "', fld_remarks_rkcl = '" . mysqli_real_escape_string($_ObjConnection->Connect(),$fld_remarks) . "' WHERE fld_ref_no = '" . mysqli_real_escape_string($_ObjConnection->Connect(),$_Code) . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
            } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
      public function commitChangesRejectBySP($_Code, $_Reason) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '7', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "', fld_selected_reason = '" . $_Reason . "' WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function commitChangesRevoke($_Code) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_DeleteQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '6', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "' WHERE fld_ref_no = '" . $_Code . "'  ";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAddressData($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_change_address_itgk WHERE fld_ref_no = '" . $_Code . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function commitChangeAddress($postData) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

        /*    $fld_rsp = mysqli_real_escape_string($postData['fld_RSP']);
            $fld_ref_no = mysqli_real_escape_string($postData['fld_ref_no']);
            $fld_ITGK_UserCode = mysqli_real_escape_string($postData['fld_ITGK_UserCode']);
            $fld_ITGK_Code = mysqli_real_escape_string($postData['fld_ITGK_Code']); */

            $fld_rsp = trim($postData['fld_RSP']);
            $fld_ref_no = trim($postData['fld_ref_no']);
            $fld_ITGK_UserCode = trim($postData['fld_ITGK_UserCode']);
            $fld_ITGK_Code = trim($postData['fld_ITGK_Code']);

            $_InsertQuery = "INSERT INTO tbl_address_name_transaction (`fld_ref_no`,`fld_rsp`, `fld_ITGK_UserCode`,`fld_ITGK_Code`,`fld_paymentStatus`) VALUES ('" . $fld_ref_no . "', '" . $fld_rsp . "', '" . $fld_ITGK_UserCode . "','" . $fld_ITGK_Code . "', '0') ";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function commitChangeAddressFinal($postData, $flag) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            if ($flag == "name") {

                $_SelectQuery = "UPDATE tbl_organization_detail SET Organization_Name='" . $postData['Organization_Name'] . "'  WHERE Organization_User = '" . $postData['fld_ITGK_UserCode'] . "'  ";
            } elseif ($flag == "address") {
                if ($postData['Organization_AreaType'] == "Rural") {
                    $postData['Organization_WardNo'] = '';
                    $postData['Organization_Municipal'] = '';
                    $postData['Organization_Municipal_Type'] = '';
                }

                if ($postData['Organization_AreaType'] == "Urban") {
                    $postData['Organization_Village'] = '';
                    $postData['Organization_Gram'] = '';
                    $postData['Organization_Panchayat'] = '';
                }

                $_SelectQuery = "UPDATE tbl_organization_detail SET
                                     Organization_Type = '" . $postData['Organization_Type'] . "',
                                     Organization_Address = '" . $postData['Organization_Address'] . "',
                                     Organization_AreaType = '" . $postData['Organization_AreaType'] . "',
                                     Organization_WardNo = '" . $postData['Organization_WardNo'] . "',
                                     Organization_Municipal = '" . $postData['Organization_Municipal'] . "',
                                     Organization_Municipal_Type = '" . $postData['Organization_Municipal_Type'] . "',
                                     Organization_Village = '" . $postData['Organization_Village'] . "',
                                     Organization_Gram = '" . $postData['Organization_Gram'] . "',
                                     Organization_Panchayat = '" . $postData['Organization_Panchayat'] . "'  
                                     WHERE Organization_User = '" . $postData['fld_ITGK_UserCode'] . "'  ";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch
        (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function commitChangeAddressFinal2($postData) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            $_SelectQuery = "UPDATE tbl_address_name_transaction SET fld_paymentTitle = '" . $postData['fld_paymentTitle'] . "', fld_paymentStatus='1', fld_transactionID = '" . $postData['fld_transactionID'] . "', fld_bankTransactionID = '" . $postData['fld_bankTransactionID'] . "',fld_amount = '" . $postData['fld_amount'] . "' WHERE fld_ref_no = '" . $postData['fld_ref_no'] . "'  ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch
        (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getCenterDetails($_Code) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            $_SelectQuery = "SELECT tcai.fld_ref_no AS ref_no, tcai.fld_RSP AS fld_rspCode, tcai.fld_requestChangeType AS requestType, tum.User_Code AS User_Code, tum.User_EmailId AS User_EmailId, tum.User_MobileNo AS User_MobileNo,tum.User_LoginId AS ITGK_Name FROM tbl_user_master tum LEFT JOIN tbl_change_address_itgk tcai ON tum.User_Code = tcai.fld_ITGK_UserCode WHERE tcai.fld_ref_no = '" . $_Code . "'";


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function sendSMSEmail($postData) {

        $variableArray = [];
        $_SMS = '';

        $_Mobile = $postData["User_MobileNo"];
        $_Email = $postData["User_EmailId"];
        $type = $postData["type"];

        $variableArray["centerID"] = $postData["ITGK_Name"];
        $variableArray["ref_no"] = $postData["ref_no"];
        $variableArray["loginLink"] = "https://www.myrkcl.com";
        $variableArray["ITGK_Name"] = $postData["ITGK_Name"];
        $variableArray["User_LoginId"] = $postData["ITGK_Name"];
        $variableArray["requestType"] = ucfirst($postData["requestType"]);
        $sendEmail = new sendMail();

        if ($type == "accepted") {

            $variableArray["username"] = "ITGK Center - " . $postData["ITGK_Name"];
            $variableArray["htmlMessage"] = "Your " . $variableArray["requestType"] . " Update Request (Ref. Id: <span style=\"color: orangered;\">" . $postData["ref_no"] . "</span>) has been approved by your service provider. The request has been forwarded to RKCL for further verification, once done you will be notified.";

            $sendEmail->sendEmailHTML($_Email, $variableArray["ITGK_Name"], $variableArray["requestType"] . " Update Request approved by Service Provider : Ref. Id : " . $variableArray["ref_no"], "send_email_template.php", $variableArray);

            $_SMS = "Dear ITGK Center (" . $variableArray["centerID"] . "), Your " . $variableArray["requestType"] . " Update Request: " . $variableArray["ref_no"] . ", has been approved by your service provider. The request has been forwarded to RKCL for further approval.";
        }

        if ($type == "accepted2") {

            $variableArray["username"] = "ITGK Center - " . $postData["ITGK_Name"];
            $variableArray["htmlMessage"] = "Your " . $variableArray["requestType"] . " Update Request (Ref. Id: <span style=\"color: orangered;\">" . $postData["ref_no"] . "</span>) has been Approved by RKCL. You are now required to Pay the fee for the same in order to reflect the changes, kindly login at MyRKCL Portal for more details.";

            $sendEmail->sendEmailHTML($_Email, $variableArray["ITGK_Name"], $variableArray["requestType"] . " Update Request approved by RKCL : Ref. Id : " . $variableArray["ref_no"], "send_email_template.php", $variableArray);
            $_SMS = "Dear ITGK Center (" . $variableArray["centerID"] . "), Your " . $variableArray["requestType"] . " Update Request: " . $variableArray["ref_no"] . ", has been approved by RKCL. Please pay the fee in order to reflect the changes. Kindly login at MyRKCL portal for more details.";
        }

        if ($type == "paid") {

            $variableArray["username"] = "ITGK Center - " . $postData["ITGK_Name"];
            $variableArray["htmlMessage"] = "We have received payment against " . $variableArray["requestType"] . " Update Request (Ref. Id : <span style=\"color: orangered;
\">" . $postData["ref_no"] . "</span>), Kindly login at MyRKCL Portal for more details.";

            $sendEmail->sendEmailHTML($_Email, $variableArray["ITGK_Name"], "Payment Received against " . $variableArray["requestType"] . " Update Request : Ref. Id : " . $variableArray["ref_no"], "send_email_template.php", $variableArray);
            $_SMS = "Dear ITGK Center (" . $variableArray["centerID"] . "), We have received your payment against " . $variableArray["requestType"] . " Update Request: " . $variableArray["ref_no"] . ", the changes has been reflected as per your request. Kindly login at MyRKCL portal for more details.";
        }

        if ($type == "rejected") {

            $variableArray["username"] = "ITGK Center - " . $postData["ITGK_Name"];
            $variableArray["htmlMessage"] = "We are sorry to inform you that your " . $variableArray["requestType"] . " Update Request (Ref. Id : <span style=\"color: orangered;\">" . $postData["ref_no"] . "</span>) has been rejected by RKCL. Kindly Login at MyRKCL for more detailed remarks.";

            $sendEmail->sendEmailHTML($_Email, $variableArray["ITGK_Name"], $variableArray["requestType"] . " Update Request rejected by RKCL : Ref. Id : " . $variableArray["ref_no"], "send_email_template.php", $variableArray);

            $_SMS = "Dear ITGK Center (" . $variableArray["centerID"] . "), Your " . $variableArray["requestType"] . " Update Request: " . $variableArray["ref_no"] . ", has been rejected by RKCL. Kindly login at MyRKCL portal for more detailed remarks.";
        }

        if ($type == "revoked") {

            $variableArray["username"] = "ITGK Center - " . $postData["ITGK_Name"];
            $variableArray["htmlMessage"] = "Your " . $variableArray["requestType"] . " update request (Ref. No. <span style=\"color: orangered;\">" . $postData["ref_no"] . "</span>) has been revoked by RKCL. Kindly Login at MyRKCL portal for more details.";

            $sendEmail->sendEmailHTML($_Email, $variableArray["ITGK_Name"], $variableArray["requestType"] . " Update Request revoked : " . $variableArray["ref_no"], "send_email_template.php", $variableArray);

            $_SMS = "Dear ITGK Center (" . $variableArray["centerID"] . "), Your " . $variableArray["requestType"] . " Update Request: " . $variableArray["ref_no"] . ", has been successfully revoked. Kindly login at MyRKCL portal for more details.";
        }
        SendSMS($_Mobile, $_SMS);
    }

    public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_netbanking ORDER BY BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getPaymentDetails($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_address_name_transaction WHERE fld_ref_no = '" . $_Code . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    function getTransactionStatusByPayU($response) {
        $txnid = $response['txnid'];
        if (!empty($txnid)) {

            $key = ($_SERVER["HTTP_HOST"] == "localhost") ? "gtKFFx" : "X2ZPKM";
            $salt = ($_SERVER["HTTP_HOST"] == "localhost") ? "eCwWELxi" : "8IaBELXB";

            $command = "verify_payment";
            $hash_str = $key . '|' . $command . '|' . $txnid . '|' . $salt;
            $hash = strtolower(hash('sha512', $hash_str));
            $request = [
                'key' => $key,
                'hash' => $hash,
                'var1' => $txnid,
                'command' => $command
            ];

            $qs = http_build_query($request);

            $wsUrl = ($_SERVER['HTTP_HOST'] == "localhost") ? "https://test.payu.in/merchant/postservice.php?form=2" : "https://info.payu.in/merchant/postservice?form=2";

            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $wsUrl);
            curl_setopt($c, CURLOPT_POST, 1);
            curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
            curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            $o = curl_exec($c);
            if (curl_errno($c)) {
                $c_error = curl_error($c);
                if (empty($c_error)) {
                    $c_error = 'Some server error';
                }
                return array('curl_status' => 'FAILURE', 'error' => $c_error);
            }
            $out = trim($o);
            $arr = json_decode($out, true);


            $payuResponses = $arr;
            $response['key'] = $key;
            $response['hash'] = $hash;
        }
        if (!empty($payuResponses['status']) && $payuResponses['status'] == 1) {
            foreach ($payuResponses['transaction_details'] as $val) {
                $status = $val['status'];
                $response['amount'] = intval($val['amt']);
            }
        } else {
            $status = 'PaymentnotDone';
        }

        $response['status'] = $status;

        return $response;
    }

    public function getInvoiceMasterDetails($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_gst_invoice_master WHERE Gst_Invoce_Category_Type = '" . $_Code . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function generateInvoice($postedData, $dbData) {
        $fld_referenceID = $postedData['fld_ref_no'];
        $fld_gst_invoice_masterID = $dbData['Gst_Invoce_Code'];
        $fld_ITGK_Code = $_SESSION['User_LoginId'];
        $fld_ITGK_UserCode = $_SESSION['User_Code'];
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_nlc_itgk_gstinvoice (fld_ITGK_Code, fld_ITGK_UserCode, fld_referenceID, fld_gst_invoice_masterID) VALUES ('" . $fld_ITGK_Code . "', '" . $fld_ITGK_UserCode . "', '" . $fld_referenceID . "', " . $fld_gst_invoice_masterID . ")";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            $fld_invoice = "NLC/" . (mysqli_insert_id() + 999);
            $_UpdateQuery = "UPDATE tbl_nlc_itgk_gstinvoice SET fld_invoice = '" . $fld_invoice . "' WHERE fld_ID = " . mysqli_insert_id() . " ";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getITGKInvoiceDetails($fld_referenceID) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $_SelectQuery = "SELECT a.fld_invoice AS invoice_no, DATE_FORMAT(a.fld_addedOn,'%d/%m/%Y') AS transaction_date, 
//                                 a.fld_ITGK_Code AS ITGK_code, d.Organization_Name AS ITGK_name, 
//                                 d.Organization_Address AS ITGK_address, e.User_MobileNo AS ITGK_mobile, c.Bank_Gstin_No AS ITGK_gstin, b.Gst_Invoce_SAC AS sac,
//                                 b.Gst_Invoce_CGST AS cg, b.Gst_Invoce_SGST AS sg, REPLACE(FORMAT(b.Gst_Invoce_TotalFee, 2),',','') AS amount_figures, 
//                                 b.Gst_Invoce_BaseFee AS service_charge, b.Gst_Invoce_Category_Type AS service 
//                                 FROM tbl_nlc_itgk_gstinvoice a 
//                                 LEFT JOIN tbl_gst_invoice_master b ON a.fld_gst_invoice_masterID = b.Gst_Invoce_Code
//                                 LEFT JOIN tbl_bank_account c ON a.fld_ITGK_Code = c.Bank_User_Code
//                                 LEFT JOIN tbl_organization_detail d ON d.Organization_User = a.fld_ITGK_UserCode
//                                 LEFT JOIN tbl_user_master e ON a.fld_ITGK_UserCode = e.User_Code
//                                 WHERE a.fld_referenceID = '" . $fld_referenceID . "' ";
             $_SelectQuery = "SELECT a.fld_invoice AS invoice_no, DATE_FORMAT(a.fld_addedOn,'%d/%m/%Y') AS transaction_date, 
                                 a.fld_ITGK_Code AS ITGK_code, d.Organization_Name AS ITGK_name, 
                                 d.Organization_Address AS ITGK_address, e.User_MobileNo AS ITGK_mobile, c.Bank_Gstin_No AS ITGK_gstin, b.Gst_Invoce_SAC AS sac,
                                 b.Gst_Invoce_CGST AS cg, b.Gst_Invoce_SGST AS sg, REPLACE(FORMAT(f.fld_amount, 2),',','') AS amount_figures, 
                                 b.Gst_Invoce_BaseFee AS service_charge, b.Gst_Invoce_Category_Type AS service 
                                 FROM tbl_nlc_itgk_gstinvoice a 
                                 LEFT JOIN tbl_gst_invoice_master b ON a.fld_gst_invoice_masterID = b.Gst_Invoce_Code
                                 LEFT JOIN tbl_bank_account c ON a.fld_ITGK_Code = c.Bank_User_Code
                                 LEFT JOIN tbl_organization_detail d ON d.Organization_User = a.fld_ITGK_UserCode
                                 LEFT JOIN tbl_user_master e ON a.fld_ITGK_UserCode = e.User_Code 
                                 inner join tbl_address_name_transaction as f on a.fld_referenceID=f.fld_ref_no 
                                 WHERE a.fld_referenceID = '" . $fld_referenceID . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getIndianCurrency($number) {
        $decimal = round($number - ($no = floor($number)), 2) * 10;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
            } else
                $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? $Rupees . '' : '') . $paise;
    }

    public function gps2Num($coordPart) {
        $parts = explode('/', $coordPart);
        if (count($parts) <= 0)
            return 0;
        if (count($parts) == 1)
            return $parts[0];
        return floatval($parts[0]) / floatval($parts[1]);
    }

    public function get_image_location($image = '') {
        $exif = exif_read_data($image, 0, true);
        if ($exif && isset($exif['GPS'])) {
            $GPSLatitudeRef = $exif['GPS']['GPSLatitudeRef'];
            $GPSLatitude = $exif['GPS']['GPSLatitude'];
            $GPSLongitudeRef = $exif['GPS']['GPSLongitudeRef'];
            $GPSLongitude = $exif['GPS']['GPSLongitude'];

            $lat_degrees = count($GPSLatitude) > 0 ? $this->gps2Num($GPSLatitude[0]) : 0;
            $lat_minutes = count($GPSLatitude) > 1 ? $this->gps2Num($GPSLatitude[1]) : 0;
            $lat_seconds = count($GPSLatitude) > 2 ? $this->gps2Num($GPSLatitude[2]) : 0;

            $lon_degrees = count($GPSLongitude) > 0 ? $this->gps2Num($GPSLongitude[0]) : 0;
            $lon_minutes = count($GPSLongitude) > 1 ? $this->gps2Num($GPSLongitude[1]) : 0;
            $lon_seconds = count($GPSLongitude) > 2 ? $this->gps2Num($GPSLongitude[2]) : 0;

            $lat_direction = ($GPSLatitudeRef == 'W' or $GPSLatitudeRef == 'S') ? -1 : 1;
            $lon_direction = ($GPSLongitudeRef == 'W' or $GPSLongitudeRef == 'S') ? -1 : 1;

            $latitude = $lat_direction * ($lat_degrees + ($lat_minutes / 60) + ($lat_seconds / (60 * 60)));
            $longitude = $lon_direction * ($lon_degrees + ($lon_minutes / 60) + ($lon_seconds / (60 * 60)));

            return array('latitude' => $latitude, 'longitude' => $longitude);
        } else {
            return false;
        }
    }

    public function getLatitudeLongitude($_Code) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            $_SelectQuery = "SELECT Organization_long, Organization_lat FROM tbl_organization_detail  WHERE Organization_User = '" . $_Code . "'   ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch
        (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function updateLatitudeLongitude($postData) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            $_SelectQuery = "UPDATE tbl_organization_detail SET Organization_long = '" . $postData['longitude'] . "',  Organization_lat='" . $postData['latitude'] . "'  WHERE Organization_User = '" . $postData['fld_UserCode'] . "'   ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);

            $_Insert = "INSERT INTO tbl_latlonglogs (fld_UserCode, fld_UserRoll, fld_oldCordinates, fld_newCordinates, fld_formattedAddress, fld_addedOn) VALUES ('" . $postData['fld_UserCode'] . "', '" . $postData['fld_UserRoll'] . "', '" . $postData['fld_oldCordinates'] . "', 
'" . $postData['fld_newCordinates'] . "', '" . $postData['fld_formattedAddress'] . "', NOW() )";

            $_Response2 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        } catch
        (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function fetchAddress($postData) {
        $latitude = $postData['latitude'];
        $longitude = $postData['longitude'];
        if (!empty($latitude) && !empty($longitude)) {
            $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false');
            $output = json_decode($geocodeFromLatLong);
            $status = $output->status;
            $address = ($status == "OK") ? $output->results[1]->formatted_address : '';
            if (!empty($address)) {
                return $address;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*     * * GENERATE RKCL TRANSACTION ID HERE ** */

    public function generateID() {
        $a = '';
        for ($i = 0; $i < 16; $i++) {
            $a .= mt_rand(0, 9);
        }
        return $a;
    }

    public function addTransaction($postData) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            $_Insert = "INSERT INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_Fname, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray, Pay_Tran_LCount, Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_PG_Trnid, timestamp, paymentgateway) VALUES ('" . $postData['Pay_Tran_ITGK'] . "', '" . $postData['Pay_Tran_Fname'] . "', '" . $postData['Pay_Tran_RKCL_Trnid'] . "', '" . $postData['Pay_Tran_AdmissionArray'] . "', '" . $postData['Pay_Tran_LCount'] . "', '" . $postData['Pay_Tran_Amount'] . "', '" . $postData['Pay_Tran_Status'] . "', '" . $postData['Pay_Tran_ProdInfo'] . "', '" . $postData['Pay_Tran_PG_Trnid'] . "', NOW(), '" . $postData['paymentgateway'] . "')";

            $_Response2 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        } catch
        (Exception $_ex) {
            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response2;
    }

    public function updateTransaction($postData) {
        try {
            global $_ObjConnection;
            $_ObjConnection->Connect();

            $_SelectQuery = "UPDATE tbl_payment_transaction SET Pay_Tran_Status = '" . $postData['Pay_Tran_Status'] . "' WHERE Pay_Tran_PG_Trnid = '" . $postData['Pay_Tran_PG_Trnid'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
        } catch
        (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
