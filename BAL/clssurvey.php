<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clssurvey
 *
 *  author yogendra
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clssurvey {

    //put your code here


    public function Showsurvey($_actionvalue) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
            $date = date("Y-m-d");
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select * from tbl_survey as a inner join tbl_survey_master as b on a.Servey_Code=b.Survey_id where Servey_Code='$_actionvalue' AND b.Survey_Date1<='" . $date . "' AND b.Survey_Date2>='" . $date . "'";


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function INSERT($_surveyid) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

                mysqli_set_charset('utf8');
				$_surveyid = mysqli_real_escape_string($_ObjConnection->Connect(),$_surveyid);
				
                $ques_array = $_POST['txtquestion'];
                $ans_array = $_POST['Surveyans'];
				 
                for ($i = 0; $i < count($ques_array); $i++) {

                    $question = mysqli_real_escape_string($ques_array[$i]);
                    $answers = mysqli_real_escape_string($ans_array[$i]);
                    $_InsertQuery = "Insert Into tbl_survey_results(survey_code,survey_question,survey_centercode,survey_answers,survey_sp) 
													 VALUES ('" . $_surveyid . "','" . $question . "',
													 '" . $_SESSION['User_LoginId'] . "','" . $answers . "','" . $_SESSION['User_Rsp'] . "'
													  )";
					$query = "INSERT IGNORE INTO tbl_fixed_popup_viewedby SET 
								usercode = '" . $_SESSION['User_LoginId'] . "', popupid = '5'";
        
                    $_DuplicateQuery = "Select * From tbl_survey_results Where survey_centercode='" . $_SESSION['User_LoginId'] . "' and survey_question='" . $question . "' and survey_code='" . $_surveyid . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                    //print_r($_Response);

                    if ($_Response[0] == Message::NoRecordFound) {
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
						$_Responses = $_ObjConnection->ExecuteQuery($query, Message::InsertStatement);
                        // $_UpdateQuery = "Update tbl_intake_master set Intake_Available=Intake_Available - 1 ,Intake_Consumed = Intake_Consumed + 1 Where Intake_Center = '" .$_ITGK_Code  . "' AND Intake_Batch = '" . $_LearnerBatch . "' ";
                        //$_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    } else {
                        $_Response[0] = Message::DuplicateRecord;
                        $_Response[1] = Message::Error;
                    }
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        //print_r($_Response);
        return $_Response;
    }

    public function GetSurvey() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
			$date = date("Y-m-d");

            $_SelectQuery = "Select Survey_id,Survey_Name from tbl_survey_master where Survey_Date1<='" . $date . "' AND Survey_Date2>='" . $date . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetResult($_actionvalue) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			
            $_SelectQuery = "SELECT survey_code,survey_question,survey_centercode,survey_answers,COUNT(survey_answers) FROM tbl_survey_results where survey_code='$_actionvalue' GROUP BY survey_answers,survey_question";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
