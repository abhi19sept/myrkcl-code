<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php'; // added by sunil
$_ObjConnection = new _Connection();
$_Response = array();

class clsLogin {

    
     /* Added By Sunil : For mobile Deatil varify*/
    
     public function GetVarifyLearnerCode($_LearnerCode)
    {
        global $_ObjConnection,$_Response;
        $_ObjConnection->Connect();
        try
        {
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				
            $_SelectQuery="Select * From tbl_admission Where Admission_LearnerCode='" . $_LearnerCode . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery,  Message::SelectStatement);
            
        }
        catch(Exception $_ex)
        {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetVarifyLearnerMobileNo($_MobileNo,$_DOB)
    {
        global $_ObjConnection,$_Response;
        $_ObjConnection->Connect();
        try
        {
			$_MobileNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_MobileNo);
			
            $_SelectQuery="Select * From tbl_admission Where Admission_Mobile='" . $_MobileNo . "' or Admission_DOB='" . $_DOB . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery,  Message::SelectStatement);
            
        }
        catch(Exception $_ex)
        {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function AddLernerMob($_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        //$_SMS = "OTP for your Learner Login in MYRKCL Application is " . $_OTP;
          $_SMS = $_OTP." is the OTP for your Learner Login in MYRKCL Application.";


        try {
            /* Here it checking that the OTP in which we are sending that is allredy in our database or not.*/
				$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
				
            $_SelectQuery = "Select * FROM tbl_ao_learner_register WHERE AO_Mobile = '" . $_Mobile . "' AND AO_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response1);
            if ($_Response1[0] == 'Success') 
                {
                $_Row = mysqli_fetch_array($_Response1[2]);
                //print_r($_Row);
                $old_OTP=$_Row['AO_OTP'];
                //$_OLD_SMS = "OTP for your Learner Login in MYRKCL Application is  " . $old_OTP;
                
                $_OLD_SMS = $old_OTP. "is the OTP for your Learner Login in MYRKCL Application.";
                
                SendSMS($_Mobile, $_OLD_SMS);
                return $_Response1;
            }
            else{
                $_InsertQuery = "Insert Into tbl_ao_learner_register(AO_Code,AO_Mobile,AO_OTP)"
                    . "Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,"
                    . "'" . $_Mobile . "' as AO_Mobile,'"
                    . "" . $_OTP . " as AO_OTP '"
                    . " From tbl_ao_learner_register";
                //echo $_InsertQuery;die;            
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
                return $_Response;
            }
            //die;
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    public function verifyLotp($_mobile, $_otp) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_mobile);
				$_otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_otp);
				
            $_SelectQuery = "Select * FROM tbl_ao_learner_register WHERE AO_Mobile = '" . $_mobile . "' AND AO_OTP = '" . $_otp . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response[0]);
            if ($_Response1[0] == Message::NoRecordFound) {
                //echo "Invalid OTP. Please Try Again";
                $er='InvalidOTP';
                return $er;
            } else {
                $_UpdateQuery = "Update tbl_ao_learner_register set AO_Status='1' WHERE AO_Mobile='" . $_mobile . "' AND AO_OTP = '" . $_otp . "' AND AO_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_ao_learner_register WHERE AO_Mobile='" . $_mobile . "' AND AO_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    
    public function GetLernerLoginDetail($_MobileNo, $_LearnerCode) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_MobileNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_MobileNo);
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				
          $_SelectQuery = "select b.Course_Name,c.Batch_Name, a.* from tbl_admission a
                            INNER JOIN tbl_course_master as b ON a.Admission_Course = b.Course_Code
                            INNER JOIN tbl_batch_master as c ON a.Admission_Batch = c.Batch_Code
                            where Admission_LearnerCode='" . $_LearnerCode . "' AND Admission_Mobile ='" . $_MobileNo . "'";
             $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetLernerLoginDetailFinal($_LearnerCode) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				
          $_SelectQuery = "select b.Course_Name,c.Batch_Name, a.* from tbl_admission a
                            INNER JOIN tbl_course_master as b ON a.Admission_Course = b.Course_Code
                            INNER JOIN tbl_batch_master as c ON a.Admission_Batch = c.Batch_Code
                            where a.Admission_LearnerCode='" . $_LearnerCode . "' ";
             $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetITGKUserCode($_ITGKCode) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_ITGKCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGKCode);
				
              $_SelectQuery = "Select * from tbl_govempentryform where Govemp_ITGK_Code ='" . $_ITGKCode . "'";
              $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
   /* Added By Sunil : For mobile Deatil varify*/
    
    public function GetLoginDetail($_UserName, $_Password) {
        global $_ObjConnection, $_Response, $general;
        $_ObjConnection->Connect();
        try {
				$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
				
            $_SelectQuery = "Select a.*, b.*, c.* FROM tbl_user_master as a INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code INNER JOIN tbl_organization_detail
							as c on a.User_Code=c.Organization_User WHERE a. User_LoginId = '" . $_UserName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                $_Row = mysqli_fetch_array($_Response[2]);
                $encpass = $general->encrypt_with_key($_Row['User_Password']);
                $dbpassword = $general->decrypt_with_key($encpass);
                if ($dbpassword == $_Password) {
                    $query = "UPDATE tbl_user_master SET User_Password = '" . $encpass . "' WHERE User_Code = '" . $_Row['User_Code'] . "'";
                   // $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
                    $_Response[2] = $_Row;
                } else {
                   // $dbpassword = $general->decrypt_with_key($_Row['User_Password']);
				    $dbpassword = $_Row['User_Password'];
                    $_Password = $general->encrypt_with_key($_Password);
                    if ($dbpassword == $_Password) {
                        $_Response[2] = $_Row;
                    } else {
                        $_Response[2] = 0;
                    }
                }
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetRootMenuByUserRole($_UserRole) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_UserRole = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserRole);
				
            $_SelectQuery = "Select * From vw_userrolewiserootmenu Where UserRole='" . $_UserRole . "' Order By DisplayOrder";
         
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetParentMenuByUserRole($_UserRole, $_RootMenu) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_UserRole = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserRole);
				$_RootMenu = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootMenu);
				
            $_SelectQuery = "Select * From vw_userrolewiseparentmenu Where UserRole='" . $_UserRole . "' and RootMenu='" . $_RootMenu . "' Order By DisplayOrder";
          
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetChildMenuUserRole($_UserRole, $_Parent) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_UserRole = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserRole);
				$_Parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent);
				
            $_SelectQuery = "Select * From vw_userrolewisefunction where "
                    . "UserRole='" . $_UserRole . "' and Parent='" . $_Parent . "' Order by Display";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Get1() {
        global $_ObjConnection;
        $reg=$_ObjConnection->Connect();
        try {
            mysqli_set_charset($reg,'utf8');
            $_SelectQuery = "Select id,value,news,link From tb1_news_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SaveSession($User_LoginId, $User_UserRoll, $User_ParentId, $User_Status, $User_Code, $User_EmailId, $UserRoll_Name, $Organization_Name) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $minutes = 120;
            $_DeleteQuery = "delete FROM tbl_user_session WHERE User_Session_LoginId='" . $User_LoginId . "' AND User_Session_Timestamp < DATE_SUB(NOW(),INTERVAL $minutes MINUTE)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);

            $_InsertQuery = "Insert Into tbl_user_session(User_Session_Code,User_Session_LoginId,User_Session_UserRoll,"
                    . "User_Session_ParentId,User_Session_Status,User_Session_UserCode,User_Session_Email,"
                    . "User_Session_UserName,User_Session_OrgName) "
                    . "Select Case When Max(User_Session_Code) Is Null Then 1 Else Max(User_Session_Code)+1 End as User_Session_Code,"
                    . "'" . $User_LoginId . "' as User_Session_LoginId,'" . $User_UserRoll . "' as User_Session_UserRoll,'" . $User_ParentId . "' as User_Session_ParentId,"
                    . "'" . $User_Status . "' as User_Session_Status,'" . $User_Code . "' as User_Session_UserCode,'" . $User_EmailId . "' as User_Session_Email,"
                    . "'" . $UserRoll_Name . "' as User_Session_UserName,'" . $Organization_Name . "' as User_Session_OrgName"
                    . " From tbl_user_session";

            $_DuplicateQuery = "Select * From tbl_user_session Where User_Session_LoginId='" . $User_LoginId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
     
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function CheckITGKExpireDate($itgkCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkCode);
		
        $_SelectQuery = "SELECT CourseITGK_ExpireDate, DATEDIFF(now(), CourseITGK_ExpireDate) AS days, IF(DATE(CourseITGK_ExpireDate) < now(), 'Expired', IF(DATE(CourseITGK_ExpireDate) > (now() + 30), 'Renewed', IF(DATE(CourseITGK_ExpireDate) > now(),'ExpireSoon', 'ExpireToday'))) AS status FROM tbl_courseitgk_mapping WHERE Courseitgk_ITGK = '" . $itgkCode . "' AND Courseitgk_EOI = 1 AND CourseITGK_Renewal_Status = 'Y' AND EOI_Fee_Confirm = 1 ORDER BY Courseitgk_Code ASC LIMIT 1";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $type = 'RS-CIT';
        if ($_Response[0] == Message::NoRecordFound) {
            $type = 'RS-CFA';
            $_SelectQuery = "SELECT CourseITGK_ExpireDate, DATEDIFF(now(), CourseITGK_ExpireDate) AS days, IF(DATE(CourseITGK_ExpireDate) < now(), 'Expired', IF(DATE(CourseITGK_ExpireDate) > (now() + 30), 'Renewed', IF(DATE(CourseITGK_ExpireDate) > now(),'ExpireSoon', 'ExpireToday'))) AS status FROM tbl_courseitgk_mapping WHERE Courseitgk_ITGK = '" . $itgkCode . "' AND Courseitgk_EOI IN (5,6,7) AND CourseITGK_Renewal_Status = 'Y' AND EOI_Fee_Confirm = 1 ORDER BY Courseitgk_Code ASC LIMIT 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        $result = 0;
        if ($_Response[0] != Message::NoRecordFound) {
            $row = mysqli_fetch_array($_Response[2]);
            $renewLink = '<a href="frmusermasterdetails.php"><b>Click here</b></a> to renew.';
            switch($row['status']) {
                case 'ExpireToday':
                    $result = 'Dear ITGK, your ' . $type . ' authorization with RKCL will expire today. ' . $renewLink;
                    break;
                case 'Expired':
                    $result = 'Dear ITGK, your ' . $type . ' authorization with RKCL is expired. You have not renewed your center in past ' . $row['days'] . ' days. ' . $renewLink;
                    break;
                case 'ExpireSoon':
                    $result = 'Dear ITGK, your ' . $type . ' authorization with RKCL is about to expire in ' . $row['days'] . ' days. ' . $renewLink;
                    break;
            }
        }

        return $result;
    }
    
    
        /**
         * @function GetCenterWiseReport
         * @param $CenterCode  
         * @return mixed|array : returns array 
         * @Description : This function work when any ITGK is block / unblock for any reason. Gives some detail for that.
         */
    
    public function GetCenterWiseReport($CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_SelectQuery = "Select activitydate,activityreason,remark,duration From block Where CenterCode='" . $CenterCode . "' Order By Id Desc";
				$CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CenterCode);
				
            $_SelectQuery = "Select count(*) as count,activitydate From block Where CenterCode='" . $CenterCode . "' Order By Id Desc";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    /* Added By Sunil : 25-10-2018*/
    
     public function GetLoginDetailNewOld($_UserName, $_Password) {
        
         global $_ObjConnection, $_Response, $general;
        $_ObjConnection->Connect();
        try {
				$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
				
            function encryptPasswordFinal($password, $hashkey) {
                    return md5(sha1($password . $hashkey));
            } 
            $_SelectQuery = "Select a.*, b.*, c.*,d.*,e.*
                            FROM tbl_user_master as a 
                            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
                            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
                            INNER JOIN tbl_into_s as d on a.User_Code=d.User_Code
                            LEFT JOIN tbl_phrase_ke as e on d.Phrase_Ke=e.Phrase_ID
                            WHERE d.User_LoginId = '" . $_UserName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
            if (mysqli_num_rows($_Response[2])) {
                $_Row = mysqli_fetch_array($_Response[2],true);
                //echo "<pre>"; print_r($_Row);die;
            
                $txtPassword = encryptPasswordFinal($_Password, $_Row["Phrase_Ke"]); //echo "<br>";
                $hash = $_Row["Open_Sesame_En"];
                if (password_verify($txtPassword, $hash)) {
                        $_Response[2] = $_Row;
                } else {
                        $_Response[2] = 0;    
                }

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }           
    
     public function GetLoginDetailNew18122018($_UserName, $_Password) {
        
        global $_ObjConnection, $_Response, $general;
        $_ObjConnection->Connect();
        try {
				$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
				
            function encryptPasswordLogin1($password, $hashkey) {
                    return md5(sha1($password . $hashkey));
            }
          
//            $_SelectQuery ="Select a.*, b.*, c.* 
//            FROM tbl_user_master as a 
//            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
//            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
//            WHERE a. User_LoginId = '" . $_UserName . "'";
           
            // NEW FILTERED SQL FOR LOGIN
            $_SelectQuery ="Select a.User_Code,a.User_EmailId,a.User_MobileNo,a.User_LoginId,
            a.User_UserRoll,a.User_ParentId,a.User_Rsp,a.User_Status,
            b.UserRoll_Name, 
            c.Organization_Name,c.Organization_District 
            FROM tbl_user_master as a 
            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
            WHERE a.User_LoginId = '" . $_UserName . "' and a.User_Status = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if($_Response[0]=='Success')
                {
                        $_Row = mysqli_fetch_array($_Response[2],true);
                        
                        // FOR SENDING MESSAGE TO SUPERADMIN IF LOGIN
                        if($_UserName=='superadmin' || $_UserName=='knaresh')
                        {
                           $_SMS = " Dear ".$_UserName.", you have just logged into your MYRKCL. If you are not, please contact your administrator. Or Call 0141-5159700 (if in India).";
                           SendSMS($_Row['User_MobileNo'], $_SMS);
                           //return $_Response1;
                        }
                        // FOR SENDING MESSAGE TO SUPERADMIN IF LOGIN
                         
                        //echo "<pre>"; print_r($_Row);
//                        if($_Row['User_Status'] == '2') {
//                                $_Response[0] = 'AccountisInactive';//Your Account is Inactive. Please contact your Service Provider for Reactivation.
//                        }
//                        else
//                        {
                            
//                            $_SelectQuery = "Select a.*, b.*, c.*,d.*,e.*
//                            FROM tbl_user_master as a 
//                            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
//                            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
//                            INNER JOIN tbl_into_s as d on a.User_Code=d.User_Code
//                            LEFT JOIN tbl_phrase_ke as e on d.Phrase_Ke=e.Phrase_ID
//                            WHERE d.User_LoginId = '" . $_UserName . "'";
                            
                            $_SelectQuery = "Select a.User_Code,a.User_EmailId,a.User_MobileNo,a.User_LoginId,
                            a.User_UserRoll,a.User_ParentId,a.User_Rsp,a.User_Status,
                            b.UserRoll_Name, 
                            c.Organization_Name,c.Organization_District,
                            d.Open_Sesame,d.Open_Sesame_En,d.Phrase_Ke,d.Open_Sesame_Update_Date,
                            e.Phrase_Ke
                            FROM tbl_user_master as a 
                            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
                            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
                            INNER JOIN tbl_into_s as d on a.User_Code=d.User_Code
                            LEFT JOIN tbl_phrase_ke as e on d.Phrase_Ke=e.Phrase_ID
                            WHERE d.User_LoginId = '" . $_UserName . "'";
                            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                            //echo "<pre>"; print_r($_Response);die;
                            if($_Response[0]=='Success')
                                {
                                    $_Row = mysqli_fetch_array($_Response[2],true);
                                    //echo "<pre>"; print_r($_Row);die;

                                    $txtPassword = encryptPasswordLogin1($_Password, $_Row["Phrase_Ke"]); //echo "<br>";
                                    $hash = $_Row["Open_Sesame_En"];
                                    if (password_verify($txtPassword, $hash)) {
                                            $_Response[2] = $_Row;
                                    } else {
                                            $_Response[2] = 0;    
                                    }
                                }
                            else
                                {
                                    $_Response[0] ='NORECORDINNEWTABLE';// No Record found in new table
                                }
                            
                            
                       // }
                }
            
            
            
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }  
    
     public function GetLoginDetailNew($_UserName, $_Password) {
        
        global $_ObjConnection, $_Response, $general;
        $_ObjConnection->Connect();
        try {
            function encryptPasswordLogin($password, $hashkey) {
                    return md5(sha1($password . $hashkey));
            }

			$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
            // NEW FILTERED SQL FOR LOGIN
            $_SelectQuery ="Select a.User_Code,a.User_EmailId,a.User_MobileNo,a.User_LoginId,
            a.User_UserRoll,a.User_ParentId,a.User_Rsp,a.User_Status,
            b.UserRoll_Name, 
            c.Organization_Name,c.Organization_District 
            FROM tbl_user_master as a 
            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
            WHERE a.User_LoginId = '" . $_UserName . "' and a.User_Status = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if($_Response[0]=='Success')
                {
                        $_Row = mysqli_fetch_array($_Response[2],true);
                        
                        // FOR SENDING MESSAGE TO SUPERADMIN IF LOGIN
                        if($_Row['User_Code']==1)
                        {
                           $_SMS = " Dear ".$_UserName.", you have just logged into MYRKCL Portal.";
                           SendSMS($_Row['User_MobileNo'], $_SMS);
                           //return $_Response1;
                        }
                        // FOR SENDING MESSAGE TO SUPERADMIN IF LOGIN
                                                   
                            $_SelectQuery = "Select a.User_Code,a.User_EmailId,a.User_MobileNo,a.User_LoginId,
                            a.User_UserRoll,a.User_ParentId,a.User_Rsp,a.User_Status,
                            b.UserRoll_Name, 
                            c.Organization_Name,c.Organization_District,
                            d.Open_Sesame,d.Open_Sesame_En,d.Phrase_Ke,d.Open_Sesame_Update_Date,
                            e.Phrase_Ke
                            FROM tbl_user_master as a 
                            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
                            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
                            INNER JOIN tbl_into_s as d on a.User_Code=d.User_Code
                            LEFT JOIN tbl_phrase_ke as e on d.Phrase_Ke=e.Phrase_ID
                            WHERE d.User_LoginId = '" . $_UserName . "'";
                            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                            //echo "<pre>"; print_r($_Response);die;
                            if($_Response[0]=='Success')
                                {
                                    $_Row = mysqli_fetch_array($_Response[2],true);
                                    //echo "<pre>"; print_r($_Row);die;

                                    $txtPassword = encryptPasswordLogin($_Password, $_Row["Phrase_Ke"]); //echo "<br>";
                                    $hash = $_Row["Open_Sesame_En"];
                                    if (password_verify($txtPassword, $hash)) {
                                            $_Response[2] = $_Row;
                                    } else {
                                            $_Response[2] = 0;    
                                    }
                                }
                            else
                                {
                                    $_Response[0] ='NORECORDINNEWTABLE';// No Record found in new table
                                }
                            
                            
                       // }
                }
            
            
            
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    } 
    
    /**
         * @function addLoginAttempt
         * @param $CenterCode  
         * @return mixed|array : returns array 
         * @Description : This function check the login attempt and if more then 3 attempt found it may block ITGK.
         */
    
    public function addLoginAttempt($_UserName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
				
            $_SelectQuery = "Select * From tbl_loginattempts Where User_LoginId='".$_UserName."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //echo "<pre>";print_r($_Response);die;
            if($_Response[0]=='Success')
            {
                $_Row = mysqli_fetch_array($_Response[2],true);
                //echo "<pre>";print_r($_Row);die;
                $Attempts = $_Row["Attempts"]+1;        

                if($_Row["Attempts"]==3) 
                {
                  //$_UpdateQuery = "UPDATE tbl_user_master SET User_Status=2  WHERE User_LoginId = '".$_UserName."'";
                  //$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                  //$_UpdateQuery2 = "UPDATE tbl_loginattempts SET Attempts=0 WHERE User_LoginId = '".$_UserName."'";
                  //$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
                  return 4; // sent to sequerty question // center has been blocked
                }
                else {
                  $_UpdateQuery = "UPDATE tbl_loginattempts SET Attempts=".$Attempts." WHERE User_LoginId = '".$_UserName."'";
                  $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                   return 3-$Attempts; // return '1 Attempts left.';
                }
            }
            else
            {
                $_InsertQuery = "INSERT INTO tbl_loginattempts (User_LoginId,Attempts) values ('".$_UserName."', 1)";
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                return '2'; // return '2 Attempts left.';
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        //return $_Response;
    }
    
    /**
         * @function addLoginAttempt
         * @param $CenterCode  
         * @return mixed|array : returns array 
         * @Description : This function check the login attempt and if more then 3 attempt found it may block ITGK.
         */
    
    public function clearLoginAttempts($_UserName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
				
            $_UpdateQuery2 = "UPDATE tbl_loginattempts SET Attempts=0 WHERE User_LoginId = '".$_UserName."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
                  
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        //return $_Response;
    }
    
    
    
    
    
    
    
    
    
    public function VerifyOTPNEW($oid, $otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$oid = mysqli_real_escape_string($_ObjConnection->Connect(),$oid);
				$otp = mysqli_real_escape_string($_ObjConnection->Connect(),$otp);
				
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Code='" . $oid . "' AND AO_OTP = '" . $otp . "' AND AO_Status = '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //echo "<pre>"; print_r($_Response);die;
            if($_Response[0]=='Success'){
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d h:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);


                if($datetime1 > $datetime2)
                {
                    if ($_Response[0] != Message::NoRecordFound) {
                        $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Code='" . $oid . "'";
                        $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    }
                }
                else
                {   
                    $_Response[0]='EXPIRE';
                }
            }
            else
                {   
                    $_Response[0]='NORECORD';
                }
            
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
//echo "<pre>"; print_r($_Response);die;
        return $_Response;
    }
    
    public function GetLoginDetailNewForLogin($_UserName) {
        
        global $_ObjConnection, $_Response, $general;
        $_ObjConnection->Connect();
        try { 
				$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
				
            // NEW FILTERED SQL FOR LOGIN
            $_SelectQuery ="Select a.User_Code,a.User_EmailId,a.User_MobileNo,a.User_LoginId,
            a.User_UserRoll,a.User_ParentId,a.User_Rsp,a.User_Status,
            b.UserRoll_Name, 
            c.Organization_Name,c.Organization_District 
            FROM tbl_user_master as a 
            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
            WHERE a.User_LoginId = '" . $_UserName . "' and a.User_Status = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if($_Response[0]=='Success')
                {
                        $_Row = mysqli_fetch_array($_Response[2],true);
                        $_Response[2] = $_Row;
                        
                }
             
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    } 
    
    public function TrackUser($loginid,$userroll,$REMOTE_HOST,$REMOTE_ADDR,$LOCAL_ADDR,$lat,$long) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "insert into tbl_usertrackmaster (Track_Userloginid,Track_Userroll,Track_REMOTE_HOST,Track_REMOTE_ADDR,Track_LOCAL_ADDR,Track_Lat,Track_Long)values('".$loginid."','".$userroll."','".$REMOTE_HOST."','".$REMOTE_ADDR."','".$LOCAL_ADDR."','".$lat."','".$long."')";          
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}
