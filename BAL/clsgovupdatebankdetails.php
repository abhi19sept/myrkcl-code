<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsUpdateBankDetails
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsgovupdatebankdetails {
    //put your code here   
	
	public function GetBankDetails($code)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_LoginRole = $_SESSION['User_UserRoll'];
         if ($_LoginRole == '1' || $_SESSION['User_Code']=='6588') {
				$_SelectQuery = "SELECT Govemp_ITGK_Code,learnercode,fname,faname,gbankname,gbranchname,empaccountno,gifsccode FROM tbl_govempentryform WHERE learnercode = '" . $code . "'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				return $_Response;
		 }
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 
		}	 
		
		public function UpdateBankDetails($lcode, $accnumber, $ifsccode)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_UpdateQuery = "Update tbl_govempentryform set empaccountno='".$accnumber."', gifsccode = '" . $ifsccode . "' Where learnercode = '" . $lcode  . "'";
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				
				
				date_default_timezone_set('Asia/Kolkata');
				$_Date = date("Y-m-d h:i:s");
					 
				$_Insert = "Insert Into tbl_gov_bankdetails_log(Gov_BankDetails_Lcode, Gov_BankDetails_ITGK, Gov_BankDetails_Account_no,
								Gov_BankDetails_Ifsc_Code,Gov_BankDetails_DateTime)
								SELECT learnercode, Govemp_ITGK_Code, '".$accnumber."', '" . $ifsccode . "', '" . $_Date . "'
								FROM tbl_govempentryform where learnercode='" . $lcode . "'";						
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);		
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 return $_Response;
		}	
		
}
