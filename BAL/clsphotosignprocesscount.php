<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsphotosignprocesscount {

    //put your code here

    public function GetAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_LoginUserRole = $_SESSION['User_UserRoll'];
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				
			if($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11'){
			
			 $_SelectQuery =	"select Admission_ITGK_Code, sum(case when Admission_PhotoProcessing_Status = 'Approved' then 1 else 0 end) Approved,
sum(case when Admission_PhotoProcessing_Status = 'Reprocess' then 1 else 0 end)
Reprocess ,
sum(case when Admission_PhotoProcessing_Status = 'Rejected' then 1 else 0 end)
Rejected from tbl_admission WHERE Admission_Course=".$_course." AND Admission_Batch=".$_batch." group by Admission_ITGK_Code";
							  $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else
			{
				//print_r( $_SESSION['User_LoginId']);
				  $_SelectQuery =	"select Admission_ITGK_Code, sum(case when Admission_PhotoProcessing_Status = 'Approved' then 1 else 0 end) Approved,
sum(case when Admission_PhotoProcessing_Status = 'Reprocess' then 1 else 0 end)
Reprocess ,
sum(case when Admission_PhotoProcessing_Status = 'Rejected' then 1 else 0 end)
Rejected from tbl_admission WHERE Admission_Course=".$_course." AND Admission_Batch=".$_batch." AND Admission_ITGK_Code =" . $_SESSION['User_LoginId'] . " ";
 $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}				
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    
   

}
