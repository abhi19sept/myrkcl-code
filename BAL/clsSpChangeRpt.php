<?php

/**
 * Description of clsSpChangeRpt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsSpChangeRpt {

//put your code here


    public function GetDatafirst() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11') {
                // $_SelectQuery3 = "select Rspitgk_ItgkCode,Rspitgk_Rspcode,Rspitgk_Rspname,Rspitgk_Date,Rspitgk_RspApproval_Date,Rspitgk_End_Date,
                //                                Rspitgk_Status,b.ITGK_Name,b.District_Name from tbl_rspitgk_mapping as a inner join vw_itgkname_distict_rsp as b on 
                //                                a.Rspitgk_ItgkCode=b.ITGKCODE where rspitgk_Status ='Approved' order by b.District_Name,Rspitgk_ItgkCode";
                //$_SelectQuery3 ="select a.*,b.ITGK_Name,b.District_Name from tbl_rspitgk_mapping as a left join vw_itgkname_distict_rsp as b on 
                //      a.Rspitgk_ItgkCode=b.ITGKCODE where rspitgk_Status ='Approved' and Rspitgk_UserType in ('New')
                //     order by Rspitgk_ItgkCode";
                $_SelectQuery3 = "select a.*,b.ITGK_Name,b.District_Name,b.ITGKMOBILE,b.ITGKEMAIL from tbl_rspitgk_mapping as a 
                                                left join vw_itgkname_distict_rsp as b on a.Rspitgk_ItgkCode=b.ITGKCODE 
                                                where Rspitgk_ItgkCode in (select Rspitgk_ItgkCode from tbl_rspitgk_mapping  group by 
                                                Rspitgk_ItgkCode having count(Rspitgk_ItgkCode)>1) order by Rspitgk_ItgkCode,Rspitgk_RspApproval_Date";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDataSP() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
            if ($_LoginUserRole == '14') {
                $_SelectQuery3 = "select Rspitgk_ItgkCode,Rspitgk_Rspcode,Rspitgk_Rspname,Rspitgk_Date,Rspitgk_RspApproval_Date,Rspitgk_End_Date,
                                                Rspitgk_Status,b.ITGK_Name,b.District_Name from tbl_rspitgk_mapping as a inner join vw_itgkname_distict_rsp as b on 
                                                a.Rspitgk_ItgkCode=b.ITGKCODE where rspitgk_Status ='Approved' and Rspitgk_Rspcode='" . $_SESSION['User_Code'] . "' 
                                                order by b.District_Name, Rspitgk_ItgkCode";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
