<?php

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsMenuManagement {

    public function GetRootMenu() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select root_menu_code,root_menu_name,root_menu_status From tbl_root_menu Order By root_menu_display";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetParentMenu($_RootMenu) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootMenu = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootMenu);
				
            $_SelectQuery = "Select Parent_Function_Code,Parent_Function_Name,Parent_Function_Status From tbl_parent_function_master Where Parent_Function_Root='" . $_RootMenu . "' Order By Parent_Function_Display";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetChildMenu($_Parent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent);
				
            $_SelectQuery = "Select Function_Code,Function_Name,Function_URL,Function_Status From tbl_function_master where Function_Parent='" . $_Parent . "' Order by Function_Display";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Status_Code,Status_Name from tbl_status_master";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillRootForMenu() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select root_menu_code,root_menu_name from tbl_root_menu";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillParent($root) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$root = mysqli_real_escape_string($_ObjConnection->Connect(),$root);
			
            $_SelectQuery = "Select Parent_Function_Code,Parent_Function_Name from tbl_parent_function_master where Parent_Function_Code='" . $root . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDisplayOrder() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select MAX(root_menu_display) as displayorder from tbl_root_menu";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDisplayOrderParent($_Root) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Root = mysqli_real_escape_string($_ObjConnection->Connect(),$_Root);
				
            $_SelectQuery = "select MAX(Parent_Function_Display) as displayorder from tbl_parent_function_master where Parent_Function_Root='" . $_Root . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDisplayOrderChild($_Parent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent);
				
            $_SelectQuery = "select MAX(Function_Display) as displayorder from tbl_function_master where
							Function_Parent='" . $_Parent . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddRootMenu($_RootName, $_RootStatus, $_RootDisplay) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootName);
				$_RootStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootStatus);
				$_RootDisplay = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootDisplay);
				
            $_DuplicateQuery = "Select * From tbl_root_menu Where Root_Menu_Name='" . $_RootName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_InsertQuery = "Insert Into tbl_root_menu(Root_Menu_Name," . "Root_Menu_Status, Root_Menu_Display) "
                        . "Select '" . $_RootName . "' as Root_Menu_Name,'" . $_RootStatus . "' as Root_Menu_Status,'" . $_RootDisplay . "' as Root_Menu_Display";
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddParentMenu($_RootCode, $_ParentName, $_DisplayOrder, $_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootCode);
				$_ParentName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentName);
				$_DisplayOrder = mysqli_real_escape_string($_ObjConnection->Connect(),$_DisplayOrder);
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				
            $_DuplicateQuery = "Select * From tbl_parent_function_master Where Parent_Function_Name='" . $_ParentName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_InsertQuery = "Insert Into tbl_parent_function_master(Parent_Function_Code,Parent_Function_Name,"
                        . "Parent_Function_Status,Parent_Function_Display,Parent_Function_Root) "
                        . "Select Case When Max(Parent_Function_Code) Is Null Then 1 Else Max(Parent_Function_Code)+1 End as Parent_Function_Code,"
                        . "'" . $_ParentName . "' as Parent_Function_Name,'" . $_Status . "' as Parent_Function_Status,"
                        . "'" . $_DisplayOrder . "' as Parent_Function_Display,'" . $_RootCode . "' as Parent_Function_Root "
                        . " From tbl_parent_function_master";
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddChildMenu($_ChildName, $_ParentCode, $_FunctionURL, $_DisplayOrder, $_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ChildName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ChildName);
				$_ParentCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentCode);
				$_DisplayOrder = mysqli_real_escape_string($_ObjConnection->Connect(),$_DisplayOrder);
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				
            $_DuplicateQuery = "Select * From tbl_function_master Where Function_Name='" . $_ChildName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_InsertQuery = "Insert Into tbl_function_master(Function_Code,Function_Name,Function_URL,"
                        . "Function_Parent,Function_Status,Function_Display) "
                        . "Select Case When Max(Function_Code) Is Null Then 1 Else Max(Function_Code)+1 End as Function_Code,"
                        . "'" . $_ChildName . "' as Function_Name,'" . $_FunctionURL . "' as Function_URL,"
                        . "'" . $_ParentCode . "' as Function_Parent,'" . $_Status . "' as Function_Status,'" . $_DisplayOrder . "' as Function_Display"
                        . " From tbl_function_master";
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRoot($deleteid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$deleteid = mysqli_real_escape_string($_ObjConnection->Connect(),$deleteid);
				
            $_DeleteQuery = "Delete From tbl_root_menu Where root_menu_code='" . $deleteid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteParent($deleteid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$deleteid = mysqli_real_escape_string($_ObjConnection->Connect(),$deleteid);
				
            $_DeleteQuery = "Delete From tbl_parent_function_master Where Parent_Function_Code='" . $deleteid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteChild($deleteid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$deleteid = mysqli_real_escape_string($_ObjConnection->Connect(),$deleteid);
				
            $_DeleteQuery = "Delete From tbl_function_master Where Function_Code='" . $deleteid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillRootEditData($editid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$editid = mysqli_real_escape_string($_ObjConnection->Connect(),$editid);
				
            $_SelectQuery = "select * from tbl_root_menu where root_menu_code='" . $editid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillParentEditData($editid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$editid = mysqli_real_escape_string($_ObjConnection->Connect(),$editid);
				
            $_SelectQuery = "select * from tbl_parent_function_master where Parent_Function_Code='" . $editid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillChildEditData($editid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$editid = mysqli_real_escape_string($_ObjConnection->Connect(),$editid);
				
            $_SelectQuery = "select * from tbl_function_master where Function_Code='" . $editid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateRootMenu($_RootName, $_RootStatus, $_RootDisplay, $_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_UpdateQuery = "Update tbl_root_menu set root_menu_name='" . $_RootName . "',root_menu_status='" . $_RootStatus . "',root_menu_display='" . $_RootDisplay . "' where root_menu_code='" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateParentMenu($_RootCode, $_ParentName, $_DisplayOrder, $_Status, $_ParentCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ParentCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentCode);
            $_UpdateQuery = "Update tbl_parent_function_master set Parent_Function_Name='" . $_ParentName . "',Parent_Function_Display='" . $_DisplayOrder . "',Parent_Function_Status='" . $_Status . "',Parent_Function_Root='" . $_RootCode . "' where Parent_Function_Code='" . $_ParentCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateChildMenu($_ChildName, $_ParentCode, $_FunctionURL, $_DisplayOrder, $_Status, $_ChildCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ChildCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ChildCode);
            $_UpdateQuery = "Update tbl_function_master set Function_Name='" . $_ChildName . "',Function_URL='" . $_FunctionURL . "',Function_Parent='" . $_ParentCode . "',Function_Display='" . $_DisplayOrder . "',Function_Status='" . $_Status . "' where Function_Code='" . $_ChildCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}

?>