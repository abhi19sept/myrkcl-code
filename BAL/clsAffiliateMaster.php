<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAffiliateMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAffiliateMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Affiliate_Code,Affiliate_Name,Affiliate_Status from tbl_affiliate_master ";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Affiliate_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Affiliate_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Affiliate_Code);
            $_SelectQuery = "Select Affiliate_Code,Affiliate_Name,"
                    . "Affiliate_Status From tbl_affiliate_master Where Affiliate_Code='" . $_Affiliate_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Affiliate_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Affiliate_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Affiliate_Code);
            $_DeleteQuery = "Delete From tbl_affiliate_master Where Affiliate_Code='" . $_Affiliate_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_AffiliateName,$_AffiliateStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_AffiliateName = mysqli_real_escape_string($_ObjConnection->Connect(),$_AffiliateName);
            $_AffiliateStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_AffiliateStatus);
            $_InsertQuery = "Insert Into tbl_affiliate_master(Affiliate_Code,Affiliate_Name,"
                    . "Affiliate_Status) "
                    . "Select Case When Max(Affiliate_Code) Is Null Then 1 Else Max(Affiliate_Code)+1 End as Affiliate_Code,"
                    . "'" . $_AffiliateName . "' as Affiliate_Name,'" . $_AffiliateStatus . "' as Affiliate_Status"
                    . " From tbl_affiliate_master";
            $_DuplicateQuery = "Select * From tbl_affiliate_master Where Affiliate_Name='" . $_AffiliateName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_AffiliateCode,$_AffiliateName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_AffiliateCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AffiliateCode);
            $_AffiliateName = mysqli_real_escape_string($_ObjConnection->Connect(),$_AffiliateName);
            $_FunctionStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_FunctionStatus);
            $_UpdateQuery = "Update tbl_affiliate_master set Affiliate_Name='" . $_AffiliateName . "',"
                    . "Affiliate_Status='" . $_FunctionStatus . "' Where Affiliate_Code='" . $_AffiliateCode . "'";
            $_DuplicateQuery = "Select * From tbl_affiliate_master Where Affiliate_Name='" . $_AffiliateName . "' "
                    . "and Affiliate_Code <> '" . $_AffiliateCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
