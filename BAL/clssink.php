<?php

/**
 * Description of clsUpdExamResult
 *
 * @author yogendra soni
 */
ini_set("memory_limit", "5000M");
ini_set('max_execution_time', 0); 
set_time_limit(0);
ini_set("upload_max_filesize", "100000M");
ini_set("post_max_size", "10000M");
ini_set("max_execution_time", "30000000000");
ini_set("max_input_time", "30000000000");
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clssink {

    //put your code here
    //print_r($_SESSION['DataArrayExamResult']);
    public function Addfinalexamresult($rows) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
           //die(); 
            $_InsertQuery = "INSERT INTO tbl_uploaded_scores (Admission_Code, Admission_LearnerCode, Score, Score_Per, 
			Admission_ITGK_Code,Fileid
			) VALUES " . $rows;
			//die();
			 
			
		    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	public function syncscorestomaintable() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_learner_score (Admission_Code, Learner_Code, Score, Score_Per, ITGK_code) (SELECT us.Admission_Code, us.Admission_LearnerCode, us.Score, us.Score_Per, us.Admission_ITGK_Code FROM tbl_uploaded_scores AS us WHERE us.synced = 0 AND us.Score > 0) ON DUPLICATE KEY UPDATE tbl_learner_score.Score = IF(tbl_learner_score.Score > us.Score, tbl_learner_score.Score, us.Score), tbl_learner_score.Score_Per = IF(tbl_learner_score.Score_Per > us.Score_Per, tbl_learner_score.Score_Per, us.Score_Per)";
		    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			if ($_Response[0] == 'Successfully Inserted') {
				$_UpdateQuery = "UPDATE tbl_uploaded_scores SET synced = 1 WHERE synced = 0";
				$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			}
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	 public function updatescoreid($_eventid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$_eventid);
				
			$_UpdateQuery = "Update tbl_upload_file set Status='1' Where  id='".$_eventid."'";
            
		    $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	 public function GetSink() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select * from tbl_upload_file order by Filename";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
public function searchxlsFiles($fileName = '*.csv' )
	{
		global $_ObjConnection;
        $_ObjConnection->Connect();
		try
		{
			
			$path = '/upload/Score/';
			$filePath = $path . $fileName;
			$files = [];
			foreach (glob($filePath) as $file) 
			{
				if (!is_dir($file)) {
					$files[] = $file;
					
				
				}
				
			}
			
			$temp = count($files);
			for($i=0;$i<$temp;$i++)
			{
			$path=pathinfo($files[$i]);
			//print_r($temp);
			$_InsertQuery1 = "INSERT INTO tbl_upload_file ( Filename,Status
					) VALUES ('" . $path['basename']. "','0')";
		
			$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
			//print_r($files);
			}	
			
			
		} 
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $files;
    }
	

}
