<?php

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsDigitalCertificateStatus {

    public function GetCourseCertificate() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','4','3','20')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllBatchCertificate($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Code,Batch_Name From tbl_batch_master where Course_Code ='" . $_CourseCode . "'  and 
							 Batch_Code>111 order by Batch_Code DESC ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateCertificateStatus($course, $batch, $learnercode, $statusAddmission, $statusResult) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$learnercode = mysqli_real_escape_string($_ObjConnection->Connect(),$learnercode);
				
                $_UpdateQuery = "update tbl_final_result set status='".$statusResult."' where scol_no ='".$learnercode."'";
                $_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

                $_UpdateQuery1 = "Update tbl_admission set Admission_Aadhar_Status='".$statusAddmission."'
                            Where Admission_Course='".$course."' and Admission_Batch='".$batch."' AND Admission_LearnerCode='".$learnercode."'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::OnUpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}