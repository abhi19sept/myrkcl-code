<?php

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsLotMaster {
    
    public function AddGovRole($_LotName,$_Sdate,$_Status,$_Role,$_Year) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_User_Code = $_SESSION['User_Code'];
            $_InsertQuery = "Insert Into tbl_clot(lotid,lotname,year,active,date,govrole,correctionrole) 
					Select Case When Max(lotid) Is Null Then 1 Else Max(lotid)+1 End as lotid,'" . $_LotName . "' as lotname, '" . $_Year . "' as Year,
					'" . $_Status . "' as active, '" . $_Sdate . "' as date, 'Yes' as govrole, 'No' as correctionrole From tbl_clot";
            
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	public function AddCorrectionRole($_LotName,$_Sdate,$_Status,$_Role,$_Year) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_User_Code = $_SESSION['User_Code'];
			$_LotName = mysqli_real_escape_string($_ObjConnection->Connect(),$_LotName);
			$_Sdate = mysqli_real_escape_string($_ObjConnection->Connect(),$_Sdate);
			$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
			$_Role = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role);
			$_Year = mysqli_real_escape_string($_ObjConnection->Connect(),$_Year);
			
            $_InsertQuery = "Insert Into tbl_clot(lotid,lotname,year,active,date,govrole,correctionrole) 
					Select Case When Max(lotid) Is Null Then 1 Else Max(lotid)+1 End as lotid,'" . $_LotName . "' as lotname, '" . $_Year . "' as Year,
					'" . $_Status . "' as active, '" . $_Sdate . "' as date, 'No' as govrole, 'Yes' as correctionrole From tbl_clot";
            
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

   

}

?>