<?php

/*
 * @author Abhi

 */

/**
 * Description of clsMunicipalMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsMunicipalMaster {
    //put your code here
    public function GetAll($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$district = mysqli_real_escape_string($_ObjConnection->Connect(),$district);
			
            if(!$district)
            {

            $_SelectQuery = "Select Municipality_Code, Municipality_Name, District_Name, Region_Name, State_Name, Country_Name, Status_Name From tbl_municipality_master as a 
inner join tbl_district_master as b on a.Municipality_District=b.District_Code 
inner join tbl_region_master as c on b.District_Region=c.Region_Code 
inner join tbl_state_master as d on c.Region_State=d.State_Code 
inner join tbl_country_master as f on d.State_Country=f.Country_Code 
inner join tbl_status_master as e on a.Municipality_Status=e.Status_Code ORDER BY State_Name, Region_Name, District_Name, Municipality_Name";
            }
            else {
                 $_SelectQuery = "Select Municipality_Code,Municipality_Name,District_Name,Region_Name,State_Name,"
                    . "Status_Name,Country_Name From tbl_municipality_master as a inner join tbl_district_master as b "
                    . "on a.Municipality_District=b.District_Code inner join tbl_region_master as c "
                    . "on b.District_Region=c.Region_Code inner join tbl_state_master as d "
                    . "on c.Region_State=d.State_Code inner join tbl_status_master as e "
                    . "on a.Municipality_Status=e.Status_Code inner join tbl_country_master as f "
                    . "on d.State_Country=f.Country_Code and a.Municipality_District='" . $district . "' ORDER BY State_Name, Region_Name, District_Name, Municipality_Name";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Municipality_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Municipality_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Municipality_Code);
			
            $_SelectQuery = "Select Municipality_Code,Municipality_Name,Municipality_District,Municipality_Status"
                    . " From tbl_municipality_master Where Municipality_Code='" . $_Municipality_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Municipality_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Municipality_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Municipality_Code);
			
            $_DeleteQuery = "Delete From tbl_municipality_master Where Municipality_Code='" . $_Municipality_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_MunicipalName,$_District,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_MunicipalName = mysqli_real_escape_string($_ObjConnection->Connect(),$_MunicipalName);
			$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
			$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
			
            $_InsertQuery = "Insert Into tbl_municipality_master(Municipality_Code,Municipality_Name,"
                    . "Municipality_District,Municipality_Status) "
                    . "Select Case When Max(Municipality_Code) Is Null Then 1 Else Max(Municipality_Code)+1 End as Municipality_Code,"
                    . "'" . $_MunicipalName . "' as Municipality_Name,"
                    . "'" . $_District . "' as Municipality_District,'" . $_Status . "' as Municipality_Status"
                    . " From tbl_municipality_master";
            $_DuplicateQuery = "Select * From tbl_municipality_master Where Municipality_Name='" . $_MunicipalName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_MunicipalName,$_District,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			$_MunicipalName = mysqli_real_escape_string($_ObjConnection->Connect(),$_MunicipalName);
			$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
			$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
			
            $_UpdateQuery = "Update tbl_municipality_master set Municipality_Name='" . $_MunicipalName . "',"
                    . "Municipality_District='" . $_District . "',"
                    . "Municipality_Status='" . $_Status . "' Where Municipality_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_municipality_master Where Municipality_Name='" . $_MunicipalName . "' "
                    . "and Municipality_Code <> '" . $_Code . "'";
           
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
