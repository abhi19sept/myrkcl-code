<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPrintRecpRHLS
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response2 = array();

class clsPrintRecpRHLS {

    //put your code here
    //put your code here
    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

			$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				
            $_SelectQuery2 = " Select a.*, b.* from tbl_redhat_expcenter_transaction as a inner join tbl_redhat_exp_center as b "
                    . " on a.ExpCenter_Transaction_Txtid=b.exp_center_txnid WHERE "
                    . "a.ExpCenter_Transaction_timestamp >= '" . $_StartDate . "' AND a.ExpCenter_Transaction_timestamp <= '" . $_EndDate . "' AND "
                    . "a.ExpCenter_Transaction_CenterCode ='" . $_SESSION['User_LoginId'] . "' and a.ExpCenter_Transaction_Status = 'Success'";


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

    public function GetAllPrintRecpDwnld($cid) { /// for new invoice system GST
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$cid = mysqli_real_escape_string($_ObjConnection->Connect(),$cid);
				
            $_SelectQuery = " Select a.*, b.*,c.*,d.*, e.ITGKMOBILE, e.ITGK_Name,e.District_Name from "
                    . " tbl_redhat_expcenter_transaction as a inner join tbl_redhat_exp_center as b "
                    . " on a.ExpCenter_Transaction_Txtid=b.exp_center_txnid inner join tbl_redhat_expcenter_invoice as c on "
                    . "b.exp_center_id =c.expcenter_eoi_code inner join tbl_gst_invoice_master as d on d.Gst_Invoce_Category_Id = '18' "
                    . " inner join vw_itgkname_distict_rsp as e on e. ITGKCODE = b.exp_center_code WHERE "
                    . "a.ExpCenter_Transaction_CenterCode ='" . $_SESSION['User_LoginId'] . "' and a.ExpCenter_Transaction_Status = 'Success' "
                    . "and d.Gst_Invoce_Category_Type='EOI_RedHat_Subs' and c.expcenter_eoi_code = '" . $cid . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function addPhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
       
            $Imagepath = '../upload/RedHatPrintRecpt/redhatlogo.png';
      

        $pdf->Image($Imagepath, 8, 105, 50, 25);
        $pdf->Image($Imagepath, 8, 251, 50, 25);

        $pdf->Output($path, "F");
    }

    public function convert_number_to_words($number) {

        $hyphen = ' ';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'Zero',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Fourty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100 => 'Hundred',
            1000 => 'Thousand',
            1000000 => 'Million',
            1000000000 => 'Billion',
            1000000000000 => 'Trillion',
            1000000000000000 => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

}
