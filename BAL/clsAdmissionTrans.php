<?php

/**
 * Description of clsAdmissionTrans
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsAdmissionTrans {

    //put your code here

    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
            $_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
            
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {

                 $_SelectQuery2 = "Select a.*, b.* From tbl_admission as a INNER JOIN tbl_admission_transaction as b ON a.Admission_TranRefNo = b.Admission_Transaction_Txtid WHERE b.Admission_Transaction_timestamp >= '" . $_StartDate . "' AND b.Admission_Transaction_timestamp <= '" . $_EndDate . "' AND a.Admission_Payment_Status = '1'";
            } else {

                $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission_transaction AS b ON a.Centercode = b.Admission_Transaction_CenterCode WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response1[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";
                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");
                if ($CenterCode3) {
                     $_SelectQuery2 = "Select a.*, b.* From tbl_admission as a INNER JOIN tbl_admission_transaction as b ON a.Admission_TranRefNo = b.Admission_Transaction_Txtid WHERE b.Admission_Transaction_timestamp >= '" . $_StartDate . "' AND b.Admission_Transaction_timestamp <= '" . $_EndDate . "' AND a.Admission_Payment_Status = '1' AND b.Admission_Transaction_CenterCode IN ($CenterCode3)";
                }
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

    public function ShowSummary($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
            $_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
            
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            // $_SESSION['UserType'] = $_LoginUserType;

            if ($_loginflag == "1") {

                 $_SelectQuery2 = "Select b.Admission_Transaction_ProdInfo, b.Admission_Transaction_CenterCode, b.Admission_Transaction_Fname, b.Admission_Transaction_Status, b.Admission_Transaction_Txtid, b.Admission_Transaction_timestamp, count(a.Admission_LearnerCode) as lcount, b.Admission_Transaction_Amount as totalamount From tbl_admission as a INNER JOIN tbl_admission_transaction as b ON a.Admission_TranRefNo = b.Admission_Transaction_Txtid WHERE b.Admission_Transaction_timestamp >= '" . $_StartDate . "' AND b.Admission_Transaction_timestamp <= '" . $_EndDate . "' AND a.Admission_Payment_Status = '1' GROUP BY b.Admission_Transaction_Txtid";
            } else {

                $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission_transaction AS b ON a.Centercode = b.Admission_Transaction_CenterCode WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response1[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";
                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");

                if ($CenterCode3) {
                    $_SelectQuery2 = "Select b.Admission_Transaction_ProdInfo, b.Admission_Transaction_CenterCode, b.Admission_Transaction_Fname, b.Admission_Transaction_Status, b.Admission_Transaction_Txtid, b.Admission_Transaction_timestamp, count(a.Admission_LearnerCode) as lcount, b.Admission_Transaction_Amount as totalamount From tbl_admission as a INNER JOIN tbl_admission_transaction as b ON a.Admission_TranRefNo = b.Admission_Transaction_Txtid WHERE b.Admission_Transaction_timestamp >= '" . $_StartDate . "' AND b.Admission_Transaction_timestamp <= '" . $_EndDate . "' AND a.Admission_Payment_Status = '1' AND b.Admission_Transaction_CenterCode IN ($CenterCode3) GROUP BY b.Admission_Transaction_Txtid";
                    //echo $_SelectQuery;
                }
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

}
