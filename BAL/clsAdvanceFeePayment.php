<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsAdvanceFeePayment
 *
 *  author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdvanceFeePayment extends paymentFunctions {

    //put your code here
	public function GetAdvanceCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN 
			tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND 
			`CourseITGK_BlockStatus` = 'unblock' AND `Courseitgk_ITGK` = '" . $_SESSION['User_LoginId'] . "' AND CURDATE() >= 
			`CourseITGK_UserCreatedDate` AND CURDATE() <= `CourseITGK_ExpireDate` AND Course_Type='6'
			 ORDER BY `CourseITGK_UserCreatedDate` ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAdvanceCourseName($_CategoryCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CategoryCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CategoryCode);
            $_SelectQuery = "Select Ad_Course_Id,Ad_Course_Name from tbl_advancecourse_master where
							Ad_Course_Category='". $_CategoryCode . "' and Ad_Course_Status='Active'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	public function GetAdvanceCourseCategory($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectQuery = "Select Ad_Category_Code,Ad_Category_Name from tbl_advancecourse_category where
							Ad_Category_Course='". $_CourseCode . "' and Ad_Category_Status='Active'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }	
	
    public function GetAll($batch, $course, $paymode, $coursepkg) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {    
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$paymode);
            $coursepkg = mysqli_real_escape_string($_ObjConnection->Connect(),$coursepkg);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK = $_SESSION['User_LoginId'];
                $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_Course = '" . $course . "' AND 
				Event_Batch = '" . $batch . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '3' AND NOW() >= Event_Startdate 
				AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
                if ($_getEvent['Event_Payment'] == '1') {
                    $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_Batch = '" . $batch . "' AND
									Admission_Course = '" . $course . "' And Admission_ITGK_Code='" . $_ITGK . "' And 
									Admission_Advance_CourseCode='".$coursepkg."' AND Admission_Payment_Status='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_getEvent['Event_Payment'] == '2') {
                    $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_Batch = '" . $batch . "' AND
									Admission_Course = '" . $course . "' And Admission_ITGK_Code='" . $_ITGK . "' And 
									Admission_Advance_CourseCode='".$coursepkg."' AND Admission_Payment_Status='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    //echo "Invalid User Input";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select RKCL_Share, Course_Fee, VMOU_Share FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }



    public function AddPayTran($postValues, $productinfo) {
        $return = 0;
        try {
            $postValues = mysqli_real_escape_string($_ObjConnection->Connect(),$postValues);
            $productinfo = mysqli_real_escape_string($_ObjConnection->Connect(),$productinfo);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $postValues['amounts'], 
							$postValues['AdmissionCodes'], $postValues['ddlCourse'], 0, $postValues['ddlBatch'], $postValues['ddlPkg']);
            } else {
                session_destroy();
            ?>
                <script> window.location.href = "logout.php";</script> 
            <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }
	
	public function GetDatabyTrnxid($trnxId) {
        return parent::GetPaymentDataByTrnxid($trnxId); 
    }

	public function updateTransactionStatus($trnxId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $trnxId = mysqli_real_escape_string($_ObjConnection->Connect(),$trnxId);
            $_UpdateQuery = "UPDATE tbl_payment_transaction SET Pay_Tran_Status = 'PaymentInProcess' WHERE
							Pay_Tran_PG_Trnid = '" . $trnxId . "'";
            
             $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;

        }

        return $_Response;
    }
	
	public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}