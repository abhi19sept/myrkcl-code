<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBankAccount
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsmodifyorgdetails {
    //put your code here
    
	
	public function SHOWDATA($_activate) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11'){
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
				$_SelectQuery2 = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_org_type_master as c on a.Organization_Type=c.Org_Type_Code inner join tbl_state_master as d on a.Organization_State=d.State_Code  inner join tbl_district_master as f on a.Organization_District=f.District_Code inner join tbl_tehsil_master as g on a.Organization_Tehsil=g.Tehsil_Code inner join tbl_country_master as h on a.Organization_Country=h.Country_Code  inner join tbl_courseitgk_mapping as i
				    on b.user_loginid=i.Courseitgk_ITGK where User_LoginId=" . $_activate . " and 
					Courseitgk_Course='RS-CIT' and b.User_UserRoll='7'";
               }
			 $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	
	
   public function UPDATE($_code,$_OrgName,$_OrgType,$_OrgRegno,$_State,$_Region,$_District,$_Tehsil,$_Landmark,$_Road,$_Street,$_HouseNo,$_OrgEstDate,$_OrgCountry,$_address) 
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
		  $_UpdateQuery = "Update tbl_organization_detail set 
						Organization_Name='". $_OrgName . "',
						Organization_RegistrationNo='" . $_OrgRegno . "',
						Organization_FoundedDate='" . $_OrgEstDate . "',
						Organization_Type='" . $_OrgType . "',
						Organization_State='" . $_State . "',
						Organization_Region='" . $_Region . "',
						Organization_District='" . $_District . "',
						Organization_Tehsil='" . $_Tehsil . "',
						
						Organization_Landmark='" . $_Landmark . "',
						Organization_Road='" . $_Road . "',
						Organization_Street='" . $_Street . "',
						Organization_HouseNo='" . $_HouseNo . "',
						Organization_Country='" . $_OrgCountry . "',
						Organization_Address='" . $_address . "',
						IsNewRecord='Y'
						Where Organization_Code='" . $_code. "'";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	public function DeleteRecord($_actionvalue)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			
            $_DeleteQuery = "Delete From tbl_organization_detail Where Organization_Code='" . $_actionvalue . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	public function GetDatabyCode($_Org_Code)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Org_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Org_Code);
			
           $_SelectQuery = "Select *  From tbl_organization_detail Where Organization_Code='" . $_Org_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	

}
