<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsCorrectionUpdateRejectedLearner
 *
 *  author Mayank
 */

//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();
$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionUpdateRejectedLearner {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
				$_ITGK_Code = $_SESSION['User_LoginId'];
				
					if($_SESSION['User_UserRoll'] == '7'){
						$field="Correction_ITGK_Code = '" . $_ITGK_Code . "' ";
					}
					else if($_SESSION['User_UserRoll'] == '19'){
						$_Learner_Code = $_SESSION['User_LearnerCode'];
						$field = "lcode = '" . $_Learner_Code . "' ";
					}
				 $_SelectQuery = "Select * FROM tbl_correction_copy WHERE $field
								AND dispatchstatus='4' AND Correction_Payment_Status='1' order by cid DESC"; 
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	public function UpdateLearnerProcessStatus($_Cid, $_Reason, $_Delv_Date) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			date_default_timezone_set("Asia/Kolkata");
			$processdate = date("Y-m-d H:i:s");
			$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
			
           $_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='14', remarks='" . $_Reason . "',
							form_delivered_date='".$_Delv_Date."' Where cid='" . $_Cid . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);			
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	 public function GetExistingLearnerDetails($_Cid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				
            $_SelectQuery = "Select * FROM tbl_correction_copy WHERE cid = '" . $_Cid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	 public function update($_Cid, $_LearnerCode, $_LearnerName, $_FatherName, $_Mobile, $dtstamp){
		  global $_ObjConnection;
		  global $_ObjFTPConnection;
        $_ObjConnection->Connect();
        try {
				$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				
				$cphoto	= $_LearnerCode .'_photo_'.$dtstamp.'.jpg';
				$_ITGK_Code 	=   $_SESSION['User_LoginId'];
				$_User_Code 	=   $_SESSION['User_Code'];
				
				
				$_UpdateQuery = "Update tbl_correction_copy set cfname='".$_LearnerName."', cfaname='".$_FatherName."',
								photo='".$cphoto."', dispatchstatus='3' where cid='".$_Cid."'";
				
					$ftpaddress = $_ObjFTPConnection->ftpPathIp();			
					$photodoc = $ftpaddress.'correction_photo/' . '/' . $cphoto;
					
					$ch = 	curl_init($photodoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
						
					//$photodoc = '../upload/correction_photo/'.$cphoto;
					if ($responseCodePhoto == 200) {					
						$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Certificate Name Correction/Duplicate Certificate is Successfully Updated.";
						//$_Mobile=$_MobileNo;
						SendSMS($_Mobile, $_SMS);
				   }
					else {
						echo "Please Re-Upload Documents Again.";
						return;
					} 
					
		}
		catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
	 }
	
}