<?php


/**
 * Description of clsQualificationMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsQualificationMaster {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Qualification_Code,Qualification_Name,"
                    . "Status_Name From tbl_qualification_master as a inner join tbl_status_master as b "
                    . "on a.Qualification_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Qualification_Code)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Qualification_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Qualification_Code);
				
            $_SelectQuery = "Select Qualification_Code,Qualification_Name,Qualification_Status"
                    . " From tbl_qualification_master Where Qualification_Code='" . $_Qualification_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Qualification_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Qualification_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Qualification_Code);
				
            $_DeleteQuery = "Delete From tbl_qualification_master Where Qualification_Code='" . $_Qualification_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_QualificationName,$_QualificationStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_QualificationName = mysqli_real_escape_string($_ObjConnection->Connect(),$_QualificationName);
				$_QualificationStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_QualificationStatus);
				
            $_InsertQuery = "Insert Into tbl_qualification_master(Qualification_Code,Qualification_Name,"
                    . "Qualification_Status) "
                    . "Select Case When Max(Qualification_Code) Is Null Then 1 Else Max(Qualification_Code)+1 End as Qualification_Code,"
                    . "'" . $_QualificationName . "' as Qualification_Name,"
                    . "'" . $_QualificationStatus . "' as Qualification_Status"
                    . " From tbl_qualification_master";
            $_DuplicateQuery = "Select * From tbl_qualification_master Where Qualification_Name='" . $_QualificationName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_QualificationCode,$_QualificationName,$_QualificationStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_QualificationCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_QualificationCode);
				$_QualificationName = mysqli_real_escape_string($_ObjConnection->Connect(),$_QualificationName);
				
            $_UpdateQuery = "Update tbl_qualification_master set Qualification_Name='" . $_QualificationName . "',"
                    . "Qualification_Status='" . $_QualificationStatus . "' Where Qualification_Code='" . $_QualificationCode . "'";
            $_DuplicateQuery = "Select * From tbl_qualification_master Where Qualification_Name='" . $_QualificationName . "' "
                    . "and Qualification_Code <> '" . $_QualificationCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
