<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cls pop up Master
 *
 * @author Yogendra soni
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clspolicy {
    //put your code here
    
     public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_policy";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
          //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
   
    
   
   
    public function Add($_Msg) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "Insert Into tbl_policy (Message) VALUES ('". $_Msg ."')";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_policy Where Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	 public function DeleteRecord($_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $query = "SELECT * FROM tbl_policy_acceptedby WHERE policyid = '" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            if (!mysqli_num_rows($_Response[2])) {
                $_DeleteQuery = "Delete From tbl_policy Where id='" . $_Code . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            } else {
                $query = "UPDATE tbl_policy SET status = 0 WHERE id = '" . $_Code . "'";
                $_Response=$_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }

    public function GetRecord() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
            $query = "SELECT * FROM tbl_policy WHERE Status = 1 ORDER BY id DESC LIMIT 1";
            $_Response=$_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

        try {
        }  catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }

    public function CheckIfAccepted($policyId, $userCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
			$policyId = mysqli_real_escape_string($_ObjConnection->Connect(),$policyId);
			$userCode = mysqli_real_escape_string($_ObjConnection->Connect(),$userCode);
			
            $query = "SELECT * FROM tbl_policy_acceptedby WHERE policyid = '" . $policyId . "' AND usercode = '" . $userCode . "'
						LIMIT 1";
            $_Response=$_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

        try {
        }  catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function acceptPolicy($id, $userCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
			$id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
			$userCode = mysqli_real_escape_string($_ObjConnection->Connect(),$userCode);
			
        $response = $this->getFixedActivePopupContent($userCode);
        if (mysqli_num_rows($response[2])) {
            $data = mysqli_fetch_array($response[2]);
            $this->setFixedPopUpContentInactive($data['id'], $userCode);
        }
        $query = "INSERT INTO tbl_policy_acceptedby SET 
            usercode = '" . $userCode . "',
            policyid = '" . $id . "'";
        $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
    }

    public function setFixedPopUpContentInactive($id, $userCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $query = "INSERT INTO tbl_fixed_popup_viewedby SET 
            usercode = '" . $userCode . "',
            popupid = '" . $id . "'";
        $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
    }
    
    public function getFixedActivePopupContent($userCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userRole = (isset($_SESSION['User_UserRoll'])) ? $_SESSION['User_UserRoll'] : '';
        try {
        mysqli_set_charset('utf8');
        $condition = (!empty($userCode)) ? " AND (fp.usercode='" . $userCode . "' OR fp.usercode = 0)" : '';
        $query = "SELECT fp.* FROM tbl_fixed_popup_content fp WHERE fp.status = 1 AND fp.Role = '" . $userRole . "' $condition AND fp.id NOT IN (SELECT popupid FROM tbl_fixed_popup_viewedby WHERE usercode='" . $userCode . "' AND status = 1) ORDER BY fp.id DESC LIMIT 1";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        }  catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

}
