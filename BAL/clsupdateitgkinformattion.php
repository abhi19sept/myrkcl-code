<?php


/**
 * Description of clsHRDetail
 *
 * @author yogendra
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
//require 'DAL/sendemail.php';
include('DAL/smtp_class.php');
$_ObjConnection = new _Connection();
$_Response = array();

class clsupdateitgkinfromattion {
    
	 
      public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {

            $_SelectQuery = "Select * from tbl_user_master ";
            
            } 
			else
			{
                
            $_SelectQuery = "Select * from tbl_user_master where User_LoginId = '" .$_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
  
	public function GetDatabyCode($_code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
				
            $_SelectQuery = "Select * from tbl_user_master Where User_Code= '" . $_code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
  
	
	public function updateemail($_Code,$_email)
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_email = mysqli_real_escape_string($_ObjConnection->Connect(),$_email);
			
			 $_SelectQuery = "Select * from tbl_user_master where User_LoginId ='" .$_SESSION['User_LoginId'] . "'";
			 
			 $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			 
			 
			 
			 if($_Response[0]='Success')
		     {
				 $_Row = mysqli_fetch_array($_Response[2]);
				 $_oldemail= $_Row['User_EmailId'];
				 $_oldmobile= $_Row['User_MobileNo'];
				 $_InsertQuery = "Insert Into tbl_user_master_log(User_Code,User_EmailId,User_MobileNo,
				centercode) 
				VALUES ('". $_Code ."','". $_oldemail ."','".$_oldmobile."','". $_SESSION['User_LoginId'] ."')";
				
				$_DuplicateQuery = "Select * From tbl_user_master_log Where centercode='".$_SESSION['User_LoginId']."' and
					User_Code='". $_Code ."'";
				$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
				if($_Response[0]==Message::NoRecordFound)
				{
					$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				}
				else {
					$_Response[0] = Message::DuplicateRecord;
					$_Response[1] = Message::Error;
				}
					
				$_UpdateQuery = "Update tbl_user_master set User_EmailId='" . $_email . "' ,IsNewRecord='Y',IsNewCRecord='Y',IsNewSRecord='Y',IsNewOnlineLMSRecord='Y'
			     Where User_Code='" . $_Code . "' AND User_LoginId = '" .$_SESSION['User_LoginId'] . "' ";
				 
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				
				
				
				if($_Response[0]='Successfully Updated')
				{
					
			     $_InsertQuery1 = "Insert Into tbl_user_master_log(User_Code,User_EmailId,User_MobileNo,
				centercode) 
				VALUES ('". $_Code ."','". $_email ."','Not Changed','". $_SESSION['User_LoginId'] ."')";
					
				$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
				
				
				 $_UpdateQuery1 = "Update tbl_userprofile set UserProfile_Email='" . $_email ."',IsNewRecord='Y'
                    Where UserProfile_User='" . $_Code . "' ";
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
				//echo 'yogendra';
				return $_Response;
				}
				else
				{
					echo "Not Updated Successsfully";
				}
			 }
			 else
			 {
				 echo "Not Updated Successsfully";
			 }
			   //echo $_Response[0];
				
                
                //print_r($_Response);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	
	
	
	public function updatemobile($_Code,$_mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_mobile);
				
			  $_SelectQuery = "Select * from tbl_user_master where User_LoginId ='" .$_SESSION['User_LoginId'] . "'";
			 $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			 
			 
			
			 if($_Response[0]='Success')
		     {
				 $_Row = mysqli_fetch_array($_Response[2]);
				 $_oldmobile = $_Row['User_MobileNo'];
				 $_oldemail = $_Row['User_EmailId'];
			 	 $_InsertQuery = "Insert Into tbl_user_master_log(User_Code,User_EmailId,User_MobileNo,
				centercode) 
				VALUES ('". $_Code ."','".$_oldemail."','". $_oldmobile ."','". $_SESSION['User_LoginId'] ."')";
				
				
				$_DuplicateQuery = "Select * From tbl_user_master_log Where centercode='".$_SESSION['User_LoginId']."' and User_Code='". $_Code ."'";
				$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);	
				if($_Response[0]==Message::NoRecordFound)
				{
					$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				}
				else {
					$_Response[0] = Message::DuplicateRecord;
					$_Response[1] = Message::Error;
				}
				 
				 
				$_UpdateQuery = "Update tbl_user_master set User_MobileNo='" . $_mobile . "',IsNewRecord='Y',IsNewCRecord='Y',IsNewSRecord='Y',IsNewOnlineLMSRecord='Y' Where User_Code='" . $_Code . "' AND User_LoginId = '" .$_SESSION['User_LoginId'] . "' ";
				 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				 
				 
				if($_Response[0]='Successfully Updated')
				{
					
					
			     $_InsertQuery1 = "Insert Into tbl_user_master_log(User_Code,User_EmailId,User_MobileNo,
				centercode) 
				VALUES ('". $_Code ."','Not Changed','". $_mobile ."','". $_SESSION['User_LoginId'] ."')";
					
				$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
				 
				 $_UpdateQuery1 = "Update tbl_userprofile set UserProfile_Mobile='" . $_mobile ."',IsNewRecord='Y'
                    Where UserProfile_User='" . $_Code . "'  ";
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
				//echo 'yogendra';
				return $_Response;
				}
				else
				{
					echo "Not Updated Successsfully";
				}
			 
			 }
			 else
			 {
				 echo "Not Updated Successsfully";
			 }
			 
				
				//echo $_Response[0];
				
                
                //print_r($_Response);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	 public function SendOTP() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        //$_SMS = "OTP for Modifiying User Details on MYRKCL is " . $_OTP;
        $_SMS = "Modifiying User Details OTP is '".$_OTP."' in myrkcl. Rajasthan Knowledge Corporation Limited";
		
		


        try {
            $_UserM = $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_LoginId='" . $_UserM . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_Mobile = $_Row1['User_MobileNo'];
			 $_Email= $_Row1['User_EmailId'];
           


            $_InsertQuery = "Insert Into tbl_choice_otp_register(Choice_Code,Choice_Mobile,Choice_OTP)"
                    . "Select Case When Max(Choice_Code) Is Null Then 1 Else Max(Choice_Code)+1 End as Choice_Code,"
                    . "'" .  $_Mobile . "' as Choice_Mobile,'"
                    . "" . $_OTP . " as Choice_OTP '"
                    . " From tbl_choice_otp_register";
            //echo $_InsertQuery;
             $_DuplicateQuery = "Select * From tbl_choice_otp_register Where Choice_Mobile='" . $_Mobile . "' AND Choice_OTP = '" . $_OTP . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
			   $Admin_Name='RKCL';
			   	$mail_data = array();
			$mail_data['email'] = $_Email;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_SMS;
			$subject = "OTP for Modifiying User Details on MYRKCL is";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
				//sendMail("".$_Email."", "".$Admin_Name."", $_SMS, $_SMS, "RKCL Support");
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function Verify($_Otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_Otp);
				
            $_SelectQuery = "Select * FROM tbl_choice_otp_register WHERE  Choice_OTP = '" . $_Otp . "' AND Choice_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid Verification Details. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_choice_otp_register set Choice_Status='1' WHERE  Choice_OTP = '" . $_Otp . "' AND Choice_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
             // $_DeleteQuery = "Delete From tbl_choice_otp_register WHERE  Choice_Status = '0'";
               // $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	public function SendfinalOTPmobile($_mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = "OTP for Verification of Authentication is " . $_OTP;
		
		


        try {
            //$_UserM = $_SESSION['User_LoginId'];
			$_mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_mobile);
			
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_MobileNo='".$_mobile."'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			if ($_Response1[0] == "Success")
			{
				
				return;
			}
			else
			{
			
            $_Mobile = $_mobile;
			$_InsertQuery = "Insert Into tbl_choice_otp_register(Choice_Code,Choice_Mobile,Choice_OTP)"
                    . "Select Case When Max(Choice_Code) Is Null Then 1 Else Max(Choice_Code)+1 End as Choice_Code,"
                    . "'" .  $_Mobile . "' as Choice_Mobile,'"
                    . "" . $_OTP . " as Choice_OTP '"
                    . " From tbl_choice_otp_register";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_choice_otp_register Where Choice_Mobile='" . $_Mobile . "' AND Choice_OTP = '" . $_OTP . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
				if ($_Response[0] == Message::NoRecordFound) 
				{
					$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					SendSMS($_Mobile, $_SMS);
					//$Admin_Name='RKCL';
					//sendMail("".$_Email."", "".$Admin_Name."", $_SMS, $_SMS, "RKCL Support");
				} 
				else 
				{
					$_Response[0] = Message::DuplicateRecord;
					$_Response[1] = Message::Error;
				}
			}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	public function SendfinalOTP($_newemail) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = "OTP for Verification of Authentication is " . $_OTP;
		
		


        try {
            $_UserM = $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE  User_EmailId='".$_newemail."'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			if ($_Response1[0] == "Success")
			{
				//echo "Email Id Alrady Exist";
				
				return;
			}
			else
			{	
				$_SelectQuery2 = "SELECT * FROM tbl_user_master WHERE  User_LoginId='".$_UserM."'";
               $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
				$_Row1 = mysqli_fetch_array($_Response2[2]);
				$_Mobile = $_Row1['User_MobileNo'];
				$_Email= $_newemail;
				$_InsertQuery = "Insert Into tbl_choice_otp_register(Choice_Code,Choice_Mobile,Choice_OTP)"
                    . "Select Case When Max(Choice_Code) Is Null Then 1 Else Max(Choice_Code)+1 End as Choice_Code,"
                    . "'" .  $_Mobile . "' as Choice_Mobile,'"
                    . "" . $_OTP . " as Choice_OTP '"
                    . " From tbl_choice_otp_register";
            //echo $_InsertQuery;
				$_DuplicateQuery = "Select * From tbl_choice_otp_register Where Choice_Mobile='" . $_Mobile . "' AND Choice_OTP = '" . $_OTP . "'";
				$_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
				if ($_Response[0] == Message::NoRecordFound) {
					$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				   // SendSMS($_Mobile, $_SMS);
				$mail_data = array();
				$Admin_Name='RKCL';
				$mail_data['email'] = $_newemail;
				$mail_data['admin'] = $Admin_Name;
							
				$mail_data['Msg'] = $_SMS;						
				$subject = "OTP for Modifiying User Details on MYRKCL is";						
				$sendMail = new sendMail();
				$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
				
				}
				else 
				{
					$_Response[0] = Message::DuplicateRecord;
					$_Response[1] = Message::Error;
				}
				
				
				
			}
            
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
