<?php

/**
 * Description of clswcdrexamapplicationpaymentreport
 *
 * @author Yogi
 */
require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clswcdrexamapplicationpaymentreport {

    //put your code here
	
	public function GetWcdrexamapplicationCourse() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master WHERE Course_Code IN (5,24)";
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	

	public function FILLWcdrexamapplicationBatch($_Coursecode) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_SelectQuery = "select Batch_Code,Batch_Name from tbl_batch_master where Course_Code='".$_Coursecode."'";
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }	
	
	

    public function GetApplication($_course, $startdate, $enddate) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            //$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
			{
				if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 8 || 
					$_SESSION['User_UserRoll'] == 4) {
                
                $_SelectQuery2 = "select rscfa_reexam_lcode,rscfa_reexam_lname,rscfa_reexam_fname,a.itgkcode,
									rscfa_reexam_paydate,District_Name,RSP_Name,Batch_Name 
									from tbl_rscfa_reexam_application as a inner join vw_itgkname_distict_rsp as b
									on a.itgkcode=b.ITGKCODE inner join tbl_batch_master as c on a.batchname=c.Batch_Code
									where coursename='" . $_course . "' and
									rscfa_reexam_paydate >= '" . $startdate . "' AND rscfa_reexam_paydate <= '". $enddate ."'
									and rscfa_reexam_paymentstatus='1'";    
		
            } 
			elseif ($_SESSION['User_UserRoll'] == 14) 
			{
				
				$_SelectQuery2 = "select rscfa_reexam_lcode,rscfa_reexam_lname,rscfa_reexam_fname,a.itgkcode,
									rscfa_reexam_paydate,District_Name,RSP_Name,Batch_Name
									from tbl_rscfa_reexam_application as a inner join vw_itgkname_distict_rsp as b
									on a.itgkcode=b.ITGKCODE inner join tbl_batch_master as c on a.batchname=c.Batch_Code
									where coursename='" . $_course . "' and
									rscfa_reexam_paydate >= '" . $startdate . "' AND rscfa_reexam_paydate <= '". $enddate ."'
									AND rscfa_reexam_paymentstatus='1' and RSP_Code='".$_SESSION['User_LoginId']."'";    

                
            } 
			elseif ($_SESSION['User_UserRoll'] == 7) 
			{
                
                //$CenterCode3 = $_SESSION['User_LoginId']; 
                $_SelectQuery2 = "select rscfa_reexam_lcode,rscfa_reexam_lname,rscfa_reexam_fname,a.itgkcode,
									rscfa_reexam_paydate,District_Name,RSP_Name,Batch_Name 
									from tbl_rscfa_reexam_application as a inner join vw_itgkname_distict_rsp as b
									on a.itgkcode=b.ITGKCODE inner join tbl_batch_master as c on a.batchname=c.Batch_Code
									where coursename='" . $_course . "' and
									rscfa_reexam_paydate >= '" . $startdate . "' AND rscfa_reexam_paydate <= '". $enddate ."'
									AND rscfa_reexam_paymentstatus='1' and a.itgkcode='".$_SESSION['User_LoginId']."'";    
		
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                     
            //print_r($_Response);
            } 
			else 
			{
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } 
		catch (Exception $_ex) 
		{

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	public function GetCertificate($_course, $startdate, $enddate) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            //$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
			{
				if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 8 || 
					$_SESSION['User_UserRoll'] == 4) {
                
                 $_SelectQuery2 = "Select apply_rscfa_cert_lcode,apply_rscfa_cert_itgk,apply_rscfa_cert_lname,apply_rscfa_cert_fname,
									District_Name,RSP_Name, apply_rscfa_cert_payment_date, Batch_Name, apply_rscfa_cert_email
									from tbl_apply_rscfa_certificate as a inner join
									vw_itgkname_distict_rsp as b on a.apply_rscfa_cert_itgk=b.ITGKCODE
									inner join tbl_batch_master as c on a.apply_rscfa_cert_batch=c.Batch_Code
									where apply_rscfa_cert_course='" . $_course . "' and
								apply_rscfa_cert_payment_date >='" . $startdate . "' and apply_rscfa_cert_payment_date <= '". $enddate ."'
									and apply_rscfa_cert_payment_status='1'";    
		
            } 
			elseif ($_SESSION['User_UserRoll'] == 14) 
			{
				$_Rsp = $_SESSION['User_Code'];
			    $_SelectQuery2 = "Select apply_rscfa_cert_lcode,apply_rscfa_cert_itgk,apply_rscfa_cert_lname,apply_rscfa_cert_fname,
								District_Name,RSP_Name, apply_rscfa_cert_payment_date, Batch_Name, apply_rscfa_cert_email
								from tbl_apply_rscfa_certificate as a inner join
								vw_itgkname_distict_rsp as b on a.apply_rscfa_cert_itgk=b.ITGKCODE
								inner join tbl_batch_master as c on a.apply_rscfa_cert_batch=c.Batch_Code
								where apply_rscfa_cert_course='" . $_course . "' and
								apply_rscfa_cert_payment_date >='" . $startdate . "' and apply_rscfa_cert_payment_date <= '". $enddate ."'
								and apply_rscfa_cert_payment_status='1' and apply_rscfa_cert_rsp_code='".$_SESSION['User_Code']."'";    

                
            } 
			elseif ($_SESSION['User_UserRoll'] == 7) 
			{
                
                $CenterCode3 = $_SESSION['User_LoginId']; 
                $_SelectQuery2 = "Select apply_rscfa_cert_lcode,apply_rscfa_cert_itgk,apply_rscfa_cert_lname,apply_rscfa_cert_fname,
								District_Name,RSP_Name, apply_rscfa_cert_payment_date, Batch_Name, apply_rscfa_cert_email
								from tbl_apply_rscfa_certificate as a inner join
								vw_itgkname_distict_rsp as b on a.apply_rscfa_cert_itgk=b.ITGKCODE
								inner join tbl_batch_master as c on a.apply_rscfa_cert_batch=c.Batch_Code
								where apply_rscfa_cert_course='" . $_course . "' and
								apply_rscfa_cert_payment_date >='" . $startdate . "' and apply_rscfa_cert_payment_date <= '". $enddate ."'
								and apply_rscfa_cert_payment_status='1' and a.apply_rscfa_cert_itgk='".$_SESSION['User_LoginId']."'";    
		
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                     
            //print_r($_Response);
            } 
			else 
			{
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } 
		catch (Exception $_ex) 
		{

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
