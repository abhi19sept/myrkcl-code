<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsLogin {

    public function GetLoginDetail($_UserName, $_Password) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
             //$_SelectQuery = "Select * From tbl_user_master where User_LoginId='" . $_UserName . "' and User_Password='" . $_Password . "'";		
			$_SelectQuery = "Select a.*, b.*, c.* FROM tbl_user_master as a INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code INNER JOIN tbl_organization_detail
							as c on a.User_Code=c.Organization_User WHERE a. User_LoginId = '" . $_UserName . "' AND a. User_Password = '" . $_Password . "'";
             $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetRootMenuByUserRole($_UserRole)
    {
        global $_ObjConnection,$_Response;
        $_ObjConnection->Connect();
        try
        {
            $_SelectQuery="Select * From vw_userrolewiserootmenu Where UserRole='" . $_UserRole . "' Order By DisplayOrder";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery,  Message::SelectStatement);
            
        }
        catch(Exception $_ex)
        {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetParentMenuByUserRole($_UserRole,$_RootMenu)
    {
        global $_ObjConnection,$_Response;
        $_ObjConnection->Connect();
        try
        {
            $_SelectQuery="Select * From vw_userrolewiseparentmenu Where UserRole='" . $_UserRole . "' and RootMenu='" . $_RootMenu . "' Order By DisplayOrder";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery,  Message::SelectStatement);
            
        }
        catch(Exception $_ex)
        {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetChildMenuUserRole($_UserRole,$_Parent)
    {
        global $_ObjConnection,$_Response;
        $_ObjConnection->Connect();
        try
        {
            
            $_SelectQuery="Select * From vw_userrolewisefunction where "
                    . "UserRole='" . $_UserRole . "' and Parent='" . $_Parent . "' Order by Display";
           
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery,  Message::SelectStatement);
        }
        catch(Exception $_ex)
        {
            $_Response[0]=$_ex->getTraceAsString();
            $_Response[1]=  Message::Error;
        }
        return $_Response;
    }
	
	
	public function Get1() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select id,value,news,link From tb1_news_master";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
	 public function SaveSession($User_LoginId, $User_UserRoll, $User_ParentId, $User_Status, $User_Code, $User_EmailId, $UserRoll_Name, $Organization_Name) {
        
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $minutes = 120;
            $_DeleteQuery = "delete FROM tbl_user_session WHERE User_Session_LoginId='".$User_LoginId."' AND User_Session_Timestamp < DATE_SUB(NOW(),INTERVAL $minutes MINUTE)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);

            $_InsertQuery = "Insert Into tbl_user_session(User_Session_Code,User_Session_LoginId,User_Session_UserRoll,"
                    . "User_Session_ParentId,User_Session_Status,User_Session_UserCode,User_Session_Email,"
                    . "User_Session_UserName,User_Session_OrgName) "
                    . "Select Case When Max(User_Session_Code) Is Null Then 1 Else Max(User_Session_Code)+1 End as User_Session_Code,"
                    . "'" . $User_LoginId . "' as User_Session_LoginId,'" . $User_UserRoll . "' as User_Session_UserRoll,'" . $User_ParentId . "' as User_Session_ParentId,"
                    . "'" . $User_Status . "' as User_Session_Status,'" . $User_Code . "' as User_Session_UserCode,'" . $User_EmailId . "' as User_Session_Email,"
                    . "'" . $UserRoll_Name . "' as User_Session_UserName,'" . $Organization_Name . "' as User_Session_OrgName"
                    . " From tbl_user_session";
            
            $_DuplicateQuery = "Select * From tbl_user_session Where User_Session_LoginId='" . $User_LoginId . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
