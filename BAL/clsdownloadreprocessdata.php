<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

  ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsdownloadreprocessdata {
    //put your code here  
	
	
	public function ShowDetailsToDownload() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select * FROM `tbl_admission` where Admission_PhotoProcessing_Status='Reprocess'";
			}
			else{
				$_SelectQuery = "Select * FROM `tbl_admission` where Admission_PhotoProcessing_Status='Reprocess' AND Admission_ITGK_Code= '" . $_SESSION['User_LoginId'] ."'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetLearnerPhoto() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select * FROM `tbl_admission` where Admission_PhotoProcessing_Status='Reprocess'";
			}
			else{
				$_SelectQuery = "Select * FROM `tbl_admission` where Admission_PhotoProcessing_Status='Reprocess' AND Admission_ITGK_Code= '" . $_SESSION['User_LoginId'] ."'";
			}			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
 
}
