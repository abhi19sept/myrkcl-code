<?php

/**
 * Description of clsUpdatelearnerbatch
 *
 * @author Abhishek
 */
//put your code here
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUpdatelearnerbatch {
    /* Function for Load course */

    public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','4','5')";
            // $_SelectQuery = "Select distinct a.Courseitgk_Course as Course_Name, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' ORDER BY Course_Code ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /* Function for Load Previous batch */

    public function GetPrevBatch($CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        if ($CourseCode == '1' || $CourseCode == '4' || $CourseCode == '5') {

            try {
					$CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CourseCode);
					
                $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $CourseCode . "'
							order by Batch_Code DESC LIMIT 1 , 1";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
        } else {
            $_Response = "Invalid Entry";
        }
        return $_Response;
    }

    /* Function for Load Current batch */

    public function GetCurBatch($CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        if ($CourseCode == '1' || $CourseCode == '4' || $CourseCode == '5') {

            try {
					$CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CourseCode);
					
                $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $CourseCode . "'
							order by Batch_Code DESC LIMIT 1";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
        } else {
            $_Response = "Invalid Entry";
        }
        return $_Response;
    }

    /* Function for Load Previous batch Learners */

    public function GetPrevBatchLearners($BatchCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$BatchCode = mysqli_real_escape_string($_ObjConnection->Connect(),$BatchCode);
					
                $_SelectQuery = "SELECT a.*, b.Batch_Name,c.Course_Name  FROM tbl_admission as a inner join tbl_batch_master as b on "
                        . "a.Admission_Batch= b.Batch_Code inner join tbl_course_master as c on a.Admission_Course=c.Course_Code "
                        . "WHERE Admission_Batch = '" . $BatchCode . "' and "
                        . "Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' and Admission_Payment_Status='0' order by Batch_Code DESC";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /* Function for Update Previous batch Learner to current batch */

    public function UpdateBatch($_AdmissionArray, $CourseCode, $BatchCode) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CourseCode);
				$BatchCode = mysqli_real_escape_string($_ObjConnection->Connect(),$BatchCode);
				
				
                $_UpdateQuery = "update tbl_admission set Admission_Batch='" . $BatchCode . "', Admission_Date='" . $_Date . "' "
                        . "where Admission_Code in (" . $_AdmissionArray . ") "
                        . "and Admission_Course= '" . $CourseCode . "' and Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "' "
                        . "and Admission_Payment_Status='0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_InsertQuery = "Insert Into tbl_adm_batch_migrate_log select a.*,'" . $_Date . "' from tbl_admission as a where "
                        . "Admission_Code in (" . $_AdmissionArray . ") "
                        . "and a.Admission_Course= '" . $CourseCode . "' and a.Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "' "
                        . "and a.Admission_Payment_Status='0'";
                $_Responseinsert = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
