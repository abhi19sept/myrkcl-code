<?php
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsnews {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select value,inclusions From tbl_inclusions";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	public function Get() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select id,value,news,link From tb1_news_master";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
	
	
	
    
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_SelectQuery = "Select id,value,news,link From tb1_news_master Where id='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function DeleteRecord($_values)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_values = mysqli_real_escape_string($_ObjConnection->Connect(),$_values);
				
           $_DeleteQuery = "Delete From tb1_news_master Where id='" . $_values . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Add($_icons,$_News,$_Link) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
            $_InsertQuery = "Insert Into tb1_news_master(value, news,link) VALUES ('" . $_icons . "', '" . $_News . "','" . $_Link . "')";
            $_DuplicateQuery = "Select * From tb1_news_master Where value='" . $_News . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_InsertQuery1 = "Insert Into tbl_news_master_app(value, news,link) VALUES ('" . $_icons . "', '" . base64_encode($_News) . "','" . $_Link . "')";
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_code,$_icons, $_News,$_Link) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
				
            $_UpdateQuery = "Update tb1_news_master set 
						value='" . $_icons . "'
						news='" . $_News . "'
						link='" . $_Link . "'
                     Where id='" . $_code . "'";
            $_DuplicateQuery = "Select * From tb1_news_master Where id='" . $_code . "' and "
                    . "id <> '" . $_code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
    

}

?>