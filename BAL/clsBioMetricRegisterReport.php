<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionTrans
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsBioMetricRegisterReport {

    //put your code here

    public function ShowRegistered($_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 28){
					$_SelectQuery = "Select a.*,c.Organization_Name,d.District_Name FROM `tbl_biomatric_register` as a INNER JOIN tbl_courseitgk_mapping as e on
										a.BioMatric_ITGK_Code = e.Courseitgk_ITGK INNER JOIN tbl_user_master as b on a.BioMatric_ITGK_Code = b.User_LoginId
										INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User INNER JOIN tbl_district_master as d on 
										c.Organization_District = d.District_Code where BioMatric_Status = 'Success' AND Courseitgk_Course='RS-CIT'";
				}
				else if ($_SESSION['User_UserRoll'] == 14){
					$_SelectQuery = "Select a.*,c.Organization_Name,d.District_Name FROM `tbl_biomatric_register` as a INNER JOIN tbl_courseitgk_mapping as e on
										a.BioMatric_ITGK_Code = e.Courseitgk_ITGK INNER JOIN tbl_user_master as b on a.BioMatric_ITGK_Code = b.User_LoginId
										INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User INNER JOIN tbl_district_master as d on 
										c.Organization_District = d.District_Code where BioMatric_Status = 'Success' AND Courseitgk_Course='RS-CIT'
										AND User_Rsp='".$_SESSION['User_Code']."' ";
					}
                                        else if ($_SESSION['User_UserRoll'] == 23){
					$_SelectQuery = "Select a.*,c.Organization_Name,d.District_Name FROM `tbl_biomatric_register` as a INNER JOIN tbl_courseitgk_mapping as e on
										a.BioMatric_ITGK_Code = e.Courseitgk_ITGK INNER JOIN tbl_user_master as b on a.BioMatric_ITGK_Code = b.User_LoginId
										INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User INNER JOIN tbl_district_master as d on 
										c.Organization_District = d.District_Code where BioMatric_Status = 'Success' AND Courseitgk_Course='RS-CIT'
										AND c.Organization_District='".$_SESSION['Organization_District']."' order by a.Timestamp DESC limit 1";
					}
				else{
					$_SelectQuery = "Select a.*,c.Organization_Name,d.District_Name FROM `tbl_biomatric_register` as a INNER JOIN tbl_courseitgk_mapping as e on
									   a.BioMatric_ITGK_Code = e.Courseitgk_ITGK INNER JOIN tbl_user_master as b on a.BioMatric_ITGK_Code = b.User_LoginId
									INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User INNER JOIN tbl_district_master as d on 
									c.Organization_District = d.District_Code where BioMatric_Status = 'Success' AND Courseitgk_Course='RS-CIT'
									AND BioMatric_ITGK_Code = '".$_SESSION['User_LoginId']."'";
				}				
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function ShowNotRegistered($_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 4
				|| $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 28){
						

					$_SelectQuery = "SELECT tbl_courseitgk_mapping.Courseitgk_ITGK,tbl_biomatric_register.*,tbl_organization_detail.Organization_Name,tbl_district_master.District_Name
									FROM tbl_courseitgk_mapping LEFT JOIN tbl_biomatric_register ON tbl_courseitgk_mapping.Courseitgk_ITGK = tbl_biomatric_register.BioMatric_ITGK_Code
									INNER JOIN tbl_user_master ON tbl_courseitgk_mapping.Courseitgk_ITGK = tbl_user_master.User_LoginId
									INNER JOIN tbl_organization_detail ON tbl_user_master.User_Code=tbl_organization_detail.Organization_User
									INNER JOIN tbl_district_master ON tbl_organization_detail.Organization_District=tbl_district_master.District_Code
									WHERE tbl_courseitgk_mapping.Courseitgk_Course='RS-CIT' AND tbl_biomatric_register.BioMatric_ITGK_Code IS NULL";
				}
				else{
					$_SelectQuery = "SELECT tbl_courseitgk_mapping.Courseitgk_ITGK,tbl_biomatric_register.*,tbl_organization_detail.Organization_Name,tbl_district_master.District_Name
									FROM tbl_courseitgk_mapping LEFT JOIN tbl_biomatric_register ON tbl_courseitgk_mapping.Courseitgk_ITGK = tbl_biomatric_register.BioMatric_ITGK_Code
									INNER JOIN tbl_user_master ON tbl_courseitgk_mapping.Courseitgk_ITGK = tbl_user_master.User_LoginId
									INNER JOIN tbl_organization_detail ON tbl_user_master.User_Code=tbl_organization_detail.Organization_User
									INNER JOIN tbl_district_master ON tbl_organization_detail.Organization_District=tbl_district_master.District_Code
									WHERE tbl_courseitgk_mapping.Courseitgk_Course='RS-CIT' AND tbl_biomatric_register.BioMatric_ITGK_Code IS NULL AND
									BioMatric_ITGK_Code = '".$_SESSION['User_LoginId']."'";
				}				
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
