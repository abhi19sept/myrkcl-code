<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgFinalApproval
 *
 * @author VIVEK
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsReleaseIntake {

    public function Allocate($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master";
            $_SelectQuery = "SELECT Batch_Name, Batch_Code FROM tbl_batch_master where Course_Code = '" . $course . "' ORDER BY Batch_Code DESC LIMIT 1";
            //$_SelectQuery = "Select LAST(Batch_Code, Batch_Name) from tbl_batch_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Release($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $course . "' and Batch_Code != '" . $batch . "' ORDER BY Batch_Code DESC LIMIT 3";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add($_Allocate, $_Release, $_Course) {
        global $_ObjConnection;
        $_Response1 = array();
        $_ObjConnection->Connect();

        try {
				$_Allocate = mysqli_real_escape_string($_ObjConnection->Connect(),$_Allocate);
				
            $_UpdateQuery = "UPDATE tbl_intake_master as t1,(Select Intake_Center,Intake_Consumed 
                                    from tbl_intake_master where Intake_Batch = '" . $_Release . "' and Intake_Course = '" . $_Course . "') as t2
                                    set t1.Intake_Consumed = t1.Intake_Consumed - t2.Intake_Consumed,
                                    t1.Intake_Available = t1.Intake_Available + t2.Intake_Consumed
                                    where t1.Intake_Center =t2.Intake_Center AND t1.Intake_Batch = '" . $_Allocate . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}

?>