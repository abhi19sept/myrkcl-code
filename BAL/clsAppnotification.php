<?php

require 'DAL/classconnectionNEW.php';
include '../android/MyRkcl/push-notification.php';
$_ObjConnection = new _Connection();
$_Response = array();
$_Response1 = array();

class clsAppNotification {

    public function Add($postedArray) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $userArray = explode(",", $postedArray['notificationUsers']);
            foreach ($userArray as $key => $value) {
                $tittle = base64_encode($postedArray['notificationTittle']);
                $message = base64_encode($postedArray['notificationMessage']);
                $type = $postedArray['notificationType'];
                $user = trim($value);
                $_InsertQuery = "insert into tbl_appnotification(Ntittle,Ntype,Nuserid,Nmessage)values('" . $tittle . "','" . $type . "','" . $user . "','" . $message . "')";
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                if($_Response[0]=="Successfully Inserted"){
                    if(strlen($user) >10 && is_numeric($user)){
                        $_SelectQuery = "select User_device_id from tbl_admission where Admission_LearnerCode='".$user."'";
                        $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                        if($_Response1[0]=="Success"){
                            while($_Row = mysqli_fetch_array($_Response1[2])){
                                $args = array(
                                    'device_id'=>$_Row['User_device_id'],
                                    'title'=>$postedArray['notificationTittle'],
                                    'message'=>$postedArray['notificationMessage'],

                                );
                                $resposePush = _push_notification($args); 
                                $resposePush = json_decode($resposePush,true);
                                if(isset($resposePush['success'])){
                                    if($resposePush['success']==1){
                                        $_Response[0] = "SuccessN";
                                    }
                                }
                            }
                        }
                    }else{
                        $user = mysqli_real_escape_string($_ObjConnection->Connect(),$user);
                        $_SelectQuery = "select User_device_id from tbl_user_master where User_LoginId='".$user."'";
                        $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                        if($_Response1[0]=="Success"){
                            while($_Row = mysqli_fetch_array($_Response1[2])){
                                $args = array(
                                    'device_id'=>$_Row['User_device_id'],
                                    'title'=>$postedArray['notificationTittle'],
                                    'message'=>$postedArray['notificationMessage'],

                                );
                                $resposePush = _push_notification($args); 
                                $resposePush = json_decode($resposePush,true);
                                if(isset($resposePush['success'])){
                                    if($resposePush['success']==1){
                                         $_Response[0] = "SuccessN";
                                    }
                                }
                            }
                        }  
                    }
                    }  
                }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllNotificationType() {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Ntype_id,Ntype from tbl_appnotificationtypemaster";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDistrict() {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select District_Code,District_Name from tbl_district_master where District_Name != 'NA' AND District_Name != ''";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetByEntity() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select UserRoll_Code,UserRoll_Name From tbl_userroll_master Where UserRoll_Entity='6' and UserRoll_Status=1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillUserNotification($UserRoll) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $UserRoll = mysqli_real_escape_string($_ObjConnection->Connect(),$UserRoll);
            $_SelectQuery = "select User_LoginId from tbl_user_master where User_UserRoll in ($UserRoll)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function FillUserNotificationForSP($UserSPD) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $UserSPD = mysqli_real_escape_string($_ObjConnection->Connect(),$UserSPD);
             $_SelectQuery = "Select b.User_LoginId,a.Organization_Name from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
            where User_UserRoll='14'  AND Organization_District in ($UserSPD)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FillUserNotificationForITGK($UserTIGK,$course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $UserTIGK = mysqli_real_escape_string($_ObjConnection->Connect(),$UserTIGK);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
              $_SelectQuery = "Select a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
                inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
                where User_UserRoll='7' AND Organization_District in ($UserTIGK) AND Courseitgk_Course='". $course ."' AND CourseITGK_BlockStatus='unblock'
                order by a.Organization_Name";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FillUserNotificationForITGKALL($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
              $_SelectQuery = "Select a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
                inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
                where User_UserRoll='7' AND Courseitgk_Course='". $course ."' AND CourseITGK_BlockStatus='unblock'
                order by a.Organization_Name";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAllUSER() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select User_LoginId from tbl_user_master where User_LoginId != '' order by User_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
  public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ALLBatch($coursecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$coursecode);
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master where Course_Code='".$coursecode."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ALLSelectedLearner($UserTIGK,$course,$batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $UserTIGK = mysqli_real_escape_string($_ObjConnection->Connect(),$UserTIGK);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            
            $_SelectQuery = "select Admission_LearnerCode,Admission_Name from tbl_admission where Admission_Course='".$course."' AND Admission_Batch='".$batch."' AND Admission_ITGK_Code in($UserTIGK)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
