<?php

/**
 * Description of clsShareDistributeRpt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsShareDistributeRpt {

    //put your code here

    public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				
            $_SelectQuery = "select a.Admission_ITGK_Code,count(a.Admission_Code) as confirmcount, b.course_Name, c.Batch_Name, 
            c.Financial_Year,c.Course_Fee,c.RKCL_Share,c.VMOU_Share,c.ITGK_Share,d.Bank_Account_Name,d.Bank_Account_Number,d.Bank_Ifsc_code,f.Organization_Name 
            as RSP_Name from tbl_admission as a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code inner join 
            tbl_batch_master as c on a.Admission_Batch=c.Batch_Code inner join tbl_user_master as e on 
            a.Admission_ITGK_Code=e.User_LoginId inner join tbl_organization_detail as f on e.User_Rsp=f.Organization_User 
            left join tbl_bank_account as d on a.Admission_ITGK_Code=d.Bank_User_Code where a.Admission_Course = '" . $_course . "' AND
            a.Admission_Batch = '" . $_batch . "' and a.Admission_Payment_Status='1' group by a.Admission_ITGK_Code";
            
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
