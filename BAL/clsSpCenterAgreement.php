<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsSpCenterAgreement
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsSpCenterAgreement {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
//            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Org_Ack=e.User_Ack "
                    . " WHERE e.User_PaperLess = 'Yes' "
                    . " AND e.User_Rsp = '" . $_SESSION['User_Code'] . "' AND a.Org_Final_Approval_Status='Approved'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetUploadStatus($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
//            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_DuplicateQuery1 = "Select * From tbl_spcenter_agreement Where SPCenter_CenterCode = '" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, f.SPCenter_Agreement as SP_Agreement FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code LEFT JOIN tbl_spcenter_agreement as f "
                    . " ON e.User_LoginId=f.SPCenter_CenterCode"
                    . " WHERE e.User_LoginId = '" . $_CenterCode . "' AND e.User_UserRoll ='7' "
                    . " AND e.User_PaperLess = 'Yes' AND e.User_Rsp = '" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
    public function Add($_CenterCode, $PANfilename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
                 $_InsertQuery = "Insert Into tbl_spcenter_agreement"
                    . "(SPCenter_LoginId,SPCenter_CenterCode,SPCenter_Agreement,"
                    . " SPCenter_Date) values "                   
                    . "('" . $_SESSION['User_LoginId'] . "','" . $_CenterCode . "','" . $PANfilename . "',"
                    . "'" . $_Date . "')";
            
                $_DuplicateQuery1 = "Select * From tbl_spcenter_agreement Where SPCenter_LoginId='" . $_SESSION['User_LoginId'] . "' "
                        . " AND SPCenter_CenterCode = '" . $_CenterCode . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);

                if ($_Response1[0] == Message::NoRecordFound) {
                 	
                        $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                         
                } else {
                    echo " SP-Center Agreement Copy Already uploaded for this Center.";
                    return;                     
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
