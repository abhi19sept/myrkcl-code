<?php


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsStatusMaster {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Status_Code,Status_Name,Status_Description From tbl_status_master";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_SelectQuery = "Select Status_Code,Status_Name,Status_Description From "
                    . "tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function DeleteRecord($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_DeleteQuery = "Delete From tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Add($_StatusName, $_Status_Description) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StatusName = mysqli_real_escape_string($_ObjConnection->Connect(),$_StatusName);
				
            $_InsertQuery = "Insert Into tbl_status_master(Status_Code,Status_Name,Status_Description) "
                    . "Select Case When Max(Status_Code) Is Null Then 1 Else Max(Status_Code)+1 End as Status_Code,"
                    . "'" . $_StatusName . "' as Status_Name,'" . $_Status_Description . "' as Status_Description "
                    . "From tbl_status_master";
            $_DuplicateQuery = "Select * From tbl_status_master Where Status_Name='" . $_StatusName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Status_Code,$_StatusName, $_Status_Description) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				$_StatusName = mysqli_real_escape_string($_ObjConnection->Connect(),$_StatusName);
				
            $_UpdateQuery = "Update tbl_status_master set Status_Name='" . $_StatusName . "',"
                    . "Status_Description='" . $_Status_Description . "' "
                    . "Where Status_Code='" . $_Status_Code . "'";
            $_DuplicateQuery = "Select * From tbl_status_master Where Status_Name='" . $_StatusName . "' and "
                    . "Status_Code <> '" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
    

}

?>