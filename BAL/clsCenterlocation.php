<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgUpdate
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOrgUpdate {
    //put your code here
    
    public function GetDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                
                $_SelectQuery = "Select a.Organization_Name, a.Organization_District, a.Organization_Tehsil, a.Organization_lat, a.Organization_long, a.Org_formatted_address, b.District_Name, c.Tehsil_Name  FROM tbl_organization_detail  AS a INNER JOIN tbl_district_master AS b ON a.Organization_District=b.District_Code  INNER JOIN tbl_tehsil_master as c ON a.Organization_Tehsil=c.Tehsil_Code  WHERE Organization_User = '" . $_SESSION['User_Code'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	public function FillStates()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_state_master Where State_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	public function FillTehsils($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_tehsil_master Where Tehsil_Status='1' AND Tehsil_District = '".$_District_Code."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	public function MapSave($lat,$long,$org_code, $address)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_organization_detail set Organization_lat='".$lat."',Organization_long = '".$long."', Org_formatted_address = '".$address."'  Where Organization_User='" . $org_code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);          
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	public function MapStart($_Response)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Organization_Name, Organization_lat, Organization_long, Org_formatted_address From tbl_organization_detail Where Org_formatted_address IS NOT NULL AND Organization_District = '".$_Response['ddlDistrict']."'";
			if(isset($_Response['ddlTehsil']) && $_Response['ddlTehsil'] != '' && $_Response['ddlTehsil'] != 0){
				$_SelectQuery .= " AND Organization_Tehsil = '".$_Response['ddlTehsil']."'";
			}
			if(isset($_Response['area']) && $_Response['area'] != '' && $_Response['area'] != 0){
				$_SelectQuery .= " AND Organization_AreaType = '".$_Response['area']."'";
			}
			if(isset($_Response['ddlMunicipalType']) && $_Response['ddlMunicipalType'] != '' && $_Response['ddlMunicipalType'] != 0){
				$_SelectQuery .= " AND Organization_Municipal_Type = '".$_Response['ddlMunicipalType']."'";
			}
			if(isset($_Response['ddlMunicipalName']) && $_Response['ddlMunicipalName'] != '' && $_Response['ddlMunicipalName'] != 0){
				$_SelectQuery .= " AND Organization_Municipal = '".$_Response['ddlMunicipalName']."'";
			}
			if(isset($_Response['ddlWardno']) && $_Response['ddlWardno'] != '' && $_Response['ddlWardno'] != 0){
				$_SelectQuery .= " AND Organization_WardNo = '".$_Response['ddlWardno']."'";
			}
			if(isset($_Response['ddlPanchayat']) && $_Response['ddlPanchayat'] != '' && $_Response['ddlPanchayat'] != 0){
				$_SelectQuery .= " AND Organization_Panchayat = '".$_Response['ddlPanchayat']."'";
			}
			if(isset($_Response['ddlGramPanchayat']) && $_Response['ddlGramPanchayat'] != '' && $_Response['ddlGramPanchayat'] != 0){
				$_SelectQuery .= " AND Organization_Gram = '".$_Response['ddlGramPanchayat']."'";
			}
			if(isset($_Response['ddlVillage']) && $_Response['ddlVillage'] != '' && $_Response['ddlVillage'] != 0){
				$_SelectQuery .= " AND Organization_Village = '".$_Response['ddlVillage']."'";
			}

			//"Org_formatted_address IS NOT NULL AND Organization_Tehsil = '".$_Tehsil_Code."' GROUP BY Org_formatted_address";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	public function Nearestcenter($data)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		$points = explode("--",$data);
		$radius = 500;
		$center_lat = $points[0];
		$center_lng = $points[1];
        try {
			$_SelectQuery = sprintf("SELECT a.Organization_User, a.Organization_Name, a.Organization_Address, b.Org_Mobile, a.Organization_lat, a.Organization_long, a.Org_formatted_address, ( 3959 * acos( cos( radians('%s') ) * cos( radians( a.Organization_lat ) ) * cos( radians( a.Organization_long ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( a.Organization_lat ) ) ) ) AS distance from  tbl_organization_detail_10feb17 as a join tbl_org_master as b on b.Organization_Code = a.Organization_Code join tbl_user_master as c on c.User_Code = a.Organization_User join tbl_courseitgk_mapping as d on d.Courseitgk_ITGK = c.User_LoginId where c.User_UserRoll = 7 AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock' HAVING distance < '%s' ORDER BY distance LIMIT 0 , 3",
			  mysqli_real_escape_string($center_lat),
			  mysqli_real_escape_string($center_lng),
			  mysqli_real_escape_string($center_lat),
			  mysqli_real_escape_string($radius));
			  
			  //echo $_SelectQuery; die;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
        return $_Response;
    }
	
	public function FillDistricts($_State_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_district_master Where District_Status='1' and District_StateCode = '".$_State_Code."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function FillMunicipalType()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_municipality_type Where Municipality_Type_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function FillMunicipalName($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_municipality_master Where Municipality_District='" . $_District_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function FILLWardno($_Municipal_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_ward_master Where 
								Ward_Municipality_Code='" . $_Municipal_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function UpdateOrg($_Areatype, $_WardNo, $_village, $_GramPanch, $_Panchayat, $_MName, $_MType) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		$_User=$_SESSION['User_Code'];
        try {	
				if($_Areatype == 'Urban') {
					if($_MName == '0' || $_MType == '0' || $_WardNo == '0') {
						 echo " Please select all fields";
						 return;
					}
					else {
						$_SelectQuery = "Select * From tbl_organization_detail Where Organization_Status='1' AND Organization_User='" . $_User . "'";
						$_SelectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement); 
						if($_SelectResponse[0]==Message::NoRecordFound) {
							$_UpdateQuery = "Update tbl_organization_detail set Organization_WardNo='" . $_WardNo . "',Organization_Municipal='" . $_MName . "',
											Organization_Municipal_Type='" . $_MType . "', Organization_Gram='',Organization_Panchayat='',
											Organization_Village='', Organization_Status='1' Where Organization_User='" . $_User . "'";
						}
						else {
							echo " Please select all fields";
							return;
						}					
					}
				}
				else {
					if($_GramPanch =='00' || $_Panchayat =='0' || $_village =='0') {
						 echo " You have already submitted your details";
						 return;
					}
					else {
							$_SelectQuery = "Select * From tbl_organization_detail Where Organization_Status='1' AND Organization_User='" . $_User . "'";
							$_SelectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement); 
							if($_SelectResponse[0]==Message::NoRecordFound) {
								$_UpdateQuery = "Update tbl_organization_detail set Organization_Gram='" . $_GramPanch . "',Organization_Panchayat='" . $_Panchayat . "',
											Organization_Village='" . $_village . "', Organization_WardNo='',Organization_Municipal='',
											Organization_Municipal_Type='', Organization_Status='1' Where Organization_User='" . $_User . "'";
							}
							else {
								echo " You have already submitted your details";
								return;
							}	
					}
				}
           
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateOrglocation($_Areatype, $_WardNo, $_village, $_GramPanch, $_Panchayat, $_MName, $_MType, $frm_add, $lat, $long) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		$_User=$_SESSION['User_Code'];
        try {	
				if($_Areatype == 'Urban') {
					if($_MName == '0' || $_MType == '0' || $_WardNo == '0') {
						 echo " Please select all fields";
						 return;
					}
					else {
						$_SelectQuery = "Select * From tbl_organization_detail Where Organization_Status='1' AND Organization_User='" . $_User . "'";
						$_SelectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
						
						if($_SelectResponse[0]==Message::NoRecordFound) {
							$_UpdateQuery = "Update tbl_organization_detail set Organization_WardNo='" . $_WardNo . "',Organization_Municipal='" . $_MName . "',
											Organization_Municipal_Type='" . $_MType . "', Organization_Gram='',Organization_Panchayat='',
											Organization_Village='', Organization_Status='1', Organization_lat= '".$lat."', Organization_long= '".$long."', Org_formatted_address= '".$frm_add."' Where Organization_User='" . $_User . "'";
						}
						else {
							echo " Please select all fields";
							return;
						}					
					}
				}
				else {
					if($_GramPanch =='00' || $_Panchayat =='0' || $_village =='0') {
						 echo " You have already submitted your details";
						 return;
					}
					else {
							$_SelectQuery = "Select * From tbl_organization_detail Where Organization_Status='1' AND Organization_User='" . $_User . "'";
							$_SelectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement); 
							if($_SelectResponse[0]==Message::NoRecordFound) {
								$_UpdateQuery = "Update tbl_organization_detail set Organization_Gram='" . $_GramPanch . "',Organization_Panchayat='" . $_Panchayat . "',Organization_Village='" . $_village . "', Organization_WardNo='',Organization_Municipal='',				Organization_Municipal_Type='', Organization_Status='1', Organization_lat= '".$lat."', Organization_long= '".$long."', Org_formatted_address= '".$frm_add."' Where Organization_User='" . $_User . "'";
							}
							else {
								echo " You have already submitted your details";
								return;
							}	
					}
				}
           
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	public function GetAllPanchayat($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Block_Code,Block_Name"
                    . " From tbl_panchayat_samiti Where Block_District='" . $_District_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
		public function FILLCourse()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','5','21','25',28,29,30,31,32)";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
    public function GetAllGramPanchayat($_Panchayat_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select GP_Code,GP_Name"
                    . " From  tbl_gram_panchayat Where GP_Block='" . $_Panchayat_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function Center_locationdetail() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                
                $_SelectQuery = "Select a.Organization_User, a.Organization_Name, a.Organization_District, a.Organization_Tehsil, a.Organization_lat, a.Organization_long, a.Org_formatted_address, a.Organization_Address, a.Organization_AreaType , b.District_Name, c.Tehsil_Name  FROM tbl_organization_detail  AS a INNER JOIN tbl_district_master AS b ON a.Organization_District=b.District_Code  INNER JOIN tbl_tehsil_master as c ON a.Organization_Tehsil=c.Tehsil_Code  WHERE Organization_User = '" . $_SESSION['User_Code'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				// echo "<pre>";
				// print_r($_Response);
				// echo "</pre>";
				
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	public function Sendsms($mobileno,$_SMS,$filledotp){
		global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_learner_location_otp where AO_Mobile = '".$mobileno."' Order by AO_Code desc limit 1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$_Row = mysqli_fetch_array($_Response[2]);
			// echo "<pre>";
			// print_r($_Row);
			// echo "</pre>";
			if($_Row['AO_OTP'] == $filledotp){
				SendSMS($mobileno, $_SMS);
				$_DeleteQuery = "Delete From tbl_learner_location_otp Where AO_Code='" . $_Row['AO_Code'] . "'";
				$_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
				$response = "success";
			}
			else{
				$response = "Invalid OTP.";
			}
			return $response; 
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
        return $response;
	}
	public function Sendotp($_Mobile){
		global $_ObjConnection;
        $_ObjConnection->Connect();
		function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = "OTP for mobile no verification is " . $_OTP;
		$_InsertQuery = "Insert Into tbl_learner_location_otp(AO_Code,AO_Mobile,AO_OTP) Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,'" . $_Mobile . "' as AO_Mobile,'" . $_OTP . "' as AO_OTP From tbl_learner_location_otp";
                //echo $_InsertQuery;die;            
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
                return $_Response;

	} 
	public function Learnercenterlist($_Response){
		global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
				if($_Response['ddlCourse']=='1')
				{
					$_Course='RS-CIT';
				}
				else if($_Response['ddlCourse']=='5')
				{
					$_Course='RS-CFA';
				}
				else if($_Response['ddlCourse']=='25'){
					$_Course='RS-CEE';
				}
				else if($_Response['ddlCourse']=='28'){
					$_Course='RS-CAE';
				}
				else if($_Response['ddlCourse']=='31'){
					$_Course='RS-CDM';
				}
				else if($_Response['ddlCourse']=='32'){
					$_Course='RS-CDP';
				}
				else if($_Response['ddlCourse']=='29'){
					$_Course='RS-CBC';
				}
				else if($_Response['ddlCourse']=='30')
				{
					$_Course='RS-CCS';
				}
				if(isset($_Response['area']) && $_Response['area'] != ''){
					if($_Response['area']=='Urban'){						
					  $_SelectQuery = "Select Organization_User, Organization_Name, b.User_EmailId, b.User_MobileNo ,
								d.District_Name, e.Tehsil_Name, b.User_LoginId,a.Organization_Address From 
								tbl_organization_detail as a join tbl_user_master as b on a.Organization_User = b.User_Code
								join tbl_courseitgk_mapping as c on c.Courseitgk_ITGK = b.User_LoginId join
								tbl_district_master as d on d.District_Code = a.Organization_District join 
								tbl_tehsil_master as e on e.Tehsil_Code = a.Organization_Tehsil 
								Where b.User_Status = '1' AND b.User_UserRoll = '7' AND Courseitgk_Course='" . $_Course ."'
								AND CourseITGK_BlockStatus='unblock' AND Organization_State = '".$_Response['ddlState']."'
								AND Organization_District = '".$_Response['ddlDistrict']."' AND 
								EOI_Fee_Confirm='1' AND CURDATE() >= CourseITGK_StartDate AND 
								CURDATE() <= CourseITGK_ExpireDate
								AND Organization_AreaType = '".$_Response['area']."'
								AND Organization_Municipal = '".$_Response['ddlMunicipalName']."'";
						
						if(isset($_Response['ddlWardno']) && $_Response['ddlWardno'] != '' && $_Response['ddlWardno'] != '0'){
							$_SelectQuery .= " AND Organization_WardNo = '".$_Response['ddlWardno']."'";
						}
							$_SelectQuery .= " order by Organization_Name ASC";
					}
					else if($_Response['area']=='Rural'){
						$_SelectQuery = "Select Organization_User, Organization_Name, b.User_EmailId, b.User_MobileNo ,
								d.District_Name, e.Tehsil_Name, b.User_LoginId,a.Organization_Address From 
								tbl_organization_detail as a join tbl_user_master as b on a.Organization_User = b.User_Code
								join tbl_courseitgk_mapping as c on c.Courseitgk_ITGK = b.User_LoginId join
								tbl_district_master as d on d.District_Code = a.Organization_District join 
								tbl_tehsil_master as e on e.Tehsil_Code = a.Organization_Tehsil 
								Where b.User_Status = '1' AND b.User_UserRoll = '7' AND Courseitgk_Course='" . $_Course ."'
								AND CourseITGK_BlockStatus='unblock' AND Organization_State = '".$_Response['ddlState']."'
								AND Organization_District = '".$_Response['ddlDistrict']."' AND 
								EOI_Fee_Confirm='1' AND CURDATE() >= CourseITGK_StartDate AND 
								CURDATE() <= CourseITGK_ExpireDate
								AND Organization_AreaType = '".$_Response['area']."' AND 
								Organization_Panchayat = '".$_Response['ddlPanchayat']."'";
						if(isset($_Response['ddlGramPanchayat']) && $_Response['ddlGramPanchayat'] != '' && $_Response['ddlGramPanchayat'] != 0){
							$_SelectQuery .= " AND Organization_Gram = '".$_Response['ddlGramPanchayat']."'";
						}
						if(isset($_Response['ddlVillage']) && $_Response['ddlVillage'] != '' && $_Response['ddlVillage'] != 0){
							$_SelectQuery .= " AND Organization_Village = '".$_Response['ddlVillage']."'";
						}
							$_SelectQuery .= " order by Organization_Name ASC";
					}
				}
			$_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response1[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response1[1] = Message::Error;           
        }
         return $_Response1;
	}
 /**
         * @function LearnercenterlistWebAPI
         * @param $_Response array 
         * @return mixed|array : returns array 
         * @Description : Returns the detail result of Nearest ITGK Details details here
         */
        
	public function LearnercenterlistWebAPI($_Response){
		global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Organization_User, Organization_Name, b.User_EmailId, b.User_MobileNo , d.District_Name, e.Tehsil_Name, b.User_LoginId,a.Organization_Address From tbl_organization_detail as a join tbl_user_master as b on a.Organization_User = b.User_Code join tbl_courseitgk_mapping as c on c.Courseitgk_ITGK = b.User_LoginId join tbl_district_master as d on d.District_Code = a.Organization_District join tbl_tehsil_master as e on e.Tehsil_Code = a.Organization_Tehsil Where  b.User_Status = '1' AND b.User_UserRoll = 7 AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock' AND Organization_State = '".$_Response['ddlState']."' AND Organization_District = '".$_Response['ddlDistrict']."' AND Organization_Tehsil = '".$_Response['ddlTehsil']."'";
			if(isset($_Response['area']) && !empty($_Response['area']) ){
				$_SelectQuery .= " AND Organization_AreaType = '".$_Response['area']."'";
			}
			if(isset($_Response['ddlMunicipalName']) && !empty($_Response['ddlMunicipalName']) ){
				$_SelectQuery .= " AND Organization_Municipal = '".$_Response['ddlMunicipalName']."'";
			}
			if(isset($_Response['ddlWardno']) && !empty($_Response['ddlWardno']) ){
				$_SelectQuery .= " AND Organization_WardNo = '".$_Response['ddlWardno']."'";
			}
			if(isset($_Response['ddlPanchayat']) && !empty($_Response['ddlPanchayat']) ){
				$_SelectQuery .= " AND Organization_Panchayat = '".$_Response['ddlPanchayat']."'";
			}
			if(isset($_Response['ddlGramPanchayat']) && !empty($_Response['ddlGramPanchayat']) ){
				$_SelectQuery .= " AND Organization_Gram = '".$_Response['ddlGramPanchayat']."'";
			}
			if(isset($_Response['ddlVillage']) && !empty($_Response['ddlVillage'])){
				$_SelectQuery .= " AND Organization_Village = '".$_Response['ddlVillage']."'";
			}
			
			//echo $_SelectQuery;

			//"Org_formatted_address IS NOT NULL AND Organization_Tehsil = '".$_Tehsil_Code."' GROUP BY Org_formatted_address";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response1[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response1[1] = Message::Error;           
        }
         return $_Response1;
	}
	
	# Permission Releted Query Here #
        /**
         * @function getRkclApiPermissions
         * @param $ClientName,$api_key,$clientip  
         * @return mixed|array : returns array 
         * @Description : Returns TRUE OR FALSE according to provided detail
         */
    
        public function getRkclApiPermissions($ClientName,$api_key,$clientip)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_SelectQuery = "SELECT * FROM tbl_rkcl_api_permissions where ClientName = '".$ClientName."' and Apikey = '".$api_key."' and ClientIP = '".$clientip."'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
             return $_Response;
        }
	
}
