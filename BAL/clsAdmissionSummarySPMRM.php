
<?php

/**
 * Description of clsAdmissionSummarySPMRM
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsAdmissionSummarySPMRM {

    //put your code here

    public function GetDataAll($_Role, $_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_UserRole = $_SESSION['User_UserRoll'];
            $_SelectedRole = $_Role;
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '28') {

                $_SelectQuery3 = "SELECT tbl_admission.Admission_ITGK_Code AS RoleName, "
                        . "tbl_admission.Admission_ITGK_Code AS RoleCode, Count(tbl_admission.Admission_Code) "
                        . "AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch,"
                        . "sum(case when tbl_admission.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount "
                        . "FROM tbl_admission WHERE tbl_admission.Admission_Course = '" . $_course . "' "
                        . "AND tbl_admission.Admission_Batch = '" . $_batch . "'"
                        . "group by tbl_admission.Admission_ITGK_Code";
            }
            elseif ($_LoginRole == '34') {

                $_SelectQuery3 = "SELECT a.Admission_ITGK_Code AS RoleName, "
                        . "a.Admission_ITGK_Code AS RoleCode, Count(a.Admission_Code) "
                        . "AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch,"
                        . "sum(case when a.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount "
                        . "FROM tbl_admission as a inner join vw_itgkname_distict_rsp as vw on a.Admission_ITGK_Code = vw.ITGKCODE "
                        . "WHERE a.Admission_Course = '" . $_course . "' AND "
                        . " a.Admission_Batch = '" . $_batch . "' and  vw.District_Code='" . $_SESSION['Organization_District'] . "'"
                        . " group by a.Admission_ITGK_Code";
            }
            elseif ($_LoginRole == '14') {

                $_SelectQuery3 = "SELECT tbl_admission.Admission_ITGK_Code AS RoleName, "
                        . "tbl_admission.Admission_ITGK_Code AS RoleCode, Count(tbl_admission.Admission_Code) "
                        . "AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch,"
                        . "sum(case when tbl_admission.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount "
                        . "FROM tbl_admission WHERE tbl_admission.Admission_Course = '" . $_course . "' "
                        . "AND tbl_admission.Admission_Batch = '" . $_batch . "' and tbl_admission.Admission_RspName='" . $_SESSION['User_Code'] . "'"
                        . "group by tbl_admission.Admission_ITGK_Code";
            }else {
                $_SelectQuery3 = "";
            }


            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerList($_course, $_batch, $_rolecode, $_mode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            if($_mode=="ShowUpload")
            {
                $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,"
                    . "a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, b.Admission_Code as bAdmission_Code"
                    . ",b.Admission_HighQualification,"
                    . "b.Admission_Cast,b.Admission_Cast_Doc,b.Admission_SSC_Doc,b.Admission_SPC,b.Admission_SPC_Doc,b.Admission_AadharDoc,b.Admission_JanAadhar,b.Admission_JanAadharDoc,b.Admission_Ration,b.Admission_RationDoc,"
                    . "b.Admission_Ph,c.Category_Name FROM tbl_admission AS a left join tbl_admission_spmrm_scheme as b "
                    . "on a.Admission_Code=b.Admission_Code left join tbl_category_master as c on b.Admission_Cast=c.Category_Code"
                    . " WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND "
                    . "a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "'";
            }
            elseif($_mode=="ShowConfirm")
            {
                $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,"
                    . "a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, b.Admission_Code as bAdmission_Code"
                    . ",b.Admission_HighQualification,"
                    . "b.Admission_Cast,b.Admission_Cast_Doc,b.Admission_SSC_Doc,b.Admission_SPC,b.Admission_SPC_Doc,b.Admission_AadharDoc,b.Admission_JanAadhar,b.Admission_JanAadharDoc,b.Admission_Ration,b.Admission_RationDoc,"
                    . "b.Admission_Ph,c.Category_Name FROM tbl_admission AS a left join tbl_admission_spmrm_scheme as b "
                    . "on a.Admission_Code=b.Admission_Code left join tbl_category_master as c on b.Admission_Cast=c.Category_Code"
                    . " WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND "
                    . "a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND "
                    . "Admission_Payment_Status = '1'";
            }

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {

            //$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";
            echo $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Mobile, "
            . "a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, a.Admission_Payment_Status FROM tbl_admission AS a "
            . "WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' "
            . "AND a.Admission_Batch = '" . $_batch . "' ORDER BY a.Admission_Payment_Status DESC";
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);

            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedList($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
