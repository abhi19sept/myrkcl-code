<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsLearnerTransferIlearn
 *
 * @author ABHISHEK
 */

require 'DAL/classconnection.php';
require 'DAL/sendsms.php';
//$BASE_ILEARN = "http://localhost/toc/";
$BASE_ILEARN = "http://49.50.65.36/";
$ILEEARN_FILE_PATH = "uploads/jsonfile/"; // path for ilearn server
$BASE_SERVER_FILE = "../upload/jsonfile/"; // path for same/myrkcl server

$_ObjConnection = new _Connection();
$_Response = array();

class clsLearnerTransferIlearn {
    //put your code here
    
    public function GetITGK_NCR_Details($_CourseCode, $_BatchCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			
			$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
			$_BatchCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchCode);
			
            if ($_LoginRole == '7') {
                 
                $_SelectQuery = "SELECT * FROM tbl_admission where Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "' and Admission_Course='" . $_CourseCode . "' AND  Admission_Batch='" . $_BatchCode . "' ORDER BY IsNewRecord DESC";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
           }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
 public function GetDatabyCode($_CenterCode,$_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					
            $_SelectQuery = "select Admission_Code,Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB 
                from tbl_admission where Admission_Code in ($_LearnerCode) and Admission_ITGK_Code='" . $_CenterCode . "'";
                    //. " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
        public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_course_master where Course_Code in (1,4,5,26,3,23)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
           
           $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "' and Batch_Code >='261' ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAllMyrkclTbl($lcode, $batch, $course) {
        global $_ObjConnection;
        
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
             if($batch >= '261')
                {
                    $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$lcode."' AND Admission_Course = '".$course."' and Admission_Batch= '".$batch."' AND IsNewRecord ='Y' and Admission_Payment_Status='1'";
                     $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
                
            
        } catch (Exception $_exp) {

            $_Response[0] = $_exp->getLine() . $_exp->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function myrkcltoilearnFileCreate($lcode, $batche, $course) {
        $response = $this->GetAllMyrkclTbl($lcode, $batche, $course);
 
        $resultset = [];
        if (!empty($response)) {
            while ($_Row = mysqli_fetch_array($response[2],MYSQLI_ASSOC)) {
               // $resultset[] = $_Row;
                $resultset[] = array_map("utf8_encode", $_Row);
            }

            $json_string =  json_encode($resultset);

      
            $result = [
                'status' => '200',
                'result' => $json_string
            ];
        } else {
            $result = ['status' => '404', 'error' => 'No data found'];
            
        }
        return $result;
    }
    public function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
        $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
        $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
        $result = str_replace($escapers, $replacements, $value);
        return $result;
    }
    public function InsertiLearnTbl($data_json) {

        // $data_json = json_encode(["filen" => $file_name]);
        $abc= $this->curlPost("http://49.50.65.36/common/cfiLearnDatasync.php?action=myrkcllearntransfer", $data_json);
        m_log("curl response".date('Y-m-d h:i:s').json_encode($abc));
       return $abc;


    }
public function curlPost($url, $data_json = NULL) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 300,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data_json,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: 5c22b1ed-b568-1c45-3d0d-ebff096e4cda"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
        $data = ['status' => '404', 'error' => $err];
    } else {
        $data = ['status' => '200', 'success' => $response];
    }
    return $data;
}
public function TblStatusUpdtoN($lcode, $batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
                $_SelectQuery = "update tbl_admission set IsNewRecord='N' where Admission_LearnerCode='".$lcode."' AND Admission_Course = '".$course."' and Admission_Batch= '".$batch."'";
                       
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::OnUpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
