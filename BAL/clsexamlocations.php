<?php
/**
 * Description of clsAreaMaster
 *
 * @author Abhi
 */
require('../DAL/classconnectionNEW.php');
$_ObjConnection = new _Connection();
$_ObjConnection->Connect();
$_Response = array();

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

class clsexamchoicestatus {
    public function GetAllLocationDistricts($examId, $districts = [], $itgk = '') {
        global $_ObjConnection;
        $result = [];
        try {
            $filter = ($districts) ? " AND (el.districtId IN (" . implode(',', $districts) . ") OR el.location_rkcl_district IN (" . implode(',', $districts) . "))" : '';
            $filter .= ($itgk) ? " AND el.itgkcode = '" . $itgk . "'" : '';
			$_SelectQuery = "SELECT dm.District_Code AS `districtId`, dm.District_Name FROM tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master atm ON atm.Tehsil_Code = el.location_rkcl INNER JOIN tbl_district_master dm ON dm.District_Code = atm.Tehsil_District WHERE el.eventid = '" . $examId . "'  AND status LIKE ('Eligible') $filter GROUP BY dm.District_Code ORDER BY dm.District_Name";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if (mysqli_num_rows($_Response[2])) {
            	while ($row = mysqli_fetch_array($_Response[2])) {
            		$result[$row['districtId']]['name'] = $row['District_Name'];
                    $result[$row['districtId']]['location_details'] = $this->showDistrictLocationCounts($examId, $row['districtId'], '', $itgk);
                    $result[$row['districtId']]['thesils'] = $this->getDistrictThesils($row['districtId']);
            	}
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function getExamEvents() {
    	global $_ObjConnection;
        $result = [];
        try {
			$_SelectQuery = "SELECT el.eventid, e.Event_Name FROM tbl_eligiblelearners_temp el INNER JOIN tbl_events e ON e.Event_Id = el.eventid WHERE el.eventid >= 1222 GROUP BY el.eventid ORDER BY el.eventid DESC LIMIT 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if (mysqli_num_rows($_Response[2])) {
            	while ($row = mysqli_fetch_array($_Response[2])) {
            		$result[$row['eventid']] = $row['Event_Name'];
            	}
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function showDistrictLocationCounts($eventid, $districtId = '', $thesilId = '', $itgk = '') {
    	global $_ObjConnection;
        $result = [];
        try {
            $filter = ($thesilId) ? " AND el.location_rkcl = '" . $thesilId . "'" : " AND el.location_rkcl_district = '" . $districtId . "'";
            $filter .= ($itgk) ? " AND el.itgkcode = '" . $itgk . "'" : '';
			$_SelectQuery = "SELECT el.districtId, el.thesilId, tm.Tehsil_Name AS thesil, el.location_rkcl, atm.Tehsil_District, atm.Tehsil_Name AS exam_thesil, COUNT(*) as n FROM tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = el.thesilId INNER JOIN tbl_tehsil_master atm ON atm.Tehsil_Code = el.location_rkcl WHERE el.eventid = '" . $eventid . "' AND status LIKE ('Eligible') $filter GROUP BY el.thesilId, el.location_rkcl ORDER BY atm.Tehsil_Name ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
            	while ($row = mysqli_fetch_array($_Response[2])) {
            		$result[] = $row;
            	}
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function showExamLocationCounts($eventid, $districtId) {
        global $_ObjConnection;
        $result = [];
        try {
            $filter1 = ($districtId) ? "AND el.location_rkcl_district = '" . $districtId . "'" : '';
            $filter2 = ($districtId) ? "AND vm.district = '" . $districtId . "'" : '';
            $filter3 = ($districtId) ? "AND  location_rkcl_district = '" . $districtId . "'" : '';
            $filter4 = ($districtId) ? "AND el.location_rkcl_district = '" . $districtId . "' AND ch1.Tehsil_District = '" . $districtId . "'" : '';
            $_SelectQuery = "(SELECT el.districtId, el.thesilId, el.location_rkcl, dm.District_Name, atm.Tehsil_Name AS exam_thesil, COUNT(*) as n, ( SELECT SUM(examcentercapacity) FROM tbl_examcenterdetailmaster WHERE examid = '" . $eventid . "' AND district = el.location_rkcl_district AND examtehsil = el.location_rkcl ) AS vmou_count FROM tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master atm ON atm.Tehsil_Code = el.location_rkcl INNER JOIN tbl_district_master dm ON dm.District_Code = atm.Tehsil_District WHERE el.eventid = '" . $eventid . "'  AND status LIKE ('Eligible') $filter1 GROUP BY el.location_rkcl ORDER BY el.location_rkcl_district, exam_thesil) UNION (

                SELECT '', '', '', dm.District_Name, atm.Tehsil_Name AS exam_thesil, '-' AS n, SUM(vm.examcentercapacity) AS vmou_count FROM tbl_examcenterdetailmaster vm INNER JOIN tbl_tehsil_master atm ON vm.examtehsil = atm.Tehsil_Code INNER JOIN tbl_district_master dm ON  dm.District_Code = atm.Tehsil_District WHERE vm.examid = '" . $eventid . "' $filter2 AND vm.examtehsil NOT IN (SELECT distinct location_rkcl FROM tbl_eligiblelearners_temp WHERE eventid = '" . $eventid . "' AND status LIKE ('Eligible') $filter3 ) GROUP BY vm.district
                ) 

                UNION (SELECT '', '', '', CONCAT(dm.District_Name, ' Total Counts:') AS District_Name, '' AS exam_thesil, COUNT(*) as n, ( SELECT SUM(examcentercapacity) FROM tbl_examcenterdetailmaster WHERE examid = '" . $eventid . "' AND district = el.location_rkcl_district ) AS vmou_count FROM tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master ch1 ON ch1.Tehsil_Code = el.location_rkcl INNER JOIN tbl_district_master dm ON  dm.District_Code = ch1.Tehsil_District WHERE el.eventid = '" . $eventid . "' AND status LIKE ('Eligible') $filter4 GROUP BY el.location_rkcl_district)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $result[] = $row;
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function setExamThesil($newTehsilId, $eventId, $itgkThesil, $examThesil, $itgkcode, $ids = '', $follow = 1) {
        global $_ObjConnection;
        $filter = ($examThesil) ? " AND el.location_rkcl = '" . $examThesil . "'" : "";
        $filter .= ($itgkcode) ? " AND el.itgkcode = '" . $itgkcode . "'" : "";
        $filter .= ($ids) ? " AND el.id IN (" . $ids . "0)" : "";
        $choiceFilter = ($follow) ? "AND (ecm.choice1 = '" . $newTehsilId . "' OR ecm.choice2 = '" . $newTehsilId . "' OR ecm.choice3 = '" . $newTehsilId . "' OR ecm.choice4 = '" . $newTehsilId . "' OR ecm.choice5 = '" . $newTehsilId . "')" : '';
        $updateQuery = "UPDATE tbl_eligiblelearners_temp el INNER JOIN tbl_examchoicemaster_temp ecm ON (ecm.centercode = el.itgkcode $choiceFilter) SET el.location_rkcl = '" . $newTehsilId . "', el.location_rkcl_district = (SELECT Tehsil_District FROM tbl_tehsil_master WHERE Tehsil_Code = '" . $newTehsilId . "') WHERE el.eventid = '" . $eventId . "'  AND el.status LIKE ('Eligible') AND el.thesilId = '" . $itgkThesil . "'" . $filter;
        //die;
        $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
    }

    public function getDistrictThesils($districtId, $thesilId = '') {
        global $_ObjConnection;
        $result = [];
        try {
            $filter = ($thesilId) ? " AND Tehsil_Code = '" . $thesilId . "'" : "";
            $_SelectQuery = "SELECT Tehsil_Code, Tehsil_Name FROM tbl_tehsil_master WHERE Tehsil_District = '" . $districtId . "' $filter";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $result[$row['Tehsil_Code']] = $row['Tehsil_Name'];
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function getDistrict($districtId) {
        global $_ObjConnection;
        $result = [];
        try {
				$districtId = mysqli_real_escape_string($_ObjConnection->Connect(),$districtId);
				
            $_SelectQuery = "SELECT District_Name FROM tbl_district_master WHERE District_Code = '" . $districtId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $result[$districtId] = $row['District_Name'];
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function getThesilDistrict($thesilIds = []) {
        global $_ObjConnection;
        $result = [];
        try {
            $_SelectQuery = "SELECT Tehsil_Code, Tehsil_District FROM tbl_tehsil_master WHERE Tehsil_Code IN (" . implode(',', $thesilIds) . ")";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $result[$row['Tehsil_Code']] = $row['Tehsil_District'];
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    function getThesilsWithLessCounts($minCount = 150, $eventId) {
        global $_ObjConnection;
        $result = [];
        try {
            $_SelectQuery = "SELECT location_rkcl_district, location_rkcl, count(*) AS n, ( SELECT SUM(examcentercapacity) FROM tbl_examcenterdetailmaster WHERE examid = '" . $eventId . "' AND district = location_rkcl_district AND examtehsil = location_rkcl ) AS vmou_count FROM tbl_eligiblelearners_temp WHERE status LIKE('Eligible') AND eventid = '" . $eventId . "' GROUP BY location_rkcl_district, location_rkcl HAVING ((n < $minCount) OR (vmou_count IS NOT NULL AND n <> vmou_count))";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    if ($row['n'] < $minCount) {
                        $result['less_count'][$row['location_rkcl']] = $row['location_rkcl_district'];
                    } else {
                        $result['vmou_count_missmatch'][$row['location_rkcl']] = $row['location_rkcl_district'];
                    }
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    function switchLessCountLocations($round, $eventId) {
        if ($round > 1 && $round < 6) {
            $thesilsWithLessCounts = $this->getThesilsWithLessCounts(150, $eventId);
            if (isset($thesilsWithLessCounts['less_count'])) {
                $lessCounts = $thesilsWithLessCounts['less_count'];
                foreach ($lessCounts as $thesil => $district) {
                    $this->switchLocationChoice($round, $thesil, $district, $eventId);
                }
            }
        }
    }

    function resetonFinalizedAllotments($eventId) {
        global $_ObjConnection;
        $updateQuery = "UPDATE tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = el.location_rkcl_finalized SET el.location_rkcl = el.location_rkcl_finalized, el.location_rkcl_district = tm.Tehsil_District WHERE el.eventid = '" . $eventId . "'";
        $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
    }

    function setInitialAllotments($eventId) {
        global $_ObjConnection;
		$eventId = mysqli_real_escape_string($_ObjConnection->Connect(),$eventId);
		
        $updateQuery = "UPDATE tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = el.location_rkcl SET el.location_rkcl_fixed = el.location_rkcl, el.location_rkcl_finalized = el.location_rkcl, el.location_rkcl_district = tm.Tehsil_District WHERE el.eventid = '" . $eventId . "'";
        $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
    }

    function setAllotmentsAsFinalized($eventid) {
        global $_ObjConnection;
		$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
		
        $updateQuery = "UPDATE tbl_eligiblelearners_temp el INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = el.location_rkcl SET el.location_rkcl_finalized = el.location_rkcl, el.location_rkcl_district = tm.Tehsil_District WHERE el.eventid = '" . $eventId . "'";
        $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
    }

    function switchLocationChoice($id, $oldLocation, $district, $eventId) {
        global $_ObjConnection;
        $result = [];
        try {
            $choice = 'choice' . $id;
            $oldChoice = 'choice' . ($id - 1);
            $_SelectQuery = "SELECT centercode, $oldChoice, $choice FROM tbl_examchoicemaster_temp WHERE $oldChoice = '" . $oldLocation . "' AND $choice > 0";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $updateQuery = "UPDATE tbl_eligiblelearners_temp SET location_rkcl = '" . $row[$choice] . "', location_rkcl_district = (SELECT Tehsil_District FROM tbl_tehsil_master WHERE Tehsil_Code = '" . $row[$choice] . "') WHERE eventid = '" . $eventId . " '  AND status LIKE ('Eligible') AND itgkcode = '" . $row['centercode'] . "' AND location_rkcl = '" . $row[$oldChoice] . "' AND location_rkcl_district = '" . $district . "'";
                    $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function getLocationWiseItgkCount($eventId, $itgkThesil, $locationId) {
        global $_ObjConnection;
        $result = [];
        try {
            $_SelectQuery = "SELECT el.itgkcode, count(*) AS n FROM tbl_eligiblelearners_temp el WHERE el.status LIKE('Eligible') AND el.eventid = '" . $eventId . "' AND el.location_rkcl = '" . $locationId . "' AND thesilId = '" . $itgkThesil . "' GROUP BY el.itgkcode ORDER BY el.itgkcode ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $result[$row['itgkcode']] = $row['n'];
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function getLocationITGKIds($eventId, $itgkThesil, $locationId, $newTehsilId) {
        global $_ObjConnection;
        $result = [];
        try {
            $_SelectQuery = "SELECT el.itgkcode FROM tbl_eligiblelearners_temp el INNER JOIN tbl_examchoicemaster_temp ecm ON (ecm.centercode = el.itgkcode AND (ecm.choice1 = '" . $newTehsilId . "' OR ecm.choice2 = '" . $newTehsilId . "' OR ecm.choice3 = '" . $newTehsilId . "' OR ecm.choice4 = '" . $newTehsilId . "' OR ecm.choice5 = '" . $newTehsilId . "')) WHERE el.status LIKE('Eligible') AND el.eventid = '" . $eventId . "' AND el.location_rkcl = '" . $locationId . "' AND el.thesilId = '" . $itgkThesil . "' ORDER BY el.learnername ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $result[] = $row['itgkcode'];
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return implode(',', $result);
    }

    public function getItgkWiseLearnerListOfaLocation($eventId, $itgkcode, $itgkThesil, $locationId, $newTehsilId) {
        global $_ObjConnection;
        $result = [];
        try {
            $filter = ($itgkcode != 'all') ? " AND itgkcode = '" . $itgkcode . "'" : "";
            $joinFilter = ($newTehsilId) ? "INNER JOIN tbl_examchoicemaster_temp ecm ON (ecm.centercode = el.itgkcode AND (ecm.choice1 = '" . $newTehsilId . "' OR ecm.choice2 = '" . $newTehsilId . "' OR ecm.choice3 = '" . $newTehsilId . "' OR ecm.choice4 = '" . $newTehsilId . "' OR ecm.choice5 = '" . $newTehsilId . "'))" : "";
            $_SelectQuery = "SELECT el.id, el.learnercode, el.learnername, el.itgkcode FROM tbl_eligiblelearners_temp el $joinFilter WHERE el.status LIKE('Eligible') AND el.eventid = '" . $eventId . "' AND el.location_rkcl = '" . $locationId . "' AND el.thesilId = '" . $itgkThesil . "' $filter ORDER BY el.learnername ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $result[$row['id']] = $row;
                }
            }
        } catch (Exception $_ex) {
            $result = [];
        }

        return $result;
    }

    public function resetAsHard() {
        global $_ObjConnection;

        $drop = "DROP TABLE IF EXISTS tbl_finalexammapping_temp";
        $_DropResponse = $_ObjConnection->ExecuteQuery($drop, Message::MultipleStatement);
        $drop = "DROP TABLE IF EXISTS tbl_examchoicemaster_temp";
        $_DropResponse = $_ObjConnection->ExecuteQuery($drop, Message::MultipleStatement);

        $create = "CREATE TABLE tbl_examchoicemaster_temp LIKE tbl_examchoicemaster";
        $_CreateResponse = $_ObjConnection->ExecuteQuery($create, Message::MultipleStatement);
        $create = "CREATE TABLE tbl_finalexammapping_temp LIKE tbl_finalexammapping";
        $_CreateResponse = $_ObjConnection->ExecuteQuery($create, Message::MultipleStatement);

        $updateQuery = "UPDATE tbl_examchoicemaster SET status = 'Approve' WHERE status NOT LIKE('%dis%')";
        $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);

        $insert = "INSERT INTO tbl_examchoicemaster_temp SELECT * FROM tbl_examchoicemaster WHERE status LIKE('Approve')";
        $_InsertResponse = $_ObjConnection->ExecuteQuery($insert, Message::InsertStatement);

        $updateQuery = "UPDATE tbl_eligiblelearners_temp SET location_rkcl = '', location_rkcl_district = '', location_rkcl_fixed = '', location_vmou_district = '', location_vmou = ''";
        $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
        $_SESSION['final_count'] = 0;
        $_SESSION['Exam_Districts'] = [];
    }

    public function assignChoices() {
        global $_ObjConnection;
        $events = $this->getExamEvents();
        if ($events) {
            foreach ($events as $examId => $eventName) {
                $fillChoices = "CALL `fill_default_exam_choices`(" . $examId . ")";
                $_Response = $_ObjConnection->ExecuteQuery($fillChoices, Message::MultipleStatement);
                exit;
            }
        }
    }

    public function resetChoices($eventId) {
        global $_ObjConnection;
        $updateQuery = "UPDATE tbl_eligiblelearners_temp SET location_rkcl = location_rkcl_fixed, location_rkcl_district = (SELECT Tehsil_District FROM tbl_tehsil_master WHERE Tehsil_Code = location_rkcl_fixed) WHERE eventid = '" . $eventId . "'";
        $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
    }

    public function generateFinalMapping($eventId) {
        global $_ObjConnection;
        $drop = "DROP TABLE IF EXISTS tbl_finalexammapping_temp";
        $_DropResponse = $_ObjConnection->ExecuteQuery($drop, Message::MultipleStatement);
        $create = "CREATE TABLE tbl_finalexammapping_temp LIKE tbl_finalexammapping";
        $_CreateResponse = $_ObjConnection->ExecuteQuery($create, Message::MultipleStatement);
		
		$alter = "ALTER TABLE tbl_finalexammapping_temp change `id` `id` bigint(20) NOT NULL AUTO_INCREMENT";
        $_AlterResponse = $_ObjConnection->ExecuteQuery($alter, Message::MultipleStatement);

        $alter = "ALTER TABLE `tbl_finalexammapping_temp` Engine=InnoDB checksum=1 auto_increment=100001 comment='' delay_key_write=1 row_format=dynamic charset=latin1 collate=latin1_swedish_ci";
        $_AlterResponse = $_ObjConnection->ExecuteQuery($alter, Message::MultipleStatement);

        $alter = "ALTER TABLE `tbl_finalexammapping_temp` add unique `uniquecenter` (`learnercode`, `examcentercode`)";
        $_AlterResponse = $_ObjConnection->ExecuteQuery($alter, Message::MultipleStatement);

        $callProc = "CALL generate_final_exam_mapping($eventId)";
        $_AlterResponse = $_ObjConnection->ExecuteQuery($callProc, Message::MultipleStatement);

        /*$_SelectQuery = "SELECT ec.examcentercode, ec.examcentercapacity FROM tbl_examcenterdetailmaster ec INNER JOIN tbl_tehsil_master th ON th.Tehsil_Code = ec.examtehsil INNER JOIN tbl_district_master dm ON dm.District_Code = th.Tehsil_District WHERE ec.examid = '" . $eventId . "' ORDER BY ec.examcentercode";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                $insertQuery = "INSERT IGNORE INTO tbl_finalexammapping_temp (learnercode, learnername, fathername, dob, tac, coursename, batchname, centercode, centername, examcentercode, examcentername, examcenterdistrict, examcentertehsil, examcenteraddress, examcentercapacity, mobileno, email, noofrooms, coname, eventname, rollno, examid, examdate, examtime) (
                SELECT el.learnercode, el.learnername, el.fathername, el.dob, ls.Score, el.coursename, el.batchname, el.itgkcode, el.itgkname, ec.examcentercode, ec.examcentername, dm.District_Name, th.Tehsil_Name, ec.examcenteradd, ec.examcentercapacity, ec.examcentermobile, ec.examcenteremail, ec.noofrooms, ec.examcentercoordinator, ev.Event_Name_VMOU, '' AS rollno, ec.examid, DATE_FORMAT(el.examdate, '%d %M %Y') AS examdate, '12:00 NOON to 01:00 PM' AS examtime FROM tbl_eligiblelearners_temp el INNER JOIN tbl_examcenterdetailmaster ec ON ec.examid = el.eventid AND ec.examtehsil = el.location_rkcl LEFT JOIN tbl_learner_score ls ON ls.Learner_Code = el.learnercode INNER JOIN tbl_tehsil_master th ON th.Tehsil_Code = el.location_rkcl INNER JOIN tbl_district_master dm ON dm.District_Code = th.Tehsil_District INNER JOIN tbl_events ev ON ev.Event_Id = el.eventid WHERE el.eventid = '" . $eventId . "' AND ec.examcentercode = '" . $row['examcentercode'] . "' AND el.status LIKE ('Eligible') AND el.learnercode NOT IN (SELECT distinct learnercode FROM tbl_finalexammapping_temp) GROUP BY el.learnercode ORDER BY dm.District_Name, th.Tehsil_Name, el.learnername, el.learnercode LIMIT 0, " . $row['examcentercapacity'] . "
                )";
                $_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
            }
        }

        $update = "UPDATE tbl_finalexammapping_temp SET rollno = id WHERE rollno = '0'";
        $_ObjConnection->ExecuteQuery($update, Message::UpdateStatement);*/
    }

    public function getFinalMappingCount($eventId) {
        global $_ObjConnection;
        $result = [];
        try {
            $_SelectQuery = "SELECT COUNT(*) AS n FROM tbl_finalexammapping_temp WHERE examid = '" . $eventId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    if ($row['n']) {
                        $result[] = $row['n'];
                    }
                }
            } else {
                unset($_SESSION['final_count']);
            }
        } catch (Exception $_ex) {
            $result = [];
            unset($_SESSION['final_count']);
        }

        return $result;
    }
}
?>