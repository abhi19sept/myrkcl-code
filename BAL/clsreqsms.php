<?php


/**
 * Description of clsOrgDetail
 *
 * @author 
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsreqsms {
	
	 
	
	public function Add($_package,$_price) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			  $_ITGK_Code =   $_SESSION['User_LoginId'];
             $_InsertQuery = "Insert Into tbl_package_transections(id,package,price,centercode,Payment_Status) 
			 Select Case When Max(id) Is Null Then 1 Else Max(id)+1 End as id,
			 '" . $_package. "' as package,
			 '" .$_price. "' as price,
			 '" .$_ITGK_Code. "' as centercode,
			 '0' as Payment_Status
			 From tbl_package_transections";
		
            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				//$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
       //print_r($_Response);
        return $_Response;
    }
	
	
	public function Getpackage() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "Select * from tbl_package where Status='1' order by id";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetPkgPrice() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "Select price from tbl_package_transections where centercode='". $_SESSION['User_LoginId'] ."' AND Payment_Status='0' ORDER BY id DESC LIMIT 1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function Getprice($_activate) 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
		    $_SelectQuery = "Select Price from tbl_package where package='" .$_activate. "' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function GetAll() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		     $_ITGK_Code =   $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select * from tbl_package_transections where centercode='".$_ITGK_Code."' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
public function AddPayTran($amount, $txtGenerateId, $txtGeneratePayUId,$_smspackageid) {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_RKCL_Id = $txtGenerateId . '_RKCLtranid';

                $_ITGK_Code = $_SESSION['User_LoginId'];
                //$_Fname = $_SESSION['User_Code'];

				 $_InsertQuery = "INSERT INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_RKCL_Trnid, "
                        . " Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_PG_Trnid,Pay_Tran_AdmissionArray)"
                        . " values ('" . $_ITGK_Code . "', '" . $_RKCL_Id . "', '" . $amount . "', 'PaymentInProcess', 'SMSFeePayment', '" . $txtGeneratePayUId . "',
									'" . $_smspackageid . "')";

                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

                $_UpdateQuery = "Update tbl_package_transections set Package_TranRefNo = '" . $txtGeneratePayUId . "',
								Package_RKCL_Trnid='" . $_RKCL_Id . "' Where
								centercode='". $_SESSION['User_LoginId'] ."' AND Payment_Status='0' AND id in ($_smspackageid)";
                $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
		
public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetSMSPkgAmt($_SMSId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code =   $_SESSION['User_LoginId'];
             $_SelectQuery = "Select price From tbl_package_transections where centercode='".$_ITGK_Code."' AND id IN ($_SMSId)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
