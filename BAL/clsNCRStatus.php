<?php

/**
 * Description of clsNCRStatus
 *
 * @author Abhishek
 */
//require 'DAL/classconnectionNEW.php';
//$_ObjConnection = new _Connection();
$_Response = array();

class clsNCRStatus {

    //put your code here
    public function chk_sp_ao($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            $_SelectQuery = "Select * FROM tbl_user_master where User_LoginId='" . $_Ccode . "' and
								User_Rsp='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStep2Data($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 28) {
                $_SelectQuery = "Select * FROM tbl_user_master where User_LoginId='" . $_Ccode . "' and User_UserRoll in ('22','15','7') and "
                        . " User_PaperLess='Yes'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else if ($_SESSION['User_UserRoll'] == 14) {
                $_SelectQuery = "Select * FROM tbl_user_master where User_LoginId='" . $_Ccode . "' and User_UserRoll in ('22','15','7') and "
                        . " User_Rsp='" . $_SESSION['User_Code'] . "' and User_PaperLess='Yes'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStep3Data($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 28) {
                $_SelectQuery = "Select a.*, b.Ncr_Transaction_Amount,b.Ncr_Transaction_Txtid,b.Ncr_Transaction_timestamp FROM tbl_user_master as a inner join tbl_ncr_transaction as b on"
                        . " a.User_LoginId=b.Ncr_Transaction_CenterCode where User_LoginId='" . $_Ccode . "' and User_UserRoll in ('15','7') and "
                        . " User_PaperLess='Yes' and b.Ncr_Transaction_Status='success'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else if ($_SESSION['User_UserRoll'] == 14) {
                $_SelectQuery = "Select a.*, b.Ncr_Transaction_Amount,b.Ncr_Transaction_Txtid,b.Ncr_Transaction_timestamp FROM tbl_user_master as a inner join tbl_ncr_transaction as b on"
                        . " a.User_LoginId=b.Ncr_Transaction_CenterCode where User_LoginId='" . $_Ccode . "' and User_UserRoll in ('15','7') and "
                        . " User_PaperLess='Yes' and b.Ncr_Transaction_Status='success' and "
                        . " User_Rsp='" . $_SESSION['User_Code'] . "' and User_PaperLess='Yes'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SHOWGIDATA($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            $_SelectQuery = "Select * FROM tbl_intake_master where Intake_Center='" . $_Ccode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStep11Data($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            $_SelectQuery = "Select * From tbl_rspitgk_mapping Where Rspitgk_ItgkCode='" . $_Ccode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStep12Data($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            $_SelectQuery = "Select * From tbl_ncrvisit_photo Where NCRVisit_CenterCode = '" . $_Ccode . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStep14Data($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 28) {
                $_SelectQuery = "Select * FROM tbl_user_master where User_LoginId='" . $_Ccode . "' and User_UserRoll ='7' and "
                        . " User_PaperLess='Yes'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else if ($_SESSION['User_UserRoll'] == 14) {
                $_SelectQuery = "Select * FROM tbl_user_master where User_LoginId='" . $_Ccode . "' and User_UserRoll ='7' and "
                        . " User_Rsp='" . $_SESSION['User_Code'] . "' and User_PaperLess='Yes'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStep15Data($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            $_SelectQuery = "Select * From tbl_spcenter_agreement Where SPCenter_CenterCode = '" . $_Ccode . "'";


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetStep17Data($_Ccode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ccode);
				
            $_SelectQuery = "Select a.*, b.EOI_Name From tbl_eoi_centerlist as a inner join tbl_eoi_master as b on  a.EOI_Code=b.EOI_Code "
                    . " Where a.EOI_ECL = '" . $_Ccode . "'";


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
