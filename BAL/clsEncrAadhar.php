<?php

/**
 * Description of clsEncrAadhar
 * Created by SUNIL KUMAR BAINDARA
 * Date: 24-02-2020
 */
require 'DAL/classconnectionAadhar.php';

$_ObjConnection_Aadhar = new _Connection_Aadhar();
//$_Response = array();
//$_Response1 = array();
//$_Response2 = array();
//$_Response3 = array();

class clsEncrAadhar {

    //put your code here

    /* Insert Aadhar encryption details into aadhar enc detail table  */
    public function InsertAadharEncrData($Learner_Code,$referenceID,$EncAadharNumber)
        {
            global $_ObjConnection_Aadhar;
            $_ObjConnection_Aadhar->Connect();
            try {
            $_SelectQuery = "SELECT * FROM tbl_aadhar_enc_details WHERE LearnerCode = '" . $Learner_Code . "' and UUIDReferenceID= '" . $referenceID . "'";
            $_Response=$_ObjConnection_Aadhar->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //echo "<pre>"; print_r($_Response);die;
            if ($_Response[0] =='No Record Found') {
                        $_InsertQuery="INSERT INTO `tbl_aadhar_enc_details`(`LearnerCode`, `UUIDReferenceID`, `EncAadharNumber`) 
                        VALUES ('".$Learner_Code."','".$referenceID."','".$EncAadharNumber."')";
                        $_Response1 = $_ObjConnection_Aadhar->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                        return $_Response1;
                    }
             else{
                        $_UpdateQuery="UPDATE `tbl_aadhar_enc_details` SET
                           `UUIDReferenceID`='".$referenceID."',
                           `EncAadharNumber`='".$EncAadharNumber."'
                            WHERE `LearnerCode`='".$Learner_Code."'";
                        $_Response2 = $_ObjConnection_Aadhar->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                        return $_Response2;
             }

            } catch (Exception $_ex) {

                $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response3[1] = Message::Error;
                return $_Response3;
            }
            
        }
    
    
    /* Get encrypted  Aadhar details by learner code  */    
    public function GetEncrAadharDataByLCode($lcode) {
        global $_ObjConnection_Aadhar;
        $_ObjConnection_Aadhar->Connect();
        try {            
            $_SelectQuery = "select * from tbl_aadhar_enc_details where LearnerCode='".$lcode."'";
            $_Response = $_ObjConnection_Aadhar->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }    
}
