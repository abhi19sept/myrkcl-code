<?php

/*
 * @author Abhi

 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
require 'common/payments.php';
class clsPayment extends paymentFunctions {
    //put your code here
    public function GetDatabyCode($_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            if (isset($_SESSION['User_UserRoll']) && ($_SESSION['User_UserRoll'] == 19 || $_SESSION['User_UserRoll'] == 33)) {
                $_SelectQuery = "Select Admission_Fname AS UserProfile_FirstName, Admission_Mobile AS UserProfile_Mobile, Admission_Email AS UserProfile_Email From tbl_admission Where Admission_LearnerCode='" . $_SESSION['User_LearnerCode'] . "'";
            } else {
                $_SelectQuery = "Select UserProfile_FirstName,UserProfile_Mobile,UserProfile_Email From tbl_userprofile Where UserProfile_User='" . $_Code . "'";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     public function Update($_Code,$_PayTypeCode,$_TranRefNo) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_PayTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_PayTypeCode);
				
            $_UpdateQuery = "Update tbl_courseitgk_mapping set EOI_Fee_Confirm='1', Courseitgk_TranRefNo= '" . $_TranRefNo . "'"
                    . " Where Courseitgk_ITGK='" . $_Code . "' AND Courseitgk_EOI='" . $_PayTypeCode . "'";
			
             $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }

    public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyTrnxid($trnxId) {
        return parent::GetPaymentDataByTrnxid($trnxId); 
    }

    public function updateTransactionStatus($trnxId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_UpdateQuery = "UPDATE tbl_payment_transaction SET Pay_Tran_Status = 'PaymentInProcess' WHERE Pay_Tran_PG_Trnid = '" . $trnxId . "'";
            
             $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;

        }

        return $_Response;
    }
}
