<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';
date_default_timezone_set("Asia/Kolkata");

$_ObjConnection = new _Connection();
$_ObjConnection->Connect();

$_Response = array();
$_Response3 = array();

class clstransectionauditreport {

    //put your code here
    function dbConnection() {
        global $_ObjConnection;

        return $_ObjConnection;
    }

    public function GetAll($trnsactionsId) {
        $_ObjConnection = $this->dbConnection();
        try {
    ///        $query = "SELECT pt.*, pr.Payment_Refund_Amount FROM tbl_payment_transaction pt LEFT JOIN tbl_payment_refund pr ON pt.Pay_Tran_PG_Trnid = pr.Payment_Refund_Txnid WHERE Pay_Tran_PG_Trnid = '" . $trnsactionsId . "'";
	
	 $query = "SELECT Pay_Tran_Status, Pay_Tran_PG_Trnid  as trnid
, pr.Payment_Refund_Amount,Pay_Tran_Amount,Pay_Tran_ProdInfo,Pay_Tran_ITGK,Pay_Tran_Fname,`timestamp`,Pay_Tran_LCount,Pay_Tran_AdmissionArray,Pay_Tran_Course,Pay_Tran_Batch 
FROM tbl_payment_transaction pt LEFT JOIN tbl_payment_refund pr 
ON pt.Pay_Tran_PG_Trnid = pr.Payment_Refund_Txnid WHERE Pay_Tran_PG_Trnid ='" . $trnsactionsId . "' OR razorpay_payment_id='" . $trnsactionsId . "'";

            $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            if (mysqli_num_rows($_Response[2])) {
                $data = mysqli_fetch_array($_Response[2]);
                return [
                    'status' => $this->getStatus($data['Pay_Tran_Status']),
                    'txnid' => $data['trnid'],
                    'amount' => (!is_null($data['Payment_Refund_Amount']) && $data['Payment_Refund_Amount'] >= 0) ? $data['Payment_Refund_Amount'] : $data['Pay_Tran_Amount'],
                    'productinfo' => $data['Pay_Tran_ProdInfo'],
                    'udf1' => $data['Pay_Tran_ITGK'],
                    'firstname' => $data['Pay_Tran_Fname'],
                    'datetime' => $data['timestamp'],
                    'lcount' => ((!empty($data['Pay_Tran_LCount'])) ? $data['Pay_Tran_LCount'] : '-'),
                    'lcodes' => ((!empty($data['Pay_Tran_AdmissionArray'])) ? $this->getLearnerCodesByAdmissionArray($data['Pay_Tran_AdmissionArray'], $data['Pay_Tran_Course'], $data['Pay_Tran_Batch']) : '-'),
                ];
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    private function getLearnerCodesByAdmissionArray($admissionArray, $course, $batch) {
        $_ObjConnection = $this->dbConnection();
        $codes = [];
        $filter = (!empty($course)) ? " AND Admission_Course = '" . $course . "'" : '';
        $filter .= (!empty($batch)) ? " AND Admission_Batch = '" . $batch . "'" : '';
        $query = "SELECT distinct Admission_LearnerCode FROM tbl_admission WHERE Admission_Code IN (0" . $admissionArray . ") $filter";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while($data = mysqli_fetch_array($_Response[2])) {
                $codes[] = $data['Admission_LearnerCode'];
            }
        }

        return ($codes) ? implode(', ', $codes) : ''; 

    }

    private function getStatus($status) {
        $txStatus = '';
        $isSuccess = stripos($status, 'receive');
        if ($isSuccess) {
            $txStatus = 'Success';
        } else {
            $isRefund = stripos($status, 'refund');
            if ($isRefund) {
                $txStatus = 'set for refund';
            } else {
                $isProcess = stripos($status, 'process');
                if ($isProcess) {
                    $txStatus = 'Pending';
                } else {
                    $isNotDone = stripos($status, 'not');
                    if ($isNotDone) {
                        $txStatus = 'not initiated';
                    } else {
                        $txStatus = 'Failed';
                    }
                }
            }
        }

        return $txStatus;
    }
}
