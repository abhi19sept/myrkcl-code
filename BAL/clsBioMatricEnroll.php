<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of clsApplyEoi
     *
     *  author Abhishek
     */
	ini_set("memory_limit", "-1");
	ini_set("max_execution_time", 0);
	ini_set("max_input_vars", 10000);
	set_time_limit(0);



    require 'DAL/classconnectionNEW.php';
    require 'DAL/sendsms.php';

    $_ObjConnection = new _Connection();
    $_Response = array();
    $_Response2 = array();

    class clsBioMatricEnroll
    {

        //put your code here

        public function GetMobileNumber($code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
                $_SelectQuery = "SELECT Admission_Mobile FROM tbl_admission WHERE Admission_LearnerCode = '" . $code . "' ";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetAllCourse()
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
               //  $_SelectQuery = "SELECT Course_Code,Course_Name FROM tbl_course_master  where Course_Code not in ('3','24')";               
			   $_SelectQuery = "SELECT Course_Code,Course_Name FROM tbl_course_master";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function SelectedCourse($_CourseCode)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
                $_SelectQuery = "SELECT Course_Name FROM tbl_course_master WHERE Course_Code='" . $_CourseCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function Batch($course)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
           //     $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $course . "' 
			//	and Course_Code not in ('3','24') order by Batch_Code DESC";
				
				 $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $course . "' 
				 order by Batch_Code DESC";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function SelectedBatch($_BatchCode)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_BatchCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchCode);
                $_SelectQuery = "SELECT DISTINCT Batch_Name FROM tbl_batch_master WHERE Batch_Code = '" . $_BatchCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetAllLearner($batch, $course)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
                $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
                $_SelectQuery = "SELECT Admission_Code, Admission_LearnerCode, Admission_Name, Admission_Batch, Admission_Fname, Admission_DOB, Admission_Photo, Admission_Sign
								FROM tbl_admission WHERE  Admission_Batch = '" . $batch . "' AND  Admission_Course = '" . $course . "'  AND  Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND BioMatric_Status='0' AND Admission_Payment_Status='1'";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetLearnerDetails($code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
                $_SelectQuery = "SELECT a.Admission_Code, a.Admission_LearnerCode, a.Admission_Name, a.Admission_Fname, a.Admission_DOB, a.Admission_Photo, a.Admission_Sign, b.Course_Name,c.Batch_Name
								FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch = c.Batch_Code WHERE  a.Admission_Code = '" . $code . "' AND Admission_Payment_Status='1'";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function AddLearnerDetails($txtLCode, $txtLName, $txtLFname, $txtLDOB, $txtLPhoto, $txtLCourse, $txtLBatch, $txtQuality, $txtNFIQ, $txtIsoTemplate, $txtIsoImage, $txtRawData, $txtWsqData, $admission_code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                
                $admission_code = mysqli_real_escape_string($_ObjConnection->Connect(),$admission_code);
                $txtLCode = mysqli_real_escape_string($_ObjConnection->Connect(),$txtLCode);
                
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                    $_ITGK_Code = $_SESSION['User_LoginId'];
                    $_Rsp_Code = $_SESSION['User_Rsp'];

                    //$_SelectbiopinQuery = "SELECT BioMatric_Admission_BioPIN as biopin FROM tbl_biomatric_registration WHERE BioMatric_ITGK_Code='" . $_ITGK_Code . "' ORDER BY BioMatric_Admission_BioPIN DESC LIMIT 1";

                    $_SelectbiopinQuery = "SELECT IFNULL(MAX(BioMatric_Admission_BioPIN),0) AS biopin FROM tbl_biomatric_registration WHERE BioMatric_ITGK_Code='" . $_ITGK_Code . "'";

                    $_Response2 = $_ObjConnection->ExecuteQuery($_SelectbiopinQuery, Message::SelectStatement);
                    //print_r($_Response2[0]);

                    $r = mysqli_fetch_array($_Response2[2]);

                    if ($r["biopin"] == 0) {
                        $biopin = '1000';
                    } else {
                        $biopin = $r['biopin'] + 1;
                    }

                    /* if ($_Response2[0] == Message::NoRecordFound) {
                         $biopin = '1000';
                     } else {
                         $_Row1 = mysqli_fetch_array($_Response2[2]);
                         $biopin = $_Row1['biopin'] + 1;
     //                    echo $biopin;
                     }*/
                    if ($txtIsoTemplate == '') {
                        echo "Finger not captured properly. Please try again.";
                        return;
                    } else {

                        $_InsertQuery = "INSERT INTO tbl_biomatric_registration(BioMatric_Code,BioMatric_ITGK_Code,BioMatric_Admission_Code,BioMatric_Admission_Course,BioMatric_Admission_Batch,BioMatric_Quality,"
                            . "BioMatric_NFIQ,BioMatric_ISO_Template,BioMatric_ISO_Image,BioMatric_Raw_Data,BioMatric_WSQ_Image,BioMatric_Admission_BioPIN, "
                            . "BioMatric_Admission_LCode,BioMatric_Admission_Name,BioMatric_Admission_Fname,BioMatric_Admission_DOB,BioMatric_Admission_Photo, "
                            . "BioMatric_Admission_RspName) "
                            . "SELECT CASE WHEN Max(BioMatric_Code) IS NULL THEN 1 ELSE Max(BioMatric_Code)+1 END AS BioMatric_Code,"
                            . "'" . $_ITGK_Code . "' AS BioMatric_ITGK_Code,'" . $admission_code . "' AS BioMatric_Admission_Code,"
                            . "'" . $txtLCourse . "' AS BioMatric_Admission_Course,'" . $txtLBatch . "' AS BioMatric_Admission_Batch,'" . $txtQuality . "' AS BioMatric_Quality,'" . $txtNFIQ . "' AS BioMatric_NFIQ,"
                            . "'" . $txtIsoTemplate . "' AS BioMatric_ISO_Template,'" . $txtIsoImage . "' AS BioMatric_ISO_Image,'" . $txtRawData . "' AS BioMatric_Raw_Data,'" . $txtWsqData . "' AS BioMatric_WSQ_Image,"
                            . "'" . $biopin . "'AS BioMatric_Admission_BioPIN,"
                            . "'" . $txtLCode . "' AS BioMatric_Admission_LCode,'" . $txtLName . "' AS BioMatric_Admission_Name,"
                            . "'" . $txtLFname . "' AS BioMatric_Admission_Fname,'" . $txtLDOB . "' AS BioMatric_Admission_DOB,'" . $txtLPhoto . "' AS BioMatric_Admission_Photo, "
                            . "'" . $_Rsp_Code . "' AS BioMatric_Admission_RspName "
                            . " FROM tbl_biomatric_registration";

                        $_DuplicateQuery = "SELECT * FROM tbl_biomatric_registration WHERE BioMatric_Admission_Code='" . $admission_code . "'";
                        $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                        if ($_Response[0] == Message::NoRecordFound) {


                            $_UpdateQuery = "UPDATE tbl_admission SET BioMatric_Status='1' WHERE Admission_Code = '" . $admission_code . "' AND Admission_LearnerCode = '" . $txtLCode . "' AND Admission_ITGK_Code = '" . $_ITGK_Code . "' ";
                            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

                            //$affected_rows = mysqli_affected_rows();

                     if ($_Response1[0] == Message::SuccessfullyUpdate) {
                                /*** IF ROWS AFFECTED THEN EXECUTE THIS // ELSE SEND ERROR MESSAGE ***/
                                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

                                if($_Response[0] == "Successfully Inserted" && $_Response[1] == 1){
                                    $_Mobile = $this->GetMobileNumber($txtLCode);
                                    $row_mobile = mysqli_fetch_array($_Mobile[2]);
                                    $_SMS = "Dear " . $txtLName . ", your bio-pin for attendance is " . $biopin;
                                    SendSMS($row_mobile["Admission_Mobile"], $_SMS);
                                } else {
                                    $_UpdateQuery_deregister = "UPDATE tbl_admission SET BioMatric_Status='0' WHERE Admission_Code = '" . $admission_code . "' AND Admission_LearnerCode = '" .
                                        $txtLCode . "' AND Admission_ITGK_Code = '" . $_ITGK_Code . "' ";
                                    $_Response_deregister = $_ObjConnection->ExecuteQuery($_UpdateQuery_deregister, Message::UpdateStatement);
                                    $_Response[0] = "Execution error occurred while inserting records, please try again";
                                    $_Response[1] = Message::Error;
                                }
                            } else {
                                $_UpdateQuery_deregister = "UPDATE tbl_admission SET BioMatric_Status='0' WHERE Admission_Code = '" . $admission_code . "' AND Admission_LearnerCode = '" .
                                    $txtLCode . "' AND Admission_ITGK_Code = '" . $_ITGK_Code . "' ";
                                $_Response_deregister = $_ObjConnection->ExecuteQuery($_UpdateQuery_deregister, Message::UpdateStatement);
                                $_Response[0] = "Execution timeout, please try again";
                                $_Response[1] = Message::Error;
                            }

                        } else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
                    }
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_e) {
                $_Response[0] = $_e->getTraceAsString();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetAllRegisterLearner($batch, $course)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
                $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
                
                $_SelectQuery = "SELECT a.Admission_Code, a.Admission_LearnerCode, a.Admission_Name, a.Admission_Batch, a.Admission_Fname, a.Admission_DOB, a.Admission_Photo, a.Admission_Sign, b.BioMatric_Admission_Code
								FROM tbl_admission AS a INNER JOIN tbl_biomatric_registration AS b ON a.Admission_Code = b.BioMatric_Admission_Code WHERE  a.Admission_Batch = '" . $batch . "' AND  a.Admission_Course = '" . $course . "' AND b.BioMatric_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetVerifyLearnerDetails($code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
                
                $_SelectQuery = "SELECT a.Admission_Code, a.Admission_Name, a.Admission_Fname, a.Admission_DOB, a.Admission_Photo, a.Admission_Sign, b.Course_Name,c.Batch_Name,
							d.BioMatric_Quality,d.BioMatric_NFIQ,d.BioMatric_ISO_Template,d.BioMatric_ISO_Image,d.BioMatric_Raw_Data,d.BioMatric_WSQ_Image
							FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c 
							ON a.Admission_Batch = c.Batch_Code INNER JOIN tbl_biomatric_registration AS d ON a.Admission_Code = d.BioMatric_Admission_Code WHERE  a.Admission_Code = '" . $code . "'";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function LearnerIn($txtQuality, $txtNFIQ, $admission_code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_ITGK_Code = $_SESSION['User_LoginId'];
                $_InsertQuery = "INSERT INTO tbl_learner_attendance(Attendance_Code,Attendance_ITGK_Code,Attendance_Admission_Code,Attendance_Quality,"
                    . "Attendance_NFIQ,Attendance_In_Time) "
                    . "SELECT CASE WHEN Max(Attendance_Code) IS NULL THEN 1 ELSE Max(Attendance_Code)+1 END AS Attendance_Code,"
                    . "'" . $_ITGK_Code . "' AS Attendance_ITGK_Code,'" . $admission_code . "' AS Attendance_Admission_Code,"
                    . "'" . $txtQuality . "' AS Attendance_Quality,'" . $txtNFIQ . "' AS Attendance_NFIQ,"
                    . "  CURRENT_TIMESTAMP AS Attendance_In_Time"
                    . " FROM tbl_learner_attendance";

                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } catch (Exception $_e) {
                $_Response[0] = $_e->getTraceAsString();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetMaxAttendanceCode($admission_code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $admission_code = mysqli_real_escape_string($_ObjConnection->Connect(),$admission_code);
                $_SelectQuery = "SELECT Max(Attendance_Code) AS Code, MAX(Attendance_In_Time) as Attendance_In_Time FROM tbl_learner_attendance WHERE Attendance_Admission_Code = '" . $admission_code .
                    "'";
                //echo $_SelectQuery;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_e) {
                $_Response[0] = $_e->getTraceAsString();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function LearnerOut($txtQuality, $txtNFIQ, $admission_code, $attendance_code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $attendance_code = mysqli_real_escape_string($_ObjConnection->Connect(),$attendance_code);
                $_ITGK_Code = $_SESSION['User_LoginId'];
                $_UpdateQuery = "UPDATE tbl_learner_attendance SET Attendance_Out_Time=CURRENT_TIMESTAMP WHERE Attendance_Code = '" . $attendance_code . "' AND Attendance_ITGK_Code = '" . $_ITGK_Code . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } catch (Exception $_e) {
                $_Response[0] = $_e->getTraceAsString();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetExistingLearnerData($_Course, $_Batch)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
                $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
                $_SelectLastBatches = "Select Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '".$_Course."' ORDER BY Batch_Code DESC LIMIT 4";
                $_Response2=$_ObjConnection->ExecuteQuery($_SelectLastBatches, Message::SelectStatement);
               
                $batches ="";
                while ($_Row = mysqli_fetch_array($_Response2[2])) {
                            $batches .= $_Row['Batch_Code'].",";
                         
                    
                }
                $batches = rtrim($batches, ',');
                $_SelectQuery = "SELECT BioMatric_ISO_Template FROM tbl_biomatric_registration WHERE BioMatric_Admission_Course = '" . $_Course . "' AND 
                BioMatric_Admission_Batch in ($batches) AND BioMatric_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";

                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetLearnerCourse($code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
                $_SelectQuery = "SELECT Admission_Course FROM tbl_admission WHERE Admission_Code = '" . $code . "'";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetLearnerBatch($code)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
                $_SelectQuery = "SELECT Admission_Batch FROM tbl_admission WHERE Admission_Code = '" . $code . "'";
                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetRegisterDeviceInfo()
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_ITGK_Code = $_SESSION['User_LoginId'];
                $_SelectQuery = "SELECT * FROM tbl_biomatric_register WHERE BioMatric_ITGK_Code = '" . $_ITGK_Code . "' AND BioMatric_Flag = '1'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetLearnerByPIN($biopin)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $biopin = mysqli_real_escape_string($_ObjConnection->Connect(),$biopin);
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                    $_ITGK_Code = $_SESSION['User_LoginId'];

               //     $_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name FROM tbl_biomatric_registration AS a INNER JOIN tbl_course_master AS b 
//ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.BioMatric_Admission_Batch = c.Batch_Code 
//WHERE BioMatric_Admission_BioPIN='" . $biopin . "' AND BioMatric_ITGK_Code='" . $_ITGK_Code . "'";

 $_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name FROM tbl_biomatric_registration AS a INNER JOIN tbl_course_master AS b 
ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.BioMatric_Admission_Batch = c.Batch_Code inner join tbl_admission as d on a.BioMatric_Admission_Code=d.admission_code   
WHERE BioMatric_Admission_BioPIN='" . $biopin . "' AND BioMatric_ITGK_Code='" . $_ITGK_Code . "' and d.Admission_Payment_Status='1'";

                    //$_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name FROM tbl_biomatric_registration as a INNER JOIN tbl_course_master as b ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master as c ON a.BioMatric_Admission_Batch = c.Batch_Code WHERE BioMatric_Admission_BioPIN='" . $biopin . "'";

                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function GetLearnerByLID($lcode)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                    $_ITGK_Code = $_SESSION['User_LoginId'];

                    if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 28) {
                        $_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name FROM tbl_biomatric_registration AS a INNER JOIN tbl_course_master AS b ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.BioMatric_Admission_Batch = c.Batch_Code WHERE BioMatric_Admission_LCode='" . $lcode . "'";
                    } else if ($_SESSION['User_UserRoll'] == 7) {
                        $_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name FROM tbl_biomatric_registration AS a INNER JOIN tbl_course_master AS b ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.BioMatric_Admission_Batch = c.Batch_Code WHERE BioMatric_Admission_LCode='" . $lcode . "' AND BioMatric_ITGK_Code='" . $_ITGK_Code . "'";
                    }
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    if ($_Response[0] == "No Record Found" && $_SESSION['User_UserRoll'] != 7) {
                        $_SelectQuery = "SELECT 'NA' as BioMatric_Quality, 'NA' as BioMatric_NFIQ, 'NA' as BioMatric_ISO_Template, "
                            . "'NA' as BioMatric_ISO_Image,a.Admission_ITGK_Code as BioMatric_ITGK_Code,a.Admission_LearnerCode as"
                            . " BioMatric_Admission_LCode,a.Admission_Name as BioMatric_Admission_Name,"
                            . "a.Admission_Fname as BioMatric_Admission_Fname, a.Admission_DOB as BioMatric_Admission_DOB,"
                            . "a.Admission_Photo as BioMatric_Admission_Photo,a.Admission_Code as BioMatric_Admission_Code, b.Course_Name, c.Batch_Name FROM tbl_admission as a"
                            . " INNER JOIN tbl_course_master as b ON a.Admission_Course = b.Course_Code INNER JOIN tbl_batch_master as c ON"
                            . " a.Admission_Batch = c.Batch_Code WHERE Admission_LearnerCode='" . $lcode . "'";
                        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    }
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

        public function LearnerDeregister($lcodes)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {

                $lcodes = mysqli_real_escape_string($_ObjConnection->Connect(),$lcodes);
                $_DeleteQuery = "DELETE FROM tbl_biomatric_registration WHERE BioMatric_Admission_LCode='" . $lcodes . "'";

                $_Response1 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);

                $_UpdateQuery = "UPDATE tbl_admission SET BioMatric_Status='0' WHERE Admission_LearnerCode='" . $lcodes . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
		
		
public function UpdAdmLogLearnerDeregister($lcodes, $OldIso_template, $itgkcode , $num_rowsSco, $deregreason, $attnd ) {
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_Upd_User_Code = $_SESSION['User_LoginId'];
				$_InsertQuery = "INSERT INTO tbl_adm_data_log (Adm_Data_Log_Lcode,Adm_Data_Log_Oldisotemplate,Adm_Data_Log_Upduserloginid,Adm_Data_Log_ITGK, Adm_Data_Log_LMSscore, Adm_Data_Log_Reason, Adm_Data_Log_Attendance) "
						. "values ('" . $lcodes . "','" . $OldIso_template . "','" . $_Upd_User_Code . "', '" . $itgkcode . "', '" . $num_rowsSco . "', '" . $deregreason . "', '".$attnd."')";

				$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			} catch (Exception $_ex) {

				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;
			}
			return $_Response;
		}

        public function GetAllMachines()
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_SelectQuery = "SELECT DISTINCT(BioMatric_Make), BioMatric_Register_Code, BioMatric_Model, BioMatric_SerailNo FROM tbl_biomatric_register WHERE BioMatric_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND BioMatric_Flag = '1'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
		
        public function CheckCaptureEvent($coursecode,$batchcode)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$coursecode);
                $batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
                $_SelectQuery = "SELECT Event_LearnerAttendance from tbl_event_management where Event_Course='".$coursecode."'
				 and Event_Batch='".$batchcode."' and Event_Category='10' and Event_Name='13' and Event_LearnerAttendance='1'
				 AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
		

        public function GetRptByLearner($_Batch, $_Course)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
                $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
                $_SelectQuery = "SELECT Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_Mobile,Adm_Data_Log_ITGK,
count(a.Adm_Data_Log_Lcode) as ct,Adm_Data_Log_Reason, Adm_Data_Log_LMSscore ,Adm_Data_Log_LMSscore,Adm_Data_Log_Reason,Adm_Data_Log_Attendance FROM tbl_adm_data_log as a inner join
 tbl_admission as b on a.Adm_Data_Log_Lcode=b.Admission_LearnerCode WHERE Admission_Course = '" . $_Course . "' AND 
Admission_Batch = '" . $_Batch . "' AND Admission_Payment_Status = '1' group by a.Adm_Data_Log_Lcode
order by Admission_ITGK_Code";

                //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
                //WHERE b.Course_Name = '" . $course . "'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

      public function GetRptByITGK($_Batch, $_Course)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
                $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
//                 $_SelectQuery = "SELECT Adm_Data_Log_ITGK,Adm_Data_Log_LMSscore,Adm_Data_Log_Reason, c.*, count(a.Adm_Data_Log_Lcode) as ct FROM tbl_adm_data_log as a inner join 
// tbl_admission as b on a.Adm_Data_Log_Lcode=b.Admission_LearnerCode inner join 
// vw_itgkname_distict_rsp as c on a.Adm_Data_Log_ITGK=c.ITGKCODE WHERE Admission_Course = '" . $_Course . "'
// AND Admission_Batch = '" . $_Batch . "' AND Admission_Payment_Status = '1' group by a.Adm_Data_Log_ITGK 
// order by Admission_ITGK_Code ";
            $_SelectQuery =  "Select Adm_Data_Log_ITGK,Adm_Data_Log_LMSscore,Adm_Data_Log_Reason, ct, tct, ITGK_Name,ITGKMOBILE, RSP_Name 
            from (SELECT Adm_Data_Log_ITGK,Adm_Data_Log_LMSscore,Adm_Data_Log_Reason, count(a.Adm_Data_Log_Lcode) as ct, 
            c.RSP_Name, c.ITGK_Name, c.ITGKMOBILE FROM tbl_adm_data_log as a right join tbl_admission as b on a.Adm_Data_Log_Lcode=b.Admission_LearnerCode 
            inner join vw_itgkname_distict_rsp as c on a.Adm_Data_Log_ITGK=c.ITGKCODE 
            WHERE Admission_Course = '" . $_Course . "' AND Admission_Batch = '" . $_Batch . "' AND Admission_Payment_Status = '1'
             group by a.Adm_Data_Log_ITGK order by Adm_Data_Log_ITGK) as q1 Inner JOIN (SELECT Admission_ITGK_Code, 
            count(Admission_LearnerCode) as tct  FROM tbl_admission WHERE Admission_Course = '" . $_Course . "'  AND Admission_Batch = '" . $_Batch . "'
             AND  Admission_Payment_Status = '1'  group by Admission_ITGK_Code order by Admission_ITGK_Code) as q2 on
             q1.Adm_Data_Log_ITGK=q2.Admission_ITGK_Code";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
    public function GetAttendanceCode($admission_code, $itgkcode)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $admission_code = mysqli_real_escape_string($_ObjConnection->Connect(),$admission_code);
                $itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkcode);
                //$_SelectQuery = "select * from vw_learner_attendance where Attendance_Admission_Code='" . $admission_code ."'";
                //echo $_SelectQuery;

                $_SelectQuery = "select count(t) as Days from 
(select distinct date(Attendance_In_Time) as t, Attendance_Admission_Code  
from tbl_learner_attendance where Attendance_Admission_Code='".$admission_code."' and
Attendance_ITGK_Code='".$itgkcode."') as tb 
 group by Attendance_Admission_Code";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_e) {
                $_Response[0] = $_e->getTraceAsString();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
    
    public function GetAllCourseRpt()
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_SelectQuery = "Select distinct a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' and Course_Type!='6' ORDER BY `CourseITGK_StartDate` ASC";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
    public function BatchRpt($course)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
                $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $course . "' and Batch_Code>='168' order by Batch_Code DESC";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }

}
