<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsUpdateLocationDetails
 *
 * @author VIVEK
 */

//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUpdateLocationDetails {
    //put your code here
    
     public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8') {
					
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
                    $_SelectQuery = "SELECT a.Organization_Code, a.Organization_Name, a.Organization_District, a.Organization_Tehsil, a.Organization_AreaType, b.District_Name, c.Tehsil_Name
                                    from tbl_organization_detail as a INNER JOIN tbl_district_master as b on a.Organization_District=b.District_Code
                                    INNER JOIN tbl_tehsil_master as c on a.Organization_Tehsil=c.Tehsil_Code
                                    INNER JOIN tbl_user_master as d on a.Organization_User=d.User_Code where d.User_LoginId='" . $_CenterCode . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    echo "You are not Auhorized to use this functionality on MYRKCL.";
                    return;
                }
                
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_Tehsil, $_AreaType, $_OrgCode) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {

            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					
					$_Tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil);
					$_AreaType = mysqli_real_escape_string($_ObjConnection->Connect(),$_AreaType);
					$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
					
                $_SelectQuery = "SELECT Organization_Name, Organization_Tehsil, Organization_AreaType
                                 from tbl_organization_detail where Organization_Code='" . $_OrgCode . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                
                $_Row = mysqli_fetch_array($_Response1[2]);
                $_OrgName = $_Row['Organization_Name'];
                $_OrgOldTehsil = $_Row['Organization_Tehsil'];
                $_OrgOldAreaType = $_Row['Organization_AreaType'];
                
                date_default_timezone_set('Asia/Calcutta');
                $_Date = date("Y-m-d h:i:s");
                
                $_InsertQuery = "Insert Into tbl_locationupdate_log(LocationUpdate_OrgCode,LocationUpdate_OrgName,LocationUpdate_TehsilCode,"
                    . " LocationUpdate_AreaType,LocationUpdate_Date,LocationUpdate_LoginId)"
                    . "VALUES ('" . $_OrgCode . "','" . $_OrgName . "','" . $_OrgOldTehsil . "','" . $_OrgOldAreaType . "',
								'" . $_Date . "','".$_SESSION['User_LoginId']."')";
            //echo $_InsertQuery;
                $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                
                $_UpdateQuery = "Update tbl_organization_detail set Organization_Tehsil='" . $_Tehsil . "', 
									Organization_AreaType='" . $_AreaType . "' WHERE Organization_Code = '" . $_OrgCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
              
                        
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
}
