<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsApplyEoi
 *
 *  author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response1 = array();

class clsApplyEoi1 extends paymentFunctions {

    //put your code here

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select Distinct a.EOI_Code,b.EOI_Name "
                    . " From tbl_eoi_centerlist as a inner join tbl_eoi_master as b "
                    . "on a.EOI_Code=b.EOI_Code INNER JOIN tbl_event_management as c "
                    . "on b.EOI_Code = c.Event_CourseEOI "
                    . "WHERE a.EOI_ECL = '" . $_SESSION['User_LoginId'] . "'"
                    . " AND c.Event_Name='4' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
            //print_r($_SelectQuery);
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetEOI($eoicode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $eoicode = mysqli_real_escape_string($_ObjConnection->Connect(),$eoicode);
            $_SelectQuery = "Select EOI_Code, EOI_Name From tbl_eoi_master WHERE EOI_Code = '" . $eoicode . "'";
            //print_r($_SelectQuery);
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetEOICourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select distinct a.Courseitgk_Course,a.Courseitgk_EOI,b.Course_Name,b.Course_Code From tbl_courseitgk_mapping AS a
 INNER JOIN tbl_course_master AS b ON a.Courseitgk_Course = b.Course_Name
inner join tbl_event_management as c
on a.Courseitgk_EOI=c.Event_CourseEOI
 WHERE a.EOI_Fee_Confirm = 0 and  NOW() >= Event_Startdate AND NOW() <= Event_Enddate
 AND a.Courseitgk_ITGK = '" . $_SESSION['User_LoginId'] . "'";
 
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetEOICourseCode($code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
            $_SelectQuery = "Select a.Courseitgk_Course,b.Course_Name,b.Course_Code From tbl_courseitgk_mapping AS a INNER JOIN 
			 tbl_course_master AS b ON a.Courseitgk_Course = b.Course_Name WHERE a.EOI_Fee_Confirm = 0 AND a.Courseitgk_EOI ='" . $code . "'
			 AND a.Courseitgk_ITGK = '" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

   public function Add($_Course, $_CourseCode, $_EOICode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_EOICode = mysqli_real_escape_string($_ObjConnection->Connect(),$_EOICode);
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select * From tbl_eoi_master Where EOI_Code = '" . $_EOICode . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            $_Row2 = mysqli_fetch_array($_Response1[2]);
            $cfee = $_Row2['EOI_ProcessingFee'];
            $pfee = $_Row2['EOI_ProcessingFee'];

            $_EOIAmount = $cfee + $pfee;
            $_ITGKCode = $_SESSION['User_LoginId'];
            $_EOIStartDate = $_Row2['EOI_StartDate'];
            $_EOIEndDate = $_Row2['EOI_EndDate'];
            $_CourseCode2 = $_Row2['EOI_Course'];


            $_SelectQuery2 = "Select User_CreatedDate,User_FinalApprovalDate From tbl_user_master Where User_LoginId = '" . $_ITGKCode . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);

            $_Row3 = mysqli_fetch_array($_Response2[2]);
            $Created_Date = date_format(date_create($_Row3['User_CreatedDate']), "Y-m-d");
            $Approval_Date = date_format(date_create($_Row3['User_FinalApprovalDate']), "Y-m-d");

            $_SelectQuery3 = "Select Course_Name from tbl_course_master Where Course_Code = '" . $_CourseCode2 . "'";
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);

            $_Row4 = mysqli_fetch_array($_Response3[2]);
            $_CourseNamebyQuery = trim($_Row4['Course_Name']);

            date_default_timezone_set('Asia/Kolkata');
            $_SDate = date("Y-m-d");
            $a = date("Y-m-d", strtotime(date("Y-m-d", strtotime($Created_Date)) . " + 1 year" . " + 1 month"));
            $r = explode("-", $a);
            $year = $r[0];
            $month = $r[1];
           // $expire_date = date("$year-$month-05");
		   	$isRSCfa = stripos($_CourseNamebyQuery, 'cfa');
		   	//$isRSCee = stripos($_CourseNamebyQuery, 'cee');
				if($_CourseNamebyQuery=='RS-CEE'){
					$expire_date='2021-08-31';
				}
				else if($_CourseNamebyQuery=='RS-CIT SPMRM'){
					$expire_date='2021-12-31';
				}
				
				
				else{
					$expire_date = ($isRSCfa === false) ? date("$year-$month-10") : '2022-09-30';
				}
            

            $_CourseName = trim($_Course);
			if($_EOICode == '13'){
				$_InsertQuery = "Insert Into tbl_courseitgk_mapping(Courseitgk_Code,Courseitgk_Course,Courseitgk_ITGK,Courseitgk_EOI,"
                        . "CourseITGK_BlockStatus,CourseITGK_StartDate,CourseITGK_ExpireDate,CourseITGK_UserCreatedDate,"
                        . "CourseITGK_UserFinalApprovalDate) "
                        . "Select Case When Max(Courseitgk_Code) Is Null Then 1 Else Max(Courseitgk_Code)+1 End as Courseitgk_Code,"
                        . "'" . $_CourseNamebyQuery . "' as Courseitgk_Course,'" . $_ITGKCode . "' as Courseitgk_ITGK,"
                        . "'" . $_EOICode . "' as Courseitgk_EOI,'unblock' as CourseITGK_BlockStatus,"
                        . "'" . $_SDate . "' as CourseITGK_StartDate,'" . $expire_date . "' as CourseITGK_ExpireDate,"
                        . "'" . $Created_Date . "' as CourseITGK_UserCreatedDate,'" . $Approval_Date . "' as CourseITGK_UserFinalApprovalDate"
                        . " From tbl_courseitgk_mapping";
			}
			else {
				if ($_EOIAmount == 0) {
                $_InsertQuery = "Insert Into tbl_courseitgk_mapping(Courseitgk_Code,Courseitgk_Course,Courseitgk_ITGK,Courseitgk_EOI,"
                        . "CourseITGK_BlockStatus,CourseITGK_StartDate,CourseITGK_ExpireDate,CourseITGK_UserCreatedDate,"
                        . "CourseITGK_UserFinalApprovalDate,EOI_Fee_Confirm,Courseitgk_TranRefNo) "
                        . "Select Case When Max(Courseitgk_Code) Is Null Then 1 Else Max(Courseitgk_Code)+1 End as Courseitgk_Code,"
                        . "'" . $_CourseNamebyQuery . "' as Courseitgk_Course,'" . $_ITGKCode . "' as Courseitgk_ITGK,"
                        . "'" . $_EOICode . "' as Courseitgk_EOI,'unblock' as CourseITGK_BlockStatus,"
                        . "'" . $_SDate . "' as CourseITGK_StartDate,'" . $expire_date . "' as CourseITGK_ExpireDate,"
                        . "'" . $Created_Date . "' as CourseITGK_UserCreatedDate,'" . $Approval_Date . "' as CourseITGK_UserFinalApprovalDate,"
                        . "'1' as EOI_Fee_Confirm,'PayByWithoutFee' as Courseitgk_TranRefNo"
                        . " From tbl_courseitgk_mapping";
            } else {
                $_InsertQuery = "Insert Into tbl_courseitgk_mapping(Courseitgk_Code,Courseitgk_Course,Courseitgk_ITGK,Courseitgk_EOI,"
                        . "CourseITGK_BlockStatus,CourseITGK_StartDate,CourseITGK_ExpireDate,CourseITGK_UserCreatedDate,"
                        . "CourseITGK_UserFinalApprovalDate) "
                        . "Select Case When Max(Courseitgk_Code) Is Null Then 1 Else Max(Courseitgk_Code)+1 End as Courseitgk_Code,"
                        . "'" . $_CourseNamebyQuery . "' as Courseitgk_Course,'" . $_ITGKCode . "' as Courseitgk_ITGK,"
                        . "'" . $_EOICode . "' as Courseitgk_EOI,'unblock' as CourseITGK_BlockStatus,"
                        . "'" . $_SDate . "' as CourseITGK_StartDate,'" . $expire_date . "' as CourseITGK_ExpireDate,"
                        . "'" . $Created_Date . "' as CourseITGK_UserCreatedDate,'" . $Approval_Date . "' as CourseITGK_UserFinalApprovalDate"
                        . " From tbl_courseitgk_mapping";
				}
			}
            



            $_DuplicateQuery = "Select * From tbl_courseitgk_mapping Where Courseitgk_Course='" . $_CourseName . "' AND Courseitgk_ITGK = '" . $_ITGKCode . "' AND Courseitgk_EOI='" . $_EOICode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

            if ($_Response[0] == Message::NoRecordFound) {
                if (isset($_CourseName) && !empty($_CourseName) && $_CourseCode2 == $_CourseCode && $_CourseNamebyQuery == $_CourseName) {
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                } else {
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                $_Response[0] = "You have Already Applied for this EOI"; //Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        }
            else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "index.php";</script> 
                        <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


    public function GetDatabyCode($_Code, $_Paymode, $_EOICode) {
        //echo $_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            $_Paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Paymode);
            $_EOICode = mysqli_real_escape_string($_ObjConnection->Connect(),$_EOICode);
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK = $_SESSION['User_LoginId'];
                $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_CourseEOI = '" . $_EOICode . "' AND Event_Payment = '" . $_Paymode . "' AND Event_Name = '5' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
                if ($_getEvent['Event_Payment'] == '1') {
                    $_SelectQuery = "Select * From tbl_eoi_master Where EOI_Code = '" . $_Code . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_getEvent['Event_Payment'] == '2') {
                    $_SelectQuery = "Select * From tbl_eoi_master Where EOI_Code = '" . $_Code . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function initiateEOIPayTran($eoi, $postValues) {
        $return = 0;
        try {
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $payFor = 'EOI Fee Payment';
                $return = parent::insertPaymentTransaction($payFor, $_SESSION['User_LoginId'], ($eoi['PFees'] + $eoi['CFees']), [$eoi['Ecode']], $postValues['values'], $eoi['Ecode']);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }

    public function GETEOIDetails($_Code) {
        //echo $_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            
            $_SelectQuery = "Select * From tbl_eoi_master Where EOI_Code = '" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetEOIbycourse($_CourseCode) {
        //echo $_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            // $_SelectQuery = "Select a.EOI_Course, b.Course_Name From tbl_eoi_master AS a INNER JOIN tbl_course_master AS b ON a.EOI_Course = b.Course_Code WHERE a.EOI_Course = '" . $_CourseCode . "'";
            $_SelectQuery = "Select EOI_Code, EOI_Name From tbl_eoi_master WHERE EOI_Course = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
