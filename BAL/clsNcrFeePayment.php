<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrFeePayment extends paymentFunctions {

    //put your code here

    public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_SelectQuery = "Select Organization_Name,Organization_RegistrationNo,Organization_FoundedDate,Org_Type_Name,Organization_DocType,
							District_Name,Tehsil_Name,Organization_Road,Org_AreaType,Org_Ack from tbl_org_master as a inner join tbl_org_type_master as b 
							on a.Organization_Type=b.Org_Type_Code inner join tbl_district_master as c on a.Organization_District=c.District_Code
							inner join tbl_tehsil_master as d on a.Organization_Tehsil=d.Tehsil_Code inner join tbl_user_master as e
							on a.Org_Ack=e.User_Ack where User_LoginId='" . $_SESSION['User_LoginId'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetNcrAmt($_Type) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Type = mysqli_real_escape_string($_ObjConnection->Connect(),$_Type);
				
            $_SelectQuery = "Select Ncr_Fee_Amount FROM tbl_ncrfee_master WHERE Ncr_Fee_Status = 'Active' AND
						Ncr_Fee_Category='" . $_Type . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function CheckPayStatus($_Ack) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ack = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ack);
				
            $_SelectQuery = "Select Org_PayStatus,Org_AreaType FROM tbl_org_master WHERE Org_Ack = '" . $_Ack . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ChkPayEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Event_NCRPayment,Event_Startdate,Event_Enddate FROM tbl_event_management WHERE CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='12'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddNcrPayTran($amount, $productinfo, $postValues) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $amount, [$postValues['ack']], 0, 0);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }

    public function GetDetails($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
            $_SelectQuery = "Select b.Organization_Name,a.User_MobileNo From tbl_user_master as a, 
							tbl_organization_detail as b Where a.User_Code=b.Organization_User and
							a.User_Code ='" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
