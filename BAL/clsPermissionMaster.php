<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPermissionMaster
 *
 * @author Lalit
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
class clsPermissionMaster {
    //put your code here
    
    public function GetAllParent($_parent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_parent);
				
           $_SelectQuery = "Select * from tbl_parent_function_master WHERE Parent_Function_Root = '" . $_parent . "' Order by Parent_Function_Display";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetChildMenuUserRole($_UserRole,$_Parent)
    {
        global $_ObjConnection,$_Response;
        $_ObjConnection->Connect();
        try
        {
            $_UserRole = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserRole);
			$_Parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent);
			
            $_SelectQuery="Select * From vw_userrolewisefunction where "
                    . "UserRole='" . $_UserRole . "' and Parent='" . $_Parent . "' Order by Display";
           
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery,  Message::SelectStatement);
        }
        catch(Exception $_ex)
        {
            $_Response[0]=$_ex->getTraceAsString();
            $_Response[1]=  Message::Error;
        }
        return $_Response;
    }
	
	
    public function GetAllClild($_Parent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent);
			
            $_SelectQuery = "Select * from tbl_function_master where Function_Parent='" . $_Parent . "' Order by Function_Display";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Role_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Role_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role_Code);
			
            $_SelectQuery = "Select UserRoll_Code,UserRoll_Name,UserRoll_Status From tbl_userroll_master Where
								UserRoll_Code='" . $_Role_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
   public function DeleteRecord($_Role_Code,$_Root_Menu)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Role_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role_Code);
				$_Root_Menu = mysqli_real_escape_string($_ObjConnection->Connect(),$_Root_Menu);
				
            $_DeleteQuery = "Delete From tbl_Permission_Master Where Permission_UserRoll='" . $_Role_Code . "' and Permission_Function in (Select Function_Code From vw_rootparentchildmenu where Root_Menu_Code='" . $_Root_Menu . "')";
			//echo $_DeleteQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_UserPermissionArray) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery="";
            $_CountLength=count($_UserPermissionArray);
            for($i=0;$i<$_CountLength;$i++)
            {
            $_InsertQuery = "Insert Into tbl_permission_master(Permission_Code,Permission_UserRoll,Permission_Function,"
                    . "Permission_Attribute,Permission_Status) Select Case When Max(Permission_Code) Is Null Then "
                    . "1 Else Max(Permission_Code)+1 End as Permission_Code,'" . $_UserPermissionArray[$i]['UserRole'] . "' "
                    . "as Permission_UserRoll,'" . $_UserPermissionArray[$i]['Function'] . "' as Permission_Function,"
                    . "'" . $_UserPermissionArray[$i]['Attribute'] . "' as Permission_Attribute,"
                    . "'" . $_UserPermissionArray[$i]['Status'] . "' as Permission_Status "
                    . " From tbl_Permission_Master; ";
             $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
            }
                      
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_RoleCode,$_RoleName, $_RoleStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RoleCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleCode);
				$_RoleName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleName);
				$_RoleStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleStatus);
				
            $_UpdateQuery = "Update tbl_userroll_master set UserRoll_Name='" . $_RoleName . "',UserRoll_Status='" . $_RoleStatus . "' Where UserRoll_Code='" . $_RoleCode . "'";
            $_DuplicateQuery = "Select * From tbl_userroll_master Where UserRoll_Name='" . $_RoleName . "' and UserRoll_Code <> '" . $_RoleCode . "'";
            $_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }


}
