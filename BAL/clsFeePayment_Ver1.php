<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsFeePayment {

    //put your code here

    public function GetAll($batch, $course, $paymode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {   
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$paymode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK = $_SESSION['User_LoginId'];
                $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_Course = '" . $course . "' AND Event_Batch = '" . $batch . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '3' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
                if ($_getEvent['Event_Payment'] == '1') {
                    $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_Batch = '" . $batch . "' AND Admission_Course = '" . $course . "' And Admission_ITGK_Code='" . $_ITGK . "' And Admission_Payment_Status='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_getEvent['Event_Payment'] == '2') {
                    $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_Batch = '" . $batch . "' AND Admission_Course = '" . $course . "' And Admission_ITGK_Code='" . $_ITGK . "' And Admission_Payment_Status='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    //echo "Invalid User Input";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select RKCL_Share, Course_Fee, VMOU_Share FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }



    public function AddPayTran($batch, $course, $amount1, $_AdmissionCode, $txtGenerateId, $Lcount, $txtGeneratePayUId) {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_AdmissionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AdmissionCode);
            $txtGeneratePayUId = mysqli_real_escape_string($_ObjConnection->Connect(),$txtGeneratePayUId);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

                $_RKCL_Id = $txtGenerateId . '_RKCLtranid';

                $_ITGK_Code = $_SESSION['User_LoginId'];
                $_SelectQuery = "Select * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "'";            
				$_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_Response1[0]!='Success'){
						
						$_InsertQuery = "INSERT INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray,Pay_Tran_LCount,"
								. "Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_Course, Pay_Tran_Batch, Pay_Tran_PG_Trnid) "
								. "values('" . $_ITGK_Code . "' ,'" . $_RKCL_Id . "','" . $_AdmissionCode . "','" . $Lcount . "','" . $amount1 . "','PaymentInProcess','LearnerFeePayment','" . $course . "','" . $batch . "','" . $txtGeneratePayUId . "')";

						$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

						$_UpdateQuery = "Update tbl_admission set Admission_RKCL_Trnid = '" . $_RKCL_Id . "', Admission_TranRefNo='" . $txtGeneratePayUId . "' Where Admission_Code IN ($_AdmissionCode) AND Admission_Course='" . $course . "' AND Admission_Batch='" . $batch . "'";

						$_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                } else {
							$_UpdateQuery = "UPDATE tbl_payment_transaction SET 
								Pay_Tran_ITGK = '" . $_ITGK_Code . "', 
								Pay_Tran_RKCL_Trnid = '" . $_RKCL_Id . "', 
								Pay_Tran_AdmissionArray = '" . $_AdmissionCode . "',
								Pay_Tran_LCount = '" . $Lcount . "', 
								Pay_Tran_Amount = '" . $amount1 . "', 
								Pay_Tran_ProdInfo = 'LearnerFeePayment',
								Pay_Tran_Course = '" . $course . "',
								Pay_Tran_Batch = '" . $batch . "'
							WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "' AND Pay_Tran_Status = 'PaymentInProcess'";
							$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
							}        
                
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
	
	public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}