<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAddressChangeFinalApprovalRpt
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsAddressChangeFinalApprovalRpt {
    //put your code here
    
       public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "SELECT tcai.Organization_Name,
                            tcai.fld_remarks AS fld_remarks,                                   
                            tcai.Organization_Address AS Organization_Address, 
                            tcai.fld_ITGK_Code AS fld_ITGK_Code, 
                            tcai.fld_addedOn AS submission_date,
                            tcai.fld_spActionDate AS submission_date_sp,
                            tcai.fld_rkclActionDate AS submission_date_rkcl,
                            tcai.Organization_AreaType AS Organization_AreaType,                                  
                            tcai.Organization_Address_old AS Organization_Address_old,                                  
                            tcai.Organization_AreaType_old AS Organization_AreaType_old                                    
                            FROM tbl_change_address_itgk tcai                                   
                            WHERE fld_requestChangeType = 'address' and fld_status = '2' GROUP BY tcai.fld_ITGK_Code";
            } 
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
