<?php


/**
 * Description of clsAdmission
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsAdmission {
    //put your code here
    public function Add($_LearnerTitle,$_LearnerName,$_ParentTitle,$_ParentName,$_IdProof,$_IdNo,$_DOB,$_MotherTongue,$_Medium,$_Gender,$_MaritalStatus,
                        $_Area,$_HouseNo,$_StreetNo,$_Road,$_ResiPh,$_Mobile,$_Qualification,$_LearnerType,$_PhysicallyChallenged,$_Email,$_LearnerPhoto,$_LearnerSign) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "Insert Into tbl_userprofile(UserProfile_Code, UserProfile_User,"
                    . "UserProfile_Initial, UserProfile_FirstName,"
                    . "UserProfile_ParentName, UserProfile_ParentTitle,"
                    . "UserProfile_PanNo,UserProfile_DOB, UserProfile_MTongue,"
                    . "UserProfile_Medium,UserProfile_Gender, UserProfile_Marital,"
                    . "UserProfile_HouseNo, UserProfile_Street, UserProfile_Road, UserProfile_Sign,"
                    . "UserProfile_Area, UserProfile_Image,"
                    . "UserProfile_Qualification,UserProfile_LearnerType, UserProfile_PhysicallyChallenged,"
                    . "UserProfile_Mobile, UserProfile_PhoneNo) "
                    . "Select Case When Max(UserProfile_Code) Is Null Then 1 Else Max(UserProfile_Code)+1 End as UserProfile_Code, '1' as UserProfile_User,"
                    . "'" .$_LearnerTitle. "' as UserProfile_Initial,'" . $_LearnerName . "' as UserProfile_FirstName,"
                    . "'" .$_ParentName. "' as UserProfile_ParentName,'" .$_ParentTitle. "' as UserProfile_ParentTitle,"
                    . "'" .$_IdProof. "' as UserProfile_PanNo,'" .$_DOB. "' as UserProfile_DOB,'" .$_MotherTongue. "' as UserProfile_MTongue,"
                    . "'" .$_Medium. "' as UserProfile_Medium,'" .$_Gender. "' as UserProfile_Gender,'" .$_MaritalStatus. "' as UserProfile_Marital,"
                    
                    . "'" .$_HouseNo. "' as UserProfile_HouseNo,'" .$_StreetNo. "' as UserProfile_Street,"
                    . "'" .$_Road. "' as UserProfile_Road,'" .$_LearnerSign. "' as UserProfile_Sign,"
                    . "'" .$_Area. "' as UserProfile_Area,'" .$_LearnerPhoto. "' as UserProfile_Image,"
                    
                   
                    . "'" .$_Qualification. "' as UserProfile_Qualification,'" .$_LearnerType. "' as UserProfile_LearnerType,"
                    . "'" .$_PhysicallyChallenged. "' as UserProfile_PhysicallyChallenged,"
                    . "'" .$_Mobile. "' as UserProfile_Mobile,"
                    . "'" .$_ResiPh. "' as UserProfile_PhoneNo"
                    . " From tbl_userprofile";
            $_DuplicateQuery = "Select * From tbl_userprofile Where UserProfile_FirstName='" . $_LearnerName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        //print_r($_Response);
        return $_Response;
    }
    
    public function Update($_Code,$_LearnerTitle,$_LearnerName,$_ParentTitle,$_ParentName,$_IdProof,$_IdNo,$_DOB,
                           $_MotherTongue,$_Medium,$_Gender,$_MaritalStatus,$_Area,$_HouseNo,$_StreetNo,$_Road,
                           $_ResiPh,$_Mobile,$_Qualification,$_LearnerType,$_PhysicallyChallenged,$_Email,
                           $_LearnerPhoto,$_LearnerSign) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_userprofile set UserProfile_Initial='" . $_LearnerTitle . "',"
                    . "UserProfile_FirstName='" . $_LearnerName . "'," . "UserProfile_ParentTitle='" . $_ParentTitle . "',"
                    . "UserProfile_ParentName='" . $_ParentName . "'," . "UserProfile_PanNo='" . $_IdProof . "',"
                    . "UserProfile_DOB='" . $_DOB . "'," . "UserProfile_MTongue='" . $_MotherTongue . "',"
                    . "UserProfile_Medium='" . $_Medium . "'," . "UserProfile_Gender='" . $_Gender . "',"
                    . "UserProfile_Marital='" . $_MaritalStatus . "'," . "UserProfile_Area='" . $_Area . "',"
                    . "UserProfile_HouseNo='" . $_HouseNo . "'," . "UserProfile_Street='" . $_StreetNo . "',"
                    . "UserProfile_Road='" . $_Road . "'," . "UserProfile_PhoneNo='" . $_ResiPh . "',"
                    . "UserProfile_Mobile='" . $_Mobile . "'," . "UserProfile_Qualification='" . $_Qualification . "',"
                    . "UserProfile_LearnerType='" . $_LearnerType . "'," . "UserProfile_PhysicallyChallenged='" . $_PhysicallyChallenged . "',"
                    . "UserProfile_Image='" . $_LearnerPhoto . "',"
                    . "UserProfile_Sign='" . $_LearnerSign . "'"
                    . " Where UserProfile_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_userprofile Where UserProfile_FirstName='" . $_LearnerName . "' "
                    . "and UserProfile_Code <> '" . $_Code . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
     public function GetDatabyCode($_AdmissionCode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_userprofile Where UserProfile_Code='" . $_AdmissionCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
   
}
