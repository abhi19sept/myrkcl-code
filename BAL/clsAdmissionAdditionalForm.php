<?php

/**
 * Description of clsAdmissionAdditionalForm
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionAdditionalForm {

    //put your code here
    public function GetAllLearner($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(), $batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(), $course);

        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_SelectQuery = "";
                
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "Select a.Admission_Code,b.Admission_Code as abc, a.Admission_LearnerCode, Admission_Name, Admission_Batch, Admission_Fname,"
                        . " Admission_DOB, Admission_Photo, Admission_Sign FROM tbl_admission as a left join "
                        . "tbl_admission_spmrm_scheme as b on a.Admission_LearnerCode = b.Admission_LearnerCode "
                        . "WHERE Admission_Batch = '" . $batch . "' AND  Admission_Course = '26'  AND "
                        . " Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

     public function GetExistingLearnerDetails($_Cid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_Cid = mysqli_real_escape_string($_ObjConnection->Connect(), $_Cid);
        try {
            $_SelectQuery = "Select * FROM tbl_admission WHERE Admission_Code = '" . $_Cid . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
     public function update($_ddlCategory,$_ddlSpecialPref, $_LearnerCode, $_AdmCode, $_PHstatus){
          global $_ObjConnection;
        $_ObjConnection->Connect();
                $_ddlCategory = mysqli_real_escape_string($_ObjConnection->Connect(), $_ddlCategory);
                $_ddlSpecialPref = mysqli_real_escape_string($_ObjConnection->Connect(), $_ddlSpecialPref);
                $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(), $_LearnerCode);
                $_AdmCode = mysqli_real_escape_string($_ObjConnection->Connect(), $_AdmCode);
                $_PHstatus = mysqli_real_escape_string($_ObjConnection->Connect(), $_PHstatus);
        try {
            if($_ddlCategory == '1'){
                $_ddlCategoryDoc ='NA';
            }
            else{
                $_ddlCategoryDoc = $_LearnerCode . "_CasteProof.jpg";
            }
            if($_ddlSpecialPref == 'NA'){
                $_ddlSpecialPrefDoc ='NA';
            }
            else{
                $_ddlSpecialPrefDoc = $_LearnerCode . "_SpecialPref.jpg";
            }

            $_InsertQuery = "INSERT INTO  tbl_admission_spmrm_scheme (Admission_Code, Admission_LearnerCode, Admission_HighQualification, 
	Admission_Cast, Admission_Cast_Doc, Admission_SSC_Doc, Admission_SPC, Admission_SPC_Doc, 	Admission_Ph) 
        VALUES ('" . $_AdmCode . "','" . $_LearnerCode . "','" . $_LearnerCode . "_HighQulification.jpg','" . $_ddlCategory . "','" . $_ddlCategoryDoc . "','" . $_LearnerCode . "_ClassXCert.jpg','" . $_ddlSpecialPref . "','" . $_ddlSpecialPrefDoc . "','" . $_PHstatus . "')";



            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
                    
        }
        catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
     }
     

}
