<?php

/**
 * Description of clsBioMetricDeregistration
 *
 * @author SUNIL KUMAR BAINDARA
 * DATED: 26-09-2018
  * Updated by : Abhishek
 * DATED: 15-05-2019
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsBioMetricDeregistrationByTech {

    //put your code here

    public function update_learner_deregister_apply_table($LD_LearnerCode, $LD_ITGK_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            date_default_timezone_set("Asia/Kolkata");
            $d = date("Y-m-d H:i:s");
            $_UpdateQuery = "UPDATE tbl_biomatric_deregistration_apply
            SET Bio_Dereg_Apply_Status='dereg', Bio_Dereg_Approve_Datetime='" . $d . "'
            WHERE Bio_Dereg_Apply_Lcode ='" . $LD_LearnerCode . "' and Bio_Dereg_Apply_ITGK='" . $LD_ITGK_Code . "' and Bio_Dereg_Apply_Status='applied'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function update_learner_deregister_apply_table_reject($LD_LearnerCode, $LD_ITGK_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            date_default_timezone_set("Asia/Kolkata");
            $d = date("Y-m-d H:i:s");
            $_UpdateQuery = "UPDATE tbl_biomatric_deregistration_apply
            SET Bio_Dereg_Apply_Status='reject', Bio_Dereg_Approve_Datetime='" . $d . "'
            WHERE Bio_Dereg_Apply_Lcode ='" . $LD_LearnerCode . "' and Bio_Dereg_Apply_ITGK='" . $LD_ITGK_Code . "' and Bio_Dereg_Apply_Status='applied'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function update_tbl_admission_table($LD_LearnerCode, $LD_ITGK_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            $_UpdateQuery = "UPDATE tbl_admission
            SET BioMatric_Status=0
            WHERE Admission_LearnerCode ='" . $LD_LearnerCode . "' and Admission_ITGK_Code='" . $LD_ITGK_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function delete_biomatric_registration_entry($LD_LearnerCode, $LD_ITGK_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            $_DeleteQuery = "delete from tbl_biomatric_registration where BioMatric_Admission_LCode ='" . $LD_LearnerCode . "' and BioMatric_ITGK_Code='" . $LD_ITGK_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddAdmAataLog($LD_AdmCode, $LD_LearnerCode, $LD_ITGK_Code, $User_LoginId, $BioMatric_ISO_Template, $BioMatric_Reason, $BioMatric_LMS, $BioMatric_Attnd, $Bio_Dereg_Apply_Course, $Bio_Dereg_Apply_Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO `tbl_adm_data_log`(`Adm_Data_Log_Lcode`, `Adm_Data_Log_Oldisotemplate`, `Adm_Data_Log_ITGK`, `Adm_Data_Log_Upduserloginid`,Adm_Data_Log_LMSscore,Adm_Data_Log_Reason,Adm_Data_Log_Attendance)
                                VALUES
                        ('" . $LD_LearnerCode . "', '" . $BioMatric_ISO_Template . "', '" . $LD_ITGK_Code . "', '" . $User_LoginId . "', '" . $BioMatric_LMS . "', '" . $BioMatric_Reason . "', '" . $BioMatric_Attnd . "')";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetBioMetricRegistration($LD_LearnerCode, $LD_ITGK_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            $_SelectQuery = "select BioMatric_Code,BioMatric_ISO_Template 
                from tbl_biomatric_registration
                where BioMatric_ITGK_Code='" . $LD_ITGK_Code . "' and BioMatric_Admission_LCode='" . $LD_LearnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCurrentBatchLearnerListFinal($LD_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $_SelectQuery = "select *
                    from tbl_learner_deregister 
                    WHERE LD_Status=1 
                    AND LD_ID IN (" . $LD_LearnerCode . ")";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCurrentBatchLearnerListPreview($LD_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
//                    $_SelectQuery3 = "select a.*,b.Admission_Name,b.Admission_Fname, c.Course_Name, d.Batch_Name 
//                    from tbl_learner_deregister a
//                    LEFT JOIN tbl_admission b ON a.LD_LearnerCode=b.Admission_LearnerCode
//                    INNER JOIN tbl_course_master AS c ON a.LD_Course=c.Course_Code 
//                    INNER JOIN tbl_batch_master AS d ON a.LD_Batch=d.Batch_Code 
//                    WHERE a.LD_Status=1 
//                    AND a.LD_ID IN (".$LD_LearnerCode.") ORDER BY a.LD_DateApply DESC";
            // $_SelectQuery3 = "select a.*, c.Course_Name, d.Batch_Name 
            // from tbl_learner_deregister a
            // INNER JOIN tbl_course_master AS c ON a.LD_Course=c.Course_Code 
            // INNER JOIN tbl_batch_master AS d ON a.LD_Batch=d.Batch_Code 
            // WHERE a.LD_Status=1 
            // AND a.LD_ID IN (".$LD_LearnerCode.") ORDER BY a.LD_DateApply DESC";
            // By Abhishek
            $_SelectQuery3 = "select l.*,cnt, r.Course_Name, r.Batch_Name 
from tbl_biomatric_deregistration_apply l
inner join (
  select Bio_Dereg_Apply_Admcode, max(Bio_Dereg_Apply_Datetime) as latest,
sum(case when Bio_Dereg_Apply_Status='dereg' then 1 else 0 end) cnt, Course_Name, Batch_Name  
  from tbl_biomatric_deregistration_apply as a INNER JOIN tbl_course_master AS c ON a.Bio_Dereg_Apply_Course=c.Course_Code 
INNER JOIN tbl_batch_master AS d ON a.Bio_Dereg_Apply_Batch=d.Batch_Code 
  group by Bio_Dereg_Apply_Admcode) r
  on l.Bio_Dereg_Apply_Datetime = r.latest and l.Bio_Dereg_Apply_Admcode = r.Bio_Dereg_Apply_Admcode
 and Bio_Dereg_Apply_Status='applied' and r.Bio_Dereg_Apply_Admcode in (" . $LD_LearnerCode . ")
order by Bio_Dereg_Apply_Datetime desc limit 50";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);

            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCurrentBatchLearnerList() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

//                $_SelectQuery3 = "select a.*,b.Admission_Name,b.Admission_Fname, c.Course_Name, d.Batch_Name 
//                    from tbl_learner_deregister a
//                    LEFT JOIN tbl_admission b ON a.LD_LearnerCode=b.Admission_LearnerCode
//                    INNER JOIN tbl_course_master AS c ON a.LD_Course=c.Course_Code 
//                    INNER JOIN tbl_batch_master AS d ON a.LD_Batch=d.Batch_Code 
//                    WHERE b.Admission_Payment_Status = '1'
//                    AND b.BioMatric_Status=1 
//                    AND a.LD_Status=1 ORDER BY a.LD_DateApply DESC";
            // $_SelectQuery3 = "select a.*, c.Course_Name, d.Batch_Name 
            //     from tbl_learner_deregister a 
            //     INNER JOIN tbl_course_master AS c ON a.LD_Course=c.Course_Code 
            //     INNER JOIN tbl_batch_master AS d ON a.LD_Batch=d.Batch_Code 
            //     WHERE a.LD_Status=1 ORDER BY a.LD_DateApply DESC";
            //By Abhishek

            $_SelectQuery3 = "select l.*,cnt, r.Course_Name, r.Batch_Name 
from tbl_biomatric_deregistration_apply l
inner join (
  select Bio_Dereg_Apply_Admcode, max(Bio_Dereg_Apply_Datetime) as latest,
sum(case when Bio_Dereg_Apply_Status='dereg' then 1 else 0 end) cnt, Course_Name, Batch_Name  
  from tbl_biomatric_deregistration_apply as a INNER JOIN tbl_course_master AS c ON a.Bio_Dereg_Apply_Course=c.Course_Code 
INNER JOIN tbl_batch_master AS d ON a.Bio_Dereg_Apply_Batch=d.Batch_Code 
  group by Bio_Dereg_Apply_Admcode) r
  on l.Bio_Dereg_Apply_Datetime = r.latest and l.Bio_Dereg_Apply_Admcode = r.Bio_Dereg_Apply_Admcode
 and Bio_Dereg_Apply_Status='applied'
order by Bio_Dereg_Apply_Datetime desc Limit 50";


            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddDeregisterLearner($LD_LearnerCode, $LD_ITGK_Code, $LD_Batch, $LD_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $_InsertQuery = "INSERT INTO `tbl_learner_deregister`(`LD_LearnerCode`, `LD_ITGK_Code`, `LD_Batch`, `LD_Course`)
                                VALUES
                        ('" . $LD_LearnerCode . "', '" . $LD_ITGK_Code . "', '" . $LD_Batch . "', '" . $LD_Course . "')";
            $_DuplicateQuery = "Select * From tbl_learner_deregister Where LD_LearnerCode='" . $LD_LearnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLCurrentOpenBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "select a.Batch_Code, a.Batch_Name from tbl_batch_master as a 
                inner join tbl_event_management as b on a.Batch_Code=b.Event_Batch and a.Course_Code=b.Event_Course 
                where b.Event_LearnerAttendance='1' and curdate() between b.Event_Startdate and b.Event_Enddate 
                and a.Course_Code='" . $course . "' ORDER BY a.Batch_Code DESC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SendSMSTOITGK($LD_ITGK_Code, $learnercount) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            $_SelectQueryMOb = "select User_MobileNo from tbl_user_master where User_LoginId='" . $LD_ITGK_Code . "'";
            $_ResponseMob = $_ObjConnection->ExecuteQuery($_SelectQueryMOb, Message::SelectStatement);
            if ($_ResponseMob[0] == 'Success') {
                $_RowMob = mysqli_fetch_array($_ResponseMob[2], true);
                $_Mobile = $_RowMob['User_MobileNo'];
                $_SMS = "Dear IT-GK, Your Learner(" . $learnercount . ") Biometric Enrollment is Deregistered Successfully. You can enroll Learner again.";
                SendSMS($_Mobile, $_SMS);
                $_Response = 'Success';
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function SendSMSTOITGK_reject($LD_ITGK_Code, $LD_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            $_SelectQueryMOb = "select User_MobileNo from tbl_user_master where User_LoginId='" . $LD_ITGK_Code . "'";
            $_ResponseMob = $_ObjConnection->ExecuteQuery($_SelectQueryMOb, Message::SelectStatement);
            if ($_ResponseMob[0] == 'Success') {
                $_RowMob = mysqli_fetch_array($_ResponseMob[2], true);
                $_Mobile = $_RowMob['User_MobileNo'];
                $_SMS = "Dear IT-GK, Your Learner(" . $LD_LearnerCode . ") Biometric Deregister Application got Rejected.";
                SendSMS($_Mobile, $_SMS);
                $_Response = 'Success';
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
 public function Filltotalreq(){
            global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQueryMOb = "select count(*) as cnt from tbl_biomatric_deregistration_apply where Bio_Dereg_Apply_Status='applied'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQueryMOb, Message::SelectStatement);

        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
 }
}
