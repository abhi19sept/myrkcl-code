<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrFeedbackUpdate
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrFeedbackUpdate {
    //put your code here
    
    public function Add($_Reason,$_CenterCode,$_VisitId,$_TheoryArea,$_LabArea,$_TotalArea) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d H:i:s");
            $_VisitId = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitId);
			
             if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
                 $_InsertQuery = "INSERT INTO tbl_ncr_feedback_log
                            SELECT * FROM tbl_ncr_feedback 
                            WHERE NCRFeedback_Code='" . $_VisitId . "'";    	
            $_ResponseI=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
             $_UpdateQuery = "update tbl_ncr_feedback set NCRFeedback_UpdateReason='" . $_Reason . "', NCRFeedback_UpdateBy='" . $_SESSION['User_LoginId'] . "', NCRFeedback_UpdateDate='" . $_Date . "',"
                     . "NCRFeedback_TheoryArea='" . $_TheoryArea . "',NCRFeedback_LABArea='" . $_LabArea . "', NCRFeedback_TotalArea='" . $_TotalArea . "' "
                     . "where NCRFeedback_Code='" . $_VisitId . "'";   
        
             $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetITGKCodes() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
                
                  $_SelectQuery = "select * from tbl_ncr_feedback where NCRFeedback_User like ('%DPO%')";  
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function GetVisitCloseReason() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                
                  $_SelectQuery = "select * from tbl_visitclosereason_master where VisitClose_Status='1'";  
               
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
         public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
			$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
                  $_SelectQuery = "select * from tbl_ncr_feedback where NCRFeedback_AOCode='" . $_CenterCode . "' and NCRFeedback_User like ('%DPO%')";  
                
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
