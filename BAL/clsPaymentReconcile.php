<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionTrans
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPaymentReconcile {

    //put your code here
	
	public function GetAllProdInfo() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	$_SelectQuery = "Select DISTINCT Pay_Tran_ProdInfo From tbl_payment_transaction";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);          
			}	 
		catch (Exception $_ex) {	$_Response[0] = $_ex->getLine() . $_ex->getTrace();
									$_Response[1] = Message::Error;            
								}
        return $_Response;
    } 

    public function ShowDetails($_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_SelectQuery = "Select * From tbl_payment_transaction Where Pay_Tran_ProdInfo='" . $_Status . "' AND pay_verifyapi_status='verified' AND Pay_Tran_Status='PaymentReceive'";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			

				} catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function Update($_ProductInfo, $count, $_TxnId) {
		//print_r($_TxnId);
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_SelectQuery = "Select * From tbl_payment_transaction Where Pay_Tran_ProdInfo='" . $_ProductInfo . "' AND pay_verifyapi_status='verified' AND Pay_Tran_Status='PaymentReceive' AND Pay_Tran_PG_Trnid IN ($_TxnId)";
				$_SelectResponse = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			
				
				while ($_Row = mysqli_fetch_array($_SelectResponse[2])) {
					    //print_r($_Row['Payumoney_Transaction']);
					$Admission_code = $_Row['Pay_Tran_AdmissionArray'];
					$_Txnid = $_Row['Pay_Tran_PG_Trnid'];					
					$firstname = $_Row['Pay_Tran_Fname'];
					$amount = $_Row['Pay_Tran_Amount'];
					$udf2 = $_Row['Pay_Tran_RKCL_Trnid'];
					$productinfo = $_Row['Pay_Tran_ProdInfo'];
					$udf1 = $_Row['Pay_Tran_ITGK'];
					
					$_SelectQuery1 = "Select Admission_LearnerCode FROM tbl_admission Where Admission_Payment_Status = '1' AND Admission_Code IN ($Admission_code)";					
					$_SelectResponse1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
					
					if($_SelectResponse1[0]=='Success') {
						/*  Refund  */
						$_InsertQuery = "INSERT INTO tbl_payment_refund (Payment_Refund_Id, Payment_Refund_Txnid, Payment_Refund_Amount, Payment_Refund_Status,"
								. "Payment_Refund_ProdInfo,"
								. "Payment_Refund_ITGK,Payment_Refund_RKCL_Txid) "
								. "Select Case When Max(Payment_Refund_Id) Is Null Then 1 Else Max(Payment_Refund_Id)+1 End as Payment_Refund_Id,"
								. "'" . $_Txnid . "' as Payment_Refund_Txnid,'" .$amount. "' as Payment_Refund_Amount, 'Refund' as Payment_Refund_Status,"
								. "'" .$productinfo. "' as Payment_Refund_ProdInfo,"
								. "'" .$udf1. "' as Payment_Refund_ITGK, '" .$udf2. "' as Payment_Refund_RKCL_Txid"
								. " From tbl_payment_refund";
						$_Response4=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
						
						$_UpdateQuery = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndRefund'"			
									. "Where Pay_Tran_PG_Trnid = '" . $_Txnid . "' ";								
						$_Response3=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
					}
					else {
						/*  Update  */
						$_UpdateQuery = "Update tbl_admission set Admission_Payment_Status = '1', Admission_TranRefNo='" . $_Txnid . "' "			
									. "Where Admission_Code IN ($Admission_code)";								
						$_Response3=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						
						$_SelectQuery2 = "Select * From tbl_admission_transaction Where Admission_Transaction_Txtid='" . $_Txnid . "'";
						$_SelectResponse2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);			
						if($_SelectResponse2[0]==Message::NoRecordFound) {
							$_InsertQuery = "INSERT INTO tbl_admission_transaction (Admission_Transaction_Code, Admission_Transaction_Status, Admission_Transaction_Fname, Admission_Transaction_Amount,"
								. "Admission_Transaction_Txtid, Admission_Transaction_ProdInfo,"
								. "Admission_Transaction_CenterCode,Admission_Transaction_RKCL_Txid) "
								. "Select Case When Max(Admission_Transaction_Code) Is Null Then 1 Else Max(Admission_Transaction_Code)+1 End as Admission_Transaction_Code,"
								. "'Success' as Admission_Transaction_Status,'" .$firstname. "' as Admission_Transaction_Fname,'" .$amount. "' as Admission_Transaction_Amount,"
								. "'" .$_Txnid. "' as Admission_Transaction_Txtid,"
								. "'" .$productinfo. "' as Admission_Transaction_ProdInfo,'" .$udf1. "' as Admission_Transaction_CenterCode, '" .$udf2. "' as Admission_Transaction_RKCL_Txid"
								. " From tbl_admission_transaction";
							$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);						
						}
						else {
							$_UpdateAdmissionTranQuery = "Update tbl_admission_transaction set Admission_Transaction_Status = 'Success', Admission_Payment_Mode='VerifyApi'"			
								. "Where Admission_Transaction_Txtid='" . $_Txnid . "'";								
							$_Response4=$_ObjConnection->ExecuteQuery($_UpdateAdmissionTranQuery, Message::UpdateStatement);				
						}
						
						$_UpdateQuery5 = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndConfirmed'"			
									. "Where Pay_Tran_PG_Trnid = '" . $_Txnid . "' ";								
						$_Response4=$_ObjConnection->ExecuteQuery($_UpdateQuery5, Message::UpdateStatement);
						
					}
				}
		
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response3;        
    }
	
}
