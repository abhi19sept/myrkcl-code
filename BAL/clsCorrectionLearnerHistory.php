<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsCorrectionLearnerHistory
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionLearnerHistory {
    //put your code here
    
    public function GetAll($_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				
			$_SelectQuery = "select lcode, Correction_ITGK_Code, upper(cfname) as name, totalmarks, upper(cfaname) as fname,	
							applicationfor, application_type, applicationdate, cstatus, dispatchstatus, dob, exameventname,
							cid, Correction_Payment_Status, form_process_date, form_delivered_date
							from tbl_correction_copy as a inner join tbl_capcategory as b 
							on a.dispatchstatus=b.capprovalid 
							where lcode='".$_Lcode."' order by cid DESC";  				
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function FILLStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where correctionrole='Yes' AND capprovalid='3'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetLotName($_cid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_cid);
				
            $_SelectQuery = "select b.lotname from tbl_correction_copy as a inner join tbl_clot as b on a.lot=b.lotid
								where cid='".$_cid."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}