<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOwnershipFeePayment
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'common/payments.php';


$_ObjConnection = new _Connection();
$_Response = array();

class clsOwnershipFeePayment extends paymentFunctions {
    //put your code here

    
    public function GetDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type 
                            FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b
                            ON a.Organization_Type=b.Org_Type_Code where
                            Org_Itgk_Code='" . $_SESSION['User_LoginId'] . "' AND a.Org_Application_Approval_Status='Pending'
                    and Org_Final_Approval_Status='Pending'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function CheckPayStatus($_Ack) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ack = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ack);
				
            $_SelectQuery = "Select Org_PayStatus FROM tbl_ownership_change WHERE Org_Ack = '" . $_Ack . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ChkPayEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Event_OwnershipChangePayment,Event_Startdate,Event_Enddate FROM tbl_event_management "
                    . "WHERE CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='14'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetOwnershipAmt() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Ownership_Fee_Amount FROM tbl_ownershipchange_fee_master"
                    . " WHERE Ownership_Fee_Status = 'Active'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function AddOwnershipPayTran($amount, $postValues, $productinfo) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $amount, [$postValues['txtAck']], 0, 0);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }
    
    
}
