<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsChangeRsp
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsChangeRsp {
    //put your code here
    
    public function Add($_Rspcode, $_Rspname, $_Rspregno, $_Rspestdate, $_Rsptype, $_Rspdistrict, $_Rsptehsil, $_Rspstreet, $_Rsproad, $_RspSelectReason, $_RspReason) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_User = $_SESSION['User_LoginId'];
                $_SelectQuery1 = "SELECT User_MobileNo FROM tbl_user_master WHERE User_LoginId='" . $_User . "'";
                $_Response4 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_Row1 = mysqli_fetch_array($_Response4[2]);
                $_MobileITGK = $_Row1['User_MobileNo'];
				date_default_timezone_set('Asia/Calcutta');
                $_Date = date("Y-m-d h:i:s");
                $_InsertQuery = "Insert Into tbl_rspitgk_mapping(Rspitgk_Code,Rspitgk_ItgkCode,Rspitgk_Rspcode,Rspitgk_Rspname,"
                        . "Rspitgk_Rspregno,Rspitgk_Rspestdate,Rspitgk_Rsporgtype,Rspitgk_Rspdistrict,"
                        . "Rspitgk_Rsptehsil,Rspitgk_Rspstreet,Rspitgk_Rsproad,Rspitgk_Date,Rspitgk_RspSelectReason,Rspitgk_Reason) "
                        . "Select Case When Max(Rspitgk_Code) Is Null Then 1 Else Max(Rspitgk_Code)+1 End as Rspitgk_Code,"
                        . "'" . $_User . "' as Rspitgk_ItgkCode,'" . $_Rspcode . "' as Rspitgk_Rspcode,'" . $_Rspname . "' as Rspitgk_Rspname,"
                        . "'" . $_Rspregno . "' as Rspitgk_Rspregno,'" . $_Rspestdate . "' as Rspitgk_Rspestdate,"
                        . "'" . $_Rsptype . "' as Rspitgk_Rsporgtype,'" . $_Rspdistrict . "' as Rspitgk_Rspdistrict,"
                        . "'" . $_Rsptehsil . "' as Rspitgk_Rsptehsil,'" . $_Rspstreet . "' as Rspitgk_Rspstreet,"
                        . "'" . $_Rsproad . "' as Rspitgk_Rsproad,'" . $_Date . "' as Rspitgk_Date,"
                        . "'" . $_RspSelectReason . "' as Rspitgk_RspSelectReason,'" . $_RspReason . "' as Rspitgk_Reason"
                        . " From tbl_rspitgk_mapping";
                //echo $_InsertQuery;
                $_SelectQuery = "Select User_Rsp From tbl_user_master Where User_LoginId='" . $_User . "'";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if ($_Response3[0] == 'Success') {
                    $_Row = mysqli_fetch_array($_Response3[2]);
                    $_RSP = $_Row['User_Rsp'];
                    $_SelectQuery2 = "Select * From tbl_user_master Where User_Code='" . $_RSP . "'";
                    $_Response4 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                    $_Row2 = mysqli_fetch_array($_Response4[2]);
                    $_OldRSPMobile = $_Row2['User_MobileNo'];
                } else {
                    $_RSP = '0';
                }


                //echo $_Response[0];
                if ($_RSP == $_Rspcode) {
                    echo "This is Your Current RSP. Please Select Another RSP.";
                    return;
                } else {
                    $_DuplicateQuery = "Select * From tbl_rspitgk_mapping Where Rspitgk_ItgkCode='" . $_User . "' AND Rspitgk_Status='Pending'";
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                    //echo $_Response[0];
                    if ($_Response[0] == Message::NoRecordFound) {
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//                        $_UpdateQuery = "Update tbl_user_master set User_Rsp='" . $_Rspcode . "'"
//                                . " Where User_LoginId='" . $_User . "'";
//                        $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                        $_SMSITGK = "Dear IT-GK" . $_User . ", Your Application to update " . $_Rspname . " as your Service Provider has been submitted successfully.";
                        $_SMSNEWRSP = "Dear Service Provider, IT-GK " . $_User . " has submitted an application to update you as its New Service Provider.";
                        $_SMSOLDRSP = "Dear Service Provider, IT-GK " . $_User . " has submitted an application to update its Service Provider.";
                        SendSMS($_MobileITGK, $_SMSITGK);
                        SendSMS($_Rspregno, $_SMSNEWRSP);
                        SendSMS($_OldRSPMobile, $_SMSOLDRSP);
                    } else {
                        $_Response[0] = Message::DuplicateRecord;
                        $_Response[1] = Message::Error;
                    }
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {                
                $_SelectQuery = "SELECT * FROM tbl_rspitgk_mapping WHERE Rspitgk_ItgkCode = '" . $_SESSION['User_LoginId'] . "' ORDER BY Rspitgk_Code DESC LIMIT 1";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
          
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d "
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e"
                    . " ON a.Organization_User=e.User_Code WHERE Organization_User = '" . $_SESSION['User_Rsp'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	 public function CheckITGK() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
          if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select * from tbl_eligible_center_spchange WHERE EC_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND EC_App_Eligible='Yes'";
			
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
