<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionSummaryCount
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionSummaryCount {
    //put your code here
    
      public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
             if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 ||
				$_SESSION['User_UserRoll'] == 4) {
            
             $_SelectQuery2 = "Select count(Admission_LearnerCode) as Admission_Code, 
								sum(case when Admission_Payment_Status = '1' then 1 else 0 end) Confirmed_Learner, 
								sum(case when r.result like '%pass%' then 1 else 0 end) as pass_count 
								from tbl_admission as a left join tbl_result as r on a.Admission_LearnerCode=r.scol_no WHERE
								Admission_Course='".$_course."' and Admission_Batch='".$_batch."'"; 
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
			 } 
				
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
