<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPremisesDetail
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPremisesDetail {
    //put your code here
    
   public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
                    
                $_SelectQuery = "Select * from tbl_premises_details";
                
            } else {
                
		$_SelectQuery = "Select * from tbl_premises_details where Premises_User = '" .$_SESSION['User_LoginId'] . "'";              
                
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_actionvalue)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select * From tbl_premises_details Where Premises_Code='" . $_actionvalue . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_actionvalue)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_DeleteQuery = "Delete From tbl_premises_details Where Premises_Code='" . $_actionvalue . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
   public function Add($_Reception,$_ReceptionCapacity,$_receptionAreaInSqFt,$_receptionDetails,$_parkingYesNo,
           $_parkingType,$_parkingTW,$_fwCapacity,$_twCapacity,$_toiletYes,$_OwnershipType,$_toiletType,$_pantryYesNo,
           $_pantryDetails,$_libraryYes,$_libraryDetails,$_staffRoomYesNo,$_staffRoomDetails, $_AreaType, $_TheoryArea, $_LabArea, $_TotalArea) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
$_mobile=$_SESSION['User_MobileNo'];
            $_SMS = "Dear  Applicant, Your Premises Details has been submitted to RKCL. Kindly complete entry of all other details for reaching next stage of NCR.";
            
             $_InsertQuery = "Insert Into tbl_premises_details(Premises_Code,Premises_User,Premises_Receptiontype,Premises_seatingcapacity,Premises_area,Premises_details,Premises_parkingfacility,Premises_ownership,Premises_parkingfor, 	Premises_fwcapacity,Premises_twcapacity,Premises_toiletavailable,Premises_ownershiptoilet,Premises_toilet_type,Premises_panatry,Premises_panatry_capacity,Premises_library_available,Premises_library_capacity,Premises_Staff_available,Premises_Staff_capacity, Permises_Separate, Premises_Theory_area, Premises_Lab_area, Premises_Total_area) 
			 Select Case When Max(Premises_Code) Is Null Then 1 Else Max(Premises_Code)+1 End as Premises_Code,'".$_SESSION['User_LoginId']."' as Premises_User,'".$_Reception."' as Premises_Receptiontype,'" .$_ReceptionCapacity. "' as Premises_seatingcapacity,
			 '" . $_receptionAreaInSqFt. "' as Premises_area,
			 '" .$_receptionDetails. "' as Premises_Details,
			 '" .$_parkingYesNo. "' as Premises_parkingfacility,
			  '" .$_parkingType. "' as Premises_ownership,
			 '" .$_parkingTW. "' as Premises_parkingfor,
			 '" .$_fwCapacity. "' as Premises_fwcapacity,
			 '" .$_twCapacity. "' as Premises_twcapacity,
			 '" .$_toiletYes. "' as Premises_toiletavailable,
			 '" .$_OwnershipType. "' as Premises_ownershiptoilet,
			 '" .$_toiletType. "' as Premises_toilet_type,
			 '" .$_pantryYesNo. "' as Premises_panatry,
			 '" .$_pantryDetails. "' as Premises_panatry_capacity,
			 '" .$_libraryYes. "' as Premises_library_available,
			 '" .$_libraryDetails. "' as Premises_library_capacity,
			 '" .$_staffRoomYesNo. "' as Premises_Staff_available,
			 '" .$_staffRoomDetails. "' as Premises_Staff_capacity,
                             
                             '" .$_AreaType. "' as Permises_Separate,
			 '" .$_TheoryArea. "' as Premises_Theory_area,
			 '" .$_LabArea. "' as Premises_Lab_area,
			 '" .$_TotalArea. "' as Premises_Total_area
			 
			 From tbl_premises_details";
			
                    
            $_DuplicateQuery = "Select * From tbl_premises_details Where Premises_User='".$_SESSION['User_LoginId']."'";
					
                    
             $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
             if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_mobile, $_SMS);
				//$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
       //print_r($_Response);
        return $_Response;
    }
		
    public function Update($_Code,$_Reception,$_ReceptionCapacity,$_receptionAreaInSqFt,$_receptionDetails,$_parkingYesNo,$_parkingType,$_parkingTW,$_fwCapacity,$_twCapacity,$_toiletYes,$_OwnershipType,$_toiletType,$_pantryYesNo,$_pantryDetails,$_libraryYes,$_libraryDetails,$_staffRoomYesNo,$_staffRoomDetails) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_UpdateQuery = "Update tbl_premises_details set Premises_Receptiontype='" . $_Reception . "',
			 	Premises_seatingcapacity='" . $_ReceptionCapacity . "',
			Premises_area='" . $_receptionAreaInSqFt . "',
			Premises_details='" . $_receptionDetails . "',
			Premises_parkingfacility='" . $_parkingYesNo . "',
			Premises_ownership='" . $_parkingType . "',
			Premises_parkingfor='" . $_parkingTW . "',
			Premises_fwcapacity='" . $_fwCapacity . "',
			Premises_twcapacity='" . $_twCapacity . "',
			Premises_toiletavailable='" . $_toiletYes . "',
			Premises_ownershiptoilet='" . $_OwnershipType . "',
			Premises_toilet_type='" . $_toiletType . "',
			Premises_panatry='" . $_pantryYesNo . "',
			Premises_panatry_capacity='" . $_pantryDetails . "',
			Premises_library_available='" . $_libraryYes . "',
			Premises_library_capacity='" . $_libraryDetails . "',
			Premises_Staff_available='" . $_staffRoomYesNo . "',
			Premises_Staff_capacity='" . $_staffRoomDetails . "'
			  Where 	Premises_Code='" . $_Code . "'";
            
            
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
