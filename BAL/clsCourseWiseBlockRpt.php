<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsCourseWiseBlockRpt
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsCourseWiseBlockRpt {
    //put your code here
    
    public function GETCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Distinct Courseitgk_Course from tbl_courseitgk_mapping";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCourseWise($Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$Course = mysqli_real_escape_string($_ObjConnection->Connect(),$Course);
				
            $_SelectQuery = "select a.User_LoginId as CenterCode, a.User_EmailId,a.User_MobileNo, "
                            . "b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, "
                            . "c.Organization_Name as RSPName, e.Tehsil_Name, h.remark, h.activitydate, "
                            . "f.District_Name from tbl_user_master as a INNER JOIN tbl_organization_detail as b "
                            . "on a.User_Code = b.Organization_User Inner Join tbl_Organization_Detail as c "
                            . "on a.User_Rsp = c.Organization_User Inner Join tbl_tehsil_master as e "
                            . "on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f "
                            . "on f.District_Code =  b.Organization_District inner join tbl_courseitgk_mapping as g "
                            . "on a.User_LoginID = g.Courseitgk_ITGK Inner Join block as h "
                            . "on a.User_LoginId=h.centercode "
                            . "where CourseITGK_BlockStatus='Block' and Courseitgk_Course='".$Course."'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
