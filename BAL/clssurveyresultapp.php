<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clssurvey
 *
 *  author yogendra
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clssurveyresultapp {

    //put your code here

	public function GetSurvey() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
			$date = date("Y-m-d");

            $_SelectQuery = "Select Survey_id,Survey_Name from tbl_survey_master where Survey_Date1<='" . $date . "' AND Survey_Date2>='" . $date . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
    
   
    public function GetResult($_actionvalue) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			
			if($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '14')
			{
				  $_SelectQuery = "SELECT survey_code,survey_question,survey_centercode,survey_answers,survey_sp,Organization_Name
						FROM tbl_survey_results as a 
						inner join tbl_user_master as b on a.survey_centercode=b.User_LoginId
						inner join tbl_organization_detail as c on b.User_Code=c.Organization_User
						where survey_code='$_actionvalue' and survey_centercode='".$_SESSION['User_LoginId']."' ";
				
			}
			
		    else if($_SESSION['User_UserRoll'] == '1')
			{
					 $_SelectQuery = "SELECT survey_code,survey_question,survey_centercode,survey_answers,survey_sp,Organization_Name
						FROM tbl_survey_results as a 
						inner join tbl_user_master as b on a.survey_centercode=b.User_LoginId
						inner join tbl_organization_detail as c on b.User_Code=c.Organization_User
						where survey_code='$_actionvalue'";
				
				
			}

           
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
