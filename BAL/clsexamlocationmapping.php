<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsexamlocationmapping {
	 public function GetAll($_actionvalue) {
        global $_ObjConnection;
		$_Response2 = array();
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select District_Name,c.Tehsil_Name as Tehsil_Name,d.Tehsil_Name as Exam_maprkcl,e.Tehsil_Name as Exam_mapvmou,learnercode,learnername,fathername,dob,district,itgktehsil,ExamMapTehsil_rkcl,ExamLocation_vmou,examid,examcode from tbl_examlocationmapping as a inner join tbl_district_master as b on a.district=b.District_Code inner join tbl_tehsil_master as c on a.itgktehsil=c.Tehsil_Code   inner join tbl_tehsil_master as d on a.ExamMapTehsil_rkcl=d.Tehsil_Code inner join tbl_tehsil_master as e on a.ExamLocation_vmou=e.Tehsil_Code  where
			a.examid='".$_actionvalue."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			
			
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	public function GetAll1() {
        global $_ObjConnection;
		$_Response= array();
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select learnercode,learnername,fathername,dob,district,itgktehsil,ExamMapTehsil_rkcl,examid,count(itgktehsil) as itgkcount from tbl_examlocationmapping  GROUP BY itgktehsil ";
            
            $_Response2=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			
			$_SelectQuery2 = "Select *  from tbl_examchoicemaster";
            
            $_Response3=$_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
			while ($_data = mysqli_fetch_array($_Response2[2])) 
			{
				if($_data['itgkcount']>=1)
				{
					$_UpdateQuery = "Update tbl_examlocationmapping set ExamMapTehsil_rkcl 	='" . $_data['itgktehsil'] . "',ExamLocation_vmou	='" . $_data['itgktehsil'] . "' 
					
					Where  	itgktehsil='" . $_data['itgktehsil'] . "'";
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				}
				else
				{
					$_row = mysqli_fetch_array($_Response3[2]);
					$_UpdateQuery1 = "Update tbl_examlocationmapping set ExamMapTehsil_rkcl='" .$_row['choice1'] . "',ExamLocation_vmou='" . $_row['choice1'] . "'
					
					Where  	itgktehsil='" . $_row['tehsil'] . "' AND ExamMapTehsil_rkcl=''";
					$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
					
					
				}
				
				
				
				
				
				
			
		    }
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	public function Add()
	{	global $_ObjConnection;
		$_Response1 = array();
        $_ObjConnection->Connect();
        try {			
				$_SelectQuery = "Select * From examdata";            
				$_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				
            	 while ($_Row = mysqli_fetch_array($_Response1[2])) {
					 
					 $_InsertQuery = "Insert Into tbl_examlocationmapping(learnercode,learnername,fathername,dob,district,itgkcode,itgktehsil,ExamMapTehsil_rkcl,ExamLocation_vmou,examid) 
												 VALUES ('" .$_Row['learnercode']. "',
												 '" .$_Row['learnername']. "',
												 '" .$_Row['fathername']. "',
												  '" .$_Row['dob']. "',
												  '" .$_Row['itgkdistrict']. "',
												  '" .$_Row['itgkcode']. "',
												  '" .$_Row['itgktehsil']. "',
												  '',
												  ' ',
												   '" .$_data['examid']. "'
												  
												)";
												
            
            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				 }		
		}
		
		catch (Exception $_e) {
										$_Response[0] = $_e->getTraceAsString();
										$_Response[1] = Message::Error;
            
									}
        return $_Response;
	}
	public function Getexception() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT  Event_Id,Event_Name ,Affilate_Event From tbl_exammaster as a inner join tbl_events as b on a.Affilate_Event=b.Event_Id ";
			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	public function Getexambatch()
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_SelectQuery = "Select DISTINCT Affilate_FreshBaches FROM tbl_exammaster";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			   
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 return $_Response;
		}
		
		
		public function GeteselectedEvents($_Event)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
					$_Event = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event);
					
				$_SelectQuery = "Select DISTINCT Affilate_FreshBaches  FROM tbl_exammaster where Affilate_Event='".$_Event."'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			   
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 return $_Response;
		}
		
		
		

		public function Getexambatchname($_Batch_Code)
      {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
				
            $_SelectQuery = "Select DISTINCT Event_Id,Event_Name From tbl_exammaster as a INNER JOIN tbl_events as b ON a. Affilate_Event = b.Event_Id inner join tbl_admission as c where a.Affilate_FreshBaches IN (" . $_Batch_Code . ") and c.Admission_Batch IN (" . $_Batch_Code . ") ";            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }		

	
	
	
	
	
	public function GetDatabyCode($_actionvalue)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select *  From tbl_examlocationmapping  Where examcode='" . $_actionvalue . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	 public function Update($_Code,$_Tehsil) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_Tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil);
				
            $_UpdateQuery = "Update tbl_examlocationmapping set ExamLocation_vmou='" . $_Tehsil . "'
			Where  examcode='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_examlocationmapping Where examcode='" . $_Code . "' and examcode <> '" .$_Code. "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
	
	
}
