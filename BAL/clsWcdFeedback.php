<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsWcdFeedback
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdFeedback {
    //put your code here
    
    public function AddFeedback($_VisitCode,$_CenterCode, $_Desktop,$_Toilet,$_Stoilet,$_Water,$_Inagurated,$_Upload,$_PlanDate,$_Informed,
                $_FacultyName,$_Qualification,$_AdmissionCodes) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();
        date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d h:i:s");
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_VisitCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitCode);
				
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {             

                $_InsertQuery = "Insert into tbl_wcdvisit_feedback 
	(WCDFeedback_VisitCode,WCDFeedback_User, WCDFeedback_ITGKCode, WCDFeedback_Desktop, WCDFeedback_Toilet, 
	WCDFeedback_SToilet, WCDFeedback_Water, WCDFeedback_Inagurated, WCDFeedback_Upload, 
        WCDFeedback_PlaneDate, WCDFeedback_Informed, WCDFeedback_FacultyName, WCDFeedback_Qualification,
	 WCDFeedback_AdmissionCodes,WCDFeedback_Date)
	values
	('" . $_VisitCode . "','" . $_SESSION['User_LoginId'] . "', '" . $_CenterCode . "', '" . $_Desktop . "', '" . $_Toilet . "', '" . $_Stoilet . "', 
	'" . $_Water . "', '" . $_Inagurated . "', '" . $_Upload . "', '" . $_PlanDate . "', '" . $_Informed . "', '" . $_FacultyName . "', 
	'" . $_Qualification . "', '" . $_AdmissionCodes . "', '" . $_Date . "')";

        $_DuplicateQuery = "Select * From tbl_wcdvisit_feedback Where WCDFeedback_User='" . $_SESSION['User_LoginId'] . "' and  WCDFeedback_ITGKCode = '" . $_CenterCode . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                if ($_Response[0] == Message::NoRecordFound) {
					
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                        
                } else {
                    echo " Feedback Already Exists.";
                    return;                     
                }
                } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
    
    public function GetITGK_Details($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
			
                $_SelectQuery = "Select * From vw_itgkname_distict_rsp WHERE ITGKCODE='" . $_CenterCode . "'";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
	
	public function CheckEligible($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "select * from tbl_wcd_intake_master where Wcd_Intake_Center='" . $_CenterCode . "' and Wcd_Intake_Batch in (249,250)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function CheckRSCFA($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "select * from tbl_courseitgk_mapping where Courseitgk_ITGK='" . $_CenterCode . "' and Courseitgk_Course='RS-CFA Women'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "' order by Batch_Code DESC limit 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetDatabyCode($_CourseCode, $_CenterCode, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					
            $_SelectQuery = "select Admission_Code,Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB 
                from tbl_admission where Admission_Course='" . $_CourseCode . "' and Admission_ITGK_Code='" . $_CenterCode . "' and Admission_batch='" . $_batch . "'";
                    //. " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
