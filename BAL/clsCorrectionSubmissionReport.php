<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionSubmissionReport {
    //put your code here
    public function GetAllDetails($_LOT) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LOT = mysqli_real_escape_string($_ObjConnection->Connect(),$_LOT);
				
				if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258'){
					 $_SelectQuery = "Select a.*, b.District_Name, c.Result_Date as result_date FROM `tbl_correction_copy` as
									a LEFT JOIN tbl_district_master as b on a.district_code = b.District_Code LEFT JOIN 
									tbl_events as c on a.exameventname=c.Event_Name OR a.exameventname=c.Event_Name_VMOU		
									where lot='" . $_LOT . "' AND dispatchstatus='0'";
				}
				else{
					$_SelectQuery = "Select a.*, b.District_Name, c.Result_Date as result_date FROM `tbl_correction_copy` as
									a LEFT JOIN tbl_district_master as b on a.district_code = b.District_Code LEFT JOIN 
									tbl_events as c on a.exameventname=c.Event_Name OR a.exameventname=c.Event_Name_VMOU
									where Correction_ITGK_Code = '".$_SESSION['User_LoginId']."' AND
									lot='" . $_LOT . "' AND dispatchstatus='0'";
				}				
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function FILLCorrectionLot() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where correctionrole='Yes'";
            $_SelectQuery = "SELECT distinct lotid, lotname FROM tbl_clot as a inner join tbl_correction_copy as b
								on a.lotid=b.lot order by lotid DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
