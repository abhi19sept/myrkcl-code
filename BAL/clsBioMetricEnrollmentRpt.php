<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionTrans
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsBioMetricEnrollmentRpt {

    //put your code here

    public function ShowEnrolled($_Status, $_Course, $_Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 28) {
//                $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.BioMatric_Status,d.District_Name FROM `tbl_admission` as a INNER JOIN
//										tbl_user_master as b on a.Admission_ITGK_Code = b.User_LoginId INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User
//										INNER JOIN tbl_district_master as d on c.Organization_District = d.District_Code where BioMatric_Status = '1' AND
//										Admission_Course='" . $_Course . "' AND Admission_Batch= '" . $_Batch . "'";
                $_SelectQuery = "Select BioMatric_Admission_BioPIN,BioMatric_Admission_LCode,BioMatric_ITGK_Code,BioMatric_Admission_Name,"
                        . "BioMatric_Admission_Fname FROM tbl_biomatric_registration WHERE BioMatric_Admission_Course='" . $_Course . "' AND "
                        . "BioMatric_Admission_Batch= '" . $_Batch . "' ORDER BY BioMatric_ITGK_Code";
            } elseif ($_SESSION['User_UserRoll'] == 14) {

                $_SelectQuery = "Select BioMatric_Admission_BioPIN,BioMatric_Admission_LCode,BioMatric_ITGK_Code,BioMatric_Admission_Name,"
                        . "BioMatric_Admission_Fname FROM tbl_biomatric_registration WHERE BioMatric_Admission_Course='" . $_Course . "' AND "
                        . " BioMatric_Admission_Batch= '" . $_Batch . "' AND BioMatric_Admission_RspName = '" . $_SESSION['User_Code'] . "' "
                        . " ORDER BY BioMatric_ITGK_Code";
            } else {
//                $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.BioMatric_Status,d.District_Name FROM `tbl_admission` as a INNER JOIN
//										tbl_user_master as b on a.Admission_ITGK_Code = b.User_LoginId INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User
//										INNER JOIN tbl_district_master as d on c.Organization_District = d.District_Code where BioMatric_Status = '1' AND
//										Admission_Course='" . $_Course . "' AND Admission_Batch= '" . $_Batch . "' AND Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";

                $_SelectQuery = "Select BioMatric_Admission_BioPIN,BioMatric_Admission_LCode,BioMatric_ITGK_Code,BioMatric_Admission_Name,"
                        . "BioMatric_Admission_Fname FROM tbl_biomatric_registration WHERE BioMatric_Admission_Course='" . $_Course . "' AND "
                        . "BioMatric_Admission_Batch= '" . $_Batch . "' AND BioMatric_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' ORDER BY "
                        . "BioMatric_ITGK_Code";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ShowNotEnrolled($_Status, $_Course, $_Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 28) {

                $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.BioMatric_Status,a.Admission_Mobile,d.District_Name FROM `tbl_admission` as a INNER JOIN
                                            tbl_user_master as b on a.Admission_ITGK_Code = b.User_LoginId INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User
                                            INNER JOIN tbl_district_master as d on c.Organization_District = d.District_Code where BioMatric_Status = '0' AND Admission_Payment_Status='1' and
                                            Admission_Course='" . $_Course . "' AND Admission_Batch= '" . $_Batch . "'";
            } elseif ($_SESSION['User_UserRoll'] == 14) {

                $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,Admission_Fname,a.BioMatric_Status,a.Admission_Mobile,d.District_Name FROM `tbl_admission` as a INNER JOIN
                                            tbl_user_master as b on a.Admission_ITGK_Code = b.User_LoginId INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User
                                            INNER JOIN tbl_district_master as d on c.Organization_District = d.District_Code where BioMatric_Status = '0' AND Admission_Payment_Status='1' and
                                            Admission_Course='" . $_Course . "' AND Admission_Batch= '" . $_Batch . "' AND Admission_RspName = '" . $_SESSION['User_Code'] . "'";
            } else {
                $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,Admission_Fname,a.BioMatric_Status,a.Admission_Mobile,d.District_Name FROM `tbl_admission` as a INNER JOIN
                                            tbl_user_master as b on a.Admission_ITGK_Code = b.User_LoginId INNER JOIN tbl_organization_detail as c on b.User_Code=c.Organization_User
                                            INNER JOIN tbl_district_master as d on c.Organization_District = d.District_Code where BioMatric_Status = '0' AND Admission_Payment_Status='1' and
                                            Admission_Course='" . $_Course . "' AND Admission_Batch= '" . $_Batch . "' AND Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
