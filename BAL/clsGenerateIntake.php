<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsGenerateIntake
 *
 * @author VIVEK
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGenerateIntake {
    //put your code here
    
    public function GenerateIntake() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_SelectQuery = "SELECT * FROM tbl_system_info WHERE System_User='" . $_SESSION['User_LoginId'] . "' AND System_Type='Client'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Count = mysqli_num_rows($_Response1[2]);
                if($_Count){
           
                $_Intake=$_Count * 12;
                
                $_SelectQuery = "Select Max(Batch_Code) as  Batch_Code From tbl_batch_master WHERE Course_Code = '1'";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                //print_r($_Response3[2]);
                $_Row = mysqli_fetch_array($_Response3[2]);
                $_Batch = $_Row['Batch_Code'];        
                
                $_InsertQuery = "Insert Into tbl_intake_master(Intake_Code,Intake_User,Intake_Center,Intake_Course,Intake_Batch,"
                    . "Intake_Base_Intake,Intake_Available,Intake_Consumed,Intake_NOS) "
                    . "Select Case When Max(Intake_Code) Is Null Then 1 Else Max(Intake_Code)+1 End as Intake_Code,"
                    . "'1' as Intake_User,'" . $_SESSION['User_LoginId'] . "' as Intake_Center,'1' as Intake_Course,"
                    . "'" . $_Batch . "' as Intake_Batch,'" . $_Intake . "' as Intake_Base_Intake,'" . $_Intake . "' as Intake_Available,"
                    . "'0' as Intake_Consumed,'" . $_Count . "' as Intake_NOS"
                    . " From tbl_intake_master";
            //echo $_InsertQuery;
                $_DuplicateQuery = "Select * From tbl_intake_master Where Intake_Center='" . $_SESSION['User_LoginId'] . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
                if($_Response[0]==Message::NoRecordFound)
                {
                    $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_UpdateQuery = "Update tbl_system_info SET System_Status='Confirmed By Center'"
                    . "Where System_User='" . $_SESSION['User_LoginId'] . "'";
            
                    $_Response2=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                }
                else {
                    $_Response[0] = Message::DuplicateRecord;
                    $_Response[1] = Message::Error;
                }   
                } 
                else{
                    echo "Please Add Client Systems to Generate Intake";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }  
                      
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
