<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsRSPTargetDeactive
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRSPTargetDeactive {
    //put your code here
    
    public function GetAll($_UserName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_User = $_SESSION['User_LoginId'];
				$_UserName = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserName);
				
            $_SelectQuery = "Select a.*,b.District_Name, c.User_Code"
                    . " From tbl_rsptarget as a inner join tbl_district_master as b "
                    . "on a.Rsptarget_District"
                    . "=b.District_Code "
                    . " INNER JOIN tbl_user_master as c "
                    . "on a.Rsptarget_User = c.User_LoginId "
                    . "WHERE Rsptarget_User='" . $_UserName . "' AND Rsptarget_Status='Approved'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ValidateDistrict($_UserCode,$_DistrictCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_User = $_SESSION['User_LoginId'];
				$_UserCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_UserCode);
				$_DistrictCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DistrictCode);
				
            $_SelectQuery = "SELECT count(a.User_Code) as UserCount from tbl_user_master as a INNER JOIN tbl_organization_detail as b "
                    . " ON a.User_Code = b.Organization_User "
                    . "WHERE a.User_Rsp = '" . $_UserCode . "' and b.Organization_District = '" . $_DistrictCode . "' ORDER BY UserCount ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAllRSP() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_User = $_SESSION['User_LoginId'];
            //$_SelectQuery = "Select Distinct Rsptarget_User"
            //        . " From tbl_rsptarget where Rsptarget_Status='Approved'";
            
            $_SelectQuery = "SELECT DISTINCT b.Organization_Name, c.Rsptarget_User FROM tbl_user_master as a INNER JOIN"
                    . " tbl_organization_detail as b on a.User_Code=b.Organization_User INNER JOIN"
                    . " tbl_rsptarget as c ON c.Rsptarget_User=a.User_LoginId"
                    . " WHERE Rsptarget_Status='Approved' ORDER BY Organization_Name ";


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateRSPStatus($_RSPCode) {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_RSPCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RSPCode);
				
                $_UpdateQuery = "Update tbl_rsptarget set Rsptarget_Status = 'Deactive' Where Rsptarget_Code IN ($_RSPCode)";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                
                $_SelectQuery = "Select User_MobileNo FROM tbl_user_master WHERE User_LoginId = '" . $_SESSION['User_LoginId'] . "'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Row1 = mysqli_fetch_array($_Response2[2]);
                $_Mobile = $_Row1['User_MobileNo'];
                $_SMS = "Dear Service Provider, Some of your Approved Districts have been De-Activated by RKCL. Kindly Login to MYRKCL for more details.";
                SendSMS($_Mobile, $_SMS);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
