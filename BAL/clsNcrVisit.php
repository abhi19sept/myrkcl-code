<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrVisit
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/smtp_class.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrVisit {
    //put your code here
    
    public function GetAOList() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Org_Ack=e.User_Ack INNER JOIN tbl_intake_master as f "
                    . " ON e.User_LoginId=f.Intake_Center INNER JOIN tbl_staff_detail as g "
                    . " ON e.User_LoginId=g.Staff_User INNER JOIN tbl_bank_account as h "
                    . " ON e.User_LoginId=h.Bank_User_Code INNER JOIN tbl_it_peripherals as i "
                    . " ON e.User_LoginId=i.IT_UserCode INNER JOIN tbl_premises_details as j "
                    . " ON e.User_LoginId=j.Premises_User INNER JOIN tbl_userprofile as k "
                    . " ON e.User_Code=k.UserProfile_User INNER JOIN tbl_rspitgk_mapping as l"
                    . " ON e.User_LoginId=l.Rspitgk_ItgkCode INNER JOIN tbl_payment_transaction as m "
                    . " ON e.User_LoginId=m.Pay_Tran_ITGK"
                    . " WHERE a.Org_Visit='Yes' AND m.Pay_Tran_Status = 'PaymentReceive' AND m.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                    . " AND e.User_PaperLess = 'Yes' AND e.User_Rsp = '" . $_SESSION['User_Code'] . "'";
                } else if ($_SESSION['User_UserRoll'] == 23){
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Org_Ack=e.User_Ack "
                    . " WHERE a.Org_Visit='Yes' AND e.User_PaperLess = 'Yes' "
                    . " AND a.Organization_District = '" . $_SESSION['Organization_District'] . "' AND a.Org_Final_Approval_Status='Approved' "
                    . "AND Org_Course_Allocation_Status='Pending'";        
                }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
             } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code "
                    . " WHERE e.User_LoginId = '" . $_CenterCode . "'";
                    //. " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function AddVisit($_AOCode,$_Date) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_AOCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AOCode);
				
            $_InsertQuery = "Insert Into tbl_ncr_visit(NCRVisit_Code,NCRVisit_UserCode,NCRVisit_UserRoll,"
                    . "NCRVisit_LoginId,NCRVisit_Center_Code,NCRVisit_Selected_Date) "
                    . "Select Case When Max(NCRVisit_Code) Is Null Then 1 Else Max(NCRVisit_Code)+1 End as NCRVisit_Code,"
                    . "'" . $_SESSION['User_Code'] . "' as NCRVisit_UserCode,"
                    . "'" . $_SESSION['User_UserRoll'] . "' as NCRVisit_UserRoll,"
                    . "'" . $_SESSION['User_LoginId'] . "' as NCRVisit_LoginId,"
                    . "'" . $_AOCode . "' as NCRVisit_Center_Code,"
                    . "'" . $_Date . "' as NCRVisit_Selected_Date"
                    . " From tbl_ncr_visit";
            $_DuplicateQuery = "Select * From tbl_ncr_visit Where NCRVisit_Center_Code='" . $_AOCode . "' AND
				NCRVisit_UserCode = '" . $_SESSION['User_Code'] . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
