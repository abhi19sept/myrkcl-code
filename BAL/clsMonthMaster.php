<?php
/**
 * Description of clsMonthMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsMonthMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Month_Code,Month_Name,"
                    . "Status_Name From tbl_month_master as a inner join tbl_status_master as b "
                    . "on a.Month_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Month_Code)
    {   //echo $_Month_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Month_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Month_Code);
			
            $_SelectQuery = "Select Month_Code,Month_Name,Month_Status"
                    . " From tbl_month_master Where Month_Code='" . $_Month_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Month_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Month_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Month_Code);
			
            $_DeleteQuery = "Delete From tbl_month_master Where Month_Code='" . $_Month_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_MonthName,$_MonthStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_MonthName = mysqli_real_escape_string($_ObjConnection->Connect(),$_MonthName);
			$_MonthStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_MonthStatus);
			
            $_InsertQuery = "Insert Into tbl_month_master(Month_Code,Month_Name,"
                    . "Month_Status) "
                    . "Select Case When Max(Month_Code) Is Null Then 1 Else Max(Month_Code)+1 End as Month_Code,"
                    . "'" . $_MonthName . "' as Month_Name,"
                    . "'" . $_MonthStatus . "' as Month_Status"
                    . " From tbl_month_master";
            $_DuplicateQuery = "Select * From tbl_month_master Where Month_Name='" . $_MonthName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_MonthCode,$_MonthName,$_MonthStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
				$_MonthCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_MonthCode);
				$_MonthName = mysqli_real_escape_string($_ObjConnection->Connect(),$_MonthName);
				$_MonthStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_MonthStatus);
				
            $_UpdateQuery = "Update tbl_month_master set Month_Name='" . $_MonthName . "',"
                    . "Month_Status='" . $_MonthStatus . "' Where Month_Code='" . $_MonthCode . "'";
            $_DuplicateQuery = "Select * From tbl_month_master Where Month_Name='" . $_MonthName . "' "
                    . "and Month_Code <> '" . $_MonthCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
