<?php

/**
 * Description of clsUpdExamResult
 *
 * @author Abhishek
 */

ini_set("memory_limit", "-1");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
ini_set("upload_max_filesize", "855M");
ini_set("post_max_size", "1000M");
ini_set("max_execution_time", "30000000000");
ini_set("max_input_time", "30000000000");
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUpdExamResult {

    //put your code here
    //print_r($_SESSION['DataArrayExamResult']);
    public function Addfinalexamresult($rows) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $UNIX_DATE = ($b - 25569) * 86400;
//            $b = gmdate("M-y", $UNIX_DATE);
//            $UNIX_DATE = ($c - 25569) * 86400;
//            $c = gmdate("Y-m-d H:i:s", $UNIX_DATE);
//            $UNIX_DATE = ($g - 25569) * 86400;
//            $g = gmdate("d-m-Y", $UNIX_DATE);
             $_TrunQuery = "TRUNCATE TABLE tbl_result_copy";
             $_Response2 = $_ObjConnection->ExecuteQuery($_TrunQuery, Message::OnUpdateStatement);
            $_InsertQuery = "INSERT INTO tbl_result_copy (scol_no, exam_event_name, exam_date, rollno, `name`, fname, dob, study_cen, net_marks, "
                    . "inte_marks, tot_marks, percentage, result, exameventnameID, examdate_ID,exam_held_date) "
                    . "VALUES " . $rows;
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function transferexamresult($eventname) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$eventname = mysqli_real_escape_string($_ObjConnection->Connect(),$eventname);
				
                // $_InsertQuery = "INSERT INTO tbl_result (scol_no, exam_event_name, exam_date, rollno, `name`, fname, dob, study_cen, net_marks, 
                //      inte_marks, tot_marks, percentage, result, exameventnameID, examdate_ID,exam_held_date) select scol_no, exam_event_name, exam_date, rollno, `name`, fname, dob, study_cen, net_marks, 
                //     inte_marks, tot_marks, percentage, result, exameventnameID, examdate_ID,exam_held_date from tbl_result_copy where exameventnameID= '".$eventname."'";
            $_InsertQuery = "INSERT INTO tbl_result (scol_no, exam_event_name, exam_date, rollno, `name`, fname, dob, study_cen, net_marks, 
                     inte_marks, tot_marks, percentage, result, exameventnameID, examdate_ID,exam_held_date, IsNewOnlineLMSRecord) select scol_no, exam_event_name, exam_date, rollno, `name`, fname, dob, study_cen, net_marks, 
                    inte_marks, tot_marks, percentage, result, exameventnameID, examdate_ID,exam_held_date,'Y' from tbl_result_copy where exameventnameID= '".$eventname."'";
                    
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function transferexamresultfinal($eventname, $result_date) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$eventname = mysqli_real_escape_string($_ObjConnection->Connect(),$eventname);
				$result_date = mysqli_real_escape_string($_ObjConnection->Connect(),$result_date);
				
            $_InsertQuery = "INSERT INTO tbl_final_result (scol_no, exam_event_name, exam_date, rollno, `name`, fname, dob, study_cen, net_marks, 
inte_marks, tot_marks, percentage, result, exameventnameID, examdate_ID,exam_held_date, result_date)
 select scol_no, exam_event_name, exam_date, rollno, `name`, fname, dob, study_cen, net_marks, inte_marks, tot_marks, percentage, result, exameventnameID, examdate_ID,
exam_held_date,'".$result_date."' from tbl_result_copy where exameventnameID= '".$eventname."' and result = 'PASS' and scol_no not in  (SELECT scol_no FROM tbl_final_result where result = 'PASS')";
                    
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function Addfinalexamellist($rows) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $UNIX_DATE = ($b - 25569) * 86400;
//            $b = gmdate("M-y", $UNIX_DATE);
//            $UNIX_DATE = ($c - 25569) * 86400;
//            $c = gmdate("Y-m-d H:i:s", $UNIX_DATE);
//            $UNIX_DATE = ($g - 25569) * 86400;
//            $g = gmdate("d-m-Y", $UNIX_DATE);
            $_InsertQuery = "INSERT INTO tbl_eligiblelearners_copy (eventname, eventid, learnercode, learnername, fathername,"
                    . " dob, coursename, batchname, itgkcode, itgkname, itgkdistrict, itgktehsil, remark, status, examdate, "
                    . "Reason) VALUES " . $rows;
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Addfinalexamschedule($rows) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $UNIX_DATE = ($b - 25569) * 86400;
//            $b = gmdate("M-y", $UNIX_DATE);
//            $UNIX_DATE = ($c - 25569) * 86400;
//            $c = gmdate("Y-m-d H:i:s", $UNIX_DATE);
//            $UNIX_DATE = ($g - 25569) * 86400;
//            $g = gmdate("d-m-Y", $UNIX_DATE);
          echo  $_InsertQuery = "INSERT INTO tbl_finalexammapping_copy (learnercode, learnername, fathername, dob,tac,"
                    . " coursename, batchname, centercode, centername, examcentercode, examcentername,examcenterdistrict, "
                    . "examcentertehsil, examcenteraddress, examcentercapacity, mobileno, email, noofrooms, coname, eventname,"
                    . " rollno, examid) VALUES " . $rows;
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
