<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsUpdateWcdLearnerStatus {

    //put your code here
	
public function GETITGKLEARNERDATA($_ITGK)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
				
             $_SelectQuery = "Select a.*,b.Category_Name,c.Qualification_Name From tbl_oasis_admission as a INNER JOIN tbl_category_master as b 
								on a.Oasis_Admission_Category = b.Category_Code INNER JOIN tbl_qualification_master as c
								on a.Oasis_Admission_Qualification = c.Qualification_Code Where 
								Oasis_Admission_Final_Preference='" . $_ITGK . "' AND 
								Oasis_Admission_LearnerStatus in ('Approved','Rejected')";
								
			 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GETLEARNERDATA($_Lcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				
             $_SelectQuery = "Select a.*,b.Category_Name,c.Qualification_Name From tbl_oasis_admission as a INNER JOIN tbl_category_master as b 
								on a.Oasis_Admission_Category = b.Category_Code INNER JOIN tbl_qualification_master as c
								on a.Oasis_Admission_Qualification = c.Qualification_Code Where Oasis_Admission_LearnerCode='" . $_Lcode . "' AND 
								Oasis_Admission_LearnerStatus in ('Approved','Rejected')";
								
			 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function UpdateWcdLearnerCode($_LCode, $_ProcessStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {		
				$_LCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LCode);
				
           $_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_LearnerStatus='Pending', Oasis_Admission_Reason=''"                   
                    . " Where Oasis_Admission_LearnerCode='" . $_LCode . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateWcdLearnerITGK($_ITGK, $_ProcessStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {		
				$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
				
           $_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_LearnerStatus='Pending', Oasis_Admission_Reason=''"                   
                    . " Where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_LearnerStatus in ('Approved','Rejected')";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
}
