<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsSlaReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsSlaReport {
    //put your code here
    
//     public function GetAllCourse() {
//        global $_ObjConnection;
//        $_ObjConnection->Connect();
//        try {
//            $_SelectQuery = "Select Distinct Courseitgk_Course From tbl_courseitgk_mapping order by Courseitgk_EOI";
//            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
//        } catch (Exception $_ex) {
//            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
//            $_Response[1] = Message::Error;
//        }
//        return $_Response;
//    }

    public function GetITGK_SLA_Details() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '11' || $_LoginRole == '30') {
//                $_SelectQuery = "select a.ITGKCODE, a.ITGK_Name, a.District_Name, a.RSP_Name, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate
//                                from vw_itgkname_distict_rsp as a  inner join tbl_courseitgk_mapping as b on a.ITGKCODE=b.Courseitgk_ITGK 
//                                where Courseitgk_Course='RS-CIT' and b.CourseITGK_ExpireDate < NOW()";
                
                
//                 $_SelectQuery = "Select DISTINCT a.*, b.Courseitgk_Course, b.Courseitgk_ITGK AS ITGKCODE, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
//				c.Organization_Name as ITGK_Name, c.*,
//				g.Organization_Name as RSP_Name From tbl_user_master as a 
//                                INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
//                                INNER join tbl_organization_detail as c on a.User_Code=c.Organization_User
//				
//				INNER join tbl_user_master as f on a.User_Rsp=f.User_Code
//				INNER join tbl_organization_detail as g on f.User_Code=g.Organization_User
//                                LEFT JOIN tbl_org_master as h on a.User_Ack=h.Org_Ack
//				WHERE b.Courseitgk_Course='RS-CIT' AND CURDATE() > b.CourseITGK_ExpireDate";
                 
                $_SelectQuery = "Select DISTINCT a.*, b.Courseitgk_Course, b.Courseitgk_ITGK AS ITGKCODE, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
				c.Organization_Name as ITGK_Name, c.*, j.Block_Name, k.GP_Name,
				g.Organization_Name as RSP_Name, Ncr_Transaction_Amount From tbl_user_master as a 
                                INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
                                INNER join tbl_organization_detail as c on a.User_Code=c.Organization_User
				
				INNER join tbl_user_master as f on a.User_Rsp=f.User_Code
				INNER join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_org_master as h on a.User_Ack=h.Org_Ack
                                LEFT JOIN tbl_ncr_transaction as i on h.Org_TranRefNo=i.Ncr_Transaction_Txtid
                                LEFT JOIN tbl_panchayat_samiti as j on c.Organization_Panchayat=j.Block_Code
                                LEFT JOIN tbl_gram_panchayat as k on c.Organization_Gram=k.GP_Code
				WHERE b.Courseitgk_Course='RS-CIT' AND CURDATE() > b.CourseITGK_ExpireDate";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '14') {
                $_SelectQuery = "Select DISTINCT a.*, b.Courseitgk_Course, b.Courseitgk_ITGK AS ITGKCODE, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
				c.Organization_Name as ITGK_Name, c.*, j.Block_Name, k.GP_Name,
				g.Organization_Name as RSP_Name, Ncr_Transaction_Amount From tbl_user_master as a 
                                INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
                                INNER join tbl_organization_detail as c on a.User_Code=c.Organization_User
				
				INNER join tbl_user_master as f on a.User_Rsp=f.User_Code
				INNER join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_org_master as h on a.User_Ack=h.Org_Ack
                                LEFT JOIN tbl_ncr_transaction as i on h.Org_TranRefNo=i.Ncr_Transaction_Txtid
                                LEFT JOIN tbl_panchayat_samiti as j on c.Organization_Panchayat=j.Block_Code
                                LEFT JOIN tbl_gram_panchayat as k on c.Organization_Gram=k.GP_Code
				WHERE b.Courseitgk_Course='RS-CIT' AND CURDATE() > b.CourseITGK_ExpireDate AND f.User_LoginId='" . $_SESSION['User_LoginId'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
         public function GetITGK_Admission_Details($_CenterCode, $_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				
            $SDate = $_StartDate.' 00:00:00';
            $EDate = $_EndDate.' 00:00:00';
            $_SelectQuery = "select count(Admission_Code) as Admission_Count from tbl_admission where Admission_ITGK_Code='" . $_CenterCode . "' 
                                and Admission_Date_Payment>'" . $SDate . "' and Admission_Date_Payment<='" . $EDate . "' and Admission_Payment_Status='1'"
                    . " and Admission_Course in (1,4)";
        
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRenewalPenaltyAmount() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Renewal_Penalty_Amount from tbl_renewal_penalty_master where Renewal_Penalty_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
