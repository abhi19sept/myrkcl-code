<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrFeedback
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');

$_ObjConnection = new _Connection();
$_Response = array();

date_default_timezone_set('Asia/Calcutta');

class clsNcrFeedback {
    //put your code here
    
    public function AddFeedback($_SPName, $_AOCode, $_ITGKName, $_AddressYesNo, $_Country,$_State, $_Region, $_District, $_Tehsil,
                $_PinCode, $_Address, $_Landmark, $_Police, $_AreaType, $_MunicipalType, $_MunicipalName, $_WardNo, 
                $_Village, $_Gram, $_Panchayat, $_OwnerName, $_OwnerMobNo, $_FacultyName, $_FacultyQuali, $_Separate,
                $_TheoryArea, $_LabArea, $_TotalArea, $_Furniture, $_Electrict, $_Water, $_Toilet, $_Deaktop, $_Laptop, 
                $_NComputing, $_Printer, $_Webcam, $_Biometric, $_Internet, $_Backup, $_AO_Infrastructure, 
                $_AO_Faculty_Quality, $_Overall_Remarks,$_SPOfficialName, $_SPOfficialMob) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_Date = date("Y-m-d h:i:s");
        
    //    $_SMS = "Dear Applicant, " . $_OrgName . " Your Application for " . $type . " has been Submitted to RKCL. Once your Submitted Information is Verified, you will get your MYRKCL login Details on your Registered Mobile Number.";
    //    $_SMSRSP = "Dear SP, IT-GK " . $_OrgName . " has been registered on MYRKCL. Kindly Verify and Approve Submitted Information. MYRKCL login Details will be sent to IT-GK Registered Mobile Number.";
        
    //    $_Email = "Dear Applicant, " . $_OrgName . " Your Application for " . $type . " has been Submitted to RKCL. Once your Submitted Information is Verified, you will get your MYRKCL login Details on your Registered Mobile Number.";
    //    $_EmailRSP = "Dear SP, IT-GK " . $_OrgName . " has been registered on MYRKCL. Kindly Verify and Approve Submitted Information. MYRKCL login Details will be sent to IT-GK Registered Mobile Number.";
        
        try {

            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {             
			$_AOCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AOCode);
			
                $_InsertQuery = "Insert into tbl_ncr_feedback 
	(NCRFeedback_User, NCRFeedback_SPName, NCRFeedback_AOCode, NCRFeedback_AOName, NCRFeedback_Location, 
	NCRFeedback_Country, NCRFeedback_State, NCRFeedback_Region, NCRFeedback_District, NCRFeedback_Tehsil, NCRFeedback_Pincode, 
	NCRFeedback_Address, NCRFeedback_Landmark, NCRFeedback_Police, NCRFeedback_AreaType, NCRFeedback_MunicipalityType, 
	NCRFeedback_MunicipalityName, NCRFeedback_WardNo, NCRFeedback_PanchayatSamiti, NCRFeedback_GramPanchayat, 
	NCRFeedback_Village, NCRFeedback_OwnerName, NCRFeedback_OwnerMob, NCRFeedback_FacultyName, NCRFeedback_Qualification, 
	NCRFeedback_Furniture, NCRFeedback_Electricity, NCRFeedback_Water, NCRFeedback_Toilet, NCRFeedback_Separate, 
	NCRFeedback_TheoryArea, NCRFeedback_LABArea, NCRFeedback_TotalArea, NCRFeedback_Desktop, NCRFeedback_Laptop, 
	NCRFeedback_NComputing, NCRFeedback_Printer, NCRFeedback_Webcam, NCRFeedback_Biometric, NCRFeedback_Internet, 
	NCRFeedback_Backup, NCRFeedback_AO_Infrastructure, NCRFeedback_AO_Faculty_Quality, NCRFeedback_Overall_Remark, 
	NCRFeedback_SPOfficialName, NCRFeedback_SPOfficialMob, NCRFeedback_Date)
	values
	('" . $_SESSION['User_LoginId'] . "', '" . $_SPName . "', '" . $_AOCode . "', '" . $_ITGKName . "', '" . $_AddressYesNo . "', 
	'" . $_Country . "', '" . $_State . "', '" . $_Region . "', '" . $_District . "', '" . $_Tehsil . "', '" . $_PinCode . "', 
	'" . $_Address . "', '" . $_Landmark . "', '" . $_Police . "', '" . $_AreaType . "', '" . $_MunicipalType . "', 
	'" . $_MunicipalName . "', '" . $_WardNo . "', '" . $_Panchayat . "', '" . $_Gram . "', 
	'" . $_Village . "', '" . $_OwnerName . "', '" . $_OwnerMobNo . "', '" . $_FacultyName . "', '" . $_FacultyQuali . "', 
	'" . $_Furniture . "', '" . $_Electrict . "', '" . $_Water . "', '" . $_Toilet . "', '" . $_Separate . "', 
	'" . $_TheoryArea . "', '" . $_LabArea . "', '" . $_TotalArea . "', '" . $_Deaktop . "', '" . $_Laptop . "', 
	'" . $_NComputing . "', '" . $_Printer . "', '" . $_Webcam . "', '" . $_Biometric . "', '" . $_Internet . "', 
	'" . $_Backup . "', '" . $_AO_Infrastructure . "', '" . $_AO_Faculty_Quality . "', '" . $_Overall_Remarks . "', 
	'" . $_SPOfficialName . "', '" . $_SPOfficialMob . "', '" . $_Date . "')";

                $_DuplicateQuery = "Select * From tbl_ncr_feedback Where NCRFeedback_User='" . $_SESSION['User_LoginId'] . "' and  NCRFeedback_AOCode = '" . $_AOCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                if ($_Response[0] == Message::NoRecordFound) {
                        if($_AreaType == 'Urban') {
					if($_MunicipalName == '0' || $_MunicipalType == '0' || $_WardNo == '0') {
						 echo " Please select all fields";
						 return;
					}
                        }
                        elseif($_AreaType == 'Rural') {
                         if($_Gram =='00' || $_Panchayat =='0' || $_Village =='0') {
						 echo " Please select all fields";
						 return;
					}
                        }
					
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//                        SendSMS($_OrgMobile, $_SMS);
//                        SendSMS($_RspMobile, $_SMSRSP);
                        
//                        $Admin_Name='RKCL';
//			$mail_data = array();
//			$mail_data['email'] = $_OrgEmail;
//			$mail_data['admin'] = $Admin_Name;
//						
//			$mail_data['Msg'] = $_Email;
//			$subject = "Registration Successfulll on MYRKCL";						
//			$sendMail = new sendMail();
//			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
//                        
//                        $Admin_Name='RKCL';
//			$mail_data = array();
//			$mail_data['email'] = $_RspEmail;
//			$mail_data['admin'] = $Admin_Name;
//						
//			$mail_data['Msg'] = $_EmailRSP;
//			$subject = "Registration Successfulll on MYRKCL";						
//			$sendMail = new sendMail();
//			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
                                        
                } else {
                    echo " Feedback Already Exists.";
                    return;                     
                }
                } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
    
    public function GetITGK_Details($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
           /**     $_SelectQuery = "Select DISTINCT a.*, c.Organization_Name as ITGK_Name, c.*, d.District_Name,
                                e.Tehsil_Name,f.User_LoginId as RSP_Code, r.Region_Name,
                                g.Organization_Name as RSP_Name, 
                                j.Municipality_Type_Name, k.Municipality_Name, l.Ward_Name, 
                                m.Block_Name, n.GP_Name, o.Village_Name, q.Org_Type_Name From tbl_user_master as a 
                                LEFT join tbl_organization_detail as c on a.User_Code=c.Organization_User
                                LEFT join tbl_org_type_master as q on c.Organization_Type=q.Org_Type_Code
                                LEFT join tbl_district_master as d on c.Organization_District=d.District_Code
                                LEFT join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
                                LEFT join tbl_municipality_type as j on c.Organization_Municipal_Type=j.Municipality_Type_Code
                                LEFT join tbl_municipality_master as k on c.Organization_Municipal=k.Municipality_Code
                                LEFT join tbl_ward_master as l on c.Organization_WardNo=l.Ward_Code
                                LEFT join tbl_panchayat_samiti as m on c.Organization_Panchayat=m.Block_Code
                                LEFT join tbl_gram_panchayat as n on c.Organization_Gram=n.GP_Code
                                LEFT join tbl_village_master as o on c.Organization_Village=o.Village_Code
                                LEFT join tbl_user_master as f on a.User_Rsp=f.User_Code
                                LEFT join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_region_master as r on c.Organization_Region=r.Region_Code
                                WHERE a.User_UserRoll in ('7','15') and a.User_LoginId='" . $_CenterCode . "'";
								**/
								$_SelectQuery = "Select DISTINCT a.*, c.Organization_Name as ITGK_Name, c.*, d.District_Name,
                                e.Tehsil_Name,f.User_LoginId as RSP_Code, r.Region_Name,
                                g.Organization_Name as RSP_Name, 
                                j.Municipality_Type_Name, k.Municipality_Name, l.Ward_Name, 
                                m.Block_Name, n.GP_Name, o.Village_Name, q.Org_Type_Name From tbl_user_master as a 
                                LEFT join tbl_organization_detail as c on a.User_Code=c.Organization_User
                                LEFT join tbl_org_type_master as q on c.Organization_Type=q.Org_Type_Code
                                LEFT join tbl_district_master as d on c.Organization_District=d.District_Code
                                LEFT join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
                                LEFT join tbl_municipality_type as j on c.Organization_Municipal_Type=j.Municipality_Type_Code
                                LEFT join tbl_municipality_master as k on c.Organization_Municipal=k.Municipality_Raj_Code
                                LEFT join tbl_ward_master as l on c.Organization_WardNo=l.Ward_Code
                                LEFT join tbl_panchayat_samiti as m on c.Organization_Panchayat=m.Block_Code
                                LEFT join tbl_gram_panchayat as n on c.Organization_Gram=n.GP_Code
                                LEFT join tbl_village_master as o on c.Organization_Village=o.Village_Code
                                LEFT join tbl_user_master as f on a.User_Rsp=f.User_Code
                                LEFT join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_region_master as r on c.Organization_Region=r.Region_Code
                                WHERE a.User_UserRoll in ('7','15') and a.User_LoginId='" . $_CenterCode . "'";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
}
