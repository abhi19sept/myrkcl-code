<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsCenterRegistration
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCenterRegistration {

    //put your code here

    public function Add($_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_State, $_Region,
            $_District, $_Tehsil, $_Landmark, $_Road, $_PinCode, $_OrgEstDate, $_OrgCountry, $_docproof, $_doctype, 
            $_PanNo, $_AreaType, $_MunicipalType, $_WardNo, $_Police, $_MunicipalName, $_Village, $_Gram, $_Panchayat) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();
        if ($_OrgAOType == '14') {
            $type = 'RKCL Service Provider';
        } elseif ($_OrgAOType == '15') {
            $type = 'IT-GK';
        }
        $_SMS = "Dear Applicant, " . $_OrgName . " Your Application for " . $type . " has been Submitted to RKCL. Once your Submitted Information is Verified, you will get your MYRKCL login Details on your Registered Mobile Number.";
        $_SMSRSP = "Dear SP, IT-GK " . $_OrgName . " has been registered thru your MYRKCL login. Once your Submitted Information is Verified, MYRKCL login Details will be sent to IT-GK Registered Mobile Number.";
        try {
            $_OrgMobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgMobile);
            $_OrgEmail = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgEmail);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_photoname = $_docproof . '_orgdoc' . '.png';
                $_RspLoginId = $_SESSION['User_LoginId'];
                
                $_SelectQuery = "Select * FROM tbl_user_master WHERE User_LoginId = '" . $_RspLoginId . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Row = mysqli_fetch_array($_Response1[2]);                
                $_RspMobile = $_Row['User_MobileNo'];                

                $_InsertQuery = "Insert Into tbl_org_master(Organization_Code,Org_Ack,Org_RspLoginId,Organization_Name,"
                        . "Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,"
                        . "Organization_DocType,Organization_ScanDoc,Organization_State,Organization_Region,Organization_District,"
                        . "Organization_Tehsil,Organization_Landmark,Organization_Road,"
                        . "Organization_Country,"
                        . "Org_Role,Org_Email,Org_Mobile,Org_PAN,Org_AreaType,Org_Municipal_Type,Org_WardNo,Org_Police,"
                        . "Org_Municipal,Org_Village,Org_Gram,Org_Panchayat,Org_PinCode) "
                        . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                        . " Case When Max(Org_Ack) Is Null Then 4000 Else Max(Org_Ack)+1 End as Org_Ack,"
                        . "'" . $_RspLoginId . "' as Org_RspLoginId,'" . $_OrgName . "' as Organization_Name,'" . $_OrgRegno . "' as Organization_RegistrationNo,"
                        . "'" . $_OrgEstDate . "' as Organization_FoundedDate,  '" . $_OrgType . "' as Organization_Type,"
                        . "'" . $_doctype . "' as Organization_DocType,'" . $_photoname . "' as Organization_ScanDoc,"
                        . "'" . $_State . "' as Organization_State,'" . $_Region . "' as Organization_Region,'" . $_District . "' as Organization_District,"
                        . "'" . $_Tehsil . "' as Organization_Tehsil,"
                        . "'" . $_Landmark . "' as Organization_Landmark,'" . $_Road . "' as Organization_Road,"                        
                        . "'" . $_OrgCountry . "' as Organization_Country,"                       
                        . "'" . $_OrgAOType . "' as Org_Role,'" . $_OrgEmail . "' as Org_Email,'" . $_OrgMobile . "' as Org_Mobile,"
                        . "'" . $_PanNo . "' as Org_PAN,'" . $_AreaType . "' as Org_AreaType,"
                        . "'" . $_MunicipalType . "' as Org_Municipal_Type,'" . $_WardNo . "' as Org_WardNo,"
                        . "'" . $_Police . "' as Org_Police,"
                        . "'" . $_MunicipalName . "' as Org_Municipal,'" . $_Village . "' as Org_Village,"
                        . "'" . $_Gram . "' as Org_Gram,'" . $_Panchayat . "' as Org_Panchayat,'" . $_PinCode . "' as Org_PinCode"
                        . " From tbl_org_master";

                $_DuplicateQuery = "Select * From tbl_org_master Where Org_Mobile='" . $_OrgMobile . "' OR Org_Email = '" . $_OrgEmail . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                $_DuplicateQuery1 = "Select * From tbl_user_master Where User_MobileNo='" . $_OrgMobile . "' OR User_EmailId = '" . $_OrgEmail . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);


                if ($_Response[0] == Message::NoRecordFound && $_Response1[0] == Message::NoRecordFound) {
                    $lphotodoc =  '../upload/orgdocupload/' . $_photoname;
//                $lsigndoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/' . $_signname;


                    if (file_exists($lphotodoc)) {
                        if($_AreaType == 'Urban') {
					if($_MunicipalName == '0' || $_MunicipalType == '0' || $_WardNo == '0') {
						 echo " Please select all fields";
						 return;
					}
                        }
                        elseif($_AreaType == 'Rural') {
                         if($_Gram =='0' || $_Panchayat =='0' || $_Village =='0') {
						 echo " Please select all fields";
						 return;
					}
                        }
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                        SendSMS($_OrgMobile, $_SMS);
                        SendSMS($_RspMobile, $_SMSRSP);
                    } else {
                        echo "Document not attached successfully, Please Re-Upload Document";
                        return;
                    }
                } else {
//                    $_Response[0] = Message::DuplicateRecord;
//                    $_Response[1] = Message::Error;
                    echo " Mobile Number or Email Id Already Exists.";
                    return;                     
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function FILLStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT DISTINCT Org_Status FROM tbl_org_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function FILLDistrict($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $region = mysqli_real_escape_string($_ObjConnection->Connect(),$region);
            $_SelectQuery = "Select District_Code,District_Name From tbl_district_master as a 
                            INNER JOIN tbl_rsptarget as b on a.district_Code=b.Rsptarget_District 
                            Where a.District_Region='" . $region . "' and b.Rsptarget_Status='Approved' 
                            and b.Rsptarget_User='" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll($_Role, $_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Role = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role);
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code WHERE Org_Role = '" . $_Role . "' AND Org_Status = '" . $_Status . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_OrgCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code WHERE Organization_Code = '" . $_OrgCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateOrgMaster($_OrgCode, $_Status, $_Remark) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            $_Remark = mysqli_real_escape_string($_ObjConnection->Connect(),$_Remark);
            $_UpdateQuery = "Update tbl_org_master set Org_Status='Rejected', Org_Remark='" . $_Remark . "'"
                    . " Where Organization_Code='" . $_OrgCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateToCreateLogin($_OrgCode, $_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            
            $_SelectQuery = "Select * FROM tbl_org_master WHERE Organization_Code = '" . $_OrgCode . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response1[2]);
            $_Email = $_Row['Org_Email'];
            $_mobile = $_Row['Org_Mobile'];
            $_roll = $_Row['Org_Role'];
            $_Status = '1';

            $_OrgName = $_Row['Organization_Name'];
            $_OrgRegno = $_Row['Organization_RegistrationNo'];
            $_OrgEstDate = $_Row['Organization_FoundedDate'];
            $_OrgType = $_Row['Organization_Type'];
            $_doctype = $_Row['Organization_DocType'];
            $_photoname = $_Row['Organization_ScanDoc'];
            $_State = $_Row['Organization_State'];
            $_Region = $_Row['Organization_Region'];
            $_District = $_Row['Organization_District'];
            $_Tehsil = $_Row['Organization_Tehsil'];
            $_Landmark = $_Row['Organization_Landmark'];
            $_Road = $_Row['Organization_Road'];
            $_Street = $_Row['Organization_Street'];
            $_HouseNo = $_Row['Organization_HouseNo'];
            $_OrgCountry = $_Row['Organization_Country'];

            function PASSWORD($length = 6, $chars = 'abcdefghijklmnopqrstuvwxyz1234567890') {
                $chars_length = (strlen($chars) - 1);
                $string = $chars{rand(0, $chars_length)};
                for ($i = 1; $i < $length; $i = strlen($string)) {
                    $r = $chars{rand(0, $chars_length)};
                    if ($r != $string{$i - 1})
                        $string .= $r;
                }
                return $string;
            }

            if ($_Row['Org_Role'] == '14') {
                $_SelectQuery = "Select Aologin_Username FROM tbl_aologin_code WHERE Aologin_Status = '0' ORDER BY Aologin_Code ASC LIMIT 1";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Row1 = mysqli_fetch_array($_Response2[2]);
                $_USERNAME = $_Row1['Aologin_Username'];
                $_UpdateQuery1 = "Update tbl_aologin_code set Aologin_Status = '1'"
                        . " Where Aologin_Username='" . $_USERNAME . "'";
                $_Response3 = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            }
            $_PASSWORD = PASSWORD();

            $_SMS = "Your Login details for submitted application in MYRKCL is User Name: " . $_USERNAME . " Password: " . $_PASSWORD . "";

            $_InsertQuery = "Insert Into tbl_user_master(User_Code,User_EmailId,User_MobileNo,User_LoginId,User_Password,"
                    . " User_UserRoll,User_ParentId,User_Status)"
                    . "Select Case When Max(User_Code) Is Null Then 1 Else Max(User_Code)+1 End as User_Code,"
                    . "'" . $_Email . "' as User_EmailId,'" . $_mobile . "' as User_MobileNo,"
                    . "'" . $_USERNAME . "' as User_LoginId,'" . $_PASSWORD . "' as User_Password,"
                    . "'" . $_roll . "' as User_UserRoll,'1' as User_ParentId,"
                    . "'" . $_Status . "' as User_Status From tbl_user_master";
            //echo $_InsertQuery;
            $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            $_SelectQuery1 = "Select User_Code FROM tbl_user_master WHERE User_EmailId = '" . $_Email . "' AND User_MobileNo = '" . $_mobile . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row2 = mysqli_fetch_array($_Response5[2]);
            $_usercode = $_Row2['User_Code'];

            $_InsertQuery1 = "Insert Into  tbl_organization_detail(Organization_Code,Organization_User,Organization_Name,"
                    . "Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,"
                    . "Organization_DocType,Organization_ScanDoc,Organization_State,Organization_Region,Organization_District,"
                    . "Organization_Tehsil,Organization_Landmark,Organization_Road,Organization_Street,"
                    . "Organization_HouseNo,Organization_Country)"
                    . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                    . "'" . $_usercode . "' as Organization_User,'" . $_OrgName . "' as Organization_Name,'" . $_OrgRegno . "' as Organization_RegistrationNo,"
                    . "'" . $_OrgEstDate . "' as Organization_FoundedDate,  '" . $_OrgType . "' as Organization_Type,"
                    . "'" . $_doctype . "' as Organization_DocType,'" . $_photoname . "' as Organization_ScanDoc,"
                    . "'" . $_State . "' as Organization_State,'" . $_Region . "' as Organization_Region,'" . $_District . "' as Organization_District,"
                    . "'" . $_Tehsil . "' as Organization_Tehsil,"
                    . "'" . $_Landmark . "' as Organization_Landmark,'" . $_Road . "' as Organization_Road,"
                    . "'" . $_Street . "' as Organization_Street,'" . $_HouseNo . "' as Organization_HouseNo,"
                    . "'" . $_OrgCountry . "' as Organization_Country"
                    . " From  tbl_organization_detail";
            $_Response6 = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);


            $_UpdateQuery = "Update tbl_org_master set Org_Status='Approved'"
                    . " Where Organization_Code='" . $_OrgCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            SendSMS($_mobile, $_SMS);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAllPanchayat($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_District_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_District_Code);
            
            $_SelectQuery = "Select Block_Code,Block_Name"
                    . " From tbl_panchayat_samiti Where Block_District='" . $_District_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllGramPanchayat($_Panchayat_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Panchayat_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Panchayat_Code);
            $_SelectQuery = "Select GP_Code,GP_Name"
                    . " From  tbl_gram_panchayat Where GP_Block='" . $_Panchayat_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

}
