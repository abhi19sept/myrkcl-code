<?php

/**
 * Description of clsLearnerExamDetails
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsLearnerExamDetails {

    //put your code here
    public function GetLearnerAllMarks($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
					
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id 
                                        where a.scol_no ='" . $_actionvalue . "'";
            } elseif ($_LoginRole == '14') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id inner join tbl_user_master as c on a.study_cen=c.User_LoginId 
                                        where a.scol_no ='" . $_actionvalue . "' and c.User_Rsp='" . $_SESSION['User_Code'] . "'";
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id 
                                        where a.scol_no ='" . $_actionvalue . "' and study_cen='" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    //put your code here
    public function GetLearnerAllMarksbyrollno($rollno, $examevent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			$rollno = mysqli_real_escape_string($_ObjConnection->Connect(),$rollno);
			$examevent = mysqli_real_escape_string($_ObjConnection->Connect(),$examevent);
			
//            $_SelectQuery = "select a.*, b.Event_Name 
//                                        from tbl_result a
//                                        left join tbl_events b 
//                                        on a.exameventnameID= b.Event_Id 
//                                        where a.rollno ='" . $rollno . "' and exameventnameID='" . $examevent . "'";
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id
                                        where a.rollno ='" . $rollno . "' and exameventnameID='" . $examevent . "'";
            } elseif ($_LoginRole == '14') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id  inner join tbl_user_master as c on a.study_cen=c.User_LoginId 
                                        where a.rollno ='" . $rollno . "' and exameventnameID='" . $examevent . "' and c.User_Rsp='" . $_SESSION['User_Code'] . "'";
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id 
                                        where a.rollno ='" . $rollno . "' and exameventnameID='" . $examevent . "' and study_cen='" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Getbyall($lanme, $ccode, $txtLdob) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			
			$lanme = mysqli_real_escape_string($_ObjConnection->Connect(),$lanme);
			$ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$ccode);
			
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30') {

                $_SelectQuery = "select a.*,b.* from  tbl_result as a left join tbl_events b on a.exameventnameID= b.Event_Id where `name` like '%" . $lanme . "%' and dob like '%" . $txtLdob . "%' and study_cen like '%" . $ccode . "%'";
            } elseif ($_LoginRole == '14') {

                echo $_SelectQuery = "select a.*,b.* from  tbl_result as a left join tbl_events b on a.exameventnameID= b.Event_Id inner join tbl_user_master as c on a.study_cen=c.User_LoginId  where `name` like '%" . $lanme . "%' and dob like '%" . $txtLdob . "%' and study_cen like '%" . $ccode . "%'"
                . " and c.User_Rsp='" . $_SESSION['User_Code'] . "'";
            } elseif ($_LoginRole == '7') {

                $_SelectQuery = "select a.*,b.* from  tbl_result as a left join tbl_events b on a.exameventnameID= b.Event_Id where `name` like '%" . $lanme . "%' and dob like '%" . $txtLdob . "%'"
                        . " and study_cen='" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Getbysearch($study_cen, $scol_no) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			
			$study_cen = mysqli_real_escape_string($_ObjConnection->Connect(),$study_cen);
			$scol_no = mysqli_real_escape_string($_ObjConnection->Connect(),$scol_no);
			
            $_SelectQuery = "select * from tbl_result where scol_no='" . $scol_no . "' and study_cen='" . $study_cen . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerReexam($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from examdata a
                                        left join tbl_events b 
                                        on a.examid= b.Event_Id 
                                        where a.learnercode ='" . $_actionvalue . "'";
            } elseif ($_LoginRole == '14') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id inner join tbl_user_master as c on a.learnercode=c.User_LoginId 
                                        where a.learnercode ='" . $_actionvalue . "' and c.User_Rsp='" . $_SESSION['User_Code'] . "'";
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.examid= b.Event_Id 
                                        where a.learnercode ='" . $_actionvalue . "' and itgkcode='" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Getbyallreexam($lanme, $ccode, $txtLdob) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			
			$lanme = mysqli_real_escape_string($_ObjConnection->Connect(),$lanme);
			$ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$ccode);
			
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30') {

                $_SelectQuery = "select * from  examdata where `learnername` like '%" . $lanme . "%' and dob like '%" . $txtLdob . "%' and itgkcode like '%" . $ccode . "%'";
            } elseif ($_LoginRole == '14') {

                echo $_SelectQuery = "select * from  examdata as a inner join tbl_user_master as c on a.itgkcode=c.User_LoginId  where `learnername` like '%" . $lanme . "%' and dob like '%" . $txtLdob . "%' and itgkcode like '%" . $ccode . "%'"
                . " and c.User_Rsp='" . $_SESSION['User_Code'] . "'";
            } elseif ($_LoginRole == '7') {

                $_SelectQuery = "select * from  examdata where `learnername` like '%" . $lanme . "%' and dob like '%" . $txtLdob . "%'"
                        . " and study_cen='" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Getbysearchreexam($study_cen, $scol_no) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
			
			$study_cen = mysqli_real_escape_string($_ObjConnection->Connect(),$study_cen);
			$scol_no = mysqli_real_escape_string($_ObjConnection->Connect(),$scol_no);
			
            $_SelectQuery = "select a.*,b.Event_Name from examdata as a  left join tbl_events b on a.examid= b.Event_Id  where learnercode='" . $scol_no . "' and itgkcode='" . $study_cen . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
