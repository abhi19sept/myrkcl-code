<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsAdmissionSummary {

    //put your code here

    public function GetDataAll($_Role, $_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Role = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role);
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            //$_UserRole = $_SESSION['User_UserRoll'];
            $_SelectedRole = $_Role;
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '28') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } 
			elseif ($_LoginRole == '14') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "8";
            } 
			
			else {
                echo "hello";
            }

            $_SESSION['UserType'] = $_LoginUserType;

            if ($_loginflag == "1") {

					$_SelectQuery3 = "SELECT tbl_admission.Admission_ITGK_Code AS RoleName, "
                        . "tbl_admission.Admission_ITGK_Code AS RoleCode, Count(tbl_admission.Admission_Code) "
                        . "AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch,"
                        . "sum(case when tbl_admission.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount "
                        . "FROM tbl_admission WHERE tbl_admission.Admission_Course = '" . $_course . "' "
                        . "AND tbl_admission.Admission_Batch = '" . $_batch . "' "
                        . "group by tbl_admission.Admission_ITGK_Code";
			} 
			elseif ($_loginflag == "8") 
			{
				
					    $_SelectQuery3 = "SELECT a.Admission_ITGK_Code AS RoleName, "
                        . "a.Admission_ITGK_Code AS RoleCode, Count(a.Admission_Code) "
                        . "AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch,"
                        . "sum(case when a.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount "
                        . "FROM tbl_admission as a inner join tbl_user_master as b on a.Admission_ITGK_Code=b.User_LoginId  WHERE a.Admission_Course = '" . $_course . "' "
                        . "AND a.Admission_Batch = '" . $_batch . "' "
						. "AND b.User_UserRoll='7' AND a.Admission_RspName='".$_SESSION['User_Code']."' "
                        . "group by a.Admission_ITGK_Code";
			 
            } 
			
			
			else {
                $_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";

                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");

                $_SESSION['AdmSummaryCenter'] = $CenterCode3;

                if ($CenterCode3) {

                    $_SelectQuery3 = "SELECT tbl_admission.Admission_ITGK_Code AS RoleName, tbl_admission.Admission_ITGK_Code AS RoleCode, Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM tbl_admission WHERE tbl_admission.Admission_ITGK_Code IN ($CenterCode3) AND tbl_admission.Admission_Course = '" . $_course . "' AND tbl_admission.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1' group by tbl_admission.Admission_ITGK_Code";
                }
            }

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerList($_course, $_batch, $_rolecode, $_mode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

		//echo $_mode;
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
            $_mode = mysqli_real_escape_string($_ObjConnection->Connect(),$_mode);
			if($_mode=="ShowUpload")
			{
				$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,
				a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, c.Batch_Name FROM tbl_admission AS a
				INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code WHERE
				a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND 
				a.Admission_Batch = '" . $_batch . "'  ";
			}
			else if($_mode=="ShowConfirm")
            {
				$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,
				a.Admission_Fname,a.Admission_Photo,a.Admission_Sign,c.Batch_Name FROM tbl_admission AS a
				INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code WHERE 
				a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND 
				a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1' ";
			}
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
      
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
            //$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";
			 $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,Admission_Mobile,"
                    . "a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, a.Admission_Payment_Status,c.Batch_Name FROM
					tbl_admission AS a inner join tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code"
                    . " WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' "
                    . "AND a.Admission_Batch = '" . $_batch . "' ORDER BY a.Admission_Payment_Status DESC";
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
          
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
             $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedList($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
             $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
