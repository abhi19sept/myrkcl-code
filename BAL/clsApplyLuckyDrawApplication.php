<?php

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsApplyLuckyDrawApplication {

    public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code='1' ORDER BY Course_Code ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            
		   $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "' and Batch_Code >= '270' ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAll($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                 $_SelectQuery = "Select i.Admission_Code as applystatus, a.Admission_Code,a.Admission_LearnerCode, a.Admission_Name, a.Admission_Batch, a.Admission_Fname, a.Admission_DOB, a.Admission_Photo, a.Admission_Sign, a.Admission_Fee, a.Admission_Payment_Status,a.Admission_Course  
                                FROM tbl_admission as a LEFT JOIN tbl_admission_fee_waiver_scheme as i on a.Admission_Code=i.Admission_Code WHERE  a.Admission_Batch = '" . $batch . "' AND  a.Admission_Course = '" . $course . "' AND a.Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "index.php";</script> 
                        <?php

            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function LcodeDetail($_batch, $_course,  $_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_batch >= '270') {

                $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name,h.Organization_Name,h.Organization_District,h.Organization_Tehsil  FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code INNER JOIN tbl_organization_detail as h ON a.User_Code=h.Organization_User   WHERE a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_LearnerCode = '".$_Lcode."' and Admission_ITGK_Code ='".$_SESSION['User_LoginId']."'";
                } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "index.php";</script> 
                        <?php

                }
            } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "index.php";</script> 
                        <?php

            }

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add($_postdata, $admitcardfilename, $itgksignfilename, $lsignfilename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User_Code = $_SESSION['User_Code'];
            
            $_InsertQuery = "insert into tbl_admission_fee_waiver_scheme (Admission_Code, Admission_LearnerCode, Admission_ITGK_Code, BioMatric_Status, 
	Admission_Aadhar_Status, 
	Admission_Date, 
	Admission_Date_LastModified, 
	Admission_Date_Payment, 
	Admission_Course, 
	Admission_Batch, 
	Admission_Advance_CourseCode, 
	Admission_Course_Category, 
	Admission_Fee, 
	Admission_Installation_Mode, 
	Admission_PhotoUpload_Status, 
	Admission_SignUpload_Status, 
	Admission_PhotoProcessing_Status, 
	rejection_reason, 
	rejection_type, 
	Admission_Payment_Status, 
	Admission_ReceiptPrint_Status, 
	Admission_TranRefNo, 
	Admission_RKCL_Trnid, 
	Admission_Name, 
	Admission_Fname, 
	Admission_DOB, 
	Admission_MTongue, 
	Admission_Scan, 
	Admission_Photo, 
	Admission_Sign, 
	Admission_Gender, 
	Admission_MaritalStatus, 
	Admission_Medium, 
	Admission_PH, 
	Admission_PID, 
	Admission_UID, 
	Admission_District, 
	Admission_Tehsil, 
	Admission_Address, 
	Admission_PIN, 
	Admission_Mobile, 
	Admission_Phone, 
	Admission_Email, 
	Admission_Qualification, 
	Admission_Ltype, 
	Admission_GPFNO, 
	User_Code, 
	Admission_RspName, 
	Timestamp, 
	Admission_yearid, 
	IsNewRecord, 
	IsNewOnlineLMSRecord, 
	User_device_id) select * from tbl_admission where Admission_Code='".$_postdata["acode"]."'";
                $_Response_InsertQuery = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_UpdateQuery = "Update tbl_admission_fee_waiver_scheme set admitcardfilename='" . $admitcardfilename . "', itgksignfilename='" . $itgksignfilename . "',lsignfilename='".$lsignfilename."',tenboardname='".$_postdata["tenboardname"]."',tenboardrollno='".$_postdata["tenboardroll"]."',tweboardname='".$_postdata["tweboardname"]."',tweboardrollno='".$_postdata["tweboardroll"]."',tenboardexamyear='".$_postdata["examyear"]."',tweboardexamyear='".$_postdata["tweexamyear"]."', ItgkTehsil='".$_postdata["itgktehsil"]."', ItgkDistrict='".$_postdata["itgkdistrict"]."' Where Admission_Code='" . $_postdata["acode"] . "' and Admission_Batch >= '270'";
                $_Response_UpdateQuery = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                 $_SMS = "Dear Learner (" . $_postdata["lcodes"] . "), you have syccessfully applied for RS-CIT Lucky Draw Scheme(July-2020)";
            SendSMS($_postdata["lmobile"], $_SMS);

            
        } catch (Exception $_ex) {

            $_Response_InsertQuery[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response_InsertQuery[1] = Message::Error;
        }
        return $_Response_InsertQuery;
    }

}

?>