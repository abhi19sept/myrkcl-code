<?php


/**
 * Description of clsOrgDetail
 *
 * @author 
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsdownloadinnogrationimage {
	
	
	
	public function GetDetails($_ddlDistrict,$_batchcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
					$_ddlDistrict = mysqli_real_escape_string($_ObjConnection->Connect(),$_ddlDistrict);
					$_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
			
			    $_SelectQuery = "select b.Innaug_image1 as Innaug_image1,b.Innaug_image2 as Innaug_image2,centercode,d.District_Name from tbl_user_master as a
				inner join tbl_inauguration_details as b on b.centercode = a.User_LoginId
				inner join tbl_organization_detail as c on a.User_Code=c.Organization_User
				inner join tbl_district_master as d on c.Organization_District=d.District_Code
				WHERE 
				c.Organization_District='" . $_ddlDistrict . "' AND b.Batchcode= '" . $_batchcode . "'";
			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function Getcourse() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select Course_Code, Course_Name From tbl_course_master WHERE Course_Code ='3'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	public function GetDistrict() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select District_Code,District_Name From tbl_district_master where District_StateCode='29'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	
    
}
