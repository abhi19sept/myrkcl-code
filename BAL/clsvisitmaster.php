<?php

/**
 * Description of clsvisitmaster
 *
 * @author Deep
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/smtp_class.php';
$_ObjConnection = new _Connection();
$_ObjConnection->Connect();
$_Response = array();

define("SUPER_ROLES", serialize(array(1, 4)));

class clsvisitmaster {

    private function dbConnection() {
        global $_ObjConnection;

        return $_ObjConnection;
    }

    public function getVisitList($visitCode, $formValues = [], $status = 1) {
        $_ObjConnection = $this->dbConnection();
        $visitCode = mysqli_real_escape_string($_ObjConnection->Connect(), $visitCode);

        $filter = (in_array($_SESSION['User_UserRoll'], unserialize(SUPER_ROLES))) ? "vr.status IN ($status)" : "um.User_LoginId = '" . $_SESSION['User_LoginId'] . "'";
        $filter .= ($visitCode) ? " AND vr.id = '" . $visitCode . "' AND vr.status IN ($status)" : '';
        if (isset($formValues['dateFrom']) && !empty($formValues['dateFrom']) && $_SESSION['User_UserRoll'] != '4') {
            $from = explode('-', $formValues['dateFrom']);
            $timestamp = mktime(0, 0, 0, $from[1], $from[0], $from[2]);
            //$visitdate = STR_TO_DATE("vr.visit_date", '%d/%m/%Y');
            $filter .= ($status) ? " AND vr.confirm_date >= " . $timestamp : " AND STR_TO_DATE(vr.visit_date, '%d-%m-%Y') >= '" . date("Y-m-d", strtotime($formValues['dateFrom'])) . "'";
        }
        if (isset($formValues['dateTo']) && !empty($formValues['dateTo']) && $_SESSION['User_UserRoll'] != '4') {
            $dtto = explode('-', $formValues['dateTo']);
            $timestamp = mktime(23, 59, 59, $dtto[1], $dtto[0], $dtto[2]);
            $filter .= ($status) ? " AND vr.confirm_date < " . $timestamp : " AND STR_TO_DATE(vr.visit_date, '%d-%m-%Y') <= '" . date("Y-m-d", strtotime($formValues['dateTo'])) . "'";
        }


        $isDPO = stripos($_SESSION['UserRoll_Name'], 'DPO-RKCL');
        if (isset($formValues['rsp_code']) && !empty($formValues['rsp_code']) && empty($formValues['itgk_code']) && $isDPO === false) {
            $filter .= " AND vr.request_by = '" . $formValues['rsp_code'] . "'";
        }
        if (isset($formValues['itgk_code']) && !empty($formValues['itgk_code'])) {
            $filter .= " AND vr.itgk_code = '" . $formValues['itgk_code'] . "'";
        }
        if (isset($formValues['dateFrom']) && !empty($formValues['dateFrom']) && isset($formValues['dateTo']) && !empty($formValues['dateTo']) && ($_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '11')) {
            //$filter = " vr.request_by = '" . $_SESSION['User_Code'] . "'";
            $filter = " um.User_UserRoll in ('4','23','14') ";
            $filter .= " AND STR_TO_DATE(vr.visit_date, '%d-%m-%Y') >= '" . date("Y-m-d", strtotime($formValues['dateFrom'])) . "'";
            $filter .= " AND STR_TO_DATE(vr.visit_date, '%d-%m-%Y') <= '" . date("Y-m-d", strtotime($formValues['dateTo'])) . "'";
        }
        if (empty($formValues['dateFrom']) || empty($formValues['dateTo']) && ($_SESSION['User_UserRoll'] == '4')) {
            $filter = " vr.request_by = '" . $_SESSION['User_Code'] . "' AND vr.id = '" . $visitCode . "'";
        }

        $_SelectQuery = "SELECT vr.*, um.User_Code, vf.feedbackdata, vf.overall_feedback, tm.Tehsil_Name, dm.District_Name, odm.Organization_Name, od.Organization_Name AS ITGK_NAME, uc.User_EmailId, uc.User_MobileNo FROM tbl_visit_requests vr INNER JOIN tbl_user_master um ON vr.request_by = um.User_Code INNER JOIN tbl_user_master uc ON uc.User_LoginId = vr.itgk_code INNER JOIN tbl_organization_detail odm ON odm.Organization_User = um.User_Code INNER JOIN tbl_organization_detail od ON od.Organization_User = uc.User_Code INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = od.Organization_Tehsil INNER JOIN tbl_district_master dm ON dm.District_Code = od.Organization_District LEFT JOIN tbl_visit_feedback vf ON vf.visitid = vr.id WHERE $filter ORDER BY vr.id DESC";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function getVisitPhotos($VisitId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $VisitId = mysqli_real_escape_string($_ObjConnection->Connect(), $VisitId);

            $_SelectQuery = "Select exterior_photo, reception_photo, classroom_photo, lab_photo From tbl_visit_feedback"
                    . " where visitid='" . $VisitId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getITGKList($itgkCode = '', $rsp_code = '') {
        $_ObjConnection = $this->dbConnection();
        $filter = ($itgkCode) ? " AND um.User_LoginId = '" . $itgkCode . "'" : '';
        $rsp_code = ($rsp_code == 'auto') ? $_SESSION['User_Code'] : $rsp_code;
        $filter .= ($rsp_code && $rsp_code != 'auto') ? " AND um.User_Rsp = '" . $rsp_code . "'" : $filter;
        if ($_SESSION['User_UserRoll'] != 7) {
            $isDPO = stripos($_SESSION['UserRoll_Name'], 'DPO-RKCL');
            $filter .= ($isDPO === false && !$rsp_code) ? " AND um.User_Rsp = (SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '" . $_SESSION['User_LoginId'] . "')" : " AND dm.District_Code = (SELECT Organization_District FROM tbl_organization_detail WHERE Organization_User='" . $_SESSION['User_Code'] . "')";
        }
        if ($_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "SELECT CourseITGK_ExpireDate, um.*, od.*, dm.District_Name, tm.Tehsil_Name, mm.Municipality_Name, wm.Ward_Name, wm.Ward_Raj_Code, pm.Block_Name,
                gp.GP_Name, vm.Village_Name, up.UserProfile_FirstName, up.UserProfile_Mobile FROM tbl_user_master um
                INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code
                INNER JOIN tbl_courseitgk_mapping as ac on ac.Courseitgk_ITGK=um.User_LoginId
                INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = od.Organization_Tehsil
                INNER JOIN tbl_district_master dm ON dm.District_Code = od.Organization_District
                LEFT JOIN tbl_municipality_master mm ON mm.Municipality_Raj_Code = od.Organization_Municipal
                LEFT JOIN tbl_ward_master wm ON wm.Ward_Code = od.Organization_WardNo
                LEFT JOIN tbl_panchayat_samiti pm ON pm.Block_Code = od.Organization_Panchayat
                LEFT JOIN tbl_gram_panchayat gp ON gp.GP_Code = od.Organization_Gram
                LEFT JOIN tbl_village_master vm ON vm.Village_Code = od.Organization_Village
                LEFT JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code
                WHERE um.User_UserRoll in (7,14) and
                NOW()<DATE_ADD(CourseITGK_ExpireDate, INTERVAL +11 MONTH) and Courseitgk_Course='RS-CIT'
               ORDER BY um.User_LoginId";
        } else {
            $_SelectQuery = "SELECT um.*, od.*, dm.District_Name, tm.Tehsil_Name, mm.Municipality_Name, wm.Ward_Name, wm.Ward_Raj_Code, pm.Block_Name, gp.GP_Name, vm.Village_Name, up.UserProfile_FirstName, up.UserProfile_Mobile FROM tbl_user_master um INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = od.Organization_Tehsil INNER JOIN tbl_district_master dm ON dm.District_Code = od.Organization_District LEFT JOIN tbl_municipality_master mm ON mm.Municipality_Raj_Code = od.Organization_Municipal LEFT JOIN tbl_ward_master wm ON wm.Ward_Code = od.Organization_WardNo LEFT JOIN tbl_panchayat_samiti pm ON pm.Block_Code = od.Organization_Panchayat LEFT JOIN tbl_gram_panchayat gp ON gp.GP_Code = od.Organization_Gram LEFT JOIN tbl_village_master vm ON vm.Village_Code = od.Organization_Village LEFT JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code INNER JOIN tbl_courseitgk_mapping as ac on ac.Courseitgk_ITGK=um.User_LoginId WHERE um.User_UserRoll in (7,14) and NOW()<DATE_ADD(CourseITGK_ExpireDate, INTERVAL +11 MONTH) and Courseitgk_Course='RS-CIT' $filter ORDER BY um.User_LoginId";
        }

        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                $result[$row['User_LoginId']] = $row;
            }
        }

        return $result;
    }

    public function getITGKList1($itgkCode = '', $rsp_code = '') {
        $_ObjConnection = $this->dbConnection();
        $filter = ($itgkCode) ? " AND um.User_LoginId = '" . $itgkCode . "'" : '';
        $rsp_code = ($rsp_code == 'auto') ? $_SESSION['User_Code'] : $rsp_code;
        $filter .= ($rsp_code && $rsp_code != 'auto') ? " AND um.User_Rsp = '" . $rsp_code . "'" : $filter;
        if ($_SESSION['User_UserRoll'] != 7) {
            $isDPO = stripos($_SESSION['UserRoll_Name'], 'DPO-RKCL');
            $filter .= ($isDPO === false && !$rsp_code) ? " AND um.User_Rsp = (SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '" . $_SESSION['User_LoginId'] . "')" : " AND dm.District_Code = (SELECT Organization_District FROM tbl_organization_detail WHERE Organization_User='" . $_SESSION['User_Code'] . "')";
        }
        if ($_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "SELECT um.*, od.*, dm.District_Name, tm.Tehsil_Name, mm.Municipality_Name, wm.Ward_Name, wm.Ward_Raj_Code, pm.Block_Name, gp.GP_Name, vm.Village_Name, up.UserProfile_FirstName, up.UserProfile_Mobile FROM tbl_user_master um INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = od.Organization_Tehsil INNER JOIN tbl_district_master dm ON dm.District_Code = od.Organization_District LEFT JOIN tbl_municipality_master mm ON mm.Municipality_Raj_Code = od.Organization_Municipal LEFT JOIN tbl_ward_master wm ON wm.Ward_Code = od.Organization_WardNo LEFT JOIN tbl_panchayat_samiti pm ON pm.Block_Code = od.Organization_Panchayat LEFT JOIN tbl_gram_panchayat gp ON gp.GP_Code = od.Organization_Gram LEFT JOIN tbl_village_master vm ON vm.Village_Code = od.Organization_Village LEFT JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code WHERE um.User_UserRoll in (7,14) AND um.User_LoginId = '" . $itgkCode . "' ORDER BY um.User_LoginId";
        } else {
            $_SelectQuery = "SELECT um.*, od.*, dm.District_Name, tm.Tehsil_Name, mm.Municipality_Name, wm.Ward_Name, wm.Ward_Raj_Code, pm.Block_Name, gp.GP_Name, vm.Village_Name, up.UserProfile_FirstName, up.UserProfile_Mobile FROM tbl_user_master um INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = od.Organization_Tehsil INNER JOIN tbl_district_master dm ON dm.District_Code = od.Organization_District LEFT JOIN tbl_municipality_master mm ON mm.Municipality_Raj_Code = od.Organization_Municipal LEFT JOIN tbl_ward_master wm ON wm.Ward_Code = od.Organization_WardNo LEFT JOIN tbl_panchayat_samiti pm ON pm.Block_Code = od.Organization_Panchayat LEFT JOIN tbl_gram_panchayat gp ON gp.GP_Code = od.Organization_Gram LEFT JOIN tbl_village_master vm ON vm.Village_Code = od.Organization_Village LEFT JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code WHERE um.User_UserRoll in (7,14) $filter ORDER BY um.User_LoginId";
        }

        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                $result[$row['User_LoginId']] = $row;
            }
        }

        return $result;
    }

    public function setNewVisit($itgkCode, $visitDate, $requestBy) {
        $_ObjConnection = $this->dbConnection();

        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkCode);
        $visitDate = mysqli_real_escape_string($_ObjConnection->Connect(), $visitDate);

        $insertQuery = "INSERT IGNORE INTO tbl_visit_requests SET request_by = (SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '" . $requestBy . "'), itgk_code = '" . $itgkCode . "', visit_date = '" . $visitDate . "', adddate = '" . time() . "'";
        $_Response = $_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
        $result = 0;
        if ($_Response[0] == Message::SuccessfullyInsert) {
            $isDPO = stripos($_SESSION['UserRoll_Name'], 'DPO-RKCL');
            $ptype = ($isDPO === false) ? 'SP' : 'DPO';
            $result = 1;
            $itgk = $this->getITGKList($itgkCode);
            $_SMS = "Dear ITGK - " . $itgkCode . ", a visit has been scheduled on " . $visitDate . " by your " . $ptype . " at your center's registred address.";
            SendSMS($itgk[$itgkCode]['User_MobileNo'], $_SMS);
            if (isset($itgk[$itgkCode]['User_EmailId'])) {
                $email = $itgk[$itgkCode]['User_EmailId'];
                $subject = "New visit has been scheduled on " . $visitDate;
                $sent = $this->sendEmail($_SMS, $subject, $email, 'ITGK - ' . $itgkCode);
            }
        }

        return $result;
    }

    public function deleteVisit($visitId, $userId) {
        $_ObjConnection = $this->dbConnection();

        $visitId = mysqli_real_escape_string($_ObjConnection->Connect(), $visitId);
        $userId = mysqli_real_escape_string($_ObjConnection->Connect(), $userId);

        $visitInfo = $this->getVisitList($visitId, [], '0,1');
        if (!$visitInfo) {
            return false;
        }

        $itgkCode = $visitInfo[0]['itgk_code'];
        $visitDate = $visitInfo[0]['visit_date'];
        $email = $visitInfo[0]['User_EmailId'];
        $mobile = $visitInfo[0]['User_MobileNo'];

        $deleteQuery = "DELETE FROM tbl_visit_requests WHERE request_by = (SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '" . $userId . "') AND id = '" . $visitId . "'";
        $_Response = $_ObjConnection->ExecuteQuery($deleteQuery, Message::DeleteStatement);
        $result = ($_Response[0] == Message::SuccessfullyDelete) ? 1 : 0;

        if ($result) {
            $isDPO = stripos($_SESSION['UserRoll_Name'], 'DPO-RKCL');
            $ptype = ($isDPO === false) ? 'SP' : 'DPO';
            $_SMS = "Dear ITGK - " . $itgkCode . ", a visit scheduled on " . $visitDate . ", has been canceled by your " . $ptype . ".";
            SendSMS($mobile, $_SMS);
            $subject = "Visit scheduled on " . $visitDate . ", has been canceled.";
            $sent = $this->sendEmail($_SMS, $subject, $email, 'ITGK - ' . $itgkCode);
        }

        return $result;
    }

    public function getVisitListForITGK($visitId) {
        $_ObjConnection = $this->dbConnection();
        $visitId = mysqli_real_escape_string($_ObjConnection->Connect(), $visitId);

        $filter = ($visitId) ? " AND vr.id = '" . $visitId . "'" : '';
        $_SelectQuery = "SELECT vr.*, um.User_LoginId, um.User_EmailId, um.User_MobileNo, od.Organization_Name, vf.feedbackdata FROM tbl_visit_requests vr INNER JOIN tbl_user_master um ON vr.request_by = um.User_Code INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code LEFT JOIN tbl_visit_feedback vf ON vf.visitid = vr.id WHERE vr.itgk_code = '" . $_SESSION['User_LoginId'] . "' $filter ORDER BY vr.id DESC";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function getBioDevices($itgkCode, $serial = '') {
        $_ObjConnection = $this->dbConnection();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkCode);
        $filter = ($serial) ? " AND BioMatric_SerailNo = '" . $serial . "'" : '';
        $_SelectQuery = "SELECT * FROM tbl_biomatric_register WHERE BioMatric_ITGK_Code = '" . $itgkCode . "' AND BioMatric_Status LIKE('success') AND BioMatric_Flag = 1 $filter ORDER BY BioMatric_Make";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function getitgkBioPics($itgkCode) {
        $result = [];

        /* $_i = 0;
          while ($_Row = mysqli_fetch_array($response[2])) {
          $_Datatable[$_i] = array("BioMatric_ISO_Template" => $_Row['BioMatric_ISO_Template']);
          $_i = $_i + 1;
          } */

        return $result;
    }

    public function getVisitorBioPics($visitId) {
        $result = [];

        return $result;
    }

    public function getBioEnrolledList($userCode, $status = 0, $unique = '') {
        $_ObjConnection = $this->dbConnection();
        $userCode = mysqli_real_escape_string($_ObjConnection->Connect(), $userCode);
        $filter = ($status) ? ' AND be.status = 1' : '';
        $join = '';
        $field = '';
        if (in_array($_SESSION['User_UserRoll'], unserialize(SUPER_ROLES))) {
            $join = "INNER JOIN tbl_user_master um ON um.User_Code = be.userId INNER JOIN tbl_userroll_master rm ON um.User_UserRoll = rm.UserRoll_Code";
            $filter .= " AND ((rm.UserRoll_Name LIKE('%dpo%') AND um.User_Parentid = 1) OR be.addedby = '" . $_SESSION['User_Code'] . "')";
            $field = ", IF(rm.UserRoll_Name LIKE('%dpo%'), um.User_LoginId, CONCAT(be.employee_type,' (',um.User_LoginId,')')) AS employee_type";
        } elseif ($_SESSION['User_UserRoll'] == '7') {
            $filter .= ($unique == 'all') ? " AND (be.addedby = '" . $userCode . "' OR be.addedby IN (SELECT User_Rsp FROM tbl_user_master WHERE User_Code = '" . $userCode . "'))" : " AND be.addedby = '" . $userCode . "'";
        } else {

            $filter .= ($unique == 'all') ? " AND (be.addedby = '" . $userCode . "' OR be.addedby IN (SELECT User_Code FROM tbl_user_master WHERE User_Rsp = '" . $userCode . "' AND User_UserRoll = '7'))" : " AND be.addedby = '" . $userCode . "'";
        }

        //  $filter = ($unique == 'all') ? '' : $filter;

        $_SelectQuery = "SELECT be.* $field FROM tbl_biomatric_enrollments be $join WHERE 2<3 $filter ORDER BY be.fullname";
        //m_log("getBioEnrolledList ". ($_SelectQuery) . " " . date('Y-m-d h:i:s'));
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function getBioEnrolledList1($userCode, $status = 0, $unique = '') {
        $_ObjConnection = $this->dbConnection();

        $userCode = mysqli_real_escape_string($_ObjConnection->Connect(), $userCode);

        $_SelectQuery = "SELECT id,BioMatric_ISO_Template FROM tbl_biomatric_enrollments WHERE addedby = '" . $userCode . "' ORDER BY fullname";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function getBioEnrolledList2($userCode, $status = 0, $unique = '') {
        $_ObjConnection = $this->dbConnection();

        $userCode = mysqli_real_escape_string($_ObjConnection->Connect(), $userCode);

        $_SelectQuery = "SELECT id,fullname,employee_type,mobile,email_id,status FROM tbl_biomatric_enrollments WHERE addedby = '" . $userCode . "' ORDER BY fullname";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function processBioEnrollment($values, $userCode, $addedBy) {
        $_ObjConnection = $this->dbConnection();
        $mobile = trim($values['mobile']);
        $emailId = trim($values['email_id']);
        if (!in_array($_SESSION['User_UserRoll'], unserialize(SUPER_ROLES))) {
            //    $_SelectQuery = "(SELECT * FROM tbl_ao_register WHERE AO_Mobile = '" . $mobile . "' AND AO_Status = 0 AND AO_OTP = " . $values['otpmobile'] . ") UNION (SELECT * FROM tbl_ao_register WHERE AO_Email = '" . $emailId . "' AND AO_Status = 0 AND AO_OTP = " . $values['otpemail'] . ")";
            $_SelectQuery = "(SELECT * FROM tbl_ao_register WHERE AO_Mobile = '" . $mobile . "' AND AO_Status = 0 AND AO_OTP = " . $values['otpmobile'] . ") UNION (SELECT * FROM tbl_ao_register WHERE AO_Email = '" . $emailId . "' AND AO_Status = 0)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $result = '';
            if ($_Response[0] != Message::NoRecordFound) {
                //$rows = mysqli_num_rows($_Response[2]);
                //if ($rows < 2) {
                //     return $result = 'Mobile / Email OTP not matched.';
                //   exit;
                //} else {
                while ($row = mysqli_fetch_array($_Response[2])) {
                    $_UpdateQuery = "UPDATE tbl_ao_register SET 
                            AO_Status = 1 WHERE AO_Code = '" . $row['AO_Code'] . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                }
                //}
            } else {
                return $result = 'Mobile / Email OTP not matched.';
                exit;
            }
        }
        if ($_SESSION['User_UserRoll'] == '4') {
            $_SelectQuery2 = "SELECT * FROM tbl_biomatric_enrollments WHERE  status = 1 AND userId = '" . $userCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
        } else {
            $_SelectQuery = "SELECT * FROM tbl_biomatric_enrollments WHERE  status = 1 AND (mobile = '" . $mobile . "' OR email_id = '" . $emailId . "')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $result = '';
        }
        if ($_Response[0] == Message::NoRecordFound) {
            $_SelectQuery = "SELECT COUNT(*) as n FROM tbl_biomatric_enrollments WHERE userId = '" . $userCode . "' AND employee_type = '" . $values['employee_type'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $error = '';
            if ($_Response[0] != Message::NoRecordFound) {
                $row = mysqli_fetch_array($_Response[2]);

                switch ($values['employee_type']) {
                    case 'Coordinator':
                    case 'Counsler':
                        $error = ($row['n'] >= 50) ? 'Unable to register more than ' . $row['n'] . ' ' . $values['employee_type'] . '.' : '';
                        break;
                    case 'Employee':
                    case 'Faculty':
                        $error = ($row['n'] >= 50) ? 'Unable to register more than ' . $row['n'] . ' ' . $values['employee_type'] . '.' : '';
                        break;
                    case 'Owner':
                        $error = ($row['n'] >= 1) ? 'Unable to register more than ' . $row['n'] . ' ' . $values['employee_type'] . '.' : '';
                        break;
                    case 'DPO':
                        $error = ($row['n'] >= 33) ? 'Unable to register more than ' . $row['n'] . ' ' . $values['employee_type'] . '.' : '';
                        break;
                }
            }
            if (empty($error)) {
                $queryFields = "userId = '" . $userCode . "',
                    addedby = '" . $addedBy . "',
                    fullname = '" . $values['fullname'] . "',
                    employee_type = '" . $values['employee_type'] . "',
                    gender = '" . $values['gender'] . "',
                    mobile = '" . $mobile . "',
                    email_id = '" . $emailId . "',
                    BioMatric_NFIQ = '" . $values['txtNFIQ'] . "',
                    BioMatric_Quality = '" . $values['txtQuality'] . "',
                    BioMatric_ISO_Template = '" . $values['txtIsoTemplate'] . "',
                    BioMatric_ISO_Image = '" . $values['txtIsoImage'] . "',
                    BioMatric_Raw_Data = '" . $values['txtRawData'] . "',
                    BioMatric_WSQ_Image = '" . $values['txtWsqData'] . "',
                    addtime = '" . time() . "'";
                if (!$values['rowid']) {
                    $_InsertQuery = "INSERT INTO tbl_biomatric_enrollments SET 
                    $queryFields";
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                } else {
                    $_UpdateQuery = "UPDATE tbl_biomatric_enrollments SET 
                    $queryFields WHERE id = '" . $values['rowid'] . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                }
                $result = $_Response[0];
            } else {
                $result = $error;
            }
        } else {
            $result = 'Email / Mobile number already registred.';
        }

        return $result;
    }

    public function changeEnrollStatus($id, $act, $userCode) {
        $_ObjConnection = $this->dbConnection();

        $id = mysqli_real_escape_string($_ObjConnection->Connect(), $id);
        $act = mysqli_real_escape_string($_ObjConnection->Connect(), $act);
        $userCode = mysqli_real_escape_string($_ObjConnection->Connect(), $userCode);

        $status = ($act == 'Deactivate') ? 0 : 1;
        $filter = ($userCode == 1) ? '' : "AND userId = '" . $userCode . "'";
        $_UpdateQuery = "UPDATE tbl_biomatric_enrollments SET status = '" . $status . "' WHERE id = '" . $id . "' $filter";
        $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
    }

    public function getUserCodeOfITGK($itgkCode) {
        $_ObjConnection = $this->dbConnection();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkCode);

        $_SelectQuery = "SELECT User_Code FROM tbl_user_master WHERE User_LoginId = '" . $itgkCode . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = 0;
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result = $row['User_Code'];
            }
        }

        return $result;
    }

    public function GetDetails($visitId) {
        $_ObjConnection = $this->dbConnection();

        $visitId = mysqli_real_escape_string($_ObjConnection->Connect(), $visitId);

        $_SelectQuery = "SELECT vr.id, ben.id itgkid, be.id as visitorid FROM tbl_visit_requests vr 
                        INNER JOIN  tbl_biomatric_enrollments be ON vr.request_by = be.userId 
                        INNER JOIN tbl_user_master um ON vr.itgk_code = um.User_LoginId 
                        INNER JOIN  tbl_biomatric_enrollments ben ON ben.userId = um.User_Code where vr.id='" . $visitId . "' ";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        return $_Response;
    }

    public function validateVisitWithConfirmingBy($visitId, $itgkId, $visitorId) {
        $_ObjConnection = $this->dbConnection();

        $visitId = mysqli_real_escape_string($_ObjConnection->Connect(), $visitId);
        $itgkId = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkId);
        $visitorId = mysqli_real_escape_string($_ObjConnection->Connect(), $visitorId);

        $_SelectQuery = "SELECT vr.id FROM tbl_visit_requests vr INNER JOIN  tbl_biomatric_enrollments be ON vr.request_by = be.userId INNER JOIN tbl_user_master um ON vr.itgk_code = um.User_LoginId INNER JOIN  tbl_biomatric_enrollments ben ON ben.userId = um.User_Code WHERE vr.id = '" . $visitId . "' AND ben.id = '" . $itgkId . "' AND be.id = '" . $visitorId . "' AND vr.status = 0 AND be.status = 1";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = '';
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result = $row['id'];
            }
        }

        return $result;
    }

    public function setVisitConfirmed($id, $itgkCode, $visitorId, $itgkId) {
        $_ObjConnection = $this->dbConnection();

        $id = mysqli_real_escape_string($_ObjConnection->Connect(), $id);
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkCode);
        $visitorId = mysqli_real_escape_string($_ObjConnection->Connect(), $visitorId);
        $itgkId = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkId);

        $_UpdateQuery = "UPDATE tbl_visit_requests SET status = '1', confirm_by_itgk = '" . $itgkId . "', confirm_visiter_id = '" . $visitorId . "', confirm_date = '" . time() . "' WHERE itgk_code = '" . $itgkCode . "' AND id = '" . $id . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        $itgk = $this->getITGKList($itgkCode);
        //$isDPO = stripos($_SESSION['UserRoll_Name'], 'dpo');
        $isDPO = stripos($_SESSION['UserRoll_Name'], 'DPO-RKCL');
        $ptype = ($isDPO === false) ? 'SP' : 'DPO';
        //$_SMS = "Dear ITGK - " . $itgkCode . ", a scheduled visit #" . $id . " has been completed by your " . $ptype . ".";
        $_SMS = "Dear ITGK - " . $itgkCode . ", scheduled visit #" . $id . " has been completed.";
        SendSMS($itgk[$itgkCode]['User_MobileNo'], $_SMS);
        if (isset($itgk[$itgkCode]['User_EmailId'])) {
            $email = $itgk[$itgkCode]['User_EmailId'];
            $subject = "Visit #" . $id . " has been completed.";
            $sent = $this->sendEmail($_SMS, $subject, $email, 'ITGK - ' . $itgkCode);
        }

        return $_Response[0];
    }

    public function saveVisitFeedback($data, $owntypefilename, $salpifilename, $panpifilename, $ccpifilename, $_OverallFeedback) {
        $_ObjConnection = $this->dbConnection();
        if (isset($data['Located_on_Address']) && $data['Located_on_Address'] == 'No') {
            $visitCode = $data['VisitCode'];
            foreach ($data as $key => $val) {
                $data[$key] = '-';
            }
            $data['VisitCode'] = $visitCode;
            $data['Located_on_Address'] = 'No';
            $data['txtOverAllRemark'] = 'This ITGK center is not located on his registred center address.';
        }

        $insertQuery = "INSERT IGNORE INTO tbl_visit_feedback SET visitid = '" . $data['VisitCode'] . "', "
                . "usercode = '" . $_SESSION['User_Code'] . "', located_on_address = '" . $data['Located_on_Address'] . "',"
                . " exterior_photo = '" . $owntypefilename . "', reception_photo = '" . $salpifilename . "',"
                . " classroom_photo = '" . $panpifilename . "', lab_photo = '" . $ccpifilename . "', overall_feedback= '" . $_OverallFeedback . "', feedbackdata = '" . serialize($data) . "'";

        $_Response = $_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
        $result = 0;
        if ($_Response[0] == Message::SuccessfullyInsert) {
            $result = 1;
            $this->generateFeedbackPdf($data['VisitCode']);
            
             $resultFile = 'visit_feedback_' . $data['VisitCode'] . '.pdf';
                    $newURL = '../upload/visitFeedbacks/' . $resultFile;
                    //$filepath5 = 'common/showpdfftp.php?src=visitFeedbacks/' . $resultFile;
                    $directoy = '/visitFeedbacks/';
                    $file = $resultFile;
                    $response_ftp = ftpUploadFile($directoy, $file, $newURL);
//                    if(trim($response_ftp) == "SuccessfullyUploaded"){
//                        //echo $filepath5;
//                    } 
                    
            $visitInfo = $this->getVisitList($data['VisitCode']);
            if ($visitInfo) {
                $itgk = $this->getITGKList($visitInfo[0]['itgk_code']);
                $isDPO = stripos($_SESSION['UserRoll_Name'], 'DPO-RKCL');
                $ptype = ($isDPO === false) ? 'SP' : 'DPO';
                $_SMS = "Dear ITGK - " . $visitInfo[0]['itgk_code'] . ", feedback of your itgk center against visit #" . $data['VisitCode'] . " has been registred by your " . $ptype . ", you can view this in your login panel.";
                SendSMS($itgk[$visitInfo[0]['itgk_code']]['User_MobileNo'], $_SMS);
                if (isset($itgk[$visitInfo[0]['itgk_code']]['User_EmailId'])) {
                    $email = $itgk[$visitInfo[0]['itgk_code']]['User_EmailId'];
                    $subject = "Feedback for Visit #" . $id . " has been registred.";
                    $sent = $this->sendEmail($_SMS, $subject, $email, 'ITGK - ' . $visitInfo[0]['itgk_code']);
                }
            }
        }

        return $result;
    }

    public function getVisitFeedback($visitid, $exefull = 1) {
        $_ObjConnection = $this->dbConnection();
        $visitid = mysqli_real_escape_string($_ObjConnection->Connect(), $visitid);

        $_SelectQuery = "SELECT vf.feedbackdata, od.Organization_Name AS SP_NAME, vr.visit_date AS VISIT_DATE, vr.itgk_code AS ITGK_CODE, oc.Organization_Name AS ITGK_NAME, oc.Organization_Address AS txtAddress, dm.District_Name, tm.Tehsil_Name, mm.Municipality_Name, wm.Ward_Name, wm.Ward_Raj_Code, pm.Block_Name, gp.GP_Name, vm.Village_Name, oc.Organization_AreaType, oc.Organization_Tehsil, oc.Organization_Municipal, oc.Organization_WardNo, oc.Organization_Panchayat, oc.Organization_Gram, oc.Organization_Village, oc.Organization_PinCode, oc.Organization_Landmark, be.fullname AS SP_OFFICIAL_VISITOR, be.mobile AS SP_VISITOR_MOBILE FROM tbl_visit_feedback vf LEFT JOIN tbl_visit_requests vr ON vr.id = vf.visitid LEFT JOIN tbl_user_master um ON um.User_Code = vr.request_by LEFT JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code LEFT JOIN tbl_user_master uc ON uc.User_LoginId = vr.itgk_code LEFT JOIN tbl_organization_detail oc ON oc.Organization_User = uc.User_Code LEFT JOIN tbl_biomatric_enrollments be ON be.id = vr.confirm_visiter_id LEFT JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = oc.Organization_Tehsil LEFT JOIN tbl_district_master dm ON dm.District_Code = oc.Organization_District LEFT JOIN tbl_municipality_master mm ON mm.Municipality_Raj_Code = oc.Organization_Municipal LEFT JOIN tbl_ward_master wm ON wm.Ward_Code = oc.Organization_WardNo LEFT JOIN tbl_panchayat_samiti pm ON pm.Block_Code = oc.Organization_Panchayat LEFT JOIN tbl_gram_panchayat gp ON gp.GP_Code = oc.Organization_Gram LEFT JOIN tbl_village_master vm ON vm.Village_Code = oc.Organization_Village WHERE vf.visitid = '" . $visitid . "' ";
        //     $_SelectQuery = "SELECT vf.feedbackdata, od.Organization_Name AS SP_NAME, vr.visit_date AS VISIT_DATE, vr.itgk_code AS ITGK_CODE, oc.Organization_Name AS ITGK_NAME, oc.Organization_Address AS txtAddress, dm.District_Name, tm.Tehsil_Name, mm.Municipality_Name, wm.Ward_Name, wm.Ward_Raj_Code, pm.Block_Name, gp.GP_Name, vm.Village_Name, oc.Organization_AreaType, oc.Organization_Tehsil, oc.Organization_Municipal, oc.Organization_WardNo, oc.Organization_Panchayat, oc.Organization_Gram, oc.Organization_Village, oc.Organization_PinCode, oc.Organization_Landmark, be.fullname AS SP_OFFICIAL_VISITOR, be.mobile AS SP_VISITOR_MOBILE FROM tbl_visit_feedback vf INNER JOIN tbl_visit_requests vr ON vr.id = vf.visitid INNER JOIN tbl_user_master um ON um.User_Code = vr.request_by INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code INNER JOIN tbl_user_master uc ON uc.User_LoginId = vr.itgk_code INNER JOIN tbl_organization_detail oc ON oc.Organization_User = uc.User_Code INNER JOIN tbl_biomatric_enrollments be ON be.id = vr.confirm_visiter_id INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = oc.Organization_Tehsil INNER JOIN tbl_district_master dm ON dm.District_Code = oc.Organization_District LEFT JOIN tbl_municipality_master mm ON mm.Municipality_Raj_Code = oc.Organization_Municipal LEFT JOIN tbl_ward_master wm ON wm.Ward_Code = oc.Organization_WardNo LEFT JOIN tbl_panchayat_samiti pm ON pm.Block_Code = oc.Organization_Panchayat LEFT JOIN tbl_gram_panchayat gp ON gp.GP_Code = oc.Organization_Gram LEFT JOIN tbl_village_master vm ON vm.Village_Code = oc.Organization_Village WHERE vf.visitid = '" . $visitid . "' AND vf.usercode = '" . $_SESSION['User_Code'] . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = '';
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                //print_r($row); die; 
                if ($row['feedbackdata']) {

                    $fdata = unserialize($row['feedbackdata']);
                    // print_r($fdata); die;
                    $fdata1 = explode('&', $fdata['data']);

                    $data_result = [];
                    if (!empty($fdata1)) {
                        foreach ($fdata1 as $key => $value) {
                            list($key_data, $key_value) = explode('=', $value);
                            $data_result[$key_data] = trim(str_replace('+', ' ', trim($key_value)));
                        }
                    }

                    if ($exefull) {
                        $fdata['SP_NAME'] = ($row['SP_NAME']) ? $row['SP_NAME'] : '';
                        $fdata['ITGK_NAME'] = $row['ITGK_NAME'];
                        $fdata['VISIT_DATE'] = $row['VISIT_DATE'];
                        $fdata['ITGK_CODE'] = $row['ITGK_CODE'];
                        $fdata['ITGK_District'] = $row['District_Name'];
                        $fdata['ITGK_TEHSIL'] = $row['Tehsil_Name'];
                        $fdata['ITGK_CITY'] = $row['Tehsil_Name'];
                        $fdata['txtAddress'] = $row['txtAddress'];
                        $fdata['pincode_' . $data_result['areaType']] = $data_result['pincode'];
                        if ($data_result['correctArea'] == 'Yes') {
                            $fdata['areaType'] = $row['Organization_AreaType'];
                            $fdata['ddlTehsil'] = $row['Organization_Tehsil'];
                            $fdata['ddlWardno'] = $row['Ward_Raj_Code'];
                            $fdata['ddlMunicipalName'] = $row['Organization_Municipal'];
                            $fdata['Gram_Panchayat'] = $row['Organization_Gram'];
                            $fdata['ITGK_VILLAGE'] = $row['Organization_Village'];
                            $fdata['ITGK_PANC_SAMITI'] = $row['Organization_Panchayat'];
                            $fdata['landmark'] = $row['Organization_Landmark'];
                            $fdata['pincode_' . $data_result['areaType']] = $row['Organization_PinCode'];
                        }
                        if ($data_result['areaType'] == 'Urban') {
                            $fdata['Gram_Panchayat'] = 'n/a';
                            $fdata['ITGK_VILLAGE'] = 'n/a';
                            $fdata['ITGK_PANC_SAMITI'] = 'n/a';
                            $fdata['pincode_Rural'] = 'n/a';
                            $area = $this->getUrbanAreaDetails($data_result['ddlMunicipalName'], $row['Tehsil_Name'], $data_result['ddlWardno']);
                        } else {
                            $fdata['ITGK_CITY'] = 'n/a';
                            $fdata['ITGK_MOHALLA'] = 'n/a';
                            $fdata['ITGK_WARD'] = 'n/a';
                            $fdata['pincode_Urban'] = 'n/a';
                            $area = $this->getRuralAreaDetails($data_result['ddlGramPanchayat'], $data_result['ddlVillage'], $data_result['ddlTehsil']);
                        }

                        //print_r($fdata); die;

                        if ($data_result['ddlWardno']) {
                            $fdata['ITGK_WARD'] = $area['Ward_Name'];
                        }
                        if ($data_result['ddlMunicipalName']) {
                            $fdata['ITGK_MOHALLA'] = $area['Municipality_Name'];
                        }
                        if ($data_result['ddlPanchayat']) {
                            $fdata['ITGK_PANC_SAMITI'] = $area['Block_Name'];
                        }
                        if ($data_result['ddlGramPanchayat']) {
                            $fdata['Gram_Panchayat'] = $area['GP_Name'];
                        }
                        if ($data_result['ddlVillage']) {
                            $fdata['ITGK_VILLAGE'] = $area['Village_Name'];
                        }

                        $fdata['SP_OFFICIAL_VISITOR'] = $row['SP_OFFICIAL_VISITOR'];
                        $fdata['SP_VISITOR_MOBILE'] = $row['SP_VISITOR_MOBILE'];

                        for ($i = 1; $i < 4; $i++) {
                            if (!$data_result['batch' . $i]) {
                                $data_result['batch' . $i] = '-';
                                $data_result['lCount' . $i] = '-';
                                $data_result['lPresence' . $i] = '-';
                                $data_result['LearnerFeedback' . $i] = '-';
                            }
                        }
                    }


                    $result = serialize($fdata);
                }
            }
        } else {
            $result = '0';
        }

        return $result;
    }

    private function getUrbanAreaDetails($ddlMunicipal, $ddlTehsil, $wardId) {
        $_ObjConnection = $this->dbConnection();
        $_SelectQuery = "SELECT dm.District_Name, tm.Tehsil_Name, wm.Ward_Name, mp.Municipality_Name FROM tbl_tehsil_master tm INNER JOIN tbl_district_master dm ON dm.District_Code = tm.Tehsil_District INNER JOIN tbl_municipality_master mp ON mp.Municipality_District = dm.District_Code INNER JOIN tbl_ward_master wm ON wm.Ward_Municipality_Code = mp.Municipality_Code WHERE tm.Tehsil_Code = '" . $ddlTehsil . "' AND mp.Municipality_Raj_Code = '" . $ddlMunicipal . "' AND wm.Ward_Raj_Code = '" . $wardId . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = '';
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result = $row;
            }
        }

        return $result;
    }

    private function getRuralAreaDetails($gpId, $villId, $thesilId) {
        $_ObjConnection = $this->dbConnection();
        $_SelectQuery = "SELECT dm.District_Name, tm.Tehsil_Name, vm.Village_Name, gp.GP_Name, ps.Block_Name FROM tbl_village_master vm INNER JOIN tbl_gram_panchayat gp on vm.Village_GP = gp.GP_Code INNER JOIN tbl_panchayat_samiti ps ON ps.Block_Code = gp.GP_Block INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_District = ps.Block_District INNER JOIN tbl_district_master dm ON dm.District_Code = tm.Tehsil_District WHERE vm.Village_Code = '" . $villId . "' AND gp.GP_Code = '" . $gpId . "' LIMIT 1";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = '';
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result = $row;
            }
        }

        return $result;
    }

    public function fetchCenterBatches($itgkCode) {
        $_ObjConnection = $this->dbConnection();
        $_SelectQuery = "SELECT ad.Admission_Batch, COUNT(*) AS n, bm.Batch_Name FROM tbl_admission ad INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ad.Admission_Batch WHERE ad.Admission_ITGK_Code='" . $itgkCode . "' AND ad.Admission_Payment_Status=1 GROUP BY bm.Batch_Name ORDER BY ad.Admission_Batch DESC LIMIT 3";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function getUserList($type = 'dpo') {
        $_ObjConnection = $this->dbConnection();
        $filter = ($type == 'dpo') ? 'HAVING (be.id IS NULL OR be.status = 0)' : '';
        $_SelectQuery = "SELECT um.User_Code, um.User_LoginId, be.id, be.status FROM tbl_user_master um INNER JOIN tbl_userroll_master rm ON um.User_UserRoll = rm.UserRoll_Code LEFT JOIN tbl_biomatric_enrollments be ON be.userId = um.User_Code WHERE um.User_Status='1' AND rm.UserRoll_Name LIKE('%" . $type . "%') AND um.User_Parentid = 1 $filter ORDER BY um.User_LoginId";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_array($_Response[2])) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public function generateFeedbackPdf($visitId) {
        require_once '../thirdparty/autoload.php';

//$mpdf = new \Mpdf\Mpdf();
        $_ObjConnection = $this->dbConnection();
        $visitId = mysqli_real_escape_string($_ObjConnection->Connect(), $visitId);

        $feedback = $this->getVisitFeedback($visitId);
        if ($feedback != '0') {
            $filePath = 'upload/visitFeedbacks/visit_feedback_' . $visitId . '.pdf';
          //  $baseFilePath = $_SERVER['DOCUMENT_ROOT'] . '/' . $filePath;
            $baseFilePath =   '../' . $filePath;
            $html = file_get_contents("../visit_feedback_pdfform.php");
           
            if ($html) {
                //require_once '../mpdf60/mpdf.php';
                $feedback = unserialize($feedback);
                $feedback_data = explode('&', $feedback['data']);
                foreach ($feedback_data as $key => $value) {
                    list($key_data, $key_value) = explode('=', $value);
                    $feedback[$key_data] = trim(str_replace('+', ' ', trim($key_value)));
                }


//die;
                foreach ($feedback as $key => $val) {
                    $html = str_replace('{' . $key . '}', $val, $html);
                }



                $stylesheet = '<style type="text/css">
                    * {
                        margin:0;
                        padding:0;
                        text-indent:0;
                    }
                    td {
                        padding:5px;
                    }
                    h1 {
                        color: black;
                        font-family:Calibri, sans-serif;
                        font-style: normal;
                        font-weight: bold;
                        text-decoration: underline;
                        font-size: 13pt;
                    }
                    p {
                        color: black;
                        font-family:"Times New Roman", serif;
                        font-style: normal;
                        font-weight: bold;
                        text-decoration: none;
                        font-size: 9pt;
                        margin:0pt;
                    }
                    .s1 {
                        color: black;
                        font-family:"Times New Roman", serif;
                        font-style: normal;
                        font-weight: bold;
                        text-decoration: none;
                        font-size: 9pt;
                    }
                    .s2 {
                        color: #0070C0;
                        font-family:"Times New Roman", serif;
                        font-style: normal;
                        font-weight: bold;
                        text-decoration: none;
                        font-size: 9pt;
                    }
                    .s3 {
                        color: black;
                        font-family:"Times New Roman", serif;
                        font-style: normal;
                        font-weight: normal;
                        text-decoration: none;
                        font-size: 9pt;
                    }
                    .s4 {
                        color: black;
                        font-family:"Times New Roman", serif;
                        font-style: normal;
                        font-weight: normal;
                        text-decoration: none;
                        font-size: 10pt;
                    }
                    .s5 {
                        color: #0070C0;
                        font-family:"Times New Roman", serif;
                        font-style: normal;
                        font-weight: normal;
                        text-decoration: none;
                        font-size: 9pt;
                    }
                    .s6 {
                        color: black;
                        font-family:"Times New Roman", serif;
                        font-style: normal;
                        font-weight: bold;
                        text-decoration: none;
                        font-size: 9pt;
                    }
                    .s7 {
                        color: black;
                        font-family:Calibri, sans-serif;
                        font-style: normal;
                        font-weight: normal;
                        text-decoration: none;
                        font-size: 11pt;
                    }
                    </style>';
                $path = '../' . $filePath;
                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                //$mpdf = new mPDF('c', 'A4', '', '', 12, 12, 12, 12, 12, 12);
                $mpdf->SetDisplayMode('fullpage');
                $mpdf->list_indent_first_level = 0;
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->WriteHTML($html, 2);
                $mpdf->Output($path, 'F');
               // echo $baseFilePath; die;
                if (file_exists($baseFilePath)) {
                    return $filePath;
                }
            }
        } else {
            echo "0";
            return;
        }
    }

    private function OTP($length = 6, $chars = '1234567890') {
        $chars_length = (strlen($chars) - 1);
        $string = $chars{rand(0, $chars_length)};
        for ($i = 1; $i < $length; $i = strlen($string)) {
            $r = $chars{rand(0, $chars_length)};
            if ($r != $string{$i - 1})
                $string .= $r;
        }
        return substr($string, 0, 6);
    }

    public function setOTP($mobile = '', $email = '') {
        $_ObjConnection = $this->dbConnection();
        $filter = ($mobile) ? " AND AO_Mobile = '" . $mobile . "'" : '';
        $filter .= ($email) ? " AND AO_Email = '" . $email . "'" : '';
        $_SelectQuery = "SELECT * FROM tbl_ao_register WHERE AO_Status = '0' $filter LIMIT 1";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if ($_Response[0] != Message::NoRecordFound) {
            $_Row = mysqli_fetch_array($_Response[2]);
            $otp = $_Row['AO_OTP'];
        } else {
            $otp = $this->OTP();
            $insertQuery = "INSERT INTO tbl_ao_register SET AO_Mobile='" . $mobile . "', AO_Email = '" . $email . "', AO_Code = (Select Case When Max(ar.AO_Code) Is Null Then 1 Else Max(ar.AO_Code)+1 End AS id FROM tbl_ao_register ar), AO_OTP='" . $otp . "', AO_Status='0'";
            $_Response = $_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
            $result = 0;
            if ($_Response[0] != Message::SuccessfullyInsert) {
                $otp = '';
            }
        }

        if ($otp) {
            $sms = $otp . " is the OTP, for your Bio Enrollment.Rajasthan Knowledge Corporation Limited ";
            if ($mobile) {
                SendSMS($mobile, $sms);
            } elseif ($email) {
                $html = file_get_contents("../send_email_template.php");
                if ($html) {
                    $subject = "MyRKCL, your otp for email verification";
                    $sent = $this->sendEmail($sms, $subject, $email, $_SESSION['UserRoll_Name']);
                    if (!$sent) {
                        $otp = '';
                    }
                } else {
                    $otp = '';
                }
            } else {
                $otp = '';
            }
        }

        return $otp;
    }

    public function sendEmail($htmlMessage, $subject, $email, $title) {
        $sendEmail = new sendMail();
        $variableArray = [];
        $variableArray["loginLink"] = "https://www.myrkcl.com/";
        $variableArray["username"] = $title;
        $variableArray["htmlMessage"] = $htmlMessage;
        // $_Response = $sendEmail->sendEmailHTML($email, $variableArray["username"], $subject, "send_email_template.php", $variableArray);
        $_Response = $sendEmail->sendEmail($email, $title, $subject, $htmlMessage);

        return $_Response;
    }

    public function getITGKStaffInfo($itgkCode) {
        $_ObjConnection = $this->dbConnection();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkCode);

        $_SelectQuery = "SELECT dm.Designation_Name, sd.Staff_Name, sd.Staff_Mobile FROM tbl_staff_detail sd INNER JOIN tbl_designation_master dm ON dm.Designation_Code = sd.Staff_Designation WHERE sd.Staff_User = '" . $itgkCode . "' GROUP BY dm.Designation_Name";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                $result[$row['Designation_Name'] . '_Staff_Name'] = $row['Staff_Name'];
                $result[$row['Designation_Name'] . '_Staff_Mobile'] = $row['Staff_Mobile'];
            }
        }

        if (!isset($result['Counselor_Staff_Name'])) {
            $result['Counselor_Staff_Name'] = '';
            $result['Counselor_Staff_Mobile'] = '';
        }
        if (!isset($result['Faculty_Staff_Name'])) {
            $result['Faculty_Staff_Name'] = '';
            $result['Faculty_Staff_Mobile'] = '';
        }
        if (!isset($result['Co-ordinator_Staff_Name'])) {
            $result['Co-ordinator_Staff_Name'] = '';
            $result['Co-ordinator_Staff_Mobile'] = '';
        }

        return $result;
    }

    public function getITGKInfraDetails($itgkCode) {
        $_ObjConnection = $this->dbConnection();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkCode);
        $_SelectQuery = "SELECT Premises_Receptiontype, Premises_area, Premises_seatingcapacity, Premises_details FROM tbl_premises_details WHERE Premises_User = '" . $itgkCode . "' LIMIT 1";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                $result['Reception_Available'] = $row['Premises_Receptiontype'];
                $result['Reception_Area'] = $row['Premises_area'];
                $result['Reception_Capacity'] = $row['Premises_seatingcapacity'];
                $result['Reception_Remark'] = $row['Premises_details'];
            }
        }

        return $result;
    }

    public function getITGKSyatemDetails($itgkCode) {
        $_ObjConnection = $this->dbConnection();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(), $itgkCode);
        $_SelectQuery = "SELECT * FROM tbl_system_info WHERE `System_User` = '" . $itgkCode . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = [];
        if ($_Response[0] != Message::NoRecordFound) {
            $i = 1;
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                $result['processer' . $i] = $row['System_Processor'];
                $result['ram' . $i] = $row['System_RAM'];
                $result['hdd' . $i] = $row['System_HDD'];
                $result['qty' . $i] = 1;
                $result['typesc' . $i] = $row['System_Type'];
                $result['systemRemark' . $i] = $row['System_Remarks'];
                $i++;
            }
        }

        return $result;
    }

}
