<?php


/**
 * Description of clsOrgDetail
 *
 * @author  yogendra soni
 */

require '../DAL/classconnectionNEW.php';
 require '../DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsadmingroupsmsmodule {
	
	 
		
     public function ADD($_centers,$_mobile,$_Msg)
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {



            $countno=explode(',', $_mobile);
            $centers= $_centers;
            $validMobile = [];

            $values = [];
            foreach($countno as $key=>$array){
                $_Sms = trim($array);

                if( preg_match('/^(\+91[\-\s]?)?[0]?(91)?[6789]\d{9}$/i',$_Sms)) {
                    $a['mobile'] = substr($_Sms, -10);

                    $_ITGK_Code = trim($centers[$key]['center']);
                    $_Roll = trim($centers[$key]['roll']);


                    $valid['number'] = trim($_Sms);
                    $rand = rand(4, 6);
                    $valid['encode'] = base64_encode($rand . trim($_Sms));

                    $validMobile[] = $valid;
                    $encode = $valid['encode'];
                    $SMS_Log_SmsCount = ceil(strlen($_Msg) / 160);
                    $values[] = "('{$_Roll}','{$_ITGK_Code}', '{$_Sms}', '{$_Msg}', '0','SMS_PORTAL','{$encode}','{$_SESSION['User_LoginId']}','{$SMS_Log_SmsCount}')";
                }

            }

            $values = implode(", ", $values);
            $_InsertQuery = "Insert Into tbl_sms_log(SMS_Log_UserRole,SMS_Log_UserLoginId,SMS_Log_Mobile,SMS_Log_SmsText,SMS_Log_SmsSentStaus,SMS_Log_SmsTpye,SMS_Log_UpdateId,SMS_Log_SenderLoginId,SMS_Log_SmsCount)  VALUES   {$values}  ";
            $_Response =  $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            $Message = $_Msg;
            $result = '';
            $arraychunk = array_chunk($validMobile,500);
            foreach($arraychunk as $chunk){
                $num = [];
                $updateId = [];
                foreach($chunk as $c){
                    $num[]=$c['number'];
                    $updateId[]=$c['encode'];
                }
                $commaList = implode(',', $num);
                $result = SendSMS($commaList, $Message);
                if($result)
                    $this->UpdateSmsLog(implode("','",$updateId));
            }


					
				
               
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	
	
	public function GetMsg() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code =   $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='".$_ITGK_Code."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAll($_ITGK_Code)
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

		     $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$totalpackage=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$totalpackage = $totalpackage + $_Row1['package'];
		      }

			$temp=0;
			$_SelectQuery1 = "Select sum(SMS_Log_SmsCount) as count from tbl_sms_log where SMS_Log_SenderLoginId='{$_ITGK_Code}'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			while ($_data = mysqli_fetch_array($_Response1[2]))
			{
				$temp=$temp+$_data['count'];


		    }
			//print_r($temp);
			$_Response[2]=$totalpackage-$temp;
			$_Response[3]=$temp;

        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
         return $_Response;
    }

    public function GetAllITGK($_moblie)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_moblie = implode("','",$_moblie);
            $_SelectQuery = "Select User_LoginId,User_MobileNo,User_UserRoll from tbl_user_master where User_MobileNo in ('{$_moblie}')";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);




        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }


    public function getAllLearners($_moblie)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_moblie = implode("','",$_moblie);
            $_SelectQuery = "Select Admission_Mobile,Admission_LearnerCode from tbl_admission where Admission_Mobile in ('{$_moblie}')";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);




        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }



    public function GetTotalpackage($_ITGK_Code)
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$tot=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$tot = $tot + $_Row1['package'];
		      } 	
			
			
			$_Response[2]=$tot;
			
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	

	
	public function GetRsp() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "select User_Rsp,User_LoginId from tbl_user_master where User_UserRoll='14' ORDER BY User_UserRoll DESC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function GetDistrict() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "Select * from tbl_district_master order by District_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    } 
	
	
	
	
	
	
	
	
	public function Getentityname() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

                $_SelectQuery = "Select Entity_Code,Entity_Name from tbl_entity_master where enabledforsms=1 order by Entity_Name asc";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            }
            catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
    }


    public function Getentityroll($entities) {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select UserRoll_Name,UserRoll_Code from tbl_userroll_master where UserRoll_Status=1 and UserRoll_Entity in ({$entities}) order by UserRoll_Name asc";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	

	
	
	public function GetcenterList($rolls,$rsp1,$_course,$postedArray,$_rsp,$renew)
	{

        global $_ObjConnection;
        $_ObjConnection->Connect();
        $rolls = explode(',',$rolls);
        try {

            if(array_intersect($rolls, [7,15,22])) {
                $_SelectQuery = "Select distinct b.User_MobileNo,User_UserRoll,b.User_LoginId,CourseITGK_ExpireDate from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_courseitgk_mapping as c on b.user_LoginId=c.Courseitgk_ITGK 	where User_UserRoll='7' AND Courseitgk_EOI in ('" . str_replace(',', "''", $_course) . "')";
                foreach ($postedArray as $fields => $values) {
                    if ($values) {

                        $_SelectQuery .= " AND {$fields} in ('" . implode("','", $values) . "')";
                    }

                }

                if ($_rsp) {
                    $_SelectQuery .= " AND b.User_Rsp in (select User_Code from tbl_user_master u join tbl_rsptarget r on Rsptarget_User=u.User_LoginId where User_LoginId in ('" . str_replace(',', "','", $_rsp) . "') ) ";
                }
                if ($renew) {
                    $renewed = explode(',', $renew);

                    $curday = date('Y-m-d', time());
                    if (in_array('R', $renewed)) {
                        $_SelectQuery .= " AND c.CourseITGK_ExpireDate >= '{$curday}'";
                    }
                    if (in_array('N', $renewed)) {
                        $_SelectQuery .= " AND c.CourseITGK_ExpireDate < '{$curday}'";
                    }
                    if (in_array('E', $renewed)) {

                        $_SelectQuery .= " AND DATE_ADD(c.CourseITGK_ExpireDate, INTERVAL +10 MONTH) >= '{$curday}'  and c.CourseITGK_ExpireDate < '{$curday}'";
                    }

                    if (in_array('NE', $renewed)) {
                        $_SelectQuery .= " AND DATE_ADD(c.CourseITGK_ExpireDate, INTERVAL +10 MONTH) < '{$curday}'  and c.CourseITGK_ExpireDate < '{$curday}'";

                    }
                    if (in_array('S', $renewed)) {
                        $_SelectQuery .= " AND User_LoginId in(Select EOI_ECL From tbl_eoi_centerlist e join  tbl_courseitgk_mapping c on e.EOI_ECL=c.Courseitgk_ITGK Where EOI_Code in ('" . str_replace(',', "''", $_course) . "') AND YEAR(DATE_ADD(c.CourseITGK_ExpireDate, INTERVAL -1 YEAR)) =EOI_Year AND EOI_Status_Flag='Y')";
                    }

                    /*if(in_array('EP',$renewed)){

                        //$_SelectQuery .= "  order by a.Organization_Name";

$i = [];
                        $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                        while ($_record2 = mysqli_fetch_array($_Response1[2])) {
                            $i[] =  $_record2['User_LoginId'];
                            //$loginId[$_record['Courseitgk_ITGK']] = $_record['Admission_Count'];

                        }
                        $i = implode("','",$i);


                           echo $query = "Select Courseitgk_ITGK,count(Admission_Code) as Admission_Count From tbl_admission a join  tbl_courseitgk_mapping c on a.Admission_ITGK_Code=c.Courseitgk_ITGK Where  DATE(Admission_Date) > DATE_ADD(c.CourseITGK_ExpireDate, INTERVAL -13 MONTH) and DATE(Admission_Date)<=c.CourseITGK_ExpireDate and Admission_Payment_Status='1' and c.CourseITGK_ExpireDate < '{$curday}' AND Courseitgk_EOI in ('".str_replace(',',"''",$_course)."') and Admission_ITGK_Code in ('{$i}') group by Courseitgk_ITGK  having Admission_Count < 50";

exit;


                        $loginId= [];
                        $_Row  = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                        while ($_record = mysqli_fetch_array($_Row[2])) {

                            $loginId[$_record['Courseitgk_ITGK']] = $_record['Admission_Count'];

                        }
                        $ids= implode("','",array_keys($loginId));
                        $query1 = "Select User_LoginId,case when (g.GP_Name=p.Block_Name and Organization_AreaType='Rural') then 25 else 50 end  as c_count from tbl_organization_detail o join tbl_user_master u on o.Organization_User=u.User_Code inner join tbl_panchayat_samiti p on p.Block_Code=o.Organization_Panchayat inner join tbl_gram_panchayat g on g.GP_Code=o.Organization_Gram where User_LoginId in ('{$ids}') group by Organization_User";

                        $_Row1  = $_ObjConnection->ExecuteQuery($query1, Message::SelectStatement);
                        $itgk = [];
                        while ($_record1 = mysqli_fetch_array($_Row1[2])) {
                            if(isset($loginId[$_record1['User_LoginId']]) && ($_record1['c_count'] > $loginId[$_record1['Courseitgk_ITGK']]) ){
                                $itgk[] =   $_record1['User_LoginId'];
                            }

                        }

                        $User_LoginId = implode("','",array_keys($itgk));

                        $_SelectQuery .= " AND User_LoginId in('{$User_LoginId}')";


                    }*/


                }
                $_SelectQuery .= "  order by a.Organization_Name";


                $_Response[] = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            }
            $rolls = array_diff( $rolls, [7,15,22] );
            $roles= implode("','",$rolls);
            $rsp1 = explode(',',$rsp1);
            $rsp1= implode("','",$rsp1);
            if(in_array('13',$rolls) && count($rolls) > 1){
                $_SelectQuery1 = "Select  User_LoginId, User_MobileNo,User_UserRoll from tbl_user_master  Where User_UserRoll in ('{$roles}') order by User_LoginId";
                $_Response[]=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);

                $_SelectQuery  = "Select  User_UserRoll,Rsptarget_User, Rsptarget_Code,User_LoginId, Organization_Name,User_Code, User_MobileNo,Rsptarget_contactperson From tbl_rsptarget r join tbl_user_master u on u.User_LoginId=r.Rsptarget_User join tbl_organization_detail o on o.Organization_User=u.User_Code Where User_LoginId in ('{$rsp1}')  AND rsptarget_Status='Approved' group by Rsptarget_User order by Rsptarget_User";

                $_Response[]=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);


            }
            else if(in_array('13',$rolls) && count($rolls) == 1){
                $_SelectQuery  = "Select  User_UserRoll,Rsptarget_User, Rsptarget_Code,User_LoginId, Organization_Name,User_Code, User_MobileNo,Rsptarget_contactperson From tbl_rsptarget r join tbl_user_master u on u.User_LoginId=r.Rsptarget_User join tbl_organization_detail o on o.Organization_User=u.User_Code Where User_LoginId in ('{$rsp1}')  AND rsptarget_Status='Approved' group by Rsptarget_User order by Rsptarget_User  ";

                $_Response[]=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            }
            else if(count($rolls)>0 && !in_array('13',$rolls)) {
                    $_SelectQuery = "Select  User_UserRoll,User_LoginId, User_MobileNo from tbl_user_master  Where User_UserRoll in ('{$roles}') order by User_LoginId";
                    $_Response[]=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }








            }

            catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            //print_r($_Response);
            return $_Response;
    }
	
	
	
	
	public function GetMobilenumber($_activate) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			
		    $_SelectQuery = "Select User_MobileNo,User_Code from tbl_user_master where User_LoginId IN (".$_activate.") AND  User_UserRoll='7' group by User_MobileNo ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetCentermobile($_center) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			
		   $_SelectQuery = "Select distinct User_MobileNo from tbl_user_master where User_LoginId IN (".$_center.") AND  User_UserRoll='7' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function Getstatus() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_ITGK_Code = $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select Payment_Status from tbl_package_transections where  centercode='".$_ITGK_Code."'  ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	
	public function GetalltehsilCenter($_district,$_tehsil) 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
				 $_SelectQuery3 = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='7' AND Organization_District='" . $_district . "' AND Organization_Tehsil IN(" . $_tehsil . ") AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock'
											order by a.Organization_Name";
					    
						
						
			$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


    public function FILLCourse()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master order by Course_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }

    public function FillBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
         //   $_SelectQuery = "Select a.Event_Batch AS Batch_Code, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '".$course."' AND CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='3' AND Event_Payment='1'";
           
           $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "' ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function FillStates()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_state_master Where State_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FillTehsils($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Tehsil_Code,Tehsil_Name From tbl_tehsil_master Where Tehsil_Status='1' AND Tehsil_District in ({$_District_Code})";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FillDistricts($_State_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_district_master Where District_Status='1' and District_StateCode in ({$_State_Code})";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillDistrictsByRsp($_State_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_district_master d join tbl_rsptarget r on r.Rsptarget_District=d.District_Code Where District_Status='1' and District_StateCode in ({$_State_Code}) group by r.Rsptarget_District order by d.District_Name asc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


    public function GetAllRsp($_district_Code,$roles)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select distinct Rsptarget_User, Rsptarget_Code, Organization_Name,User_Code, User_MobileNo,Rsptarget_contactperson From tbl_rsptarget r join tbl_user_master u on u.User_LoginId=r.Rsptarget_User join tbl_organization_detail o on o.Organization_User=u.User_Code Where Rsptarget_District in ({$_district_Code})  AND rsptarget_Status='Approved' group by Rsptarget_User order by Rsptarget_User";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAllCenterByRsp($_values)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_values = implode("','",array_map("trim",explode(',',$_values) ));
            $_SelectQuery = "Select distinct Rsptarget_User, Rsptarget_Code,User_LoginId, Organization_Name,User_Code, User_MobileNo,Rsptarget_contactperson From tbl_rsptarget r join tbl_user_master u on u.User_LoginId=r.Rsptarget_User join tbl_organization_detail o on o.Organization_User=u.User_Code Where User_LoginId in ('{$_values}')  AND rsptarget_Status='Approved' group by Rsptarget_User order by Rsptarget_User";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAllCenterByRKCL($_values)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_values = implode("','",array_map("trim",explode(',',$_values) ));
            $_SelectQuery = "Select distinct User_LoginId,User_UserRoll, User_MobileNo from tbl_user_master  Where User_UserRoll in ('{$_values}') order by User_LoginId ";

            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillMunicipalType()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_municipality_type Where Municipality_Type_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FillMunicipalName($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_municipality_master Where Municipality_District in ({$_District_Code})";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLWardno($_Municipal_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_ward_master Where 
								Ward_Municipality in ('".str_replace(",", "','", $_Municipal_Code) ."')";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllPanchayat($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Block_Code,Block_Name From tbl_panchayat_samiti Where Block_District in ({$_District_Code})";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }
    public function GetAllGramPanchayat($_Panchayat_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select GP_Code,GP_Name"
                . " From  tbl_gram_panchayat Where GP_Block in ({$_Panchayat_Code})";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }
    public function GetAllVillage($country) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        //echo $country;
        try {

            $_SelectQuery = "Select Village_Code,Village_Name,GP_Name,"
                    . "Status_Name From tbl_village_master as a inner join tbl_gram_panchayat as b "
                    . "on a.Village_GP=b.GP_Code inner join tbl_status_master as c "
                    . "on a.Village_Status=c.Status_Code and a.Village_GP in ({$country})";

            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }

       public function GetTemplates($_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

                   $_SelectQuery = "Select a.*, b.*, c.* from tbl_message_template_master as a inner join tbl_message_type as b on a.Message_Type_ID=b.Message_Type_ID inner join tbl_message_category as c on a.Message_Cat_ID =c.Message_Cat_ID where Message_Temp_Status=1 order by c.Message_Cat_Name, a.Message_Title, b.Message_Type_Name, a.Message_For";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetTemplate($_ITGK,$_Message_Temp_ID) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select Message_Text from tbl_message_template_master where Message_Temp_ID={$_Message_Temp_ID}";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SmsLogCsv($_ITGK_Code,$userroll='',$array,$SMS_Log_UpdateId='',$action)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            foreach($array as $sms){
                if( preg_match('/^(\+91[\-\s]?)?[0]?(91)?[6789]\d{9}$/i',$sms['mobile'])) {
                    $SMS_Log_UserLoginId = (isset($sms['SMS_Log_UserLoginId'])) ? $sms['SMS_Log_UserLoginId'] : '';
                    $SMS_Log_UserRole = (isset($sms['User_UserRoll'])) ? $sms['User_UserRoll'] : '';
                    $SMS_Log_SmsCount = ceil(strlen($sms['msg']) / 160);
                    $values[] = "('{$SMS_Log_UserLoginId}','{$SMS_Log_UserRole}','{$_ITGK_Code}', '{$sms['mobile']}', '{$sms['msg']}', '0','{$action}','{$SMS_Log_UpdateId}','{$SMS_Log_SmsCount}')";
                }
            }

            $values = implode(", ", $values);
            $_InsertQuery = "Insert Into tbl_sms_log(SMS_Log_UserLoginId,SMS_Log_UserRole,SMS_Log_SenderLoginId,SMS_Log_Mobile,SMS_Log_SmsText,SMS_Log_SmsSentStaus,SMS_Log_SmsTpye,SMS_Log_UpdateId,SMS_Log_SmsCount)  VALUES   {$values}  ";
            /* $arr_length = count($_mobile);
            echo $arr_length; */
            //SendSMS($_mobile, $_Msg);



            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);




        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;

        }
        return $_Response;

    }

    public function UpdateSmsLog($SMS_Log_UpdateId)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (strpos($SMS_Log_UpdateId, '.csv') !== false) {
                $_InsertQuery = "Update tbl_sms_log set SMS_Log_SmsSentStaus=1 where SMS_Log_UpdateId in ('{$SMS_Log_UpdateId}')  ";
            }
            else{
                $_InsertQuery = "Update tbl_sms_log set SMS_Log_SmsSentStaus=1,SMS_Log_UpdateId='' where SMS_Log_UpdateId in ('{$SMS_Log_UpdateId}')  ";
            }

            /* $arr_length = count($_mobile);
            echo $arr_length; */
            //SendSMS($_mobile, $_Msg);



            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::UpdateStatement);




        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;

        }
        return $_Response;

    }


    public function SendSimpleSms($_mobile,$_Msg)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {


            $result= '';


            $arraychunk = array_chunk($_mobile,500);
            foreach($arraychunk as $chunk){
                $num = [];
                $updateId = [];
                foreach($chunk as $c){
                    $num[]=$c->mobile;
                    $updateId[]=$c->encode;
                }
                $commaList = implode(',', $num);
                $result = SendSMS($commaList, $_Msg);
                if($result)
                $this->UpdateSmsLog(implode("','",$updateId));
            }


            $_Response[0] =  Message::SuccessfullyUpdate;
            $_Response[1] =  'Success';





        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;

        }
        return $_Response;

    }

    public function SendSimpleSmsLog($SMS_Log_SenderLoginId,$array)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {


            foreach($array as $entity){
                if( preg_match('/^(\+91[\-\s]?)?[0]?(91)?[6789]\d{9}$/i',$entity['mobile'])) {
                    $SMS_Log_UserLoginId = (isset($entity['SMS_Log_UserLoginId'])) ? $entity['SMS_Log_UserLoginId'] : '';
                    $SMS_Log_UserRole = (isset($entity['User_UserRoll'])) ? $entity['User_UserRoll'] : '';
                    $SMS_Log_SmsCount = ceil(strlen($entity['msg']) / 160);
                    $values[] = "('{$SMS_Log_UserLoginId}','{$SMS_Log_UserRole}','{$SMS_Log_SenderLoginId}', '{$entity['mobile']}', '{$entity['msg']}', '0','SIMPLE_SMS','{$entity['encode']}','{$SMS_Log_SmsCount}')";
                }


            }

            $values = implode(", ", $values);
            $_InsertQuery = "Insert Into tbl_sms_log(SMS_Log_UserLoginId,SMS_Log_UserRole,SMS_Log_SenderLoginId,SMS_Log_Mobile,SMS_Log_SmsText,SMS_Log_SmsSentStaus,SMS_Log_SmsTpye,SMS_Log_UpdateId,SMS_Log_SmsCount)  VALUES   {$values}  ";
            $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);



            $_Response[0] =  Message::SuccessfullyInsert;
            $_Response[1] =  'Success';





        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;

        }
        return $_Response;

    }


}
