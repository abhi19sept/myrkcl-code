<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsSwitchReport
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsSwitchReport {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
                $_SelectQuery = "SELECT a.*, b.*, c.*, d.District_Name, e.Tehsil_Name from tbl_rspitgk_mapping AS a INNER JOIN tbl_user_master as b"
                            . " ON a.Rspitgk_ItgkCode=b.User_LoginId INNER JOIN tbl_organization_detail as c"
                            . " ON b.User_Code=c.Organization_User INNER JOIN tbl_district_master as d"
                            . " ON c.Organization_District=d.District_Code INNER JOIN tbl_tehsil_master as e"
                            . " ON c.Organization_Tehsil=e.Tehsil_Code where NOW() >= a.Rspitgk_End_Date";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
    
            } else if ($_SESSION['User_UserRoll'] == 14){
                $_SelectQuery = "SELECT Distinct User_LoginId from tbl_user_master where User_Rsp='" .$_SESSION['User_Code'] . "' "
                        . "and User_UserRoll='7'";
                $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                
                $_i = 0;
                $CenterCode = '';
                while ($_Row = mysqli_fetch_array($_Response1[2])) {

                    $CenterCode.=$_Row['User_LoginId'] . ",";

                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode, ",");

                //$_SESSION['AdmSummaryCenter'] = $CenterCode3;

                if ($CenterCode3) {

                    $_SelectQuery3 = "SELECT a.*, b.*, c.*, d.District_Name, e.Tehsil_Name from tbl_rspitgk_mapping AS a INNER JOIN tbl_user_master as b"
                            . " ON a.Rspitgk_ItgkCode=b.User_LoginId INNER JOIN tbl_organization_detail as c"
                            . " ON b.User_Code=c.Organization_User INNER JOIN tbl_district_master as d"
                            . " ON c.Organization_District=d.District_Code INNER JOIN tbl_tehsil_master as e"
                            . " ON c.Organization_Tehsil=e.Tehsil_Code where a.Rspitgk_ItgkCode IN ($CenterCode3) and Rspitgk_UserType='New'";
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                }
            }
            
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
