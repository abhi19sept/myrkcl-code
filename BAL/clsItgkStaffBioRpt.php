<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsItgkStaffBioRpt
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsItgkStaffBioRpt {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 10 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 30) {
            $_SelectQuery = "select  b.User_LoginId,c.Organization_Name, a.fullname, a.employee_type, a.gender, a.mobile, a.email_id
 from tbl_biomatric_enrollments as a inner join tbl_user_master as b 
on a.userId=b.User_Code inner join tbl_organization_detail as c 
on b.User_Code=c.Organization_User where b.User_UserRoll='7'";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "select  b.User_LoginId,c.Organization_Name, a.fullname, a.employee_type, a.gender, a.mobile, a.email_id
 from tbl_biomatric_enrollments as a inner join tbl_user_master as b 
on a.userId=b.User_Code inner join tbl_organization_detail as c 
on b.User_Code=c.Organization_User where b.User_Rsp='" . $_SESSION['User_Code'] . "'";
            } elseif ($_SESSION['User_UserRoll'] == 23) {
            $_SelectQuery = "select  b.User_LoginId,c.Organization_Name, a.fullname, a.employee_type, a.gender, a.mobile, a.email_id
 from tbl_biomatric_enrollments as a inner join tbl_user_master as b 
on a.userId=b.User_Code inner join tbl_organization_detail as c 
on b.User_Code=c.Organization_User where Organization_District='" . $_SESSION['Organization_District'] . "'";
            } elseif ($_SESSION['User_UserRoll'] == 7) {
            $_SelectQuery = "select  b.User_LoginId,c.Organization_Name, a.fullname, a.employee_type, a.gender, a.mobile, a.email_id
 from tbl_biomatric_enrollments as a inner join tbl_user_master as b 
on a.userId=b.User_Code inner join tbl_organization_detail as c 
on b.User_Code=c.Organization_User where userId = '" . $_SESSION['User_Code'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
