<?php

/**
 * Description of clsApplyLuckyDrawApplicationReport
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsApplyLuckyDrawApplicationReport {

    //put your code here

    public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '28') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            }elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } 
			elseif ($_LoginRole == '14') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "14";
            } 
			
			else {
                echo "hello";
            }

            $_SESSION['UserType'] = $_LoginUserType;

            if ($_loginflag == "1") {

					$_SelectQuery3 = "select a.Admission_ITGK_Code AS RoleName, a.Admission_ITGK_Code AS RoleCode, Count(a.Admission_Code) 
                        AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch, Count(b.Admission_Code) AS applycount 
                        FROM tbl_admission as a LEFT JOIN tbl_admission_fee_waiver_scheme as b 
                        on a.Admission_Code=b.Admission_Code WHERE  
                         a.Admission_Course = '" . $_course . "' AND a.Admission_Batch =" . $_batch . " group by a.Admission_ITGK_Code";
			} 
			elseif ($_loginflag == "14") 
			{
				
					    $_SelectQuery3 = "select a.Admission_ITGK_Code AS RoleName, a.Admission_ITGK_Code AS RoleCode, Count(a.Admission_Code) 
                        AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch, Count(b.Admission_Code) AS applycount 
                        FROM tbl_admission as a LEFT JOIN tbl_admission_fee_waiver_scheme as b 
                        on a.Admission_Code=b.Admission_Code WHERE  
                         a.Admission_Course = '" . $_course . "' AND a.Admission_Batch ='" . $_batch . "' AND a.Admission_RspName='".$_SESSION['User_Code']."' group by a.Admission_ITGK_Code";
			 
            } 
			

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);

        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response3;
    }
	
	public function GetLearnerListSuperadmin($_course, $_batch, $_rolecode, $_mode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

		echo $_mode;
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
            $_mode = mysqli_real_escape_string($_ObjConnection->Connect(),$_mode);
			if($_mode=="ShowUpload")
			{
				$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE  a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "'  ";
			}
			else if($_mode=="ShowConfirm")
            {
                $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign,a.admitcardfilename,b.RSP_Code,b.RSP_Name,b.District_Name FROM tbl_admission_fee_waiver_scheme AS a left join vw_itgkname_distict_rsp as b
on a.Admission_ITGK_Code=b.ITGKCODE WHERE  a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "'";
			}
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function GetLearnerList($_course, $_batch, $_rolecode, $_mode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

		echo $_mode;
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
            $_mode = mysqli_real_escape_string($_ObjConnection->Connect(),$_mode);

			if($_mode=="ShowUpload")
			{
				$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "'  ";
			}
			else if($_mode=="ShowConfirm")
            {
                $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, a.admitcardfilename,b.RSP_Code,b.RSP_Name,b.District_Name FROM tbl_admission_fee_waiver_scheme AS a left join vw_itgkname_distict_rsp as b
on a.Admission_ITGK_Code=b.ITGKCODE WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "'";
			}
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
      
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
            //$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";
			 $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Mobile, "
                    . "a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, a.Admission_Payment_Status,a.admitcardfilename FROM tbl_admission_fee_waiver_scheme AS a "
                    . "WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' "
                    . "AND a.Admission_Batch = '" . $_batch . "' ORDER BY a.Admission_Payment_Status DESC";
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
          
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
             $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedList($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
             $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
