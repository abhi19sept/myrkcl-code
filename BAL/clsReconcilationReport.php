<?php

/**
 * Description of clsAdmission
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsReconcilationReport {

//put your code here
    public function reconcilation_type1_1($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
			
			
            $_SelectQuery = "select b.* from tbl_payment_transaction as a right join tbl_admission as b on b.Admission_TranRefNo = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentReceive%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
        
    }
    public function reconcilation_type1_2($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select * from tbl_admission_transaction as a where a.Admission_Transaction_timestamp >= '".$startdate."' AND a.Admission_Transaction_timestamp <= '".$enddate."' and Admission_Transaction_Status  like('%success%')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
        
    }
    public function reconcilation_type1_3($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "SELECT * FROM tbl_payment_transaction as a WHERE (Pay_Tran_Status like '%PaymentReceive%' or Pay_Tran_Status like '%PaymentToRefund%') AND Pay_Tran_ProdInfo LIKE('%learner%') AND a.timestamp >= '".$startdate."'  AND a.timestamp <= '".$enddate."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
        
    }
    public function reconcilation_type1_4($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
           $_SelectQuery = "select c.invoice_no as Invoice_No,c.amount as Invoice_Amount,Date_Format(c.addtime,'%d-%m-%Y') as Invoice_Date, if(b.Admission_Payment_Status=1,'Success','Cancelled') as Invoice_Status  from tbl_payment_transaction as a right join tbl_admission as b on b.Admission_TranRefNo = a.Pay_Tran_PG_Trnid right join tbl_admission_invoice as c ON c.invoice_ref_id = b.Admission_Code where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentReceive%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
        
    }
    public function reconcilation_type1_5($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.* from tbl_payment_transaction as a right join tbl_payment_refund as c on c.Payment_Refund_Txnid = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentToRefund%' and c.Payment_Refund_ProdInfo like '%Learner%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type2_1($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select b.* from tbl_payment_transaction as a left join examdata as b on b.reexam_TranRefNo = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and (Pay_Tran_Status like '%PaymentReceive%') AND Pay_Tran_ProdInfo LIKE('%exam%')  and paymentstatus='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type2_2($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select * from tbl_reexam_transaction as a where a.Reexam_Transaction_timestamp >= '".$startdate."' AND a.Reexam_Transaction_timestamp <= '".$enddate."' and Reexam_Transaction_Status  like('%success%')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type2_3($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "SELECT * FROM tbl_payment_transaction as a WHERE (Pay_Tran_Status like '%PaymentReceive%' or Pay_Tran_Status like '%PaymentToRefund%') AND Pay_Tran_ProdInfo LIKE('%exam%') AND a.timestamp >= '".$startdate."'  AND a.timestamp <= '".$enddate."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type2_4($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.invoice_no as Invoice_No,c.amount as Invoice_Amount,Date_Format(c.addtime,'%d-%m-%Y') as Invoice_Date, if(b.paymentstatus=1,'Success','Cancelled') as Invoice_Status  from tbl_payment_transaction as a right join examdata as b on b.reexam_TranRefNo = a.Pay_Tran_PG_Trnid right join tbl_reexam_invoice as c ON c.exam_data_code = b.examdata_code where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentReceive%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type2_5($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.* from tbl_payment_transaction as a right join tbl_payment_refund as c on c.Payment_Refund_Txnid = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentToRefund%' and c.Payment_Refund_ProdInfo like '%Reexam%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type3_1($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select b.* from tbl_payment_transaction as a left join tbl_org_master as b on b.Org_TranRefNo = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and (Pay_Tran_Status like '%PaymentReceive%' or Pay_Tran_Status like '%PaymentToRefund%') AND Pay_Tran_ProdInfo LIKE('%Ncr%')  and Org_PayStatus='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type3_2($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select * from tbl_ncr_transaction as a where a.Ncr_Transaction_timestamp >= '".$startdate."' AND a.Ncr_Transaction_timestamp <= '".$enddate."' and Ncr_Transaction_Status  like('%success%')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type3_3($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "SELECT * FROM tbl_payment_transaction as a WHERE (Pay_Tran_Status like '%PaymentReceive%' or Pay_Tran_Status like '%PaymentToRefund%') AND Pay_Tran_ProdInfo LIKE('%Ncr%') AND a.timestamp >= '".$startdate."'  AND a.timestamp <= '".$enddate."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type3_4($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
                      $_SelectQuery = "select c.invoice_no as Invoice_No,c.amount as Invoice_Amount,Date_Format(c.addtime,'%d-%m-%Y') as Invoice_Date, if(b.Org_PayStatus=1,'Success','Cancelled') as Invoice_Status  from tbl_payment_transaction as a right join tbl_org_master as b on b.Org_TranRefNo = a.Pay_Tran_PG_Trnid right join tbl_ncr_invoice as c ON c.Ao_Ack_No = b.Org_Ack  where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentReceive%'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type3_5($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.* from tbl_payment_transaction as a right join tbl_payment_refund as c on c.Payment_Refund_Txnid = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentToRefund%' and c.Payment_Refund_ProdInfo like '%Ncr%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type4_1($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select b.* from tbl_payment_transaction as a left join tbl_courseitgk_mapping as b on b.Courseitgk_TranRefNo = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and Pay_Tran_Status like '%PaymentReceive%' AND Pay_Tran_ProdInfo LIKE('%eoi%')  and EOI_Fee_Confirm='1' and a.Pay_Tran_ITGK = b.Courseitgk_ITGK and Courseitgk_Course like '%CFA%' And Courseitgk_EOI=12";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type4_2($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select * from tbl_eoi_transaction as a where a.EOI_Transaction_timestamp >= '".$startdate."' AND a.EOI_Transaction_timestamp <= '".$enddate."' and EOI_Transaction_Status  like('%success%')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type4_3($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "SELECT * FROM tbl_payment_transaction as a WHERE (Pay_Tran_Status like '%PaymentReceive%' or Pay_Tran_Status like '%PaymentToRefund%') AND Pay_Tran_ProdInfo LIKE('%eoi%') AND a.timestamp >= '".$startdate."'  AND a.timestamp <= '".$enddate."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type4_4($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.invoice_no as Invoice_No,c.amount as Invoice_Amount,Date_Format(c.addtime,'%d-%m-%Y') as Invoice_Date, if(b.EOI_Fee_Confirm=1,'Success','Cancelled') as Invoice_Status  from tbl_payment_transaction as a right join tbl_courseitgk_mapping as b on b.Courseitgk_TranRefNo = a.Pay_Tran_PG_Trnid right join tbl_eoi_invoice as c ON c.Itgkcode = b.Courseitgk_ITGK where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentReceive%' and a.Pay_Tran_ITGK = b.Courseitgk_ITGK  And Courseitgk_EOI in(13,14,15) ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type4_5($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.* from tbl_payment_transaction as a right join tbl_payment_refund as c on c.Payment_Refund_Txnid = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentToRefund%' and c.Payment_Refund_ProdInfo like '%eoi%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type5_1($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select b.* from tbl_payment_transaction as a left join tbl_correction_copy as b on b.Correction_TranRefNo = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and (Pay_Tran_Status like '%PaymentReceive%') AND (Pay_Tran_ProdInfo like ('%Correction%') or Pay_Tran_ProdInfo like ('%dup%')) and Correction_Payment_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type5_2($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select * from tbl_correction_transaction as a where a.Correction_Transaction_timestamp >= '".$startdate."' AND a.Correction_Transaction_timestamp <= '".$enddate."' and Correction_Transaction_Status  like('%success%')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type5_3($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "SELECT * FROM tbl_payment_transaction as a WHERE (Pay_Tran_Status like '%PaymentReceive%' or Pay_Tran_Status like '%PaymentToRefund%') AND (Pay_Tran_ProdInfo like ('%Correction%') or Pay_Tran_ProdInfo like ('%dup%')) AND a.timestamp >= '".$startdate."'  AND a.timestamp <= '".$enddate."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type5_4($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.invoice_no as Invoice_No,c.amount as Invoice_Amount,Date_Format(c.addtime,'%d-%m-%Y') as Invoice_Date, if(b.Correction_Payment_Status=1,'Success','Cancelled') as Invoice_Status  from tbl_payment_transaction as a right join tbl_correction_copy as b on b.Correction_TranRefNo = a.Pay_Tran_PG_Trnid right join tbl_correction_invoice as c ON c.transaction_ref_id = b.cid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentReceive%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type5_5($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.* from tbl_payment_transaction as a right join tbl_payment_refund as c on c.Payment_Refund_Txnid = a.Pay_Tran_PG_Trnid where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentToRefund%' and (c.Payment_Refund_ProdInfo like ('%Correction%') or c.Payment_Refund_ProdInfo like ('%dup%'))";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type6_1($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.* from tbl_payment_transaction as a left join tbl_address_name_transaction as b on b.fld_transactionID = a.Pay_Tran_PG_Trnid join tbl_change_address_itgk as c on c.fld_ref_no = b.fld_ref_no	where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and (Pay_Tran_Status like '%PaymentReceive%') AND Pay_Tran_ProdInfo LIKE('%name%')  and b.fld_paymentStatus='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type6_2($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select * from tbl_address_name_transaction as a where a.fld_updatedOn >= '".$startdate."' 	AND a.fld_updatedOn <= '".$enddate."' and fld_Transaction_Status  like('%success%')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type6_3($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "SELECT * FROM tbl_payment_transaction as a WHERE (Pay_Tran_Status like '%PaymentReceive%' or Pay_Tran_Status like '%PaymentToRefund%') AND Pay_Tran_ProdInfo LIKE('%name%') AND a.timestamp >= '".$startdate."'  AND a.timestamp <= '".$enddate."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type6_4($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.invoice_no as Invoice_No,c.amount as Invoice_Amount,Date_Format(c.addtime,'%d-%m-%Y') as Invoice_Date, if(b.fld_paymentStatus='1','Success','Cancelled') as Invoice_Status  from tbl_payment_transaction as a right join tbl_address_name_transaction as b on b.fld_transactionID = a.Pay_Tran_PG_Trnid 	join tbl_change_address_itgk as d on d.fld_ref_no = b.fld_ref_no right join tbl_nameaddress_invoice as c ON c.invoice_ref_id = b.fld_ref_no where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentReceive%'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
    public function reconcilation_type6_5($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
				$enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
				
            $_SelectQuery = "select c.* from tbl_payment_transaction as a right join tbl_payment_refund as c on c.Payment_Refund_Txnid = a.Pay_Tran_PG_Trnid 
	where a.timestamp >= '".$startdate."' AND a.timestamp <= '".$enddate."' and a.Pay_Tran_Status like '%PaymentToRefund%' 
	and c.Payment_Refund_ProdInfo like '%name%';";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response ;
    }
}
