<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsWcdrejectedlearnercount {

    //put your code here

public function GetAllRejected($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
			$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4'){
				 $_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, 
									Count(b.Oasis_Admission_LearnerCode)
									AS pref from tbl_user_master as a
									inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.user_loginid
									inner join tbl_district_master as c on 
									b.Oasis_Admission_District = c.District_Code WHERE a.User_UserRoll='7' and 
									b.Oasis_Admission_Course='".$_course."' AND 
									b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible'
									AND Oasis_Admission_LearnerStatus in ('Rejected') group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			
			else if ($_LoginUserRole == '7') {
				//print_r($_SESSION);
				  $_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, Count(b.Oasis_Admission_LearnerCode)
									AS pref from tbl_user_master as a
									inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.user_loginid inner join tbl_district_master as c on 
									b.Oasis_Admission_District = c.District_Code WHERE a.User_UserRoll='7' and b.Oasis_Admission_Course='".$_course."' AND 
									b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible'
									AND Oasis_Admission_Final_Preference='". $_SESSION['User_LoginId'] ."'
									AND Oasis_Admission_LearnerStatus in ('Rejected') group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

			}
			else if ($_LoginUserRole == '17') {
				//print_r($_SESSION);
				  $_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, Count(b.Oasis_Admission_LearnerCode) 
								AS pref from tbl_user_master as a
								inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.User_LoginId inner join tbl_district_master as c on
								b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible' AND b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "'
								AND b.Oasis_Admission_LearnerStatus in ('Rejected') group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetLearnerList($_mode, $_batch, $_districtcode, $_Itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				$_districtcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_districtcode);
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
			   
				  $_SelectQuery3 = "SELECT b.*,d.Category_Name,e.Qualification_Name FROM tbl_oasis_admission as b
									LEFT JOIN tbl_category_master as d on b.Oasis_Admission_Category = d.Category_Code
									LEFT JOIN tbl_qualification_master as e on b.Oasis_Admission_Qualification = e.Qualification_Code
									WHERE Oasis_Admission_Batch = '" . $_batch . "' AND Oasis_Admission_District='" . $_districtcode . "'
									AND Oasis_Admission_Final_Preference='" . $_Itgk . "' AND b.Oasis_Admission_Eligibility='eligible' 
									AND Oasis_Admission_LearnerStatus in ('Rejected') Order by Oasis_Admission_Final_Priority";   
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
