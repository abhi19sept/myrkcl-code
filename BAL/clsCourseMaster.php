<?php

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCourseMaster {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select Course_Name, Course_Type,Course_Duration,Course_DurationType,
			Course_Medium,Course_ExaminationType,
			Course_ClassType,Course_Affiliate,Course_Status From tbl_course_master Where Course_Code='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRecord($_Status_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_DeleteQuery = "Delete From tbl_course_master Where Course_Code='" . $_Status_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add($_txtName, $_ddlCourseType, $_txtDuration, $_Duration, $_ddlMedium, $_ddlExamination, $_ddlClasstype, $_ddlAffiliate, $_ddlStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User_Code = $_SESSION['User_Code'];
				$_txtName = mysqli_real_escape_string($_ObjConnection->Connect(),$_txtName);
				
            $_InsertQuery = "Insert Into tbl_course_master(Course_Code,Course_User,Course_Name,Course_Type,Course_Duration,Course_DurationType,Course_Medium,Course_ExaminationType,Course_ClassType,Course_Affiliate,Course_Status) 
				Select Case When Max(Course_Code) Is Null Then 1 Else Max(Course_Code)+1 End as Course_Code,'" . $_User_Code . "' as Course_User,'" . $_txtName . "' as Course_Name,'" . $_ddlCourseType . "' as Course_Type,'" . $_txtDuration . "' as Course_Duration,'" . $_Duration . "' as Course_DurationType,
				'" . $_ddlMedium . "' as Course_Medium,'" . $_ddlExamination . "' as Course_ExaminationType,'" . $_ddlClasstype . "' as Course_ClassType,'" . $_ddlAffiliate . "' as Course_Affiliate,'" . $_ddlStatus . "' as Course_Status From tbl_course_master";


            $_DuplicateQuery = "Select * From tbl_course_master Where Course_Name='" . $_txtName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_Status_Code, $_txtName, $_ddlCourseType, $_txtDuration, $_Duration, $_ddlMedium, $_ddlExamination, $_ddlClasstype, $_ddlAffiliate, $_ddlStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				$_txtName = mysqli_real_escape_string($_ObjConnection->Connect(),$_txtName);
				
            $_UpdateQuery = "Update tbl_course_master set 
			Course_Name='" . $_txtName . "',
			Course_Type='" . $_ddlCourseType . "' ,
			Course_Type='" . $_ddlCourseType . "',
			Course_Duration	='" . $_txtDuration . "',
			Course_DurationType	='" . $_Duration . "',
			Course_Medium='" . $_ddlMedium . "',
			
			Course_ExaminationType ='" . $_ddlExamination . "',
			Course_ClassType='" . $_ddlClasstype . "',
			Course_Affiliate='" . $_ddlAffiliate . "',
			Course_Status='" . $_ddlStatus . "'
							Where Course_Code='" . $_Status_Code . "'";
            $_DuplicateQuery = "Select * From tbl_course_master Where Course_Name='" . $_txtName . "' and " . "Course_Code <> '" . $_Status_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllFill() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
         //   $_SelectQuery = "Select a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' AND `Courseitgk_ITGK` = '" . $_SESSION['User_LoginId'] . "' AND CURDATE() >= `CourseITGK_StartDate` AND CURDATE() <= `CourseITGK_ExpireDate` ORDER BY `CourseITGK_StartDate` ASC";
		 
		 $_SelectQuery = "Select a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' AND `Courseitgk_ITGK` = '" . $_SESSION['User_LoginId'] . "' AND CURDATE() >= `CourseITGK_UserCreatedDate` AND CURDATE() <= `CourseITGK_ExpireDate` AND Course_Type!='6' ORDER BY `CourseITGK_UserCreatedDate` ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
            public function GetAllFillBlockCenter() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master ORDER BY Course_Code ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
        public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if($_SESSION['User_UserRoll']=='7'){
            $_SelectQuery = "Select distinct a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' AND `Courseitgk_ITGK` = '" . $_SESSION['User_LoginId'] . "' and Course_Type!='6' ORDER BY `CourseITGK_StartDate` ASC";
            }
            else
            {
            $_SelectQuery = "Select distinct a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' and Course_Type!='6' ORDER BY `CourseITGK_StartDate` ASC";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetAdmissionCourseDD() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
          
            $_SelectQuery = "Select a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 8 AND `CourseITGK_BlockStatus` = 'unblock' ORDER BY `CourseITGK_StartDate` ASC";
        
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_course_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCourseCode($_Name) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Name = mysqli_real_escape_string($_ObjConnection->Connect(),$_Name);
				
            $_SelectQuery = "Select Course_Code, Course_Name From tbl_course_master WHERE `Course_Name` = '" . $_Name . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllMODIFY() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT Admission_Course, Course_Name From tbl_admission as a INNER JOIN tbl_course_master as b ON a.Admission_Course = b.Course_Code WHERE Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAdmissionCourseName($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_SelectQuery = "Select Course_Code, Course_Name From tbl_course_master WHERE `Course_Code` = '" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}

?>