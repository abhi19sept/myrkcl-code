<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionManualApproved {
    //put your code here
    
    public function GetAll($_CenterCode, $_Lcode, $_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				
			$_SelectQuery = "Select * FROM tbl_correction_copy WHERE Correction_ITGK_Code = '" . $_CenterCode . "' AND
								dispatchstatus = '" . $_Status . "'
							  AND lcode = '" . $_Lcode . "'	AND Correction_Payment_Status='1'";  				
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function FILLStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where correctionrole='Yes' AND capprovalid='3'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	 public function FILLAPPROVEDSTATUS() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT capprovalid, cstatus FROM tbl_capcategory where correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GETALLLOT() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetDatabyCode($_Cid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				
            $_SelectQuery = "Select * From tbl_correction_copy Where cid='" . $_Cid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetOldValues($_LearnerCode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				
            $_SelectQuery = "Select Admission_Name,Admission_Fname,Admission_Code From tbl_admission Where
								Admission_LearnerCode='" . $_LearnerCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function UpdateToProcess($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_Txnid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId']))
					{
							$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
							$_LOT = mysqli_real_escape_string($_ObjConnection->Connect(),$_LOT);
							
						$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', remarks='Manually Lot Assign',"
										. "lot='" . $_LOT . "', totalmarks='" . $_Marks . "'" 
										. " Where cid='" . $_Cid . "'";
						$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);					
				} else {
						session_destroy();
						?>
						<script> window.location.href = "index.php";</script> 
						<?php
					}                      
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }	
	
	public function UpdateToProcessWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks, $_Txnid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId']))
					{	
						$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
						
						$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', remarks='Manually Lot Assign', exameventname= '" . $_EventName . "',"
										. "lot='" . $_LOT . "', totalmarks='" . $_Marks . "'"
										. " Where cid='" . $_Cid . "'";
						$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
					} else {
							session_destroy();
							?>
							<script> window.location.href = "index.php";</script> 
							<?php
						}                      
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateToProcessRejected($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_LName, $_Fname) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	
				$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				$_LOT = mysqli_real_escape_string($_ObjConnection->Connect(),$_LOT);
				
           $_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', remarks='" . $_Remark . "',"
                    . "lot='" . $_LOT . "', totalmarks='" . $_Marks . "', cfname='" . $_LName . "', cfaname='" . $_Fname . "'"
                    . " Where cid='" . $_Cid . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateToProcessRejectedWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks, $_LName, $_Fname) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	
				$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				$_LOT = mysqli_real_escape_string($_ObjConnection->Connect(),$_LOT);
				
           $_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', remarks='" . $_Remark . "', exameventname= '" . $_EventName . "',"
                    . "lot='" . $_LOT . "', totalmarks='" . $_Marks . "', cfname='" . $_LName . "', cfaname='" . $_Fname . "'"
                    . " Where cid='" . $_Cid . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
}