<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFacultyResultRPT
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');

$_ObjConnection = new _Connection();
$_Response = array();

class clsFacultyResultRPT {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "select c.Organization_Name as ITGK_Name,d.Organization_Name as RSP_Name,e.District_Name,f.Tehsil_Name,
                                max(a.ResExam_obtainmarks) as Obtained_Marks,ITGK_Code,a.ResExam_learner,g.Staff_Name,a.ResExam_totalmarks,
                                a.ResExam_date,a.ResExam_attempt
                                from tbl_facultyexamresultreport as a inner join tbl_user_master as b
                                on a.ITGK_Code=b.User_LoginId inner join tbl_organization_detail as c 
                                on b.User_Code=c.Organization_User inner join tbl_organization_detail as d
                                on a.Admission_RspName=d.Organization_User inner join tbl_district_master as e
                                on a.Admission_District=e.District_Code inner join tbl_tehsil_master as f
                                on c.Organization_Tehsil=f.Tehsil_Code inner join tbl_staff_detail as g
                                on concat(g.Staff_User,'_',g.Staff_Code)=a.ResExam_learner  group by ResExam_learner";
            } elseif ($_SESSION['User_UserRoll'] == 23) {
//                $_SelectQuery = "select c.Organization_Name as ITGK_Name,d.Organization_Name as RSP_Name,e.District_Name,f.Tehsil_Name,
//                                max(a.ResExam_obtainmarks) as Obtained_Marks,ITGK_Code,a.ResExam_learner,g.Staff_Name,a.ResExam_totalmarks,
//                                a.ResExam_date,a.ResExam_attempt
//                                from tbl_facultyexamresultreport as a inner join tbl_user_master as b
//                                on a.ITGK_Code=b.User_LoginId inner join tbl_organization_detail as c 
//                                on b.User_Code=c.Organization_User inner join tbl_organization_detail as d
//                                on a.Admission_RspName=d.Organization_User inner join tbl_district_master as e
//                                on a.Admission_District=e.District_Code inner join tbl_tehsil_master as f
//                                on c.Organization_Tehsil=f.Tehsil_Code inner join tbl_staff_detail as g
//                                on concat(g.Staff_User,'_',g.Staff_Code)=a.ResExam_learner 
//                                 where DPO='" . $_SESSION['User_LoginId'] . "' group by ResExam_learner";
//            
                $_SelectQuery ="";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}
