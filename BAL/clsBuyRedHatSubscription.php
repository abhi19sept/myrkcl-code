<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBuyRedHatSubscription
 *
 * @author Mayank
 */

require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();
 
class clsBuyRedHatSubscription extends paymentFunctions {
    //put your code here

	public function checkauthorization() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_ITGK_Code = $_SESSION['User_LoginId'];
					$_SelectQuery="select * from tbl_courseitgk_mapping where Courseitgk_ITGK='".$_SESSION['User_LoginId']."' and 
									Courseitgk_EOI='18' and EOI_Fee_Confirm='1'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);					
				}
				else{
					session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
				}
             } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAuthorizeType() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_ITGK_Code = $_SESSION['User_LoginId'];
					$_SelectQuery="select * from tbl_redhat_sub_type where redhat_sub_status='Active'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);					
				}
				else{
					session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
				}
             } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetFee()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
            $_SelectQuery="select redhat_expcenter_fee FROM tbl_redhat_expcenter_fee WHERE redhat_expcenter_fee_status = 'Active'";
			$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function ShowPayMode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b 
								ON a.Event_Payment = b.payment_id WHERE a.Event_CourseEOI = '18' AND Event_Name='5' AND
								NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function AddPayTran($postValues, $productinfo, $centerarray) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
                $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $postValues['amounts'], 
															$centerarray, $postValues['ddlQty'], 18);
            } else {
                session_destroy();
            ?>
                <script> window.location.href = "logout.php";</script> 
            <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }
	
 public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
