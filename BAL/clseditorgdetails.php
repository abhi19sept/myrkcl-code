<?php


/**
 * Description of clsOrgDetail
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';


$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clseditorgdetails {
	
	 public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		$_LoginRole = $_SESSION['User_UserRoll'];
			if($_LoginRole == '11' || $_LoginRole == '1') {
					$_SelectQuery2 = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_courseitgk_mapping as c
				    on b.user_loginid=c.Courseitgk_ITGK where b.User_UserRoll='7' AND Courseitgk_Course='RS-CIT'";
             
			}
			  
				//$_SelectQuery2 = "Select * from tbl_staff_detail where Staff_User IN ($CenterCode3)";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response2;
            
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response1;
    }
	
		
     public function UPDATE($_Code,$_OrgName) 
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_OrgName = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgName);
				
			
		  $_UpdateQuery = "Update tbl_organization_detail set 
						
						Organization_Name='" . $_OrgName . "',
						IsNewRecord='Y'
						
						Where Organization_Code='" . $_Code . "'";
            
			
			
					 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
					
              
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	public function GetDatabyCode($_Org_Code)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Org_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Org_Code);
				
            $_SelectQuery = "Select *  From tbl_organization_detail Where Organization_Code='" . $_Org_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Org_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Org_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Org_Code);
				
            $_DeleteQuery = "Delete From tbl_organization_detail Where Organization_Code='" . $_Org_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	
	
	
	
}
