<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOwnershipChangeFinalApproval
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/upload_ftp_doc.php';

$_ObjFTPConnection = new ftpConnection();


$_ObjConnection = new _Connection();
$_Response = array();

class clsOwnershipChangeFinalApproval {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code 
                            where Org_PayStatus='1' and Org_Application_Approval_Status='Approved' and SPCenter_Agreement!='0'";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code 
                            INNER JOIN tbl_user_master as c on a.Org_Itgk_Code = c.User_LoginId 
                            where Org_PayStatus='1' and Org_Application_Approval_Status='Approved' and SPCenter_Agreement!='0'
                            and c.User_Rsp= '" . $_SESSION['User_Code'] . "'";
            }
            else{
            $_SelectQuery = "";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function GetOrgDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b
                            ON a.Organization_Type=b.Org_Type_Code where Org_PayStatus='1' and Organization_Code='" . $_CenterCode . "'";
        
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function SendSMS($_SMS, $_CenterCode, $_MobileNew) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_MobileNew = mysqli_real_escape_string($_ObjConnection->Connect(),$_MobileNew);
				
            $_UpdateQuery = "Update tbl_ownership_change set Org_Final_Message='" . $_SMS . "', Org_Final_Approval_Status='OnHold'"
                    . "Where Org_Itgk_Code='" . $_CenterCode . "' AND Org_Final_Approval_Status='Pending'";
            
            $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);   
                
            $_SelectQuery = "Select * from tbl_user_master Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response[2]);
            $_Mobile = $_Row['User_MobileNo'];
            $_Rsp_Code = $_Row['User_Rsp'];
            
            $_SelectQuery1 = "Select * from tbl_user_master Where User_Code='" . $_Rsp_Code . "'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_RSPMobile = $_Row1['User_MobileNo'];
            SendSMS($_Mobile,$_SMS);
            SendSMS($_MobileNew,$_SMS);
            SendSMS($_RSPMobile,$_SMS);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
    public function GenerateOTP($_Technical,$_Accounts,$_Marketing,$_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }
        
        
        if($_Technical == '1'){
            $_Tech = '9649900809';
            $_OTPTech = OTP();
            //$_SMSTech = "OTP for Verifying Authority to approve Ownership Change on MYRKCL is " . $_OTPTech;
            $_SMSTech = "Verifying Authority OTP is '" . $_OTPTech ."' for  approve Ownership Change on MYRKCL. Rajasthan Knowledge Corporation Limited";
        }else{
            $_Tech = '';
            $_OTPTech = '';
            $_SMSTech = '';
        }
        if($_Accounts == '24'){
            $_Acc = '9649900801';
            $_OTPAcc = OTP();
            //$_SMSAcc = "OTP for Verifying Authority to approve Ownership Change on MYRKCL is " . $_OTPAcc;
            $_SMSAcc = "Verifying Authority OTP is '" . $_OTPAcc ."' for  approve Ownership Change on MYRKCL. Rajasthan Knowledge Corporation Limited";
        }else{
            $_Acc = '';
            $_OTPAcc = '';
            $_SMSAcc = '';
        }
        if($_Marketing == '4'){
            $_Markt = '9649900940';
            $_OTPMarkt = OTP();
            //$_SMSMarkt = "OTP for Verifying Authority to approve Ownership Change on MYRKCL is " . $_OTPMarkt;
            $_SMSMarkt = "Verifying Authority OTP is '" . $_OTPMarkt ."' for  approve Ownership Change on MYRKCL. Rajasthan Knowledge Corporation Limited";
        }else{
            $_Markt = '';
            $_OTPMarkt = '';
            $_SMSMarkt = '';
        }
        
        date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d H:i:s");

        try {
           if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) { 
            $_InsertQuery = "Insert Into tbl_approving_authority_otpverify(Approving_Authority_Code,Approving_Authority_CenterCode,Approving_Authority_Mobile_Tech"
                    . ",Approving_Authority_Mobile_Acc,Approving_Authority_Mobile_Markt,Approving_Authority_SMS_Tech,Approving_Authority_SMS_Acc"
                    . ",Approving_Authority_SMS_markt,Approving_Authority_OTP_Start_Date)"
                    . "Select Case When Max(Approving_Authority_Code) Is Null Then 1 Else Max(Approving_Authority_Code)+1 End as Approving_Authority_Code,"
                    . "'" .  $_CenterCode . "' as Approving_Authority_CenterCode,'" .  $_Tech . "' as Approving_Authority_Mobile_Tech,'" . $_Acc . "' as Approving_Authority_Mobile_Acc,'" . $_Markt . "' as Approving_Authority_Mobile_Markt,"
                    . "'" .  $_OTPTech . "' as Approving_Authority_SMS_Tech,'" . $_OTPAcc . "' as Approving_Authority_SMS_Acc,'" . $_OTPMarkt . "' as Approving_Authority_SMS_markt,"
                    . "'" . $_Date . "' as Approving_Authority_OTP_Start_Date"
                    . " From tbl_approving_authority_otpverify";
//            echo $_InsertQuery;
//            die;
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            SendSMS($_Tech, $_SMSTech);
            SendSMS($_Acc, $_SMSAcc);
            SendSMS($_Markt, $_SMSMarkt);
            
//            $_DuplicateQuery = "Select * From tbl_ownership_change Where Org_Mobile='" . $_MobNo . "' OR Org_Email = '" . $_EmailId . "'";
//            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            
            
            //echo $_Response[0];
//            if ($_Response[0] == Message::NoRecordFound) {
//                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
//                SendSMS($_MobNo, $_SMS);
//			   $Admin_Name='RKCL';
//			   	$mail_data = array();
//			$mail_data['email'] = $_EmailId;
//			$mail_data['admin'] = $Admin_Name;
//						
//			$mail_data['Msg'] = $_EmailMsg;
//			$subject = "OTP for Validating Email on MYRKCL";						
//			$sendMail = new sendMail();
//			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
//				sendMail("".$_Email."", "".$Admin_Name."", $_SMS, $_SMS, "RKCL Support");
//            } else {
//                $_Response[0] = Message::DuplicateRecord;
//                $_Response[1] = Message::Error;
//            }
            
             } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Verify($_OTPETech, $_OTPAcc, $_OTPMrkt) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) { 
            $_SelectQuery = "Select * FROM tbl_approving_authority_otpverify WHERE Approving_Authority_SMS_Tech='" . $_OTPETech . "' "
                    . "AND Approving_Authority_SMS_Acc = '" . $_OTPAcc . "' AND Approving_Authority_SMS_markt = '" . $_OTPMrkt . "' "
                    . "AND Approving_Authority_Status = '0' and NOW() <= DATE_ADD(Approving_Authority_OTP_Start_Date, INTERVAL 30 MINUTE)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid Verification Details. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_approving_authority_otpverify set Approving_Authority_Status='1' "
                                . "WHERE Approving_Authority_SMS_Tech='" . $_OTPETech . "' "
                                . "AND Approving_Authority_SMS_Acc = '" . $_OTPAcc . "' AND Approving_Authority_SMS_markt = '" . $_OTPMrkt . "' "
                                 . "AND Approving_Authority_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateUserMaster($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                
            function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
            }
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
            $_Password = OTP();
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
            $_SelectQuery = "Select * FROM tbl_ownership_change WHERE Organization_Code='" . $_Code . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response1[2]);
            $Email = $_Row['Org_Email'];
            $Mobile = $_Row['Org_Mobile'];
            $ITGKCode = $_Row['Org_Itgk_Code'];
            
            //$_SMSApproval = "Dear ITGK, Your request for Ownership Change has been APPROVED on MYRKCL."
                  //  . "Your new login details User Name: " . $ITGKCode . " Password: " . $_Password . "";
					
			$_SMSApproval = "Dear ITGK, Your request for Ownership Change has been APPROVED on MYRKCL
User Name: '" . $ITGKCode . "' Password: '" . $_Password . "'. Kindly use above username and password for login. Rajasthan Knowledge Corporation Limited";
            
            $_SMSOld = "Dear ITGK, Your request for Ownership Change has been APPROVED on MYRKCL. RKCL";
             
            $_SMSRSP = "Dear Service Provider, Ownership Change request for Center Code" .$ITGKCode. "has been COMPLETED on MYRKCL. RKCL";
            
            $_InsertQuery = "INSERT INTO tbl_user_master_ownership_change_log
                            (User_LoginId,User_EmailId,User_MobileNo,User_Password,User_Status,User_Ownership_Change_date)
                            SELECT User_LoginId,User_EmailId,User_MobileNo,User_Password,User_Status,'" . $_Date . "' FROM tbl_user_master 
                            WHERE User_LoginId='" . $ITGKCode . "'";    	
            $_ResponseI=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
            $_UpdateQuery1 = "Update tbl_user_master set User_EmailId='" . $Email . "',"	         
                    . "User_Password='" . $_Password . "', User_MobileNo='" . $Mobile . "', IsNewRecord='Y', IsNewCRecord='Y',"
                    . "IsNewSRecord='Y', IsNewOnlineLMSRecord='Y' Where User_LoginId='" . $ITGKCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            
            $_SelectQuery2 = "Select * from tbl_user_master Where User_LoginId='" . $ITGKCode . "'";
            
            $_Response2=$_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            $_Row11 = mysqli_fetch_array($_Response2[2]);
            $_Mobileold = $_Row11['User_MobileNo'];
            $_Rsp_Code = $_Row11['User_Rsp'];
            
            $_SelectQuery1 = "Select * from tbl_user_master Where User_Code='" . $_Rsp_Code . "'";
            $_Response12=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row12 = mysqli_fetch_array($_Response12[2]);
            $_RSPMobile = $_Row12['User_MobileNo'];
            
            SendSMS($Mobile, $_SMSApproval);
            SendSMS($_Mobileold, $_SMSOld);
            SendSMS($_RSPMobile, $_SMSRSP);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function UpdateOrgDetail($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
            $_SelectQuery = "Select * FROM tbl_ownership_change WHERE Organization_Code='" . $_Code . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response1[2]);
            $ITGKCode = $_Row['Org_Itgk_Code'];
            
            $_InsertQuery = "INSERT INTO tbl_organization_ownership_change_log
                            (Organization_User,Organization_Name,Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,
                            Organization_DocType,Organization_ScanDoc,Organization_UID,Organization_AddProof,Organization_AppForm,
                            Organization_TypeDocId,Organization_PAN,Organization_Ownership_Change_Date)
                            SELECT Organization_User,Organization_Name,Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,
                            Organization_DocType,Organization_ScanDoc,Organization_UID,Organization_AddProof,Organization_AppForm,
                            Organization_TypeDocId,Organization_PAN,'" . $_Date . "' FROM tbl_organization_detail AS a INNER JOIN 
                            tbl_user_master AS b ON a.Organization_User=b.User_Code
                            WHERE b.User_LoginId='" . $ITGKCode . "'";    	
            $_ResponseI=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
            $_InsertQuery1 = "Insert into tbl_organization_detail_log select A.*,'Ownership_Change' from tbl_organization_detail as a "
                    . "INNER JOIN tbl_user_master AS b ON a.Organization_User=b.User_Code WHERE b.User_LoginId='" . $ITGKCode . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
            
           /** $_UpdateQuery = "Update tbl_organization_detail AS a, tbl_user_master AS b "
                    . "set Organization_Name='" . $_Row['Organization_Name'] . "',"
                    . "Organization_RegistrationNo='" . $_Row['Organization_RegistrationNo'] . "',"
                    . "Organization_FoundedDate='" . $_Row['Organization_FoundedDate'] . "',"
                    . "Organization_Type='" . $_Row['Organization_Type'] . "',"
                    . "Organization_DocType='" . $_Row['Organization_DocType'] . "', Organization_ScanDoc='" . $_Row['Organization_ScanDoc'] . "',"
                    . "Organization_UID='" . $_Row['Organization_UID'] . "', Organization_AddProof='" . $_Row['Organization_AddProof'] . "',"
                    . "Organization_AppForm='" . $_Row['Organization_AppForm'] . "', Organization_TypeDocId='" . $_Row['Organization_TypeDocId'] . "',"
                    . "Organization_PAN='" . $_Row['Org_PAN'] . "'"
                    . "Where a.Organization_User=b.User_Code AND b.User_LoginId='" . $ITGKCode . "'"; **/
					
					
			$_UpdateQuery = "Update tbl_organization_detail AS a, tbl_user_master AS b "
                    . "set Organization_Name='" . $_Row['Organization_Name'] . "',"
                    . "Organization_RegistrationNo='" . $_Row['Organization_RegistrationNo'] . "',"
                    . "Organization_FoundedDate='" . $_Row['Organization_FoundedDate'] . "',"
                    . "Organization_Type='" . $_Row['Organization_Type'] . "',"
                    . "Organization_DocType='" . $_Row['Organization_DocType'] . "', Organization_ScanDoc='" . $_Row['Organization_ScanDoc'] . "',"
                    . "Organization_UID='" . $_Row['Organization_UID'] . "', Organization_AddProof='" . $_Row['Organization_AddProof'] . "',"
                    . "Organization_AppForm='" . $_Row['Organization_AppForm'] . "', Organization_TypeDocId='" . $_Row['Organization_TypeDocId'] . "',"
                    . "Organization_PAN='" . $_Row['Org_PAN'] . "', a.IsNewCRecord='Y' ,"
                    . "a.IsNewSRecord='Y', a.IsNewOnlineLMSRecord='Y' "
                    . "Where a.Organization_User=b.User_Code AND b.User_LoginId='" . $ITGKCode . "'";
		
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function UpdateBankAcc($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            $_SelectQuery = "Select * FROM tbl_ownership_change WHERE Organization_Code='" . $_Code . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response1[2]);
            $ITGKCode = $_Row['Org_Itgk_Code'];
            
            $_InsertQuery = "INSERT INTO tbl_bank_account_ownership_change_log"
                    . "(SELECT * FROM tbl_bank_account WHERE Bank_User_Code='" . $ITGKCode . "')";    	
            $_ResponseI=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            //UPDATE QUERY TO UPDATE BANK ACCOUNT DATA IN MAIN TABLE
            $_UpdateQuery = "Update tbl_bank_account"
                    . " set Bank_Account_Name='" . $_Row['Bank_Account_Name'] . "',"
                    . " Bank_Account_Number='" . $_Row['Bank_Account_Number'] . "',"
                    . "Bank_Account_Type='" . $_Row['Bank_Account_Type'] . "',"
                    . " Bank_Ifsc_code='" . $_Row['Bank_Ifsc_code'] . "',"
                    . "Bank_Name='" . $_Row['Bank_Name'] . "', Bank_Micr_Code='" . $_Row['Bank_Micr_Code'] . "',"
                    . "Bank_Branch_Name='" . $_Row['Bank_Branch_Name'] . "', Pan_No='" . $_Row['Bank_Pan_No'] . "',"
                    . "Pan_Name='" . $_Row['Bank_Pan_Name'] . "', Bank_Id_Proof='" . $_Row['Bank_Id_Proof'] . "',"
                    . "Pan_Document='" . $_Row['Bank_Pan_Document'] . "', Bank_Document='" . $_Row['Bank_Id_Doc'] . "'"
                    . "Where Bank_User_Code ='" . $ITGKCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function UpdateAgreement($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
            $_SelectQuery = "Select * FROM tbl_ownership_change WHERE Organization_Code='" . $_Code . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response1[2]);
            $ITGKCode = $_Row['Org_Itgk_Code'];
            
            $_InsertQuery = "INSERT INTO tbl_spcenter_agreement_ownership_change_log 
                            (SELECT * FROM tbl_spcenter_agreement WHERE SPCenter_CenterCode='" . $ITGKCode . "')";    	
            $_ResponseI=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
            $_UpdateQuery1 = "Update tbl_spcenter_agreement set SPCenter_Agreement='" . $_Row['SPCenter_Agreement'] . "'"
                    . "Where Organization_Code='" . $_Code . "'";
            
            $_Response2=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            
            $_UpdateQuery = "Update tbl_ownership_change set Org_Final_Approval_Status='Approved', Org_Final_Approval_Date='" . $_Date . "'"
                    . "Where Organization_Code='" . $_Code . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
