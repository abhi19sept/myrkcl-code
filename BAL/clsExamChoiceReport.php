<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsExamChoiceReport.php
 *
 * @author Yogendra soni
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsExamChoiceReport 
{
    //put your code here
  
   public function GetAll() 
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
		    $_SelectQuery = "SELECT ch.centercode, 
							dm1.District_Name AS Choice1_Dist, th1.Tehsil_Name AS Choice1_Thesil,district,choice1,
							dm2.District_Name AS Choice2_Dist, th2.Tehsil_Name AS Choice2_Thesil,district,choice2,
							dm3.District_Name AS Choice3_Dist, th3.Tehsil_Name AS Choice3_Thesil,district,choice3,
							dm4.District_Name AS Choice4_Dist, th4.Tehsil_Name AS Choice4_Thesil,district,choice4, 
							dm5.District_Name AS Choice5_Dist, th5.Tehsil_Name AS Choice5_Thesil ,district,choice5
							FROM tbl_examchoicemaster ch LEFT JOIN tbl_tehsil_master th1 ON th1.Tehsil_Code = ch.choice1 
							LEFT JOIN tbl_district_master dm1 ON dm1.District_Code = th1.Tehsil_District
							LEFT JOIN tbl_tehsil_master th2 ON th2.Tehsil_Code = ch.choice2 
							LEFT JOIN tbl_district_master dm2 ON dm2.District_Code = th2.Tehsil_District
							LEFT JOIN tbl_tehsil_master th3 ON th3.Tehsil_Code = ch.choice3 
							LEFT JOIN tbl_district_master dm3 ON dm3.District_Code = th3.Tehsil_District 
							LEFT JOIN tbl_tehsil_master th4 ON th4.Tehsil_Code = ch.choice4 
							LEFT JOIN tbl_district_master dm4 ON dm4.District_Code = th4.Tehsil_District 
							LEFT JOIN tbl_tehsil_master th5 ON th5.Tehsil_Code = ch.choice5 
							LEFT JOIN tbl_district_master dm5 ON dm5.District_Code = th5.Tehsil_District 
							WHERE ch.status like '%Approve%' ORDER BY ch.centercode";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
}
