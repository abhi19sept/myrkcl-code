<?php

/**
 * Description of clsApplyRscfaCertificate
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
include('DAL/smtp_class.php');
$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsApplyRscfaCertificate {

    //put your code here

    public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master WHERE Course_Code='5'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_ITGK = $_SESSION['User_LoginId'];
            $_SelectQueryGetEvent1 = "SELECT Event_Name FROM tbl_event_management WHERE Event_Category = '14' AND
										Event_Name = '19' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
            $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
            $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
            if ($_getEvent['Event_Name'] == '19') {
                $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
								order by Batch_Code DESC";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                //echo "Invalid User Input";
                return;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function viewlearners($_Course, $_Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_LoginRole = $_SESSION['User_UserRoll'];
                if ($_LoginRole == '7') {
                    $_SelectQuery = "select Admission_LearnerCode,Admission_ITGK_Code,Admission_Name,Admission_Fname,Admission_DOB,
							Admission_Mobile,Admission_Email,Timestamp,apply_rscfa_cert_lcode,Admission_Course,Admission_Batch
							from tbl_admission as a left join tbl_apply_rscfa_certificate as b
							on a.Admission_LearnerCode=b.apply_rscfa_cert_lcode
							where Admission_Course='" . $_Course . "' and Admission_Batch='" . $_Batch . "' and
							Admission_Payment_Status='1' and Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Applyforcertificate($lcode, $batch, $course, $EmailAdd) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $EmailAdd = mysqli_real_escape_string($_ObjConnection->Connect(),$EmailAdd);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK_Code = $_SESSION['User_LoginId'];
                $_LoginRole = $_SESSION['User_UserRoll'];
                if ($_LoginRole == '7') {
                    $_chkDuplicate_learnerCode = "Select apply_rscfa_cert_email From tbl_apply_rscfa_certificate Where apply_rscfa_cert_email='" . $EmailAdd . "'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate_learnerCode, Message::SelectStatement);
                    if ($_Response2[0] == Message::NoRecordFound) {
                        $_InsertQuery = "insert into tbl_apply_rscfa_certificate 	
                                                                (apply_rscfa_cert_itgk,apply_rscfa_cert_lcode,apply_rscfa_cert_lname,
                                                                apply_rscfa_cert_fname,apply_rscfa_cert_dob,apply_rscfa_cert_course,apply_rscfa_cert_batch,
                                                                apply_rscfa_cert_lmobile,apply_rscfa_cert_rsp_code,apply_rscfa_cert_admission_date,apply_rscfa_cert_email)
                                                                select Admission_ITGK_Code,Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB,
                                                                Admission_Course,Admission_Batch,Admission_Mobile,Admission_RspName,Timestamp,'" . $EmailAdd . "' from 
                                                                tbl_admission where Admission_LearnerCode='" . $lcode . "' and Admission_Course='" . $course . "' and 
                                                                Admission_Batch='" . $batch . "' and Admission_Payment_Status='1'";
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    } else {
                        $_Response[0] = Message::DuplicateRecord;
                        $_Response[1] = Message::Error;
                    }
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
