<?php

/*
 *  author Viveks

 */


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


class clsUpdateIntake {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select System_Code,System_Type,"
                    . "System_Processor,System_RAM,System_HDD, System_Status, Processor_Name From tbl_system_info as a inner join tbl_processor_master as b on a.System_Processor = b.Processor_Code where System_User='". $_SESSION['User_LoginId']."'" ;
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function Update($_Count,$Course,$Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$_Count = mysqli_real_escape_string($_ObjConnection->Connect(),$_Count);
				$Course = mysqli_real_escape_string($_ObjConnection->Connect(),$Course);
				$Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$Batch);
				
            $_Intake=$_Count * 24;
//            if($Course == '4'){
                $Course = '1';
//            }
            $_InsertQuery = "Insert Into tbl_intake_master(Intake_Code,Intake_User,Intake_Center,Intake_Course,Intake_Batch,"
                    . "Intake_Base_Intake,Intake_Available,Intake_Consumed,Intake_NOS) "
                    . "Select Case When Max(Intake_Code) Is Null Then 1 Else Max(Intake_Code)+1 End as Intake_Code,"
                    . "'1' as Intake_User,'" . $_SESSION['User_LoginId'] . "' as Intake_Center,'" . $Course . "' as Intake_Course,"
                    . "'" . $Batch . "' as Intake_Batch,'" . $_Intake . "' as Intake_Base_Intake,'" . $_Intake . "' as Intake_Available,"
                    . "'0' as Intake_Consumed,'" . $_Count . "' as Intake_NOS"
                    . " From tbl_intake_master";
            $_DuplicateQuery = "Select * From tbl_intake_master  Where Intake_Center= '". $_SESSION['User_LoginId']."' AND Intake_Batch= '" . $Batch . "' AND Intake_Course= '" . $Course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
            
            $_UpdateQuery = "Update tbl_intake_master set Intake_Base_Intake=Intake_Base_Intake + " . $_Intake . ",Intake_Available=Intake_Available + " . $_Intake . ",Intake_NOS=Intake_NOS + " . $_Count . " Where Intake_Center= '". $_SESSION['User_LoginId']."' AND Intake_Batch= '" . $Batch . "' AND Intake_Course= '" . $Course . "'";           
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
            }
            
         } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
     public function Update_System($_UserPermissionArray) {
         //print_r($_UserPermissionArray);
        global $_ObjConnection;
        $_ObjConnection->Connect();
         try {
            $_InsertQuery="";
            $_CountLength=count($_UserPermissionArray);
            for($i=0;$i<$_CountLength;$i++)
            {
             $_UpdateQuery = "Update tbl_system_info set System_Status='Confirmed By Center' WHERE System_Code = '" . $_UserPermissionArray[$i]['Function'] . "' AND System_User='". $_SESSION['User_LoginId']."'"; 
             $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
                      
        }
        
        catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
    
    public function DeleteRecord($_System_Code,$_type,$_status)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_System_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_System_Code);
				
            if($_type == 'Client' && $_status == 'Confirmed By Center')
            {
              $_Intake = 12;
              $_UpdateQuery = "Update tbl_intake_master set Intake_Base_Intake=Intake_Base_Intake - " . $_Intake . ","
                      . "Intake_Available=Intake_Available - " . $_Intake . " Where Intake_Center= '". $_SESSION['User_LoginId']."' "; 
              $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
              $_DeleteQuery = "Delete From tbl_system_info Where System_Code='" . $_System_Code . "'"; 
              $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);               
            }
            else{
            $_DeleteQuery = "Delete From tbl_system_info Where System_Code='" . $_System_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}


