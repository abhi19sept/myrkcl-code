<?php
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsgoventryformoperator {
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	$_SelectQuery = "Select District_Name From tbl_district_master where District_Status=1";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);          
			}	 
		catch (Exception $_ex) {	$_Response[0] = $_ex->getLine() . $_ex->getTrace();
									$_Response[1] = Message::Error;            
								}
        return $_Response;
    } 
	
	public function GetEmpId() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select id From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }	
	
     public function GetEmpdepartment() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select gdname From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }    
    
    public function GetEmpdesignation() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct designationname From tbl_designationname_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetBankdistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct District_Name From tbl_district_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetBankname($districtid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct bankname From tbl_rajbank_master where bankdistrict='" . $districtid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetDobProof() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select cdocname, docid From tbl_cdoccategory_master where active = 'Yes' AND type = 'govreimbursement'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }     
    
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Status_Code,Status_Name,Status_Description From "
                    . "tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
    public function DeleteRecord($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_DeleteQuery = "Delete From tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
    public function Add($_LearnerCode, $_CenterCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName,
						$_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
						$_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_fileReceipt, $_fileBirth, $_EmpDobProof) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_fileReceipt1 =	$_fileReceipt .'_payment' . '.jpg';
			$_fileBirth1   =	$_fileBirth .'_birth' . '.jpg';				
			//$_ITGK_Code 	=   $_SESSION['User_LoginId'];
			$_User_Code		 =   $_SESSION['User_Code'];
			
            $_InsertQuery = "Insert Into tbl_govempentryform(learnercode, fname, faname, ldistrictname, lmobileno, lemailid, officeaddress, gdname, 
							 empid, empgpfno, designation, exammarks, aattempt, panno, empaccountno, bankdistrict, gbankname, gbranchname, gifsccode,
							 gmicrcode, attach1, attach2, userid, Govemp_ITGK_Code, dobtype)
								VALUES
							('" . $_LearnerCode . "', '" . $_EmpName . "', '" . $_FatherName . "', '" . $_EmpDistrict . "',
							 '" . $_EmpMobile . "', '" . $_EmpEmail . "', '" . $_EmpAddress . "', '" . $_EmpDeptName . "', '" . $_EmpId . "',
							 '" . $_EmpGPF . "', '" . $_EmpDesignation . "', '" . $_EmpMarks . "', '" . $_EmpExamAttempt . "', '" . $_EmpPan . "',
							 '" . $_EmpBankAccount . "', '" . $_EmpBankDistrict . "','" . $_EmpBankName . "', '" . $_EmpBranchName . "',
							 '" . $_EmpIFSC . "', '" . $_EmpMICR . "', '" . $_fileReceipt1 . "', '" . $_fileBirth1 . "', '" . $_User_Code . "', '" . $_CenterCode . "', '" . $_EmpDobProof ."')";
            $_DuplicateQuery = "Select * From tbl_govempentryform Where learnercode='" . $_LearnerCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Government Reimbursement is Successfully Applied to RKCL";
				$_Mobile=$_EmpMobile;
                                SendSMS($_Mobile, $_SMS);
            }
			else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function Update($_Status_Code,$_StatusName, $_Status_Description) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_status_master set Status_Name='" . $_StatusName . "',"	. "Status_Description='" . $_Status_Description . "' "
							. "Where Status_Code='" . $_Status_Code . "'";
            $_DuplicateQuery = "Select * From tbl_status_master Where Status_Name='" . $_StatusName . "' and "	. "Status_Code <> '" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;        
    }
	
	public function GetAllDETAILS($_action, $_Centercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//$_ITGK=$_SESSION['User_LoginId'];
		    $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_action."' AND Admission_ITGK_Code='" . $_Centercode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
?>