<?php
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
$_ObjConnection = new _Connection();
$_Response = array();
class clsPushSMSRSCFA {
    public function PushMessageNew($arrayroll, $txtMessageDes, $Status, $newfilename)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_InsertQuery = "insert into tbl_rscfanotification (Message_roll,Message_description,Message_file,Message_status,Message_addbyuser)"
                        . "values('".$arrayroll."','".$txtMessageDes."','".$newfilename."','".$Status."','".$_SESSION["User_LoginId"]."')";
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function ReadStatus($id)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
					
                $_chkDuplicate_learnerCode = "Select Id From tbl_rscfamessagereadlog Where Loginid='" . $_SESSION['User_LoginId'] . "' and Userroll='".$_SESSION['User_UserRoll']."' and Messageid='".$id."'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate_learnerCode, Message::SelectStatement);
                if ($_Response2[0] == Message::NoRecordFound) {
                    $_InsertQuery = "insert into tbl_rscfamessagereadlog (Loginid,Userroll,Messageid,Flag)"
                        . "values('".$_SESSION['User_LoginId']."','".$_SESSION['User_UserRoll']."','".$id."','1')";
                   $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement); 
                }
           }else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function ShowMessageList($_Status)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select a.*,b.UserRoll_Name from tbl_rscfanotification as a inner join tbl_userroll_master as b
            on a.Message_roll=b.UserRoll_Code order by Message_datetime desc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function ShowMessageListNotification()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_UserRoll']) && !empty($_SESSION['User_UserRoll'])) {
                $_SelectQuery = "select * from tbl_rscfanotification where Message_roll='".$_SESSION['User_UserRoll']."' and Message_status='1' order by Message_datetime desc";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           }else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
   
    public function Changestatus($messageid,$messagestatus)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$messageid = mysqli_real_escape_string($_ObjConnection->Connect(),$messageid);
					
                $_UpdateQuery = "update tbl_rscfanotification set Message_status='".$messagestatus."',
					Message_addbyuser='".$_SESSION["User_LoginId"]."' Where Message_id='" . $messageid . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function DeleteMessage($messageid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$messageid = mysqli_real_escape_string($_ObjConnection->Connect(),$messageid);
					
                $_DeleteQuery = "delete from tbl_rscfanotification Where Message_id='" . $messageid . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetRoll()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select UserRoll_Code,UserRoll_Name from tbl_userroll_master where UserRoll_Status='1' and UserRoll_Code in (7,14,15,23)";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function CountMessage()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_SelectQuery = "select count(Message_id) as messagecount from tbl_rscfanotification where Message_id not in 
                    (select Messageid from tbl_rscfamessagereadlog where Userroll='".$_SESSION['User_UserRoll']."' and Loginid='".$_SESSION['User_LoginId']."') and Message_roll='".$_SESSION['User_UserRoll']."' and Message_status='1'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function ShowRead($messageid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$messageid = mysqli_real_escape_string($_ObjConnection->Connect(),$messageid);
					
                $_SelectQuery = "select Flag from tbl_rscfamessagereadlog where Userroll='".$_SESSION['User_UserRoll']."' and Loginid='".$_SESSION['User_LoginId']."' and Messageid='".$messageid."'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
