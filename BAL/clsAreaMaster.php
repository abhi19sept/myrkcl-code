<?php



/**
 * Description of clsAreaMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAreaMaster {
    //put your code here
     public function GetAll($tehsil) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$tehsil);
            if(!$tehsil)
            {
            $_SelectQuery = "Select Area_Code,Area_Name,Tehsil_Name,Area_PinCode,District_Name,Region_Name,State_Name,"
                    . "Status_Name,Country_Name From tbl_area_master as a inner join tbl_tehsil_master as b "
                    . "on a.Area_Tehsil=b.Tehsil_Code inner join tbl_district_master as c "
                    . "on b.Tehsil_District=c.District_Code inner join tbl_region_master as d "
                    . "on c.District_Region=d.Region_Code inner join tbl_state_master as e "
                    . "on d.Region_State=e.State_Code inner join tbl_status_master as f "
                    . "on a.Area_Status=f.Status_Code inner join tbl_country_master as g "
                    . "on e.State_Country=g.Country_Code";
            }
            else {
             $_SelectQuery = "Select Area_Code,Area_Name,Tehsil_Name,Area_PinCode,District_Name,Region_Name,State_Name,"
                    . "Status_Name,Country_Name From tbl_area_master as a inner join tbl_tehsil_master as b "
                    . "on a.Area_Tehsil=b.Tehsil_Code inner join tbl_district_master as c "
                    . "on b.Tehsil_District=c.District_Code inner join tbl_region_master as d "
                    . "on c.District_Region=d.Region_Code inner join tbl_state_master as e "
                    . "on d.Region_State=e.State_Code inner join tbl_status_master as f "
                    . "on a.Area_Status=f.Status_Code inner join tbl_country_master as g "
                    . "on e.State_Country=g.Country_Code and a.Area_Tehsil='" . $tehsil . "'";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Area_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Area_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Area_Code);
            $_SelectQuery = "Select Area_Code,Area_Name,Area_Tehsil,Area_Status,Area_PinCode"
                    . " From tbl_area_master Where Area_Code='" . $_Area_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Area_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Area_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Area_Code);
            $_DeleteQuery = "Delete From tbl_area_master Where Area_Code='" . $_Area_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_AreaName,$_Tehsil,$_Status,$_PinCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_AreaName = mysqli_real_escape_string($_ObjConnection->Connect(),$_AreaName);
            $_Tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil);
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            $_PinCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_PinCode);
            $_InsertQuery = "Insert Into tbl_area_master(Area_Code,Area_Name,"
                    . "Area_Tehsil,Area_Status,Area_PinCode) "
                    . "Select Case When Max(Area_Code) Is Null Then 1 Else Max(Area_Code)+1 End as Area_Code,"
                    . "'" . $_AreaName . "' as Area_Name,"
                    . "'" . $_Tehsil . "' as Area_Tehsil,'" . $_Status . "' as Area_Status,'" . $_PinCode . "' as Area_PinCode"
                    . " From tbl_area_master";
            $_DuplicateQuery = "Select * From tbl_area_master Where Area_Name='" . $_AreaName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_AreaName,$_Tehsil,$_Status,$_PinCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            $_AreaName = mysqli_real_escape_string($_ObjConnection->Connect(),$_AreaName);
            $_Tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil);
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
            $_PinCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_PinCode);
            
            $_UpdateQuery = "Update tbl_area_master set Area_Name='" . $_AreaName . "',"
                    . "Area_Tehsil='" . $_Tehsil . "',"
                    . "Area_Status='" . $_Status . "', Area_PinCode='" . $_PinCode . "' Where Area_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_area_master Where Area_Name='" . $_AreaName . "' "
                    . "and Area_Code <> '" . $_Code . "'";
           
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
