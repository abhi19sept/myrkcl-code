<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionCenterWise
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionCenterWise {
    //put your code here
    
    public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
                
                $_SelectQuery2 = "Select Admission_ITGK_Code as Center_Code, count(Admission_LearnerCode) as Admission_Code, 
								sum(case when Admission_Payment_Status = '1' then 1 else 0 end) Confirmed_Learner, 
								sum(case when r.result like '%pass%' then 1 else 0 end) as pass_count 
								from tbl_admission as a left join tbl_result as r on a.Admission_LearnerCode=r.scol_no WHERE
								Admission_Course='".$_course."' and Admission_Batch='".$_batch."' group by Admission_ITGK_Code";    
		
            } elseif ($_SESSION['User_UserRoll'] == 14) {
				
				$_SelectQuery2 = "Select Admission_ITGK_Code as Center_Code, count(Admission_LearnerCode) as Admission_Code, sum(case when Admission_Payment_Status = '1' then 1 else 0 end) Confirmed_Learner from tbl_admission WHERE Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' AND Admission_RspName = '" . $_SESSION['User_Code'] . "' group by Admission_ITGK_Code";    

                /* $_SelectQuery1 = "SELECT DISTINCT a.User_LoginId FROM tbl_user_master AS a INNER JOIN tbl_admission AS b ON a.User_LoginId = b.Admission_ITGK_Code WHERE a.User_Rsp = '" . $_SESSION['User_Code'] . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response1[2])) {

                    $CenterCode2.=$_Row['User_LoginId'] . ",";
                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");
                
		if ($CenterCode3) {
			   
                $_SelectQuery2 = "Select Admission_ITGK_Code as Center_Code, count(Admission_LearnerCode) as Admission_Code, sum(case when Admission_Payment_Status = '1' then 1 else 0 end) Confirmed_Learner from tbl_admission WHERE Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' AND Admission_ITGK_Code IN ($CenterCode3) group by Admission_ITGK_Code";    
		} */
            } elseif ($_SESSION['User_UserRoll'] == 7) {
                
                $CenterCode3 = $_SESSION['User_LoginId']; 
                $_SelectQuery2 = "Select Admission_ITGK_Code as Center_Code, count(Admission_LearnerCode) as Admission_Code, sum(case when Admission_Payment_Status = '1' then 1 else 0 end) Confirmed_Learner from tbl_admission WHERE Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' AND Admission_ITGK_Code IN ($CenterCode3) group by Admission_ITGK_Code";    
		
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                     
            //print_r($_Response);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
