<?php


/**
 * Description of clsHRDetail
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsHRDetail {
    
	 
      public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {

            $_SelectQuery = "Select * from tbl_staff_detail";
            
            } else {
                
            $_SelectQuery = "Select a.*, b.Designation_Name from tbl_staff_detail AS a LEFT JOIN tbl_designation_master AS b
                       ON a.Staff_Designation=b.Designation_Code where Staff_User = '" .$_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    public function Add($_StaffName,$_StaffDesignation,$_StaffGender,$_StaffDob,$_StaffMobile,$_StaffEmail,$_StaffQuali1,$_Experience,$_StaffQuali2,$_StaffQuali3) 
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_StaffMobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_StaffMobile);
            $_StaffEmail = mysqli_real_escape_string($_ObjConnection->Connect(),$_StaffEmail);
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {	
		 $_mobile=$_SESSION['User_MobileNo'];
            $_SMS = "Dear  Applicant, Your HR Details has been submitted to RKCL. Kindly complete entry of all other details for reaching next stage of NCR.";
            	
		   
			
            $_InsertQuery = "Insert Into tbl_staff_detail(Staff_Code,Staff_Designation,Staff_User,Staff_Name,Staff_Dob,Staff_Gender,Staff_Qualification1,Staff_Experience,Staff_Qualification2,Staff_Qualification3,Staff_Mobile,Staff_Email_Id) Select Case When Max(Staff_Code) Is Null Then 1 Else Max(Staff_Code)+1 End as Staff_Code,'" .$_StaffDesignation. "' as Staff_Designation,'" .$_SESSION['User_LoginId']. "' as Staff_User,'" .$_StaffName. "' as Staff_Name,'" . $_StaffDob. "' as Staff_Dob,'" .$_StaffGender. "' as Staff_Gender,'" .$_StaffQuali1. "' as Staff_Qualification1,'" .$_Experience. "' as Staff_Experience,'" .$_StaffQuali2. "' as Staff_Qualification2,'" .$_StaffQuali3. "' as Staff_Qualification3,'" .$_StaffMobile. "' as Staff_Mobile,'" .$_StaffEmail. "' as Staff_Email_Id From tbl_staff_detail";
			
                    
            $_DuplicateQuery = "Select * From tbl_staff_detail Where Staff_Mobile='" . $_StaffMobile . "' OR Staff_Email_Id='". $_StaffEmail ."'";
			
			$_Affilate='2';
				$_InsertQuery1 = "Insert Into tbl_ncrstatus(status) 
												 VALUES ('" .$_Affilate. "')";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
              
				//$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
                                if($_Response[0] == 'Successfully Inserted'){
                                    //$_lastid = $_ObjConnection->last_id();
                                      $StaffCode =mysqli_insert_id();
                                 //$_SelectQuery = "Select * From tbl_staff_detail Where Staff_Code='" . $StaffCode . "'";
                                 //$_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);   
                                 //$_Row = mysqli_fetch_array($_Response1[2]);
                                 $data = "'" .$StaffCode. "','" .$_StaffDesignation. "','".$_SESSION['User_LoginId']."'"
                                         . ",'" .$_StaffName. "','" .$_StaffDob. "','" .$_StaffGender. "'"
                                         . ",'" .$_StaffQuali1. "','" .$_StaffQuali2. "','".$_StaffQuali3."'"
                                         . ",'" .$_StaffMobile. "','" .$_Experience. "','" .$_StaffEmail. "'" ;
                                 $this->PutFacultyData($data);
                                  SendSMS($_mobile, $_SMS);
                                }
                               
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
       //print_r($_Response);
        return $_Response;
    }
	
	
	public function GetDatabyCode($_staff)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_staff = mysqli_real_escape_string($_ObjConnection->Connect(),$_staff);
            $_SelectQuery = "Select * from tbl_staff_detail Where  	Staff_Code= '" . $_staff . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_staff)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_staff = mysqli_real_escape_string($_ObjConnection->Connect(),$_staff);
            $_DeleteQuery = "Delete From tbl_staff_detail Where Staff_Code='" . $_staff . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	 public function Getdesigantion()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * From tbl_designation_master";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	public function Update($_Code,$_StaffName,$_StaffDesignation,$_StaffGender,$_StaffDob,$_StaffMobile,$_StaffEmail,$_StaffQuali1,$_Experience,$_StaffQuali2,$_StaffQuali3) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            $_UpdateQuery = "Update tbl_staff_detail set Staff_Designation='" . $_StaffDesignation . "',"
                    . "Staff_User='" . $_SESSION['User_LoginId'] ."',"
                    . "Staff_Name='" . $_StaffName ."',"
                    . "Staff_Dob='" . $_StaffDob ."',"
                    . "staff_Gender='" . $_StaffGender ."',"
					 . "Staff_Qualification1='" . $_StaffQuali1 ."',"
					  . "Staff_Qualification2='" . $_StaffQuali2 ."',"
					   . "Staff_Qualification3='" . $_StaffQuali3 ."',"
					    . "Staff_Mobile='" . $_StaffMobile ."',"
					   . "Staff_Experience 	='" . $_Experience ."',"
					   
                    . "Staff_Email_Id='" . $_StaffEmail ."'"
                    . " Where Staff_Code='" . $_Code . "'";
            
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                //print_r($_Response);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    function PutFacultyData($data) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data_json = json_encode(["FunctionName" => "PutFacultyData", "RowData" => $data]);
            $curl_request = $this->curlPost("http://49.50.65.36/webservice/allFunctions.php", $data_json);
            if ($curl_request['status'] == '200') {
               // echo $curl_request['success'];
            } else {
                echo "0";
            }
        }
        }
    function curlPost($url, $data_json = NULL) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data_json,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 5c22b1ed-b568-1c45-3d0d-ebff096e4cda"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //print_r($response);
        if ($err) {
            $data = ['status' => '404', 'error' => $err];
        } else {
            $data = ['status' => '200', 'success' => $response];
        }
        return $data;
    }
}
