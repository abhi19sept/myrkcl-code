<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsUploadOwnerChangeAgreement
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUploadOwnerChangeAgreement {
    //put your code here

    
    public function GetDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type 
                            FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b
                            ON a.Organization_Type=b.Org_Type_Code where Org_PayStatus='1' 
                    and Org_Itgk_Code='" . $_SESSION['User_LoginId'] . "' AND a.Org_Application_Approval_Status='Approved'
                    ORDER BY Organization_Code DESC LIMIT 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
    public function Add($PANfilename,$_Ack) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$_Ack = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ack);
				
                $_UpdateQuery = "Update tbl_ownership_change set SPCenter_Agreement='" . $PANfilename . "'"
                    . "Where Org_Ack='" . $_Ack . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Ack) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					
					$_Ack = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ack);
					
                $_UpdateQuery = "Update tbl_ownership_change set Org_Final_Approval_Status='Pending', Org_Agreement_Status='Re-Uploaded'"
                    . "Where Org_Ack='" . $_Ack . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
