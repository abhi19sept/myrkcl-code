<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsGramPanchayatMaster
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGramPanchayatMaster {
    //put your code here
    
     public function GetAll($country, $panchayatId = 0) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
       //echo $country;
        $country = mysqli_real_escape_string($_ObjConnection->Connect(),$country);
        $panchayatId = mysqli_real_escape_string($_ObjConnection->Connect(),$panchayatId);
        $filter = ($panchayatId) ? " AND GP_Block = '" . $panchayatId . "'" : '';
        try {
            
            if ($country && is_array($country)) {
                $filter .= '';
                foreach($country as $field => $val) {
                    $filter .= ' AND ' . $field . ' ' . $val;
                }
                $_SelectQuery = "Select GP_Code,GP_Name,GP_Block,Block_Name,"
                    . "Status_Name From tbl_gram_panchayat as a inner join tbl_panchayat_samiti as b "
                    . "on a.GP_Block=b.Block_Code inner join tbl_status_master as c "
                    . "on a.GP_Status=c.Status_Code " . $filter . " $filter ORDER BY GP_Name";
            } elseif ($country) {
                $_SelectQuery = "Select GP_Code,GP_Name,GP_Block,Block_Name,"
                    . "Status_Name From tbl_gram_panchayat as a inner join tbl_panchayat_samiti as b "
                    . "on a.GP_Block=b.Block_Code inner join tbl_status_master as c "
                    . "on a.GP_Status=c.Status_Code and a.GP_Block='" . $country . "' $filter ORDER BY GP_Name";
            } else {
                $_SelectQuery = "Select GP_Code,GP_Name,GP_Block,Block_Name,"
                    . "Status_Name From tbl_gram_panchayat as a inner join tbl_panchayat_samiti as b "
                    . "on a.GP_Block=b.Block_Code inner join tbl_status_master as c "
                    . "on a.GP_Status=c.Status_Code $filter ORDER BY GP_Name";
            }
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_GP_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_GP_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_GP_Code);
            $_SelectQuery = "Select GP_Code,GP_Name,GP_Block,GP_Status"
                    . " From tbl_gram_panchayat Where GP_Code='" . $_GP_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_GP_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_GP_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_GP_Code);
            $_DeleteQuery = "Delete From tbl_gram_panchayat Where GP_Code='" . $_GP_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_GPName,$_GPParent,$_GPStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_GPName = mysqli_real_escape_string($_ObjConnection->Connect(),$_GPName);
            $_InsertQuery = "Insert Into tbl_gram_panchayat(GP_Code,GP_Name,"
                    . "GP_Block,GP_Status) "
                    . "Select Case When Max(GP_Code) Is Null Then 1 Else Max(GP_Code)+1 End as GP_Code,"
                    . "'" . $_GPName . "' as GP_Name,"
                    . "'" . $_GPParent . "' as GP_Block,'" . $_GPStatus . "' as GP_Status"
                    . " From tbl_gram_panchayat";
            $_DuplicateQuery = "Select * From tbl_gram_panchayat Where GP_Name='" . $_GPName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_GPCode,$_GPName,$_GPParent,$_GPStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_GPName = mysqli_real_escape_string($_ObjConnection->Connect(),$_GPName);
            $_GPCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_GPCode);
            $_UpdateQuery = "Update tbl_gram_panchayat set GP_Name='" . $_GPName . "',"
                    . "GP_Block='" . $_GPParent . "',"
                    . "GP_Status='" . $_GPStatus . "' Where GP_Code='" . $_GPCode . "'";
            $_DuplicateQuery = "Select * From tbl_gram_panchayat Where GP_Name='" . $_GPName . "' "
                    . "and GP_Code <> '" . $_GPCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
