<?php
require_once 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clschangepassword {

    public function updatePassword($password) {
    	global $_ObjConnection;
    	$_ObjConnection->Connect();

    	$_UpdateQuery = "UPDATE `tbl_user_master` SET `User_Password`='". $password ."', encrypted = 0 WHERE `User_LoginId` = '". $_SESSION['User_LoginId'] . "'";
		return $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
    }

    public function VerifyToChangePassword($_old, $_new, $_confirm) {
        global $_ObjConnection, $general;
        $_ObjConnection->Connect();
        try {
				$_DuplicateQuery = "SELECT * FROM `tbl_user_master` WHERE `User_LoginId` = '" . $_SESSION['User_LoginId'] . "' LIMIT 1";
				$_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
					if ($_Response[0] == Message::SuccessfullyFetch) {
						$oldData = mysqli_fetch_array($_Response[2]);
						$oldPass = ($oldData['encrypted']) ? $general->decrypt_with_key($oldData['User_Password']) : $oldData['User_Password'];
						if ($oldPass === $_old && $oldPass != $_new) {
							$_Response[0] = '1';
							$_Response[2] = $oldData;
						} else {
							$_Response[0] = '0';
						}
					}
					
					//$result = $_ObjConnection->query( $query );				
         } catch (Exception $_e) {
            $_Response[0] = '0';
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    /* 30-10-2018: */
    
    public function VerifyToChangePasswordNEW($_old_password) {
        global $_ObjConnection, $general;
        $_ObjConnection->Connect();
        try {
            function encryptPasswordFinal2($password, $hashkey) {
                    return md5(sha1($password . $hashkey));
            } 
//            $_SelectQuery = "Select a.*, b.*, c.*,d.*,e.*
//                            FROM tbl_user_master as a 
//                            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
//                            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
//                            INNER JOIN tbl_into_s as d on a.User_Code=d.User_Code
//                            LEFT JOIN tbl_phrase_ke as e on d.Phrase_Ke=e.Phrase_ID
//                            WHERE d.User_LoginId = '" . $_SESSION['User_LoginId'] . "'";
            
            $_SelectQuery = "Select a.User_Code,a.User_MobileNo,a.User_LoginId,
                            d.Open_Sesame,d.Open_Sesame_En,d.Phrase_Ke,d.Open_Sesame_Update_Date,
                            e.Phrase_Ke
                            FROM tbl_user_master as a 
                            INNER JOIN tbl_into_s as d on a.User_Code=d.User_Code
                            LEFT JOIN tbl_phrase_ke as e on d.Phrase_Ke=e.Phrase_ID
                            WHERE d.User_LoginId = '" . $_SESSION['User_LoginId'] . "'";
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
            if (mysqli_num_rows($_Response[2])) {
                $_Row = mysqli_fetch_array($_Response[2],true);
                
            
                $txtPassword = encryptPasswordFinal2($_old_password, $_Row["Phrase_Ke"]); //echo "<br>";
                $hash = $_Row["Open_Sesame_En"];
                if (password_verify($txtPassword, $hash)) {
                        //$_Response[2] = $_Row;
                        $_Response[2] = "MATCHED";
                        
                } else {
                        $_Response[2] = "NOTMATCHED";    
                }
                //echo "<pre>"; print_r($_Response);die;
            }
					
					//$result = $_ObjConnection->query( $query );				
         } catch (Exception $_e) {
            $_Response[0] = '0';
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    /* Added By : Sunil Kuamr Baindara 30-10-2018*/
    
    public function updateUserPasswordNew($pass) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        
            $User_LoginId=$_SESSION['User_LoginId'];
            
            function encryptPasswordFinal($password, $hashkey) {
                    return md5(sha1($password . $hashkey));
            } 
      
            $_SelectQuery = "select a.*,b.* from tbl_into_s a
            LEFT JOIN tbl_phrase_ke B ON a.Phrase_Ke = b.Phrase_ID where a.User_LoginId = '" . $User_LoginId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //echo "<pre>"; print_r($_Response);die;
            if($_Response[0]=='Success')
                {                
                $_row = mysqli_fetch_array($_Response[2]);            
                $Phrase_Ke=$_row['Phrase_Ke'];            
                $Open_Sesame = encryptPasswordFinal($pass, $Phrase_Ke);
                $Open_Sesame_En = password_hash($Open_Sesame, PASSWORD_BCRYPT); 
                
                /* Checking the password should not match with last 3 password*/
                $_SelectQuery2 = "Select * from tbl_into_s_log where Open_Sesame='".$Open_Sesame."' and User_LoginId = '" . $User_LoginId . "' order by In_ID desc limit 3";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                $_row2 = mysqli_num_rows($_Response2[2]); 
                /* Checking the password should not match with last 3 password*/
                if($_row2 > 0){
                    $responceNew[0]='MORE3';
                    return $responceNew;
                }
                else{
                    $_SelectQuery1 = "Select * FROM tbl_phrase_ke WHERE Phrase_Status = 1";
                    $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                    $_row1 = mysqli_fetch_array($_Response1[2]);            
                    $Phrase_ID=$_row1['Phrase_ID'];            
                    $Phrase_Ke=$_row1['Phrase_Ke'];
                
                    $_UpdateQuery = "UPDATE tbl_into_s SET Open_Sesame = '" . $Open_Sesame . "', Open_Sesame_En = '" . $Open_Sesame_En . "', Phrase_Ke = '" . $Phrase_ID . "' WHERE User_LoginId = '" . $User_LoginId . "'";            
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement); 
                    
                    $_UpdateQuery2 = "UPDATE tbl_ps SET PS_Status = '0' WHERE User_LoginId = '" . $User_LoginId . "'";            
                    $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
                    
                    $_InsertQuery3 = "INSERT INTO `tbl_ps`(`User_LoginId`, `PS`, `PS_Status`) 
                                 VALUES ('" . $User_LoginId . "', '" . $pass . "',  '1')";
                    $_ObjConnection->ExecuteQuery($_InsertQuery3, Message::InsertStatement);

                    $_InsertQuery2 = "INSERT INTO `tbl_into_s_log`(`User_Code`, `User_LoginId`, `Open_Sesame`, `Open_Sesame_En`, `Phrase_Ke`) 
                                     VALUES ('" . $_row['User_Code'] . "', '" . $User_LoginId . "', '" . $Open_Sesame . "',  '" . $Open_Sesame_En . "', '" . $Phrase_ID . "')";
                    return $_ObjConnection->ExecuteQuery($_InsertQuery2, Message::InsertStatement); 
                }
                
            }
            else{
                    
               $responceNew[0]='0';
               return $responceNew; 
            }
            
            
        
    }
}
?>