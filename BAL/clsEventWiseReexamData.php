<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsEventWiseReexamData.php
 *
 * @author Yogendra soni
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsEventWiseReexamData 
{
    //put your code here  
   public function GetAll($event) 
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
				$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
				
		    $_SelectQuery ="select examdata_code, examid, learnercode, upper(learnername) as learnername,
							upper(fathername) as fathername, dob, Admission_Mobile, b.Course_Name, c.Batch_Name, itgkcode,
							itgkname, d.District_Name, e.Tehsil_Name, remark, examdate, learnertype
							from examdata as a inner join tbl_course_master as b on a.coursename=b.Course_Code
							inner join tbl_batch_master as c on a.batchname=c.Batch_Code
							inner join tbl_district_master as d on a.itgkdistrict=d.District_Code
							inner join tbl_tehsil_master as e on a.itgktehsil=e.Tehsil_Code
							inner join tbl_admission as ad on a.learnercode=ad.Admission_LearnerCode
							where examid='".$event."' and paymentstatus='1' and ad.Admission_Payment_Status=1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}