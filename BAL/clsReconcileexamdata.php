<?php

/*
 * @author Mayank

 */

 ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsReconcileexamdata {
    //put your code here

	public function ReconcileReexamdata($txnid) {		
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_SelectQuery = "Select * From tbl_payment_transaction Where Pay_Tran_ProdInfo='ReexamPayment' AND Pay_Tran_PG_Trnid='" . $txnid . "'";
				$_SelectResponse = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			
				
					$_Row = mysqli_fetch_array($_SelectResponse[2]);					   
					$Admission_code = $_Row['Pay_Tran_AdmissionArray'];
					$_Txnid = $_Row['Pay_Tran_PG_Trnid'];					
					$udf2 = $_Row['Pay_Tran_RKCL_Trnid'];					
					$udf1 = $_Row['Pay_Tran_ITGK'];
					
					$_SelectQuery1 = "Select paymentstatus FROM examdata Where itgkcode='" . $udf1 . "' AND paymentstatus = '1' AND examdata_code IN ($Admission_code)";					
					$_SelectResponse1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);				
					//$num_rows = mysqli_num_rows($_SelectResponse1[2]);
					
					if($_SelectResponse1[0] == 'Success') {
						echo "Learner already confirmed with this ".$txnid." Id.";
						return; 
					}
					else {
						$_UpdateReexam = "Update examdata set paymentstatus = '1', reexam_TranRefNo='" . $txnid . "', reexam_RKCL_Trnid='" . $udf2 . "' Where itgkcode = '" . $udf1 . "'  AND examdata_code IN ($Admission_code)";
						$_Response3 = $_ObjConnection->ExecuteQuery($_UpdateReexam, Message::UpdateStatement);
					}
					
					
							
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response3;        
    }
	
}