<?php
/**
 * Description of clscountrymaster
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCountryMaster {
    //put your code here
     public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Country_Code,Country_Name,"
                    . "Status_Name From tbl_country_master as a inner join tbl_status_master as b "
                    . "on a.Country_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Country_Code)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Country_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Country_Code);
				
            $_SelectQuery = "Select Country_Code,Country_Name,Country_Status"
                    . " From tbl_country_master Where Country_Code='" . $_Country_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Country_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Country_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Country_Code);
				
            $_DeleteQuery = "Delete From tbl_country_master Where Country_Code='" . $_Country_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_CountryName,$_CountryStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CountryName = mysqli_real_escape_string($_ObjConnection->Connect(),$_CountryName);
				$_CountryStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_CountryStatus);
				
            $_InsertQuery = "Insert Into tbl_country_master(Country_Code,Country_Name,"
                    . "Country_Status) "
                    . "Select Case When Max(Country_Code) Is Null Then 1 Else Max(Country_Code)+1 End as Country_Code,"
                    . "'" . $_CountryName . "' as Country_Name,"
                    . "'" . $_CountryStatus . "' as Country_Status"
                    . " From tbl_country_master";
            $_DuplicateQuery = "Select * From tbl_country_master Where Country_Name='" . $_CountryName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_CountryCode,$_CountryName,$_CountryStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CountryCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CountryCode);
				$_CountryName = mysqli_real_escape_string($_ObjConnection->Connect(),$_CountryName);
				
            $_UpdateQuery = "Update tbl_country_master set Country_Name='" . $_CountryName . "',"
                    . "Country_Status='" . $_CountryStatus . "' Where Country_Code='" . $_CountryCode . "'";
            $_DuplicateQuery = "Select * From tbl_country_master Where Country_Name='" . $_CountryName . "' "
                    . "and Country_Code <> '" . $_CountryCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
