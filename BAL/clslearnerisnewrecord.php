<?php
require 'DAL/classconnection.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsLearnerIsnewrecord {
	
    public function UpdateINewRecord($Learner_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$Learner_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$Learner_Code);
				
				$_UpdateQuery = "update tbl_admission set IsNewRecord='Y' where Admission_LearnerCode = '".$Learner_Code."'
									and Admission_Payment_Status='1'";
				$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
