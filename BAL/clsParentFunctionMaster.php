<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsParentFunctionMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsParentFunctionMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Parent_Function_Code,Parent_Function_Name,"
                    . "Status_Name,Parent_Function_Display,Root_Menu_Name From tbl_parent_function_master as a inner join tbl_status_master as b "
                    . "on a.Parent_Function_Status"
                    . "=b.Status_Code Inner Join tbl_root_menu as c on a.Parent_Function_Root=C.Root_Menu_Code Order By Parent_Function_Display";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetDatabyRoot($_RootCode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootCode);
				
            $_SelectQuery = "Select Parent_Function_Code,Parent_Function_Name From tbl_parent_function_master Where Parent_Function_Root='" . $_RootCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    public function GetDatabyCode($_Parent_Function_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Parent_Function_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent_Function_Code);
				
            $_SelectQuery = "Select Parent_Function_Code,Parent_Function_Name,"
                    . "Parent_Function_Status,Parent_Function_Display,Parent_Function_Root From tbl_parent_function_master Where Parent_Function_Code='" . $_Parent_Function_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Parent_Function_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Parent_Function_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent_Function_Code);
				
            $_DeleteQuery = "Delete From tbl_parent_function_master Where Parent_Function_Code='" . $_Parent_Function_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ParentFunctionName,$_ParentFunctionStatus,$_ParentFunctionDisplay,$_ParentFunctionRoot) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ParentFunctionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentFunctionName);
				
            $_InsertQuery = "Insert Into tbl_parent_function_master(Parent_Function_Code,Parent_Function_Name,"
                    . "Parent_Function_Status,Parent_Function_Display,Parent_Function_Root) "
                    . "Select Case When Max(Parent_Function_Code) Is Null Then 1 Else Max(Parent_Function_Code)+1 End as Parent_Function_Code,"
                    . "'" . $_ParentFunctionName . "' as Parent_Function_Name,'" . $_ParentFunctionStatus . "' as Parent_Function_Status,"
                    . "'" . $_ParentFunctionDisplay . "' as Parent_Function_Display,'" . $_ParentFunctionRoot . "' as Parent_Function_Root "
                    . " From tbl_parent_function_master";
            $_DuplicateQuery = "Select * From tbl_parent_function_master Where Parent_Function_Name='" . $_ParentFunctionName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_ParentFunctionCode,$_ParentFunctionName,$_FunctionStatus,$_ParentFunctionDisplay,$_ParentFunctionRoot) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ParentFunctionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentFunctionCode);
				$_ParentFunctionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentFunctionName);
				
            $_UpdateQuery = "Update tbl_parent_function_master set Parent_Function_Name='" . $_ParentFunctionName . "',"
                    . "Parent_Function_Status='" . $_FunctionStatus . "',Parent_Function_Display='" . $_ParentFunctionDisplay . "',Parent_Function_Root='" . $_ParentFunctionRoot . "' "
                    . "Where Parent_Function_Code='" . $_ParentFunctionCode . "'";
            $_DuplicateQuery = "Select * From tbl_parent_function_master Where Parent_Function_Name='" . $_ParentFunctionName . "' "
                    . "and Parent_Function_Code <> '" . $_ParentFunctionCode . "'";
           // $_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
