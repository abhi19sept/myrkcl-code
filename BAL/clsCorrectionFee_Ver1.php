<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
 
class clsCorrectionFee {
    //put your code here
    public function GetAll($_application, $_correctioncode, $paymode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code=$_SESSION['User_LoginId'];
			
			$_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_CorrectionEvent = '" . $_correctioncode . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '8' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
			$_ResponseGetEvent1=$_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
			$_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
			if($_getEvent['Event_Payment'] == '1')
			{
				$_SelectQuery = "Select * FROM  `tbl_correction_copy` WHERE Correction_Payment_Status = '0' AND applicationfor = '" . $_application . "' AND Correction_ITGK_Code='" . $_ITGK_Code . "'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if($_getEvent['Event_Payment'] == '2')
			{
				$_SelectQuery = "Select * FROM  `tbl_correction_copy` WHERE Correction_Payment_Status = '0' AND applicationfor = '" . $_application . "' AND Correction_ITGK_Code='" . $_ITGK_Code . "'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else
			{
				//echo "Invalid User Input";
                    return;
			}
	   } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	
	public function GetCorrectionName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select DISTINCT a.applicationfor, a.docid FROM tbl_cdoccategory_master AS a INNER JOIN tbl_correction_copy AS b ON a.applicationfor = b.applicationfor WHERE a.active = 'Y'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetCorrectionFee($_doccode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select RKCL_Share FROM tbl_cdoccategory_master WHERE docid = '" . $_doccode . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetApplicationName($_application)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select applicationfor FROM tbl_cdoccategory_master WHERE docid = '" . $_application . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	 public function Update($_Code, $_PayTypeCode, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_correction_ddpay (correction_ddpay_code, dd_Transaction_Status, dd_Transaction_Name, dd_amount,"
                    . "dd_Transaction_Txtid, dd_no, dd_date,"
                    . "dd_centercode,dd_bank,dd_branch,dd_Transaction_Email) "
                    . "Select Case When Max(correction_ddpay_code) Is Null Then 1 Else Max(correction_ddpay_code)+1 End as correction_ddpay_code,"
                    . "'Intermediate' as dd_Transaction_Status,'" . $_firstname . "' as dd_Transaction_Name,'" . $_amount . "' as dd_amount,"
                    . "'" . $_TranRefNo . "' as dd_Transaction_Txtid,"
                    . "'" . $_ddno . "' as dd_no,'" . $_dddate . "' as dd_date,'" . $_Code . "' as dd_centercode,'" . $_ddlBankName . "' as dd_bank,'" . $_txtBranchName . "' as dd_branch,'" . $_email . "' as dd_Transaction_Email"
                    . " From tbl_correction_ddpay";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

			$_InsertQuery1 = "INSERT INTO tbl_correction_transaction (Correction_Transaction_Code, Correction_Transaction_Status, Correction_Transaction_Fname, Correction_Transaction_Amount,"
								. "Correction_Transaction_Txtid, Correction_Payment_Mode, Correction_Transaction_ProdInfo, Correction_Transaction_Email,"
								. "Correction_Transaction_CenterCode) "
								. "Select Case When Max(Correction_Transaction_Code) Is Null Then 1 Else Max(Correction_Transaction_Code)+1 End as Correction_Transaction_Code,"
								. "'Intermediate' as Correction_Transaction_Status,'" .$_firstname. "' as Correction_Transaction_Fname,'" .$_amount. "' as Correction_Transaction_Amount,"
								. "'" .$_TranRefNo. "' as Correction_Transaction_Txtid, 'DD Mode' as Correction_Payment_Mode,"
								. "'" .$_PayTypeCode. "' as Correction_Transaction_ProdInfo,'" .$_email. "' as Correction_Transaction_Email,'" .$_Code. "' as Correction_Transaction_CenterCode"
								. " From tbl_correction_transaction";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);

            $_UpdateQuery = "Update tbl_correction_copy set Correction_Payment_Status = 8, Correction_TranRefNo='" . $_TranRefNo . "' "
                    . "Where Correction_ITGK_Code='" . $_Code . "' AND lcode IN (" . $_SESSION['AdmissionArray'] . ")";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetCenterCode($_code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select DISTINCT Correction_ITGK_Code, Correction_TranRefNo FROM tbl_correction_copy WHERE applicationfor = '" . $_code . "' AND Correction_Payment_Status = '8' GROUP BY Correction_ITGK_Code";
//echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GETCorrectionData($_refno)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select * FROM tbl_correction_ddpay WHERE dd_Transaction_Txtid = '" . $_refno . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function UpdateDDPaymentStatus($Transactionid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
			//$Transactionid = $_SESSION['LearnerCorrectionCodes'];
			
          $_UpdateQuery = "Update tbl_correction_ddpay set dd_Transaction_Status = 'Success'"
                      . " Where dd_Transaction_Txtid IN ($Transactionid) "; 
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		  
		  $_UpdateQuery1 = "Update tbl_correction_transaction set Correction_Transaction_Status = 'Success'"
                      . " Where Correction_Transaction_Txtid IN ($Transactionid) "; 
              $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
			  
		  $_UpdateQuery2 = "Update tbl_correction_copy set Correction_Payment_Status = '1'"
                      . " Where Correction_TranRefNo IN ($Transactionid) "; 
              $_Response2=$_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	 public function AddCorrectionPayTran($_txtapplicationfor, $amount1, $_AdmissionCode, $txtGenerateId, $Lcount, $txtGeneratePayUId) {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_RKCL_Id = $txtGenerateId . '_RKCLtranid';

            $_ITGK_Code = $_SESSION['User_LoginId'];
			
			
			
            //$_Fname = $_SESSION['User_Code'];
				$_SelectQuery = "Select * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "'";            
				$_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if($_Response1[0]!='Success'){
					
					   $_InsertQuery = "INSERT INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray,Pay_Tran_LCount,"
								. "Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_PG_Trnid) "
								. "values('" . $_ITGK_Code . "' ,'" . $_RKCL_Id . "','" . $_AdmissionCode . "','" . $Lcount . "','" . $amount1 . "','PaymentInProcess','" . $_txtapplicationfor . "','" . $txtGeneratePayUId . "')";

						$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

						$_UpdateQuery = "Update tbl_correction_copy set Correction_RKCL_Trnid = '" . $_RKCL_Id . "', Correction_TranRefNo='" . $txtGeneratePayUId . "' Where cid IN ($_AdmissionCode)";
						$_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				} else {
					$_UpdateQuery = "UPDATE tbl_payment_transaction SET 
						Pay_Tran_ITGK = '" . $_ITGK_Code . "', 
						Pay_Tran_RKCL_Trnid = '" . $_RKCL_Id . "', 
						Pay_Tran_AdmissionArray = '" . $_AdmissionCode . "',
						Pay_Tran_LCount = '" . $Lcount . "', 
						Pay_Tran_Amount = '" . $amount1 . "', 
						Pay_Tran_ProdInfo = '" . $_txtapplicationfor . "'
					WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "' AND Pay_Tran_Status = 'PaymentInProcess'";
					$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				}
			 } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
	
 public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
