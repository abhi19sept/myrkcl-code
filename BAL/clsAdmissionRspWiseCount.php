<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionRspWiseCount
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionRspWiseCount {
    //put your code here
    public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11'){
			
                        $_SelectQuery =	"Select User_LoginID as User_LoginID, Organization_Name, count(Admission_LearnerCode) as Admission_LearnerCode, "
                                        . " sum(case when Admission_Payment_Status = '1' then 1 else 0 end) Confirmed_Learner "
                                        . " from tbl_admission as a inner join tbl_user_master as b "
                                        . " ON a.Admission_RspName = b.User_Code inner join tbl_organization_detail as c "
                                        . " ON b.User_Code=c.Organization_User where Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' group by User_LoginID";
                        
			}
			 $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
