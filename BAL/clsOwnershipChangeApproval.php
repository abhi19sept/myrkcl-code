<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOwnershipChangeApproval
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/upload_ftp_doc.php';


$_ObjFTPConnection = new ftpConnection();

$_ObjConnection = new _Connection();
$_Response = array();

class clsOwnershipChangeApproval {
    //put your code here
    
//    public function GetITGK_Details($CenterCode) {
//        global $_ObjConnection;
//        $_ObjConnection->Connect();
//        try {
//                
//            echo    $_SelectQuery = "Select DISTINCT a.*, c.Organization_Name as ITGK_Name, c.*, d.District_Name,
//                                e.Tehsil_Name,f.User_LoginId as RSP_Code,
//                                g.Organization_Name as RSP_Name, 
//                                j.Municipality_Type_Name, k.Municipality_Name, l.Ward_Name, 
//                                m.Block_Name, n.GP_Name, o.Village_Name, q.Org_Type_Name From tbl_user_master as a 
//                                LEFT join tbl_organization_detail as c on a.User_Code=c.Organization_User
//                                LEFT join tbl_org_type_master as q on c.Organization_Type=q.Org_Type_Code
//                                LEFT join tbl_district_master as d on c.Organization_District=d.District_Code
//                                LEFT join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
//                                LEFT join tbl_municipality_type as j on c.Organization_Municipal_Type=j.Municipality_Type_Code
//                                LEFT join tbl_municipality_master as k on c.Organization_Municipal=k.Municipality_Code
//                                LEFT join tbl_ward_master as l on c.Organization_WardNo=l.Ward_Code
//                                LEFT join tbl_panchayat_samiti as m on c.Organization_Panchayat=m.Block_Code
//                                LEFT join tbl_gram_panchayat as n on c.Organization_Gram=n.GP_Code
//                                LEFT join tbl_village_master as o on c.Organization_Village=o.Village_Code
//                                LEFT join tbl_user_master as f on a.User_Rsp=f.User_Code
//                                LEFT join tbl_organization_detail as g on f.User_Code=g.Organization_User
//                                LEFT JOIN tbl_courseitgk_mapping as h on h.Courseitgk_ITGK=a.User_LoginId
//                                WHERE a.User_UserRoll = '7' and a.User_LoginId='" . $CenterCode . "'
//                                and CURDATE() < h.CourseITGK_ExpireDate and h.Courseitgk_Course='RS-CIT'";
//            //die;
//                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
//        } catch (Exception $_ex) {
//            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
//            $_Response[1] = Message::Error;
//        }
//          return $_Response;
//    }
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b
                            ON a.Organization_Type=b.Org_Type_Code where Org_PayStatus='1'";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code 
                            INNER JOIN tbl_user_master as c on a.Org_Itgk_Code = c.User_LoginId 
                            where Org_PayStatus='1' and c.User_Rsp= '" . $_SESSION['User_Code'] . "'";
            }
            else{
            $_SelectQuery = "";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function GetOrgDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b
                            ON a.Organization_Type=b.Org_Type_Code where Org_PayStatus='1' and Organization_Code='" . $_CenterCode . "'";
        
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ApproveOwnershipChange($_Code,$_Mobile,$_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
					$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
            
            $_UpdateQuery = "Update tbl_ownership_change set Org_Application_Approval_Status='Approved', Org_Application_Approval_Date='" . $_Date . "'"
                    . " Where Organization_Code='" . $_Code . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            $_SMSITGK = "Dear ITGK, Your request for Ownership Change has been APPROVED on MYRKCL."
                    . "Kindly upload SP-ITGK Agreement for final approval from RKCL.";
            
            $_SMSRSP = "Dear Service Provider, Ownership Change request for Center Code" .$_CenterCode. "has been APPROVED on MYRKCL."
                    . "Kindly update NEW Agreement from MYRKCL for final APPROVAL";
            
            $_SelectQuery = "Select * from tbl_user_master Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response2=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response2[2]);
            $_Mobileold = $_Row['User_MobileNo'];
            $_Rsp_Code = $_Row['User_Rsp'];
            
            $_SelectQuery1 = "Select * from tbl_user_master Where User_Code='" . $_Rsp_Code . "'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_RSPMobile = $_Row1['User_MobileNo'];
            
            SendSMS($_Mobile, $_SMSITGK);
            SendSMS($_Mobileold, $_SMSITGK);
            SendSMS($_RSPMobile, $_SMSRSP);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	   public function RejectOwnershipChange($_Code,$_Mobile,$_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
					$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
            
            $_UpdateQuery = "Update tbl_ownership_change set Org_Application_Approval_Status='Rejected', Org_Application_Approval_Date='" . $_Date . "'"
                    . " Where Organization_Code='" . $_Code . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            $_SMSITGK = "Dear ITGK, Your request for Ownership Change has been REJECTED on MYRKCL.";
            
            $_SMSRSP = "Dear Service Provider, Ownership Change request for Center Code" .$_CenterCode. "has been REJECTED on MYRKCL.";
            
            $_SelectQuery = "Select * from tbl_user_master Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response2=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response2[2]);
            $_Mobileold = $_Row['User_MobileNo'];
            $_Rsp_Code = $_Row['User_Rsp'];
            
            $_SelectQuery1 = "Select * from tbl_user_master Where User_Code='" . $_Rsp_Code . "'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_RSPMobile = $_Row1['User_MobileNo'];
            
            SendSMS($_Mobile, $_SMSITGK);
            SendSMS($_Mobileold, $_SMSITGK);
            SendSMS($_RSPMobile, $_SMSRSP);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
    
    public function SendSMS($_SMS, $_CenterCode, $_MobileNew) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_MobileNew = mysqli_real_escape_string($_ObjConnection->Connect(),$_MobileNew);
					$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
					
            $_UpdateQuery = "Update tbl_ownership_change set Org_Message='" . $_SMS . "', Org_Application_Approval_Status='OnHold'"
                    . "Where Org_Itgk_Code='" . $_CenterCode . "' AND Org_Application_Approval_Status='Pending'";
            
            $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);   
                
            $_SelectQuery = "Select * from tbl_user_master Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response[2]);
            $_Mobile = $_Row['User_MobileNo'];
            $_Rsp_Code = $_Row['User_Rsp'];
            
            $_SelectQuery1 = "Select * from tbl_user_master Where User_Code='" . $_Rsp_Code . "'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_RSPMobile = $_Row1['User_MobileNo'];
            SendSMS($_Mobile,$_SMS);
            SendSMS($_MobileNew,$_SMS);
            SendSMS($_RSPMobile,$_SMS);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function SHOWPAYENTDETAILS($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
                $_SelectQuery = "Select * from tbl_payment_transaction where Pay_Tran_ITGK ='" . $_CenterCode . "'"
                        . " AND Pay_Tran_Status = 'PaymentReceive' AND Pay_Tran_ProdInfo = 'OwnershipFeePayment'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetNcrPhotoData($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
                $_SelectQuery = "Select * from tbl_ncrvisit_photo where NCRVisit_CenterCode ='" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetSPCenteragreement($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
                $_SelectQuery = "Select * from tbl_spcenter_agreement where SPCenter_CenterCode ='" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetRspDetails($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
          $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_SelectQuery = "Select * from tbl_rspitgk_mapping WHERE Rspitgk_ItgkCode = '" . $_CenterCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
