<?php


    /**
     * Description of clsOrgDetail
     * @author yogi
     */

    require 'DAL/classconnectionNEW.php';
    require 'DAL/sendsms.php';
    require 'DAL/smtp_class.php';

    $_ObjConnection = new _Connection();
    $_Response = array();

    class clslearnerdetails
    {

        public function GetAll($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_LoginRole = $_SESSION['User_UserRoll'];
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
                if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30' || $_LoginRole == '28') {
                    $_SelectQuery = "SELECT * FROM vw_learnerdetails WHERE Admission_LearnerCode='" . $_actionvalue . "' ";

                } else {
                    $_SelectQuery = "SELECT * FROM vw_learnerdetails WHERE Admission_LearnerCode='" . $_actionvalue . "' AND Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "'";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	 public function checkGovtLearnerStatus($_LearnerCode,$_ITGK) {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
				
                $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_LearnerCode."' AND Govemp_ITGK_Code='" . $_ITGK . "' ";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
             return $_Response;
        }
	
	
	
	
	
	public function GetAllMarks($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			
			$_LoginRole = $_SESSION['User_UserRoll'];
         if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30' || $_LoginRole == '28')
		 {
			 $_SelectQuery = "Select * from vw_learnerdetails where Admission_LearnerCode='".$_actionvalue."' ";
			 
		 }
		 else
		 {
		$_SelectQuery = "Select * from vw_learnerdetails where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
		
		}
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        /*for learner 7-4-2017*/
        public function GetAllLearner($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
                $_LoginRole = $_SESSION['User_UserRoll'];
                if ($_LoginRole == '21' || $_LoginRole == '18' || $_LoginRole == '19' || $_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30' || $_LoginRole == '28') {
                    //$_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' ";

                    $_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
                                                    FROM tbl_admission a
                                                    LEFT JOIN tbl_course_master b ON a.Admission_Course= b.Course_Code 
                                                    LEFT JOIN tbl_batch_master c ON a.Admission_Batch = c.Batch_Code
                                                    LEFT JOIN tbl_result d ON a.Admission_LearnerCode = d.scol_no
                                                    WHERE a.Admission_LearnerCode='" . $_actionvalue . "'";

                } else if ($_LoginRole == '14') {
                    //echo $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";

               /**     $_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
                                                    FROM tbl_admission a
                                                    LEFT JOIN tbl_course_master b ON a.Admission_Course= b.Course_Code 
                                                    LEFT JOIN tbl_batch_master c ON a.Admission_Batch = c.Batch_Code
                                                    LEFT JOIN tbl_result d ON a.Admission_LearnerCode = d.scol_no
                                                    WHERE a.Admission_LearnerCode='" . $_actionvalue . "' AND a.Admission_RspName='" . $_SESSION['User_Code'] . "'"; **/
													
													
													$_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
                                                    FROM tbl_admission a
                                                    LEFT JOIN tbl_course_master b ON a.Admission_Course= b.Course_Code 
                                                    LEFT JOIN tbl_batch_master c ON a.Admission_Batch = c.Batch_Code
                                                    LEFT JOIN tbl_result d ON a.Admission_LearnerCode = d.scol_no 
                                                    LEFT JOIN tbl_user_master as u on a.Admission_ITGK_Code=u.User_LoginId 
                                                    WHERE a.Admission_LearnerCode='" . $_actionvalue . "' AND u.User_Rsp='" . $_SESSION['User_Code'] . "'";
													
                } else {
                    //echo $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";

                    $_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
                                                    FROM tbl_admission a
                                                    LEFT JOIN tbl_course_master b ON a.Admission_Course= b.Course_Code 
                                                    LEFT JOIN tbl_batch_master c ON a.Admission_Batch = c.Batch_Code
                                                    LEFT JOIN tbl_result d ON a.Admission_LearnerCode = d.scol_no
                                                    WHERE a.Admission_LearnerCode='" . $_actionvalue . "' AND a.Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "'";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        /*for learner 7-4-2017*/

        public function UserDetails($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
					
                $_SelectQuery = "SELECT *  FROM tbl_admission WHERE lcode='" . $_actionvalue . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        /*  Added By Sunil : 3-2-2017 for getting center Detail*/

        public function GetAllLearnerScore($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
					
                $_SelectQuery = "SELECT * FROM tbl_learner_score WHERE Learner_Code='" . $_actionvalue . "' AND
									ITGK_code='" . $_SESSION['User_LoginId'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        public function GetLearnerAllMarks($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_LoginRole = $_SESSION['User_UserRoll'];
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
                $_SelectQuery = "SELECT a.*, b.Event_Name 
                                        FROM tbl_result a
                                        LEFT JOIN tbl_events b 
                                        ON a.exameventnameID= b.Event_Id 
                                        WHERE a.scol_no ='" . $_actionvalue . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SPDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
            /*$_SelectQuery = "Select a.* , b.*,c.* from tbl_admission a 
                            LEFT JOIN tbl_organization_detail b on a.Admission_RspName=b.Organization_User 
                            LEFT JOIN tbl_user_master c on a.Admission_RspName=c.User_Code
                            where a.Admission_LearnerCode='".$_actionvalue."'";*/
            
             $_SelectQuery = "Select b.*,a.*,c.User_MobileNo as spmobile,c.User_EmailId as spemail 
                                FROM tbl_user_master AS a 
                                INNER JOIN tbl_organization_detail AS b ON a.User_Rsp=b.Organization_User 
								inner join tbl_user_master as c on a.User_Rsp=c.User_Code 
                                where a.User_LoginId='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        public function CenterDetails($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
                $_SelectQuery = "SELECT a.* , b.* 
                        FROM tbl_user_master a
                        LEFT JOIN tbl_organization_detail b
                        ON a.User_Code=b.Organization_User
                        WHERE a.User_UserRoll=7 AND User_LoginId='" . $_actionvalue . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        public function checkLearnerCourse($_LearnerCode, $_ITGK)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                //$_LearnerCode=$_SESSION['User_LearnerCode'];
                //$_ITGK=$_SESSION['User_LoginId'];
					$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
					
                $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_LearnerCode='" . $_LearnerCode . "' AND Admission_ITGK_Code='" . $_ITGK . "' AND Admission_Course='4'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                if ($_Response[0] == Message::SuccessfullyFetch) {
                    $_SelectQuery1 = "SELECT * FROM tbl_govempentryform WHERE learnercode='" . $_LearnerCode . "'";
                    $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                    //echo "<pre>";print_r($_Response1);
                    if ($_Response1[0] == Message::SuccessfullyFetch) {
                        return $_Response1;
                    } else {
                        return "NRED";
                    }
                } else {
                    $_Response[0] = Message::NoRecordFound;
                    $_Response[1] = Message::Error;
                    return $_Response;
                }


            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
                return $_Response;
            }

        }

        public function GetAllRDETAILS($_LearnerCode, $_ITGK)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
                //$_ITGK=$_SESSION['User_LoginId'];
                //$_action=$_SESSION['User_LearnerCode'];
                $_SelectQuery = "SELECT a.*,b.cstatus,c.lotname FROM tbl_govempentryform a
                                LEFT JOIN tbl_capcategory b ON  a.trnpending = b.capprovalid
                                LEFT JOIN tbl_clot c ON a.lot = c.lotid 
                                WHERE a.learnercode='" . $_LearnerCode . "' AND a.Govemp_ITGK_Code='" . $_ITGK . "'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        public function checkcorrectionStatus($_LearnerCode, $_ITGK)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
					
                //$_LearnerCode=$_SESSION['User_LearnerCode'];
                //$_ITGK=$_SESSION['User_LoginId'];
                $_SelectQuery = "SELECT * FROM tbl_correction_copy WHERE lcode='" . $_LearnerCode . "' AND Correction_ITGK_Code='" . $_ITGK . "' ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

    
        public function GetCorrectionDuplicateCertificate($_LearnerCode,$_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
              $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
					
            $_SelectQuery = "Select a.*,b.cstatus from tbl_correction_copy a
                                left join tbl_capcategory b 
                                on  a.dispatchstatus = b.capprovalid 
                                where a.lcode='".$_LearnerCode."' AND a.Correction_ITGK_Code='" . $_ITGK . "' "
                                . "order by a.cid desc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }  

		public function GetCorrectionDuplicateCertificateLot($_LearnerCode,$LotId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					$LotId = mysqli_real_escape_string($_ObjConnection->Connect(),$LotId);
					
				$_SelectQuery = "select lotname from tbl_clot where lotid='".$LotId."'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }


        public function checkEMPACKDETAILS($_action)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_action = mysqli_real_escape_string($_ObjConnection->Connect(),$_action);
					
                $_SelectQuery = "SELECT * FROM tbl_govempentryform WHERE learnercode='" . $_action . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        public function sendOTP($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
					
                $_SelectQuery = "SELECT count(*) AS total_count FROM tbl_admission WHERE Admission_Mobile='" . $_actionvalue . "' ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        public function insertOTP($_OTP, $_Mobile)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();

				$_OTP = mysqli_real_escape_string($_ObjConnection->Connect(),$_OTP);
				$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
				
            $_InsertQuery = "INSERT INTO tbl_ao_learner_register(AO_Code,AO_Mobile,AO_OTP, AO_Status)"
                . "SELECT CASE WHEN Max(AO_Code) IS NULL THEN 1 ELSE Max(AO_Code)+1 END AS AO_Code,"
                . "'" . $_Mobile . "' AS AO_Mobile,'"
                . "" . $_OTP . " as AO_OTP ', "
                . "0 AS AO_Status"
                . " FROM tbl_ao_learner_register";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

           // $_SMS = "OTP for Mobile Number Update Request is : " . $_OTP;
		   
		   $_SMS = "OTP is '" . $_OTP ."' for Mobile Number Update Request . RKCL";

            SendSMS($_Mobile, $_SMS);
            return $_Response;

        }


        public function verifyOTP($_OTP)
        {
            try {
                global $_ObjConnection;
                $_ObjConnection->Connect();
				$_OTP = mysqli_real_escape_string($_ObjConnection->Connect(),$_OTP);
                $_SelectQuery = "SELECT COUNT(*) AS total_count FROM tbl_ao_learner_register WHERE AO_OTP = '" . $_OTP . "'
					AND AO_Status = 0 ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;

        }

        public function updateLearnerMobile($_Mobile, $_Learner, $_OTP, $_OldMobile, $_itgkcode)
        {

            $_Response1 = [];


            global $_ObjConnection;
            $_ObjConnection->Connect();
			
			$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
			$_Learner = mysqli_real_escape_string($_ObjConnection->Connect(),$_Learner);
			$_OTP = mysqli_real_escape_string($_ObjConnection->Connect(),$_OTP);
			$_OldMobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_OldMobile);
			$_itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_itgkcode);
			
            $_UpdateQuery = "UPDATE tbl_ao_learner_register SET AO_Status='1' WHERE AO_OTP = '" . $_OTP . "' AND AO_Status = '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_DeleteQuery = "DELETE FROM tbl_ao_learner_register WHERE AO_Mobile='" . $_Mobile . "' AND AO_Status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
			$_Upd_User_Code = $_SESSION['User_LoginId'];
			//query to create log for mobile no update
			$_InsertLogQuery = "INSERT INTO tbl_adm_data_log (Adm_Data_Log_Lcode,Adm_Data_Log_Oldmobile,Adm_Data_Log_Newmobile,Adm_Data_Log_Upduserloginid,Adm_Data_Log_ITGK) values ('" . $_Learner . "','" . $_OldMobile . "','" . $_Mobile . "','" . $_Upd_User_Code . "', '" . $_itgkcode . "')";
			//query to create log for mobile no update
            if ($_Response[0] == "Successfully Updated") {
                $_UpdateQuery = "UPDATE tbl_admission SET Admission_Mobile ='" . $_Mobile . "' WHERE Admission_LearnerCode = '" . $_Learner . "' ";
                $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				 $_Response2 = $_ObjConnection->ExecuteQuery($_InsertLogQuery, Message::InsertStatement);
				 $this->UpdateLearnerMobileIlearn($_Learner,$_Mobile);

            }

            return $_Response1;
        }

        public function getLearnerEmailMobile($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
					
                $_SelectQuery = "SELECT Admission_Email, Admission_Mobile FROM tbl_admission WHERE
									Admission_LearnerCode ='" . $_actionvalue . "' ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        public function insertCodeLearnerEmail($_Code, $_Email)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();

			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			$_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email);
			
            $_InsertQuery = "INSERT INTO tbl_ao_register(AO_Code,AO_Email,AO_OTP, AO_Status)"
                . "SELECT CASE WHEN Max(AO_Code) IS NULL THEN 1 ELSE Max(AO_Code)+1 END AS AO_Code,"
                . "'" . $_Email . "' AS AO_Email,'"
                . "" . $_Code . " as AO_OTP ', "
                . "0 AS AO_Status"
                . " FROM tbl_ao_register";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            if ($_Response[0] == "Successfully Inserted") {
                $this->sendEmailtoLearner($_Email, $_Code);
            }

            return $_Response;

        }

        public function sendEmailtoLearner($email, $code)
        {
            $variableArray = [];
            $variableArray["User_LearnerCode"] = $_SESSION["User_LearnerCode"];
            $variableArray["loginLink"] = "https://www.myrkcl.com/verifyLearnerEmail.php?Code=" . base64_encode($code);
            $variableArray["username"] = $_SESSION['UserRoll_Name'];
            $sendEmail = new sendMail();
            $variableArray["htmlMessage"] = "Please click on the link below to complete your Email Update Process at MyRKCL Portal.";
            $_Response = $sendEmail->sendEmailHTML($email, $variableArray["username"], " Verification Link : MyRKCL ", "send_email_template_verify.php", $variableArray);
            return $_Response;
        }

        public function verifyEmail($_OTP)
        {
            try {
                global $_ObjConnection;
                $_ObjConnection->Connect();
				$_OTP = mysqli_real_escape_string($_ObjConnection->Connect(),$_OTP);
                $_SelectQuery = "SELECT COUNT(*) AS total_count, AO_Email FROM tbl_ao_register WHERE AO_OTP = '" . $_OTP . "'
							AND AO_Status = 0 ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;

        }

        public function sendFinalEmailtoLearner()
        {

            $response = $this->getLearnerEmailMobile($_SESSION['User_LearnerCode']);
            $row = mysqli_fetch_array($response[2]);
            $getLearnerEmail = $row["Admission_Email"];

            $sendEmail = new sendMail();
            $variableArray = [];
            $variableArray["loginLink"] = "https://www.myrkcl.com";
            $variableArray["username"] = $_SESSION['UserRoll_Name'];
            $variableArray["htmlMessage"] = "Your email <span style='color: orangered;'>".$getLearnerEmail."</span> has been successfully updated at MyRKCL Portal. Kindly Login for more details.";

            $_Response = $sendEmail->sendEmailHTML($getLearnerEmail, $variableArray["username"], "Email has been successfully updated", "send_email_template.php", $variableArray);
            return $_Response;
        }

        public function updateLearnerEmail($_Email, $_Learner, $_OTP)
        {

            $_Response1 = [];

            global $_ObjConnection;
            $_ObjConnection->Connect();
			
				$_Learner = mysqli_real_escape_string($_ObjConnection->Connect(),$_Learner);
				$_OTP = mysqli_real_escape_string($_ObjConnection->Connect(),$_OTP);
				
            $_UpdateQuery = "UPDATE tbl_ao_register SET AO_Status='1' WHERE AO_OTP = '" . $_OTP . "' AND AO_Status = '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_DeleteQuery = "DELETE FROM tbl_ao_register WHERE AO_Email='" . $_Email . "' AND AO_Status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);

            if ($_Response[0] == "Successfully Updated") {
                $_UpdateQuery = "UPDATE tbl_admission SET Admission_Email ='" . $_Email . "' WHERE Admission_LearnerCode = '" . $_Learner . "' ";
                $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $this->sendFinalEmailtoLearner();
            }

            return $_Response1;
        }

		public function finalExamMappingEvents() {
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_SelectQuery = "SELECT distinct fm.eventid AS Event_Id, ev.Event_Name 
						 FROM tbl_eligiblelearners as fm INNER JOIN tbl_events as ev 
						 on fm.eventid = ev.Event_Id WHERE ev.Event_Status='1' ORDER BY fm.eventid DESC";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;
			}

		return $_Response;
    }
	
	public function finalExamMappingCount($examId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
		
        $selectQuery = "SELECT count(*) AS n FROM tbl_eligiblelearners el WHERE el.eventid = '" . $examId . "'
						AND status LIKE('Eligible')";
        $_Response = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);

        return $_Response;
	}
	
	public function getEligibleLearnerDetails($learnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		$selectQuery = "SELECT el.learnercode AS Admission_LearnerCode, el.itgkcode AS Admission_ITGK_Code, el.batchname AS Batch_Name, el.coursename AS Course_Name FROM tbl_eligiblelearners el WHERE el.learnercode IN ('" . $learnerCode . "') GROUP BY el.learnercode";
        $_Response = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);

        return $_Response;
    }
	
	public function GetCorrectionPhoto($_LearnerCode, $_ITGK)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
					
                $_SelectQuery = "SELECT * FROM tbl_correction_copy WHERE lcode='" . $_LearnerCode . "' AND 
								Correction_ITGK_Code='" . $_ITGK . "' AND Correction_Payment_Status='1' AND dispatchstatus in ('0','13','14')
 order by cid DESC LIMIT 1";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

        /*  Added By Sunil : 3-2-2017 for getting center Detail*/

        //put your code here

		        public function checkcorrectionhistory($_LearnerCode, $_ITGK)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
					$_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK);
					
                //$_LearnerCode=$_SESSION['User_LearnerCode'];
                //$_ITGK=$_SESSION['User_LoginId'];
                $_SelectQuery = "SELECT a.*,b.Batch_Name FROM tbl_adm_log_foraadhar as a inner join tbl_batch_master as b on a.Adm_Log_Batch=b.Batch_Code WHERE Adm_Log_AdmissionLcode='" . $_LearnerCode . "' AND Adm_Log_ITGK_Code='" . $_ITGK . "' order by Adm_log_ApproveDate";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }
		
		function UpdateLearnerMobileIlearn($lcode,$mobile) {
			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		//			$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
		//			$mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$mobile);
					
				$data_json = json_encode(["FunctionName" => "UpdateLearnerMobile", "Learner_Code" => $lcode, "Learner_Mobile" => $mobile]);
				//$curl_request = $this->curlPost("http://localhost/toc/webservice/allFunctions.php", $data_json);
				$curl_request = $this->curlPost("http://49.50.65.36/webservice/allFunctions.php", $data_json);
				if ($curl_request['status'] == '200') {
				   // echo $curl_request['success'];
				} else {
				   // echo "0";
				}
			}
			}
		function curlPost($url, $data_json = NULL) {
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 300,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $data_json,
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",
					"postman-token: 5c22b1ed-b568-1c45-3d0d-ebff096e4cda"
				),
			));
			$response = curl_exec($curl);
		   // print_r($response);
			$err = curl_error($curl);
			curl_close($curl);

			if ($err) {
				$data = ['status' => '404', 'error' => $err];
			} else {
				$data = ['status' => '200', 'success' => $response];
			}
			return $data;
		}
		
		public function GetLearnerNameOnCert($_actionvalue)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_LoginRole = $_SESSION['User_UserRoll'];
                if ($_LoginRole == '21' || $_LoginRole == '18' || $_LoginRole == '19' || $_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30' || $_LoginRole == '28') {
                    //$_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' ";

                    $_SelectQuery = "select name as cert_name, fname as cert_fname from tbl_result where 
									scol_no='" . $_actionvalue . "' and result='pass' order by id DESC LIMIT 1 ";

                } else if ($_LoginRole == '14') {
                    //echo $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";

                    $_SelectQuery = "SELECT name as cert_name, fname as cert_fname 
                                     FROM tbl_admission a LEFT JOIN tbl_result d ON a.Admission_LearnerCode = d.scol_no
                                     WHERE a.Admission_LearnerCode='" . $_actionvalue . "' AND
									 a.Admission_RspName='" . $_SESSION['User_Code'] . "'
									 and result='pass' order by id DESC LIMIT 1";
                } else {
                    $_SelectQuery = "SELECT name as cert_name, fname as cert_fname 
                                     FROM tbl_result WHERE scol_no='" . $_actionvalue . "' AND 
									study_cen='" . $_SESSION['User_LoginId'] . "' and result='pass' order by id DESC LIMIT 1";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }

    }
