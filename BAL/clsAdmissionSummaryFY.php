<?php
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
/**
 * Description of clsAdmissionSummaryFY
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionSummaryFY {

    //put your code here

    public function FILLFY() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct Financial_Year From tbl_batch_master where Batch_Status='1' order by Batch_Code desc";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLCC() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct Course_Code From tbl_batch_master where Batch_Status='1' order by Batch_Code desc";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDataAll($_fyear, $_ccat) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
            $_fyear = mysqli_real_escape_string($_ObjConnection->Connect(),$_fyear);
            $_ccat = mysqli_real_escape_string($_ObjConnection->Connect(),$_ccat);
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11') {
                $_SelectQuery3 = "select count(Admission_LearnerCode) as admcount, Admission_Batch, Course_Name,Course_Code from 
                tbl_admission as a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code where 
                Admission_Batch in (select distinct Batch_Code from tbl_batch_master where 
                Financial_Year='" . $_fyear . "') and Admission_Course in 
                (select distinct Course_Code from tbl_course_master where Course_Type='" . $_ccat . "')
                 and Admission_Payment_Status='1' group by Admission_Course";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
			 elseif ($_SESSION['User_UserRoll'] == 14) {
			 
				$_SelectQuery3 = "select count(Admission_LearnerCode) as admcount, Admission_Batch, Course_Name,Course_Code from 
                tbl_admission as a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code where 
                Admission_Batch in (select distinct Batch_Code from tbl_batch_master where 
                Financial_Year='" . $_fyear . "') and Admission_Course in 
                (select distinct Course_Code from tbl_course_master where Course_Type='" . $_ccat . "')
                 and Admission_Payment_Status='1' and Admission_RspName = '".$_SESSION['User_Code']."' group by Admission_Course";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
			 }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetBatchFY($_course, $_fyear) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_fyear = mysqli_real_escape_string($_ObjConnection->Connect(),$_fyear);
            $_SelectQuery3 = "select distinct Batch_Code, Batch_Name, b.Course_Name from tbl_batch_master as a inner join"
                    . " tbl_course_master as b on a.Course_Code=b.Course_Code where a.Course_Code='" . $_course . "' and "
                    . "Financial_Year='" . $_fyear . "'";
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionSymmaryFYCenter($batcharray) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            $batcharray = mysqli_real_escape_string($_ObjConnection->Connect(),$batcharray);
            $_SelectQuery = "select distinct Admission_ITGK_Code from tbl_admission where Admission_Batch in ('" . $batcharray . "') ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionSymmaryFYdetail($batcharray) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            //$batcharray = mysqli_real_escape_string($_ObjConnection->Connect(),$batcharray);
            //$_SelectQuery3 = "select Admission_ITGK_Code,count(Admission_LearnerCode) as acount from tbl_admission "
            //     . "where Admission_Payment_Status='1' and Admission_Batch='" . $batchcode . "' group by Admission_ITGK_Code";
			if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {

                $_SelectQuery3 = "select Admission_ITGK_Code,
									sum(case when Batch_Code = '" . $batcharray[0]['Batch_Code'] . "' then 1 else 0 end) batch0 ,
									sum(case when Batch_Code = '" . $batcharray[1]['Batch_Code'] . "' then 1 else 0 end)  batch1,
									sum(case when Batch_Code = '" . $batcharray[2]['Batch_Code'] . "' then 1 else 0 end)  batch2 ,
									sum(case when Batch_Code = '" . $batcharray[3]['Batch_Code'] . "' then 1 else 0 end)  batch3,
									sum(case when Batch_Code = '" . $batcharray[4]['Batch_Code'] . "' then 1 else 0 end)  batch4,
									sum(case when Batch_Code = '" . $batcharray[5]['Batch_Code'] . "' then 1 else 0 end)  batch5 ,
									sum(case when Batch_Code = '" . $batcharray[6]['Batch_Code'] . "' then 1 else 0 end)  batch6 ,
									sum(case when Batch_Code = '" . $batcharray[7]['Batch_Code'] . "' then 1 else 0 end)  batch7,
									sum(case when Batch_Code = '" . $batcharray[8]['Batch_Code'] . "' then 1 else 0 end)  batch8 ,
									sum(case when Batch_Code = '" . $batcharray[9]['Batch_Code'] . "' then 1 else 0 end)  batch9,
									sum(case when Batch_Code = '" . $batcharray[10]['Batch_Code'] . "' then 1 else 0 end)  batch10,
									sum(case when Batch_Code = '" . $batcharray[11]['Batch_Code'] . "' then 1 else 0 end)  batch11 
									from tbl_admission as a inner join tbl_batch_master as b on a.Admission_Batch=b.Batch_Code 
									where Admission_Payment_Status='1' group by Admission_ITGK_Code ";
            } 
            elseif ($_SESSION['User_UserRoll'] == 14) {
			
				 $_SelectQuery3 = "select Admission_ITGK_Code,
                                            sum(case when Batch_Code = '" . $batcharray[0]['Batch_Code'] . "' then 1 else 0 end) batch0 ,
                                            sum(case when Batch_Code = '" . $batcharray[1]['Batch_Code'] . "' then 1 else 0 end)  batch1,
                                            sum(case when Batch_Code = '" . $batcharray[2]['Batch_Code'] . "' then 1 else 0 end)  batch2 ,
                                            sum(case when Batch_Code = '" . $batcharray[3]['Batch_Code'] . "' then 1 else 0 end)  batch3,
                                            sum(case when Batch_Code = '" . $batcharray[4]['Batch_Code'] . "' then 1 else 0 end)  batch4,
                                            sum(case when Batch_Code = '" . $batcharray[5]['Batch_Code'] . "' then 1 else 0 end)  batch5 ,
                                            sum(case when Batch_Code = '" . $batcharray[6]['Batch_Code'] . "' then 1 else 0 end)  batch6 ,
                                            sum(case when Batch_Code = '" . $batcharray[7]['Batch_Code'] . "' then 1 else 0 end)  batch7,
                                            sum(case when Batch_Code = '" . $batcharray[8]['Batch_Code'] . "' then 1 else 0 end)  batch8 ,
                                            sum(case when Batch_Code = '" . $batcharray[9]['Batch_Code'] . "' then 1 else 0 end)  batch9,
                                            sum(case when Batch_Code = '" . $batcharray[10]['Batch_Code'] . "' then 1 else 0 end)  batch10,
                                            sum(case when Batch_Code = '" . $batcharray[11]['Batch_Code'] . "' then 1 else 0 end)  batch11 
                                            from tbl_admission as a inner join tbl_batch_master as b on a.Admission_Batch=b.Batch_Code 
                                            where Admission_Payment_Status='1' and Admission_RspName = '".$_SESSION['User_Code']."' 
											group by Admission_ITGK_Code ";
			}
           


            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response3;
    }

    public function GetCenterdetail($centercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
            $centercode = mysqli_real_escape_string($_ObjConnection->Connect(),$centercode);
            $_SelectQuery3 = "select * from vw_itgkname_distict_rsp where ITGKCODE='" . $centercode . "'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
