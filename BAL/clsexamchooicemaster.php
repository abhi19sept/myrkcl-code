<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogendra 
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsexamchooicemaster {

    public function GetThesils($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$district = mysqli_real_escape_string($_ObjConnection->Connect(),$district);
				
            $_SelectQuery = "Select Tehsil_Code,Tehsil_Name,District_Name,Region_Name,State_Name, Status_Name,Country_Name From tbl_Tehsil_master as a inner join tbl_district_master as b on a.Tehsil_District=b.District_Code inner join tbl_region_master as c on b.District_Region=c.Region_Code inner join tbl_state_master as d on c.Region_State=d.State_Code inner join tbl_status_master as e on a.Tehsil_Status=e.Status_Code inner join tbl_country_master as f on d.State_Country=f.Country_Code and a.Tehsil_District='" . $district . "' AND (Tehsil_Name NOT LIKE('%east%') AND Tehsil_Name NOT LIKE('%west%') AND Tehsil_Name NOT LIKE('%north%') AND Tehsil_Name NOT LIKE('%south%')) ORDER BY State_Name, Region_Name, District_Name, Tehsil_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			if($_SESSION['User_UserRoll']=='1')
			{
				 $_SelectQuery = "Select *,d.Tehsil_Name as choice1 ,e.Tehsil_Name as choice2 ,f.Tehsil_Name as choice3  from tbl_examchoicemaster as a left join tbl_region_master as b on a.Region=b.Region_Code inner join tbl_district_master as c on a.district=c.District_Code 
				left join tbl_tehsil_master as d on a.choice1=d.Tehsil_Code 
				left join tbl_tehsil_master as e on a.choice2=e.Tehsil_Code 
				left join tbl_tehsil_master as f on a.choice3=f.Tehsil_Code 
				";
				
				
			}
			else
			{
				
				 $_SelectQuery = "Select *,d.Tehsil_Name as choice1 ,e.Tehsil_Name as choice2 ,f.Tehsil_Name as choice3 from tbl_examchoicemaster as a left join tbl_region_master as b on a.Region=b.Region_Code inner join tbl_district_master as c on a.district=c.District_Code 
				left join tbl_tehsil_master as d on a.choice1=d.Tehsil_Code 
				left join tbl_tehsil_master as e on a.choice2=e.Tehsil_Code 
				left join tbl_tehsil_master as f on a.choice3=f.Tehsil_Code 
			    where centercode='".$_SESSION['User_LoginId']."' ";
				
				
			}
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    //put your code here
     public function Add($_Region,$_District,$_Chooice1,$_Chooice2,$_Chooice3) 
    {
         //echo "org call";
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
			{
				$_Region = mysqli_real_escape_string($_ObjConnection->Connect(),$_Region);
				$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
				
				 $_InsertQuery = "Insert Into tbl_examchoicemaster(Region,district,choice1,
				 choice2,choice3,centercode) 
													 VALUES (
													 '". $_Region ."',
													 '". $_District ."',
													 '". $_Chooice1 ."',
													 '". $_Chooice2 ."',
													 '". $_Chooice3."',
													 '". $_SESSION['User_LoginId'] ."')";
				//echo $_InsertQuery;        
				$_DuplicateQuery = "Select * From tbl_examchoicemaster Where centercode='".$_SESSION['User_LoginId']."' ";
				$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
			
			}
			else 
			{
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else 
			{
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
       //print_r($_Response);
        return $_Response;
    }
	
	
	
	
	
	
	public function GetDatabyCode($_actionvalue)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select *  From tbl_examchoicemaster  Where id='" . $_actionvalue . "' AND
								centercode=".$_SESSION['User_LoginId']." ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_active)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_active = mysqli_real_escape_string($_ObjConnection->Connect(),$_active);
				
            $_DeleteQuery = "Delete from tbl_examchoicemaster Where  id='".$_active."'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	public function GetcenterDistrict() 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			
			 $_SelectQuery = "Select c.District_Name,c.District_Code from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join  tbl_district_master as c  on a.Organization_District=c.District_Code
											where b.User_LoginId=".$_SESSION['User_LoginId']." ";
					    
					
			$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	
	
	
	
	public function Update($_Code,$_District,$_Chooice1,$_Chooice2,$_Chooice3) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			 if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
			 {
					$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
					$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
					
				 $_UpdateQuery = "Update tbl_examchoicemaster set district='" . $_District . "',"
						. "choice1='" . $_Chooice1 ."',"
						. "choice2='" . $_Chooice2 ."',"
						. "choice3='" . $_Chooice3 ."',"
						
						. "status='Pending'"
						. " Where id='" . $_Code . "' AND centercode=".$_SESSION['User_LoginId']." ";
				$_DuplicateQuery = "Select * From tbl_examchoicemaster Where status='Approve' AND  id='" . $_Code . "'";
				//$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
				$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
				
			} 
			else 
			{
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            if($_Response[0]==Message::NoRecordFound)
            {
                //echo $_UpdateQuery;
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                //print_r($_Response);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	 public function SendOTP() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = "OTP for Verification of Authentication is " . $_OTP;

        try {
            $_UserM = $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_LoginId='" . $_UserM . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_Mobile = $_Row1['User_MobileNo'];
           


            $_InsertQuery = "Insert Into tbl_choice_otp_register(Choice_Code,Choice_Mobile,Choice_OTP)"
                    . "Select Case When Max(Choice_Code) Is Null Then 1 Else Max(Choice_Code)+1 End as Choice_Code,"
                    . "'" .  $_Mobile . "' as Choice_Mobile,'"
                    . "" . $_OTP . " as Choice_OTP '"
                    . " From tbl_choice_otp_register";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_choice_otp_register Where Choice_Mobile='" . $_Mobile . "' AND Choice_OTP = '" . $_OTP . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	
	public function Verify($_Otp) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_Otp);
				
            $_SelectQuery = "Select * FROM tbl_choice_otp_register WHERE  Choice_OTP = '" . $_Otp . "' AND Choice_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) 
			{
                echo "Invalid Verification Details. Please Try Again";
                return;
            }
			else 
			{
				$_row = mysqli_fetch_array($_Response1[2]);
				$_DBOTP = $_row['Choice_OTP'];
				
				if($_DBOTP == $_Otp) {
					$_UpdateQuery = "Update tbl_choice_otp_register set Choice_Status='1' WHERE  Choice_OTP = '" . $_Otp . "' AND Choice_Status = '0'";
					$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				}
				else {
					echo "Invalid Verification Details. Please Try Again";
                return;
				}
                
             
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	
	
	
	public function GetAllDistrict($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if(!$region)
            {
            $_SelectQuery = "Select District_Code,District_Name,Region_name,State_Name, Status_Name,Country_Name From tbl_district_master as a inner join tbl_region_master as b on a.District_Region=b.Region_Code inner join tbl_state_master as c on b.Region_State=c.State_Code inner join tbl_status_master as d on a.District_Status=d.Status_Code inner join tbl_country_master as e on c.State_Country=e.Country_Code where District_StateCode='29' order by District_Name";
            }
            else 
			{
                $_SelectQuery = "Select District_Code,District_Name,Region_name,State_Name,"
                    . "Status_Name,Country_Name From tbl_district_master as a inner join tbl_region_master as b "
                    . "on a.District_Region=b.Region_Code inner join tbl_state_master as c "
                    . "on b.Region_State=c.State_Code inner join tbl_status_master as d "
                    . "on a.District_Status=d.Status_Code inner join tbl_country_master as e "
                    . "on c.State_Country=e.Country_Code and a.District_Region='" . $region . "' order by District_Name";
                
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	public function Getmobile()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_user_master Where User_LoginId= '" . $_SESSION['User_LoginId'] ."' Limit 1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
    
    
    
    
}
