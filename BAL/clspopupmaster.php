<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cls pop up Master
 *
 * @author Yogendra soni
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clspopupmaster {
    //put your code here
    
     public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_fixed_popup_content as a inner join tbl_userroll_master as b on a.Role=b.UserRoll_Code";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
          //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
   
    
   
    public function DeleteRecord($_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $query = "SELECT * FROM tbl_fixed_popup_viewedby WHERE popupid = '" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            if (!mysqli_num_rows($_Response[2])) {
                $_DeleteQuery = "Delete From tbl_fixed_popup_content Where id='" . $_Code . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            } else {
                $query = "UPDATE tbl_fixed_popup_content SET status = 0 WHERE id = '" . $_Code . "'";
                $_Response=$_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_Msg, $_Centercode, $_Link,$_Sub,$_Role) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Role = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role);
				$_Centercode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Centercode);
				
            $_InsertQuery = "Insert Into tbl_fixed_popup_content(content,usercode,
			 Link,subject,Role,status) 
												 VALUES (
												 '". $_Msg ."',
												 '". $_Centercode ."',
												 '". $_Link ."',
												 '". $_Sub ."',
												 '".$_Role."',
												 '1'
												 )";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_fixed_popup_content Where status='1' AND usercode='". $_Centercode ."' AND Role='".$_Role."' ";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
}
