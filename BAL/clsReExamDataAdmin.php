<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsReExamDataAdmin {

    //put your code here   
    public function GetDataAll($_event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginUserRole = $_SESSION['User_UserRoll'];
				$_event = mysqli_real_escape_string($_ObjConnection->Connect(),$_event);
				
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '28' || $_LoginUserRole == '10' || $_LoginUserRole == '11') {

                $_SelectQuery = "select a.learnercode,a.learnername,a.fathername,a.dob,ad.Admission_Mobile,b.Course_Name, c.Batch_Name, a.itgkcode, d.ITGK_NAME, d.District_Name as itgkdistrict, 					d.Tehsil_Name as itgktehsil from examdata as a inner join tbl_course_master as b on 
								a.coursename=b.Course_Code inner join tbl_batch_master as c on a.batchname=c.Batch_Code inner join 
								vw_itgkname_distict_rsp as d on a.itgkcode=d.ITGKCODE INNER JOIN tbl_admission as ad
 								on a.learnercode=ad.Admission_LearnerCode where examid='" . $_event . "' AND learnertype='reexam' AND paymentstatus='1'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                //print_r( $_SESSION['User_LoginId']);
                $_SelectQuery = "select * from examdata  where itgkcode =" . $_SESSION['User_LoginId'] . " AND examid='" . $_event . "'  AND learnertype='reexam'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }


            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_events ORDER BY Event_Id DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
