<?php

/**
 * Description of clsLIVRrpt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsLIVRrpt {

    //put your code here
    public function GetDatabyCode($userid) {
        //echo $userid;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$userid = mysqli_real_escape_string($_ObjConnection->Connect(),$userid);
				
            $_SelectQuery = "Select a.Admission_Code, a.Admission_LearnerCode, a.Admission_Name, a.Admission_Fname, a.Admission_DOB, a.Admission_Medium,"
                    . "a.Admission_Fee, a.Admission_Photo, a.Admission_Sign, a.Admission_ITGK_Code, Course_Name, Batch_Name, a.Admission_Date,a.Admission_Mobile,"
                    . "a.Admission_Email,a.Timestamp,District_Name, Organization_Name, Batch_StartDate "
                    . "From tbl_Admission as a inner join tbl_user_master as g "
                    . "on a.Admission_ITGK_Code=g.User_LoginId inner join tbl_organization_detail as b "
                    . "on g.User_Code=b.Organization_User inner join tbl_district_master as c "
                    . "on b.Organization_District=c.District_Code inner join tbl_batch_master as d "
                    . "on a.Admission_Batch=d.Batch_Code inner join tbl_course_master as e "
                    . "on a.Admission_Course=e.Course_Code "
                    . " WHERE Admission_Code='" . $userid . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            $_SelectQuery = "Select Batch_Name,Admission_Code,Admission_LearnerCode, Admission_Name, Admission_Batch, Admission_Fname, Admission_DOB, Admission_Photo, Admission_Sign, Admission_Fee, Admission_Payment_Status
				FROM tbl_admission as a inner join tbl_batch_master as b on a.Admission_Batch=b.Batch_Code WHERE  Admission_Batch = '" . $batch . "' AND  Admission_Course = '" . $course . "' AND Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
            //WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRecord($_Admission_Code, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Admission_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Admission_Code);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_DeleteQuery = "Delete From tbl_admission Where Admission_Code= " . $_Admission_Code . "";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);

            $_UpdateQuery = "Update tbl_intake_master set Intake_Consumed=Intake_Consumed - 1,"
                    . "Intake_Available=Intake_Available + 1 Where Intake_Center = '" . $_ITGK_Code . "' AND Intake_Batch = " . $_batch . " ";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllPrintRecp($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            $_SelectQuery = "Select Admission_Code,Admission_LearnerCode, Admission_Name, Admission_Batch, Admission_Fname, Admission_DOB, Admission_Photo, Admission_Sign, Admission_Fee, Admission_Payment_Status
								FROM tbl_admission WHERE  Admission_Batch = '" . $batch . "' AND  Admission_Course = '" . $course . "' AND Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND Admission_Payment_Status='1'";
            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
            //WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllPrintRecpDwnld($lid) { /// for new invoice system GST
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lid = mysqli_real_escape_string($_ObjConnection->Connect(),$lid);
				
            $_SelectQuery = "Select Admission_Code,Admission_LearnerCode, Admission_Name, Admission_Batch, 
                Admission_Course,Admission_Fname, Admission_DOB,Admission_Mobile, Gst_Invoce_SAC,
                Admission_ITGK_Code,Admission_Photo, Admission_Sign, Admission_Fee, Admission_Address,
                Admission_Payment_Status,Admission_Date,b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
                b.amount,b.addtime,c.Organization_Name,Course_Name,Course_Duration,Course_DurationType, District_Name,
                Batch_Name, Gst_Invoce_TotalFee,Gst_Invoce_VMOU, RKCL_Share, VMOU_Share FROM tbl_admission 
                as a inner join tbl_admission_invoice as b on a.Admission_Code=b.invoice_ref_id inner join tbl_organization_detail 
                as c on a.User_Code=c.Organization_User inner join tbl_batch_master as d on a.Admission_Batch=d.Batch_Code
                inner join tbl_course_master as e on a.Admission_Course=e.Course_Code inner join tbl_district_master as g on
                Organization_District=District_Code inner join tbl_gst_invoice_master as f on a.Admission_Batch=f.Gst_Invoce_Category_Id WHERE  
                Admission_Code = '" . $lid . "' AND Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' 
                AND Admission_Payment_Status='1' AND f.Gst_Invoce_Category_Type='admission'";
            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
            //WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function addPhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        if ($coursecode == '5') {
            $Imagepath = '../upload/DownloadPrintRecpt/rscfa.jpg';
        } else {
            $Imagepath = '../upload/DownloadPrintRecpt/rscit.jpg';
        }

        $pdf->Image($Imagepath, 8, 107, 50, 25);
        $pdf->Image($Imagepath, 8, 256, 50, 25);

        $pdf->Output($path, "F");
    }
 public function addNotePhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        if ($coursecode == '1' || $coursecode == '4') {
            $Imagepath = '../upload/DownloadPrintRecpt/GovEmpNote.jpg';
        } 

        $pdf->Image($Imagepath, 63, 123, 140, 10);
        $pdf->Image($Imagepath, 63, 272, 140, 10);
        //$pdf->Image($Imagepath, 8, 256, 50, 25);

        $pdf->Output($path, "F");
    }
    public function convert_number_to_words($number) {

        $hyphen = ' ';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'Zero',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Fourty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100 => 'Hundred',
            1000 => 'Thousand',
            1000000 => 'Million',
            1000000000 => 'Billion',
            1000000000000 => 'Trillion',
            1000000000000000 => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

}
