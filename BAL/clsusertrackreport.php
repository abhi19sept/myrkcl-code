<?php
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsUserReport {
    
    public function GetUserRoll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select UserRoll_Code,UserRoll_Name From tbl_userroll_master where UserRoll_Status = '1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAllUser($useroll) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$useroll = mysqli_real_escape_string($_ObjConnection->Connect(),$useroll);
				
            $_SelectQuery = "Select User_Code,User_LoginId From tbl_user_master where User_UserRoll='".$useroll."' and User_Status = '1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function ShowUserLoginReport($ddlUserroll,$ddlusersloginid,$ddlstartdate,$ddlenddate)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$ddlUserroll = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlUserroll);
				$ddlusersloginid = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlusersloginid);
				$ddlstartdate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlstartdate);
				$ddlenddate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlenddate);
				
              $_SelectQuery = "select * from tbl_usertrackmaster where Track_Userroll='".$ddlUserroll."' and Track_Userloginid='".$ddlusersloginid."' "
                      . "and (Track_Datetime BETWEEN '".$ddlstartdate."' AND '".$ddlenddate."')";
              $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
}

?>