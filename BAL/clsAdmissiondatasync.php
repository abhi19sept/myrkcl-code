<?php
	
	require 'DAL/classconnectionNEW.php' ;
	
	$_ObjConnection=new _Connection();
	$_Response=array();
	
	class clsAdmissiondatasync
	{   
		public function GetAllCourse()
			{
				global $_ObjConnection;
				$_ObjConnection->Connect();
				 try {
			   $_SelectQuery = "Select * From tbl_course_master";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			   //print_r($_Response);
			} catch (Exception $_ex) {

				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;
			   
			}			
				return $_Response;
		}
		
		public function GetAllBatch($_coursecode)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			 try {
                             $_coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_coursecode);
				   $_SelectQuery = "Select * From tbl_batch_master where Course_Code = '" . $_coursecode . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				   //print_r($_Response);
				} catch (Exception $_ex) {
					$_Response[0] = $_ex->getLine() . $_ex->getTrace();
					$_Response[1] = Message::Error;				   
				}			
					return $_Response;
		}	
		
		public function GetBatchFee($_batch)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			 try {
                             $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				   $_SelectQuery = "Select Course_Fee From tbl_batch_master where Batch_Code = '" . $_batch . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				   //print_r($_Response);
				} catch (Exception $_ex) {
					$_Response[0] = $_ex->getLine() . $_ex->getTrace();
					$_Response[1] = Message::Error;				   
				}			
					return $_Response;
		}	
		
		public function GetCourseName($_course)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			 try {
                             $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				   $_SelectQuery = "Select Course_Name From tbl_course_master where Course_Code = '" . $_course . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				   //print_r($_Response);
				} catch (Exception $_ex) {
					$_Response[0] = $_ex->getLine() . $_ex->getTrace();
					$_Response[1] = Message::Error;				   
				}			
					return $_Response;
		}
		
		public function FillBatchName($_batch)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			 try {
                             $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				   $_SelectQuery = "Select Batch_Name From tbl_batch_master where Batch_Code = '" . $_batch . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				   //print_r($_Response);
				} catch (Exception $_ex) {
					$_Response[0] = $_ex->getLine() . $_ex->getTrace();
					$_Response[1] = Message::Error;				   
				}			
					return $_Response;
		}
		
		public function GetTmpData($_course, $_batch, $_coursecode, $_batchcode)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			 try {
                             $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
                             $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
                             $_coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_coursecode);
                             $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
                             
				   $_SelectQuery = "Select * From tbl_tmp_admission_copy where CourseId = '" . $_coursecode . "' AND BatchId = '" . $_batchcode . "'";
				   
				   $_DuplicateQuery = "Select * From tbl_admission Where Admission_Course='" . $_coursecode . "' AND Admission_Batch= '" . $_batchcode . "'";
                   $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            
            if($_Response[0]==Message::NoRecordFound)
            {
					   $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					   
					   while($_data = mysqli_fetch_array($_Response1[2]))
					   {
						   $pic= str_replace("'","",$_data['Learnercode']).'_'.'photo'.'.'.'jpg';
						   $sign= str_replace("'","",$_data['Learnercode']).'_'.'sign'.'.'.'jpg';
						   $scan= str_replace("'","",$_data['Learnercode']).'_'.'scan'.'.'.'jpg';
						   $_User_Code =   $_SESSION['User_Code'];
						   
						   $_InsertQuery = "Insert Into tbl_admission(Admission_Code,Admission_LearnerCode,"
							. "Admission_ITGK_Code,BioMatric_Status,Admission_Date,Admission_Course,Admission_Batch,"
							. "Admission_Fee,Admission_Installation_Mode,Admission_PhotoProcessing_Status,rejection_reason,rejection_type,Admission_Payment_Status,Admission_ReceiptPrint_Status,Admission_TranRefNo,Admission_Name,Admission_Fname,Admission_DOB,"
							. "Admission_MTongue,Admission_Photo,Admission_Sign,Admission_Gender,Admission_Scan,"
							. "Admission_MaritalStatus,Admission_Medium,Admission_PH,Admission_PID,"
							. "Admission_UID,Admission_District,Admission_Tehsil,Admission_Address,"
							. "Admission_PIN,Admission_Mobile,Admission_Phone,Admission_Email,User_Code,"
							. "Admission_Qualification,Admission_Ltype,Admission_GPFNO) "
							. "Select Case When Max(Admission_Code) Is Null Then 1 Else Max(Admission_Code)+1 End as Admission_Code,"
							. "'" . str_replace("'","",$_data['Learnercode']) . "' as Admission_LearnerCode,'" .$_data['CenterCode']. "' as Admission_ITGK_Code,"
							. "'0','" . $_data['AdmissionDate'] . "' as Admission_Date,'" . $_data['CourseId'] . "' as Admission_Course,  '" . $_data['BatchId'] . "' as Admission_Batch,"
							. "'" . $_data['Fee']  . "' as Admission_Fee,'" . $_data['Imode']  . "' as Admission_Installation_Mode, 'Approved' as Admission_PhotoProcessing_Status, 'Success' as rejection_reason, 'Success' as rejection_type,'1' as Admission_Payment_Status,'1' as Admission_ReceiptPrint_Status,'123456789' as Admission_TranRefNo, '" .$_data['FirstName']. "' as Admission_Name,'" .$_data['FatherName_HusbandName']. "' as Admission_Fname,"
							. "'" .$_data['DOB']. "' as Admission_DOB,'" .$_data['MotherToungue']. "' as Admission_MTongue,'" .$pic. "' as Admission_Photo,'" .$sign. "' as Admission_Sign,"  
							. "'" .$_data['Gender']. "' as Admission_Gender,'" .$scan. "' as Admission_Scan,"
							. "'" .$_data['MaritalStatus']. "' as Admission_MaritalStatus,'" .$_data['Medium']. "' as Admission_Medium,"
							. "'" .$_data['PhysicallyChalengedStatus']. "' as Admission_PH,'123456789' as Admission_PID,"
							. "'123456789' as Admission_UID,'" .$_data['DistrictId']. "' as Admission_District,"
							. "'011' as Admission_Tehsil,'" . $_data['LearnerAddress'] . "' as Admission_Address,"
							. "'" .$_data['PinCode']. "' as Admission_PIN,'" . $_data['LearnerMobileNo'] . "' as Admission_Mobile,'" . $_data['Landline'] . "' as Admission_Phone,"
							. "'" .$_data['EmailId']. "' as Admission_Email,'" . $_User_Code . "' as User_Code, '123' as Admission_Qualification,"
							. "'" . $_data['LearnerType_Id']  . "' as Admission_Ltype, '123456789' as Admission_GPFNO"
							. " From tbl_admission";
							
							$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					   }
			 
				 }
				   //print_r($_Response);
				} catch (Exception $_ex) {
					$_Response[0] = $_ex->getLine() . $_ex->getTrace();
					$_Response[1] = Message::Error;				   
				}			
					return $_Response;
		}	

 
	}
?>