<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAddressChangeVisitRpt
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAddressChangeVisitRpt {
    //put your code here
    
         public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "SELECT tcai.Organization_Name,
                            tcai.fld_remarks AS fld_remarks,                                   
                            tcai.Organization_Address AS Organization_Address, 
                            tcai.fld_ITGK_Code AS fld_ITGK_Code, 
                            tcai.fld_addedOn AS submission_date,
                            tcai.fld_spActionDate AS submission_date_sp,
                            tcai.fld_rkclActionDate AS submission_date_rkcl,
                            tcai.Organization_AreaType AS Organization_AreaType,
                            ttm.Tehsil_Name AS Organization_Tehsil,
                            tmt.Municipality_Type_Name AS Organization_Municipal_Type, 
                            tmm.Municipality_Name AS Organization_Municipal,
                            twm.Ward_Name AS Organization_WardNo, 
                            tps.Block_Name AS Organization_Panchayat, 
                            tgp.GP_Name AS Organization_Gram, 
                            tvm.Village_Name AS Organization_Village,                                   
                            tcai.Organization_Address_old AS Organization_Address_old, 
                            totm_old.Org_Type_Name AS Organization_Type_old,                                  
                            tcai.Organization_AreaType_old AS Organization_AreaType_old,
                            ttm_old.Tehsil_Name AS Organization_Tehsil_old,
                            tmt_old.Municipality_Type_Name AS Organization_Municipal_Type_old, 
                            tmm_old.Municipality_Name AS Organization_Municipal_old,
                            twm_old.Ward_Name AS Organization_WardNo_old, 
                            tps_old.Block_Name AS Organization_Panchayat_old, 
                            tgp_old.GP_Name AS Organization_Gram_old, 
                            tvm_old.Village_Name AS Organization_Village_old                                   
                            FROM tbl_change_address_itgk tcai                                   
                            LEFT JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode 
                            LEFT JOIN tbl_org_type_master totm ON tcai.Organization_Type = totm.Org_Type_Code 
                            LEFT JOIN tbl_org_type_master totm_old ON tcai.Organization_Type_old = totm_old.Org_Type_Code
                                                               
                            LEFT JOIN tbl_tehsil_master ttm ON tcai.Organization_Tehsil = ttm.Tehsil_Code 
                            LEFT JOIN tbl_tehsil_master ttm_old ON tcai.Organization_Tehsil_old = ttm_old.Tehsil_Code 
                            
                            LEFT JOIN tbl_municipality_type tmt ON tcai.Organization_Municipal_Type = tmt.Municipality_Type_Code 
                            LEFT JOIN tbl_municipality_type tmt_old ON tcai.Organization_Municipal_Type_old = tmt_old.Municipality_Type_Code 
                            LEFT JOIN tbl_municipality_master tmm ON tcai.Organization_Municipal = tmm.Municipality_Raj_Code 
                            LEFT JOIN tbl_municipality_master tmm_old ON tcai.Organization_Municipal_old = tmm_old.Municipality_Raj_Code 
                            LEFT JOIN tbl_ward_master twm ON tcai.Organization_WardNo = twm.Ward_Code 
                            LEFT JOIN tbl_ward_master twm_old ON tcai.Organization_WardNo_old = twm_old.Ward_Code 
                            LEFT JOIN tbl_panchayat_samiti tps ON tcai.Organization_Panchayat = tps.Block_Code 
                            LEFT JOIN tbl_panchayat_samiti tps_old ON tcai.Organization_Panchayat_old = tps_old.Block_Code 
                            LEFT JOIN tbl_gram_panchayat tgp ON tcai.Organization_Gram = tgp.GP_Code 
                            LEFT JOIN tbl_gram_panchayat tgp_old ON tcai.Organization_Gram_old = tgp_old.GP_Code 
                            LEFT JOIN tbl_village_master tvm ON tcai.Organization_Village = tvm.Village_Code 
                            LEFT JOIN tbl_village_master tvm_old ON tcai.Organization_Village_old = tvm_old.Village_Code                                   
                            WHERE fld_requestChangeType = 'address' and fld_status = '2' GROUP BY tcai.fld_ITGK_Code";
            } elseif ($_SESSION['User_UserRoll'] == 23) {
            $_SelectQuery = "SELECT tcai.Organization_Name,
                            tcai.fld_remarks AS fld_remarks,                                   
                            tcai.Organization_Address AS Organization_Address, 
                            tcai.fld_ITGK_Code AS fld_ITGK_Code, 
                            tcai.fld_addedOn AS submission_date,
                            tcai.fld_spActionDate AS submission_date_sp,
                            tcai.fld_rkclActionDate AS submission_date_rkcl,
                            tcai.Organization_AreaType AS Organization_AreaType,
                            ttm.Tehsil_Name AS Organization_Tehsil,
                            tmt.Municipality_Type_Name AS Organization_Municipal_Type, 
                            tmm.Municipality_Name AS Organization_Municipal,
                            twm.Ward_Name AS Organization_WardNo, 
                            tps.Block_Name AS Organization_Panchayat, 
                            tgp.GP_Name AS Organization_Gram, 
                            tvm.Village_Name AS Organization_Village,                                   
                            tcai.Organization_Address_old AS Organization_Address_old, 
                            totm_old.Org_Type_Name AS Organization_Type_old,                                  
                            tcai.Organization_AreaType_old AS Organization_AreaType_old,
                            ttm_old.Tehsil_Name AS Organization_Tehsil_old,
                            tmt_old.Municipality_Type_Name AS Organization_Municipal_Type_old, 
                            tmm_old.Municipality_Name AS Organization_Municipal_old,
                            twm_old.Ward_Name AS Organization_WardNo_old, 
                            tps_old.Block_Name AS Organization_Panchayat_old, 
                            tgp_old.GP_Name AS Organization_Gram_old, 
                            tvm_old.Village_Name AS Organization_Village_old                                   
                            FROM tbl_change_address_itgk tcai                                   
                            LEFT JOIN tbl_organization_detail tod ON tod.Organization_User = tcai.fld_ITGK_UserCode 
                            LEFT JOIN tbl_org_type_master totm ON tcai.Organization_Type = totm.Org_Type_Code 
                            LEFT JOIN tbl_org_type_master totm_old ON tcai.Organization_Type_old = totm_old.Org_Type_Code
                                                               
                            LEFT JOIN tbl_tehsil_master ttm ON tcai.Organization_Tehsil = ttm.Tehsil_Code 
                            LEFT JOIN tbl_tehsil_master ttm_old ON tcai.Organization_Tehsil_old = ttm_old.Tehsil_Code 
                            
                            LEFT JOIN tbl_municipality_type tmt ON tcai.Organization_Municipal_Type = tmt.Municipality_Type_Code 
                            LEFT JOIN tbl_municipality_type tmt_old ON tcai.Organization_Municipal_Type_old = tmt_old.Municipality_Type_Code 
                            LEFT JOIN tbl_municipality_master tmm ON tcai.Organization_Municipal = tmm.Municipality_Raj_Code 
                            LEFT JOIN tbl_municipality_master tmm_old ON tcai.Organization_Municipal_old = tmm_old.Municipality_Raj_Code 
                            LEFT JOIN tbl_ward_master twm ON tcai.Organization_WardNo = twm.Ward_Code 
                            LEFT JOIN tbl_ward_master twm_old ON tcai.Organization_WardNo_old = twm_old.Ward_Code 
                            LEFT JOIN tbl_panchayat_samiti tps ON tcai.Organization_Panchayat = tps.Block_Code 
                            LEFT JOIN tbl_panchayat_samiti tps_old ON tcai.Organization_Panchayat_old = tps_old.Block_Code 
                            LEFT JOIN tbl_gram_panchayat tgp ON tcai.Organization_Gram = tgp.GP_Code 
                            LEFT JOIN tbl_gram_panchayat tgp_old ON tcai.Organization_Gram_old = tgp_old.GP_Code 
                            LEFT JOIN tbl_village_master tvm ON tcai.Organization_Village = tvm.Village_Code 
                            LEFT JOIN tbl_village_master tvm_old ON tcai.Organization_Village_old = tvm_old.Village_Code                                   
                            WHERE fld_requestChangeType = 'address' and tod.Organization_District='" . $_SESSION['Organization_District'] . "'
                            and fld_status = '2' GROUP BY tcai.fld_ITGK_Code";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
