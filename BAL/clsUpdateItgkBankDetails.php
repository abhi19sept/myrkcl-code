<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBankAccount
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();
require 'DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();
class clsUpdateItgkBankDetails {
    //put your code here
 
public function AuthenticateItgk()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_SelectQuery = "Select * from tbl_bank_account_itgk_update Where
									BA_ITGK_Code= '" . $_SESSION['User_LoginId'] . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function SHOWDATA() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				if ($_SESSION['User_UserRoll'] == 7 ) {
					$_SelectQuery = "Select * from tbl_bank_account where Bank_User_Code ='" .$_SESSION['User_LoginId'] . "'";  
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
				} 
				else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAllBanks() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct bankname from tbl_rajbank_master";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
   public function Updatewithoutdoc($autoid,$_IfscCode,$_BankName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$autoid = mysqli_real_escape_string($_ObjConnection->Connect(),$autoid);
					
			$_UpdateQuery = "Update tbl_bank_account set Bank_Ifsc_code='" . $_IfscCode ."', Bank_Name='" . $_BankName ."'
					Where Bank_Account_Code='" . $autoid . "' AND Bank_User_Code='".$_SESSION['User_LoginId']."'";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			$_Updatestatus = "update tbl_bank_account_itgk_update set BA_Status='1' where BA_Status='0' and
								BA_ITGK_Code='".$_SESSION['User_LoginId']."'";
			$_Responses=$_ObjConnection->ExecuteQuery($_Updatestatus, Message::UpdateStatement);
			
		} catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	public function Updatewithdoc($autoid,$_IfscCode,$_BankName,$docname) {
        global $_ObjConnection;
		global $_ObjFTPConnection;
        $_ObjConnection->Connect();
		
        try {
				$autoid = mysqli_real_escape_string($_ObjConnection->Connect(),$autoid);
				
				$_UpdateQuery = "Update tbl_bank_account set Bank_Ifsc_code='" . $_IfscCode ."', Bank_Name='" . $_BankName ."',
					Bank_Document='".$docname."' Where Bank_Account_Code='" . $autoid . "' AND
					Bank_User_Code='".$_SESSION['User_LoginId']."'";
				
				$_Updatestatus = "update tbl_bank_account_itgk_update set BA_Status='1' where BA_Status='0' and
								BA_ITGK_Code='".$_SESSION['User_LoginId']."'";
				
				$ftpaddress = $_ObjFTPConnection->ftpPathIp();
				
				$bankproofdoc = $ftpaddress.'Bankdocs/' . '/' . $docname;
				
				$ch = 	curl_init($bankproofdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeDoc = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
				if($responseCodeDoc == 200){
					$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						$_Responses=$_ObjConnection->ExecuteQuery($_Updatestatus, Message::UpdateStatement);
				}				
				else
					{
						echo "Please Attach Necessary Document.";
						return;
					}				
           } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
          
}
