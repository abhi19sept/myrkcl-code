<?php

/**
 * Description of clsGstInvoiceRpt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGstInvoiceSummary {

    //put your code here


    public function Learner_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select Admission_Code,Admission_LearnerCode, Admission_Name, Admission_Batch, 
                Admission_Course,Admission_Fname, Admission_DOB,Admission_Mobile, Gst_Invoce_SAC,
                Admission_ITGK_Code,Admission_Photo, Admission_Sign, Admission_Fee, Admission_Address,
                Admission_Payment_Status,Admission_Date,b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
                b.amount,Date_Format(b.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name,Course_Name,Course_Duration,Course_DurationType, District_Name,
                Batch_Name, Gst_Invoce_TotalFee,Gst_Invoce_VMOU, RKCL_Share, VMOU_Share FROM tbl_admission 
                as a inner join tbl_admission_invoice as b on a.Admission_Code=b.invoice_ref_id inner join tbl_organization_detail 
                as c on a.User_Code=c.Organization_User inner join tbl_batch_master as d on a.Admission_Batch=d.Batch_Code
                inner join tbl_course_master as e on a.Admission_Course=e.Course_Code inner join tbl_district_master as g on
                Organization_District=District_Code inner join tbl_gst_invoice_master as f on a.Admission_Batch=f.Gst_Invoce_Category_Id WHERE  
                 Admission_Payment_Status='1' AND f.Gst_Invoce_Category_Type='admission' AND Date_Format(b.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(b.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY b.invoice_no ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function reexam_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
b.amount,Date_Format(b.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name, District_Name,a.learnername,
 Gst_Invoce_TotalFee,Gst_Invoce_VMOU FROM examdata as a inner join 
tbl_reexam_invoice as b on a.examdata_code=b.exam_data_code inner join tbl_user_master as h on
 a.itgkcode=h.User_LoginId inner join tbl_organization_detail as c on h.User_Code=c.Organization_User inner join tbl_district_master as g on 
Organization_District=District_Code inner join tbl_gst_invoice_master as f on
 a.examid=f.Gst_Invoce_Category_Id WHERE  
                 paymentstatus='1' AND f.Gst_Invoce_Category_Type='Reexam' AND Date_Format(b.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(b.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY b.invoice_no ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function NCR_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select distinct invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST, a.amount,
Date_Format(a.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name, District_Name, 
Gst_Invoce_TotalFee,Gst_Invoce_VMOU FROM tbl_ncr_invoice as a INNER join tbl_user_master um ON 
a.Ao_Itgkcode = um.User_LoginId inner join tbl_organization_detail as c on um.User_Code=c.Organization_User
 inner join tbl_district_master as g on Organization_District=District_Code 
 inner join tbl_gst_invoice_master as f  on 
CASE WHEN f.Gst_Invoce_Category_Type='NCR_Rural'  AND a.amount='24000.00' THEN  f.Gst_Invoce_Category_Type= 'Rural'
 WHEN  f.Gst_Invoce_Category_Type='NCR_Urban'  AND a.amount='38000.00' THEN f.Gst_Invoce_Category_Type= 'Urban'
  WHEN f.Gst_Invoce_Category_Type='NCR_Rural_New'  AND a.amount='38000.00' THEN f.Gst_Invoce_Category_Type= 'Rural' 
 END   =c.Organization_AreaType where 
  um.User_PaperLess='Yes' AND um.User_UserRoll in (15,7) AND
   Date_Format(a.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(a.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY a.invoice_no ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function correction_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
b.amount,Date_Format(b.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name, District_Name,a.loglearnername,
 Gst_Invoce_TotalFee,Gst_Invoce_VMOU FROM tbl_correction_copy as a inner join 
tbl_correction_invoice as b on a.cid=b.transaction_ref_id inner join tbl_user_master as h on
 a.Correction_ITGK_Code=h.User_LoginId inner join tbl_organization_detail as c on h.User_Code=c.Organization_User inner join tbl_district_master as g on 
Organization_District=g.District_Code inner join tbl_gst_invoice_master as f  WHERE  Correction_Payment_Status='1' AND f.Gst_Invoce_Category_Type='Correction' AND Date_Format(b.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(b.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY b.invoice_no ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function eoi_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
b.amount,Date_Format(b.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name, District_Name,c.Organization_Name,
 Gst_Invoce_TotalFee,Gst_Invoce_VMOU FROM tbl_courseitgk_mapping as a inner join 
tbl_eoi_invoice as b on a.Courseitgk_ITGK=b.Itgkcode inner join tbl_user_master as h on
 a.Courseitgk_ITGK=h.User_LoginId inner join tbl_organization_detail as c on h.User_Code=c.Organization_User inner join tbl_district_master as g on 
Organization_District=g.District_Code inner join tbl_gst_invoice_master as f  
on a.Courseitgk_EOI=f.Gst_Invoce_Category_Id WHERE  EOI_Fee_Confirm='1' AND f.Gst_Invoce_Category_Type='EOI' AND Date_Format(b.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(b.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY b.invoice_no ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function name_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select b.invoice_no,
b.amount,Date_Format(b.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name, District_Name,c.Organization_Name FROM tbl_change_address_itgk as a inner join 
tbl_nameaddress_invoice as b on a.fld_ref_no=b.invoice_ref_id inner join tbl_user_master as h on
 a.fld_ITGK_Code=h.User_LoginId inner join tbl_organization_detail as c on h.User_Code=c.Organization_User inner join tbl_district_master as g on 
Organization_District=g.District_Code
 WHERE  a.fld_status='5' AND  Date_Format(b.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(b.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY b.invoice_no ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	    public function RenewalPenalty_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
b.amount,Date_Format(b.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name, District_Name,c.Organization_Name,
 Gst_Invoce_TotalFee,Gst_Invoce_VMOU FROM tbl_renewal_penalty as a inner join 
tbl_renewal_penalty_invoice as b on a.RenewalPenalty_ItgkCode=b.Itgkcode inner join tbl_user_master as h on
 a.RenewalPenalty_ItgkCode=h.User_LoginId inner join tbl_organization_detail as c on h.User_Code=c.Organization_User inner join tbl_district_master as g on 
Organization_District=g.District_Code inner join tbl_gst_invoice_master as f  
 WHERE  RenewalPenalty_Payment_Status='1' AND f.Gst_Invoce_Category_Type='CR' AND Date_Format(b.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(b.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY b.invoice_no ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function OwnerChange_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $startdate = mysqli_real_escape_string($_ObjConnection->Connect(),$startdate);
            $enddate = mysqli_real_escape_string($_ObjConnection->Connect(),$enddate);
            $_SelectQuery = "Select b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,Gst_Invoce_CGST,Gst_Invoce_SGST,
b.amount,Date_Format(b.addtime,'%d-%m-%Y') as Invoice_Date,c.Organization_Name, District_Name,c.Organization_Name,
 Gst_Invoce_TotalFee,Gst_Invoce_VMOU FROM tbl_ownership_change as a inner join 
tbl_ownershipchange_invoice as b on a.Org_Ack=b.Ao_Ack_No inner join tbl_user_master as h on
 a.Org_Itgk_Code=h.User_LoginId inner join tbl_organization_detail as c on h.User_Code=c.Organization_User inner join tbl_district_master as g on 
Organization_District=g.District_Code inner join tbl_gst_invoice_master as f  
 WHERE  Org_PayStatus='1' AND f.Gst_Invoce_Category_Type='OwnershipChange' AND Date_Format(b.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(b.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY b.invoice_no ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
