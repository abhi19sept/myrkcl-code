<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsFeePayment extends paymentFunctions {

    //put your code here

    public function GetAll($batch, $course, $paymode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {   
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$paymode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK = $_SESSION['User_LoginId'];
                $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_Course = '" . $course . "' AND Event_Batch = '" . $batch . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '3' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
                if ($_getEvent['Event_Payment'] == '1') {
                    $_SelectQuery = "SELECT a.*, bm.Batch_Name FROM tbl_admission as a inner join tbl_batch_master as bm on
									a.Admission_Batch=bm.Batch_Code WHERE Admission_Batch = '" . $batch . "' AND
									Admission_Course = '" . $course . "' And Admission_ITGK_Code='" . $_ITGK . "' And
									Admission_Payment_Status='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_getEvent['Event_Payment'] == '2') {
                    $_SelectQuery = "SELECT a.*, bm.Batch_Name FROM tbl_admission as a inner join tbl_batch_master as bm on
									a.Admission_Batch=bm.Batch_Code WHERE Admission_Batch = '" . $batch . "' AND 
									Admission_Course = '" . $course . "' And Admission_ITGK_Code='" . $_ITGK . "' And
									Admission_Payment_Status='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    //echo "Invalid User Input";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select RKCL_Share, Course_Fee, VMOU_Share FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }



    public function AddPayTran($postValues, $productinfo) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $postValues['amounts'], $postValues['AdmissionCodes'], $postValues['ddlCourse'], 0, $postValues['ddlBatch']);
            } else {
                session_destroy();
            ?>
                <script> window.location.href = "index.php";</script> 
            <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }
	
	public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}