<?php
/**
 * Description of clsWcdItgkDeclaration
 *
 * @author Mayank
 */
require 'DAL/classconnection.php';
include('DAL/smtp_class.php');
$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsWcdItgkDeclaration {
    //put your code here
	
public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master as a inner join
							tbl_wcd_itgk_declaration_master as b
							on a.Course_Code=b.wcd_itgk_declare_course_code where Course_Code in ('3','24') and 
							wcd_itgk_declare_eoi in ('32','33') and wcd_itgk_declare_itgkcode='".$_SESSION['User_LoginId']."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
							and Batch_Code>'293'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function verifyDetails($_Course,$_Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				
            $_SelectQuery = "Select * From tbl_wcd_itgk_declaration WHERE wcd_declare_course = '" . $_Course . "'
							and wcd_declare_batch='".$_Batch."' and wcd_declare_itgk='".$_SESSION['User_LoginId']."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetDetails($_Course,$_Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				
            $_SelectQuery = "select wcd_itgk_declare_itgkcode as Wcd_Intake_Center,
							wcd_itgk_declare_seat as Wcd_Base_Intake,ITGK_Name from
							tbl_wcd_itgk_declaration_master as a inner join vw_itgkname_distict_rsp as b on
							a.wcd_itgk_declare_itgkcode=b.ITGKCODE where
							wcd_itgk_declare_itgkcode='".$_SESSION['User_LoginId']."' and
							wcd_itgk_declare_course_code='" . $_Course . "' and
							wcd_itgk_declare_batch_code='".$_Batch."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function ADD($_Course,$_Batch){
	global $_ObjConnection;
        $_ObjConnection->Connect();
	try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_ITGK_Code = $_SESSION['User_LoginId'];
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
				
					if($_Course=='3'){
						$eoi_code='32';
					}else{
						$eoi_code='33';
					}
				$_InsertQuery = "Insert Into tbl_wcd_itgk_declaration(wcd_declare_itgk,wcd_declare_course,wcd_declare_batch,
									wcd_declare_eoi)
								values('".$_ITGK_Code."','".$_Course."','".$_Batch."','".$eoi_code."')";
				$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
}

}