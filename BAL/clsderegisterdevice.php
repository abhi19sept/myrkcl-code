<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsApplyEoi
 *
 *  author Viveks
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsderegisterdevice {

    //put your code here   

    public function GetDeviceDetails($code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
				
            $_SelectQuery = "SELECT * FROM tbl_biomatric_register WHERE BioMatric_ITGK_Code = '" . $code . "' AND BioMatric_Flag = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_CenterCode, $_Reason, $_serialno) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_Reason = mysqli_real_escape_string($_ObjConnection->Connect(),$_Reason);
				$_serialno = mysqli_real_escape_string($_ObjConnection->Connect(),$_serialno);
				
             $_UpdateQuery = "Update tbl_biomatric_register set BioMatric_Flag='0', BioMatric_Reason = '" . $_Reason . "', BioMatric_Upd_LoginId='" . $_SESSION['User_LoginId'] . "' "
                     . " Where BioMatric_ITGK_Code = '" . $_CenterCode . "' and BioMatric_SerailNo = '" . $_serialno . "' AND BioMatric_Flag = 1 ";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
