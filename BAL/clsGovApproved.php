<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

require 'DAL/sendsms.php';

class clsGovApproved {
    //put your code here
    
    public function GetAll($_CenterCode, $_lcode, $_eid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_lcode);
            $_eid = mysqli_real_escape_string($_ObjConnection->Connect(),$_eid);
			
				if(!empty($_CenterCode)) {
					 $_SelectQuery = "Select * FROM tbl_govempentryform WHERE Govemp_ITGK_Code = '" . $_CenterCode . "' AND trnpending = '3'";
					 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
				
			    elseif(!empty($_lcode)) {
					 $_SelectQuery = "Select * FROM tbl_govempentryform WHERE learnercode = '" . $_lcode . "' AND trnpending = '3'";
					 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
				elseif (!empty($_eid)) {
					 $_SelectQuery = "Select * FROM tbl_govempentryform WHERE empid = '" . $_eid . "' AND trnpending = '3'";
					 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
				else {
					echo "<span style='color:red !important;'>Please enter value in Atleast one field. </span>";
					return;
				}
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function GetDatabyCode($_LearnerCode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
            $_SelectQuery = "Select a.*,b.Batch_Name From tbl_govempentryform as a inner join tbl_batch_master as b
							 on a.batchid=b.Batch_Code	Where learnercode='" . $_LearnerCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetRegisteredDatabyCode($_LearnerCode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
            $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_Name,a.Admission_Fname,a.Admission_DOB,a.Admission_Mobile,b.Course_Name,c.Batch_Name
								From tbl_admission as a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code
								inner join tbl_batch_master as c on a.Admission_Batch=c.Batch_Code Where a.Admission_LearnerCode='" . $_LearnerCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

	 public function FILLAPPROVEDSTATUS() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT capprovalid, cstatus FROM tbl_capcategory where govrole='Yes' AND capprovalid in ('0','4')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GETALLLOT() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where govrole='Yes' AND active='Active'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function Update($_LearnerCode, $_ProcessStatus, $_IFSCNO, $_AccountNo, $_GPFNo, $_LOT, $_BatchId, $_Fee, $_Incentive, $_TotalAmt, $_Remark, $_Address, $_Mobile, $_DOB, $_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
			$_SelectQuery = "SELECT * FROM tbl_govempentryform where trnpending!='3' AND learnercode='" . $_LearnerCode . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			if($_Response1[0]=='Success'){
					echo "1";
					return;
				}
			else {
			
           $_UpdateQuery = "Update tbl_govempentryform set trnpending='" . $_ProcessStatus . "',"
                    . "lot='" . $_LOT . "', gifsccode='" . $_IFSCNO . "', empaccountno='" . $_AccountNo . "', empgpfno='" . $_GPFNo . "', Remarks='" . $_Remark . "',"
                    . "officeaddress='" . $_Address . "', deo='" . $_SESSION['User_LoginId'] . "', lmobileno='" . $_Mobile . "', empdob='" . $_DOB ."' Where learnercode='" . $_LearnerCode . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement); 
		   
				date_default_timezone_set('Asia/Kolkata');
				$_Date = date("Y-m-d h:i:s");	
				if($_Remark==''){
					$_Remarks = 'OK';
				}
				else {
					$_Remarks=$_Remark;
				}
				
				$_InsertQuery = "INSERT INTO tbl_govstatusupdatelog (Govemp_id, Govemp_ITGK_Code, Govemp_Learnercode, Govemp_Process_Status,Govemp_Process_Remark,"
				. "Govemp_Datetime) "
				. "Select Case When Max(Govemp_id) Is Null Then 1 Else Max(Govemp_id)+1 End as Govemp_id,"
				. "'" . $_ITGK . "' as Govemp_ITGK_Code,'" . $_LearnerCode . "' as Govemp_Learnercode,'" . $_ProcessStatus . "' as Govemp_Process_Status,'" . $_Remarks . "' as Govemp_Process_Remark,"
				. "'" . $_Date . "' as Govemp_Datetime"
				. " From tbl_govstatusupdatelog";
				$_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					
						if ($_ProcessStatus == '0') {
					$_Status = 'Processed by RKCL';
					$_SMS ="Your RS-CIT reimbursement Claim has been found eligible by RKCL. You will receive the amount only after your 
							claim is approved by DoIT&C, GoR. ";
					SendSMS($_Mobile, $_SMS);
				}
				else if($_ProcessStatus == '4') {
					$_Status = 'Rejected by RKCL';
					$_SMS = "Your RS-CIT Reimbursement Claim has been rejected by RKCL. For reason, log-on to myrkcl.com or contact your 
							ITGK/RS-CIT Center.";
					SendSMS($_Mobile, $_SMS);
						$_Reason="Your RS-CIT Reimbursement Claim Rejection Reason:" . $_Remarks;
						SendSMS($_Mobile, $_Reason);
				}
			}	
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function FILLStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where govrole='Yes' AND capprovalid='3'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLGovFee($_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
            $_SelectQuery = "SELECT Admission_Fee, Admission_Batch, Batch_Name FROM tbl_admission as a INNER JOIN tbl_batch_master as b on a.Admission_Batch = b.Batch_Code where Admission_LearnerCode='" . $_LearnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLGovAttempt($_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
            $_SelectQuery = "SELECT aattempt FROM tbl_govempentryform where learnercode='" . $_LearnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLGovFeeAttempted($_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
            $_SelectQuery = "SELECT fee,incentive,trnamount FROM tbl_govempentryform where learnercode='" . $_LearnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetPendingApplicationData() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
				 $_SelectQuery = "Select * FROM tbl_govempentryform WHERE trnpending = '3' order by applicationtype DESC, applicationdate ASC LIMIT 150";
				 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function ValidateLearnerDetails($_LearnerCode, $_AccountNo, $_Mobile, $_DOB) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
            $_AccountNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_AccountNo);
            $_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
			$_SelectQuery = "Select empaccountno,lmobileno FROM tbl_govempentryform WHERE (empaccountno='" . $_AccountNo . "' OR lmobileno='" . $_Mobile . "')
								AND learnercode!='" . $_LearnerCode . "' LIMIT 1";
			$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			if($_Response[0] == 'Success'){
				$row = mysqli_fetch_array($_Response[2]);
				    if($row['empaccountno'] == $_AccountNo) {
						echo "Duplicate Account No.";
						return;
					}
					else if($row['lmobileno'] == $_Mobile) {
						echo "Duplicate Mobile No.";
						return;
					}
			}
			else {
				return $_Response;
			}
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        
    }
	
	public function GetLearnerBatch($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_action = mysqli_real_escape_string($_ObjConnection->Connect(),$_action);
            $_SelectQuery = "Select b.Batch_Name,b.Batch_StartDate From tbl_admission a
                                left join tbl_batch_master b on a. Admission_Batch=b.Batch_Code 
                                Where a.Admission_LearnerCode='".$_action."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}