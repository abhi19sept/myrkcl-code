<?php
/**
 * Description of clsDivisionMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsDivisionMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Division_Code,Division_Name,"
                    . "Status_Name From tbl_division_master as a inner join tbl_status_master as b "
                    . "on a.Division_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Division_Code)
    {   //echo $_Division_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Division_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Division_Code);
				
            $_SelectQuery = "Select Division_Code,Division_Name,Division_Status"
                    . " From tbl_division_master Where Division_Code='" . $_Division_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Division_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Division_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Division_Code);
				
            $_DeleteQuery = "Delete From tbl_division_master Where Division_Code='" . $_Division_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_DivisionName,$_DivisionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DivisionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DivisionName);
				$_DivisionStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_DivisionStatus);
				
            $_InsertQuery = "Insert Into tbl_division_master(Division_Code,Division_Name,"
                    . "Division_Status) "
                    . "Select Case When Max(Division_Code) Is Null Then 1 Else Max(Division_Code)+1 End as Division_Code,"
                    . "'" . $_DivisionName . "' as Division_Name,"
                    . "'" . $_DivisionStatus . "' as Division_Status"
                    . " From tbl_division_master";
            $_DuplicateQuery = "Select * From tbl_division_master Where Division_Name='" . $_DivisionName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_DivisionCode,$_DivisionName,$_DivisionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DivisionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DivisionCode);
				$_DivisionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DivisionName);
				
            $_UpdateQuery = "Update tbl_division_master set Division_Name='" . $_DivisionName . "',"
                    . "Division_Status='" . $_DivisionStatus . "' Where Division_Code='" . $_DivisionCode . "'";
            $_DuplicateQuery = "Select * From tbl_division_master Where Division_Name='" . $_DivisionName . "' "
                    . "and Division_Code <> '" . $_DivisionCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
