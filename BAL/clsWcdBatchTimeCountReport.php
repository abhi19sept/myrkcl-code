<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

class clsWcdBatchTimeCountReport {

    //put your code here

public function GetAllCount($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if ($_LoginUserRole == '7') {
				//print_r($_SESSION);
				  $_SelectQuery = "select Oasis_Admission_Final_Preference,ITGKMOBILE,ITGK_Name,
								District_Name,Tehsil_Name,count(Oasis_Admission_LearnerCode) as Approved,
								sum(case when Oasis_Admission_ReportedStatus='Reported' then 1 else 0 end) as reported,
								sum(case when Batch_Time_time='08 AM' then 1 else 0 end) as eight,
								sum(case when Batch_Time_time='09 AM' then 1 else 0 end) as nine,
								sum(case when Batch_Time_time='10 AM' then 1 else 0 end) as ten,
								sum(case when Batch_Time_time='11 AM' then 1 else 0 end) as eleven,
								sum(case when Batch_Time_time='12 PM' then 1 else 0 end) as twelve,
								sum(case when Batch_Time_time='01 PM' then 1 else 0 end) as ones,
								sum(case when Batch_Time_time='02 PM' then 1 else 0 end) as two,
								sum(case when Batch_Time_time='03 PM' then 1 else 0 end) as three,
								sum(case when Batch_Time_time='04 PM' then 1 else 0 end) as four,
								sum(case when Batch_Time_time='05 PM' then 1 else 0 end) as five,
								sum(case when Batch_Time_time='06 PM' then 1 else 0 end) as six
								from tbl_oasis_admission as a left join tbl_wcd_allocate_batch_time as b
								on a.Oasis_Admission_LearnerCode=b.Batch_Time_lcode
								inner join vw_itgkname_distict_rsp as c on a.Oasis_Admission_Final_Preference=c.ITGKCODE
									WHERE Oasis_Admission_Course='".$_course."' and Oasis_Admission_Batch='".$_batch."'
								and Oasis_Admission_LearnerStatus='Approved' AND
								Oasis_Admission_Final_Preference='". $_SESSION['User_LoginId'] ."'";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

			}
			else if ($_LoginUserRole == '17') {
				//print_r($_SESSION);
				  $_SelectQuery = "select Oasis_Admission_Final_Preference,ITGKMOBILE,ITGK_Name,
								District_Name,Tehsil_Name,count(Oasis_Admission_LearnerCode) as Approved,
								sum(case when Oasis_Admission_ReportedStatus='Reported' then 1 else 0 end) as reported,
								sum(case when Batch_Time_time='08 AM' then 1 else 0 end) as eight,
								sum(case when Batch_Time_time='09 AM' then 1 else 0 end) as nine,
								sum(case when Batch_Time_time='10 AM' then 1 else 0 end) as ten,
								sum(case when Batch_Time_time='11 AM' then 1 else 0 end) as eleven,
								sum(case when Batch_Time_time='12 PM' then 1 else 0 end) as twelve,
								sum(case when Batch_Time_time='01 PM' then 1 else 0 end) as ones,
								sum(case when Batch_Time_time='02 PM' then 1 else 0 end) as two,
								sum(case when Batch_Time_time='03 PM' then 1 else 0 end) as three,
								sum(case when Batch_Time_time='04 PM' then 1 else 0 end) as four,
								sum(case when Batch_Time_time='05 PM' then 1 else 0 end) as five,
								sum(case when Batch_Time_time='06 PM' then 1 else 0 end) as six
								from tbl_oasis_admission as a left join tbl_wcd_allocate_batch_time as b
								on a.Oasis_Admission_LearnerCode=b.Batch_Time_lcode
								inner join vw_itgkname_distict_rsp as c on a.Oasis_Admission_Final_Preference=c.ITGKCODE
									WHERE Oasis_Admission_Course='".$_course."' and Oasis_Admission_Batch='".$_batch."'
								and Oasis_Admission_LearnerStatus='Approved' and
									District_Code='" . $_SESSION['Organization_District'] . "'
									group by Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_LoginUserRole == '14') {
                $_SelectQuery = "select Oasis_Admission_Final_Preference,ITGK_Name,ITGKMOBILE,
								District_Name,Tehsil_Name,count(Oasis_Admission_LearnerCode) as Approved,
								sum(case when Oasis_Admission_ReportedStatus='Reported' then 1 else 0 end) as reported,
								sum(case when Batch_Time_time='08 AM' then 1 else 0 end) as eight,
								sum(case when Batch_Time_time='09 AM' then 1 else 0 end) as nine,
								sum(case when Batch_Time_time='10 AM' then 1 else 0 end) as ten,
								sum(case when Batch_Time_time='11 AM' then 1 else 0 end) as eleven,
								sum(case when Batch_Time_time='12 PM' then 1 else 0 end) as twelve,
								sum(case when Batch_Time_time='01 PM' then 1 else 0 end) as ones,
								sum(case when Batch_Time_time='02 PM' then 1 else 0 end) as two,
								sum(case when Batch_Time_time='03 PM' then 1 else 0 end) as three,
								sum(case when Batch_Time_time='04 PM' then 1 else 0 end) as four,
								sum(case when Batch_Time_time='05 PM' then 1 else 0 end) as five,
								sum(case when Batch_Time_time='06 PM' then 1 else 0 end) as six
								from tbl_oasis_admission as a left join tbl_wcd_allocate_batch_time as b
								on a.Oasis_Admission_LearnerCode=b.Batch_Time_lcode
								inner join vw_itgkname_distict_rsp as c on a.Oasis_Admission_Final_Preference=c.ITGKCODE
								WHERE Oasis_Admission_Course='".$_course."' and Oasis_Admission_Batch='".$_batch."'
								and Oasis_Admission_LearnerStatus='Approved' and
								c.RSP_Code='" . $_SESSION['User_LoginId'] . "' group by Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
               } 
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	

public function GetAllCountForAdmin($_course, $_batch, $_district) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4'){
				 $_SelectQuery = "select Oasis_Admission_Final_Preference,ITGK_Name,ITGKMOBILE,
								District_Name,Tehsil_Name,count(Oasis_Admission_LearnerCode) as Approved,
								sum(case when Oasis_Admission_ReportedStatus='Reported' then 1 else 0 end) as reported,
								sum(case when Batch_Time_time='08 AM' then 1 else 0 end) as eight,
								sum(case when Batch_Time_time='09 AM' then 1 else 0 end) as nine,
								sum(case when Batch_Time_time='10 AM' then 1 else 0 end) as ten,
								sum(case when Batch_Time_time='11 AM' then 1 else 0 end) as eleven,
								sum(case when Batch_Time_time='12 PM' then 1 else 0 end) as twelve,
								sum(case when Batch_Time_time='01 PM' then 1 else 0 end) as ones,
								sum(case when Batch_Time_time='02 PM' then 1 else 0 end) as two,
								sum(case when Batch_Time_time='03 PM' then 1 else 0 end) as three,
								sum(case when Batch_Time_time='04 PM' then 1 else 0 end) as four,
								sum(case when Batch_Time_time='05 PM' then 1 else 0 end) as five,
								sum(case when Batch_Time_time='06 PM' then 1 else 0 end) as six
								from tbl_oasis_admission as a left join tbl_wcd_allocate_batch_time as b
								on a.Oasis_Admission_LearnerCode=b.Batch_Time_lcode
								inner join vw_itgkname_distict_rsp as c on a.Oasis_Admission_Final_Preference=c.ITGKCODE
								where Oasis_Admission_Course='".$_course."' and Oasis_Admission_Batch='".$_batch."'
								and Oasis_Admission_LearnerStatus='Approved' and District_Code='".$_district."'
								group by Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			
			
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
							order by Batch_Code DESC LIMIT 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetDistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select District_Code,District_Name from tbl_district_master where District_StateCode='29'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
