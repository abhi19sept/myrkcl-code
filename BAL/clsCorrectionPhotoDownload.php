<?php


/**
 * Description of clsCorrectionPhotoDownload
 *
 * @author 
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsCorrectionPhotoDownload {	
	
	public function GetDetails($_Lot)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Lot = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lot);
				
			$_SelectQuery = "select photo,attach1,attach2,lotname from tbl_correction_copy as a inner join tbl_clot as b
								on a.lot=b.lotid where lot='".$_Lot."'";			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function FILLCorrectionLot() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT distinct lotid, lotname FROM tbl_clot as a inner join tbl_correction_copy as b
								on a.lotid=b.lot order by lotid DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetLotname($lotid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lotid = mysqli_real_escape_string($_ObjConnection->Connect(),$lotid);
				
            $_SelectQuery = "SELECT lotname FROM tbl_clot where lotid='".$lotid."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
