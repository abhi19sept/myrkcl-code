<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clslearnerdetails {
	
	public function GetAll($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_LoginRole = $_SESSION['User_UserRoll'];
         if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11')
		 {
			 $_SelectQuery = "Select * from vw_learnerdetails where Admission_LearnerCode='".$_actionvalue."' ";
			 
		 }
		 else
		 {
			$_SelectQuery = "Select * from vw_learnerdetails where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
		 }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	 public function checkGovtLearnerStatus($_LearnerCode,$_ITGK) {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_LearnerCode."' AND Govemp_ITGK_Code='" . $_ITGK . "' ";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
             return $_Response;
        }
	
	
	
	
	
	public function GetAllMarks($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_LoginRole = $_SESSION['User_UserRoll'];
         if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11')
		 {
			 $_SelectQuery = "Select * from vw_learnerdetails where Admission_LearnerCode='".$_actionvalue."' ";
			 
		 }
		 else
		 {
		$_SelectQuery = "Select * from vw_learnerdetails where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
		
		}
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     /*for learner 7-4-2017*/
        public function GetAllLearner($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_LoginRole = $_SESSION['User_UserRoll'];
         if ($_LoginRole == '21' || $_LoginRole == '18' || $_LoginRole == '19' || $_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11')
		 {
			 //$_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' ";
                         
                         $_SelectQuery ="select a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
                                                    from tbl_admission a
                                                    left join tbl_course_master b on a.Admission_Course= b.Course_Code 
                                                    left join tbl_batch_master c on a.Admission_Batch = c.Batch_Code
                                                    left join tbl_result d on a.Admission_LearnerCode = d.scol_no
                                                    where a.Admission_LearnerCode='".$_actionvalue."'";
			 
		 }
		 else if ($_LoginRole == '14')
		 {
			//echo $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
                     
                        $_SelectQuery ="select a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
                                                    from tbl_admission a
                                                    left join tbl_course_master b on a.Admission_Course= b.Course_Code 
                                                    left join tbl_batch_master c on a.Admission_Batch = c.Batch_Code
                                                    left join tbl_result d on a.Admission_LearnerCode = d.scol_no
                                                    where a.Admission_LearnerCode='".$_actionvalue."' and a.Admission_RspName='".$_SESSION['User_Code']."'";
		 }
                 else
		 {
			//echo $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' and Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
                     
                        $_SelectQuery ="select a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
                                                    from tbl_admission a
                                                    left join tbl_course_master b on a.Admission_Course= b.Course_Code 
                                                    left join tbl_batch_master c on a.Admission_Batch = c.Batch_Code
                                                    left join tbl_result d on a.Admission_LearnerCode = d.scol_no
                                                    where a.Admission_LearnerCode='".$_actionvalue."' and a.Admission_ITGK_Code='".$_SESSION['User_LoginId']."'";
		 }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	 /*for learner 7-4-2017*/
	
	public function UserDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        $_SelectQuery = "Select *  from tbl_admission where lcode='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
    /*  Added By Sunil : 3-2-2017 for getting center Detail*/
    
        public function GetAllLearnerScore($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			         
            $_SelectQuery = "select * from tbl_learner_score where Learner_Code='".$_actionvalue."' and ITGK_code='".$_SESSION['User_LoginId']."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function GetLearnerAllMarks($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		$_LoginRole = $_SESSION['User_UserRoll'];
                $_SelectQuery = "select a.*, b.Event_Name 
                                        from tbl_result a
                                        left join tbl_events b 
                                        on a.exameventnameID= b.Event_Id 
                                        where a.scol_no ='".$_actionvalue."'";
		$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SPDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.* , b.*,c.* from tbl_admission a 
                            LEFT JOIN tbl_organization_detail b on a.Admission_RspName=b.Organization_User 
                            LEFT JOIN tbl_user_master c on a.Admission_RspName=c.User_Code
                            where a.Admission_LearnerCode='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function CenterDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.* , b.* 
                        from tbl_user_master a
                        LEFT JOIN tbl_organization_detail b
                        on a.User_Code=b.Organization_User
                        where a.User_UserRoll=7 and User_LoginId='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function checkLearnerCourse($_LearnerCode,$_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_LearnerCode=$_SESSION['User_LearnerCode'];
            //$_ITGK=$_SESSION['User_LoginId'];
            
            $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_LearnerCode."' AND Admission_ITGK_Code='" . $_ITGK . "' AND Admission_Course='4'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
            if($_Response[0]==Message::SuccessfullyFetch)
            {
                $_SelectQuery1 = "Select * From tbl_govempentryform Where learnercode='" . $_LearnerCode . "'";
                $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                 //echo "<pre>";print_r($_Response1);
                if($_Response1[0]==Message::SuccessfullyFetch)
                    {
                        return $_Response1;
                    }
                else{
                        return "NRED";
                    }
            }else{
                 $_Response[0] = Message::NoRecordFound;
                 $_Response[1] = Message::Error;
                 return $_Response;
            }
            
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
         
    }
    
        public function GetAllRDETAILS($_LearnerCode,$_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                    //$_ITGK=$_SESSION['User_LoginId'];
                    //$_action=$_SESSION['User_LearnerCode'];
            $_SelectQuery = "Select a.*,b.cstatus,c.lotname from tbl_govempentryform a
                                left join tbl_capcategory b on  a.trnpending = b.capprovalid
                                left join tbl_clot c on a.lot = c.lotid 
                                where a.learnercode='".$_LearnerCode."' AND a.Govemp_ITGK_Code='" . $_ITGK . "'";

            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function checkcorrectionStatus($_LearnerCode,$_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_LearnerCode=$_SESSION['User_LearnerCode'];
            //$_ITGK=$_SESSION['User_LoginId'];
            $_SelectQuery = "Select * from tbl_correction_copy where lcode='".$_LearnerCode."' AND Correction_ITGK_Code='" . $_ITGK . "' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function GetCorrectionDuplicateCertificate($_LearnerCode,$_ITGK) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                    //$_ITGK=$_SESSION['User_LoginId'];
                    //$_action=$_SESSION['User_LearnerCode'];
            $_SelectQuery = "Select a.*,b.cstatus from tbl_correction_copy a
                                left join tbl_capcategory b 
                                on  a.dispatchstatus = b.capprovalid 
                                where a.lcode='".$_LearnerCode."' AND a.Correction_ITGK_Code='" . $_ITGK . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
     public function checkEMPACKDETAILS($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		
            $_SelectQuery = "select * from tbl_govempentryform where learnercode='".$_action."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    /*  Added By Sunil : 3-2-2017 for getting center Detail*/

    //put your code here
    
}
