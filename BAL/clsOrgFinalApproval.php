<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgFinalApproval
 *
 * @author VIVEK
 */

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOrgFinalApproval {
    //put your code here
    
    public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				 $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				 
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code INNER JOIN tbl_intake_master as f "
                    . " ON e.User_LoginId=f.Intake_Center INNER JOIN tbl_staff_detail as g "
                    . " ON e.User_LoginId=g.Staff_User INNER JOIN tbl_bank_account as h "
                    . " ON e.User_LoginId=h.Bank_User_Code INNER JOIN tbl_it_peripherals as i "
                    . " ON e.User_LoginId=i.IT_UserCode INNER JOIN tbl_premises_details as j "
                    . " ON e.User_LoginId=j.Premises_User INNER JOIN tbl_userprofile as k "
                    . " ON e.User_Code=k.UserProfile_User"
                    . " WHERE e.User_LoginId = '" . $_CenterCode . "' AND e.User_UserRoll ='15'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code INNER JOIN tbl_intake_master as f "
                    . " ON e.User_LoginId=f.Intake_Center INNER JOIN tbl_staff_detail as g "
                    . " ON e.User_LoginId=g.Staff_User INNER JOIN tbl_bank_account as h "
                    . " ON e.User_LoginId=h.Bank_User_Code INNER JOIN tbl_it_peripherals as i "
                    . " ON e.User_LoginId=i.IT_UserCode INNER JOIN tbl_premises_details as j "
                    . " ON e.User_LoginId=j.Premises_User INNER JOIN tbl_userprofile as k "
                    . " ON e.User_Code=k.UserProfile_User"
                    . " WHERE e.User_Type = 'New'";
            } else {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Organization_User=e.User_Code "
//                    . " INNER JOIN tbl_intake_master as f "
//                    . " ON e.User_LoginId=f.Intake_Center INNER JOIN tbl_staff_detail as g "
//                    . " ON e.User_LoginId=g.Staff_User INNER JOIN tbl_bank_account as h "
//                    . " ON e.User_LoginId=h.Bank_User_Code INNER JOIN tbl_it_peripherals as i "
//                    . " ON e.User_LoginId=i.IT_UserCode INNER JOIN tbl_premises_details as j "
//                    . " ON e.User_LoginId=j.Premises_User INNER JOIN tbl_userprofile as k "
//                    . " ON e.User_Code=k.UserProfile_User"
                    . " WHERE e.User_Type = 'New' AND e.User_Rsp='" .$_SESSION['User_Code'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Upgrade($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
			$_Date = date("Y-m-d h:i:s");
            $_UpdateQuery = "Update tbl_user_master set User_UserRoll='7', User_FinalApprovalDate='" . $_Date . "'"
                    . "Where User_LoginId='" . $_CenterCode . "'";				
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
			$_Year = date("Y");
            $_PreviousYear = ($_Year - 1);
            $_DateOnly = date("Y-m-d");
            $_InsertQuery = "Insert Into tbl_eoi_centerlist"
                    . "(EOI_Code,EOI_ECL,EOI_Year,EOI_DateTime,EOI_Filename) values "                   
                    . "('1','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
                    . "('2','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
                    . "('9','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
                    . "('15','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
                    . "('13','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),";
					
            $_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
