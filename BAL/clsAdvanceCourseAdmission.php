<?php

/**
 * Description of clsAdvanceCourseAdmission
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdvanceCourseAdmission {

	public function GetAdvanceCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN 
			tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND 
			`CourseITGK_BlockStatus` = 'unblock' AND `Courseitgk_ITGK` = '" . $_SESSION['User_LoginId'] . "' AND CURDATE() >= 
			`CourseITGK_UserCreatedDate` AND CURDATE() <= `CourseITGK_ExpireDate` AND Course_Type='6'
			 ORDER BY `CourseITGK_UserCreatedDate` ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAdvanceCourseName($_CategoryCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CategoryCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CategoryCode);
            $_SelectQuery = "Select Ad_Course_Id,Ad_Course_Name from tbl_advancecourse_master where
							Ad_Course_Category='". $_CategoryCode . "' and Ad_Course_Status='Active'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	public function GetAdvanceCourseCategory($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectQuery = "Select Ad_Category_Code,Ad_Category_Name from tbl_advancecourse_category where
							Ad_Category_Course='". $_CourseCode . "' and Ad_Category_Status='Active'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }	

	public function FILLEventBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $_Course . "' AND CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='1'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }	
//put your code here
    public function Add($_LearnerName, $_LearnerCode, $_ParentName, $_IdProof, $_IdNo, $_DOB, $_MotherTongue, $_Medium, 
	$_Gender, $_MaritalStatus, $_District, $_Tehsil, $_Address, $_ResiPh, $_Mobile, $_Qualification, $_LearnerType, 
	$_PhysicallyChallenged, $_Email, $_PIN, $_LearnerCourse, $_LearnerBatch, $_LearnerCoursePkg, $_LearnerCategory, 
	$_LearnerFee, $_LearnerInstall, $_Photo, $_Sign) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_SMS = "Dear Learner, " . $_LearnerName . " Your Admission in Red Hat Advance Course has been Uploaded to RKCL Server. 
				Please ask your ITGK to Confirm your payment to RKCL. Please Note your Learner Code for further Communication
				with RKCL " . $_LearnerCode;
        try {
            $_LearnerName = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerName); 
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode); 
            $_ParentName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentName); 
            $_IdProof = mysqli_real_escape_string($_ObjConnection->Connect(),$_IdProof); 
            $_IdNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_IdNo); 
            $_DOB = mysqli_real_escape_string($_ObjConnection->Connect(),$_DOB); 
            $_MotherTongue = mysqli_real_escape_string($_ObjConnection->Connect(),$_MotherTongue); 
            $_Medium = mysqli_real_escape_string($_ObjConnection->Connect(),$_Medium); 
            $_Gender = mysqli_real_escape_string($_ObjConnection->Connect(),$_Gender); 
            $_MaritalStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_MaritalStatus); 
            $_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District); 
            $_Tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil); 
            $_Address = mysqli_real_escape_string($_ObjConnection->Connect(),$_Address); 
            $_ResiPh = mysqli_real_escape_string($_ObjConnection->Connect(),$_ResiPh); 
            $_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile); 
            $_Qualification = mysqli_real_escape_string($_ObjConnection->Connect(),$_Qualification); 
            $_LearnerType = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerType); 
            $_PhysicallyChallenged = mysqli_real_escape_string($_ObjConnection->Connect(),$_PhysicallyChallenged); 
            $_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email); 
            $_PIN = mysqli_real_escape_string($_ObjConnection->Connect(),$_PIN); 
            $_LearnerCourse = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCourse); 
            $_LearnerBatch = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerBatch); 
            $_LearnerFee = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerFee); 
            $_LearnerInstall = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerInstall); 
            $_Photo = mysqli_real_escape_string($_ObjConnection->Connect(),$_Photo); 
            $_Sign = mysqli_real_escape_string($_ObjConnection->Connect(),$_Sign);
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                    if (isset($_SESSION['User_Rsp']) && !empty($_SESSION['User_Rsp']) && $_SESSION['User_Rsp'] != '0') {
                        $_photoname = $_LearnerCode . '_photo' . '.png';
                        $_signname = $_LearnerCode . '_sign' . '.png';
                        $_ITGK_Code = $_SESSION['User_LoginId'];
                        $_User_Code = $_SESSION['User_Code'];
                        $_User_Rsp = $_SESSION['User_Rsp'];

                        $_InsertQuery = "Insert Into tbl_admission(Admission_Code,Admission_LearnerCode,"
                                . "Admission_ITGK_Code,Admission_Course,Admission_Batch,"
								. "Admission_Advance_CourseCode,Admission_Course_Category,"
                                . "Admission_Fee,Admission_Installation_Mode,Admission_Name,Admission_Fname,Admission_DOB,"
                                . "Admission_MTongue,Admission_Photo,Admission_Sign,Admission_Gender,"
                                . "Admission_MaritalStatus,Admission_Medium,Admission_PH,Admission_PID,"
                                . "Admission_UID,Admission_District,Admission_Tehsil,Admission_Address,"
                                . "Admission_PIN,Admission_Mobile,Admission_Phone,Admission_Email,User_Code,"
                                . "Admission_Qualification,Admission_Ltype,Admission_RspName) "
                                . "Select Case When Max(Admission_Code) Is Null Then 1 Else Max(Admission_Code)+1 End as Admission_Code,"
                                . "'" . $_LearnerCode . "' as Admission_LearnerCode,'" . $_ITGK_Code . "' as Admission_ITGK_Code,"
                                . "'" . $_LearnerCourse . "' as Admission_Course, '" . $_LearnerBatch . "' as Admission_Batch,"
								. "'" . $_LearnerCoursePkg . "' as Admission_Advance_CourseCode,"
								. "'" . $_LearnerCategory . "' as Admission_Course_Category,"
                                . "'" . $_LearnerFee . "' as Admission_Fee,'" . $_LearnerInstall . "' as 
								Admission_Installation_Mode,'" . $_LearnerName . "' as Admission_Name,"
								. "'" . $_ParentName . "' as Admission_Fname,"
                                . "'" . $_DOB . "' as Admission_DOB,'" . $_MotherTongue . "' as Admission_MTongue,'" . $_photoname . "' as Admission_Photo,'" . $_signname . "' as Admission_Sign,"
                                . "'" . $_Gender . "' as Admission_Gender,"
                                . "'" . $_MaritalStatus . "' as Admission_MaritalStatus,'" . $_Medium . "' as Admission_Medium,"
                                . "'" . $_PhysicallyChallenged . "' as Admission_PH,'" . $_IdProof . "' as Admission_PID,"
                                . "'" . $_IdNo . "' as Admission_UID,'" . $_District . "' as Admission_District,"
                                . "'" . $_Tehsil . "' as Admission_Tehsil,'" . $_Address . "' as Admission_Address,"
                                . "'" . $_PIN . "' as Admission_PIN,'" . $_Mobile . "' as Admission_Mobile,'" . $_ResiPh . "' as Admission_Phone,"
                                . "'" . $_Email . "' as Admission_Email,'" . $_User_Code . "' as User_Code,'" . $_Qualification . "' as Admission_Qualification,"
                                . "'" . $_LearnerType . "' as Admission_Ltype,'" . $_User_Rsp . "' as Admission_RspName"
                                . " From tbl_admission";


                        $_chkDuplicate_learnerCode = "Select Admission_LearnerCode From tbl_admission Where Admission_LearnerCode='" . $_LearnerCode . "'";
                        $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate_learnerCode, Message::SelectStatement);
                        if ($_Response2[0] == Message::NoRecordFound) {
                            $_DuplicateQuery = "Select * From tbl_admission Where Admission_Name='" . $_LearnerName . "' AND Admission_Fname = '" . $_ParentName . "' AND Admission_DOB = '" . $_DOB . "' AND Admission_Course = '" . $_LearnerCourse . "' AND Admission_Batch = '" . $_LearnerBatch . "' AND Admission_ITGK_Code = '" . $_ITGK_Code . "'";
                            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                            $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                                    . "Admission_Log_ITGK_Code,"
                                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                                    . "Select Case When Max(Admission_Log_Code) Is Null Then 1 Else Max(Admission_Log_Code)+1 End as Admission_Log_Code,"
                                    . "'" . $_ITGK_Code . "' as Admission_Log_ITGK_Code,"
                                    . "'" . $_photoname . "' as Admission_Log_Photo,'" . $_signname . "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"
                                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                                    . " From tbl_admission_log";
                            if ($_Response[0] == Message::NoRecordFound) {
                                $lphotodoc = '../upload/admission_photo/' . $_photoname;
                                $lsigndoc = '../upload/admission_sign/' . $_signname;


                                if (file_exists($lphotodoc) && file_exists($lsigndoc)) {
                                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                    $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
                                    SendSMS($_Mobile, $_SMS);
									
                                   /* if ($_LearnerCourse == "1" || $_LearnerCourse == "4") {

                                        if ($_LearnerCourse == "4") {
                                            $_GetRscitBatch = "Select max(Intake_Batch) as bc From tbl_intake_master Where Intake_Course='1'";
                                            $_Response3 = $_ObjConnection->ExecuteQuery($_GetRscitBatch, Message::SelectStatement);
                                            $_Row1 = mysqli_fetch_array($_Response3[2]);
                                             $_LearnerBatch=$_Row1['bc'];
                                        }
                                        $_UpdateQuery = "Update tbl_intake_master set Intake_Available=Intake_Available - 1 ,Intake_Consumed = Intake_Consumed + 1 Where Intake_Center = '" . $_ITGK_Code . "' AND Intake_Batch = '" . $_LearnerBatch . "' ";
                                        $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                                    }*/
                                } else {
                                    echo "Photo and Sign not attached successfully ,Please Re-Upload Photo-Sign";
                                    return;
                                }
                            } else {
                                $_Response[0] = Message::DuplicateRecord;
                                $_Response[1] = Message::Error;
                            }
                        } else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
                    } else {
                        session_destroy();
                        ?>
                        <script> window.location.href = "logout.php";</script> 
                        <?php

                    }
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerName = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerName);
            $_ParentName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ParentName);
            $_DOB = mysqli_real_escape_string($_ObjConnection->Connect(),$_DOB);
            $newpicture = mysqli_real_escape_string($_ObjConnection->Connect(),$newpicture);
            $newsign = mysqli_real_escape_string($_ObjConnection->Connect(),$newsign);
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
            
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_User_Code = $_SESSION['User_Code'];

            $_UpdateQuery = "Update tbl_admission set Admission_Name='" . $_LearnerName . "',"
                    . "Admission_Fname='" . $_ParentName . "'," . "Admission_DOB='" . $_DOB . "', " . "Admission_Photo='" . $newpicture . "',"
                    . "Admission_Sign='" . $newsign . "'"
                    . " Where Admission_Code='" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                    . "Admission_Log_ITGK_Code,"
                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                    . "'" . $_Code . "' as Admission_Log_Code,"
                    . "'" . $_ITGK_Code . "' as Admission_Log_ITGK_Code,"
                    . "'" . $newpicture . "' as Admission_Log_Photo,'" . $newsign . "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"
                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                    . " From tbl_admission_log";
            $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_AdmissionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AdmissionCode);
            $_SelectQuery = "Select * From tbl_admission Where Admission_Code='" . $_AdmissionCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCodeForUpdate($_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_AdmissionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AdmissionCode);
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_SelectQuery = "Select * From tbl_admission ad INNER JOIN tbl_event_management em ON em.Event_Batch = ad.Admission_Batch  Where ad.Admission_Code = '" . $_AdmissionCode . "' AND ad.Admission_ITGK_Code = '" . $_ITGK_Code . "' AND CURDATE() >= em.Event_Startdate AND CURDATE() <= em.Event_Enddate AND em.Event_Name='2'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select Course_Fee FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionInstall($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_SelectQuery = "Select Admission_Installation_Mode FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
// echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetIntakeAvailable($_Coursecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Coursecode);
				$_ITGK_Code = $_SESSION['User_LoginId'];
				$_SelectQuery3 = "Select a.Courseitgk_EOI,c.Plan_Admission_Count From tbl_courseitgk_mapping as a INNER JOIN
								  tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name inner join 
								  tbl_subscription_plan_master as c on a.Courseitgk_SubsPlan=c.Plan_Code
								  WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' AND
								  `Courseitgk_ITGK`= '" . $_ITGK_Code . "' AND CURDATE() >= `CourseITGK_UserCreatedDate`
								  AND CURDATE() <= `CourseITGK_ExpireDate` AND Course_Code='" . $_Coursecode . "'";
				$_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
				$_Row1 = mysqli_fetch_array($_Response2[2]);
				
				$_eoicode=$_Row1['Courseitgk_EOI'];
				$_admissioncount=$_Row1['Plan_Admission_Count'];

				$_SelectQuery = "Select count(Admission_LearnerCode) as lcode FROM tbl_admission WHERE Admission_Course = '" . $_Coursecode . "' AND Admission_ITGK_Code = '" . $_ITGK_Code . "'"; 
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				$_Row = mysqli_fetch_array($_Response[2]);
				$_adcount=$_Row['lcode'];				
				
				
				if($_admissioncount > $_adcount){
					echo $_adcount;
				}
				else {
					echo "10000";
				}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
