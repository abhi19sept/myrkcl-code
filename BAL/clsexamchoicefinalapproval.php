<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgFinalApproval
 *
 * @author yogendra
 */

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsexamchoicefinalapproval {
    //put your code here
    
    
	public function Upgrade($_Code,$_reasion) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
			if ($_SESSION['User_UserRoll'] == 1 )
			{
				
				   $_UpdateQuery = "Update tbl_examchoicemaster set status='Disapprove' , Reasion='".$_reasion."'
                    Where id='" . $_Code . "'  ";
				   $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				   return $_Response;
           
			}
			else
			{
				 $_UpdateQuery = "Update tbl_examchoicemaster set status='Disapprove' , Reasion='".$_reasion."'
                    Where id='" . $_Code . "' AND status='Pending' ";
					
				 
			}
			 $_DuplicateQuery = "Select * From tbl_examchoicemaster Where status='Approve' AND id='" . $_Code . "'";
           
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);		
			
            
            if($_Response[0]==Message::NoRecordFound)
            {
                //echo $_UpdateQuery;
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                //print_r($_Response);
            }
            else {
                $_Response[0] = 'Please contact to our technical support department to deny this request. <a href="mailto:support@rkcl.in">click here</a>';
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	
	public function Approve($_code) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
				
			if ($_SESSION['User_UserRoll'] == 1)
			{
            $_UpdateQuery = "Update tbl_examchoicemaster set status='Approve'
                     Where id='" . $_code. "'";
		    $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			return $_Response;
		    }
			else
			{
				$_UpdateQuery = "Update tbl_examchoicemaster set status='Approve'
                     Where id='" . $_code. "' AND  status='Pending'";
		        
			}
		    
            $_DuplicateQuery = "Select * From tbl_examchoicemaster Where status='Disapprove' AND id='" . $_code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
           
            if($_Response[0]==Message::NoRecordFound)
            {
                //echo $_UpdateQuery;
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                //print_r($_Response);
            }
            else {
                $_Response[0] = 'Please contact to our technical support department to deny this request. <a href="mailto:support@rkcl.in">click here</a>';
				
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
}
