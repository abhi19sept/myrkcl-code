<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsCourseAuthorization
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCourseAuthorization {
    //put your code here
    
   /** public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId, i.RSP_Name FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
					. " INNER JOIN vw_itgkname_distict_rsp as i ON a.Org_RspLoginId=i.RSP_Code "
                    . " WHERE e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment'";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                    . " WHERE e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' AND a.Org_RspLoginId='" . $_SESSION['User_LoginId'] . "'";
            } elseif ($_SESSION['User_UserRoll'] == 23) {
                $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId, i.RSP_Name FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                        . " INNER JOIN vw_itgkname_distict_rsp as i ON a.Org_RspLoginId=i.RSP_Code "
                    . " WHERE e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                        . "AND a.Organization_District='" . $_SESSION['Organization_District'] . "' AND Org_Course_Allocation_Status='Pending'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    } */
	
/**	public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId, i.RSP_Name FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                    . " INNER JOIN vw_itgkname_distict_rsp as i ON a.Org_RspLoginId=i.RSP_Code "
                    . " INNER JOIN tbl_ncr_visit as j ON e.User_LoginId=j.NCRVisit_Center_Code "
                    . " INNER JOIN tbl_ncr_feedback as k ON e.User_LoginId=k.NCRFeedback_AOCode "
                    . " WHERE a.Org_Visit='Yes' AND e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                    . " AND j.NCRVisit_UserRoll = '23'";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                    . " INNER JOIN tbl_ncr_visit as j ON e.User_LoginId=j.NCRVisit_Center_Code "
                    . " INNER JOIN tbl_ncr_feedback as k ON e.User_LoginId=k.NCRFeedback_AOCode "
                    . " WHERE a.Org_Visit='Yes' AND e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                    . " AND a.Org_RspLoginId='" . $_SESSION['User_LoginId'] . "' AND j.NCRVisit_UserRoll = '23'";
            } elseif ($_SESSION['User_UserRoll'] == 23) {
                $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId, i.RSP_Name FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                        . " INNER JOIN vw_itgkname_distict_rsp as i ON a.Org_RspLoginId=i.RSP_Code "
                    . " WHERE a.Org_Visit='Yes' AND e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                        . "AND a.Organization_District='" . $_SESSION['Organization_District'] . "' AND Org_Course_Allocation_Status='Pending'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    **/
	public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId, i.RSP_Name,MAX(l.ResExam_obtainmarks) as Marks FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                    . " INNER JOIN vw_itgkname_distict_rsp as i ON a.Org_RspLoginId=i.RSP_Code "
                    . " INNER JOIN tbl_ncr_visit as j ON e.User_LoginId=j.NCRVisit_Center_Code "
                    . " INNER JOIN tbl_ncr_feedback as k ON e.User_LoginId=k.NCRFeedback_AOCode "
                    . " INNER JOIN tbl_facultyexamresultreport as l ON e.User_LoginId=l.ITGK_Code "
                    . " WHERE a.Org_Visit='Yes' AND e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                    . " AND j.NCRVisit_UserRoll = '23' group by ITGK_code";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId, i.RSP_Name,MAX(l.ResExam_obtainmarks) as Marks FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                    . " INNER JOIN vw_itgkname_distict_rsp as i ON a.Org_RspLoginId=i.RSP_Code "
                    . " INNER JOIN tbl_ncr_visit as j ON e.User_LoginId=j.NCRVisit_Center_Code "
                    . " INNER JOIN tbl_ncr_feedback as k ON e.User_LoginId=k.NCRFeedback_AOCode "
                    . " INNER JOIN tbl_facultyexamresultreport as l ON e.User_LoginId=l.ITGK_Code "
                    . " WHERE a.Org_Visit='Yes' AND e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                    . " AND a.Org_RspLoginId='" . $_SESSION['User_LoginId'] . "' AND j.NCRVisit_UserRoll = '23' group by ITGK_code";
            } elseif ($_SESSION['User_UserRoll'] == 23) {
                $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.User_LoginId, i.RSP_Name,MAX(l.ResExam_obtainmarks) as Marks FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d ON a.Organization_Tehsil=d.Tehsil_Code "
                    . " INNER JOIN tbl_user_master as e ON a.Org_Ack=e.User_Ack "
                    . " INNER JOIN tbl_payment_transaction as f ON e.User_LoginId=f.Pay_Tran_ITGK "
                    . " INNER JOIN tbl_spcenter_agreement as g ON e.User_LoginId=g.SPCenter_CenterCode "
                    . " INNER JOIN tbl_ncrvisit_photo as h ON e.User_LoginId=h.NCRVisit_CenterCode "
                        . " INNER JOIN vw_itgkname_distict_rsp as i ON a.Org_RspLoginId=i.RSP_Code "
                        . " INNER JOIN tbl_facultyexamresultreport as l ON e.User_LoginId=l.ITGK_Code "
                    . " WHERE a.Org_Visit='Yes' AND e.User_UserRoll = '7' AND e.User_PaperLess = 'Yes' "
                    . " AND f.Pay_Tran_Status = 'PaymentReceive' AND f.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                        . "AND a.Organization_District='" . $_SESSION['Organization_District'] . "' AND Org_Course_Allocation_Status='Pending'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
       public function GetOrgDatabyCode($_OrgCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
				
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code WHERE Organization_Code = '" . $_OrgCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function CourseAllocation($_Ack, $_CenterCode, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_Ack = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ack);
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
				
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");            
            $_SMSITGK = "Dear ITGK, Your Course Allocation Process has been APPROVED on MYRKCL.";
			
            $_UpdateQuery = "Update tbl_user_master set User_FinalApprovalDate='" . $_Date . "'"
                    . "Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
            $_UpdateQuery1 = "Update tbl_org_master set Org_Course_Allocation_Status='Approved', Org_Approved_By='" . $_SESSION['User_LoginId'] . "', Org_NCR_Course_Allocation_Date='" . $_Date . "'"
                    . "Where Org_Ack='" . $_Ack . "'";
            
            $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            
            $_Year = date("Y");
            $_PreviousYear = ($_Year - 1);
            $_DateOnly = date("Y-m-d");
            $_InsertQuery = "Insert Into tbl_eoi_centerlist"
                    . "(EOI_Code,EOI_ECL,EOI_Year,EOI_DateTime,EOI_Filename) values "                   
                    . "('1','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
                    . "('2','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
                    . "('9','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR'),"
                    . "('30','" . $_CenterCode . "','" . $_PreviousYear . "','" . $_DateOnly . "','Thru_NCR')";
            $_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			$_SelectQuery = "select * from tbl_ncr_transaction where Ncr_Transaction_CenterCode='" . $_CenterCode . "' AND Ncr_Transaction_Status='Success'";
                        $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                        $_Row1 = mysqli_fetch_array($_Response2[2]);
            $_Amount = $_Row1['Ncr_Transaction_Amount'];
                        
                        $_InsertQuery1 = "Insert Into tbl_ncramount_master (NCRamount_ITGK,NCRamount_Amount) values 
                        ('" . $_CenterCode . "','" . $_Amount . "')";
            $_Response11=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
			 SendSMS($_Mobile, $_SMSITGK);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response1;
    }
	
	public function Reject($_Ack, $_CenterCode, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
			$_Ack = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ack);
			$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
			$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
			
            $_Date = date("Y-m-d h:i:s");
            $_SMSITGK = "Dear ITGK, Your Course Allocation Process has been REJECTED on MYRKCL.";
			
			 $_UpdateQuery = "Update tbl_user_master set User_FinalApprovalDate='" . $_Date . "'"
                    . "Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
            $_UpdateQuery = "Update tbl_org_master set Org_Course_Allocation_Status='Rejected', "
                    . "Org_Approved_By='" . $_SESSION['User_LoginId'] . "', Org_NCR_Course_Allocation_Date='" . $_Date . "'"
                    . "Where Org_Ack='" . $_Ack . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            SendSMS($_Mobile, $_SMSITGK);
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
    
    public function SHOWPAYENTDETAILS($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
                $_SelectQuery = "Select * from tbl_payment_transaction where Pay_Tran_ITGK ='" . $_CenterCode . "'"
                        . " AND Pay_Tran_Status = 'PaymentReceive' AND Pay_Tran_ProdInfo = 'NcrFeePayment'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetNcrPhotoData($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
                $_SelectQuery = "Select * from tbl_ncrvisit_photo where NCRVisit_CenterCode ='" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetSPCenteragreement($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
                $_SelectQuery = "Select * from tbl_spcenter_agreement where SPCenter_CenterCode ='" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetRspDetails($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_SelectQuery = "Select * from tbl_rspitgk_mapping WHERE Rspitgk_ItgkCode = '" . $_CenterCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
