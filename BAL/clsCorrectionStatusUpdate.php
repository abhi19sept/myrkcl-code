<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionStatusUpdate {
    //put your code here
    
    public function GetAll($_LCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LCode);
				
			$_SelectQuery = "Select * FROM tbl_correction_copy WHERE lcode = '" . $_LCode . "' AND dispatchstatus='0'
								AND lot!='0' AND Correction_Payment_Status='1' order by cid DESC"; 
			$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	public function UpdateLearnerProcessStatus($_Cid, $_Reason, $_Delv_Date) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			date_default_timezone_set("Asia/Kolkata");
			$processdate = date("Y-m-d H:i:s");
				$_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				
           $_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='14', remarks='" . $_Reason . "',
							form_delivered_date='".$_Delv_Date."' Where cid='" . $_Cid . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);			
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
}