<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsVisitPurposeMaster
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsVisitPurposeMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select VisitType_Code,VisitType_Name,"
                    . "Status_Name From tbl_visittype_master as a inner join tbl_status_master as b "
                    . "on a.VisitType_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
      public function GetAllActive() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select VisitType_Code,VisitType_Name,"
                    . "Status_Name From tbl_visittype_master as a inner join tbl_status_master as b "
                    . "on a.VisitType_Status=b.Status_Code where VisitType_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetDatabyCode($_VisitType_Code)
    {   //echo $_VisitType_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_VisitType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitType_Code);
				
            $_SelectQuery = "Select VisitType_Code,VisitType_Name,VisitType_Status"
                    . " From tbl_visittype_master Where VisitType_Code='" . $_VisitType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_VisitType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_VisitType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitType_Code);
				
            $_DeleteQuery = "Delete From tbl_visittype_master Where VisitType_Code='" . $_VisitType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_VisitTypeName,$_VisitTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_VisitTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitTypeName);
				$_VisitTypeStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitTypeStatus);
				
            $_InsertQuery = "Insert Into tbl_visittype_master(VisitType_Code,VisitType_Name,"
                    . "VisitType_Status) "
                    . "Select Case When Max(VisitType_Code) Is Null Then 1 Else Max(VisitType_Code)+1 End as VisitType_Code,"
                    . "'" . $_VisitTypeName . "' as VisitType_Name,"
                    . "'" . $_VisitTypeStatus . "' as VisitType_Status"
                    . " From tbl_visittype_master";
            $_DuplicateQuery = "Select * From tbl_visittype_master Where VisitType_Name='" . $_VisitTypeName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_VisitTypeCode,$_VisitTypeName,$_VisitTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_VisitTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitTypeCode);
				$_VisitTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitTypeName);
				$_VisitTypeStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitTypeStatus);
				
            $_UpdateQuery = "Update tbl_visittype_master set VisitType_Name='" . $_VisitTypeName . "',"
                    . "VisitType_Status='" . $_VisitTypeStatus . "' Where VisitType_Code='" . $_VisitTypeCode . "'";
            $_DuplicateQuery = "Select * From tbl_visittype_master Where VisitType_Name='" . $_VisitTypeName . "' "
                    . "and VisitType_Code <> '" . $_VisitTypeCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
