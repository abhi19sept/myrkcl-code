<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsAdmissionEnquirySummary {

    //put your code here
    public function EnquiryReport($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];


            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '28') {
             $_SelectQuery3 = "SELECT a.Admission_ITGK_Code AS RoleName, Count(b.AdmissionEnquiry_Lcode) AS uploadcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch, sum(case when a.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount FROM tbl_admission as a right join tbl_admission_enquiry as b on a.Admission_LearnerCode=b.AdmissionEnquiry_Lcode inner join tbl_batch_master as c on c.Batch_Code='" . $_batch . "' WHERE b.AdmissionEnquiry_Course = '" . $_course . "' and MONTH(b.AdmissionEnquiry_Timestamp) = MONTH(c.Timestamp) and YEAR(b.AdmissionEnquiry_Timestamp)=YEAR(c.Timestamp) group by AdmissionEnquiry_ITGK";
            }

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetDataEnquiry($_rolecode, $_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];


            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '28') {
             $_SelectQuery3 = "select a.AdmissionEnquiry_ITGK AS RoleName, a.AdmissionEnquiry_ITGK as RoleCode, 
count(a.AdmissionEnquiry_Code) as uploadcount, '".$_course."' AS Course, '".$_batch."' AS Batch from tbl_admission_enquiry as a inner join tbl_batch_master as b on 
b.Batch_Code='".$_batch."' where AdmissionEnquiry_Course='".$_course."' and 
 MONTH(AdmissionEnquiry_Timestamp) = MONTH(b.Timestamp)
 group by AdmissionEnquiry_ITGK";
            }
            elseif($_LoginRole == '14') {
               
            $_SelectQuery3 = "select a.AdmissionEnquiry_ITGK AS RoleName, a.AdmissionEnquiry_ITGK as RoleCode, 
count(a.AdmissionEnquiry_Code) as uploadcount, '".$_course."' AS Course, '".$_batch."' AS Batch from tbl_admission_enquiry as a  inner join tbl_user_master as b on a.AdmissionEnquiry_ITGK=b.User_LoginId inner join tbl_batch_master as c on 
c.Batch_Code='".$_batch."' where AdmissionEnquiry_Course='".$_course."'  AND b.User_Rsp='".$_SESSION['User_Code']."' AND b.User_UserRoll='7' and 
 MONTH(AdmissionEnquiry_Timestamp) = MONTH(c.Timestamp) 
 group by AdmissionEnquiry_ITGK";
            }

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetDataAll($_RoleName, $_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_UserRole = $_SESSION['User_UserRoll'];

            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '28') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } 
			elseif ($_LoginRole == '14') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "8";
            } 
			
			else {
                echo "hello";
            }

            $_SESSION['UserType'] = $_LoginUserType;

            if ($_loginflag == "1") {


					$_SelectQuery3 = "SELECT a.Admission_ITGK_Code AS RoleName, a.Admission_ITGK_Code AS RoleCode,
 Count(a.Admission_Code) AS convertcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch, sum(case when a.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount 
FROM tbl_admission as a inner join tbl_admission_enquiry as b on 
a.Admission_LearnerCode=b.AdmissionEnquiry_Lcode
 WHERE a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' and
a.Admission_ITGK_Code='".$_RoleName."'";
			} 
			elseif ($_loginflag == "8") 
			{
				
		    $_SelectQuery3 = "SELECT a.Admission_ITGK_Code AS RoleName, "
                        . "a.Admission_ITGK_Code AS RoleCode, Count(a.Admission_Code) "
                        . "AS convertcount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch,"
                        . "sum(case when a.Admission_Payment_Status = '1' then 1 else 0 end) confirmcount "
                        . "FROM tbl_admission as a inner join tbl_user_master as b on a.Admission_ITGK_Code=b.User_LoginId inner join tbl_admission_enquiry as c on 
                            a.Admission_LearnerCode=c.AdmissionEnquiry_Lcode WHERE
                             a.Admission_Course = '" . $_course . "' "
                        . "AND a.Admission_Batch = '" . $_batch . "' "
						. "AND b.User_UserRoll='7' AND a.Admission_RspName='".$_SESSION['User_Code']."' and a.Admission_ITGK_Code='".$_RoleName."'";
			 
            } 
			
			
			else {
                $_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                $_i = 0;
                $CenterCode2 = '';
                while ($_Row = mysqli_fetch_array($_Response[2])) {

                    $CenterCode2.=$_Row['Centercode'] . ",";

                    $_i = $_i + 1;
                }
                $CenterCode3 = rtrim($CenterCode2, ",");

                $_SESSION['AdmSummaryCenter'] = $CenterCode3;

                if ($CenterCode3) {

                    $_SelectQuery3 = "SELECT tbl_admission.Admission_ITGK_Code AS RoleName, tbl_admission.Admission_ITGK_Code AS RoleCode, Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM tbl_admission WHERE tbl_admission.Admission_ITGK_Code IN ($CenterCode3) AND tbl_admission.Admission_Course = '" . $_course . "' AND tbl_admission.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1' group by tbl_admission.Admission_ITGK_Code";
                }
            }

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerList($_course, $_batch, $_rolecode, $_mode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

		echo $_mode;
        try {

			if($_mode=="ShowUpload")
			{
				$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "'  ";
			}
			else if($_mode=="ShowConfirm")
            {
				$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Mobile,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1' ";
			}
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        
        try {

            //$_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";
			 $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,"
                    . "a.Admission_Fname,a.Admission_Photo,a.Admission_Sign, a.Admission_Payment_Status FROM tbl_admission AS a "
                    . "WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' "
                    . "AND a.Admission_Batch = '" . $_batch . "' ORDER BY a.Admission_Payment_Status DESC";
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
          
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedListITGK($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

             $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_ITGK_Code ='" . $_rolecode . "' AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function DetailedList($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

             $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name, d.District_Name, e.Tehsil_Name, f.Qualification_Name, g.LearnerType_Name FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code INNER JOIN tbl_district_master AS d ON a.Admission_District=d.District_Code INNER JOIN tbl_tehsil_master AS e ON a.Admission_Tehsil=e.Tehsil_Code INNER JOIN tbl_qualification_master as f ON a.Admission_Qualification=f.Qualification_Code INNER JOIN tbl_learnertype_master as g ON a.Admission_Ltype=g.LearnerType_Code  WHERE a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND Admission_Payment_Status = '1'";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_course_master where Course_Code in (1,4,5)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

           
           $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "'  ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
