<?php

    /**
     * Description of clsLearnerAttendance
     *
     * @author Mayank
     */
    require 'DAL/classconnectionNEW.php';

    $_ObjConnection = new _Connection();
    $_Response = array();
    $_Response3 = array();

    class clsLearnerAttendance
    {

        public function GetDataAll($_course, $_batch)
        {


            global $_ObjConnection;
            $_ObjConnection->Connect();
            $params = $columns = $totalRecords = $data = array();

            $params = $_REQUEST;

            //define index of column

            $where = $sqlTot = $sqlRec = "";

            // check search value exist
            if (!empty($params['search']['value'])) {
                $where .= " AND Admission_ITGK_Code LIKE '" . $params['search']['value'] . "%' ";
            } else {
                $where = "";
            }

            // getting total number records without any search
            try {
					$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					

                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll']
                    == 3 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
                    $_SelectQuery = "SELECT tbl_admission.Admission_ITGK_Code AS RoleName, tbl_admission.Admission_ITGK_Code AS RoleCode, Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM tbl_admission WHERE tbl_admission.Admission_Course = '" . $_course . "' AND tbl_admission.Admission_Batch = '" . $_batch . "' AND tbl_admission.Admission_Payment_Status = '1' ".$where." group by tbl_admission.Admission_ITGK_Code";
                }

                if ($_SESSION['User_UserRoll'] == 14) {
                    $_SelectQuery = "SELECT tbl_admission.Admission_ITGK_Code AS RoleName, tbl_admission.Admission_ITGK_Code AS RoleCode, Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM tbl_admission WHERE tbl_admission.Admission_Course = '" . $_course . "' AND tbl_admission.Admission_Batch = '" . $_batch . "' AND tbl_admission.Admission_Payment_Status = '1' AND tbl_admission.Admission_RspName='" . $_SESSION['User_Code'] . "' "
                        .$where." group by tbl_admission.Admission_ITGK_Code";
                }



                $sqlTot .= $_SelectQuery;
                $sqlRec .= $_SelectQuery;
                //concatenate search sql if value exist
                /*if (isset($where) && $where != '') {

                    $_SelectQuery .= $where;
                    $sqlRec .= $where;
                }*/


                $sqlRec .= "  LIMIT " . $params['start'] . " , 10 ";


                //echo $_SelectQuery;


                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                //echo "<pre>",print_r($_Response),"</pre>";


                $queryTot = $_Response;


                $totalRecords = mysqli_num_rows($_Response[2]);

                $queryRecords = $_ObjConnection->ExecuteQuery($sqlRec, Message::SelectStatement);

                //iterate on results row and create new index array of data
                $count = $params['start']+1;
                while ($row = mysqli_fetch_row($queryRecords[2])) {
                    $row[0] = $count;
                    $data[] = $row;
                    $count++;
                }

                $json_data = array(
                    "draw" => intval($params['draw']),
                    "recordsTotal" => intval($totalRecords),
                    "recordsFiltered" => intval($totalRecords),
                    "data" => $data   // total data array
                );

                //echo json_encode($json_data);  // send data as json format
                $_Response = $json_data;


            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
            return $_Response;
        }




        public function getSumAllData($_course, $_batch)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll']
                    == 3 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
                    $_SelectQuery3 = "SELECT Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM tbl_admission WHERE tbl_admission.Admission_Course = '" . $_course . "' AND tbl_admission.Admission_Batch = '" . $_batch . "' AND tbl_admission.Admission_Payment_Status = '1' group by tbl_admission.Admission_ITGK_Code";
                }

                if ($_SESSION['User_UserRoll'] == 14) {
                    $_SelectQuery3 = "SELECT Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM tbl_admission WHERE tbl_admission.Admission_Course = '" . $_course . "' AND tbl_admission.Admission_Batch = '" . $_batch . "' AND tbl_admission.Admission_Payment_Status = '1' AND tbl_admission.Admission_RspName='" . $_SESSION['User_Code'] . "'
					group by tbl_admission.Admission_ITGK_Code";
                }

                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;

            } catch (Exception $_ex) {
                $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response3[1] = Message::Error;
            }
            return $_Response3;
        }




        /* public function GetLearnerList($_course, $_batch, $_rolecode, $_startdate, $_enddate) {
             global $_ObjConnection;
             $_ObjConnection->Connect();
             try {
                 //$CenterCode3 = $_SESSION['AdmSummaryCenter'];
                 //$_LoginRole = $_SESSION['$_RoleFlterCode'];
                 //$_LoginType = $_SESSION['UserType'];

                 $sdate = date_format(date_create($_startdate),"Y-m-d").' 00:00:00';
                 $edate = date_format(date_create($_enddate),"Y-m-d").' 23:59:59';

                 $CenterCode3 = $_rolecode;
                 if ($CenterCode3) {

                   echo $_SelectQuery3 = "select Admission_ITGK_Code, Admission_LearnerCode,Admission_Name,Admission_Fname, COUNT(days) as Days, Attendance_Admission_Code, sum(Diff) AS
                                       Duration FROM (Select Admission_ITGK_Code, Admission_LearnerCode,Admission_Name,Admission_Fname, COUNT(DISTINCT DATE(Attendance_In_Time)) as days,
                                       Attendance_Admission_Code, TIMESTAMPDIFF(MINUTE , min(Attendance_In_Time),max(Attendance_In_Time)) AS Diff from tbl_learner_attendance AS A
                                       Right JOIN tbl_admission AS B ON b.Admission_LearnerCode = a.Attendance_Admission_Code WHERE Admission_ITGK_Code = '".  $CenterCode3 . "' AND
                                       Admission_Course = '" . $_course . "' AND Admission_Batch = '" . $_batch . "' AND Attendance_In_Time >='" . $sdate . "' AND
                                       Attendance_In_Time <= '" . $edate . "' group by Attendance_Admission_Code, DATE(Attendance_In_Time) ) AS t group by Attendance_Admission_Code";
                     $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                     return $_Response3;
                 }
             } catch (Exception $_ex) {
                 $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
                 $_Response3[1] = Message::Error;
             }
             return $_Response;
         }*/

        public function GetLearnerList($_course, $_batch, $_rolecode)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					$_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
					
                $CenterCode3 = $_rolecode;
                if ($CenterCode3) {
                    /*$_SelectQuery3 = "SELECT b.Admission_ITGK_Code, b.Admission_LearnerCode,b.Admission_Name,b.Admission_Fname, a.Days,a.Duration,
								b.BioMatric_Status FROM vw_learner_attendance AS A RIGHT JOIN tbl_admission AS B ON b.Admission_LearnerCode = a.Attendance_Admission_Code WHERE b.Admission_ITGK_Code = '" . $CenterCode3 . "' AND b.Admission_Payment_Status = '1' AND Admission_Course = '" . $_course . "'
								AND Admission_Batch = '" . $_batch . "'";*/

                    $_SelectQuery3 = "SELECT a.Attendance_ITGK_Code, a.Attendance_Admission_Code AS
                    Admission_LearnerCode, b.Admission_Name,b.Admission_Fname, b.BioMatric_Status,COUNT(DISTINCT date(Attendance_In_Time)) AS Days FROM tbl_learner_attendance a LEFT JOIN tbl_admission AS B ON b.Admission_LearnerCode = a.Attendance_Admission_Code WHERE b.Admission_ITGK_Code = '" . $_rolecode . "' AND b.Admission_Payment_Status = '1' AND b.Admission_Course = '" . $_course . "' AND b.Admission_Batch = '" . $_batch . "' GROUP BY a.Attendance_Admission_Code";

                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                    return $_Response3;
                }
            } catch (Exception $_ex) {
                $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response3[1] = Message::Error;
            }
            return $_Response3;
        }


        /*** GET USER ATTENDANCE DAYA ***/
        public function getUserAttendanceData($_course, $_batch)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					
                $_SelectQuery3 = "SELECT count(DISTINCT Attendance_Admission_Code) AS attendance_count, date(Attendance_In_Time) AS attendance_date FROM tbl_learner_attendance WHERE Attendance_ITGK_Code = '"
                    . $_SESSION['User_LoginId'] . "' AND Attendance_Admission_Code IN (SELECT Admission_LearnerCode FROM tbl_admission WHERE Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND BioMatric_Status = 1 AND Admission_Course = " . $_course . " AND Admission_Batch = " . $_batch . ") AND MONTH (Attendance_In_Time) = MONTH(CURRENT_DATE) GROUP BY DAY(Attendance_In_Time)";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;


            } catch (Exception $_ex) {
                $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response3[1] = Message::Error;
            }
            return $_Response3;
        }
		
		     public function GetAllLearnerAttendance($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				

            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 2 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 28) {
                $_SelectQuery3 = "select count(t) as attcount , Admission_LearnerCode,Admission_ITGK_Code,Attendance_Admission_Code,
                                                Attendance_ITGK_Code, Admission_Batch,Admission_Course,
                                                Admission_Name,Admission_Fname,BioMatric_Status from 
                                                (select distinct date(Attendance_In_Time) as t,Admission_LearnerCode,Admission_ITGK_Code,
                                                Attendance_Admission_Code,Attendance_ITGK_Code,
                                                Admission_Batch,Admission_Course,Admission_Name,Admission_Fname , BioMatric_Status
                                                from tbl_learner_attendance as a right join tbl_admission as b on a.Attendance_Admission_Code=
                                                b.Admission_LearnerCode where Admission_Payment_Status='1' 
                                                and Admission_Batch='" . $_batch . "' and Admission_Course='" . $_course . "') as tb group by Admission_LearnerCode";

            } else if ($_SESSION['User_UserRoll'] == 14) {
                $_SelectQuery3 = "select count(t) as attcount , Admission_LearnerCode,Admission_ITGK_Code,Attendance_Admission_Code,
                                                Attendance_ITGK_Code, Admission_Batch,Admission_Course,
                                                Admission_Name,Admission_Fname,BioMatric_Status from 
                                                (select distinct date(Attendance_In_Time) as t,Admission_LearnerCode,Admission_ITGK_Code,
                                                Attendance_Admission_Code,Attendance_ITGK_Code,
                                                Admission_Batch,Admission_Course,Admission_Name,Admission_Fname , BioMatric_Status
                                                from tbl_learner_attendance as a right join tbl_admission as b on a.Attendance_Admission_Code=
                                                b.Admission_LearnerCode where Admission_Payment_Status='1' and Admission_Batch='" . $_batch . "' and Admission_Course='" . $_course . "' 
                                            and Admission_RspName='" . $_SESSION['User_Code'] . "') as tb group by Admission_LearnerCode ";
                
            }else if ($_SESSION['User_UserRoll'] == 17) {
                               $_SelectQuery3 = "select count(t) as attcount , Admission_LearnerCode,Admission_ITGK_Code,Attendance_Admission_Code,Attendance_ITGK_Code, Admission_Batch,Admission_Course,
                                            Admission_Name,Admission_Fname,BioMatric_Status,District_Name , District_Code  from 
                                            (select distinct date(Attendance_In_Time) as t,Admission_LearnerCode,Admission_ITGK_Code,Attendance_Admission_Code,Attendance_ITGK_Code,
                                            Admission_Batch,Admission_Course,Admission_Name,Admission_Fname ,BioMatric_Status, District_Name, District_Code 
                                             from tbl_learner_attendance as a right join tbl_admission as b on a.Attendance_Admission_Code=
                                            b.Admission_LearnerCode inner join vw_itgkname_distict_rsp d on b.Admission_ITGK_Code=d.ITGKCODE  
                                             where Admission_Payment_Status='1' and Admission_Batch='" . $_batch . "' and Admission_Course='" . $_course . "' 
                                            and District_Code='" . $_SESSION['Organization_District'] . "' ) as tb group by Admission_LearnerCode";
            }
             $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
        } catch (Exception $_ex) {
            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
		
		
		

    }
