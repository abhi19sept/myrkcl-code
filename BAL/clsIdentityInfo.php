<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

class clsIdentityInfo {

    //put your code here

    public function Add($formData) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $values = unserialize($formData);

        $return = '<span class="small star">Unable to process, please check your all input details.</span>';
        $bhamasha_ID = ($values['bhamasha'] == 'bhamasha_id') ? $values['Bhamasha_ID'] : ($values['bhamashaappnum1'] . '-' . $values['bhamashaappnum2'] . '-' . $values['bhamashaappnum3']);
        if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 7 && (!empty($bhamasha_ID) && !empty($values['Name']) && !empty($values['F_Name']) && !empty($values['SSO_ID']) && !empty($values['DOB']) && !empty($values['esakhi_mobile']))) {
            $compareBy = date('d-m-Y');
            $diff = $this->calculateAge($values['DOB'], $compareBy);
            $years = $diff->y;
            $months = $diff->m;
            $days = $diff->d;
            $result = ($years < 16 || $years > 50 || ($years == 50 && ($months > 0 || $days > 0))) ? 0 : 1;
            if (!$result) {
                $return = '<span class="small star">Invalid age range, must be under 16 to 50 years.</span>';
            } else {
                $_SelectQuery = "SELECT * FROM tbl_identity_info WHERE sso_id = '" . $values['SSO_ID'] . "' OR esakhi_mobile = '" . $values['esakhi_mobile'] . "' LIMIT 1";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if (!mysqli_num_rows($_Response[2])) {
                    $values['app_application_id'] = (!$values['app_application_id']) ? 'NULL' : $values['app_application_id'];
                    $_InsertQuery = "INSERT IGNORE INTO tbl_identity_info (itgkcode, mobile, email, bhamasha_id, sso_id, name, fname, dob, esakhi_mobile, esakhi_email, app_application_id) (SELECT um.User_LoginId, um.User_MobileNo, um.User_EmailId, '" . $bhamasha_ID . "' AS bhamasha_id, '" . $values['SSO_ID'] . "' AS sso_id, '" . $values['Name'] . "' AS name, '" . $values['F_Name'] . "' AS fname, '" . $values['DOB'] . "' AS dob, '" . $values['esakhi_mobile'] . "' AS esakhi_mobile, '" . $values['esakhi_email'] . "' AS esakhi_email, " . $values['app_application_id'] . " AS app_application_id FROM tbl_user_master AS um INNER JOIN tbl_identity_master im ON um.User_LoginId = im.itgkcode WHERE um.User_LoginId = '" . $_SESSION['User_LoginId'] . "')";
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    if ($_Response[0] == Message::SuccessfullyInsert) {
                        $return = '<span class="small">Details has been saved successfully.</span>';
                    }
                } else {
                    $return = '<span class="small star">eSakhi mobile number, SSOID and App Application Id should be unique.</span>';
                }
            }
        }

        return $return;
    }

    public function ADDTrainee($formData) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $values = unserialize($formData);

        $return = '<span class="small star">Unable to process, please check your all input details.</span>';
        if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 7 && (!empty($values['ddleSakhi']) && !empty($values['TraineeName']) && !empty($values['TraineeSSO_ID']))) {
            $_InsertQuery = "INSERT IGNORE INTO tbl_identity_esakhi_trainee (itgkcode, esakhi_id, sso_id, name, mobile) VALUES ('" . $_SESSION['User_LoginId'] . "', '" . $values['ddleSakhi'] . "', '" . $values['TraineeSSO_ID'] . "', '" . $values['TraineeName'] . "', '" . $values['TraineeMobile'] . "')";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            if ($_Response[0] == Message::SuccessfullyInsert) {
                $return = '<span class="small">Trainee details has been saved successfully.</span>';
            }
        }

        return $return;
    }

    public function Remove($id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
        $query = "DELETE FROM tbl_identity_info WHERE id = '" . $id . "'";
        $_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
    }

    public function RemoveTrainee($id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
        $query = "DELETE FROM tbl_identity_esakhi_trainee WHERE id = '" . $id . "'";
        $_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
    }

    public function getInfo() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $return = ['itgkcode' => '0', 'isvalid' => '<script type="text/javascript">window.location.href="dashboard.php"</script>'];
        if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 7) {
            $valid = $this->isValidTrainer($_SESSION['User_LoginId']);
            if ($valid) $return['isvalid'] = '';
            $return['dbrow'] = $this->getItgkeSakhi($_SESSION['User_LoginId']);
        }

        return $return;
    }

    public function getTraineeInfo($esakhi_id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $esakhi_id = mysqli_real_escape_string($_ObjConnection->Connect(),$esakhi_id);
        $return = [];
        $filter = ($esakhi_id == 'all') ? '' : " AND tr.itgkcode = '" . $_SESSION['User_LoginId'] . "' AND tr.esakhi_id = '" . $esakhi_id . "'";
        $filter .= $this->getRspFilter();
        $_SelectQuery = "SELECT tr.*, ti.name AS eSakhi_Name FROM tbl_identity_esakhi_trainee tr INNER JOIN tbl_identity_info ti ON tr.esakhi_id = ti.id INNER JOIN tbl_user_master um ON um.User_LoginId = ti.itgkcode WHERE 2<3 $filter ORDER BY tr.itgkcode ASC, tr.name ASC";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while($row = mysqli_fetch_assoc($_Response[2])) {
                $return[] = $row;
            }
        }

        return $return;
    }

    public function getItgkeSakhi($itgkCode, $id = 0) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkCode);
        $id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
        $return = [];
        $filter = ($itgkCode != 'all') ? " AND itgkcode = '" . $itgkCode . "'" : '';
        $filter = ($id) ? " AND id = '" . $id . "'" : $filter;
        $filter .= $this->getRspFilter();
        $_SelectQuery = "SELECT * FROM tbl_identity_info im INNER JOIN tbl_user_master um ON um.User_LoginId = im.itgkcode WHERE 2 < 3 $filter ORDER BY name ASC";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while($row = mysqli_fetch_assoc($_Response[2])) {
                $return[] = $row;
            }
        }

        return $return;
    }

    public function isValidTrainer($itgkCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkCode);
        $return = 0;
        $_SelectQuery = "SELECT * FROM tbl_identity_master WHERE itgkcode = '" . $itgkCode . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            $return = 1;
        }

        return $return;
    }

    public function getRspFilter() {
        return (empty($_SESSION['User_UserRoll']) || $_SESSION['User_UserRoll'] == 14) ? " AND um.User_Rsp = '" . $_SESSION['User_Code'] . "'" : '';
    }

    public function getItgkReport() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $return = [];
        $filter = $this->getRspFilter();
        $_SelectQuery = "(SELECT '' AS id, '' AS itgkcode, '' AS itgkname, '' AS district, 'Total:' AS thesil, '' AS mobile, '' AS email, count(*) as n, 'all' as images_count FROM tbl_identity_info inf INNER JOIN tbl_identity_master im ON im.itgkcode = inf.itgkcode INNER JOIN tbl_user_master um ON um.User_LoginId = im.itgkcode  WHERE 2 < 3 $filter) UNION (SELECT im.id, im.itgkcode, im.itgkname, im.district, im.thesil, im.mobile, im.email, count(*) as n, images_count FROM tbl_identity_info inf INNER JOIN tbl_identity_master im ON im.itgkcode = inf.itgkcode INNER JOIN tbl_user_master um ON um.User_LoginId = im.itgkcode WHERE 2 < 3 $filter GROUP BY inf.itgkcode ORDER BY im.district, im.thesil, inf.itgkcode)";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while($row = mysqli_fetch_assoc($_Response[2])) {
                $return[] = $row;
            }
        }

        return $return;
    }

    public function addAttendance($postValues) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $return = 'Unable to process.';
        $updateQuery = "UPDATE tbl_identity_info SET attendance = '" . implode(',',$postValues['days']) . "', remark_attendance = '" . trim($postValues['attendance_remark']) . "', training_start_date = '" . $postValues['Training_Start_Date'] . "', training_attended = '1', training_completed = '" . $postValues['trainingStatus'] . "' WHERE id = '" . $postValues['att_id'] . "'";
        $_Response = $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
        if ($_Response[0] == Message::SuccessfullyUpdate) {
            $return = 'Attendance has been registred successfully.';
        }

        return $return;
    }

    public function calculateAge($date1,$date2) {
        // date2 should be greater then to date1
        $date1 = date_create($date1); //date of birth
        //creating a date object
        $date2 = date_create($date2); //current date 

        return date_diff($date2, $date1);
    }

    public function getItgkeSakhiTrainers($itgkCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkCode);
        $return = [];
        $filter = ($itgkCode == 'all') ? '' : " AND im.itgkcode = '" . $itgkCode . "'";
        $filter .= $this->getRspFilter();
        $_SelectQuery = "SELECT * FROM tbl_identity_trainer_master im INNER JOIN tbl_user_master um ON um.User_LoginId = im.itgkcode WHERE im.active = 1 AND im.trainer_name <> '' $filter ORDER BY im.itgkcode, im.trainer_name";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while($row = mysqli_fetch_assoc($_Response[2])) {
                $return[] = $row;
            }
        }

        return $return;
    }

    public function updateTrainerInfo($formValues) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $return = 'Unable to process.';
        if ($formValues['trainer_id']) {
            foreach ($formValues['trainer_id'] as $id) {
                $set = 'id = ' . $id;
                $set = (isset($formValues['ssoid' . $id]) && !empty($formValues['ssoid' . $id])) ? $set . ", ssoid = '" . $formValues['ssoid' . $id] . "'" : $set;
                $set = (isset($formValues['mobile' . $id])) ? $set . ", mobile = '" . $formValues['mobile' . $id] . "'" : $set;
                $set = (isset($formValues['email' . $id])) ? $set . ", email = '" . $formValues['email' . $id] . "'" : $set;
                $updateQuery = "UPDATE tbl_identity_trainer_master SET $set WHERE id = '" . $id . "'";
                $_Response = $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
                if ($_Response[0] == Message::SuccessfullyUpdate) {
                    $return = 'Trainer details has been saved successfully.';
                }
            }
        }

        return $return;

    }


    public function updateeSakhiInfo($formValues) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $return = 'Unable to process.';
        if ($formValues['esakhi_id']) {
            foreach ($formValues['esakhi_id'] as $id) {
                $set = 'id = ' . $id;
                $set = (isset($formValues['esakhi_mobile' . $id]) && !empty($formValues['esakhi_mobile' . $id])) ? $set . ", esakhi_mobile = '" . $formValues['esakhi_mobile' . $id] . "'" : $set;
                $set = (isset($formValues['esakhi_email' . $id]) && !empty($formValues['esakhi_email' . $id])) ? $set . ", esakhi_email = '" . $formValues['esakhi_email' . $id] . "'" : $set;
                $set = (isset($formValues['app_application_id' . $id]) && !empty($formValues['app_application_id' . $id])) ? $set . ", app_application_id = '" . $formValues['app_application_id' . $id] . "'" : $set;
                $updateQuery = "UPDATE tbl_identity_info SET $set WHERE id = '" . $id . "'";
                $_Response = $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
                if ($_Response[0] == Message::SuccessfullyUpdate) {
                    $return = 'eSakhi details has been updated successfully.';
                }
            }
        }

        return $return;

    } 

    public function updateITGKUploadStatus($itgkCode, $totalFiles) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $itgkCode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkCode);
        $totalFiles = mysqli_real_escape_string($_ObjConnection->Connect(),$totalFiles);
        $updateQuery = "UPDATE tbl_identity_master SET images_count = '" . ($totalFiles - 2) . "' WHERE itgkcode = '" . $itgkCode . "'";
        $_Response = $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
    }

    public function getRspItgkCodes() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $return = [];
        $filter = $this->getRspFilter();
        $_SelectQuery = "SELECT im.itgkcode FROM tbl_user_master um INNER JOIN tbl_identity_master im ON um.User_LoginId = im.itgkcode WHERE 2 < 3 $filter GROUP BY im.itgkcode ORDER BY im.itgkcode";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while($row = mysqli_fetch_assoc($_Response[2])) {
                $return[] = $row['itgkcode'];
            }
        }

        return $return;
    }

}

$clsIdInfo = new clsIdentityInfo();

if (isset($_Row1['ParentName']) && isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 7 && $_Row1['ParentName'] == 'Center Dashboard') {
    $valid = $clsIdInfo->isValidTrainer($_SESSION['User_LoginId']);
    if ($valid) {
        $_Menu .= '<li><a href="frm_esakhi_info.php">eSakhi Registration</a></li>';
    }
}