<?php
require 'DAL/classconnectionWebRKCL.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsEnquiryReport {
    
    public function ShowEnquiryReport($ddlstartdate,$ddlenddate)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$ddlstartdate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlstartdate);
				$ddlenddate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlenddate);
				
              $_SelectQuery = "select Enquiry_learnerName,Enquiry_learnerQualification,District_Name,Tehsil_Name,Enquiry_learnerMobile,Enquiry_learnerEmail,
                                Enquiry_timestamp,Enquiry_message from tbl_enquiry_learner as a inner join tbl_district_master as b
                                on a.Enquiry_learnerDistrict=b.District_Code inner join tbl_tehsil_master as c on a.Enquiry_learnerTehsil=c.Tehsil_Code 
                                where (Enquiry_timestamp BETWEEN '".$ddlstartdate."' AND '".$ddlenddate."')";
              $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function ShowCenterReport($ddlstartdate,$ddlenddate)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$ddlstartdate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlstartdate);
				$ddlenddate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlenddate);
				
            
              $_SelectQuery = "select Enquiry_centerName,District_Name,Enquiry_centerMobile,Enquiry_centerEmail,Enquiry_centercode,
                                Enquiry_timestamp,Enquiry_centermessage from tbl_enquiry_center as a inner join tbl_district_master as b
                                on a.Enquiry_centerDistrict=b.District_Code where (Enquiry_timestamp BETWEEN '".$ddlstartdate."' AND '".$ddlenddate."')";
              $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
}

?>