<?php

/*
 * @author SUNIL KUMAR BAINDARA

 */



require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCreateLogin {
    //put your code here
    public function GetAll($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select District_Code,District_Name,Region_name,State_Name, Status_Name,Country_Name From tbl_district_master as a inner join tbl_region_master as b on a.District_Region=b.Region_Code inner join tbl_state_master as c on b.Region_State=c.State_Code inner join tbl_status_master as d on a.District_Status=d.Status_Code inner join tbl_country_master as e on c.State_Country=e.Country_Code where District_StateCode='29' order by District_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetAllDistrict_Name() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
             $_SelectQuery = "Select District_Code, District_Name From tbl_district_master 
                            where District_StateCode='29' order by District_Name";
             $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
   
    public function Add($_Type,$_District_Code,$_District) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_SelectQuery = "Select District_Code, District_Name From tbl_district_master as a inner join tbl_region_master as b on a.District_Region=b.Region_Code inner join tbl_state_master as c on b.Region_State=c.State_Code inner join tbl_status_master as d on a.District_Status=d.Status_Code inner join tbl_country_master as e on c.State_Country=e.Country_Code where District_StateCode='29' order by District_Name limit 0,4";
            //$_SelectQuery = "Select District_Code, District_Name From tbl_district_master 
                            //where District_StateCode='29' order by District_Name ";
            //$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // echo "<pre>"; print_r($_Response);die;
            //while ($_Row007 = mysqli_fetch_array($_Response[2],true)) 
            //{
              // echo "<pre>"; print_r($_Row007);
               
            //$_District=$_Row007['District_Name'];
            
            //$_District_Code=$_Row007['District_Code'];//die;
            //echo "sunil";die; 
            $_Type='DPO';
            $username=strtoupper($_Type.''.$_District); 
            $_USERNAME=$username;
            $_PASSWORD=$username;
            $_Email = 'nareshk@rkcl.in';
            $_mobile = '9649900809';
            $_roll = '23';
            $_Status = '1';

            $_OrgName = strtoupper('ADMIN-'.$_Type.'-'.$_District);
            $_OrgRegno = '';
            $_OrgEstDate = date('d-m-y');
            $_OrgType = '1';
            $_doctype = 'NA';
            $_photoname =  'NA';
            $_State = '29';
            $_Region = '0';
            $_District = $_District_Code;
            $_Tehsil = '';
            $_Landmark = '';
            $_Road = '';
            $_Street = '';
            $_HouseNo = '';
            $_OrgCountry = '';
            
            $_DuplicateQuery = "Select * From tbl_user_master Where User_EmailId='" . $_Email . "' and User_LoginId='" . $_USERNAME . "' and User_UserRoll='" . $_roll . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                           
            
             $_InsertQuery = "Insert Into tbl_user_master(User_Code,User_EmailId,User_MobileNo,User_LoginId,User_Password,"
                    . " User_UserRoll,User_ParentId,User_Status)"
                    . "Select Case When Max(User_Code) Is Null Then 1 Else Max(User_Code)+1 End as User_Code,"
                    . "'" . $_Email . "' as User_EmailId,'" . $_mobile . "' as User_MobileNo,"
                    . "'" . $_USERNAME . "' as User_LoginId,'" . $_PASSWORD . "' as User_Password,"
                    . "'" . $_roll . "' as User_UserRoll,'1' as User_ParentId,"
                    . "'" . $_Status . "' as User_Status From tbl_user_master";
            //echo $_InsertQuery;
            $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            $_SelectQuery1 = "Select User_Code FROM tbl_user_master WHERE User_EmailId = '" . $_Email . "' AND User_LoginId='" . $_USERNAME . "' and User_UserRoll='" . $_roll . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row2 = mysqli_fetch_array($_Response5[2],true);
            //echo "<pre>"; print_r($_Row2);
            $_usercode = $_Row2['User_Code'];

            $_InsertQuery1 = "Insert Into  tbl_organization_detail(Organization_Code,Organization_User,Organization_Name,"
                    . "Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,"
                    . "Organization_DocType,Organization_ScanDoc,Organization_State,Organization_Region,Organization_District,"
                    . "Organization_Tehsil,Organization_Landmark,Organization_Road,Organization_Street,"
                    . "Organization_HouseNo,Organization_Country)"
                    . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                    . "'" . $_usercode . "' as Organization_User,'" . $_OrgName . "' as Organization_Name,'" . $_OrgRegno . "' as Organization_RegistrationNo,"
                    . "'" . $_OrgEstDate . "' as Organization_FoundedDate,  '" . $_OrgType . "' as Organization_Type,"
                    . "'" . $_doctype . "' as Organization_DocType,'" . $_photoname . "' as Organization_ScanDoc,"
                    . "'" . $_State . "' as Organization_State,'" . $_Region . "' as Organization_Region,'" . $_District . "' as Organization_District,"
                    . "'" . $_Tehsil . "' as Organization_Tehsil,"
                    . "'" . $_Landmark . "' as Organization_Landmark,'" . $_Road . "' as Organization_Road,"
                    . "'" . $_Street . "' as Organization_Street,'" . $_HouseNo . "' as Organization_HouseNo,"
                    . "'" . $_OrgCountry . "' as Organization_Country"
                    . " From  tbl_organization_detail";
            $_Response6 = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
            //return $_Response6;
            
          }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            } 
            
           // die;
          //} 
          
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function AddUserold($_roll,$username,$password,$_Email,$_mobile,$_txtRSP,$_OrgName,$_OrgType,$_OrgRegno,$_Landmark,$_Organization_PinCode,$_selState,$_selRegion,$_OrgEstDate,$_selDoctype,$_txtGenerateId,$_Organization_Course,$_HouseNo, $_Street,$_Road,$_OrgRegno,$_Organization_Area,$_Organization_Address,$_selCountry,$_selDistrict,$_selTehsil,$_Organization_Police, $_Organization_lat,$_Organization_long,$_Organization_PAN,$_txtEmpAddress,$_arearadio,$_selMuncitype,$_Organization_Municipal,$_txtWard,$_txtMohalla, $_selPanchayat,$_selGramPanchayat,$_selVillage) 
        {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_Type='DPO';
            //$username=strtoupper($_Type.''.$_District); 
            $_USERNAME=$username;
            $_PASSWORD=$password;
            //$_Email = 'nareshk@rkcl.in';
            //$_mobile = '9649900809';
            //$_roll = '23';
            $_Status = '1';

            $_OrgName = strtoupper('ADMIN-'.$_selDistrict);
            //$_OrgRegno = '';
            //$_OrgEstDate = date('d-m-y');
            $_OrgType = '1';
            $_doctype = $_selDoctype;
            $parentid = $_SESSION['User_LoginId'] . '_' . $_txtGenerateId. '_' . $_selDoctype. '.jpg';
            $_photoname =  $parentid;
            $_State = $_selState;
            $_Region = $_selRegion;
            $_District = $_selDistrict;
            $_Tehsil = $_selTehsil;
            $_OrgCountry = $_selCountry;
            
            $_DuplicateQuery = "Select * From tbl_user_master Where User_EmailId='" . $_Email . "' and User_LoginId='" . $_USERNAME . "' and User_UserRoll='" . $_roll . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                           
            
             $_InsertQuery = "Insert Into tbl_user_master(User_Code,User_EmailId,User_MobileNo,User_LoginId,User_Password,"
                    . " User_UserRoll,User_ParentId,User_Rsp,User_Status)"
                    . "Select Case When Max(User_Code) Is Null Then 1 Else Max(User_Code)+1 End as User_Code,"
                    . "'" . $_Email . "' as User_EmailId,"
                    . "'" . $_mobile . "' as User_MobileNo,"
                    . "'" . $_USERNAME . "' as User_LoginId,"
                    . "'" . $_PASSWORD . "' as User_Password,"
                    . "'" . $_roll . "' as User_UserRoll,"
                    . "'1' as User_ParentId,"
                    . "'" . $_Status . "' as User_Status,"
                    . "'" . $_txtRSP . "' as User_Status From tbl_user_master";
            //echo $_InsertQuery;
            $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            $_SelectQuery1 = "Select User_Code FROM tbl_user_master WHERE User_EmailId = '" . $_Email . "' AND User_LoginId='" . $_USERNAME . "' and User_UserRoll='" . $_roll . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row2 = mysqli_fetch_array($_Response5[2],true);
            //echo "<pre>"; print_r($_Row2);
            $_usercode = $_Row2['User_Code'];

            $_InsertQuery1 = "Insert Into  tbl_organization_detail(Organization_Code,Organization_User,Organization_Name,"
                    . "Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,"
                    . "Organization_DocType,Organization_ScanDoc,Organization_State,Organization_Region,Organization_District,"
                    . "Organization_Tehsil,Organization_Landmark,Organization_Road,Organization_Street,"
                    . "Organization_HouseNo,Organization_Country)"
                    . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                    . "'" . $_usercode . "' as Organization_User,'" . $_OrgName . "' as Organization_Name,'" . $_OrgRegno . "' as Organization_RegistrationNo,"
                    . "'" . $_OrgEstDate . "' as Organization_FoundedDate,  '" . $_OrgType . "' as Organization_Type,"
                    . "'" . $_doctype . "' as Organization_DocType,'" . $_photoname . "' as Organization_ScanDoc,"
                    . "'" . $_State . "' as Organization_State,'" . $_Region . "' as Organization_Region,'" . $_District . "' as Organization_District,"
                    . "'" . $_Tehsil . "' as Organization_Tehsil,"
                    . "'" . $_Landmark . "' as Organization_Landmark,'" . $_Road . "' as Organization_Road,"
                    . "'" . $_Street . "' as Organization_Street,'" . $_HouseNo . "' as Organization_HouseNo,"
                    . "'" . $_OrgCountry . "' as Organization_Country"
                    . " From  tbl_organization_detail";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
            //return $_Response6;
            
          }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
                //return $_Response;
            } 
            
           // die;
          //} 
          
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            //return $_Response;
            
        }
        return $_Response;
    }
    
    
    public function AddUSERMASTER($User_EmailId, $User_MobileNo, $User_LoginId, $User_Password, $User_UserRoll, $User_ParentId, $User_Rsp, $User_Status, $User_Type, $User_Ack, $User_CreatedDate)
            {
                global $_ObjConnection;
                $_ObjConnection->Connect();
                try {
                   
                    $_InsertQuery = "Insert Into tbl_user_master(   `User_Code`,                                               
                                                                        `User_EmailId`,                                                   
                                                                        `User_MobileNo`,                                                    
                                                                        `User_LoginId`,                                               
                                                                        `User_Password`,                                                   
                                                                        `User_UserRoll`,                                                    
                                                                        `User_ParentId`,                                            
                                                                        `User_Rsp`,                                           
                                                                        `User_Status`,                                             
                                                                        `User_Type`,                                             
                                                                        `User_Ack`,                                               
                                                                        `User_CreatedDate`)"
                    . "Select Case When Max(User_Code) Is Null Then 1 Else Max(User_Code)+1 End as User_Code,"
                    . "'" . $User_EmailId . "' as User_EmailId,"
                    . "'" . $User_MobileNo . "' as User_MobileNo,"
                    . "'" . $User_LoginId . "' as User_LoginId,"
                    . "'" . $User_Password . "' as User_Password,"
                    . "'" . $User_UserRoll . "' as User_UserRoll,"
                    . "'" . $User_ParentId . "' as User_ParentId,"
                    . "'" . $User_Rsp . "' as User_Rsp,"
                    . "'" . $User_Status . "' as User_Status,"
                    . "'" . $User_Type . "' as User_Type,"
                    . "'" . $User_Ack . "' as User_Ack,"
                    . "'" . $User_CreatedDate . "' as User_CreatedDate From tbl_user_master";

                    
                    $_DuplicateQuery = "Select * From tbl_user_master Where User_EmailId='" . $User_EmailId . "' and User_LoginId='" . $User_LoginId . "' and User_UserRoll='" . $User_UserRoll . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                    if ($_Response[0] == Message::NoRecordFound) {
                        $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                        
                        $_SelectQuery1 = "Select User_Code FROM tbl_user_master WHERE User_EmailId='" . $User_EmailId . "' and User_LoginId='" . $User_LoginId . "' and User_UserRoll='" . $User_UserRoll . "'";
                        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                        
                        
                    }
                    else {
                        $_Response[0] = Message::DuplicateRecord;
                        $_Response[1] = Message::Error;
                    }
                } catch (Exception $_e) {
                    $_Response[0] = $_e->getTraceAsString();
                    $_Response[1] = Message::Error;
                }
                return $_Response;
            }
    
    public function AddOrganization($Organization_User, $Organization_Name, $Organization_RegistrationNo, $Organization_FoundedDate, $Organization_Course, $Organization_Type, $Organization_DocType, $Organization_ScanDoc, $Organization_State, $Organization_Region, $Organization_District, $Organization_Tehsil, $Organization_Area, $Organization_Landmark, $Organization_Road, $Organization_Street, $Organization_HouseNo, $Organization_Country, $Organization_Address, $Organization_PAN, $Organization_AreaType, $Organization_Mohalla, $Organization_WardNo, $Organization_Police, $Organization_Municipal, $Organization_Municipal_Type, $Organization_Status, $Organization_Village, $Organization_Panchayat, $Organization_Gram, $Organization_PinCode, $Organization_lat, $Organization_long, $Org_formatted_address) 
            {
                global $_ObjConnection;
                $_ObjConnection->Connect();
                try {
                   
                   $_InsertQuery = "Insert Into tbl_organization_detail(`Organization_Code`,                                                      
                                                                        `Organization_User`,                                                   
                                                                        `Organization_Name`,                                                    
                                                                        `Organization_RegistrationNo`, 
                                                                        
                                                                        `Organization_FoundedDate`,                                                   
                                                                        `Organization_Course`,                                                    
                                                                        `Organization_Type`,                                            
                                                                        `Organization_DocType`, 
                                                                        
                                                                        `Organization_ScanDoc`,                                             
                                                                        `Organization_State`,                                             
                                                                        `Organization_Region`,                                               
                                                                        `Organization_District`, 
                                                                        
                                                                        `Organization_Tehsil`,                         
                                                                        `Organization_Area`,                                  
                                                                        `Organization_Landmark`,                                        
                                                                        `Organization_Road`,    
                                                                        
                                                                        `Organization_Street`,                                   
                                                                        `Organization_HouseNo`,                                            
                                                                        `Organization_Country`, 
                                                                        `Organization_Address`,
                                                                        
                                                                        `Organization_PAN`,                                           
                                                                        `Organization_AreaType`,                                             
                                                                        `Organization_Mohalla`,                                             
                                                                        `Organization_WardNo`,  
                                                                        
                                                                        `Organization_Police`,                                                  
                                                                        `Organization_Municipal`,                         
                                                                        `Organization_Municipal_Type`,                                  
                                                                        `Organization_Status`,   
                                                                        
                                                                        `Organization_Village`,                                             
                                                                        `Organization_Panchayat`,                                   
                                                                        `Organization_Gram`,                                            
                                                                        `Organization_PinCode`,
                                                                        
                                                                        `Organization_lat`,                                            
                                                                        `Organization_long`,                                            
                                                                        `Org_formatted_address`)"
                           
                    . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                    . "'" . $Organization_User . "' as Organization_User,"
                    . "'" . $Organization_Name . "' as Organization_Name," 
                    . "'" . $Organization_RegistrationNo . "' as Organization_RegistrationNo,"
                           
                    . "'" . $Organization_FoundedDate . "' as Organization_FoundedDate,"
                    . "'" . $Organization_Course . "' as Organization_Course,"
                    . "'" . $Organization_Type . "' as Organization_Type,"
                    . "'" . $Organization_DocType . "' as Organization_DocType,"
                           
                    . "'" . $Organization_ScanDoc . "' as Organization_ScanDoc,"
                    . "'" . $Organization_State . "' as Organization_State,"
                    . "'" . $Organization_Region . "' as Organization_Region,"
                    . "'" . $Organization_District . "' as Organization_District,"
                           
                    . "'" . $Organization_Tehsil . "' as Organization_Tehsil,"
                    . "'" . $Organization_Area . "' as Organization_Area,"
                    . "'" . $Organization_Landmark . "' as Organization_Landmark,"
                    . "'" . $Organization_Road . "' as Organization_Road,"
                           
                    . "'" . $Organization_Street . "' as Organization_Street,"
                    . "'" . $Organization_HouseNo . "' as Organization_HouseNo,"
                    . "'" . $Organization_Country . "' as Organization_Country,"
                    . "'" . $Organization_Address . "' as Organization_Address,"
                           
                    . "'" . $Organization_PAN . "' as Organization_PAN,"
                    . "'" . $Organization_AreaType . "' as Organization_AreaType,"
                    . "'" . $Organization_Mohalla . "' as Organization_Mohalla,"
                    . "'" . $Organization_WardNo . "' as Organization_WardNo,"
                           
                    . "'" . $Organization_Police . "' as Organization_Police,"
                    . "'" . $Organization_Municipal . "' as Organization_Municipal,"
                    . "'" . $Organization_Municipal_Type . "' as Organization_Municipal_Type,"
                    . "'" . $Organization_Status . "' as Organization_Status,"
                           
                    . "'" . $Organization_Village . "' as Organization_Village,"
                    . "'" . $Organization_Panchayat . "' as Organization_Panchayat,"
                    . "'" . $Organization_Gram . "' as Organization_Gram,"
                    . "'" . $Organization_PinCode . "' as Organization_PinCode,"
                           
                    . "'" . $Organization_lat . "' as Organization_lat,"
                    . "'" . $Organization_long . "' as Organization_long,"
                    . "'" . $Org_formatted_address . "' as Org_formatted_address"
                           
                    . " From  tbl_organization_detail";
                   
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                   
                } catch (Exception $_e) {
                    $_Response[0] = $_e->getTraceAsString();
                    $_Response[1] = Message::Error;
                }
                return $_Response;
            }
    
    
    public function AddLernerMob($_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = $_OTP." is the OTP for validating mobile number for User Login Process.";
        

        try {
            /* Here it checking that the OTP in which we are sending that is allredy in our database or not.*/
            $_SelectQuery = "Select * FROM tbl_ao_learner_register WHERE AO_Mobile = '" . $_Mobile . "' AND AO_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response1);
            if ($_Response1[0] == 'Success') 
                {
                $_Row = mysqli_fetch_array($_Response1[2]);
                //print_r($_Row);
                $old_OTP=$_Row['AO_OTP'];
                //$_OLD_SMS = "OTP for your Learner Login in MYRKCL Application is  " . $old_OTP;
                $_OLD_SMS = $old_OTP." is the OTP for validating mobile number for User Login Process.";

                SendSMS($_Mobile, $_OLD_SMS);
                return $_Response1;
            }
            else{
                $_InsertQuery = "Insert Into tbl_ao_learner_register(AO_Code,AO_Mobile,AO_OTP)"
                    . "Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,"
                    . "'" . $_Mobile . "' as AO_Mobile,'"
                    . "" . $_OTP . " as AO_OTP '"
                    . " From tbl_ao_learner_register";
                //echo $_InsertQuery;die;            
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
                return $_Response;
            }
            //die;
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    public function verifyLotp($_mobile, $_otp) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select * FROM tbl_ao_learner_register WHERE AO_Mobile = '" . $_mobile . "' AND AO_OTP = '" . $_otp . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response[0]);
            if ($_Response1[0] == Message::NoRecordFound) {
                //echo "Invalid OTP. Please Try Again";
                $er='InvalidOTP';
                return $er;
            } else {
                $_UpdateQuery = "Update tbl_ao_learner_register set AO_Status='1' WHERE AO_Mobile='" . $_mobile . "' AND AO_OTP = '" . $_otp . "' AND AO_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_ao_learner_register WHERE AO_Mobile='" . $_mobile . "' AND AO_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    
    
    public function Update($_Code,$_DistrictName,$_Region,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_district_master set District_Name='" . $_DistrictName . "',"
                    . "District_Region='" . $_Region . "',"
                    . "District_Status='" . $_Status . "' Where District_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_district_master Where District_Name='" . $_DistrictName . "' "
                    . "and District_Code <> '" . $_Code . "'";
           
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetAllEntity() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select entity_code, entity_name From tbl_entity_master where entity_status='1' order by entity_name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllUserRoll($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select UserRoll_Code,UserRoll_Name From tbl_userroll_master where UserRoll_Entity='".$region."' and UserRoll_Status='1' order by UserRoll_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    public function GetAllCountry($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Country_Code,Country_Name From tbl_country_master where Country_Status='1' order by Country_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllState($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select State_Code,State_Name From tbl_state_master where State_Country='".$region."' and State_Status='1' order by State_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllRegion($state) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Region_Code,Region_Name From tbl_region_master where Region_State='".$state."' and Region_Status='1' order by Region_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllDistrict($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select District_Code,District_Name From tbl_district_master where District_Region='".$region."' and District_Status='1' order by District_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllTehsil($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Tehsil_Code,Tehsil_Name From tbl_tehsil_master where Tehsil_District='".$district."' and Tehsil_Status='1' order by Tehsil_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllArea($tehsilid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Area_Code,Area_Name From tbl_area_master where Area_Tehsil='".$tehsilid."' and Area_Status='1' order by Area_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllMuncitype2() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Municipality_Type_Code, Municipality_Type_Name From tbl_municipality_type where  Municipality_Type_Status='1' order by Municipality_Type_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllMuncitype($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Municipality_Code, Municipality_Name From tbl_municipality_master where Municipality_District='".$district."' and Municipality_Status='1' order by Municipality_Name";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetMunicipalName($Municipalid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Municipality_Raj_Code From tbl_municipality_master where Municipality_Code='".$Municipalid."' and Municipality_Status='1' ";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetWardName($Municipalid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_SelectQuery = "Select Ward_Code,Ward_Name From tbl_ward_master where Ward_Municipality_Code='".$Municipalid."' and Ward_Status='1' ";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     public function GetAllPanchayat($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            echo $_SelectQuery = "Select Block_Code,Block_Name From tbl_panchayat_samiti Where Block_District='" . $_District_Code . "'  and Block_Status='1' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllGramPanchayat($_Panchayat_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select GP_Code,GP_Name From  tbl_gram_panchayat Where GP_Block='" . $_Panchayat_Code . "'  and GP_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllVillage($_GPanchayat_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Village_Code,Village_Name From  tbl_village_master Where Village_GP='" . $_GPanchayat_Code . "' and Village_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     public function GetAllORGType() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Org_Type_Code,Org_Type_Name,"
                    . "Status_Name From tbl_org_type_master as a inner join tbl_status_master as b "
                    . "on a.Org_Type_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
     public function GetAllRSP($_district)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
           $_SelectQuery = "Select Rsptarget_Code,Rsptarget_User From  tbl_rsptarget Where Rsptarget_District='" . $_district . "' and Rsptarget_Status='Approved' order by Rsptarget_User asc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     public function GetAllUserRSP($_loginid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
           $_SelectQuery = "Select User_Code From  tbl_user_master Where User_LoginId='" . $_loginid . "' and User_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
}
