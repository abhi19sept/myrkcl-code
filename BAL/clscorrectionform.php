<?php
//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/upload_ftp_doc.php';

$_ObjConnection = new _Connection();
$_ObjFTPConnection = new ftpConnection();
$_Response = array();

class clscorrectionform {

    public function GetItgkDistrict($_ITGK_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//$_ITGK_Code=$_SESSION['User_LoginId'];
			$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
			
            $_SelectQuery = "select District_Name, District_Code_VMOU from tbl_user_master as a inner join tbl_organization_detail as b
							on a.User_Code=b.Organization_User inner join tbl_district_master as c on 
							b.Organization_District=c.District_Code where a.User_LoginId='".$_ITGK_Code."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
     public function GetEmpdepartment() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select gdname From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
   
    public function GetEmpdesignation() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct designationname From tbl_designationname_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetBankdistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct District_Name From tbl_district_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function GetLearnerDistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct District_Name,District_Code From tbl_district_master where District_StateCode='29'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetBankname() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct bankname From tbl_rajbank_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetDobProof() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select cdocname From tbl_cdoccategory_master where active = 'Yes' AND type = 'govreimbursement'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
        
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_SelectQuery = "Select Status_Code,Status_Name,Status_Description From "
                    . "tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
    public function DeleteRecord($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_DeleteQuery = "Delete From tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
    public function Add($_LearnerCode, $_LearnerName, $_FatherName, $_LApplication, $_ExamName, $_TotalMarks, $_Email, $_MobileNo, 
						$_DOB, $_GENERATEDID, $_District, $_Address, $status, $_LearnerDistrict, $_LPincode, $dtstamp) {
        global $_ObjConnection;
		global $_ObjFTPConnection;
        $_ObjConnection->Connect();
        try {
			  
			$cphoto			=	$_LearnerCode .'_photo_'.$dtstamp.'.jpg';
			$cmarksheet		=	$_LearnerCode .'_marksheet_'.$dtstamp.'.jpg';	
			$ccertificate	=	$_LearnerCode .'_certificate_'.$dtstamp.'.jpg';
			$capplication	=	$_LearnerCode .'_application_'.$dtstamp.'.jpg';
			$cprovisional 	= 	$_LearnerCode .'_provisional_'.$dtstamp.'.jpg';
			$caffidavit 	= 	$_LearnerCode .'_affidavit_'.$dtstamp.'.jpg';
			
			$_ITGK_Code 	=   $_SESSION['User_LoginId'];
			$_User_Code 	=   $_SESSION['User_Code'];			
			
			if($_LApplication == 'Correction Certificate') {
				$_SelectQuery = "Select RKCL_Share From tbl_cdoccategory_master where applicationfor='".$_LApplication."' and 
									type='Correction' and active='Y'";
				$_Responsefee=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					if($_Responsefee[0]=='Success'){
						$row=mysqli_fetch_array($_Responsefee[2]);
						$fee=$row['RKCL_Share'];
					}
					else{
						$fee='200';
					}
					
            $_InsertQuery = "Insert Into tbl_correction_copy(lcode, cfname, cfaname, applicationfor, exameventname, totalmarks, emailid,
							mobile, dob, photo, attach1, attach2, attach5, userid, Correction_ITGK_Code, district_code,
							appliedby, application_type, learner_address, learner_district, learner_pincode, Correction_Fee ) 
							 VALUES ('" . $_LearnerCode . "', '" . $_LearnerName . "', '" . $_FatherName . "', '" . $_LApplication . "',
							 '" . $_ExamName . "', '" . $_TotalMarks . "', '" . $_Email . "', '" . $_MobileNo . "', 
							 '" . $_DOB . "', '" . $cphoto . "',  '" . $cmarksheet . "',  '" . $ccertificate . "', 
							 '" . $capplication . "', '" . $_User_Code . "', '" . $_ITGK_Code . "',
							 '" . $_District . "', '" . $_SESSION['User_UserRoll'] . "','" . $status . "','" . $_Address . "',
							 '" . $_LearnerDistrict . "','" . $_LPincode . "', '".$fee."')"; 
				
			 $ftpaddress = $_ObjFTPConnection->ftpPathIp();
			
			 $photodoc = $ftpaddress.'correction_photo/' . '/' . $cphoto;
			$marksheetdoc = $ftpaddress.'correction_marksheet/' . '/' . $cmarksheet;
			$certificatedoc = $ftpaddress.'correction_certificate/' . '/' . $ccertificate;
			$applicationdoc = $ftpaddress.'correction_application/' . '/' . $capplication;
                  
				$ch = 	curl_init($photodoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
								
				$ch = 	curl_init($marksheetdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeMarksheet = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
					
				$ch = 	curl_init($certificatedoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeCertificate = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
					
				$ch = 	curl_init($applicationdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeApplication = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
				
				if($responseCodePhoto == 200 && $responseCodeMarksheet == 200 && $responseCodeCertificate == 200 && 
					$responseCodeApplication == 200){
						$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
						$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Certificate Name Correction/Duplicate Certificate is Successfully Applied to RKCL";
						$_Mobile=$_MobileNo;
						SendSMS($_Mobile, $_SMS);
					}
					
				/*else if ( $responseCodeMarksheet == 200 && $responseCodeCertificate == 200 && $responseCodeApplication == 200 ) {
						$file = '../upload/admission_photo/'.$_LearnerCode .'_photo.png';
						$newfile = '../upload/correction_photo/'.$_LearnerCode .'_photo_'.$dtstamp.'.jpg';
						//$copied = copy($file , $newfile);
						if (! copy($file, $newfile)) {
							echo "Learner Photo Not Available.";
							return;
						}
						else{
							$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
							$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Certificate Name Correction/Duplicate Certificate is Successfully Applied to RKCL";
							$_Mobile=$_MobileNo;
							SendSMS($_Mobile, $_SMS);
						}
						
				   }*/
				else {
					echo "Please Re-Upload Documents Again.";
					return;
				} 			
		}
		else if($_LApplication == 'Duplicate Certificate'){
				$_SelectQuery = "Select RKCL_Share From tbl_cdoccategory_master where applicationfor='".$_LApplication."' and 
									type='Correction' and active='Y'";
				$_Responsefee=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					if($_Responsefee[0]=='Success'){
						$row=mysqli_fetch_array($_Responsefee[2]);
						$fee=$row['RKCL_Share'];
					}
					else{
						$fee='200';
					}
				
			$_InsertQuery = "Insert Into tbl_correction_copy(lcode, cfname, cfaname, applicationfor, exameventname, totalmarks,
									emailid, mobile, dob, photo, attach1, attach2, attach4, attach5, userid, Correction_ITGK_Code, 
									district_code, appliedby, application_type, learner_address, attach6, learner_district,
										learner_pincode, Correction_Fee) 
							 VALUES ('" . $_LearnerCode . "', '" . $_LearnerName . "', '" . $_FatherName . "', '" . $_LApplication . "',
							 '" . $_ExamName . "', '" . $_TotalMarks . "', '" . $_Email . "', '" . $_MobileNo . "', 
							 '" . $_DOB . "', '" . $cphoto . "',  '" . $cmarksheet . "',  '" . $ccertificate . "',
							 '" . $cprovisional . "', '" . $capplication . "', '" . $_User_Code . "','" . $_ITGK_Code . "',
							 '" . $_District . "','" . $_SESSION['User_UserRoll'] . "','" . $status . "','" . $_Address . "',
							 '" . $caffidavit . "','" . $_LearnerDistrict . "','" . $_LPincode . "', '".$fee."')";
					
					$ftpaddress = $_ObjFTPConnection->ftpPathIp();
				
				
			$photodoc = $ftpaddress.'correction_photo/' . '/' . $cphoto;
			$marksheetdoc = $ftpaddress.'correction_marksheet/' . '/' . $cmarksheet;
			$certificatedoc = $ftpaddress.'correction_certificate/' . '/' . $ccertificate;
			$applicationdoc = $ftpaddress.'correction_application/' . '/' . $capplication;
			$affidavitdoc = $ftpaddress.'correction_affidavit/' . '/' . $caffidavit;
			$provisionaldoc = $ftpaddress.'correction_provisional/' . '/' . $cprovisional;
					
				$ch = 	curl_init($photodoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
								
				$ch = 	curl_init($marksheetdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeMarksheet = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
					
				$ch = 	curl_init($certificatedoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeCertificate = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
					
				$ch = 	curl_init($applicationdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeApplication = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
				
				$ch = 	curl_init($affidavitdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeAffidavit = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
						
				$ch = 	curl_init($provisionaldoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeProvisional = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
				
				if($responseCodePhoto == 200 && $responseCodeMarksheet == 200 && $responseCodeCertificate == 200 && 
					$responseCodeApplication == 200 && $responseCodeAffidavit == 200 && $responseCodeProvisional == 200) {					
						$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
						$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Certificate Name Correction/Duplicate Certificate is Successfully Applied to RKCL";
						$_Mobile=$_MobileNo;
						SendSMS($_Mobile, $_SMS);
				   }
					else {
						echo "Please Re-Upload Documents Again.";
						return;
					} 
		}
		  else if($_LApplication == 'Duplicate Certificate with Correction'){
					$_SelectQuery = "Select RKCL_Share From tbl_cdoccategory_master where applicationfor='".$_LApplication."' and 
									type='Correction' and active='Y'";
				$_Responsefee=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					if($_Responsefee[0]=='Success'){
						$row=mysqli_fetch_array($_Responsefee[2]);
						$fee=$row['RKCL_Share'];
					}
					else{
						$fee='400';
					}
					
					 $_InsertQuery = "Insert Into tbl_correction_copy(lcode, cfname, cfaname, applicationfor, exameventname, totalmarks,
									emailid, mobile, dob, photo, attach1, attach2, attach4, attach5, userid, Correction_ITGK_Code, 
									district_code, appliedby, application_type, learner_address, attach6, learner_district,
									learner_pincode, Correction_Fee) 
							 VALUES ('" . $_LearnerCode . "', '" . $_LearnerName . "', '" . $_FatherName . "', '" . $_LApplication . "',
							 '" . $_ExamName . "', '" . $_TotalMarks . "', '" . $_Email . "', '" . $_MobileNo . "', 
							 '" . $_DOB . "', '" . $cphoto . "',  '" . $cmarksheet . "',  '" . $ccertificate . "',
							 '" . $cprovisional . "', '" . $capplication . "', '" . $_User_Code . "','" . $_ITGK_Code . "',
							 '" . $_District . "','" . $_SESSION['User_UserRoll'] . "','" . $status . "','" . $_Address . "',
							 '" . $caffidavit . "','" . $_LearnerDistrict . "','" . $_LPincode . "', '".$fee."')";
							 
					$ftpaddress = $_ObjFTPConnection->ftpPathIp();
			
			$photodoc = $ftpaddress.'correction_photo/' . '/' . $cphoto;
			$marksheetdoc = $ftpaddress.'correction_marksheet/' . '/' . $cmarksheet;
			$certificatedoc = $ftpaddress.'correction_certificate/' . '/' . $ccertificate;
			$applicationdoc = $ftpaddress.'correction_application/' . '/' . $capplication;
			 $affidavitdoc = $ftpaddress.'correction_affidavit/' . '/' . $caffidavit;
			 $provisionaldoc = $ftpaddress.'correction_provisional/' . '/' . $cprovisional;
					
				$ch = 	curl_init($photodoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
								
				$ch = 	curl_init($marksheetdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeMarksheet = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
					
				$ch = 	curl_init($certificatedoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeCertificate = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
					
				$ch = 	curl_init($applicationdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodeApplication = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
				
				$ch = 	curl_init($affidavitdoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						  $responseCodeAffidavit = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
						
				$ch = 	curl_init($provisionaldoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						  $responseCodeProvisional = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
						
						
					
				
				if($responseCodePhoto == 200 && $responseCodeMarksheet == 200 && $responseCodeCertificate == 200 && 
					$responseCodeApplication == 200 && $responseCodeAffidavit == 200 && $responseCodeProvisional == 200) {					
						$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
						$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Certificate Name Correction/Duplicate Certificate is Successfully Applied to RKCL";
						$_Mobile=$_MobileNo;
						SendSMS($_Mobile, $_SMS);
				   }
					else {
						echo "Please Re-Upload Documents Again.";
						return;
					} 
		  }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function Update($_Status_Code,$_StatusName, $_Status_Description) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				$_StatusName = mysqli_real_escape_string($_ObjConnection->Connect(),$_StatusName);
				
            $_UpdateQuery = "Update tbl_status_master set Status_Name='" . $_StatusName . "',"
                    . "Status_Description='" . $_Status_Description . "' "
                    . "Where Status_Code='" . $_Status_Code . "'";
            $_DuplicateQuery = "Select * From tbl_status_master Where Status_Name='" . $_StatusName . "' and "
                    . "Status_Code <> '" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
			else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;        
    }
    
	public function GetFillApplicationFor() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select * From tbl_cdoccategory_master where active = 'Y'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    } 
	
	public function GetExamEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select * From tbl_events where Event_Status = '1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
	
	public function FillEventById($_EventId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {       
				$_EventId = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventId);
				
            $_SelectQuery = "Select Event_Name_VMOU From tbl_events where Event_Id = '" . $_EventId . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }

	public function FillEventByLcode($_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {     
				$_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				
            $_SelectQuery = "select b.Event_Name_VMOU,a.tot_marks from tbl_result as a inner join  tbl_events as b on
								a.exameventnameID=b.Event_Id where a.scol_no='".$_Lcode."' and result like ('%Pass%') 
								order by id DESC LIMIT 1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    } 
	
	public function GetAllDETAILS($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK=$_SESSION['User_LoginId'];
			$_action = mysqli_real_escape_string($_ObjConnection->Connect(),$_action);
			
		    /*$_SelectQuery = "Select Admission_Name,Admission_Fname,Admission_Mobile,Admission_Email,Admission_Address,
							Admission_Batch,Admission_Photo,Admission_GPFNO,Admission_ITGK_Code
							from tbl_admission where Admission_LearnerCode='".$_action."' AND Admission_ITGK_Code='" . $_ITGK . "'";*/
			$_SelectQuery = "Select Admission_Name,Admission_Fname,Admission_Mobile,Admission_Email,Admission_Address,
							Admission_Batch,Admission_Photo,Admission_GPFNO,Admission_ITGK_Code,Admission_Course, bm.Batch_Name
							from tbl_admission as a inner join tbl_batch_master as bm on a.Admission_Batch=bm.Batch_Code
							where Admission_LearnerCode='".$_action."' AND Admission_ITGK_Code='" . $_ITGK . "'
							and Admission_Payment_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }	
	
	
	public function SentSms($mobileno)
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_Response = $this->SendOTPNEW($mobileno);
         } 
		 catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	   public function ResendOTP($mobileno)
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {                   
                $_Response = $this->ResendOTPNEW($mobileno);                
         } 
		 catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function GenerateOTP($length = 6, $chars = '1234567890') {
    $chars_length = (strlen($chars) - 1);
    $string = $chars{rand(0, $chars_length)};
    for ($i = 1; $i < $length; $i = strlen($string)) {
        $r = $chars{rand(0, $chars_length)};
        if ($r != $string{$i - 1})
            $string .= $r;
    }
    return $string;
}
	
	public function SendOTPNEW($mobileno = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_OTP = $this->GenerateOTP();

        try {
            $_Mobile = $mobileno;
            
            date_default_timezone_set('Asia/Kolkata');
            $_OTP_StartOn=date("Y-m-d h:i:s");
            $_OTP_EndOn=date("Y-m-d h:i:s", strtotime("+10 minutes"));

            $_InsertQuery = "Insert Into tbl_correction_otp(Correction_Otp_Code,Correction_Otp_Mobile,
								Correction_Otp_OTP,OTP_StartOn,OTP_EndOn) 
				Select Case When Max(Correction_Otp_Code) Is Null Then 1 Else Max(Correction_Otp_Code)+1 End as Correction_Otp_Code, 
				'" . $_Mobile . "' as Correction_Otp_Mobile, 
				'" . $_OTP . "' as Correction_Otp_OTP, 
				'" . $_OTP_StartOn . "' as OTP_StartOn,
				'" . $_OTP_EndOn . "' as OTP_EndOn 
            From tbl_correction_otp";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_correction_otp Where Correction_Otp_Mobile='" . $_Mobile . "' AND
								Correction_Otp_Status = 0";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            }
            else
            {
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                $Correction_Otp_Code=$_row['Correction_Otp_Code'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d h:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);
                if($datetime1 > $datetime2)
                {
                     //echo 'IF NOT EXPIRE';die;
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
                else
                {   
                    //echo 'IF EXPIRE';die;
                    $_UpdateQuery = "Update tbl_correction_otp set Correction_Otp_Status='1' WHERE 
										Correction_Otp_Code='" . $Correction_Otp_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }
	
	public function ResendOTPNEW($mobileno = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_OTP = $this->GenerateOTP();

        try {            
            $_Mobile = $mobileno;
            
            date_default_timezone_set('Asia/Kolkata');
            $_OTP_StartOn=date("Y-m-d h:i:s");
            $_OTP_EndOn=date("Y-m-d h:i:s", strtotime("+10 minutes"));
           
            $_InsertQuery = "Insert Into tbl_correction_otp(Correction_Otp_Code,Correction_Otp_Mobile,
								Correction_Otp_OTP,OTP_StartOn,OTP_EndOn) 
				Select Case When Max(Correction_Otp_Code) Is Null Then 1 Else Max(Correction_Otp_Code)+1 End as Correction_Otp_Code, 
				'" . $_Mobile . "' as Correction_Otp_Mobile, 
				'" . $_OTP . "' as Correction_Otp_OTP, 
				'" . $_OTP_StartOn . "' as OTP_StartOn,
				'" . $_OTP_EndOn . "' as OTP_EndOn 
            From tbl_correction_otp";
            $_DuplicateQuery = "Select * From tbl_correction_otp Where Correction_Otp_Mobile='" . $_Mobile . "' 
									AND Correction_Otp_Status = 0";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            }
            else
            {
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                $Correction_Otp_Code=$_row['Correction_Otp_Code'];
                //echo "<br>";
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d h:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);
                if($datetime1 > $datetime2)
                {
                     //echo 'IF NOT EXPIRE';die;
                    $_UpdateQuery = "Update tbl_correction_otp set Correction_Otp_Status='1' WHERE 
										Correction_Otp_Code='" . $Correction_Otp_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
                else
                {   
                    //echo 'IF EXPIRE';die;
                    $_UpdateQuery = "Update tbl_correction_otp set Correction_Otp_Status='1' 
										WHERE Correction_Otp_Code='" . $Correction_Otp_Code . "'";
                    $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                }
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }
	
	 public function VerifyOTPNEW($oid, $otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_correction_otp WHERE Correction_Otp_Code='" . $oid . "' AND 
								Correction_Otp_OTP = '" . $otp . "' AND Correction_Otp_Status = '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if($_Response[0]=='Success'){
                $_row = mysqli_fetch_array($_Response[2], true);
                $OTP_EndOn=$_row['OTP_EndOn'];
                date_default_timezone_set('Asia/Kolkata');
                $_CurrentTime=date("Y-m-d h:i:s");

                $datetime1 = new DateTime($OTP_EndOn);
                $datetime2 = new DateTime($_CurrentTime);


                if($datetime1 > $datetime2)
                {
                    if ($_Response[0] != Message::NoRecordFound) {
                        $_UpdateQuery = "Update tbl_correction_otp set Correction_Otp_Status='1' WHERE 
											Correction_Otp_Code='" . $oid . "'";
                        $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    }
                }
                else
                {   
                    $_Response[0]='EXPIRE';
                }
            }
            else
                {   
                    $_Response[0]='NORECORD';
                }
            
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function getVeriFyOTPForm($id, $_Mobile) {
        return '
        <div class="locksmall"><img src="images/lock_icon_small.png"></div>
                            <div class="panelforgot">Correction/Duplicate Certificate Application </div>
                         
        <div class="validate"><b>Validate OTP: (One time password)</b></div>
            <div class="goto"><a href="frmcorrection.php">Go Back</a></div>
            <div style="clear: both;"></div>
            <hr>
        <div style="margin-left:25px;"><span style="color: green;">Dear Learner a one time password has been sent to your 
			registered Mobile No. '.$_Mobile.'.</span><p></p></div>
            <div id="response" style="padding-left: 23px;"></div>  
            <div style="float:left; margin-left:24px;"><span>Please Enter OTP to Verify Your Name.</span><p></p></div>
        <div class="col-lg-12 col-md-12"><input maxlength="8" type="text" id="txtOTP" name="txtOTP" class="inputsearch" onkeypress="javascript:return allownumbers(event);" style="margin-left:8px;" Placeholder="Enter OTP"/>
        <input type="hidden" value="' . $id . '" name="oid" id="oid">
		</div>';
    }
	
}
?>