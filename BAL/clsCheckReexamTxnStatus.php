<?php

/*
 * @author Mayank

 */

 ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCheckreexamTxnStatus {
    //put your code here
    
	public function ValidateTxnId($_Txnid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Txnid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Txnid);
			 $_ITGKCODE = $_SESSION['User_LoginId']; 
			 $_LoginUserRole = $_SESSION['User_UserRoll'];
					if($_LoginUserRole == '1') {
						$_SelectQuery = "Select * From tbl_payment_transaction Where (Pay_Tran_Status='PaymentInProcess' OR Pay_Tran_Status='PaymentFailure') AND Pay_Tran_ProdInfo='ReexamPayment' AND Pay_Tran_Reconcile_status='Not Reconciled' AND Pay_Tran_PG_Trnid='" . $_Txnid . "'";
					}
					 else if($_LoginUserRole == '7') {
						 $_SelectQuery = "Select * From tbl_payment_transaction Where (Pay_Tran_Status='PaymentInProcess' OR Pay_Tran_Status='PaymentFailure') AND Pay_Tran_ProdInfo='ReexamPayment' AND Pay_Tran_Reconcile_status='Not Reconciled' AND Pay_Tran_PG_Trnid='" . $_Txnid . "' AND Pay_Tran_ITGK='" . $_ITGKCODE . "'";
					 }
			$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            if($_Response[0] == 'Success') {
						$_Row = mysqli_fetch_array($_Response[2]);
							$itgk = $_Row['Pay_Tran_ITGK'];
							$amount = $_Row['Pay_Tran_Amount'];
							$txnid = $_Row['Pay_Tran_PG_Trnid'];
							$dtime = $_Row['timestamp'];
							$event = $_Row['Pay_Tran_ReexamEvent'];							
						
						$_SelectQuery1 = "Select *, '" . $amount . "' as amount, '" . $itgk . "' as itgk,'" . $dtime . "' as dtime From tbl_event_management where 
										Event_ReexamEvent = '" . $event . "' AND Event_Category='3' AND Event_Name='7' AND Event_Payment='1' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
						$_SelectResponse = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
						if ($_SelectResponse[0] == 'Success') {							
							return $_SelectResponse;
						}
						else {
							//echo "No Record Found";
							return;
						}
					}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function ReexamMoneySettled($txnid,$amount) {		
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $txnid = mysqli_real_escape_string($_ObjConnection->Connect(),$txnid);
				$_SelectQuery = "Select * From tbl_payment_transaction Where Pay_Tran_ProdInfo='ReexamPayment' AND Pay_Tran_PG_Trnid='" . $txnid . "'";
				$_SelectResponse = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			
				
					$_Row = mysqli_fetch_array($_SelectResponse[2]);					   
					$Admission_code = $_Row['Pay_Tran_AdmissionArray'];
					$_Txnid = $_Row['Pay_Tran_PG_Trnid'];					
					$firstname = $_Row['Pay_Tran_Fname'];
					$amounts = $_Row['Pay_Tran_Amount'];
					$udf2 = $_Row['Pay_Tran_RKCL_Trnid'];
					$productinfo = $_Row['Pay_Tran_ProdInfo'];
					$udf1 = $_Row['Pay_Tran_ITGK'];
					
					$_SelectQuery1 = "Select Count(reexam_TranRefNo) AS ReexamTxnid FROM examdata Where reexam_TranRefNo='" . $txnid . "' AND itgkcode='" . $udf1 . "' group by reexam_TranRefNo";					
					$_SelectResponse1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);				
					$num_rows = mysqli_num_rows($_SelectResponse1[2]);
					
					$_SelectMobile = "Select User_MobileNo from tbl_user_master where User_LoginId= '".$udf1."'";
					$_SelectMobileResponse1 = $_ObjConnection->ExecuteQuery($_SelectMobile, Message::SelectStatement);
					$_Mobile = mysqli_fetch_array($_SelectMobileResponse1[2]);
					$mobileno = $_Mobile['User_MobileNo'];	
		
					if($_SelectResponse1[0] == 'Success' && $num_rows = '1') {
						//echo "if";
						$_Row1 = mysqli_fetch_array($_SelectResponse1[2]);
							$_PayTxnid=$_Row1['ReexamTxnid'];							
									$reexam_amount = ($_PayTxnid * 300);
								
						if($reexam_amount == $amount) {
							$_UpdateReexam = "Update examdata set paymentstatus = '1' Where itgkcode = '" . $udf1 . "'  AND reexam_TranRefNo = '" . $txnid . "'";
							$_Response3 = $_ObjConnection->ExecuteQuery($_UpdateReexam, Message::UpdateStatement);
							
							$_SelectReexamtxn = "Select * From tbl_reexam_transaction Where Reexam_Transaction_Txtid='" . $txnid . "'";
							$_SelectResponse2 = $_ObjConnection->ExecuteQuery($_SelectReexamtxn, Message::SelectStatement);			
								if($_SelectResponse2[0]==Message::NoRecordFound) {
									$_InsertQuery = "INSERT INTO tbl_reexam_transaction (Reexam_Transaction_Code, Reexam_Transaction_Status, Reexam_Transaction_Fname, Reexam_Transaction_Amount,"
										. "Reexam_Transaction_Txtid, Reexam_Transaction_ProdInfo,"
										. "Reexam_Transaction_CenterCode,Reexam_Transaction_RKCL_Txid) "
										. "Select Case When Max(Reexam_Transaction_Code) Is Null Then 1 Else Max(Reexam_Transaction_Code)+1 End as Reexam_Transaction_Code,"
										. "'Success' as Reexam_Transaction_Status,'" .$firstname. "' as Reexam_Transaction_Fname,'" .$amount. "' as Reexam_Transaction_Amount,"
										. "'" .$txnid. "' as Reexam_Transaction_Txtid,"
										. "'" .$productinfo. "' as Reexam_Transaction_ProdInfo,'" .$udf1. "' as Reexam_Transaction_CenterCode, '" .$udf2. "' as Reexam_Transaction_RKCL_Txid"
										. " From tbl_reexam_transaction";
									$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);						
								}
								else {
										$_UpdateReexamTranQuery = "Update tbl_reexam_transaction set Reexam_Transaction_Status = 'Success', Reexam_Payment_Mode='VerifyApi', Reexam_Transaction_Amount='" . $amount . "'"			
										. " Where Reexam_Transaction_Txtid='" . $txnid . "' AND Reexam_Transaction_CenterCode='" . $udf1 . "'";								
										$_Response4=$_ObjConnection->ExecuteQuery($_UpdateReexamTranQuery, Message::UpdateStatement);				
									}
							$_UpdatePayTxn = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndConfirmed', Pay_Tran_Status='PaymentReceive', Pay_Tran_Reconcile_status='Reconciled'"			
									. " Where Pay_Tran_PG_Trnid = '" . $txnid . "' AND Pay_Tran_ITGK='" . $udf1 . "'";								
							$_ResponsePayTxn=$_ObjConnection->ExecuteQuery($_UpdatePayTxn, Message::UpdateStatement);								
							
							$_SMS = "Dear ITGK your Payment for Trans. Ref. No. '".$txnid."' and Amount '".$amount."' is successful and Learner will be Confirmed .";
							SendSMS($mobileno, $_SMS);
						}
						else {
									//echo "if-else";
							$_InsertQuery = "INSERT INTO tbl_payment_refund (Payment_Refund_Id, Payment_Refund_Txnid, Payment_Refund_Amount, Payment_Refund_Status,"
								. "Payment_Refund_ProdInfo,"
								. "Payment_Refund_ITGK,Payment_Refund_RKCL_Txid) "
								. "Select Case When Max(Payment_Refund_Id) Is Null Then 1 Else Max(Payment_Refund_Id)+1 End as Payment_Refund_Id,"
								. "'" . $txnid . "' as Payment_Refund_Txnid,'" .$amount. "' as Payment_Refund_Amount, 'Refund' as Payment_Refund_Status,"
								. "'" .$productinfo. "' as Payment_Refund_ProdInfo,"
								. "'" .$udf1. "' as Payment_Refund_ITGK, '" .$udf2. "' as Payment_Refund_RKCL_Txid"
								. " From tbl_payment_refund";
								
							$_DuplicateQuery = "Select * From tbl_payment_refund Where Payment_Refund_Txnid='" . $txnid . "'";
							$_DuplicateResponse = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
								if ($_DuplicateResponse[0] == Message::NoRecordFound) {
									$_Response4=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								}
							
							$_UpdatePayTran = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndRefund', Pay_Tran_Status='PaymentToRefund', Pay_Tran_Reconcile_status='Reconciled'"			
											. " Where Pay_Tran_PG_Trnid = '" . $txnid . "' ";								
							$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTran, Message::UpdateStatement);
							
							$_SelectReexamtxn = "Select * From tbl_reexam_transaction Where Reexam_Transaction_Txtid='" . $txnid . "'";
							$_SelectResponse22 = $_ObjConnection->ExecuteQuery($_SelectReexamtxn, Message::SelectStatement);			
								if($_SelectResponse22[0]==Message::NoRecordFound) {
									$_InsertQuery = "INSERT INTO tbl_reexam_transaction (Reexam_Transaction_Code, Reexam_Transaction_Status, Reexam_Transaction_Fname, Reexam_Transaction_Amount,"
										. "Reexam_Transaction_Txtid, Reexam_Transaction_ProdInfo,"
										. "Reexam_Transaction_CenterCode,Reexam_Transaction_RKCL_Txid) "
										. "Select Case When Max(Reexam_Transaction_Code) Is Null Then 1 Else Max(Reexam_Transaction_Code)+1 End as Reexam_Transaction_Code,"
										. "'Refund' as Reexam_Transaction_Status,'" .$firstname. "' as Reexam_Transaction_Fname,'" .$amount. "' as Reexam_Transaction_Amount,"
										. "'" .$txnid. "' as Reexam_Transaction_Txtid,"
										. "'" .$productinfo. "' as Reexam_Transaction_ProdInfo,'" .$udf1. "' as Reexam_Transaction_CenterCode, '" .$udf2. "' as Reexam_Transaction_RKCL_Txid"
										. " From tbl_reexam_transaction";
									$_Response11=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);						
								}
								else {
										$_UpdateReexamTranQuery = "Update tbl_reexam_transaction set Reexam_Transaction_Status = 'Refund', Reexam_Payment_Mode='VerifyAndRefund', Reexam_Transaction_Amount='" . $amount . "'"			
										. " Where Reexam_Transaction_Txtid='" . $txnid . "' AND Reexam_Transaction_CenterCode='" . $udf1 . "'";								
										$_Response4=$_ObjConnection->ExecuteQuery($_UpdateReexamTranQuery, Message::UpdateStatement);				
									}
							
							$_UpdateExamdata1 = "Update examdata set paymentstatus = '0' Where itgkcode = '" . $udf1 . "'  AND reexam_TranRefNo = '" . $txnid . "'";
							$_ExamDataResponse1 = $_ObjConnection->ExecuteQuery($_UpdateExamdata1, Message::UpdateStatement);	

							$_SMS = "Dear ITGK your Payment for Trans. Ref. No. '".$txnid."' and Amount '".$amount."' will be shortly refunded to you.";
							SendSMS($mobileno, $_SMS);
							
							echo "Amount for Trans. Ref. No. ".$txnid." and Amount ".$amount." will be shortly refunded to you";
							return; 
						}
					}
					
					else {
							//echo "else";
							$_InsertQuery = "INSERT INTO tbl_payment_refund (Payment_Refund_Id, Payment_Refund_Txnid, Payment_Refund_Amount, Payment_Refund_Status,"
								. "Payment_Refund_ProdInfo,"
								. "Payment_Refund_ITGK,Payment_Refund_RKCL_Txid) "
								. "Select Case When Max(Payment_Refund_Id) Is Null Then 1 Else Max(Payment_Refund_Id)+1 End as Payment_Refund_Id,"
								. "'" . $txnid . "' as Payment_Refund_Txnid,'" .$amount. "' as Payment_Refund_Amount, 'Refund' as Payment_Refund_Status,"
								. "'" .$productinfo. "' as Payment_Refund_ProdInfo,"
								. "'" .$udf1. "' as Payment_Refund_ITGK, '" .$udf2. "' as Payment_Refund_RKCL_Txid"
								. " From tbl_payment_refund";
								
							$_DuplicateQuery = "Select * From tbl_payment_refund Where Payment_Refund_Txnid='" . $txnid . "'";
							$_DuplicateResponse = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
								if ($_DuplicateResponse[0] == Message::NoRecordFound) {
									$_Response4=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								}
							
							$_UpdatePayTran = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndRefund', Pay_Tran_Status='PaymentToRefund', Pay_Tran_Reconcile_status='Reconciled'"			
											. " Where Pay_Tran_PG_Trnid = '" . $txnid . "' ";								
							$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTran, Message::UpdateStatement);
							
							$_SelectReexamtxn = "Select * From tbl_reexam_transaction Where Reexam_Transaction_Txtid='" . $txnid . "'";
							$_SelectResponse22 = $_ObjConnection->ExecuteQuery($_SelectReexamtxn, Message::SelectStatement);			
								if($_SelectResponse22[0]==Message::NoRecordFound) {
									$_InsertQuery = "INSERT INTO tbl_reexam_transaction (Reexam_Transaction_Code, Reexam_Transaction_Status, Reexam_Transaction_Fname, Reexam_Transaction_Amount,"
										. "Reexam_Transaction_Txtid, Reexam_Transaction_ProdInfo,"
										. "Reexam_Transaction_CenterCode,Reexam_Transaction_RKCL_Txid) "
										. "Select Case When Max(Reexam_Transaction_Code) Is Null Then 1 Else Max(Reexam_Transaction_Code)+1 End as Reexam_Transaction_Code,"
										. "'Refund' as Reexam_Transaction_Status,'" .$firstname. "' as Reexam_Transaction_Fname,'" .$amount. "' as Reexam_Transaction_Amount,"
										. "'" .$txnid. "' as Reexam_Transaction_Txtid,"
										. "'" .$productinfo. "' as Reexam_Transaction_ProdInfo,'" .$udf1. "' as Reexam_Transaction_CenterCode, '" .$udf2. "' as Reexam_Transaction_RKCL_Txid"
										. " From tbl_reexam_transaction";
									$_Response11=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);						
								}
								else {
										$_UpdatereexamTranQuery = "Update tbl_reexam_transaction set Reexam_Transaction_Status = 'Refund', Reexam_Payment_Mode='VerifyAndRefund', Reexam_Transaction_Amount='" . $amount . "'"			
										. " Where Reexam_Transaction_Txtid='" . $txnid . "' AND Reexam_Transaction_CenterCode='" . $udf1 . "'";								
										$_Response4=$_ObjConnection->ExecuteQuery($_UpdatereexamTranQuery, Message::UpdateStatement);				
									}
							
							$_UpdateExamData1 = "Update examdata set paymentstatus = '0' Where itgkcode = '" . $udf1 . "'  AND reexam_TranRefNo = '" . $txnid . "'";
							$_ExamDataResponse1 = $_ObjConnection->ExecuteQuery($_UpdateExamData1, Message::UpdateStatement);	

							$_SMS = "Dear ITGK your Payment for Trans. Ref. No. '".$txnid."' and Amount '".$amount."' will be shortly refunded to you.";
							SendSMS($mobileno, $_SMS);
							
							echo "Amount for Trans. Ref. No. ".$txnid." and Amount ".$amount." will be shortly refunded to you";
							return; 
					}
					
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response3;        
    }
	
}