<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrReport
 *
 * @author VIVEK
 */ 

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrReport {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
                $_SelectQuery = "SELECT a.*,b.District_Name, c.Tehsil_Name, d.Organization_Name as RSPNAME, "
                        . "e.User_LoginId, f.Block_Name, g.GP_Name, h.User_LoginId as CenterCode FROM tbl_org_master AS a INNER JOIN tbl_district_master as b "
                        . "ON a.Organization_District=b.District_Code INNER JOIN tbl_tehsil_master AS c "
                        . "ON a.Organization_Tehsil=c.Tehsil_Code INNER JOIN tbl_user_master as e "
                        . "ON a.org_RspLoginId=e.User_LoginId INNER JOIN tbl_user_master as h "
                        . "ON a.Org_Ack=h.User_Ack INNER JOIN tbl_organization_detail as d "
                        . "ON d.Organization_User=e.User_Code INNER JOIN tbl_panchayat_samiti as f "
                        . "ON a.Org_Panchayat=f.Block_Code INNER JOIN tbl_gram_panchayat as g "
                        . "ON a.Org_Gram=g.GP_Code WHERE a.Org_Status = 'Approved' GROUP BY a.Org_Ack";
    
            } else {
                $_SelectQuery = "SELECT a.*,b.District_Name, c.Tehsil_Name, d.Organization_Name as RSPNAME, "
                        . "e.User_LoginId, f.Block_Name, g.GP_Name, h.User_LoginId as CenterCode FROM tbl_org_master AS a INNER JOIN tbl_district_master as b "
                        . "ON a.Organization_District=b.District_Code INNER JOIN tbl_tehsil_master AS c "
                        . "ON a.Organization_Tehsil=c.Tehsil_Code INNER JOIN tbl_user_master as e "
                        . "ON a.org_RspLoginId=e.User_LoginId INNER JOIN tbl_user_master as h "
                        . "ON a.Org_Ack=h.User_Ack INNER JOIN tbl_organization_detail as d "
                        . "ON d.Organization_User=e.User_Code INNER JOIN tbl_panchayat_samiti as f "
                        . "ON a.Org_Panchayat=f.Block_Code INNER JOIN tbl_gram_panchayat as g "
                        . "ON a.Org_Gram=g.GP_Code WHERE a.Org_Status = 'Approved' AND a.Org_RspLoginId='" .$_SESSION['User_LoginId'] . "'GROUP BY a.Org_Ack";
            }
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
