<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsMessageMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsMessageMaster {
    //put your code here
    
     public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select m.*,c.Message_Cat_Name,t.Message_Type_Name From tbl_message_template_master m join tbl_message_category c on c.Message_Cat_ID=m.Message_Cat_ID join tbl_message_type t on t.Message_Type_ID=m.Message_Type_ID ";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllMSG() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.*, b.Organization_Name From tbl_message_master as a inner join tbl_organization_detail as b "
                    . "on a.Message_Sender = b.Organization_User where a.Message_Role='".$_SESSION['User_UserRoll']."' ORDER BY Message_EndDate DESC";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
          //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetDatabyCode($_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_SelectQuery = "Select * From tbl_message_template_master Where Message_Temp_ID={$_Code} ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetCount()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT COUNT(Message_Name) as count FROM tbl_message_master where Message_Role='" .$_SESSION['User_UserRoll']. "' AND CURDATE() <= Message_EndDate";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function DeleteRecord($_ITGK_Code,$_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_DeleteQuery = "Delete From tbl_message_template_master Where Message_Temp_ID='{$_Code }'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ITGK_Code,$_Title,$_Name,$_cat,$_type,$_Mfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "Insert Into tbl_message_template_master (`User_Code`,`Message_Text`,`Message_Title`,`Message_Cat_ID`,`Message_Type_ID`,`Message_For`) values ('{$_ITGK_Code}','{$_Name}','{$_Title}','{$_cat}','{$_type}','{$_Mfor}')";
            //echo $_InsertQuery;

            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            echo '<pre>';print_r($_Response);die('i m here');

        }

        return $_Response;
    }
    
    public function Update($_Code,$_ITGK_Code,$_Title,$_Name,$_cat,$_type,$_Mfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_message_template_master set `Message_Text`='{$_Name}',`Message_Title`='{$_Title}',Message_Cat_ID={$_cat},Message_Type_ID={$_type},Message_For='{$_Mfor}' Where Message_Temp_ID={$_Code}";


                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);


        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }


    function GetAllCat($_ITGK_Code){
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_message_category";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }

    function GetAllType($_ITGK_Code){
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_message_type";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;

        }
        return $_Response;
    }

    public function AddCat($_ITGK_Code,$_name) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "Insert Into tbl_message_category(User_Code,Message_Cat_Name) values ('{$_ITGK_Code}','{$_name}')";
            //echo $_InsertQuery;

            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

        } catch (Exception $_e) {

            $_Response[0] = Message::DuplicateRecord;
            $_Response[1] = 0;


        }


        return $_Response;
    }

    public function AddType($_ITGK_Code,$_name) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "Insert Into tbl_message_type(User_Code,Message_Type_Name) values ('{$_ITGK_Code}','{$_name}')";
            //echo $_InsertQuery;

            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

        } catch (Exception $_e) {

            $_Response[0] = Message::DuplicateRecord;
            $_Response[1] = 0;

        }
        return $_Response;
    }

}
