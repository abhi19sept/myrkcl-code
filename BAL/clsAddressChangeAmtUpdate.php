<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAddressChangeAmtUpdate
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAddressChangeAmtUpdate {
    //put your code here
    
    public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8') {
                    $_SelectQuery = "select a.fld_ID,a.fld_ITGK_Code,a.Organization_AreaType_old,a.Organization_AreaType,b.Organization_Name,
                                    c.Tehsil_Name as Tehsil_Old,d.Tehsil_Name as Tehsil_New, e.NCRamount_Amount
                                    from tbl_change_address_itgk as a inner join tbl_organization_detail as b on a.fld_ITGK_UserCode=b.Organization_User
                                    left join tbl_tehsil_master as c on a.Organization_Tehsil_old=c.Tehsil_Code 
                                    left join tbl_tehsil_master as d on a.Organization_Tehsil=d.Tehsil_Code 
                                    inner join tbl_ncramount_master as e on a.fld_ITGK_Code=e.NCRamount_ITGK
                                    where fld_requestChangeType='address' and a.fld_ITGK_Code='" . $_CenterCode . "' order by a.fld_ID DESC Limit 1";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    echo "You are not Auhorized to use this functionality on MYRKCL.";
                    return;
                }
                
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_CenterCode) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                
                date_default_timezone_set('Asia/Calcutta');
                $_Date = date("Y-m-d h:i:s");
                
                $_InsertQuery = "Insert into tbl_ncramount_master_log select A.*,'" . $_Date . "' 
                    from tbl_ncramount_master as a Where NCRamount_ITGK='" . $_CenterCode . "'";
            //echo $_InsertQuery;
                $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                
                $_UpdateQuery = "Update tbl_ncramount_master set NCRamount_Amount = '38000' 
                    where NCRamount_ITGK='" . $_CenterCode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
              
                        
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
}
