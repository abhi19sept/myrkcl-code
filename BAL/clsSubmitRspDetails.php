<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsSubmitRspDetails
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsSubmitRspDetails {
    //put your code here
    
    public function GetRspDetails() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
          
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d "
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e"
                    . " ON a.Organization_User=e.User_Code WHERE Organization_User = '" . $_SESSION['User_Rsp'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function SubmitDetails() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
           if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_mobile=$_SESSION['User_MobileNo'];
            $_SMS = "Dear  Applicant, Your SP Choice Details has been submitted to RKCL.";
            
            $_USERNAME = $_SESSION['User_LoginId'];
            $_Rsp_Code = $_SESSION['User_Rsp'];
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
            $_SelectQuery = "Select a.User_Code,b.*,c.Org_Type_Name,d.District_Name,e.Tehsil_Name FROM tbl_user_master as a INNER JOIN tbl_organization_detail as b"
                    . " ON a.User_Code=b.Organization_User INNER JOIN tbl_org_type_master as c"
                    . " ON b.Organization_Type=c.Org_Type_Code INNER JOIN tbl_district_master as d"
                    . " ON b.Organization_District=d.District_Code INNER JOIN tbl_tehsil_master as e"
                    . " ON b.Organization_Tehsil=e.Tehsil_Code WHERE User_Code = '" . $_Rsp_Code . "'";
            $_Response11 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response11[2]);
            $_Code = $_Row1['User_Code'];
            $_Rspname = $_Row1['Organization_Name'];
            $_Rspregno = $_Row1['Organization_RegistrationNo'];
            $_Rspestdate = $_Row1['Organization_FoundedDate'];
            $_Rsptype = $_Row1['Org_Type_Name'];
            $_Rspdistrict = $_Row1['District_Name'];
            $_Rsptehsil = $_Row1['Tehsil_Name'];
            $_Rspstreet = $_Row1['Organization_Street'];
            $_Rsproad = $_Row1['Organization_Road'];
            
                $_InsertQuery = "Insert Into tbl_rspitgk_mapping(Rspitgk_Code,Rspitgk_ItgkCode,Rspitgk_Rspcode,Rspitgk_Rspname,"
                        . "Rspitgk_Rspregno,Rspitgk_Rspestdate,Rspitgk_Rsporgtype,Rspitgk_Rspdistrict,"
                        . "Rspitgk_Rsptehsil,Rspitgk_Rspstreet,Rspitgk_Rsproad,Rspitgk_Date,Rspitgk_RspApproval_Date,Rspitgk_Status,Rspitgk_UserType) "
                        . "Select Case When Max(Rspitgk_Code) Is Null Then 1 Else Max(Rspitgk_Code)+1 End as Rspitgk_Code,"
                        . "'" . $_USERNAME . "' as Rspitgk_ItgkCode,'" . $_Rsp_Code . "' as Rspitgk_Rspcode,'" . $_Rspname . "' as Rspitgk_Rspname,"
                        . "'" . $_Rspregno . "' as Rspitgk_Rspregno,'" . $_Rspestdate . "' as Rspitgk_Rspestdate,"
                        . "'" . $_Rsptype . "' as Rspitgk_Rsporgtype,'" . $_Rspdistrict . "' as Rspitgk_Rspdistrict,"
                        . "'" . $_Rsptehsil . "' as Rspitgk_Rsptehsil,'" . $_Rspstreet . "' as Rspitgk_Rspstreet,"
                        . "'" . $_Rsproad . "' as Rspitgk_Rsproad,'" . $_Date . "' as Rspitgk_Date,'" . $_Date . "' as Rspitgk_RspApproval_Date,'Approved' as Rspitgk_Status,'New_Center' as Rspitgk_UserType"
                        . " From tbl_rspitgk_mapping";
                $_DuplicateQuery = "Select * From tbl_rspitgk_mapping Where Rspitgk_ItgkCode='" . $_USERNAME . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                    //echo $_Response[0];
                    if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_mobile, $_SMS);
                    } else {
                        $_Response[0] = Message::DuplicateRecord;
                        $_Response[1] = Message::Error;
                    }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
