<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGovLearnerProcessStatusReport {
    //put your code here
	
	public function GetAll($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			
			$_LoginRole = $_SESSION['User_UserRoll'];
         if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '18' || $_LoginRole == '11' || $_LoginRole == '21') {
			 $_SelectQuery = "Select a.*,b.lotname from tbl_govempentryform as a inner join tbl_clot as b on
							  a.lot=b.lotid	where learnercode='".$_actionvalue."' ";
			 
		 }		
		 
		 else if ($_LoginRole == '19' || $_LoginRole == '7')
		 {
			$_SelectQuery = "Select a.*,b.lotname from tbl_govempentryform as a inner join tbl_clot as b on
							  a.lot=b.lotid where learnercode='".$_actionvalue."' AND Govemp_ITGK_Code='" . $_SESSION['User_LoginId'] . "'";
		 }
		 
		 else if ($_LoginRole == '14')
		 {
			 $_SelectQuery1 = "Select * from tbl_admission where Admission_LearnerCode='".$_actionvalue."' 
								AND Admission_RspName = '".$_SESSION['User_Code']."'";
			$_Responses=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			 if($_Responses[0]=='Success'){
				  $_SelectQuery = "Select a.*,b.lotname from tbl_govempentryform as a inner join tbl_clot as b on
							  a.lot=b.lotid where learnercode='".$_actionvalue."'";
				 $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			 }
			 else {
				//$_Responses[0] = Message::NoRecordFound;
				 //echo "No";
				 return $_Responses;
			 }
		 }
		 
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
    public function GetAll1($_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
			if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == '11' || $_SESSION['User_UserRoll'] == '18' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '9' || $_SESSION['User_UserRoll'] == 21){
				$_SelectQuery = "Select a.Govemp_ITGK_Code,a.Govemp_Learnercode,max(a.Govemp_Datetime) as Govemp_Datetime , c.cstatus, a.Govemp_Process_Remark FROM `tbl_govstatusupdatelog` as a INNER JOIN
									tbl_capcategory as c on a.Govemp_Process_Status=c.capprovalid where Govemp_Learnercode='" . $_Lcode . "' AND govrole='Yes'
									 group by c.cstatus order by Govemp_Datetime";
			}
			else if($_SESSION['User_UserRoll'] == 7 || $_SESSION['User_UserRoll'] == 19 ) {
				 $_SelectQuery = "Select a.Govemp_ITGK_Code,a.Govemp_Learnercode,max(a.Govemp_Datetime) as Govemp_Datetime, c.cstatus, a.Govemp_Process_Remark FROM `tbl_govstatusupdatelog` as a INNER JOIN
									tbl_capcategory as c on a.Govemp_Process_Status=c.capprovalid where
									Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND Govemp_Learnercode='" . $_Lcode . "' AND govrole='Yes' group by c.cstatus order by Govemp_Datetime";
			}	

			else if($_SESSION['User_UserRoll'] == 14 ) {
				$_SelectQuery = "Select a.Govemp_ITGK_Code,a.Govemp_Learnercode,max(a.Govemp_Datetime) as Govemp_Datetime, c.cstatus, a.Govemp_Process_Remark FROM `tbl_govstatusupdatelog` as a INNER JOIN
									tbl_capcategory as c on a.Govemp_Process_Status=c.capprovalid where
									Govemp_Learnercode='" . $_Lcode . "' AND govrole='Yes' group by c.cstatus order by Govemp_Datetime";
			}	
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	 public function GetStep1Data($_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
			if($_SESSION['User_UserRoll'] == 7 || $_SESSION['User_UserRoll'] == 19) {
				 $_SelectQuery = "Select * FROM `tbl_govempentryform` where
									Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND learnercode='" . $_Lcode . "'";						
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 18 || $_SESSION['User_UserRoll'] == 21) {
				$_SelectQuery = "Select * FROM `tbl_govempentryform` where learnercode='" . $_Lcode . "'";						
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetStep3Data($_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
			if($_SESSION['User_UserRoll'] == 7 || $_SESSION['User_UserRoll'] == 19) {
				 $_SelectQuery = "Select * FROM `tbl_govstatusupdatelog` where
									Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND Govemp_Learnercode='" . $_Lcode . "' And Govemp_Process_Status='3'";						
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 18 || $_SESSION['User_UserRoll'] == 21) {
				$_SelectQuery = "Select * FROM `tbl_govstatusupdatelog` where Govemp_Learnercode='" . $_Lcode . "' And Govemp_Process_Status='3'";						
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetStep4Data($_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
			if($_SESSION['User_UserRoll'] == 7 || $_SESSION['User_UserRoll'] == 19 ) {				
					  $_SelectQuery1 = "Select b.cstatus, a.trnpending FROM `tbl_govempentryform` as a INNER JOIN tbl_capcategory as b on a.trnpending=b.capprovalid where
									Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND learnercode='" . $_Lcode . "'";
					  $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);				 
			}			
            else if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 18 || $_SESSION['User_UserRoll'] == 21) {
				$_SelectQuery = "Select b.cstatus, a.trnpending FROM `tbl_govempentryform` as a INNER JOIN tbl_capcategory as b on a.trnpending=b.capprovalid where
									learnercode='" . $_Lcode . "'";						
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
