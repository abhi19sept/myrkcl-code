<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOnHoldDetails
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOnHoldDetails {
    //put your code here
    
       public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b
                            ON a.Organization_Type=b.Org_Type_Code where Org_PayStatus='1'";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code 
                            INNER JOIN tbl_user_master as c on a.Org_Itgk_Code = c.User_LoginId 
                            where Org_PayStatus='1' and c.User_Rsp= '" . $_SESSION['User_Code'] . "'";
            }
            elseif ($_SESSION['User_UserRoll'] == 7) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code 
                            INNER JOIN tbl_user_master as c on a.Org_Itgk_Code = c.User_LoginId 
                            where Org_PayStatus='1' AND Org_Application_Approval_Status='OnHold' and Org_Itgk_Code= '" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
      public function GetOrgDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code
                            where Org_Itgk_Code='" . $_SESSION['User_LoginId'] . "'
                            AND a.Org_Application_Approval_Status = 'OnHold' and Org_Final_Approval_Status='Pending' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
      
    public function Add($_OrgCode,$_OrgEmail, $_OrgMobile, $_OrgName, $_OrgRegno, $_OrgEstDate, 
                        $_PanNo, $_AADHARNo, $_IFSC_Code, $_AccountName, $_AccountNumber, 
                        $_BankName,$_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
            $_CenterCode = $_SESSION['User_LoginId'];
			
			$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
			
            $_InsertQuery = "Insert into tbl_ownership_change_log select A.*,'" . $_Date . "' from tbl_ownership_change as a Where Organization_Code='" . $_OrgCode . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
         /**   $_UpdateQuery = "Update tbl_ownership_change set Org_Application_Approval_Status='Pending', Org_Last_Updated_Date='" . $_Date . "', Org_Edited_Status='RE-SUBMITTED',"
                    . "Org_Email='" . $_OrgEmail . "',Org_Mobile='" . $_OrgMobile . "',Organization_Name='" . $_OrgName . "',"
                    . "Organization_RegistrationNo='" . $_OrgRegno . "',Organization_FoundedDate='" . $_OrgEstDate . "',"
                    . "Organization_ScanDoc='" . $_PanNo . "',Organization_UID='" . $_AADHARNo . "',"
                    . "Bank_Ifsc_code='" . $_IFSC_Code . "',Bank_Account_Name='" . $_AccountName . "',"
                    . "Bank_Account_Number='" . $_AccountNumber . "',Bank_Name='" . $_BankName . "',"
                    . "Bank_Branch_Name='" . $_BranchName . "',Bank_Micr_Code='" . $_MicrCode . "',"
                    . "Bank_Account_Type='" . $_AccountType . "',Bank_Pan_No='" . $_PanNo1 . "',"
                    . "Bank_Pan_Name='" . $_PanName . "',Bank_Id_Proof='" . $_IdProof . "'"
                    . " Where Organization_Code='" . $_OrgCode . "'";
					
					**/
					
					$_UpdateQuery = "Update tbl_ownership_change set Org_Application_Approval_Status='Pending', Org_Last_Updated_Date='" . $_Date . "', Org_Edited_Status='RE-SUBMITTED',"
                    . "Org_Email='" . $_OrgEmail . "',Org_Mobile='" . $_OrgMobile . "',Organization_Name='" . $_OrgName . "',"
                    . "Organization_RegistrationNo='" . $_OrgRegno . "',Organization_FoundedDate='" . $_OrgEstDate . "',"
                    . "Org_PAN='" . $_PanNo . "',Org_AADHAR='" . $_AADHARNo . "',"
                    . "Bank_Ifsc_code='" . $_IFSC_Code . "',Bank_Account_Name='" . $_AccountName . "',"
                    . "Bank_Account_Number='" . $_AccountNumber . "',Bank_Name='" . $_BankName . "',"
                    . "Bank_Branch_Name='" . $_BranchName . "',Bank_Micr_Code='" . $_MicrCode . "',"
                    . "Bank_Account_Type='" . $_AccountType . "',Bank_Pan_No='" . $_PanNo1 . "',"
                    . "Bank_Pan_Name='" . $_PanName . "',Bank_Id_Proof='" . $_IdProof . "'"
                    . " Where Organization_Code='" . $_OrgCode . "'";

            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            $_SMSITGK = "Dear ITGK, Your Ownership Change application has been UPDATED on MYRKCL."
                    . "Kindly wait for APPROVAL from MYRKCL";
            
            $_SMSRSP = "Dear Service Provider, Ownership Change request for Center Code" .$_CenterCode. "has been UPDATED on MYRKCL."
                    . "Kindly wait for APPROVAL from MYRKCL";
            
            $_SelectQuery = "Select * from tbl_user_master Where User_LoginId='" . $_CenterCode . "'";
            
            $_Response2=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response2[2]);
            $_Mobileold = $_Row['User_MobileNo'];
            $_Rsp_Code = $_Row['User_Rsp'];
            
            $_SelectQuery1 = "Select * from tbl_user_master Where User_Code='" . $_Rsp_Code . "'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_RSPMobile = $_Row1['User_MobileNo'];
            
            $_CFOMobile = '9649900801';
            $_SMSCFO = "Dear Sir, Ownership Change request for Center Code" .$_CenterCode. "has been Re-Submitted on MYRKCL."
                    . "Kindly procee for APPROVAL from MYRKCL";
            SendSMS($_CFOMobile, $_SMSCFO);
            SendSMS($_OrgMobile, $_SMSITGK);
            SendSMS($_Mobileold, $_SMSITGK);
            SendSMS($_RSPMobile, $_SMSRSP);
            }else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
