<?php

/**
 * Description of clsWcdFeedbackForm
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdFeedbackForm {


public function FILLBatchName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					if($_SESSION['User_UserRoll']=='19' || $_SESSION['User_UserRoll']=='1' || $_SESSION['User_UserRoll'] == '4' ){
						 $_SelectQuery = "Select b.Batch_Name, b.Batch_Code From tbl_admission as a inner join tbl_batch_master as b
										 on a.Admission_Batch=b.Batch_Code WHERE Course_Code in(3,24) and
										 Admission_LearnerCode='".$_SESSION['User_LearnerCode']."'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					}
					 else{
						 session_destroy();
						?>
						<script> window.location.href = "logout.php";</script> 
						<?php								 
					 }
				}
			else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetDetails() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Admission_LearnerCode,Admission_ITGK_Code,Admission_Name,Organization_Name from tbl_admission as a inner join tbl_organization_detail as b on a.User_Code=b.Organization_User where Admission_LearnerCode='". $_SESSION['User_LearnerCode'] ."' and Admission_Course in (3,24)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }	

public function Add($_LearnerName, $_LearnerCode, $_ITGKName, $_ITGKCode, $_BatchCode, $qt1, $qt2, $qt3, $qt4, $qps1, $qps2, $qps3, $qff1, $qff2, $qff3, $qo1, $qo2, $qe1) {
		global $_ObjConnection;
        $_ObjConnection->Connect();
		
		try {
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
			$_chkDuplicate = "select * from wcd_feedback_form where Wcd_Feedback_lcode='" . $_LearnerCode ."' and Wcd_Feedback_Batch='" . $_BatchCode ."'";
			$_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
                if ($_Response2[0] == Message::NoRecordFound){
				 $_InsertQuery = "Insert Into wcd_feedback_form(Wcd_Feedback_Id,Wcd_Feedback_lcode,Wcd_Feedback_Itgk_Code,Wcd_Feedback_Batch,Wcd_Feedback_Lname,Wcd_Feedback_Itgk_Name,
														Wcd_Feedback_Ques_1,Wcd_Feedback_Ques_2,Wcd_Feedback_Ques_3,Wcd_Feedback_Ques_4,
														Wcd_Feedback_Ques_5,Wcd_Feedback_Ques_6,Wcd_Feedback_Ques_7,Wcd_Feedback_Ques_8,
														Wcd_Feedback_Ques_9,Wcd_Feedback_Ques_10,Wcd_Feedback_Ques_11,Wcd_Feedback_Ques_12,
														Wcd_Feedback_Ques_13)"
                    . "Select Case When Max(Wcd_Feedback_Id) Is Null Then 1 Else Max(Wcd_Feedback_Id)+1 End as Wcd_Feedback_Id,"
                    . "'" . $_LearnerCode . "' as Wcd_Feedback_lcode, '" . $_ITGKCode . "' as Wcd_Feedback_Itgk_Code,"
                    . "'" . $_BatchCode . "' as Wcd_Feedback_Batch, '" . $_LearnerName . "' as Wcd_Feedback_Lname, '" . $_ITGKName . "' as Wcd_Feedback_Itgk_Name,"
                    . "'" . $qt1 . "' as Wcd_Feedback_Ques_1, '" . $qt2 . "' as Wcd_Feedback_Ques_2, '" . $qt3 . "' as Wcd_Feedback_Ques_3,"
                    . "'" . $qt4 . "' as Wcd_Feedback_Ques_4, '" . $qps1 . "' as Wcd_Feedback_Ques_5, '" . $qps2 . "' as Wcd_Feedback_Ques_6,"
                    . "'" . $qps3 . "' as Wcd_Feedback_Ques_7, '" . $qff1 . "' as Wcd_Feedback_Ques_8, '" . $qff2 . "' as Wcd_Feedback_Ques_9,"
                    . "'" . $qff3 . "' as Wcd_Feedback_Ques_10, '" . $qo1 . "' as Wcd_Feedback_Ques_11, '" . $qo2 . "' as Wcd_Feedback_Ques_12,"
                    . "'" . $qe1 . "' as Wcd_Feedback_Ques_13"
					. " From wcd_feedback_form";
                //echo $_InsertQuery;die;            
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			}
			else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
		}
		else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	public function Add1($_LearnerName, $_LearnerCode, $_ITGKName, $_ITGKCode, $_BatchCode,$qt11,$qt12,$qt13,$qt14,$qt15,$qt16) {
		global $_ObjConnection;
        $_ObjConnection->Connect();
		
		try {
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
			$_chkDuplicate = "select * from wcd_feedback_form where Wcd_Feedback_lcode='" . $_LearnerCode ."' and Wcd_Feedback_Batch='" . $_BatchCode ."'";
			$_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
                if ($_Response2[0] == Message::NoRecordFound){
				  $_InsertQuery = "Insert Into wcd_feedback_form(Wcd_Feedback_Id,Wcd_Feedback_lcode,Wcd_Feedback_Itgk_Code,Wcd_Feedback_Batch,Wcd_Feedback_Lname,Wcd_Feedback_Itgk_Name,
														Wcd_Feedback1_Ques_1,Wcd_Feedback1_Ques_2,Wcd_Feedback1_Ques_3,Wcd_Feedback1_Ques_4,Wcd_Feedback1_Ques_5,Wcd_Feedback1_Ques_6)"
                    . "Select Case When Max(Wcd_Feedback_Id) Is Null Then 1 Else Max(Wcd_Feedback_Id)+1 End as Wcd_Feedback_Id,"
                    . "'" . $_LearnerCode . "' as Wcd_Feedback_lcode, '" . $_ITGKCode . "' as Wcd_Feedback_Itgk_Code,"
                    . "'" . $_BatchCode . "' as Wcd_Feedback_Batch, '" . $_LearnerName . "' as Wcd_Feedback_Lname, '" . $_ITGKName . "' as Wcd_Feedback_Itgk_Name,"
                    . "'" . $qt11 . "' as Wcd_Feedback1_Ques_1, '" . $qt12 . "' as Wcd_Feedback1_Ques_2, '" . $qt13 . "' as Wcd_Feedback1_Ques_3,"
                    . "'" . $qt14 . "' as Wcd_Feedback1_Ques_4, '" . $qt15 . "' as Wcd_Feedback1_Ques_5, '" . $qt16 . "' as Wcd_Feedback1_Ques_6"
                    . " From wcd_feedback_form";
                //echo $_InsertQuery;die;            
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			}
			else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
		}
		else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	

}
