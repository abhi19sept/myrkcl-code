<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPremisesAreaRpt
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPremisesAreaRpt {
    //put your code here
    
           public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 10 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 30) {
            $_SelectQuery = "Select a.*, b.* FROM tbl_premises_details as a inner join vw_itgkname_distict_rsp as b "
                    . "on a.Premises_User=b.ITGKCODE";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select a.*, b.* FROM tbl_premises_details as a inner join vw_itgkname_distict_rsp as b "
                    . "on a.Premises_User=b.ITGKCODE where b.RSP_Code='" . $_SESSION['User_LoginId'] . "'";
            } elseif ($_SESSION['User_UserRoll'] == 23) {
            $_SelectQuery = "Select a.*, b.* FROM tbl_premises_details as a inner join vw_itgkname_distict_rsp as b "
                    . "on a.Premises_User=b.ITGKCODE where b.District_Code='" . $_SESSION['Organization_District'] . "'";
            } elseif ($_SESSION['User_UserRoll'] == 7) {
            $_SelectQuery = "Select a.*, b.* FROM tbl_premises_details as a inner join vw_itgkname_distict_rsp as b "
                    . "on a.Premises_User=b.ITGKCODE where Premises_User = '" . $_SESSION['User_LoginId'] . "'";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
