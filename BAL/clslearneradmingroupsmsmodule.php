<?php


/**
 * Description of clsOrgDetail
 *
 * @author  yogendra soni
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';


$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clslearneradmingroupsmsmodule {
	
	 
		
     public function ADD($_mobile,$_Msg) 
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code = $_SESSION['User_LoginId'];
			//print_r($_mobile);
			$countno=explode(',', $_mobile);
			$countno1 = count($countno);
			$_InsertQuery = "Insert Into tbl_sms(id,Mobile,sms,status,centercode,count) 
			 Select Case When Max(id) Is Null Then 1 Else Max(id)+1 End as id,
			
			 '" . $_mobile. "' as Mobile,
			 '" .$_Msg. "' as sms,
			  '0' as status,
			 '" .$_ITGK_Code. "' as centercode,
			 '" .$countno1. "' as count
			 From tbl_sms";
			 
			/*  SendSMS($_mobile, $_Msg); */
			 
			  
            
			 $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					
				
               
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	
	
	
	public function GetAll($_ITGK_Code) 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
		     $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' AND Payment_Status='0' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			
			
			$totalpackage=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$totalpackage = $totalpackage + $_Row1['package'];
		      } 	
			
			$temp=0;
			$_SelectQuery1 = "Select count from tbl_sms where centercode='$_ITGK_Code'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			while ($_data = mysqli_fetch_array($_Response1[2])) 
			{
				$temp=$temp+$_data['count'];
				
				
		    }
			//print_r($temp);
			
			$_Response[2]=$totalpackage-$temp;
			$_Response[3]=$temp;
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	public function GetTotalpackage($_ITGK_Code) 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' AND Payment_Status='0' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$tot=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$tot = $tot + $_Row1['package'];
		      } 	
			
			
			$_Response[2]=$tot;
			
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	
	
	public function Getlearner($_activate1,$_activate2) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_activate1 = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate1);
			$_activate2 = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate2);
			
		    $_SelectQuery = "Select distinct Admission_LearnerCode,Admission_Name,Admission_Fname from tbl_admission where  Admission_Course='".$_activate1."'  AND Admission_Batch='".$_activate2."' AND Admission_Mobile!=0 order by Admission_Name,Admission_LearnerCode";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function Getlearnermobile($_activate1,$_activate2) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_activate1 = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate1);
			$_activate2 = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate2);
		     $_SelectQuery = "Select  distinct  Admission_Mobile from tbl_admission where Admission_Course='".$_activate1."'  AND Admission_Batch='".$_activate2."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function Getlearnermobilevalue($_activate1,$_activate2,$_activate3) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_activate1 = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate1);
			$_activate2 = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate2);
		    $_SelectQuery = "Select distinct Admission_Mobile from tbl_admission where  Admission_Course='".$_activate1."'  AND Admission_Batch='".$_activate2."' AND Admission_LearnerCode IN (".$_activate3.")";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	public function Getstatus() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_ITGK_Code = $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select Payment_Status from tbl_package_transections where  centercode='".$_ITGK_Code."'  ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	
	
}
