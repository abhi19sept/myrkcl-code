<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Update Mobile Number for ITGK
 *
 * @author SUNIL KUAMR BAINDARA 1-9-2019
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUpdateMobileNumber {
    //put your code here
    
     public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '28') 
                    {
						$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
						
                    $_SelectQuery = "Select a.User_Code, a.User_EmailId, a.User_MobileNo,
                    b.Organization_Code, b.Organization_Name, c.District_Name, d.Tehsil_Name, 
                    b.Organization_HouseNo, b.Organization_Street, b.Organization_Road, b.Organization_Landmark, b.Org_formatted_address
                    from tbl_user_master a
                    LEFT JOIN tbl_organization_detail b on a.User_Code=b.Organization_User
                    INNER JOIN tbl_district_master as c on b.Organization_District=c.District_Code
                    INNER JOIN tbl_tehsil_master as d on b.Organization_Tehsil=d.Tehsil_Code
                    where  a.User_LoginId='" . $_CenterCode . "'";
                    
//                    $_SelectQuery = "SELECT a.Organization_Code, a.Organization_Name, a.Organization_District, a.Organization_Tehsil, a.Organization_AreaType, b.District_Name, c.Tehsil_Name
//                                    from tbl_organization_detail as a INNER JOIN tbl_district_master as b on a.Organization_District=b.District_Code
//                                    INNER JOIN tbl_tehsil_master as c on a.Organization_Tehsil=c.Tehsil_Code
//                                    INNER JOIN tbl_user_master as d on a.Organization_User=d.User_Code where d.User_LoginId='" . $_CenterCode . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    } 
                else {
                    echo "You are not Auhorized to use this functionality on MYRKCL.";
                    return;
                }
                
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateMobile($_ITGK_Code, $_User_MobileNo) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {

            if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '28')
                {
					
					$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
					$_User_MobileNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_User_MobileNo);
					
                $_SelectQuery = "select User_Code,User_MobileNo from tbl_user_master where User_LoginId='" . $_ITGK_Code . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                
                $_Row = mysqli_fetch_array($_Response1[2]);
                $_User_Code = $_Row['User_Code'];
                $_Old_Mobile = $_Row['User_MobileNo'];
                
                $_InsertQuery = "Insert Into tbl_mobile_update_log(User_Code,User_LoginId,Old_Mobile,New_Mobile)"
                    . "VALUES ('" . $_User_Code . "','" . $_ITGK_Code . "','" . $_Old_Mobile . "','" . $_User_MobileNo . "')";
                $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                
                $_UpdateQuery = "Update tbl_user_master set User_MobileNo='" . $_User_MobileNo . "',IsNewRecord='Y', IsNewCRecord='Y', IsNewSRecord='Y' , IsNewOnlineLMSRecord='Y' WHERE User_LoginId='" . $_ITGK_Code . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_SMS="Dear ITGK (" . $_ITGK_Code . "), Your mobile number has been successfully updated in MYRKCL Portal as per your request.";
                SendSMS($_User_MobileNo, $_SMS);
                        
            } 
            else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
}
