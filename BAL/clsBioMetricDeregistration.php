<?php

/**
 * Description of clsBioMetricDeregistration
 *
 * @author SUNIL KUMAR BAINDARA
 * DATED: 26-09-2018
  * Updated by : Abhishek
 * DATED: 15-05-2019
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsBioMetricDeregistration {

    //put your code here


    public function checkLearnerCode($LD_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            $_DuplicateQuery = "Select * From tbl_learner_deregister Where LD_LearnerCode='" . $LD_LearnerCode . "' and LD_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getLearnerNameFname($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "select Admission_Name,Admission_Fname 
            from tbl_admission 
            where Admission_LearnerCode='" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLCurrentOpenBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "select a.Batch_Code, a.Batch_Name from tbl_batch_master as a 
               inner join tbl_event_management as b on a.Batch_Code=b.Event_Batch and a.Course_Code=b.Event_Course 
               where b.Event_LearnerAttendance='1' and curdate() between b.Event_Startdate and b.Event_Enddate 
               and a.Course_Code='" . $course . "' ORDER BY a.Batch_Code DESC";
            //  $_SelectQuery = "select a.Batch_Code, a.Batch_Name from tbl_batch_master as a              
            // where a.Course_Code='" . $course . "' ORDER BY a.Batch_Code DESC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCurrentBatchLearnerListPreview($_course, $_batch, $LD_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            //$_batch='112';
            if ($_SESSION['User_UserRoll'] != 1) {
                $User_LoginId = " AND a.Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "'";
            } else {
                $User_LoginId = '';
            }
            $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name
                FROM tbl_admission AS a 
                INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code 
                INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code 
                WHERE a.Admission_Course = '" . $_course . "' 
                AND a.Admission_Batch = '" . $_batch . "' 
                AND a.Admission_Payment_Status = '1'
                AND a.BioMatric_Status=1 " . $User_LoginId . " AND Admission_LearnerCode IN (" . $LD_LearnerCode . ") ORDER BY Admission_Date DESC";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCurrentBatchLearnerList($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            // $_batch='112';
            if ($_SESSION['User_UserRoll'] != 1) {
                $User_LoginId = " AND a.Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "'";
            } else {
                $User_LoginId = '';
            }
            $_SelectQuery3 = "SELECT a.*, b.Course_Name, c.Batch_Name
                FROM tbl_admission AS a 
                INNER JOIN tbl_course_master AS b ON a.Admission_Course=b.Course_Code 
                INNER JOIN tbl_batch_master AS c ON a.Admission_Batch=c.Batch_Code 
                WHERE a.Admission_Course = '" . $_course . "' 
                AND a.Admission_Batch = '" . $_batch . "' 
                AND a.Admission_Payment_Status = '1'
                AND a.BioMatric_Status=1 " . $User_LoginId . " ORDER BY Admission_Date DESC";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddDeregisterLearner($LD_LearnerCode, $LD_ITGK_Code, $LD_Batch, $LD_Course, $_RowName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_LearnerCode);
            // $LD_Batch='112';
            //echo $_RowName['Admission_Name'];
            //echo $_RowName['Admission_Fname'];die;
            $_InsertQuery = "INSERT INTO `tbl_learner_deregister`(`LD_LearnerCode`, `LD_Name`, `LD_Fname`, `LD_ITGK_Code`, `LD_Batch`, `LD_Course`)
                                VALUES
                        ('" . $LD_LearnerCode . "','" . $_RowName['Admission_Name'] . "','" . $_RowName['Admission_Fname'] . "', '" . $LD_ITGK_Code . "', '" . $LD_Batch . "', '" . $LD_Course . "')";
            $_DuplicateQuery = "Select * From tbl_learner_deregister Where LD_LearnerCode='" . $LD_LearnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SendSMSTOITGK($LD_ITGK_Code, $learnercount) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $LD_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$LD_ITGK_Code);
            $_SelectQueryMOb = "select User_MobileNo from tbl_user_master where User_LoginId='" . $LD_ITGK_Code . "'";
            $_ResponseMob = $_ObjConnection->ExecuteQuery($_SelectQueryMOb, Message::SelectStatement);
            if ($_ResponseMob[0] == 'Success') {
                $_RowMob = mysqli_fetch_array($_ResponseMob[2], true);
                $_Mobile = $_RowMob['User_MobileNo'];
                $_SMS = "Your Application for DeRegistering " . $learnercount . " Learner(s) is in the process. You will be update soon.";
                SendSMS('9649900905', $_SMS);
                $_Response = 'Success';
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAttendanceCode($admission_code, $itgkcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $admission_code = mysqli_real_escape_string($_ObjConnection->Connect(),$admission_code);
            $itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkcode);
            //$_SelectQuery = "select * from vw_learner_attendance where Attendance_Admission_Code='" . $admission_code ."'";
            //echo $_SelectQuery;

            $_SelectQuery = "select count(t) as Days from 
(select distinct date(Attendance_In_Time) as t, Attendance_Admission_Code  
from tbl_learner_attendance where Attendance_Admission_Code='" . $admission_code . "' and
Attendance_ITGK_Code='" . $itgkcode . "') as tb 
 group by Attendance_Admission_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerByLID($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK_Code = $_SESSION['User_LoginId'];

                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 3 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 28) {
                    $_SelectQuery = "SELECT a.*,b.Course_Code, b.Course_Name,c.Batch_Code, c.Batch_Name FROM tbl_biomatric_registration AS a INNER JOIN tbl_course_master AS b ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.BioMatric_Admission_Batch = c.Batch_Code WHERE BioMatric_Admission_Code='" . $lcode . "'";
                } else if ($_SESSION['User_UserRoll'] == 7) {
                    $_SelectQuery = "SELECT a.*,b.Course_Code, b.Course_Name,c.Batch_Code, c.Batch_Name FROM tbl_biomatric_registration AS a INNER JOIN tbl_course_master AS b ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.BioMatric_Admission_Batch = c.Batch_Code WHERE BioMatric_Admission_Code='" . $lcode . "' AND BioMatric_ITGK_Code='" . $_ITGK_Code . "'";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if ($_Response[0] == "No Record Found" && $_SESSION['User_UserRoll'] != 7) {
                    $_SelectQuery = "SELECT 'NA' as BioMatric_Quality, 'NA' as BioMatric_NFIQ, 'NA' as BioMatric_ISO_Template, "
                            . "'NA' as BioMatric_ISO_Image,a.Admission_ITGK_Code as BioMatric_ITGK_Code,a.Admission_LearnerCode as"
                            . " BioMatric_Admission_LCode,a.Admission_Name as BioMatric_Admission_Name,"
                            . "a.Admission_Fname as BioMatric_Admission_Fname, a.Admission_DOB as BioMatric_Admission_DOB,"
                            . "a.Admission_Photo as BioMatric_Admission_Photo,a.Admission_Code as BioMatric_Admission_Code, b.Course_Name, c.Batch_Name FROM tbl_admission as a"
                            . " INNER JOIN tbl_course_master as b ON a.Admission_Course = b.Course_Code INNER JOIN tbl_batch_master as c ON"
                            . " a.Admission_Batch = c.Batch_Code WHERE Admission_Code='" . $lcode . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script>
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdDeregApplication($AdmCode, $lcodes, $OldIso_template, $num_rowsSco, $deregreason, $attnd, $Course, $Batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $lcodes = mysqli_real_escape_string($_ObjConnection->Connect(),$lcodes);
            $_Upd_User_Code = $_SESSION['User_LoginId'];
            $_InsertQuery = "INSERT INTO tbl_biomatric_deregistration_apply (Bio_Dereg_Apply_Admcode,Bio_Dereg_Apply_Lcode,Bio_Dereg_Apply_Oldisotemplate,Bio_Dereg_Apply_ITGK, Bio_Dereg_Apply_LMSscore, Bio_Dereg_Apply_Reason, Bio_Dereg_Apply_Attendance,Bio_Dereg_Apply_Status,Bio_Dereg_Apply_Course,Bio_Dereg_Apply_Batch) "
                    . "values ('" . $AdmCode . "','" . $lcodes . "','" . $OldIso_template . "','" . $_Upd_User_Code . "', '" . $num_rowsSco . "', '" . $deregreason . "', '" . $attnd . "','applied', '" . $Course . "', '" . $Batch . "')";

            $_DuplicateQuery = "Select * from tbl_biomatric_deregistration_apply where Bio_Dereg_Apply_Lcode = '" . $lcodes . "' and Bio_Dereg_Apply_Status = 'applied'";
            $_DuplicateResponse = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_DuplicateResponse);
            if ($_DuplicateResponse[0] == "No Record Found") {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = "You Have Alredy Applied for Deregistration for this Learner";
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
