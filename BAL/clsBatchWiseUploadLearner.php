<?php

/**
 * Description of clsBatchWiseUploadLearner
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsBatchWiseUploadLearner {

    //put your code here
	public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','4','5','3','23')
							order by Course_Code ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAdmissionBatchcode($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "'
								 ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	public function GetLearnerData($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				$_LoginUserRole = $_SESSION['User_UserRoll'];
				if($_LoginUserRole == '1' || $_SESSION['User_Code'] == '8'){
					$_SelectQuery = "SELECT Admission_LearnerCode,Admission_ITGK_Code,upper(Admission_Name) as Learner_Name,
								upper(Admission_Fname) as Father_Name,Admission_DOB as Learner_DOB,Admission_Mobile,
								Admission_Payment_Status,ITGK_Name,RSP_Code,RSP_Name
								FROM tbl_admission as a inner join vw_itgkname_distict_rsp as d on a.Admission_ITGK_Code=d.ITGKCODE
								where Admission_Batch='".$_batch."' and Admission_Course='".$_course."'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    
}
