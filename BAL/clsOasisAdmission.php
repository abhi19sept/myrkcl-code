<?php

/**
 * Description of clsOasisAdmission
 *
 * @author MAYANK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

date_default_timezone_set("Asia/Kolkata");

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

class clsOasisAdmission {
    //put your code here

    public function getUploadedFileName($learnercode, $type, $dir) {
        $basepath = '../upload/' . $dir . '/';
        $jpg = $learnercode . '_' . $type . '.jpg';
        $png = $learnercode . '_' . $type . '.png';
        $filename = '';
        if (file_exists($basepath . $jpg)) {
            $filename = $basepath . $jpg;
        } elseif (file_exists($basepath . $png)) {
            $filename = $basepath . $png;
        }

        return $filename;
    }

    public function getITGKCode($userCode, $_Tehsil) {
        $itgkCode = '';
        $response = $this->GetCenterDetails($userCode);
        if (mysqli_num_rows($response[2])) {
            $_Row = mysqli_fetch_array($response[2]);
            $centerCode = $_Row['User_LoginId'];
            if ($centerCode) {
                $response = $this->FILLCenterOasis($_Tehsil, $centerCode);
                if (mysqli_num_rows($response[2])) {
                    $_Row = mysqli_fetch_array($response[2]);
                    $itgkCode = $_Row['Courseitgk_ITGK'];
                }
            }
        }
        
        return $itgkCode;
    }

    public function getValidatedValues($values, $doClear = 1) {

        $photo = $this->getUploadedFileName($values['txtLearnercode'], 'photo', 'oasis_photo');
        $sign = $this->getUploadedFileName($values['txtLearnercode'], 'sign', 'oasis_sign');
        $minCertificate = $this->getUploadedFileName($values['txtLearnercode'], 'mincertificate', 'oasis_minimum_certificate');
        $ageProof = $this->getUploadedFileName($values['txtLearnercode'], 'ageproof', 'oasis_age_proof');
        $aadhaarProof = $this->getUploadedFileName($values['txtLearnercode'], 'aadhaar_card_proof', 'oasis_identity_proof');
        $idProof = ($values['ddlidproof'] != '') ? $this->getUploadedFileName($values['txtLearnercode'], 'identityproof', 'oasis_identity_proof') : 'NA';
        $subCategoryProof = ($values['workcategory'] != 'Other') ? $this->getUploadedFileName($values['txtLearnercode'], 'subcategoryproof', 'oasis_sub_category_proof') : 'NA';
        $highCertificate = ($values['hrEdu'] == 1) ? $this->getUploadedFileName($values['txtLearnercode'], 'highcertificate', 'oasis_highest_certificate') : 'NA';
        $casteProof = ($values['ddlCategory'] == 2 || $values['ddlCategory'] == 3) ? $this->getUploadedFileName($values['txtLearnercode'], 'casteproof', 'oasis_caste_proof') : 'NA';
        $mStatusProof = ($values['mstatus'] == 'Divorcee' || $values['mstatus'] == 'Widow' || $values['mstatus'] == 'Saperated') ? $this->getUploadedFileName($values['txtLearnercode'], 'maritalstatusproof', 'oasis_marital_status_proof') : 'NA';
        $phProof = ($values['PH'] == 'Yes') ? $this->getUploadedFileName($values['txtLearnercode'], 'physicaldisabilityproof', 'oasis_physical_disability_proof') : 'NA';
        $violanceProof = ($values['socialcategory'] == '1') ? $this->getUploadedFileName($values['txtLearnercode'], 'victimofviolenceproof', 'oasis_victim_of_violence_proof') : 'NA';

       // echo "$photo, $sign, $minCertificate, $ageProof, $idProof, $subCategoryProof, $highCertificate, $casteProof, $mStatusProof, $phProof, $violanceProof, $aadhaarProof";

        if (empty($photo) || empty($sign) || empty($minCertificate) || empty($ageProof) || empty($idProof) || empty($subCategoryProof) || empty($highCertificate) || empty($casteProof) || empty($mStatusProof) || empty($phProof) || empty($violanceProof) || empty($aadhaarProof)) {
            echo "Documents are not attached successfully, Please re-try by upload your documents again.";
            return;
        }

        $courseCode = 3;
        $dob = (!empty($values['dob'])) ? date("Y-m-d", strtotime($values['dob'])) : '';
        
        $_DOB = date("d-m-Y", strtotime($values['dob']));
        $compareBy = '01-01-2021';
        $diff = $this->calculateAge($_DOB, $compareBy);
        $age = $diff->y . '.' . $diff->m;

        $ddlWardno = (isset($values['ddlWardno'])) ? $values['ddlWardno'] : '';
        $ddlMunicipalName = (isset($values['ddlMunicipalName'])) ? $values['ddlMunicipalName'] : '';
        $ddlVillage = (isset($values['ddlVillage'])) ? $values['ddlVillage'] : '';
        $ddlPanchayat = (isset($values['ddlPanchayat'])) ? $values['ddlPanchayat'] : '';
        $ddlGramPanchayat = (isset($values['ddlGramPanchayat'])) ? $values['ddlGramPanchayat'] : '';
        $ddlQualification = (isset($values['ddlQualification'])) ? $values['ddlQualification'] : 3;

        $centerCode = $this->getITGKCode($values['ddlCenter'], $values['ddlTehsil']);
        $centerCode2 = ($values['ddlCenter2'] == '4332') ? 'NA' : $this->getITGKCode($values['ddlCenter2'], $values['ddlTehsil']);

        $txtPercentage = $txtYear = $ddlBoard = $txtHighRollNo = $ddlMonth = $ddlDivision = '';
        if ($values['hrEdu'] == 1) {
            $txtPercentage = $values['txtPercentage'];
            $txtYear = $values['txtYear'];
            $ddlBoard = $values['ddlBoard'];
            $txtHighRollNo = $values['txtHighRollNo'];
            $ddlMonth = $values['ddlMonth'];
            $ddlDivision = $values['ddlDivision'];
        }

        $tsp = (isset($values['TSP'])) ? $values['TSP'] : '';
        $ddlTspDistrict = (isset($values['ddlTspDistrict'])) ? $values['ddlTspDistrict'] : '';
        $ddlTspTehsil = (isset($values['ddlTspTehsil'])) ? $values['ddlTspTehsil'] : '';
        $nontspval = (isset($values['Nontspval'])) ? $values['Nontspval'] : '';

        $insertValues = [
            'Oasis_Admission_LearnerCode' => $values['txtLearnercode'],
            'Oasis_Admission_ITGK_Code' => $centerCode,
            'Oasis_Admission_ITGK_Code2' => $centerCode2,
            'Oasis_Admission_Course' => $courseCode,
            'Oasis_Admission_Batch' => $values['txtbatchcode'],
            'Oasis_Admission_Fee' => $values['txtCourseFee'],
            'Oasis_Admission_Installation_Mode' => $values['txtInstallMode'],
            'Oasis_Admission_Name' => preg_replace('/\s+/', ' ', $values['txtlname']),
            'Oasis_Admission_Fname' => preg_replace('/\s+/', ' ', $values['txtfname']),
            'Oasis_Admission_DOB' => $dob,
            'Oasis_Admission_Age' => $age,
            'Oasis_Admission_MotherName' => $values['txtmothername'],
            'Oasis_Admission_AreaType' => $values['area'],
            'Oasis_Admission_Mohalla' => '',
            'Oasis_Admission_WardNo' => $ddlWardno,
            'Oasis_Admission_Municipal' => $ddlMunicipalName,
            'Oasis_Admission_Village' => $ddlVillage,
            'Oasis_Panchayat' => $ddlPanchayat,
            'Oasis_GramPanchyat' => $ddlGramPanchayat,
            'Oasis_Admission_Photo' => $photo,
            'Oasis_Admission_Sign' => $sign,
            'Oasis_Admission_Gender' => $values['gender'],
            'Oasis_Admission_Subcategory' => $values['workcategory'],
            'Oasis_Admission_SocialCategory' => $values['socialcategory'],
            'Oasis_Admission_MaritalStatus' => $values['mstatus'],
            'Oasis_Admission_Medium' => $values['Medium'],
            'Oasis_Admission_PH' => $values['PH'],
            'Oasis_Admission_PID' => $values['ddlidproof'],
            'Oasis_Admission_UID' => $values['txtidno'],
            'Oasis_Admission_BhamashahNo' => $values['txtBhamashahNo'],
            'Oasis_Admission_District' => $values['ddlDistrict'],
            'Oasis_Admission_Tehsil' => $values['ddlTehsil'],
            'Oasis_Admission_Address' => addslashes(preg_replace('/\s+/', ' ', $values['txtAddress'])),
            'Oasis_Admission_PIN' => $values['txtPIN'],
            'Oasis_Admission_Mobile' => $values['txtApplicantmobile'],
            'Oasis_Admission_Phone' => $values['txtResiPh'],
            'Oasis_Admission_Email' => $values['txtemail'],
            'User_Code1' => $values['ddlCenter'],
            'User_Code2' => $values['ddlCenter2'],
            'Oasis_Admission_Qualification' => $ddlQualification,
            'Oasis_Admission_MinCertificate' => $minCertificate,
            'Oasis_Admission_AgeProof' => $ageProof,
            'Oasis_Admission_HighCertificate' => $highCertificate,
            'Oasis_Admission_CasteProof' => $casteProof,
            'Oasis_Admission_subCategoryProof' => $subCategoryProof,
            'Oasis_Admission_idProof' => $idProof,
            'Oasis_Admission_physicalProof' => $phProof,
            'Oasis_Admission_meritalStatusProof' => $mStatusProof,
            'Oasis_Admission_violanceProof' => $violanceProof,
            'Oasis_Admission_Category' => $values['ddlCategory'],
            'Oasis_Admission_Caste' => addslashes($values['txtcaste']),
            'Oasis_Admission_Minority' => $values['Minority'],
            'Oasis_Admission_Religion' => $values['ddlReligion'],
            'Oasis_Admission_BPL' => $values['BPL'],
            'Oasis_Admission_Percentage' => $txtPercentage,
            'Oasis_Admission_Year' => $txtYear,
            'Oasis_Admission_Board' => $ddlBoard,
            'Oasis_Admission_HighRollNo' => $txtHighRollNo,
            'Oasis_Admission_Month' => $ddlMonth,
            'Oasis_Admission_Division' => $ddlDivision,
            'Oasis_Admission_MINPercentage' => $values['txtMinPercentage'],
            'Oasis_Admission_MINRollNo' => $values['txtMinRollNo'],
            'Oasis_Admission_MinYear' => $values['txtMinYear'],
            'Oasis_Admission_MinBoard' => $values['ddlMinBoard'],
            'Oasis_Admission_MinMonth' => $values['ddlMinMonth'],
            'Oasis_Admission_MinDivision' => $values['ddlMinDivision'],
			'Oasis_Admission_MinSchoolType' => $values['ddlMinSchoolType'],
            'Oasis_ChooseTSP' => $tsp,
            'Oasis_TSP_District' => $ddlTspDistrict,
            'Oasis_TSP_Tehsil' => $ddlTspTehsil,
            'Oasis_NonTSP' => $nontspval,
            'Legislative_Assembly' => $values['Legislative_Assembly'],
        ];

        if ($doClear) {
            $insertValues = $this->doclearValues($insertValues, $values['hrEdu']);
        }

        $isValidInputs = $this->validateAllInputs($insertValues, $values['hrEdu']);

        $return = ($isValidInputs == 1) ? $insertValues : $isValidInputs;

        return $return;
    }
    
    public function Add($formData) {
        $values = unserialize($formData);
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $insertValues = $this->getValidatedValues($values);
            //print_r($insertValues); die;
            if (empty($insertValues) || !is_array($insertValues)) {
                echo $insertValues;
            } else {
                $insert = "Oasis_Admission_Code = ''";
                foreach ($insertValues as $field => $fieldValue) {
                    $insert .= ", " . $field . " = '" . $fieldValue . "' ";
                }
                $query = "SELECT Oasis_Admission_Code FROM tbl_oasis_admission WHERE Oasis_Admission_LearnerCode = '" . $insertValues['Oasis_Admission_LearnerCode'] . "' AND 
				Oasis_Admission_Batch in ('" . $insertValues['Oasis_Admission_Batch'] . "','295')  
				ORDER BY Oasis_Admission_Code ASC LIMIT 1";
                $_DuplicateResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                if (!mysqli_num_rows($_DuplicateResponse[2])) {
                    $_InsertQuery = "INSERT IGNORE INTO tbl_oasis_admission SET $insert, Oasis_Admission_Details = '" . addslashes(serialize($values)) . "'";
    				$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $query = "SELECT Oasis_Admission_Code FROM tbl_oasis_admission WHERE Oasis_Admission_LearnerCode = '" . $insertValues['Oasis_Admission_LearnerCode'] . "'  AND Oasis_Admission_Batch = '" . $insertValues['Oasis_Admission_Batch'] . "' ORDER BY Oasis_Admission_Code ASC LIMIT 1";
                    $_DuplicateResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                    if (mysqli_num_rows($_DuplicateResponse[2]) && $_Response[0] == Message::SuccessfullyInsert) {
                        $_SMS = "Dear Applicant " . ucwords(preg_replace('/\s+/', ' ', $insertValues['Oasis_Admission_Name'])) . ", Your application for RS-CIT Women course is successfully submitted to RKCL. Registered Application ID: " . $insertValues['Oasis_Admission_LearnerCode'];
                        SendSMS($insertValues['Oasis_Admission_Mobile'], $_SMS);
                       // $filename = $this->downloadApplicationFormById($insertValues['Oasis_Admission_LearnerCode'], $insertValues['Oasis_Admission_Batch']);
                        ?>
                        <script> 
                            window.location.href = "frmdownloadapplication.php";
                        </script> 
                        <?php
                    } else {
                        echo $_Response[0];
                    }
                } else {
                    ?>
                        <script> 
                        alert("Unable to process, as records already registred, Please re-try.");
                        //window.location.href = "frmlearnerotpverify.php";
                        </script> 
                        <?php
                }
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        //print_r($_Response);

        return $_Response;
    }

    public function invalidPrefixes() {
        $prefix = [
            '  ',
            'kum ',
            'kum. ',
            'ku ',
            'ku. ',
            'ku.',
            'sh ',
            'sh. ',
            'sh.',
            'mr ',
            'mr. ',
            'mr.',
            'ms ',
            'ms. ',
            'ms.',
            'mrs ',
            'mrs. ',
            'mrs.',
            'km ',
            'km. ',
            'km.',
            'kumari ',
            'shrimati ',
            'shrimati. ',
            'shrimati.',
            'shreemati ',
            'shreemati. ',
            'shreemati.',
            'shri mati ',
            'shri mati. ',
            'shri mati.',
            'shree mati ',
            'shree mati. ',
            'shree mati.',
            'shri ',
            'shri. ',
            'shri.',
            'shree ',
            'shree. ',
            'shree.',
            'smt ',
            'smt. ',
            'smt.',
            'late ',
            'late. ',
            'late.',
        ];

        return $prefix;
    }

    public function removeNamePrefixes($value) {
        $value = trim($value);
        $replace = $this->invalidPrefixes();
        foreach ($replace as $str) {
            $len = strlen($str);
            $subStr = strtolower(substr($value, 0, $len));
            if ($subStr == $str) {
                $value = substr($value, $len);
            }
        }

        $value = preg_replace('/[^A-Za-z0-9\. -]/', '', $value);

        return trim($value);
    }

    public function doclearValues($insertValues, $hrEdu) {
        $stringFields = [
            'Oasis_Admission_Name',
            'Oasis_Admission_Fname',
            'Oasis_Admission_MotherName',
            'Oasis_Admission_Address',
            'Oasis_Admission_Caste',
            'Oasis_Admission_UID',
            'Oasis_Admission_BhamashahNo',
            'Oasis_Admission_Email'
        ];
        
        foreach ($insertValues as $field => $value) {
            if (in_array($field, $stringFields)) {
                if ($field != 'Oasis_Admission_MotherName' && $field != 'Oasis_Admission_Email') {
                    $value = $this->removeNamePrefixes($value);
                }
            }
            $insertValues[$field] = trim($value);
        }

        $higherEdu = [
            'Oasis_Admission_Qualification',
            'Oasis_Admission_Board',
            'Oasis_Admission_Percentage',
            'Oasis_Admission_Division',
            'Oasis_Admission_Year',
            'Oasis_Admission_Month',
        ];

        if ($hrEdu != 1) {
            foreach ($higherEdu as $field) {
                $insertValues[$field] = '';
            }
        }

        $nonST = [
            'Oasis_ChooseTSP',
            'Oasis_TSP_District',
            'Oasis_TSP_Tehsil',
            'Oasis_NonTSP',
        ];
        if ($insertValues['Oasis_Admission_Category'] != '3') {
            foreach ($nonST as $field) {
                $insertValues[$field] = '';
            }
        } elseif ($insertValues['Oasis_ChooseTSP'] == 'TSP') {
             $insertValues['Oasis_NonTSP'] = '';
        } elseif ($insertValues['Oasis_ChooseTSP'] == 'NonTSP') {
            $insertValues['Oasis_TSP_District'] = '';
            $insertValues['Oasis_TSP_Tehsil'] = '';
        }

        if ($hrEdu != 1) {
            $insertValues['Oasis_Admission_HighCertificate'] = 'NA';
        }
        if ($insertValues['Oasis_Admission_PH'] == 'No') {
            $insertValues['Oasis_Admission_physicalProof'] = 'NA';
        }
        if ($insertValues['Oasis_Admission_Subcategory'] == 'Other') {
            $insertValues['Oasis_Admission_subCategoryProof'] = 'NA';
        }
        if ($insertValues['Oasis_Admission_SocialCategory'] == '0') {
            $insertValues['Oasis_Admission_violanceProof'] = 'NA';
        }
        if ($insertValues['Oasis_Admission_SocialCategory'] == '0') {
            $insertValues['Oasis_Admission_violanceProof'] = 'NA';
        }

        return $insertValues;
    }

    public function validateAllInputs($inputs, $hrEdu) {
        $isValid = 1;
        $checkforEmpty = [
            'Oasis_Admission_LearnerCode',
            'Oasis_Admission_ITGK_Code',
            'Oasis_Admission_ITGK_Code2',
            'Oasis_Admission_Course',
            'Oasis_Admission_Batch',
            'Oasis_Admission_District',
            'Oasis_Admission_Tehsil',
            'User_Code1',
            'User_Code2',
            'Oasis_Admission_Name',
            'Oasis_Admission_Fname',
            'Oasis_Admission_MotherName',
            'Oasis_Admission_DOB',
            'Oasis_Admission_Age',
            'Oasis_Admission_Gender',
            'Oasis_Admission_MaritalStatus',
            'Oasis_Admission_Medium',
            'Oasis_Admission_PH',
            'Oasis_Admission_Subcategory',
            'Oasis_Admission_UID',
            'Oasis_Admission_Address',
            'Oasis_Admission_PIN',
            'Oasis_Admission_Mobile',
            'Legislative_Assembly',
            'Oasis_Admission_Category',
            'Oasis_Admission_Caste',
            'Oasis_Admission_Minority',
            'Oasis_Admission_Religion',
            'Oasis_Admission_BPL',
            'Oasis_Admission_MinBoard',
            'Oasis_Admission_MINPercentage',
            'Oasis_Admission_MinDivision',
            'Oasis_Admission_MinSchoolType',
            'Oasis_Admission_MinYear',
            'Oasis_Admission_MinMonth',
            'Oasis_Admission_AreaType',
            'Oasis_Admission_Photo',
            'Oasis_Admission_Sign',
            'Oasis_Admission_MinCertificate',
            'Oasis_Admission_AgeProof',
            'Oasis_Admission_idProof',
        ];

        $ruralFields = [
            'Oasis_Panchayat',
            'Oasis_GramPanchyat',
            'Oasis_Admission_Village',
        ];

        $urbanFields = [
            'Oasis_Admission_Municipal',
            'Oasis_Admission_WardNo',
        ];

        $higherEdu = [
            'Oasis_Admission_Qualification',
            'Oasis_Admission_Board',
            'Oasis_Admission_Percentage',
            'Oasis_Admission_Division',
            'Oasis_Admission_Year',
            'Oasis_Admission_Month',
        ];

        $imgFields = [
            'Oasis_Admission_Photo' => ['photo', 'oasis_photo'],
            'Oasis_Admission_Sign' => ['sign', 'oasis_sign'],
            'Oasis_Admission_MinCertificate' => ['mincertificate', 'oasis_minimum_certificate'],
            'Oasis_Admission_AgeProof' => ['ageproof', 'oasis_age_proof'],
            'Oasis_Admission_HighCertificate' => ['highcertificate', 'oasis_highest_certificate'],
            'Oasis_Admission_CasteProof' => ['casteproof', 'oasis_caste_proof'],
            'Oasis_Admission_subCategoryProof' => ['subcategoryproof', 'oasis_sub_category_proof'],
            'Oasis_Admission_idProof' => ['identityproof', 'oasis_identity_proof'],
            'Oasis_Admission_physicalProof' => ['physicaldisabilityproof', 'oasis_physical_disability_proof'],
            'Oasis_Admission_meritalStatusProof' => ['maritalstatusproof', 'oasis_marital_status_proof'],
            'Oasis_Admission_violanceProof' => ['victimofviolenceproof', 'oasis_victim_of_violence_proof'],
        ];

        foreach ($checkforEmpty as $field) {
            if (empty($inputs[$field])) {
                $isValid = 'Error1: Unable to process, due to invalid input details.';
                break;
            }
        }

        if ($isValid == 1) {
            if ($inputs['Oasis_Admission_AreaType'] == 'Rural') {
                foreach ($ruralFields as $field) {
                    if (empty($inputs[$field])) {
                        $isValid = 'Error2: Unable to process, due to invalid input details.';
                        break;
                    }
                }
            } else if ($inputs['Oasis_Admission_AreaType'] == 'Urban') {
                foreach ($urbanFields as $field) {
                    if (empty($inputs[$field])) {
                        $isValid = 'Error3: Unable to process, due to invalid input details.';
                        break;
                    }
                }
            } else {
                $isValid = 'Error4: Unable to process, due to invalid input details.';
            }

            if ($isValid == 1 && ($hrEdu == 1 || $inputs['Oasis_Admission_Subcategory'] == 'Aaganwadi')) {
                foreach ($higherEdu as $field) {
                    if (empty($inputs[$field])) {
                        $isValid = 'Error5: Unable to process, due to invalid highest qualification details.';
                        break;
                    }
                }
                if ($inputs['Oasis_Admission_Percentage'] < 33 || $inputs['Oasis_Admission_Percentage'] > 100) {
                    $isValid = 'Error6: Unable to process, due to invalid highest qualification percentage.';
                }
            }

            if ($isValid == 1) {
                $prefixes = $this->invalidPrefixes();
                foreach ($prefixes as $str) {
                    $len = strlen($str);
                    $subStrName = strtolower(substr(trim($inputs['Oasis_Admission_Name']), 0, $len));
                    $subStrFname = strtolower(substr(trim($inputs['Oasis_Admission_Fname']), 0, $len));
                    if ($subStrName == $str || $subStrFname == $str) {
                       $isValid = "Error7: Unable to process, due to invalid name prefix '" . trim($str) . "' found either in applicant name or father name.";
                        break;
                    }
                }
            }

            if ($isValid == 1) {
                foreach ($imgFields as $field => $dir) {
                    if (empty($inputs[$field]) && $inputs[$field] != 'NA') {
                        $file = $this->getUploadedFileName($inputs['Oasis_Admission_LearnerCode'], $dir[0], $dir[1]);
                        if (! $file) {
                            $isValid = "Error8: Few of your documents are not uploaded successfully, Please re-try to upload your documents again.";
                            break;
                        }
                    }
                }
            }
        }

        if ($isValid == 1 && $inputs['User_Code1'] == $inputs['User_Code2']) {
            $isValid = "Error9: Both center preferences should be diffrent.";
        } elseif (!is_numeric($inputs['User_Code1']) || !is_numeric($inputs['User_Code2'])) {
            $isValid = 'Error10: Unable to process, due to invalid center preferences.';
        } elseif ($inputs['Oasis_Admission_MINPercentage'] < 33 || $inputs['Oasis_Admission_MINPercentage'] > 100) {
            $isValid = 'Error11: Unable to process, due to invalid minimum qualification percentage.';
        } elseif (strlen($inputs['Oasis_Admission_Name']) < 2 || strlen($inputs['Oasis_Admission_Fname']) < 2) {
            $isValid = 'Error12: Unable to process, due to invalid applicant name.';
        } elseif ($inputs['Oasis_Admission_Subcategory'] == 'Aaganwadi' && $inputs['Oasis_Admission_Qualification'] != 4 && $inputs['Oasis_Admission_Qualification'] != 5) {
            $isValid = 'Error13: Unable to process, due to invalid highest qualification.';
        } elseif ($inputs['Oasis_Admission_DOB']) {
            $dateDigits = explode('-', $inputs['Oasis_Admission_DOB']);
            if ($dateDigits[0] == 0 || $dateDigits[1] == 0 || $dateDigits[2] == 0) {
                $isValid = 'Error14: Unable to process, due to invalid date of birth.';
            } elseif ($inputs['Oasis_Admission_Age'] < 16 || $inputs['Oasis_Admission_Age'] > 40) {
                $isValid = 'Error15: Unable to process, due to invalid date of birth as age should be between 16 to 40 years.';
            }
        }

        return $isValid;
    }

    public function getAccountExistance($lname, $fname, $dob, $aadhaar, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $dob = date("d-m-Y", strtotime($dob));
        $lname = $this->removeNamePrefixes($lname);
        $lname = trim(str_replace(' ', '', $lname));
        $lname = preg_replace('/\s+/', ' ',$lname);
        $lname = str_replace('.', '', $lname);

        $fname = $this->removeNamePrefixes($fname);
        $fname = trim(str_replace(' ', '', $fname));
        $fname = preg_replace('/\s+/', ' ',$fname);
        $fname = str_replace('.', '', $fname);
        
        $query = "SELECT Oasis_Admission_UID  FROM tbl_oasis_admission WHERE Oasis_Admission_UID = '" . $aadhaar . "' AND Oasis_Admission_Batch = '" . $batch . "' and Oasis_Admission_Course='3' LIMIT 1";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

        if (!mysqli_num_rows($_Response[2])) {
            $dobYmd = date("Y-m-d", strtotime($dob));
            $query = "SELECT Course AS Oasis_Admission_Course, Batch AS Oasis_Admission_Batch, 'Reported' AS Oasis_Admission_LearnerStatus FROM tbl_oasis_reported WHERE (DOB = '" . $dob . "' OR DOB = '" . $dobYmd . "') AND REPLACE(REPLACE(LearnerName, ' ', ''), '.', '') LIKE ('" . $lname . "') AND REPLACE(REPLACE(FatherName, ' ', ''), '.', '') LIKE ('" . $fname . "') LIMIT 1";
            $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            if (!mysqli_num_rows($_Response[2])) {
                $dob = date("Y-m-d", strtotime($dob));
                $query = "SELECT Oasis_Admission_Course, Oasis_Admission_Batch, Oasis_Admission_LearnerStatus FROM tbl_oasis_admission WHERE Oasis_Admission_Batch in('" . $batch . "','295') AND Oasis_Admission_DOB = '" . $dob . "' AND REPLACE(REPLACE(Oasis_Admission_Name, ' ', ''), '.', '') LIKE ('" . $lname . "') AND REPLACE(REPLACE(Oasis_Admission_Fname, ' ', ''), '.', '') LIKE ('" . $fname . "') LIMIT 1";
                $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            }
        }

        return $_Response;
    }

    public function getCenterApplications($centercode, $batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
			$centercode = mysqli_real_escape_string($_ObjConnection->Connect(),$centercode);
			$batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
        $query = "SELECT COUNT(*) as n FROM tbl_oasis_admission oa INNER JOIN tbl_user_master as um ON (um.User_LoginId = oa.Oasis_Admission_ITGK_Code OR um.User_LoginId = oa.Oasis_Admission_ITGK_Code2) WHERE um.User_Code = '" . $centercode . "' AND oa.Oasis_Admission_Batch = '" . $batchcode . "'";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        $rows = mysqli_fetch_array($_Response[2]);

        return $rows['n'];
    }

    public function Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_User_Code = $_SESSION['User_Code'];
			
			$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			
            $_UpdateQuery = "Update tbl_admission set Admission_Name='" . $_LearnerName . "',"
                    . "Admission_Fname='" . $_ParentName . "'," . "Admission_DOB='" . $_DOB . "', " . "Admission_Photo='" . $newpicture . "',"
                    . "Admission_Sign='" . $newsign . "'"
                    . " Where Admission_Code='" . $_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
                    . "Admission_Log_ITGK_Code,"
                    . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                    . "'" . $_Code . "' as Admission_Log_Code,"
                    . "'" . $_ITGK_Code . "' as Admission_Log_ITGK_Code,"
                    . "'" . $newpicture . "' as Admission_Log_Photo,'" . $newsign . "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"
                    . "'" . $_User_Code . "' as Admission_Log_User_Code"
                    . " From tbl_admission_log";
            $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_AdmissionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AdmissionCode);
				
            $_SelectQuery = "Select * From tbl_admission Where Admission_Code='" . $_AdmissionCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				
            $_SelectQuery = "Select Course_Fee FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
//echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionInstall($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Admission_Installation_Mode FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
// echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetIntakeAvailable($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ITGK_Code = $_SESSION['User_LoginId'];
            $_SelectQuery = "Select Intake_Available FROM tbl_intake_master WHERE Intake_Center = '" . $_ITGK_Code . "' AND Intake_Batch = " . $_batchcode . "";
// echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function FILLCenterOasis($_Tehsil, $itgkCode = 0) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $filter = ($itgkCode) ? "AND a.Courseitgk_ITGK = '" . $itgkCode . "'" : '';
            $_Tehsil = ($_Tehsil==161) ? $_Tehsil . ",162,163,164,165" : $_Tehsil;
            $_SelectQuery = "Select DISTINCT a.Courseitgk_ITGK, c.Organization_Name, c.Organization_User From tbl_courseitgk_mapping as a INNER JOIN tbl_user_master as b ON a.Courseitgk_ITGK=b.User_LoginId INNER JOIN tbl_organization_detail as c ON b.User_Code=c.Organization_User WHERE Organization_Tehsil IN (" . $_Tehsil . ") AND CourseITGK_BlockStatus='unblock' AND Courseitgk_Course='RS-CIT Women' AND Courseitgk_EOI = 32 AND b.User_UserRoll = 7 $filter ORDER BY a.Courseitgk_ITGK";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCenterDetails($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_User = $_SESSION['User_LoginId'];
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, a.Organization_Address, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.*"
                    . " FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code INNER JOIN tbl_tehsil_master as d"                    
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e"
                    . " ON a.Organization_User=e.User_Code WHERE Organization_User = '" . $_CenterCode . "' AND e.User_UserRoll = 7";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLTSPDistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_tsp_district WHERE TSP_District_Status = '1'";
//echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	public function FILLTSPTehsil($_DistrictCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DistrictCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DistrictCode);
            $_SelectQuery = "Select * FROM tbl_tsp_tehsil WHERE TSP_Tehsil_District = '" . $_DistrictCode . "'";
//echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function calculateAge($date1,$date2)
    {
        // date2 should be greater then to date1
        $date1 = date_create($date1); //date of birth
        //creating a date object
        $date2 = date_create($date2); //current date 

        return date_diff($date2, $date1);
    }
	public function GetBatchFromLcode($_formpostdata) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$lcode = $_formpostdata['txtApplicationId'];
			if($lcode!=''){
				$_SelectQuery = "select Oasis_Admission_LearnerCode,Oasis_Admission_Course,Oasis_Admission_Batch from tbl_oasis_admission
							where Oasis_Admission_LearnerCode='".$lcode."' and Oasis_Admission_Batch in ('294','295')";
			}
            else{
				$lname = $_formpostdata['txtApplicantName'];
				$fname = $_formpostdata['txtApplicantFName'];
				$dob = date("Y-m-d", strtotime($_formpostdata['txtApplicantDOB']));
				
				$_SelectQuery = "select Oasis_Admission_LearnerCode,Oasis_Admission_Course,Oasis_Admission_Batch from tbl_oasis_admission
							where Oasis_Admission_Name='".$lname."' and Oasis_Admission_Fname='".$fname."' and
							Oasis_Admission_DOB='".$dob."' and Oasis_Admission_Batch in ('294','295')";
			}

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

/*	public function GetBatchFromLcode($_formpostdata) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$lcode = $_formpostdata['txtApplicationId'];
            $_SelectQuery = "select Oasis_Admission_Course,Oasis_Admission_Batch from tbl_oasis_admission
							where Oasis_Admission_LearnerCode='".$lcode."' and Oasis_Admission_Batch in ('249','250')";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	*/

    public function downloadApplicationFormById($formdata,$batchId,$coursecode) {
		//print_r($formdata);die;
        global $_ObjConnection;
        global $general;

        $_ObjConnection->Connect();

        $appId = $formdata['txtApplicationId'];

        $path = '../upload/wcd/batch_' . $batchId;
        $this->makeDir($path);

        if (!$appId) {
            $lname = $formdata['txtApplicantName'];
            $fname = $formdata['txtApplicantFName'];
            $dob = $formdata['txtApplicantDOB'];

            $dob = date("d-m-Y", strtotime($dob));
            $dobYmd = date("Y-m-d", strtotime($dob));
            $lname = $this->removeNamePrefixes($lname);
            $lname = trim(str_replace(' ', '', $lname));
            $lname = preg_replace('/\s+/', ' ',$lname);
            $lname = str_replace('.', '', $lname);

            $fname = $this->removeNamePrefixes($fname);
            $fname = trim(str_replace(' ', '', $fname));
            $fname = preg_replace('/\s+/', ' ',$fname);
            $fname = str_replace('.', '', $fname);

            $filter = "AND (Oasis_Admission_DOB = '" . $dob . "' OR Oasis_Admission_DOB = '" . $dobYmd . "') AND REPLACE(REPLACE(Oasis_Admission_Name, ' ', ''), '.', '') LIKE ('" . $lname . "') AND REPLACE(REPLACE(Oasis_Admission_Fname, ' ', ''), '.', '') LIKE ('" . $fname . "')";
        } else {
            $filter = "AND ad.Oasis_Admission_LearnerCode = '" . $appId . "'";
            $fileName = 'wcd_' . $appId . '.pdf';
            $resultFilePath = $path . '/' . $fileName;
            if (file_exists($resultFilePath)) {
               //return $resultFilePath;
            }
        }
		
		if($coursecode=='3'){
			$masterPdf = '../upload/wcd/application_form_master.pdf';
			$return = 'No Record Found';
		}
		else{
			$masterPdf = '../upload/wcd/application_form_master_RSCFA.pdf';
			$return = 'No Record Found';
		}

        

   $query = "SELECT ad.*, th.Tehsil_Name, dm.District_Name, tspth.TSP_Tehsil_Name, tspdm.TSP_District_Name, rm.Religion_Name, bm.Board_Name AS Min_Board_Name,hscb.Board_Name AS HSC_Board_Name, hbm.HighBoard_Name AS High_Board_Name, mm.Municipality_Name, vm.Village_Name, la.name AS Legislative_Assembly_Name, gp.GP_Name AS GramPanchyat_Name, ps.Block_Name AS Panchayat_Name, qm.Qualification_Name FROM tbl_oasis_admission ad INNER JOIN tbl_tehsil_master th ON th.Tehsil_Code = ad.Oasis_Admission_Tehsil 
INNER JOIN tbl_district_master dm ON dm.District_Code = th.Tehsil_District INNER JOIN tbl_religion_master rm ON rm.Religion_Code = ad.Oasis_Admission_Religion INNER JOIN tbl_board_master bm ON bm.Board_Code = ad.Oasis_Admission_MinBoard
LEFT JOIN tbl_board_master as hscb ON hscb.Board_Code = ad.Oasis_Admission_HSCBoard
 INNER JOIN tbl_legislative_assembly_constituency_list la ON la.id = ad.Legislative_Assembly LEFT JOIN tbl_highboard_master hbm ON hbm.HighBoard_Code = ad.Oasis_Admission_Board 
    LEFT JOIN tbl_tsp_tehsil tspth ON tspth.TSP_Tehsil_Code = ad.Oasis_TSP_Tehsil
LEFT JOIN tbl_tsp_district tspdm ON tspdm.TSP_District_Code = tspth.TSP_Tehsil_District LEFT JOIN tbl_municipality_master mm ON mm.Municipality_Raj_Code = ad.Oasis_Admission_Municipal LEFT JOIN tbl_village_master vm ON vm.Village_Code = ad.Oasis_Admission_Village LEFT JOIN tbl_gram_panchayat gp ON gp.GP_Code = ad.Oasis_GramPanchyat LEFT JOIN tbl_panchayat_samiti ps ON ps.Block_Code = ad.Oasis_Panchayat LEFT JOIN tbl_qualification_master qm ON qm.Qualification_Code = ad.Oasis_Admission_Qualification WHERE ad.Oasis_Admission_Batch = '" . $batchId . "' $filter ORDER BY ad.Oasis_Admission_Code ASC LIMIT 1";

        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            $data = mysqli_fetch_assoc($_Response[2]);
            if ($data) {
                $pdfData = $this->generatePdfData($data);
                $fdfPath = $this->generateFDF($pdfData, $path);
                if ($fdfPath) {
                    $fileName = 'wcd_' . $pdfData['learnercode'] . '.pdf';
                    $resultFilePath = $path . '/' . $fileName;
                    $photo = '';
                    $this->generateWcdFilledForm($pdfData['learnercode'], $path, $fdfPath, $masterPdf, $resultFilePath, $coursecode);
                    if (file_exists($resultFilePath)) {
                       $return = $resultFilePath;
                    }
                }
            }
        }

        return $return;
    }

    public function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }

    public function generatePdfData($data) {
        $name = explode(' ', $data['Oasis_Admission_Name']);
        $rawData = unserialize($data['Oasis_Admission_Details']);
        if($data['Oasis_Admission_MinSchoolType'] == '1'){
            $Oasis_Admission_MinSchoolType = "Government School";
        }
        else{
            $Oasis_Admission_MinSchoolType ="Private or Other School";
        }
        $pdfData = [
            'Oasis_Admission_ITGK_Code' => $data['Oasis_Admission_ITGK_Code'],
            'Oasis_Admission_ITGK_Code2' => $data['Oasis_Admission_ITGK_Code2'],
            'Organization_Name1' => ucwords(strtolower($rawData['txtName1'])),
            'Organization_Name2' => ucwords(strtolower($rawData['txtName2'])),
            'Oasis_Admission_FirstName' => $name[0],
            'Oasis_Admission_MidName' => (count($name) > 2 && isset($name[1])) ? $name[1] : '',
            'Oasis_Admission_LastName' => (count($name) < 3 && isset($name[1])) ? $name[1] : ((isset($name[1])) ? trim(str_replace($name[1] . ' ', '', str_replace($name[0] . ' ', '', $data['Oasis_Admission_Name']))) : ''),
            'Oasis_Admission_Fname' => $data['Oasis_Admission_Fname'],
            'Oasis_Admission_MotherName' => $data['Oasis_Admission_MotherName'],
            'Oasis_Admission_Subcategory' => $data['Oasis_Admission_Subcategory'],
            'Oasis_Admission_SocialCategory' => $data['Oasis_Admission_SocialCategory'],
            'Oasis_Admission_MaritalStatus' => $data['Oasis_Admission_MaritalStatus'],
            'Oasis_Admission_DOB' => date("d / m / Y", strtotime($data['Oasis_Admission_DOB'])),
            'Oasis_Admission_Mobile' => $data['Oasis_Admission_Mobile'],
            'Oasis_Admission_Email' => $data['Oasis_Admission_Email'],
            'Oasis_Admission_UID' => $data['Oasis_Admission_UID'],
            'Oasis_Admission_BhamashahNo' => $data['Oasis_Admission_BhamashahNo'],
            'Oasis_Admission_Address' => $data['Oasis_Admission_Address'],
            'City_Village_Name' => trim($data['Municipality_Name'] . ' ' . $data['Village_Name']),
            'Oasis_Admission_Panchayat_Assembaly' => ($data['Oasis_Admission_AreaType'] == 'Urban') ? $data['Legislative_Assembly_Name'] : $data['GramPanchyat_Name'] . ', ' . $data['Panchayat_Name'],
            'Oasis_Admission_District' => $data['District_Name'],
            'Oasis_Admission_Tehsil' => $data['Tehsil_Name'],
            'Oasis_Admission_PIN' => $data['Oasis_Admission_PIN'],
            'Oasis_Admission_Category' => $data['Oasis_Admission_Category'],
            'Oasis_TSP_Tehsil' => $data['TSP_Tehsil_Name'],
            'Oasis_TSP_District' => $data['TSP_District_Name'],
            'Oasis_Admission_Caste' => $data['Oasis_Admission_Caste'],
            'Oasis_Admission_Religion' => $data['Oasis_Admission_Religion'],
			
			'Oasis_Admission_Twl_Board' => $data['HSC_Board_Name'],
			'Oasis_Admission_Twl_Roll' => $data['Oasis_Admission_HSCRollNo'],
			'Oasis_Admission_Twl_Percentage' => $data['Oasis_Admission_HSCPercentage'],
			'Oasis_Admission_Twl_Year_Month' => date("F, Y", strtotime($data['Oasis_Admission_HSCYear'] . '/' . $data['Oasis_Admission_HSCMonth'] . "/01")),
			'Oasis_Admission_MIN_Roll' => $data['Oasis_Admission_MINRollNo'],
			'Oasis_Admission_Roll' => $data['Oasis_Admission_HighRollNo'],
	           'Oasis_School_Type' => $Oasis_Admission_MinSchoolType,
            'Oasis_Admission_MinBoard' => $data['Min_Board_Name'],
            'Oasis_Admission_MINPercentage' => $data['Oasis_Admission_MINPercentage'],
            'Oasis_Admission_MinYear_Month' => date("F, Y", strtotime($data['Oasis_Admission_MinYear'] . '/' . $data['Oasis_Admission_MinMonth'] . "/01")),
            'Oasis_Admission_Board' => $data['High_Board_Name'],
            'Oasis_Admission_Qualification' => $data['Qualification_Name'],
            'Oasis_Admission_Percentage' => ($data['Oasis_Admission_Percentage'] > 0) ? $data['Oasis_Admission_Percentage'] : '',
            'Oasis_Admission_Year_Month' => (!empty($data['Oasis_Admission_Year']) && !empty($data['Oasis_Admission_Month'])) ? date("F, Y", strtotime($data['Oasis_Admission_Year'] . '/' . $data['Oasis_Admission_Month'] . "/01")) : '',
            'Oasis_Admission_Place' => $data['Tehsil_Name'],
            'Oasis_Admission_Addtime' => date("d/m/Y", strtotime($data['Addtime'])),
            'Oasis_Admission_Addtime' => date("d/m/Y h:i:s a", strtotime($data['Addtime'])),
            'learnercode' => $data['Oasis_Admission_LearnerCode'],
        ];

        return $pdfData;
    }


    public function generateFDF($data, $path) {
        $fdfContent = '%FDF-1.2
            %Ã¢Ã£Ã?Ã“
            1 0 obj 
            <<
            /FDF 
            <<
            /Fields [';
        foreach ($data as $key => $val) {
            $fdfContent .= '
                    <<
                    /V (' . $val . ')
                    /T (' . $key . ')
                    >> 
                    ';
        }
        $fdfContent .= ']
            >>
            >>
            endobj 
            trailer
            <<
            /Root 1 0 R
            >>
            %%EOF';

        $filePath = $path . '/' . $data['learnercode'] . '.fdf';
        file_put_contents($filePath, $fdfContent);
        $return = '';
        if (file_exists($filePath)) {
            $return = $filePath;
        }

        return $return;
    }

    public function generateWcdFilledForm($learnercode, $path, $fdfPath, $masterPdf, $resultFilePath, $coursecode) {

        $pdftkPath = ($_SERVER['HTTP_HOST']=="myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : (($_SERVER['HTTP_HOST']=="staging.rkcl.co.in") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"') ;
        
        $command = $pdftkPath . '  ' . $masterPdf . ' fill_form   ' . $fdfPath . ' output  '. $resultFilePath . ' flatten';
        exec($command);
        unlink($fdfPath);
       /* $Imagepath = $this->getUploadedFileName($learnercode, 'photo', 'oasis_photo');
        $this->addPhotoInPDF($learnercode, $Imagepath, $path, $resultFilePath, [160, 80, 30, 30.5], 'photo');

        $signPath = $this->getUploadedFileName($learnercode, 'sign', 'oasis_sign');
        $this->addPhotoInPDF($learnercode, $signPath, $path, $resultFilePath, [143, 325, 31, 8], 'sign');//L,T,R,B*/
        $Imagepath = $this->getUploadedFileName($learnercode, 'photo', 'oasis_photo');
            if($coursecode=='3'){
                $this->addPhotoInPDF($learnercode, $Imagepath, $path, $resultFilePath, [152, 71, 30, 30.5], 'photo');
            }else{
                $this->addPhotoInPDF($learnercode, $Imagepath, $path, $resultFilePath, [152, 80, 30, 30.5], 'photo');
            }
            

        $signPath = $this->getUploadedFileName($learnercode, 'sign', 'oasis_sign');
            if($coursecode=='3'){
                $this->addPhotoInPDF($learnercode, $signPath, $path, $resultFilePath, [146, 319, 31, 8], 'sign');//L,T,R,B
            }else{
                $this->addPhotoInPDF($learnercode, $signPath, $path, $resultFilePath, [157, 330, 31, 8], 'sign');//L,T,R,B
            }
    }

    public function addPhotoInPDF($learnercode, $Imagepath, $path, $pdfPath, $cords, $type) {

        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        require_once("cropper.php");

        global $general;
        $cropperpath = '../upload/wcd/cropper/';
        $this->makeDir($cropperpath);
        
        //echo $Imagepath;

        //echo $Imagepath; die;
        $picResource = 0;
        if (!empty($Imagepath) && file_exists($Imagepath)) {
            $picResource = imagecreatefromstring(file_get_contents($Imagepath));
        }
        $pdf = new FPDI('P');
        $pdf->AddPage('P','Legal');
        $pdf->setSourceFile($pdfPath);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        try {
            if(!empty($Imagepath) && $picResource) $pdf->Image($Imagepath, $cords[0], $cords[1], $cords[2], $cords[3]);
        } catch (Exception $_ex) {
            $pdf->Output($pdfPath, "F");

            if (!empty($Imagepath) && file_exists($Imagepath)) {
                $cropper = new Cropper($cropperpath);
                $args = array("size" => (($type == 'sign') ? [31,10] : [31,31]), "image" => $Imagepath, "placeholder" => 2);
                $newImgPath = $cropper->crop($args);

                if (file_exists($newImgPath)) {
                    $this->addPhotoInPDF($learnercode, $newImgPath, $path, $pdfPath, $cords, $type);
                }
            }
            return;
        }

        $pdf->Output($pdfPath, "F");
    }
	
	public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
			$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
