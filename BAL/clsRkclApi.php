<?php
/**
 * Description of clsParentFunctionMaster
 *
 *  Author Name:  Sunil Kumar Baindara
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRkclApi {
    //put your code here
    
    //*/Get all data record form function table//*//
    
    
     public function GetAllFunctionName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_rkcl_api_functions Order By functionname asc";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    //*/Get all data record form function table//*//
    
    //*/Get all data record form function table//*//
    
    
     public function GetTableFiledByTabName($ftablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='database_rkclcoin_1' AND `TABLE_NAME`='" . $ftablename . "' ";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    //*/Get all data record form function table//*//
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_rkcl_api_permissions Order By rkclAPi_id desc";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_rkclAPi_id)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_rkclAPi_id = mysqli_real_escape_string($_ObjConnection->Connect(),$_rkclAPi_id);
				
            $_SelectQuery = "Select ClientName, Apikey, ClientIP, Status From tbl_rkcl_api_permissions Where 
				rkclAPi_id='" . $_rkclAPi_id . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Root_Menu_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Root_Menu_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Root_Menu_Code);
				
            $_DeleteQuery = "Delete From tbl_rkcl_api_permissions Where rkclAPi_id='" . $_Root_Menu_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ClientName,$_Apikey,$_ClientIP,$_Status) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
               $_ClientName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ClientName);
			   
            $_InsertQuery = "INSERT INTO tbl_rkcl_api_permissions (ClientName, Apikey, ClientIP, Status)
                                      VALUES ('".$_ClientName."','".$_Apikey."','".$_ClientIP."','".$_Status."')";
            $_DuplicateQuery = "Select * From tbl_rkcl_api_permissions Where ClientName='" . $_ClientName . "' ";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                 $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }        
                
                
                
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_RootCode,$_ClientName,$_Apikey,$_ClientIP,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootCode);
				
            $_UpdateQuery = "Update tbl_rkcl_api_permissions set ClientName='" . $_ClientName . "',"
                    . "Apikey='" . $_Apikey . "',ClientIP='" . $_ClientIP . "',Status='" . $_Status . "' Where
					rkclAPi_id ='" . $_RootCode . "'";
           /* $_DuplicateQuery = "Select * From tbl_rkcl_api_permissions Where rkclAPi_id ='" . $_RootCode . "' ";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {*/
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           /* }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }*/
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
