<?php

/**
 * Description of clsPrintRecpReexam
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response2 = array();

class clsPrintRecpReexam {

    //put your code here
    public function GetDataAll($_event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginUserRole = $_SESSION['User_UserRoll'];
			$_event = mysqli_real_escape_string($_ObjConnection->Connect(),$_event);
			
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11') {

                $_SelectQuery = "select a.*,b.Reexam_Transaction_timestamp from examdata as a inner join tbl_reexam_transaction as b on a.reexam_TranRefNo=b.Reexam_Transaction_Txtid where examid='" . $_event . "' AND learnertype='reexam' AND paymentstatus='1'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
/**			else {
                //print_r( $_SESSION['User_LoginId']);
                $_SelectQuery = "select a.*,b.Reexam_Transaction_timestamp from examdata as a inner join tbl_reexam_transaction as b on a.reexam_TranRefNo=b.Reexam_Transaction_Txtid where itgkcode =" . $_SESSION['User_LoginId'] . " AND examid='" . $_event . "'  AND learnertype='reexam' AND paymentstatus='1' ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
		**/
		
		elseif($_LoginUserRole == '7') {
                //print_r( $_SESSION['User_LoginId']);
                $_SelectQuery = "select a.*,b.Reexam_Transaction_timestamp from examdata as a inner join tbl_reexam_transaction as b on a.reexam_TranRefNo=b.Reexam_Transaction_Txtid where itgkcode =" . $_SESSION['User_LoginId'] . " AND examid='" . $_event . "'  AND learnertype='reexam' AND paymentstatus='1' ";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
            elseif($_LoginUserRole == '19') {
                //print_r( $_SESSION['User_LoginId']);
                $_SelectQuery = "select a.*,b.Reexam_Transaction_timestamp from examdata as a inner join tbl_reexam_transaction as b on a.reexam_TranRefNo=b.Reexam_Transaction_Txtid where itgkcode =" . $_SESSION['User_LoginId'] . " AND examid='" . $_event . "'  AND learnertype='reexam' AND paymentstatus='1' AND a.learnercode='".$_SESSION['User_LearnerCode']."'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
			


            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllPrintRecpDwnld($eid) { /// for new invoice system GST
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$eid = mysqli_real_escape_string($_ObjConnection->Connect(),$eid);
				
			
         /**    $_SelectQuery = "Select a.*, Gst_Invoce_SAC,b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,
                b.amount,b.addtime,c.Organization_Name, District_Name,Gst_Invoce_CGST,Gst_Invoce_SGST,Admission_Mobile,
                Batch_Name, Gst_Invoce_TotalFee,Gst_Invoce_VMOU,Gst_Invoce_TutionFee FROM examdata  
                as a inner join tbl_reexam_invoice as b on a.examdata_code=b.exam_data_code inner join tbl_user_master as e on 
                a.itgkcode=e.User_LoginId inner join tbl_organization_detail as c on e.User_Code=c.Organization_User 
                inner join tbl_batch_master as d on a.batchname=d.Batch_Code  inner join tbl_district_master as g on 
                Organization_District=District_Code inner join tbl_gst_invoice_master as f on a.examid=f.Gst_Invoce_Category_Id 
                inner join tbl_admission as h on a.learnercode=h.Admission_LearnerCode 
                 WHERE  a.examdata_code = '" . $eid . "' AND a.paymentstatus='1' and Gst_Invoce_Category_Type='Reexam' "; **/
				 
				  $_SelectQuery = "Select a.*, Gst_Invoce_SAC,b.invoice_no,Gst_Invoce_TutionFee,Gst_Invoce_BaseFee,
                b.amount,b.addtime,c.Organization_Name, District_Name,Gst_Invoce_CGST,Gst_Invoce_SGST,Admission_Mobile,
                Batch_Name, Gst_Invoce_TotalFee ,Gst_Invoce_VMOU,Gst_Invoce_TutionFee FROM examdata  
                as a inner join tbl_reexam_invoice as b on a.examdata_code=b.exam_data_code inner join tbl_user_master as e on 
                a.itgkcode=e.User_LoginId inner join tbl_organization_detail as c on e.User_Code=c.Organization_User 
                inner join tbl_batch_master as d on a.batchname=d.Batch_Code  inner join tbl_district_master as g on 
                Organization_District=District_Code inner join tbl_gst_invoice_master as f on a.examid=f.Gst_Invoce_Category_Id 
                inner join tbl_admission as h on a.learnercode=h.Admission_LearnerCode 
                 WHERE  a.examdata_code = '" . $eid . "' AND a.paymentstatus='1' and Gst_Invoce_Category_Type='Reexam' ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function addPhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        if ($coursecode == '5') {
            $Imagepath = '../upload/DownloadPrintRecpt/rscfa.jpg';
        } else {
            $Imagepath = '../upload/DownloadPrintRecpt/rscit.jpg';
        }

        $pdf->Image($Imagepath, 8, 105, 50, 25);
        $pdf->Image($Imagepath, 8, 251, 50, 25);

        $pdf->Output($path, "F");
    }

    public function convert_number_to_words($number) {

        $hyphen = ' ';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'Zero',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Fourty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100 => 'Hundred',
            1000 => 'Thousand',
            1000000 => 'Million',
            1000000000 => 'Billion',
            1000000000000 => 'Trillion',
            1000000000000000 => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

}
