<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPremisesDetail
 *
 *  author Yogendra
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsquestionanswers {

    //put your code here





    public function Showsurvey() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');

            $_SelectQuery = "Select * from tbl_survey as a inner join tbl_survey_master as b on a.Servey_Code=b.Survey_id";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRecord($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_DeleteQuery = "Delete From tbl_survey Where survey_id='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
			 $_SelectQuery = "Select * From tbl_survey Where survey_id='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_code, $_surveyid, $txtQuestion, $_txtoption1, $_txtoption2, $_txtoption3) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
				$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
				
            $_UpdateQuery = "Update tbl_survey set Servey_Code='" . $_surveyid . "',
			Servey_Question='" . $txtQuestion . "',
			Servey_option1='" . $_txtoption1 . "',
			Servey_option2='" . $_txtoption2 . "',
			Servey_option3='" . $_txtoption3 . "'
			Where 	survey_id='" . $_code . "'";
            $_DuplicateQuery = "Select * From tbl_survey Where Servey_option1='" . $_txtoption2 . "' OR Servey_option1='" . $_txtoption2 . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function INSERT($_surveyid, $txtQuestion, $_txtoption1, $_txtoption2, $_txtoption3) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');
			$txtQuestion = mysqli_real_escape_string($_ObjConnection->Connect(),$txtQuestion);
			
            $_InsertQuery = "Insert Into tbl_survey(Servey_Code,Servey_Question,Servey_option1,Servey_option2,Servey_option3) 
												 VALUES ('" . $_surveyid . "','" . $txtQuestion . "',
												 '" . $_txtoption1 . "','" . $_txtoption2 . "','" . $_txtoption3 . "'
												  
												   )";

            $_DuplicateQuery = "Select * From tbl_survey Where Servey_Question='" . $txtQuestion . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

            //print_r($_Response);

            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

                // $_UpdateQuery = "Update tbl_intake_master set Intake_Available=Intake_Available - 1 ,Intake_Consumed = Intake_Consumed + 1 Where Intake_Center = '" .$_ITGK_Code  . "' AND Intake_Batch = '" . $_LearnerBatch . "' ";
                //$_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        //print_r($_Response);
        return $_Response;
    }

    public function GetSurvey() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            mysqli_set_charset('utf8');

            $_SelectQuery = "Select Survey_id,Survey_Name from tbl_survey_master ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
