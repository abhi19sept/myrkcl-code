<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBankAccount
 *
 *  author Yogendra
 */
require 'DAL/classconnectionNEW.php';

require 'DAL/sendsms.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsmodifybankaccountdetailsforaccount {

    //put your code here


    public function SHOWDATA() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {

                    $_SelectQuery = "Select * from tbl_bank_account";
                } else {

                    $_SelectQuery = "Select * from tbl_bank_account where Bank_User_Code ='" . $_SESSION['User_LoginId'] . "'";
                }

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct bankname from tbl_rajbank_master";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetBranch() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct branchname from tbl_rajbank_master ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_AccountName, $_AccountNumber, $_AccountType, $_IfscCode, $_BankName, $_BranchName, $_MicrCode, $_PanNo, $_PanName, $_IdProof, $_centercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_centercode = mysqli_real_escape_string($_ObjConnection->Connect(),$_centercode);
				
            $_docname = $_centercode . '_' . $_AccountNumber . '_' . $_IdProof . '.jpg';
            $_pancard = $_centercode . '_' . $_AccountNumber . '_pancard' . '.jpg';
            $_UpdateQuery = "Update tbl_bank_account set Bank_Account_Name='" . $_AccountName . "',"
                    . "Bank_Account_Number='" . $_AccountNumber . "',"
                    . "Bank_Account_Type='" . $_AccountType . "',"
                    . "Bank_Ifsc_code='" . $_IfscCode . "',"
                    . "Bank_Micr_Code='" . $_MicrCode . "',"
                    . "Bank_Name='" . $_BankName . "',"
                    . "Bank_Branch_Name='" . $_BranchName . "',"
                    . "Pan_No='" . $_PanNo . "',"
                    . "Pan_Name='" . $_PanName . "',"
                    . "Bank_Document='" . $_docname . "',"
//                    . "Bank_User_Code='" . $_centercode . "',"
                    . "Pan_Document='" . $_pancard . "',"
                    //. "Bank_User_Code='" . $_SESSION['User_LoginId'] ."',"
                    . "Bank_Id_Proof='" . $_IdProof . "'"
                    . " Where  Bank_User_Code='" . $_centercode . "' ";
            //$_DuplicateQuery = "Select * From tbl_bank_account Where Bank_Account_Number='" . $_AccountNumber . "' ";
            //$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);





            $_InsertQuery1 = "Insert Into tbl_bank_account_log(Bank_Account_Code,Bank_Account_Name,Bank_Account_Number,Bank_Account_Type,"
                    . "Bank_Ifsc_code,Bank_Name,Bank_Branch_Name,Bank_Micr_Code,Pan_No,Pan_Name,Bank_Id_Proof,Bank_Document,Pan_Document,Bank_User_Code) "
                    . "Select Case When Max(Bank_Account_Code) Is Null Then 1 Else Max(Bank_Account_Code)+1 End as Bank_Account_Code,"
                    . "'" . $_AccountName . "' as Bank_Account_Name, "
                    . "'" . $_AccountNumber . "' as Bank_Account_Number,'" . $_AccountType . "' as Bank_Account_Type, "
                    . "'" . $_IfscCode . "' as Bank_Ifsc_code,'" . $_BankName . "' as Bank_Name, "
                    . "'" . $_BranchName . "' as Bank_Branch_Name,'" . $_MicrCode . "' as Bank_Micr_Code, "
                    . "'" . $_PanNo . "' as Pan_No, '" . $_PanName . "' as Pan_Name, '" . $_IdProof . "' as Bank_Id_Proof, "
                    . "'" . $_docname . "' as Bank_Document, '" . $_pancard . "' as Pan_Document, '" . $_centercode . "' as Bank_User_Code "
                    . " From tbl_bank_account_log";
            $_DuplicateQuery = "Select * From tbl_bank_account Where Bank_User_Code='" . $_SESSION['User_LoginId'] . "'";


            $bankdoc = '../upload/Bankdocs/' . $_docname;
            $pandoc = '../upload/pancard/' . $_pancard;

            if (file_exists($bankdoc) && file_exists($pandoc)) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                echo "Please Attach Necessary Documents on first page ";
                return;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRecord($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_DeleteQuery = "Delete From tbl_bank_account Where Bank_Account_Code='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select a.*, b.ITGK_Name from tbl_bank_account as a inner join vw_itgkname_distict_rsp as b on "
                    . "a.Bank_User_Code = b.ITGKCODE Where Bank_User_Code= '" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function checkifsccode($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select * from tbl_rajbank_master Where ifsccode= '" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
