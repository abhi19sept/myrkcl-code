<?php

/**
 * Description of clsAdmissionFeeWavier
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

class clsAdmissionFeeWavier {

    //put your code here

public function CheckExistence($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '4'){
				$update="update tbl_admission_fee_waiver_scheme as a, tbl_organization_detail as b 
						set a.ItgkDistrict=b.Organization_District,a.ItgkTehsil= b.Organization_Tehsil 
						where a.User_Code= b.Organization_User and a.ItgkDistrict is null"; 
				$_Responses = $_ObjConnection->ExecuteQuery($update, Message::UpdateStatement);
				
				 $_SelectQuery = "select * from tbl_admission_lottery_scheme where lottery_scheme_batch='".$_batch."'";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
								and Batch_Code>='270' order by Batch_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('1')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
public function GenerateDistrictWiseLottery($_levelid, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_levelid = mysqli_real_escape_string($_ObjConnection->Connect(),$_levelid);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '4'){			
				if($_levelid=='1' || $_levelid=='2' || $_levelid=='3'){
					$chckduplicate="select * from tbl_admission_lottery_scheme where lottery_scheme_master_id='".$_levelid."'
									and lottery_scheme_reservation_type='District' and lottery_scheme_batch='".$_batch."'";
					$_Response2 = $_ObjConnection->ExecuteQuery($chckduplicate, Message::SelectStatement);
						if ($_Response2[0] == Message::NoRecordFound){
							$selectdetails="select * from tbl_admission_lottery_master where level_id='".$_levelid."'
											and status='Active'";
							$_Response3 = $_ObjConnection->ExecuteQuery($selectdetails, Message::SelectStatement);
								if($_Response3[0]=='Success'){
									$details=mysqli_fetch_array($_Response3[2]);
										$loopcount=$details['District_wise_seat_count'];
										for($i=1;$i<=$loopcount;$i++){
											$_SelectQuery = "SELECT * FROM (
											SELECT Admission_Code,Admission_LearnerCode,Admission_ITGK_Code,Admission_Name,
											Admission_Fname,Admission_DOB,Admission_Course,ItgkDistrict
											from tbl_admission_fee_waiver_scheme where Admission_Batch='".$_batch."' and status='0' ORDER 
											BY RAND()) AS fd GROUP BY ItgkDistrict";
											$_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
											if($_Response1[0]=='Success'){
												$lcodes="";
												$admissioncodes="";							
												$counter=1;							
												while($row=mysqli_fetch_array($_Response1[2])){
													$lcodes .= "('".$row['Admission_LearnerCode']."','".$row['Admission_ITGK_Code']."',
																 '".$row['Admission_Name']."','".$row['Admission_Fname']."',
																 '".$row['Admission_DOB']."',$_levelid,'District',
																 '".$row['ItgkDistrict']."','".$row['Admission_Course']."',
																 '".$_batch."'),";
													$admissioncodes .=$row['Admission_Code'].",";
												}
											        $lcodes=rtrim($lcodes,",");
													$admissioncodes=rtrim($admissioncodes,",");
												$insert="insert into tbl_admission_lottery_scheme (lottery_scheme_lcode,
														lottery_scheme_itgk_code,lottery_scheme_lname,lottery_scheme_fname,
														lottery_scheme_dob,lottery_scheme_master_id,lottery_scheme_reservation_type,
														lottery_scheme_district,lottery_scheme_course,lottery_scheme_batch)
														values $lcodes";
												$_Responses = $_ObjConnection->ExecuteQuery($insert, Message::InsertStatement);
												if($_Responses[0]=="Successfully Inserted"){
													$update="update tbl_admission_fee_waiver_scheme set Lottery_master_id='".$_levelid."',
															 Lottery_reservation_type='District', status='1' where 
															 Admission_Code in ($admissioncodes)"; 
													$_Response = $_ObjConnection->ExecuteQuery($update, Message::UpdateStatement);
												}
											}
										}
								}					
						} else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
				 }
			
			}
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function GenerateRemainingSeatWiseLottery($_levelid, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_levelid = mysqli_real_escape_string($_ObjConnection->Connect(),$_levelid);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '4'){
				if($_levelid=='1' || $_levelid=='2' || $_levelid=='3'){
					$chckduplicate="select * from tbl_admission_lottery_scheme where lottery_scheme_master_id='".$_levelid."'
									and lottery_scheme_reservation_type='Remaining'	and lottery_scheme_batch='".$_batch."'";
					$_Response2 = $_ObjConnection->ExecuteQuery($chckduplicate, Message::SelectStatement);
						if ($_Response2[0] == Message::NoRecordFound){
							$selectdetails="select * from tbl_admission_lottery_master where level_id='".$_levelid."'
											and status='Active'";
							$_Response3 = $_ObjConnection->ExecuteQuery($selectdetails, Message::SelectStatement);
								if($_Response3[0]=='Success'){
									$details=mysqli_fetch_array($_Response3[2]);
										$rowlimit=$details['Remaining_seat'];										
										$_SelectQuery = "SELECT * FROM (
														SELECT Admission_Code,Admission_LearnerCode,Admission_ITGK_Code,
														Admission_Name,Admission_Fname,Admission_DOB,Admission_Course,ItgkDistrict from
														tbl_admission_fee_waiver_scheme where Admission_Batch='".$_batch."' 
														and status='0' ORDER BY RAND() limit $rowlimit) AS shuffled_items";
											$_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
											if($_Response1[0]=='Success'){
												$lcode="";
												$admissioncode="";							
												$counter=1;							
												while($row=mysqli_fetch_array($_Response1[2])){
													$lcode .= "('".$row['Admission_LearnerCode']."','".$row['Admission_ITGK_Code']."',
																 '".$row['Admission_Name']."','".$row['Admission_Fname']."',
																 '".$row['Admission_DOB']."',$_levelid,'Remaining',
																 '".$row['ItgkDistrict']."','".$row['Admission_Course']."',
																 '".$_batch."'),";
													$admissioncode .=$row['Admission_Code'].",";
												}
											        $lcode=rtrim($lcode,",");
													$admissioncode=rtrim($admissioncode,",");
												$insert="insert into tbl_admission_lottery_scheme (lottery_scheme_lcode,
														lottery_scheme_itgk_code,lottery_scheme_lname,lottery_scheme_fname,
														lottery_scheme_dob,lottery_scheme_master_id,lottery_scheme_reservation_type,
														lottery_scheme_district,lottery_scheme_course,lottery_scheme_batch)
														values $lcode";
												$_Responses = $_ObjConnection->ExecuteQuery($insert, Message::InsertStatement);
												if($_Responses[0]=="Successfully Inserted"){
													$update="update tbl_admission_fee_waiver_scheme set Lottery_master_id='".$_levelid."',
															 Lottery_reservation_type='Remaining', status='1' where 
															 Admission_Code in ($admissioncode)"; 
													$_Response = $_ObjConnection->ExecuteQuery($update, Message::UpdateStatement);
												}
											}										
								}					
						} else {
                            $_Response[0] = Message::DuplicateRecord;
                            $_Response[1] = Message::Error;
                        }
				 }
			}
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
	}
	
	public function GetLearnerWiseLotteryReport($_levelid, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_levelid = mysqli_real_escape_string($_ObjConnection->Connect(),$_levelid);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_SelectQuery = "select lottery_scheme_lcode,lottery_scheme_itgk_code,lottery_scheme_lname,lottery_scheme_fname,
							lottery_scheme_dob,Discount,lottery_scheme_reservation_type,c.District_Name,
							ITGK_Name,RSP_Code,RSP_Name from
							tbl_admission_lottery_scheme as a inner join tbl_admission_lottery_master as b 
							on a.lottery_scheme_master_id=b.level_id inner join tbl_district_master as c 
							on a.lottery_scheme_district=c.District_Code inner join vw_itgkname_distict_rsp as d
							on a.lottery_scheme_itgk_code=d.ITGKCODE where lottery_scheme_batch='".$_batch."'
							and lottery_scheme_master_id='".$_levelid."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function ResetGeneratedSeats($_levelid, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_levelid = mysqli_real_escape_string($_ObjConnection->Connect(),$_levelid);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_DeleteQuery = "delete from tbl_admission_lottery_scheme where lottery_scheme_batch='".$_batch."' and 
								lottery_scheme_master_id='".$_levelid."'";
            $_Responses = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
				if ($_Responses[0] == Message::SuccessfullyDelete){
					$_UpdateQuery = "update tbl_admission_fee_waiver_scheme set Lottery_master_id=null,Lottery_reservation_type='',
									status='0' where status ='1' and Admission_Batch='".$_batch."' and
									Lottery_master_id='".$_levelid."'";
					$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				}
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
