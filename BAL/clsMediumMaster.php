<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsMediumMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsMediumMaster {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Medium_Code,Medium_Name,"
                    . "Status_Name From tbl_medium_master as a inner join tbl_status_master as b "
                    . "on a.Medium_Status"
                    . "=b.Status_Code";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Medium_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Medium_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Medium_Code);
				
            $_SelectQuery = "Select Medium_Code,Medium_Name,"
                    . "Medium_Status From tbl_medium_master Where Medium_Code='" . $_Medium_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Medium_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Medium_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Medium_Code);
				
            $_DeleteQuery = "Delete From tbl_medium_master Where Medium_Code='" . $_Medium_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_MediumName,$_MediumStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_MediumName = mysqli_real_escape_string($_ObjConnection->Connect(),$_MediumName);
				$_MediumStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_MediumStatus);
				
            $_InsertQuery = "Insert Into tbl_medium_master(Medium_Code,Medium_Name,"
                    . "Medium_Status) "
                    . "Select Case When Max(Medium_Code) Is Null Then 1 Else Max(Medium_Code)+1 End as Medium_Code,"
                    . "'" . $_MediumName . "' as Medium_Name,'" . $_MediumStatus . "' as Medium_Status"
                    . " From tbl_medium_master";
            $_DuplicateQuery = "Select * From tbl_medium_master Where Medium_Name='" . $_MediumName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_MediumCode,$_MediumName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_MediumCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_MediumCode);
				$_MediumName = mysqli_real_escape_string($_ObjConnection->Connect(),$_MediumName);
				$_FunctionStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_FunctionStatus);
				
            $_UpdateQuery = "Update tbl_medium_master set Medium_Name='" . $_MediumName . "',"
                    . "Medium_Status='" . $_FunctionStatus . "' Where Medium_Code='" . $_MediumCode . "'";
            $_DuplicateQuery = "Select * From tbl_medium_master Where Medium_Name='" . $_MediumName . "' "
                    . "and Medium_Code <> '" . $_MediumCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
