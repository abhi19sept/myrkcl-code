<?php

/**
 * Description of wcd Report for learner feedback 
 *
 * @author 
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdFeedbackRpt {

//put your code here


    public function GetDatafirst($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11' || $_LoginUserRole == '28') {
                $_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
                                                sum(case when Wcd_Feedback_Ques_13 = 'अधिक' then 1 else 0 end) adhik,
                                                sum(case when Wcd_Feedback_Ques_13 = 'अत्यधिक' then 1 else 0 end) atyadhik,
                                                sum(case when Wcd_Feedback_Ques_13 = 'सामान्य' then 1 else 0 end) samanya,
                                                sum(case when Wcd_Feedback_Ques_13 = 'कम' then 1 else 0 end) kam 
                                                from wcd_feedback_form where Wcd_Feedback_Itgk_Code !='' and 
                                                Wcd_Feedback_Batch='" . $batch . "' group by Wcd_Feedback_Itgk_Code";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       // return $_Response;
    }
	
	
	
	public function GetCustomDatafirst($course, $batch) 
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11' || $_LoginUserRole == '28') 
			{
                
$_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
sum(case when Wcd_Feedback1_Ques_1 = 'Desktop' then 1 else 0 end) Desktop1, 
sum(case when Wcd_Feedback1_Ques_1 = 'Laptop' then 1 else 0 end) Laptop1, 
sum(case when Wcd_Feedback1_Ques_1 = 'Smartphone'  then 1 else 0 end) Smartphone1, 
sum(case when Wcd_Feedback1_Ques_1 = 'संशाधन उपलब्ध नहीं' then 1 else 0 end) notavilable1,
 
sum(case when Wcd_Feedback1_Ques_2 = 'Microsoft Team' then 1 else 0 end) Microsoft2, 
sum(case when Wcd_Feedback1_Ques_2 = 'Zoom App' then 1 else 0 end) Zoom2,
sum(case when Wcd_Feedback1_Ques_2 = 'Google' then 1 else 0 end) Google2, 
sum(case when Wcd_Feedback1_Ques_2 = 'Webex' then 1 else 0 end) Webex2, 
sum(case when Wcd_Feedback1_Ques_2 = 'WhatsApp'  then 1 else 0 end) WhatsApp2, 
sum(case when Wcd_Feedback1_Ques_2 = 'Telegram' then 1 else 0 end) Telegram2, 
sum(case when Wcd_Feedback1_Ques_2 = 'कोई अन्य टूल ' then 1 else 0 end) othertool2, 

sum(case when Wcd_Feedback1_Ques_3 = 'अत्यधिक' then 1 else 0 end) atyadhik3 ,
sum(case when Wcd_Feedback1_Ques_3 = 'उचित' then 1 else 0 end) uchit3, 
sum(case when Wcd_Feedback1_Ques_3 = 'कम' then 1 else 0 end) kam3, 
sum(case when Wcd_Feedback1_Ques_3 = 'बहुत कम'  then 1 else 0 end) bahutkam3,

sum(case when Wcd_Feedback1_Ques_4 = 'बहुत अच्छा' then 1 else 0 end) bahutaccha4, 
sum(case when Wcd_Feedback1_Ques_4 = 'सामान्य' then 1 else 0 end) samanya4, 
sum(case when Wcd_Feedback1_Ques_4 = 'संतोषप्रद' then 1 else 0 end) santoshpad4,
sum(case when Wcd_Feedback1_Ques_4 = 'असंतुष्ट ' then 1 else 0 end) asantusth4, 

sum(case when Wcd_Feedback1_Ques_5 = 'हाँ'  then 1 else 0 end) ha5, 
sum(case when Wcd_Feedback1_Ques_5 = 'नहीं'  then 1 else 0 end) nahi5, 


sum(case when Wcd_Feedback1_Ques_6 = 'अत्यधिक' then 1 else 0 end) ataydhik6, 
sum(case when Wcd_Feedback1_Ques_6 = 'सामान्य ' then 1 else 0 end) samanya6, 
sum(case when Wcd_Feedback1_Ques_6 = 'कम' then 1 else 0 end) kam6,
sum(case when Wcd_Feedback1_Ques_6 = 'बिल्कुल नहीं' then 1 else 0 end) BilkulNahi6 
from wcd_feedback_form WHERE Wcd_Feedback_Itgk_Code !='' and Wcd_Feedback_Batch='".$batch."' and  Wcd_Feedback1_Ques_1 !='' and Wcd_Feedback1_Ques_2 !='' and Wcd_Feedback1_Ques_3 !='' and Wcd_Feedback1_Ques_4 !=''
and Wcd_Feedback1_Ques_5 !='' and Wcd_Feedback1_Ques_6 !='' group by Wcd_Feedback_Itgk_Code";


$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } 
		catch (Exception $_ex) 
		{

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       // return $_Response;
    }
	
		

    public function GetDataSP($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '14'  || $_LoginUserRole == '13') {
                $_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
                                            sum(case when Wcd_Feedback1_Ques_5 = 'हाँ'  then 1 else 0 end) Ha,
                                                sum(case when Wcd_Feedback1_Ques_5 = 'नहीं'  then 1 else 0 end) Nahi,
                                                sum(case when Wcd_Feedback1_Ques_6 = 'अत्यधिक'  then 1 else 0 end) Ataydhik,
                                                sum(case when Wcd_Feedback1_Ques_6 = 'सामान्य'  then 1 else 0 end) Samanya,
												sum(case when Wcd_Feedback1_Ques_6 = 'कम'  then 1 else 0 end) Kam,
                                                sum(case when Wcd_Feedback1_Ques_6 = 'बिल्कुल नहीं' then 1 else 0 end) BilkulNahi		
                                            from wcd_feedback_form as a  inner join vw_itgkname_distict_rsp as b on 
                                            a.Wcd_Feedback_Itgk_Code = b.ITGKCODE where Wcd_Feedback_Itgk_Code !='' and
                                            RSP_Code='" . $_SESSION['User_LoginId'] . "' and Wcd_Feedback_Batch='" . $batch . "' 
                                            group by Wcd_Feedback_Itgk_Code";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       // return $_Response3;
    }
	
	
	
	public function GetCustomDataSP($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '14'  || $_LoginUserRole == '13') {
                $_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अधिक' then 1 else 0 end) adhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अत्यधिक' then 1 else 0 end) atyadhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'सामान्य' then 1 else 0 end) samanya,
                                            sum(case when Wcd_Feedback_Ques_13 = 'कम' then 1 else 0 end) kam 
                                            from wcd_feedback_form as a  inner join vw_itgkname_distict_rsp as b on 
                                            a.Wcd_Feedback_Itgk_Code = b.ITGKCODE where Wcd_Feedback_Itgk_Code !='' and
                                            RSP_Code='" . $_SESSION['User_LoginId'] . "' and Wcd_Feedback_Batch='" . $batch . "' 
                                            group by Wcd_Feedback_Itgk_Code";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       // return $_Response3;
    }


    public function GetDataPO($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '17') {
                $_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अधिक' then 1 else 0 end) adhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अत्यधिक' then 1 else 0 end) atyadhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'सामान्य' then 1 else 0 end) samanya,
                                            sum(case when Wcd_Feedback_Ques_13 = 'कम' then 1 else 0 end) kam 
                                           from wcd_feedback_form as a  inner join vw_itgkname_distict_rsp as b on 
                                            a.Wcd_Feedback_Itgk_Code = b.ITGKCODE where Wcd_Feedback_Itgk_Code !='' and 
                                            District_Code='" . $_SESSION['Organization_District'] . "'  and Wcd_Feedback_Batch='" . $batch . "' 
                                            group by Wcd_Feedback_Itgk_Code";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        //return $_Response;
    }
	
	
	public function GetCustomDataPO($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '17') {
                $_SelectQuery3 = "select a.ITGK, sum(lcount) as lcount,
			sum(total) as tcount, sum(Ha) as Ha, sum(Nahi) as Nahi, sum(Ataydhik) as Ataydhik,
			sum(Samanya) as Samanya, sum(Kam) as Kam, sum(BilkulNahi) as BilkulNahi
from
(
    select Wcd_Feedback_Itgk_Code as ITGK, count(Wcd_Feedback_lcode) as lcount,0 as total,
	sum(case when Wcd_Feedback1_Ques_5 = 'हाँ' then 1 else 0 end) Ha, 
sum(case when Wcd_Feedback1_Ques_5 = 'नहीं' then 1 else 0 end) Nahi, 
sum(case when Wcd_Feedback1_Ques_6 = 'अत्यधिक' OR 'b' then 1 else 0 end) Ataydhik,
 sum(case when Wcd_Feedback1_Ques_6 = 'सामान्य' then 1 else 0 end) Samanya,
 sum(case when Wcd_Feedback1_Ques_6 = 'कम' then 1 else 0 end) Kam, 
sum(case when Wcd_Feedback1_Ques_6 = 'बिल्कुल नहीं' then 1 else 0 end) BilkulNahi
 from wcd_feedback_form as a
inner join vw_itgkname_distict_rsp as b on 
 a.Wcd_Feedback_Itgk_Code = b.ITGKCODE where a.Wcd_Feedback_Itgk_Code !='' and 
 b.District_Code='" . $_SESSION['Organization_District'] . "' and Wcd_Feedback_Batch='".$batch."' group by Wcd_Feedback_Itgk_Code
    union all
    select Admission_ITGK_Code as ITGK, 0 as lcount, Count(Admission_LearnerCode) as total,
	0 as Ha, 0 as Nahi, 0 as Ataydhik, 0 as Samanya, 0 as Kam, 0 as BilkulNahi from tbl_admission WHERE
	Admission_Course='".$course."' and Admission_Batch='".$batch."' and Admission_Payment_Status='1' group by Admission_ITGK_Code
   
) as a
group by a.ITGK";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        //return $_Response;
    }


    
    public function GetDatafirstLearner($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11' || $_LoginUserRole == '28') {
                $_SelectQuery3 = "select a.*, b.District_Name from wcd_feedback_form as a inner join  vw_itgkname_distict_rsp as b
								  on a.Wcd_Feedback_Itgk_Code=b.ITGKCODE where Wcd_Feedback_Itgk_Code  !='' and
								  Wcd_Feedback_Batch='" . $batch . "' and Wcd_Feedback1_Ques_1 !=''";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
			elseif ($_LoginUserRole == '7') {
                $_SelectQuery3 = "select a.*, b.District_Name from wcd_feedback_form as a inner join vw_itgkname_distict_rsp as b
								on a.Wcd_Feedback_Itgk_Code=b.ITGKCODE where Wcd_Feedback_Itgk_Code !='' and 
								Wcd_Feedback_Batch='" . $batch . "' and a.Wcd_Feedback_Itgk_Code='" . $_SESSION['User_LoginId'] . "'
								and Wcd_Feedback1_Ques_1 !=''";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetCustomDatafirstforSP($course, $batch) 
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '14') 
			{
                
			$_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
			sum(case when Wcd_Feedback1_Ques_1 = 'Desktop' then 1 else 0 end) Desktop1, 
			sum(case when Wcd_Feedback1_Ques_1 = 'Laptop' then 1 else 0 end) Laptop1, 
			sum(case when Wcd_Feedback1_Ques_1 = 'Smartphone'  then 1 else 0 end) Smartphone1, 
			sum(case when Wcd_Feedback1_Ques_1 = 'संशाधन उपलब्ध नहीं' then 1 else 0 end) notavilable1,
			 
			sum(case when Wcd_Feedback1_Ques_2 = 'Microsoft Team' then 1 else 0 end) Microsoft2, 
			sum(case when Wcd_Feedback1_Ques_2 = 'Zoom App' then 1 else 0 end) Zoom2,
			sum(case when Wcd_Feedback1_Ques_2 = 'Google' then 1 else 0 end) Google2, 
			sum(case when Wcd_Feedback1_Ques_2 = 'Webex' then 1 else 0 end) Webex2, 
			sum(case when Wcd_Feedback1_Ques_2 = 'WhatsApp'  then 1 else 0 end) WhatsApp2, 
			sum(case when Wcd_Feedback1_Ques_2 = 'Telegram' then 1 else 0 end) Telegram2, 
			sum(case when Wcd_Feedback1_Ques_2 = 'कोई अन्य टूल ' then 1 else 0 end) othertool2, 

			sum(case when Wcd_Feedback1_Ques_3 = 'अत्यधिक' then 1 else 0 end) atyadhik3 ,
			sum(case when Wcd_Feedback1_Ques_3 = 'उचित' then 1 else 0 end) uchit3, 
			sum(case when Wcd_Feedback1_Ques_3 = 'कम' then 1 else 0 end) kam3, 
			sum(case when Wcd_Feedback1_Ques_3 = 'बहुत कम'  then 1 else 0 end) bahutkam3,

			sum(case when Wcd_Feedback1_Ques_4 = 'बहुत अच्छा' then 1 else 0 end) bahutaccha4, 
			sum(case when Wcd_Feedback1_Ques_4 = 'सामान्य' then 1 else 0 end) samanya4, 
			sum(case when Wcd_Feedback1_Ques_4 = 'संतोषप्रद' then 1 else 0 end) santoshpad4,
			sum(case when Wcd_Feedback1_Ques_4 = 'असंतुष्ट ' then 1 else 0 end) asantusth4, 

			sum(case when Wcd_Feedback1_Ques_5 = 'हाँ'  then 1 else 0 end) ha5, 
			sum(case when Wcd_Feedback1_Ques_5 = 'नहीं'  then 1 else 0 end) nahi5, 


			sum(case when Wcd_Feedback1_Ques_6 = 'अत्यधिक' then 1 else 0 end) ataydhik6, 
			sum(case when Wcd_Feedback1_Ques_6 = 'सामान्य ' then 1 else 0 end) samanya6, 
			sum(case when Wcd_Feedback1_Ques_6 = 'कम' then 1 else 0 end) kam6,
			sum(case when Wcd_Feedback1_Ques_6 = 'बिल्कुल नहीं' then 1 else 0 end) BilkulNahi6 
			from wcd_feedback_form as a inner join tbl_user_master as b on a.Wcd_Feedback_Itgk_Code=b.User_LoginId WHERE
			Wcd_Feedback_Itgk_Code !='' and Wcd_Feedback_Batch='".$batch."' and  Wcd_Feedback1_Ques_1 !=''
			and Wcd_Feedback1_Ques_2 !='' and Wcd_Feedback1_Ques_3 !='' and Wcd_Feedback1_Ques_4 !=''
			and Wcd_Feedback1_Ques_5 !='' and Wcd_Feedback1_Ques_6 !='' and User_Rsp='".$_SESSION['User_Code']."'
			group by Wcd_Feedback_Itgk_Code";


$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } 
		catch (Exception $_ex) 
		{

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       // return $_Response;
    }
	
	public function GetCustomDatafirstforITGK($course, $batch) 
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '7') 
			{
                
				$_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
				sum(case when Wcd_Feedback1_Ques_1 = 'Desktop' then 1 else 0 end) Desktop1, 
				sum(case when Wcd_Feedback1_Ques_1 = 'Laptop' then 1 else 0 end) Laptop1, 
				sum(case when Wcd_Feedback1_Ques_1 = 'Smartphone'  then 1 else 0 end) Smartphone1, 
				sum(case when Wcd_Feedback1_Ques_1 = 'संशाधन उपलब्ध नहीं' then 1 else 0 end) notavilable1,
				 
				sum(case when Wcd_Feedback1_Ques_2 = 'Microsoft Team' then 1 else 0 end) Microsoft2, 
				sum(case when Wcd_Feedback1_Ques_2 = 'Zoom App' then 1 else 0 end) Zoom2,
				sum(case when Wcd_Feedback1_Ques_2 = 'Google' then 1 else 0 end) Google2, 
				sum(case when Wcd_Feedback1_Ques_2 = 'Webex' then 1 else 0 end) Webex2, 
				sum(case when Wcd_Feedback1_Ques_2 = 'WhatsApp'  then 1 else 0 end) WhatsApp2, 
				sum(case when Wcd_Feedback1_Ques_2 = 'Telegram' then 1 else 0 end) Telegram2, 
				sum(case when Wcd_Feedback1_Ques_2 = 'कोई अन्य टूल ' then 1 else 0 end) othertool2, 

				sum(case when Wcd_Feedback1_Ques_3 = 'अत्यधिक' then 1 else 0 end) atyadhik3 ,
				sum(case when Wcd_Feedback1_Ques_3 = 'उचित' then 1 else 0 end) uchit3, 
				sum(case when Wcd_Feedback1_Ques_3 = 'कम' then 1 else 0 end) kam3, 
				sum(case when Wcd_Feedback1_Ques_3 = 'बहुत कम'  then 1 else 0 end) bahutkam3,

				sum(case when Wcd_Feedback1_Ques_4 = 'बहुत अच्छा' then 1 else 0 end) bahutaccha4, 
				sum(case when Wcd_Feedback1_Ques_4 = 'सामान्य' then 1 else 0 end) samanya4, 
				sum(case when Wcd_Feedback1_Ques_4 = 'संतोषप्रद' then 1 else 0 end) santoshpad4,
				sum(case when Wcd_Feedback1_Ques_4 = 'असंतुष्ट ' then 1 else 0 end) asantusth4, 

				sum(case when Wcd_Feedback1_Ques_5 = 'हाँ'  then 1 else 0 end) ha5, 
				sum(case when Wcd_Feedback1_Ques_5 = 'नहीं'  then 1 else 0 end) nahi5, 


				sum(case when Wcd_Feedback1_Ques_6 = 'अत्यधिक' then 1 else 0 end) ataydhik6, 
				sum(case when Wcd_Feedback1_Ques_6 = 'सामान्य ' then 1 else 0 end) samanya6, 
				sum(case when Wcd_Feedback1_Ques_6 = 'कम' then 1 else 0 end) kam6,
				sum(case when Wcd_Feedback1_Ques_6 = 'बिल्कुल नहीं' then 1 else 0 end) BilkulNahi6 
				from wcd_feedback_form WHERE Wcd_Feedback_Itgk_Code !='' and Wcd_Feedback_Batch='".$batch."' and  
				Wcd_Feedback1_Ques_1 !='' and Wcd_Feedback1_Ques_2 !='' and Wcd_Feedback1_Ques_3 !='' and Wcd_Feedback1_Ques_4 !=''
				and Wcd_Feedback1_Ques_5 !='' and Wcd_Feedback1_Ques_6 !='' and Wcd_Feedback_Itgk_Code='".$_SESSION['User_LoginId']."'";


				$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } 
		catch (Exception $_ex) 
		{

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       // return $_Response;
    }
}
