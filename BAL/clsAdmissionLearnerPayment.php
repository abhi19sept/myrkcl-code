<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionLearnerPayment extends paymentFunctions {

    //put your code here
	
	public function LearnerVerify($_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
            $_SelectQuery = "Select * FROM tbl_admission_enquiry WHERE AdmissionEnquiry_Lcode = '" . $_Lcode . "'";
            $_Responses = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				if ($_Responses[0] == Message::NoRecordFound){
						$_Response[0] = "1";
                        $_Response[1] = Message::Error;
					
				}else{
					$_SelectQuery = "select Admission_Code,Admission_ITGK_Code,Admission_Course,Admission_Batch,Admission_Name,
									Admission_Fname,Admission_DOB,Admission_Photo,Admission_Sign,Course_Name,Batch_Name,RKCL_Share 
									from tbl_admission as a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code
									inner join tbl_batch_master as c on a.Admission_Batch=c.Batch_Code 
									where Admission_LearnerCode='".$_Lcode."' and Admission_Payment_Status='0' and
									Admission_Batch>'260'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }  
	
	public function GetAll($batch, $course, $paymode, $lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {    
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$paymode);
            $lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
            
            $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_Course = '" . $course . "' AND
			Event_Batch = '" . $batch . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '3' AND NOW() >= Event_Startdate
			AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
                if ($_getEvent['Event_Payment'] == '1') {
                    $_SelectQuery = "select Admission_Code,Admission_ITGK_Code,Admission_Course,Admission_Batch,Admission_Name,
									Admission_Fname,Admission_DOB,Admission_Photo,Admission_Sign,Course_Name,Batch_Name,RKCL_Share,
									Admission_Payment_Status 
									from tbl_admission as a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code
									inner join tbl_batch_master as c on a.Admission_Batch=c.Batch_Code 
									where Admission_LearnerCode='".$lcode."' and Admission_Payment_Status='0' and
									Admission_Batch>'260'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_getEvent['Event_Payment'] == '2') {
                    $_SelectQuery = "select Admission_Code,Admission_ITGK_Code,Admission_Course,Admission_Batch,Admission_Name,
									Admission_Fname,Admission_DOB,Admission_Photo,Admission_Sign,Course_Name,Batch_Name,RKCL_Share,
									Admission_Payment_Status 
									from tbl_admission as a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code
									inner join tbl_batch_master as c on a.Admission_Batch=c.Batch_Code 
									where Admission_LearnerCode='".$lcode."' and Admission_Payment_Status='0' and
									Admission_Batch>'260'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    //echo "Invalid User Input";
                    return;
                }
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddPayTran($postValues, $productinfo) {
        $return = 0;
        try {
			if (isset($_SESSION['User_LearnerCode']) && !empty($_SESSION['User_LearnerCode'])) {
            $return = parent::insertPaymentTransaction($productinfo, $postValues['txtitgk'], $postValues['amounts'],
				$postValues['AdmissionCodes'], $postValues['ddlcourse'], 0, $postValues['ddlbatch']);
             } else {
                session_destroy();
            ?>
                <script> window.location.href = "logout.php";</script> 
            <?php
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }
	
	public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}