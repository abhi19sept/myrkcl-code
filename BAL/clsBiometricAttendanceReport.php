<?php

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
/**
 * Description of clsBiometricAttendanceReport
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';


$_ObjConnection = new _Connection();
$_Response = array();

class clsBiometricAttendanceReport {

    public function SHOWData($_BatchCode, $_CourseCode,$sdate,$edate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
$sdate = $sdate.' 00:00:00';
$edate = $edate.' 23:59:59';
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

                if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8') {
                     // $_SelectQuery = "SELECT b.Admission_ITGK_Code as Admission_ITGK_Code,CONCAT('_',Admission_LearnerCode) as Admission_LearnerCode, UPPER(b.Admission_Name),UPPER(b.Admission_Fname), SUM(CASE WHEN a.Attendance_Type='P' and Attendance_flag='1' THEN 1 ELSE 0 END) AS Attendance_Count from tbl_learner_attendance_wcd a right join tbl_admission AS b on a.Attendance_Admission_Code = b.Admission_Code   where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' group by b.Admission_Code ";
                   
                     $_SelectQuery = "select Attendance_Admission_Code, Admission_ITGK_Code, CONCAT('_',Admission_LearnerCode) as Admission_LearnerCode,UPPER(Admission_Name),UPPER(Admission_Fname),count(distinct Attendance_Admission_Code,Attendance_In_Time) as cnt,
                     count(distinct Attendance_Admission_Code,Attendance_In_Time ,CASE WHEN Attendance_Mode='1' THEN 1 END) as onlinecnt,
count(distinct Attendance_Admission_Code,Attendance_In_Time,CASE WHEN Attendance_Mode='2' THEN 1 END) as offlinecnt
 from tbl_learner_attendance_wcd as a inner join tbl_admission as b on a.Attendance_Admission_Code = b.Admission_Code
 where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and Attendance_Type='P' and Attendance_flag='1' and Attendance_In_Time between '".$sdate."' and '".$edate."' group by Attendance_Admission_Code
union 
select distinct Admission_Code,Admission_ITGK_Code,CONCAT('_',Admission_LearnerCode) as Admission_LearnerCode,UPPER(Admission_Name),UPPER(Admission_Fname), '0' as cnt,'0' as onlinecnt,'0' as offlinecnt from tbl_admission as a where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and a.Admission_Code not in(select b.Attendance_Admission_Code from tbl_learner_attendance_wcd as b)";
                 
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_SESSION['User_UserRoll'] == '17') {

                     $_SelectQuery = "SELECT b.Admission_ITGK_Code as Admission_ITGK_Code,CONCAT('_',Admission_LearnerCode) as Admission_LearnerCode,  UPPER(b.Admission_Name),UPPER(b.Admission_Fname), SUM(CASE WHEN a.Attendance_Type='P' and Attendance_flag='1' THEN 1 ELSE 0 END) AS Attendance_Count from tbl_learner_attendance_wcd a right join tbl_admission AS b on a.Attendance_Admission_Code = b.Admission_Code inner join tbl_user_master as um on b.Admission_ITGK_Code=um.User_LoginId inner join tbl_organization_detail as od on um.User_Code=od.Organization_User where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and od.Organization_District='" . $_SESSION['Organization_District'] . "'  and Attendance_In_Time between '".$sdate."' and '".$edate."' group by b.Admission_Code ";

                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_SESSION['User_UserRoll'] == '14') {
                    $_SelectQuery = "select Attendance_Admission_Code, Admission_ITGK_Code, CONCAT('_',Admission_LearnerCode) as Admission_LearnerCode,UPPER(Admission_Name),UPPER(Admission_Fname),count(distinct Attendance_Admission_Code,Attendance_In_Time) as cnt from tbl_learner_attendance_wcd as a inner join tbl_admission as b on a.Attendance_Admission_Code = b.Admission_Code
 where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and Attendance_Type='P' and Attendance_flag='1' and Admission_RspName='" . $_SESSION['User_Code'] . "' and Attendance_In_Time between '".$sdate."' and '".$edate."' group by Attendance_Admission_Code
union 
select distinct Admission_Code,Admission_ITGK_Code,CONCAT('_',Admission_LearnerCode) as Admission_LearnerCode,UPPER(Admission_Name),UPPER(Admission_Fname), '0' as cnt from tbl_admission as a where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and Admission_RspName='" . $_SESSION['User_Code'] . "' and a.Admission_Code not in(select b.Attendance_Admission_Code  from tbl_learner_attendance_wcd as b)"; 
                            // $_SelectQuery = "SELECT b.Admission_ITGK_Code as Admission_ITGK_Code,Admission_LearnerCode, b.Admission_Name,b.Admission_Fname, SUM(CASE WHEN a.Attendance_Type='P' and Attendance_flag='1' THEN 1 ELSE 0 END) AS Attendance_Count from tbl_learner_attendance_wcd a right join tbl_admission AS b on a.Attendance_Admission_Code = b.Admission_Code  where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and 
                            // Admission_RspName='" . $_SESSION['User_Code'] . "' group by b.Admission_Code";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else if ($_SESSION['User_UserRoll'] == '7') {
                      $_SelectQuery = "select Attendance_Admission_Code, Admission_ITGK_Code,  Admission_LearnerCode,UPPER(Admission_Name) as Admission_Name ,UPPER(Admission_Fname) as Admission_Fname,count(distinct Attendance_Admission_Code,Attendance_In_Time) as Attendance_Count  from tbl_learner_attendance_wcd as a inner join tbl_admission as b on a.Attendance_Admission_Code = b.Admission_Code
 where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and Attendance_Type='P' and Attendance_flag='1' and Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "' group by Attendance_Admission_Code";
                    // $_SelectQuery = "SELECT b.Admission_ITGK_Code as Admission_ITGK_Code,CONCAT('_',Admission_LearnerCode) as Admission_LearnerCode,  UPPER(b.Admission_Name) as Admission_Name,UPPER(b.Admission_Fname) as Admission_Fname, SUM(CASE WHEN a.Attendance_Type='P' and Attendance_flag='1' THEN 1 ELSE 0 END) AS Attendance_Count from tbl_learner_attendance_wcd a right join tbl_admission AS b on a.Attendance_Admission_Code = b.Admission_Code inner join tbl_user_master as um on b.Admission_ITGK_Code=um.User_LoginId inner join tbl_organization_detail as od on um.User_Code=od.Organization_User where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and 
                    //         Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "' group by a.Attendance_Admission_Code ";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function SHOWDataITGK($_BatchCode, $_CourseCode,$sdate,$edate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
    try {
        if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {

            if ($_SESSION['User_UserRoll'] == '7') {
        $_SelectQuery = "select Attendance_Admission_Code, Admission_ITGK_Code,  Admission_LearnerCode,UPPER(Admission_Name) as Admission_Name ,UPPER(Admission_Fname) as Admission_Fname,count(distinct Attendance_Admission_Code,Attendance_In_Time) as Attendance_Count  from tbl_learner_attendance_wcd as a inner join tbl_admission as b on a.Attendance_Admission_Code = b.Admission_Code
 where Admission_Course='" . $_CourseCode . "' AND Admission_Batch='" . $_BatchCode . "' and Admission_Payment_Status='1' and Attendance_Type='P' and Attendance_flag='1' and Admission_ITGK_Code='" . $_SESSION['User_LoginId'] . "'  and Attendance_In_Time between '".$sdate."' and '".$edate."' group by Attendance_Admission_Code";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAttendance($_BatchCode, $_AdmissionCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
           $_SelectQuery = "select Attendance_Admission_Code,count(Attendance_Code) as cnt from tbl_learner_attendance_wcd where Attendance_Type='P' and Attendance_Batch ='".$_BatchCode."' and Attendance_flag='1' and Attendance_Admission_Code='".$_AdmissionCode."' group by Attendance_Admission_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetBatch($Course_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '".$Course_Code."' order by Batch_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Getresult($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT  d.result as result, d.tot_marks as tmarks, e.Event_Name  
                                         from tbl_result as d inner join tbl_events as e on d.exameventnameID=e.Event_Id
                                         where d.scol_no ='" . $lcode . "' order by exam_held_date DESC limit 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
