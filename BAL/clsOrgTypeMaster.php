<?php

/**
 * Description of clsOrgTypeMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOrgTypeMaster {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Org_Type_Code,Org_Type_Name,"
                    . "Status_Name From tbl_org_type_master as a inner join tbl_status_master as b "
                    . "on a.Org_Type_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Org_Type_Code)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Org_Type_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Org_Type_Code);
				
            $_SelectQuery = "Select Org_Type_Code,Org_Type_Name,Org_Type_Status"
                    . " From tbl_org_type_master Where Org_Type_Code='" . $_Org_Type_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Org_Type_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Org_Type_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Org_Type_Code);
				
            $_DeleteQuery = "Delete From tbl_org_type_master Where Org_Type_Code='" . $_Org_Type_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_OrgTypeName,$_OrgTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OrgTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgTypeName);
				$_OrgTypeStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgTypeStatus);
				
            $_InsertQuery = "Insert Into tbl_org_type_master(Org_Type_Code,Org_Type_Name,"
                    . "Org_Type_Status) "
                    . "Select Case When Max(Org_Type_Code) Is Null Then 1 Else Max(Org_Type_Code)+1 End as Org_Type_Code,"
                    . "'" . $_OrgTypeName . "' as Org_Type_Name,"
                    . "'" . $_OrgTypeStatus . "' as Org_Type_Status"
                    . " From tbl_org_type_master";
            $_DuplicateQuery = "Select * From tbl_org_type_master Where Org_Type_Name='" . $_OrgTypeName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_OrgTypeCode,$_OrgTypeName,$_OrgTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OrgTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgTypeName);
				$_OrgTypeStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgTypeStatus);
				$_OrgTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgTypeCode);
				
            $_UpdateQuery = "Update tbl_org_type_master set Org_Type_Name='" . $_OrgTypeName . "',"
                    . "Org_Type_Status='" . $_OrgTypeStatus . "' Where Org_Type_Code='" . $_OrgTypeCode . "'";
            $_DuplicateQuery = "Select * From tbl_org_type_master Where Org_Type_Name='" . $_OrgTypeName . "' "
                    . "and Org_Type_Code <> '" . $_OrgTypeCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
