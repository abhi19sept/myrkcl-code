<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCourseTypeMaster {

    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_coursetype_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_CourseType_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseType_Code);
				
            $_SelectQuery = "Select CourseType_Status,CourseType_Name From tbl_coursetype_master Where
								CourseType_Code='" . $_CourseType_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRecord($_CourseType_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseType_Code);
				
            $_DeleteQuery = "Delete From tbl_coursetype_master Where CourseType_Code='" . $_CourseType_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add($_CourseTypeName, $_CourseTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseTypeName);
				$_CourseTypeStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseTypeStatus);
				
            $_InsertQuery = "Insert Into tbl_coursetype_master(CourseType_Code,CourseType_Name,CourseType_Status) "
                    . "Select Case When Max(CourseType_Code) Is Null Then 1 Else Max(CourseType_Code)+1 End as CourseType_Code,"
                    . "'" . $_CourseTypeName . "' as CourseType_Name,'" . $_CourseTypeStatus . "' as CourseType_Status From tbl_coursetype_master";
            $_DuplicateQuery = "Select * From tbl_coursetype_master Where CourseType_Name='" . $_CourseTypeName . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_CourseTypeCode, $_CourseTypeName, $_CourseTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseTypeCode);
				$_CourseTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseTypeName);
				
            $_UpdateQuery = "Update tbl_coursetype_master set CourseType_Name='" . $_CourseTypeName . "',CourseType_Status='" . $_CourseTypeStatus . "' Where CourseType_Code='" . $_CourseTypeCode . "'";
            $_DuplicateQuery = "Select * From tbl_userroll_master Where CourseType_Name='" . $_CourseTypeName . "' and CourseType_Code <> '" . $_CourseTypeCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
?>
