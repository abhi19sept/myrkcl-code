<?php

/**
 * Description of clsPrintRecpRscfaExam
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsPrintRecpRscfaExam {

    //put your code here

    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				
            if($_SESSION['User_UserRoll'] == '7'){
                  $_SelectQuery2 = "Select * From tbl_apply_rscfa_certificate WHERE "
                                . "apply_rscfa_cert_payment_date >= '" . $_StartDate . "' AND "
                                . "apply_rscfa_cert_payment_date <= '" . $_EndDate . "' AND "
                                . "apply_rscfa_cert_itgk ='" . $_SESSION['User_LoginId'] . "'";
            }
            elseif($_SESSION['User_UserRoll'] == '19'){

            $_SelectQuery2 = "Select * From tbl_apply_rscfa_certificate WHERE "
                                . "apply_rscfa_cert_payment_date >= '" . $_StartDate . "' AND "
                                . "apply_rscfa_cert_payment_date <= '" . $_EndDate . "' AND "
                                . "apply_rscfa_cert_itgk ='" . $_SESSION['User_LoginId'] . "' AND apply_rscfa_cert_lcode='".$_SESSION['User_LearnerCode']."'";
                    }          
		  $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }
    public function Showreexam($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				
            if($_SESSION['User_UserRoll'] == '7'){
                  $_SelectQuery2 = "Select * From tbl_rscfa_reexam_application WHERE "
                                . "rscfa_reexam_paydate >= '" . $_StartDate . "' AND "
                                . "rscfa_reexam_paydate <= '" . $_EndDate . "' AND "
                                . "itgkcode ='" . $_SESSION['User_LoginId'] . "'";
            }
            elseif($_SESSION['User_UserRoll'] == '19'){

            $_SelectQuery2 = "Select * From tbl_rscfa_reexam_application WHERE "
                                . "rscfa_reexam_paydate >= '" . $_StartDate . "' AND "
                                . "rscfa_reexam_paydate <= '" . $_EndDate . "' AND "
                                . "itgkcode ='" . $_SESSION['User_LoginId'] . "' AND rscfa_reexam_lcode='".$_SESSION['User_LearnerCode']."'";
                    }          
		  $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

    public function GetAllPrintRecpDwnld($cid) { /// for new invoice system GST
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$cid = mysqli_real_escape_string($_ObjConnection->Connect(),$cid);
				
			   $_SelectQuery = "Select a.*,b.*,c.*,d.*,e.Organization_Name ,Course_Name,District_Name from tbl_apply_rscfa_certificate as a inner join tbl_user_master as b on a.apply_rscfa_cert_itgk=b.User_LoginId inner join tbl_apply_rscfa_exam_invoice as c on a.apply_rscfa_cert_id= c.transaction_ref_id inner join tbl_gst_invoice_master as d on d.Gst_Invoce_Category_Type='RscfaExam' inner join tbl_organization_detail as e on b.User_Code=e.Organization_User inner join tbl_district_master as f on  e.Organization_District=f.District_Code  inner join tbl_course_master as g on a.apply_rscfa_cert_course=g.Course_Code where a.apply_rscfa_cert_id='" . $cid . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAllPrintRecpDwnldReexam($cid) { /// for new invoice system GST
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
					$cid = mysqli_real_escape_string($_ObjConnection->Connect(),$cid);
					
			   $_SelectQuery = "Select a.*,b.*,c.*,d.*,e.Organization_Name ,Course_Name,District_Name from tbl_rscfa_reexam_application as a inner join tbl_user_master as b on a.itgkcode=b.User_LoginId inner join tbl_rscfa_reexam_invoice as c on a.rscfa_reexam_code= c.transaction_ref_id inner join tbl_gst_invoice_master as d on d.Gst_Invoce_Category_Type='RscfaReExam' inner join tbl_organization_detail as e on b.User_Code=e.Organization_User inner join tbl_district_master as f on  e.Organization_District=f.District_Code  inner join tbl_course_master as g on a.coursename=g.Course_Code where a.rscfa_reexam_code='" . $cid . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function addPhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        if ($coursecode == '5') {
            $Imagepath = '../upload/DownloadPrintRecpt/rscfa.jpg';
        } else {
            $Imagepath = '../upload/DownloadPrintRecpt/rscit.jpg';
        }

        $pdf->Image($Imagepath, 8, 105, 50, 25);
        $pdf->Image($Imagepath, 8, 251, 50, 25);

        $pdf->Output($path, "F");
    }

    public function convert_number_to_words($number) {

        $hyphen = ' ';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'Zero',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Fourty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100 => 'Hundred',
            1000 => 'Thousand',
            1000000 => 'Million',
            1000000000 => 'Billion',
            1000000000000 => 'Trillion',
            1000000000000000 => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

}
