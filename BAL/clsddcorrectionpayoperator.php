<?php
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsddcorrectionpayoperator { 
	
	public function GetAllDETAILS($_action, $_Correction) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//$_ITGK=$_SESSION['User_LoginId'];
		    $_SelectQuery = "Select * FROM  `tbl_correction_copy` WHERE Correction_Payment_Status = '0' AND lcode='" . $_action . "' AND applicationfor = '" . $_Correction . "' ORDER BY cid DESC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function Update($_Code, $_LearnerCode, $_PayTypeCode, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName, $_cid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_correction_ddpay (correction_ddpay_code, dd_Transaction_Status, dd_Transaction_Name, dd_amount,"
                    . "dd_Transaction_Txtid, dd_no, dd_date,"
                    . "dd_centercode,dd_bank,dd_branch,dd_Transaction_Email) "
                    . "Select Case When Max(correction_ddpay_code) Is Null Then 1 Else Max(correction_ddpay_code)+1 End as correction_ddpay_code,"
                    . "'Intermediate' as dd_Transaction_Status,'" . $_firstname . "' as dd_Transaction_Name,'" . $_amount . "' as dd_amount,"
                    . "'" . $_TranRefNo . "' as dd_Transaction_Txtid,"
                    . "'" . $_ddno . "' as dd_no,'" . $_dddate . "' as dd_date,'" . $_Code . "' as dd_centercode,'" . $_ddlBankName . "' as dd_bank,'" . $_txtBranchName . "' as dd_branch,'" . $_email . "' as dd_Transaction_Email"
                    . " From tbl_correction_ddpay";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

			$_InsertQuery1 = "INSERT INTO tbl_correction_transaction (Correction_Transaction_Code, Correction_Transaction_Status, Correction_Transaction_Fname, Correction_Transaction_Amount,"
								. "Correction_Transaction_Txtid, Correction_Payment_Mode, Correction_Transaction_ProdInfo, Correction_Transaction_Email,"
								. "Correction_Transaction_CenterCode) "
								. "Select Case When Max(Correction_Transaction_Code) Is Null Then 1 Else Max(Correction_Transaction_Code)+1 End as Correction_Transaction_Code,"
								. "'Intermediate' as Correction_Transaction_Status,'" .$_firstname. "' as Correction_Transaction_Fname,'" .$_amount. "' as Correction_Transaction_Amount,"
								. "'" .$_TranRefNo. "' as Correction_Transaction_Txtid, 'DD Mode' as Correction_Payment_Mode,"
								. "'" .$_PayTypeCode. "' as Correction_Transaction_ProdInfo,'" .$_email. "' as Correction_Transaction_Email,'" .$_Code. "' as Correction_Transaction_CenterCode"
								. " From tbl_correction_transaction";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);

            $_UpdateQuery = "Update tbl_correction_copy set Correction_Payment_Status = '8', Correction_TranRefNo='" . $_TranRefNo . "' "
                    . "Where Correction_ITGK_Code='" . $_Code . "' AND lcode='" . $_LearnerCode . "' AND cid='" . $_cid . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}

?>