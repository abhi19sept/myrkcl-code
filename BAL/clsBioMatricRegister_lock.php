<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of clsApplyEoi
     *
     *  author Viveks
     */

    require 'DAL/classconnectionNEW.php';

    $_ObjConnection = new _Connection();
    $_Response = array();

    class clsBioMatricRegister
    {
        //put your code here

        public function RegisterBioMatric($tdSerial, $tdMake, $tdModel, $tdWidth, $tdHeight, $tdLocalMac, $tdLocalIP, $txtStatus)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                $tdSerial = mysqli_real_escape_string($_ObjConnection->Connect(),$tdSerial);
                $tdMake = mysqli_real_escape_string($_ObjConnection->Connect(),$tdMake);
                $tdModel = mysqli_real_escape_string($_ObjConnection->Connect(),$tdModel);
                $_ITGK_Code = $_SESSION['User_LoginId'];
                $_InsertQuery = "INSERT INTO tbl_biomatric_register(BioMatric_Register_Code,BioMatric_ITGK_Code,BioMatric_SerailNo,BioMatric_Make,"
                    . "BioMatric_Model,BioMatric_Width,BioMatric_Height,BioMatric_Local_MAC,BioMatric_Local_IP,BioMatric_Status) "
                    . "SELECT CASE WHEN Max(BioMatric_Register_Code) IS NULL THEN 1 ELSE Max(BioMatric_Register_Code)+1 END AS BioMatric_Register_Code,"
                    . "'" . $_ITGK_Code . "' AS BioMatric_ITGK_Code,'" . $tdSerial . "' AS BioMatric_SerailNo,"
                    . "'" . $tdMake . "' AS BioMatric_Make,'" . $tdModel . "' AS BioMatric_Model,"
                    . "'" . $tdWidth . "' AS BioMatric_Width,'" . $tdHeight . "' AS BioMatric_Height,'" . $tdLocalMac . "' AS BioMatric_Local_MAC,'" . $tdLocalIP . "' AS BioMatric_Local_IP,'" . $txtStatus . "' AS BioMatric_Status"
                    . " FROM tbl_biomatric_register";

                $_DuplicateQuery = "SELECT count(*) as machine_count FROM tbl_biomatric_register WHERE BioMatric_SerailNo ='" . $tdSerial . "' AND BioMatric_Make = '".$tdMake."' AND BioMatric_Model = '".$tdModel."' AND BioMatric_Flag = 1";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                $machine_count = 0;

                while($row = mysqli_fetch_array($_Response[2])){
                    $machine_count = $row['machine_count'];
                }

                if ($machine_count == 0) {
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response[0] = "Device has been successfully registered";
                    $_Response[1] = 1;
                } else {
                    $_Response[0] = Message::DuplicateRecord;
                    $_Response[1] = Message::Error;
                }
            } catch (Exception $_e) {
                $_Response[0] = $_e->getTraceAsString();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
    }
