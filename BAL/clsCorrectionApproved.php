<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionApproved {
    //put your code here
    
    public function GetAll($_CenterCode, $_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
			$_SelectQuery = "Select * FROM tbl_correction_copy WHERE Correction_ITGK_Code = '" . $_CenterCode . "' AND
							dispatchstatus = '" . $_Status . "' AND Correction_Payment_Status='1' group by lcode order by cid DESC"; 
			$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function GetDatabyCode($_Cid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
            $_SelectQuery = "Select * From tbl_correction_copy Where cid='" . $_Cid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetOldValues($_CorrectionId)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_CorrectionId = mysqli_real_escape_string($_ObjConnection->Connect(),$_CorrectionId);
           $_SelectQuery = "Select upper(Admission_Name) as Admission_Name,upper(Admission_Fname) as Admission_Fname,Admission_Code,mobile From 
									tbl_admission as a inner join tbl_correction_copy as b
									on a.Admission_LearnerCode=b.lcode
								Where cid='" . $_CorrectionId . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

	 public function FILLAPPROVEDSTATUS() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT capprovalid, cstatus FROM tbl_capcategory where capprovalid in (0,4) and correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GETALLLOT($Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if($Status=='4'){
					$_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where lotid='0'";
				}else{
					 $_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where correctionrole='Yes'";
				}
           
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function UpdateToProcess($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_Txnid, $_Lcode, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
            $_Txnid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Txnid);
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					
				$_selectamount = "select Correction_Transaction_Amount from tbl_correction_transaction where 
								Correction_Transaction_Status='Success' AND 
								Correction_Transaction_Txtid='" . $_Txnid . "'";
				$_TxnAmt = $_ObjConnection->ExecuteQuery($_selectamount, Message::SelectStatement);
				
				
				$_SelectCount= "Select count(lcode) AS Lcode, Correction_Fee from tbl_correction_copy where 
								Correction_Payment_Status='1' AND Correction_TranRefNo='" . $_Txnid . "'
								group by Correction_TranRefNo";
				$_Learnercount = $_ObjConnection->ExecuteQuery($_SelectCount, Message::SelectStatement);
				$num_rows = mysqli_num_rows($_Learnercount[2]);
				
				
				
				if($_TxnAmt[0] == 'Success' && $num_rows == '1') {
					$_Row1 = mysqli_fetch_array($_TxnAmt[2]);
					$_CorrectionTxnAmt = $_Row1['Correction_Transaction_Amount'];
					
					$_Row2 = mysqli_fetch_array($_Learnercount[2]);				
					$_PayTxnid=$_Row2['Lcode'];	
					$_Fee=$_Row2['Correction_Fee'];	
					$correction_amount = ($_PayTxnid * $_Fee);
					
					if($_CorrectionTxnAmt == $correction_amount) {
						date_default_timezone_set("Asia/Kolkata");
						$processdate = date("Y-m-d H:i:s");
						
						if($_ProcessStatus=='0'){
								$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "',
										remarks='" . $_Remark . "', form_process_date='".$processdate."',
										lot='" . $_LOT . "', totalmarks='" . $_Marks . "'" 
										. " Where cid='" . $_Cid . "'";
								$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						
								$_UpdateQuery3 = "Update tbl_correction_copy as a, tbl_admission as b 
								set 										
										a.loglearnername=b.Admission_Name,a.logfathername=b.Admission_Fname,
										a.loglearnerdob=b.Admission_DOB
										Where a.lcode=b.Admission_LearnerCode and a.lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
						$_Responsess = $_ObjConnection->ExecuteQuery($_UpdateQuery3, Message::UpdateStatement);
						
						$_UpdateQuery4 = "Update tbl_admission as a,tbl_correction_copy as b 
						set 										
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,
										a.Admission_DOB=b.dob
										Where a.Admission_LearnerCode=b.lcode and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
						$_Responsesss = $_ObjConnection->ExecuteQuery($_UpdateQuery4, Message::UpdateStatement);
						
						$_SelectQuery1 = "Select Admission_Aadhar_Status,b.Batch_Name as batchname,Admission_Photo,Admission_Course
											From tbl_admission as a inner join tbl_batch_master as b
											on a.Admission_Batch=b.Batch_Code 
											Where Admission_LearnerCode='" . $_Lcode . "'";
						$_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
							if($_selectResponse[0] == 'Success'){
								$_AadharStatus = mysqli_fetch_array($_selectResponse[2]);
									if($_AadharStatus['Admission_Aadhar_Status']=='2'){
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,tbl_final_result as c set 
										a.Admission_Aadhar_Status='3',
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,a.Admission_DOB=b.dob,
										c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);		
									}
									else{
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,tbl_final_result as c set 
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,a.Admission_DOB=b.dob,
										c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
									}
									$bname=$_AadharStatus['batchname'];
									$photoname=$_AadharStatus['Admission_Photo'];
									$coursecode=$_AadharStatus['Admission_Course'];
									$batcharray=explode(" ",$bname);
									$batchfolder=$batcharray[0].'_'.$batcharray[1];
									$coursepath='';
									$moveimagefolder='';
									
										if($coursecode=='3'){
											$coursepath='_Women';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='4'){
											$coursepath='_Gov';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='20'){
											$coursepath='_Madarsa';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else{
											$moveimagefolder=$batchfolder;
										}
								
									if(!is_dir("../upload/admissionphoto/".$moveimagefolder)){										
										mkdir("../upload/admissionphoto/".$moveimagefolder, 0755);
									}
									$imagePath = '../upload/correction_photo/'.$_Lcode.'_photo'.'.jpg';
									$logPath = '../upload/admission_photo/'. $photoname;
									$logCopyPath = '../upload/admission_correction_photo_log/'.$photoname;
									$newPath = "../upload/admissionphoto/".$moveimagefolder."/".$_Lcode."_photo.jpg";
									$copiedlog = copy($logPath , $logCopyPath);
									$copied = copy($imagePath , $newPath);
							}
							
								$_SMS = "Your correction/duplicate certificate application is approved by RKCL. Your 
										correction/duplicate certificate will delivered to your registered ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
					}					
							
							else if($_ProcessStatus=='4'){
								$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "',
										remarks='" . $_Remark . "', form_process_date='".$processdate."',
										lot='" . $_LOT . "' Where cid='" . $_Cid . "'";
								$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
								
								$_SMS = "Dear Learner, Your correction/duplicate certificate application is rejected by RKCL. Kindly contact to your ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
							}
						
					}
					else {
						echo "Amount for Trans. Ref. No. ".$_Txnid." need to be Reconciled";
						return; 
					}
				}
				else {
						echo "Unexceptional Condition for Trans. Ref. No. ".$_Txnid." is need to be Reconciled";
						return; 
					}
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
                      
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateToProcessRejected($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_LName, $_Fname, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        /*try {
				date_default_timezone_set("Asia/Kolkata");
			$processdate = date("Y-m-d H:i:s");
           $_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', remarks='" . $_Remark . "',"
                    . "lot='" . $_LOT . "', totalmarks='" . $_Marks . "', cfname='" . $_LName . "', cfaname='" . $_Fname . "',"
                    . " form_process_date='".$processdate."' Where cid='" . $_Cid . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			$_SMS = "Dear Learner, Your correction/duplicate certificate application is rejected by RKCL. Kindly contact to your ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
								
        } */
		try {
                    $_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {				
					
						date_default_timezone_set("Asia/Kolkata");
						$processdate = date("Y-m-d H:i:s");
					if($_ProcessStatus=='0'){
								$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', 
										 remarks='" . $_Remark . "',lot='" . $_LOT . "', totalmarks='" . $_Marks . "', 
										 cfname='" . $_LName . "',cfaname='" . $_Fname . "',
										 form_process_date='".$processdate."' Where cid='" . $_Cid . "'";
								$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						
								$_UpdateQuery3 = "Update tbl_correction_copy as a, tbl_admission as b 
										 set a.loglearnername=b.Admission_Name, a.logfathername=b.Admission_Fname,
										 a.loglearnerdob=b.Admission_DOB
										Where a.lcode=b.Admission_LearnerCode and a.lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
								$_Responsess = $_ObjConnection->ExecuteQuery($_UpdateQuery3, Message::UpdateStatement);
						
								$_UpdateQuery4 = "Update tbl_admission as a,tbl_correction_copy as b 
										set a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,
										a.Admission_DOB=b.dob
										Where a.Admission_LearnerCode=b.lcode and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
								$_Responsesss = $_ObjConnection->ExecuteQuery($_UpdateQuery4, Message::UpdateStatement);
								
								$_SelectQuery1 = "Select Admission_Aadhar_Status,b.Batch_Name as 
											batchname,Admission_Photo,Admission_Course
											From tbl_admission as a inner join tbl_batch_master as b
											on a.Admission_Batch=b.Batch_Code 
											Where Admission_LearnerCode='" . $_Lcode . "'";
						$_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
							if($_selectResponse[0] == 'Success'){
								$_AadharStatus = mysqli_fetch_array($_selectResponse[2]);
									if($_AadharStatus['Admission_Aadhar_Status']=='2'){
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,
										tbl_final_result as c set a.Admission_Aadhar_Status='3',
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,a.Admission_DOB=b.dob,
										c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and
										lcode='" . $_Lcode . "' and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);		
									}
									else{
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,
										tbl_final_result as c set a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,
										a.Admission_DOB=b.dob,c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and
										lcode='" . $_Lcode . "' and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
									}
									$bname=$_AadharStatus['batchname'];
									$photoname=$_AadharStatus['Admission_Photo'];
									$coursecode=$_AadharStatus['Admission_Course'];
									$batcharray=explode(" ",$bname);
									$batchfolder=$batcharray[0].'_'.$batcharray[1];
									$coursepath='';
									$moveimagefolder='';
									
										if($coursecode=='3'){
											$coursepath='_Women';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='4'){
											$coursepath='_Gov';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='20'){
											$coursepath='_Madarsa';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else{
											$moveimagefolder=$batchfolder;
										}
								
									if(!is_dir("../upload/admissionphoto/".$moveimagefolder)){							
										mkdir("../upload/admissionphoto/".$moveimagefolder, 0755);
									}
									$imagePath = '../upload/correction_photo/'.$_Lcode.'_photo'.'.jpg';
									$logPath = '../upload/admission_photo/'. $photoname;
									$logCopyPath = '../upload/admission_correction_photo_log/'.$photoname;
									$newPath = "../upload/admissionphoto/".$moveimagefolder."/".$_Lcode."_photo.jpg";
									$copiedlog = copy($logPath , $logCopyPath);
									$copied = copy($imagePath , $newPath);
							}
							
								$_SMS = "Your correction/duplicate certificate application is approved by RKCL. Your 
										correction/duplicate certificate will delivered to your registered ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
					}
						
						
							
							else if($_ProcessStatus=='4'){
								$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', 
										 remarks='" . $_Remark . "',lot='" . $_LOT . "', 
										 form_process_date='".$processdate."' Where cid='" . $_Cid . "'";
								$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
								
								$_SMS = "Dear Learner, Your correction/duplicate certificate application is rejected by RKCL. Kindly contact to your ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
							}
											
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
                      
        }
		catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateToProcessWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks, $_Txnid, $_Lcode, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	
            $_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
            $_Txnid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Txnid);
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lcode);
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					
				$_selectamount = "select Correction_Transaction_Amount from tbl_correction_transaction where
									Correction_Transaction_Status='Success' AND 
									Correction_Transaction_Txtid='" . $_Txnid . "'";
				$_TxnAmt = $_ObjConnection->ExecuteQuery($_selectamount, Message::SelectStatement);
				
				
				$_SelectCount= "Select count(lcode) AS Lcode, Correction_Fee from tbl_correction_copy 
								where Correction_Payment_Status='1' AND Correction_TranRefNo='" . $_Txnid . "'
								group by Correction_TranRefNo";
				$_Learnercount = $_ObjConnection->ExecuteQuery($_SelectCount, Message::SelectStatement);
				$num_rows = mysqli_num_rows($_Learnercount[2]);			
				
				if($_TxnAmt[0] == 'Success' && $num_rows == '1') {
					$_Row1 = mysqli_fetch_array($_TxnAmt[2]);
					$_CorrectionTxnAmt = $_Row1['Correction_Transaction_Amount'];
					
					$_Row2 = mysqli_fetch_array($_Learnercount[2]);				
					$_PayTxnid=$_Row2['Lcode'];	
					$_Fee=$_Row2['Correction_Fee'];	
					$correction_amount = ($_PayTxnid * $_Fee);
					
					if($_CorrectionTxnAmt == $correction_amount) {
						date_default_timezone_set("Asia/Kolkata");
						$processdate = date("Y-m-d H:i:s");
						if($_ProcessStatus=='0'){
							$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', 
										remarks='" . $_Remark . "', exameventname= '" . $_EventName . "',"
										. "lot='" . $_LOT . "', totalmarks='" . $_Marks . "',"
										. " form_process_date='".$processdate."' Where cid='" . $_Cid . "'";
						$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						
						$_UpdateQuery3 = "Update tbl_correction_copy as a, tbl_admission as b set 										
										a.loglearnername=b.Admission_Name,a.logfathername=b.Admission_Fname,
										a.loglearnerdob=b.Admission_DOB
										Where a.lcode=b.Admission_LearnerCode and a.lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
						$_Responsess = $_ObjConnection->ExecuteQuery($_UpdateQuery3, Message::UpdateStatement);
						
						$_UpdateQuery4 = "Update tbl_admission as a,tbl_correction_copy as b set 										
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,
										a.Admission_DOB=b.dob
										Where a.Admission_LearnerCode=b.lcode and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
						$_Responsesss = $_ObjConnection->ExecuteQuery($_UpdateQuery4, Message::UpdateStatement);
						
						$_SelectQuery1 = "Select Admission_Aadhar_Status,b.Batch_Name as batchname,Admission_Photo,Admission_Course
											From tbl_admission as a inner join tbl_batch_master as b
											on a.Admission_Batch=b.Batch_Code 
											Where Admission_LearnerCode='" . $_Lcode . "'";
						$_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
							if($_selectResponse[0] == 'Success'){
								$_AadharStatus = mysqli_fetch_array($_selectResponse[2]);
									if($_AadharStatus['Admission_Aadhar_Status']=='2'){
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,tbl_final_result as c set 
										a.Admission_Aadhar_Status='3',
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,a.Admission_DOB=b.dob,
										c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);		
									}
									else{
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,tbl_final_result as c set 
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,a.Admission_DOB=b.dob,
										c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
									}
									$bname=$_AadharStatus['batchname'];
									$photoname=$_AadharStatus['Admission_Photo'];
									$coursecode=$_AadharStatus['Admission_Course'];
									$batcharray=explode(" ",$bname);
									$batchfolder=$batcharray[0].'_'.$batcharray[1];
									$coursepath='';
									$moveimagefolder='';
									
										if($coursecode=='3'){
											$coursepath='_Women';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='4'){
											$coursepath='_Gov';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='20'){
											$coursepath='_Madarsa';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else{
											$moveimagefolder=$batchfolder;
										}
									if(!is_dir("../upload/admissionphoto/".$moveimagefolder)){										
										mkdir("../upload/admissionphoto/".$moveimagefolder, 0755);
									}								
									$imagePath = '../upload/correction_photo/'.$_Lcode.'_photo'.'.jpg';
									$logPath = '../upload/admission_photo/'. $photoname;
									$logCopyPath = '../upload/admission_correction_photo_log/'.$photoname;
									$newPath = "../upload/admissionphoto/".$moveimagefolder."/".$_Lcode."_photo.jpg";
									$copiedlog = copy($logPath , $logCopyPath);
									$copied = copy($imagePath , $newPath);
							}
							
								$_SMS = "Your correction/duplicate certificate application is approved by RKCL. Your 
										correction/duplicate certificate will delivered to your registered ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
							}					
						
						else if($_ProcessStatus=='4'){
							$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', 
										remarks='" . $_Remark . "',lot='" . $_LOT . "', "
										. " form_process_date='".$processdate."' Where cid='" . $_Cid . "'";
							$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
							
								$_SMS = "Dear Learner, Your correction/duplicate certificate application is rejected by RKCL. Kindly contact to your ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
							}	
						
					}
					else {
						echo "Amount for Trans. Ref. No. ".$_Txnid." need to be Reconciled";
						return; 
					}
				}
				else {
						echo "Unexceptional Condition for Trans. Ref. No. ".$_Txnid." is need to be Reconciled";
						return; 
					}
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
                      
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateToProcessRejectedWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks,
													$_LName, $_Fname, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        /*try {		
			date_default_timezone_set("Asia/Kolkata");
			$processdate = date("Y-m-d H:i:s");
           $_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "', remarks='" . $_Remark . "', 
							exameventname= '" . $_EventName . "',form_process_date='".$processdate."',"
                    . "lot='" . $_LOT . "', totalmarks='" . $_Marks . "', cfname='" . $_LName . "', cfaname='" . $_Fname . "'"
                    . " Where cid='" . $_Cid . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			$_SMS = "Dear Learner, Your correction/duplicate certificate application is rejected by RKCL. Kindly contact to your ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
        }*/
		try {	
                    $_Cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Cid);
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {				
				
						date_default_timezone_set("Asia/Kolkata");
						$processdate = date("Y-m-d H:i:s");
						if($_ProcessStatus=='0'){
								$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "',
										remarks='" . $_Remark . "', exameventname= '" . $_EventName . "',
										form_process_date='".$processdate."',lot='" . $_LOT . "', 
										totalmarks='" . $_Marks . "', cfname='" . $_LName . "', cfaname='" . $_Fname . "'
										Where cid='" . $_Cid . "'";
						$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						
						$_UpdateQuery3 = "Update tbl_correction_copy as a, tbl_admission as b 
										set 										
										a.loglearnername=b.Admission_Name,a.logfathername=b.Admission_Fname,
										a.loglearnerdob=b.Admission_DOB
										Where a.lcode=b.Admission_LearnerCode and a.lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
						$_Responsess = $_ObjConnection->ExecuteQuery($_UpdateQuery3, Message::UpdateStatement);
						
						$_UpdateQuery4 = "Update tbl_admission as a,tbl_correction_copy as b 
										set 										
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,
										a.Admission_DOB=b.dob
										Where a.Admission_LearnerCode=b.lcode and lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
						$_Responsesss = $_ObjConnection->ExecuteQuery($_UpdateQuery4, Message::UpdateStatement);
						
						$_SelectQuery1 = "Select Admission_Aadhar_Status,b.Batch_Name as 
											batchname,Admission_Photo,Admission_Course
											From tbl_admission as a inner join tbl_batch_master as b
											on a.Admission_Batch=b.Batch_Code 
											Where Admission_LearnerCode='" . $_Lcode . "'";
						$_selectResponse=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
							if($_selectResponse[0] == 'Success'){
								$_AadharStatus = mysqli_fetch_array($_selectResponse[2]);
									if($_AadharStatus['Admission_Aadhar_Status']=='2'){
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,
										tbl_final_result as c set a.Admission_Aadhar_Status='3',
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,a.Admission_DOB=b.dob,
										c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and
										lcode='" . $_Lcode . "' and Correction_Payment_Status='1' and
										cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);		
									}
									else{
										$_UpdateQuery1 = "Update tbl_admission as a,tbl_correction_copy as b,
										tbl_final_result as c set 
										a.Admission_Name=b.cfname,a.Admission_Fname=b.cfaname,a.Admission_DOB=b.dob,
										c.name=b.cfname,c.fname=b.cfaname,c.dob=b.dob
										Where a.Admission_LearnerCode=b.lcode and b.lcode=c.scol_no and
										lcode='" . $_Lcode . "' 
										and Correction_Payment_Status='1' and cid='" . $_Cid . "'";
										$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
									}
									$bname=$_AadharStatus['batchname'];
									$photoname=$_AadharStatus['Admission_Photo'];
									$coursecode=$_AadharStatus['Admission_Course'];
									$batcharray=explode(" ",$bname);
									$batchfolder=$batcharray[0].'_'.$batcharray[1];
									$coursepath='';
									$moveimagefolder='';
									
										if($coursecode=='3'){
											$coursepath='_Women';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='4'){
											$coursepath='_Gov';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else if($coursecode=='20'){
											$coursepath='_Madarsa';
											$moveimagefolder=$batchfolder.$coursepath;
										}
										else{
											$moveimagefolder=$batchfolder;
										}
									if(!is_dir("../upload/admissionphoto/".$moveimagefolder)){										
										mkdir("../upload/admissionphoto/".$moveimagefolder, 0755);
									}								
									$imagePath = '../upload/correction_photo/'.$_Lcode.'_photo'.'.jpg';
									$logPath = '../upload/admission_photo/'. $photoname;
									$logCopyPath = '../upload/admission_correction_photo_log/'.$photoname;
									$newPath = "../upload/admissionphoto/".$moveimagefolder."/".$_Lcode."_photo.jpg";
									$copiedlog = copy($logPath , $logCopyPath);
									$copied = copy($imagePath , $newPath);
							}
							
								$_SMS = "Your correction/duplicate certificate application is approved by RKCL. Your 
										correction/duplicate certificate will delivered to your registered ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
							}
						
						
						
						
						else if($_ProcessStatus=='4'){
								$_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_ProcessStatus . "',
									remarks='" . $_Remark . "',form_process_date='".$processdate."',lot='" . $_LOT . "'
									Where cid='" . $_Cid . "'";
								$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						
								$_SMS = "Dear Learner, Your correction/duplicate certificate application is rejected by RKCL. Kindly contact to your ITGK.";
								//$_Mobile=$_MobileNo;
								SendSMS($_Mobile, $_SMS);
							}
							
						
					
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
                      
        }
		catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function FILLStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetLearnerHistory($learnercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select lcode, Correction_ITGK_Code, upper(cfname) as name, mobile, upper(cfaname) as fname, cid,
							applicationfor, application_type, applicationdate, cstatus, dispatchstatus from tbl_correction_copy as a 
							inner join tbl_capcategory as b on a.dispatchstatus=b.capprovalid where 
							lcode='".$learnercode."' and Correction_Payment_Status='1' order by cid DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetLotName($_cid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_cid = mysqli_real_escape_string($_ObjConnection->Connect(),$_cid);
            $_SelectQuery = "select b.lotname from tbl_correction_copy as a inner join tbl_clot as b on a.lot=b.lotid
								where cid='".$_cid."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}