<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBlockUnblock
 *
 * @author Vivek
 */
//require 'DAL/classconnectionNEW.php';
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsBlockUnblock {

    //put your code here
    public function GenerateUploadID() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Replace(Replace(Replace(NOW(),'-',''),':',''),' ','') as UploadId";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetUnblockCenter($key) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $key = mysqli_real_escape_string($_ObjConnection->Connect(),$key);
            $_SelectQuery = "select * from mregistration where (centercode LIKE '%{$key}%' or centername LIKE '%{$key}%') and activityid=2";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetBlockCenter() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from mregistration where (centercode LIKE '%{$key}%' or centername LIKE '%{$key}%') and activityid=1";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCenterDetail($CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CenterCode);
            $_SelectQuery = "select a.User_Code, a.User_LoginId as CenterCode, a.User_MobileNo, b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, c.Organization_Name as SPName, a.DLC, d.Organization_Name as DLCName, e.Tehsil_Name, f.District_Name from ( select user_code,User_MobileNo,user_loginid, User_Rsp as SP,SUBSTRING_INDEX(user_parentid,'|',1) as DLC from tbl_user_master where user_userroll=7) as a INNER JOIN tbl_organization_detail as b on a.User_Code = b.Organization_User Inner Join tbl_Organization_Detail as c on a.SP = c.Organization_User inner join tbl_Organization_Detail as D on a.DLC = D.Organization_User Inner Join tbl_tehsil_master as e on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f on f.District_Code =  b.Organization_District where a.User_LoginId = " . $CenterCode . " ";

           // $_SelectQuery = "select a.User_Code, a.User_LoginId as CenterCode, a.User_MobileNo, b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, c.Organization_Name as PSAName, a.DLC, d.Organization_Name as DLCName, e.Tehsil_Name, f.District_Name from ( select user_code,User_MobileNo,user_loginid, SUBSTRING_INDEX(user_parentid,'|',-1) as PSA,SUBSTRING_INDEX(user_parentid,'|',1) as DLC from tbl_user_master where user_userroll=7) as a INNER JOIN tbl_organization_detail as b on a.User_Code = b.Organization_User Inner Join tbl_Organization_Detail as c on a.PSA = c.Organization_User inner join tbl_Organization_Detail as D on a.DLC = D.Organization_User Inner Join tbl_tehsil_master as e on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f on f.District_Code =  b.Organization_District where a.User_LoginId = " . $CenterCode . " ";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

  public function GetCenterDetailReport($CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CenterCode);
            $_SelectQuery = "select a.User_Code, a.User_LoginId as CenterCode, a.User_EmailId,a.User_MobileNo, "
                    . "b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, "
                    . "c.Organization_Name as RSPName, e.Tehsil_Name, "
                    . "f.District_Name from tbl_user_master as a INNER JOIN tbl_organization_detail as b "
                    . "on a.User_Code = b.Organization_User Inner Join tbl_organization_detail as c "
                    . "on a.User_Rsp = c.Organization_User Inner Join tbl_tehsil_master as e "
                    . "on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f "
                    . "on f.District_Code =  b.Organization_District where a.User_LoginId = " . $CenterCode . " ";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetBlockCategory() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select blockcategoryid,blockcategories from blockcategory";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GETCourse($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_SelectQuery = "Select DISTINCT Courseitgk_Course, Courseitgk_Code from tbl_courseitgk_mapping where Courseitgk_ITGK = " . $_CenterCode . " AND EOI_Fee_Confirm = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GETBlockCourse($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_SelectQuery = "Select DISTINCT Courseitgk_Course, Courseitgk_Code from tbl_courseitgk_mapping where Courseitgk_ITGK = " . $_CenterCode . " AND EOI_Fee_Confirm = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GETUnBlockCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT Courseitgk_Course, Courseitgk_Code from tbl_courseitgk_mapping where CourseITGK_BlockStatus = 'Block' AND EOI_Fee_Confirm = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCourseCenter($_CourseName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseName = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseName);
            $_SelectQuery = "Select Courseitgk_ITGK from tbl_courseitgk_mapping where Courseitgk_Course = '" . $_CourseName . "' AND EOI_Fee_Confirm = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GETBulkBlockCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Distinct Courseitgk_Course from tbl_courseitgk_mapping where CourseITGK_BlockStatus = 'unblock' AND EOI_Fee_Confirm = 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetUnBlockCategory() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select categoryid,categories from unblockcategory";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add($_Activity, $_ActivityId, $_CenterCode, $_ActivityDate, $_BlockCategory, $_Remark, $_BlockDuration, $_DurationType) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_UploadId = $_SESSION['User_LoginId'];

                $_InsertQuery = "Insert Into Block(activity,activityid,centercode,activitydate,"
                        . "activityreason,remark,uploadid,duration,durationtype,blocktype) values('" . $_Activity . "',"
                        . "'" . $_ActivityId . "','" . $_CenterCode . "','" . $_ActivityDate . "',"
                        . "'" . $_BlockCategory . "','" . $_Remark . "','" . $_UploadId . "',"
                        . "'" . $_BlockDuration . "','" . $_DurationType . "','Full Block') ";

                $_UpdateQuery = "Update tbl_user_master Set User_Status = 2 "
                        . "Where User_LoginId = " . $_CenterCode . " ";

                //echo $_InsertQuery;
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                if ($_Response[0] == Message::SuccessfullyInsert) {
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function BULKUNBLOCK($_Activity, $_ActivityId, $_CenterCode, $_ActivityDate, $_BlockCategory, $_Remark, $_UploadId,
								$_BlockDuration, $_DurationType) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_InsertQuery = "Insert Into Block(activity,activityid,centercode,activitydate,"
                    . "activityreason,remark,uploadid,duration,durationtype,blocktype) values('" . $_Activity . "',"
                    . "'" . $_ActivityId . "','" . $_CenterCode . "','" . $_ActivityDate . "',"
                    . "'" . $_BlockCategory . "','" . $_Remark . "','" . $_UploadId . "',"
                    . "'" . $_BlockDuration . "','" . $_DurationType . "','Full Block') ";

            		
			$_UpdateQuery = "Update tbl_user_master Set User_Status = '1' Where User_LoginId = '" . $_CenterCode . "'
							and User_Status = '2'";					
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::OnUpdateStatement);
			
			if ($_Response[2]>0) {
                $_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				echo $_Response[2];
				return; 
            }
			else{
				echo $_Response[2];
				return;
			}           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        //return $_Response;
    }

    public function UnblockCenter($_Activity, $_ActivityId, $_CenterCode, $_ActivityDate, $_BlockCategory, $_Remark,
								$_UploadId, $_BlockDuration, $_DurationType) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_InsertQuery = "Insert Into Block(activity,activityid,centercode,activitydate,"
                    . "activityreason,remark,uploadid,duration,durationtype,blocktype) values('" . $_Activity . "',"
                    . "'" . $_ActivityId . "','" . $_CenterCode . "','" . $_ActivityDate . "',"
                    . "'" . $_BlockCategory . "','" . $_Remark . "','" . $_UploadId . "',"
                    . "'" . $_BlockDuration . "','" . $_DurationType . "','Full Unblock') ";

            $_UpdateQuery = "Update tbl_user_master Set User_Status = '1' where User_LoginId = '" . $_CenterCode . "' 
							and User_Status='2'";
			$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::OnUpdateStatement);
            
            if ($_Response[2]>0) {
                $_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				echo $_Response[2];
				return; 
            }
			else{
				echo $_Response[2];
				return;
			}
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        //return $_Response;
    }

    public function AddCourseBlock($_Activity, $_ActivityId, $_CenterCode, $_ActivityDate, $_BlockCategory, $_Remark, $_BlockDuration, $_DurationType, $_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_UploadId = $_SESSION['User_LoginId'];			
				
                $_SelectCourse = "SELECT Courseitgk_Course,Courseitgk_Code FROM tbl_courseitgk_mapping WHERE 
									Courseitgk_Code IN (" . $_CourseCode . ")";
                $_CourseName = $_ObjConnection->ExecuteQuery($_SelectCourse, Message::SelectStatement);

                while ($_Row = mysqli_fetch_array($_CourseName[2])) {
                    $_CourseName2 = $_Row['Courseitgk_Course'];
                    $_Courseitgk_Code = $_Row['Courseitgk_Code'];
					
					 $_Selectstatus = "SELECT * FROM Block WHERE blocktype='" . $_CourseName2 . "' 
									and centercode='" . $_CenterCode . "' order by id DESC LIMIT 1";
					$_status = $_ObjConnection->ExecuteQuery($_Selectstatus, Message::SelectStatement);
						$_Rows = mysqli_fetch_array($_status[2]);
							$blockstatus=$_Rows['activity'];
								if($blockstatus=='Block'){
									$_Response[0]='Already Blocked';
								}
								else{					
								$_InsertQuery = "Insert Into Block(activity,activityid,centercode,activitydate,"
                            . "activityreason,remark,uploadid,duration,durationtype,blocktype) values('" . $_Activity . "',"
                            . "'" . $_ActivityId . "','" . $_CenterCode . "','" . $_ActivityDate . "',"
                            . "'" . $_BlockCategory . "','" . $_Remark . "','" . $_UploadId . "',"
                            . "'" . $_BlockDuration . "','" . $_DurationType . "','" . $_CourseName2 . "') ";

								$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								
								$_UpdateQuery = "Update tbl_courseitgk_mapping Set CourseITGK_BlockStatus = 'Block' "
								. "Where Courseitgk_Code='" . $_Courseitgk_Code . "'";
									if ($_Response[0] == Message::SuccessfullyInsert) {
										$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
									}
							}
                }
                
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddCourseUnBlock($_Activity, $_ActivityId, $_CenterCode, $_ActivityDate, $_BlockCategory, $_Remark, $_UploadId, $_BlockDuration, $_DurationType, $_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectCourse = "SELECT Courseitgk_Course,Courseitgk_Code FROM tbl_courseitgk_mapping WHERE 
								Courseitgk_Code IN (" . $_CourseCode . ")";
            $_CourseName = $_ObjConnection->ExecuteQuery($_SelectCourse, Message::SelectStatement);
            while ($_Row = mysqli_fetch_array($_CourseName[2])) {
                $_CourseName2 = $_Row['Courseitgk_Course'];
                $_Courseitgk_Code = $_Row['Courseitgk_Code'];

                $_InsertQuery = "Insert Into Block(activity,activityid,centercode,activitydate,"
                        . "activityreason,remark,uploadid,duration,durationtype,blocktype) values('" . $_Activity . "',"
                        . "'" . $_ActivityId . "','" . $_CenterCode . "','" . $_ActivityDate . "',"
                        . "'" . $_BlockCategory . "','" . $_Remark . "','" . $_UploadId . "',"
                        . "'" . $_BlockDuration . "','" . $_DurationType . "','" . $_CourseName2 . "') ";				
			
				$_UpdateQuery = "Update tbl_courseitgk_mapping Set CourseITGK_BlockStatus = 'unblock' 
							Where Courseitgk_Code='" . $_Courseitgk_Code . "' and CourseITGK_BlockStatus='Block'";
				$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::OnUpdateStatement);
				
				if ($_Response[2]>0) {
					$_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					echo $_Response[2];					
				}
				else{
					echo $_Response[2];					
				}                
            }           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response[2];
    }

    public function BulkCourseBlock($_Activity, $_ActivityId, $_CenterCode, $_ActivityDate, $_BlockCategory, $_Remark, $_UploadId, $_BlockDuration, $_DurationType, $_ITGKCenter, $_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {   
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_ITGKCenter = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGKCenter);
            $_InsertQuery = "Insert Into Block(activity,activityid,centercode,activitydate,"
                    . "activityreason,remark,uploadid,duration,durationtype,blocktype) values('" . $_Activity . "',"
                    . "'" . $_ActivityId . "','" . $_CenterCode . "','" . $_ActivityDate . "',"
                    . "'" . $_BlockCategory . "','" . $_Remark . "','" . $_UploadId . "',"
                    . "'" . $_BlockDuration . "','" . $_DurationType . "','" . $_CourseCode . "') ";

            $_UpdateQuery = "Update tbl_courseitgk_mapping Set CourseITGK_BlockStatus = 'Block' 
							Where Courseitgk_Course = '" . $_CourseCode . "' AND Courseitgk_ITGK = '" . $_ITGKCenter . "' ";

            //echo $_InsertQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            if ($_Response[0] == Message::SuccessfullyInsert) {
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function BulkCourseUnBlock($_Activity, $_ActivityId, $_CenterCode, $_ActivityDate, $_BlockCategory, $_Remark,
									$_UploadId, $_BlockDuration, $_DurationType, $_ITGKCenter, $_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_ITGKCenter = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGKCenter);
            $_SelectCourse = "SELECT Courseitgk_Course FROM tbl_courseitgk_mapping WHERE 
								Courseitgk_Code =  '" . $_CourseCode . "'";
            $_CourseName = $_ObjConnection->ExecuteQuery($_SelectCourse, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_CourseName[2]);
            $_CName = $_Row['Courseitgk_Course'];

            $_InsertQuery = "Insert Into Block(activity,activityid,centercode,activitydate,"
                    . "activityreason,remark,uploadid,duration,durationtype,blocktype) values('" . $_Activity . "',"
                    . "'" . $_ActivityId . "','" . $_CenterCode . "','" . $_ActivityDate . "',"
                    . "'" . $_BlockCategory . "','" . $_Remark . "','" . $_UploadId . "',"
                    . "'" . $_BlockDuration . "','" . $_DurationType . "','" . $_CourseCode . "') ";

            $_UpdateQuery = "Update tbl_courseitgk_mapping Set CourseITGK_BlockStatus = 'unblock' Where 
							 Courseitgk_Course = '" . $_CourseCode . "' AND Courseitgk_ITGK = '" . $_ITGKCenter . "'
							 AND CourseITGK_BlockStatus = 'Block'";			
			$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::OnUpdateStatement);
			
			if ($_Response[2]>0) {
                $_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				echo $_Response[2];
				return; 
            }
			else{
				echo $_Response[2];
				return;
			}           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        //return $_Response;
    }

    public function GetCenterWiseReport($CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CenterCode);
            $_SelectQuery = "Select * From block Where CenterCode='" . $CenterCode . "' Order By Id Desc";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

  public function GetStatusWise($Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($Status == "BOTH") {
                $_SelectQuery = "select a.User_Code, a.User_Status, a.User_LoginId as CenterCode, a.User_EmailId,a.User_MobileNo, "
                            . "b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, "
                            . "c.Organization_Name as RSPName, e.Tehsil_Name, g.remark, g.activitydate,"
                            . "f.District_Name from tbl_user_master as a INNER JOIN tbl_organization_detail as b "
                            . "on a.User_Code = b.Organization_User Inner Join tbl_Organization_Detail as c "
                            . "on a.User_Rsp = c.Organization_User Inner Join tbl_tehsil_master as e "
                            . "on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f "
                            . "on f.District_Code =  b.Organization_District Inner Join block as g "
                            . "on a.User_LoginId=g.centercode";
            } else {
                if ($Status == "BLOCKED") {
                    $_SelectQuery = "select a.User_Code, a.User_Status, a.User_LoginId as CenterCode, a.User_EmailId,a.User_MobileNo, "
                            . "b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, "
                            . "c.Organization_Name as RSPName, e.Tehsil_Name, g.remark, g.activitydate,"
                            . "f.District_Name from tbl_user_master as a INNER JOIN tbl_organization_detail as b "
                            . "on a.User_Code = b.Organization_User Inner Join tbl_Organization_Detail as c "
                            . "on a.User_Rsp = c.Organization_User Inner Join tbl_tehsil_master as e "
                            . "on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f "
                            . "on f.District_Code =  b.Organization_District Inner Join block as g "
                            . "on a.User_LoginId=g.centercode where a.User_Status=2";
                } else {
                    $_SelectQuery = "select a.User_Code, a.User_Status, a.User_LoginId as CenterCode, a.User_EmailId,a.User_MobileNo, "
                            . "b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, "
                            . "c.Organization_Name as RSPName, e.Tehsil_Name, g.remark, g.activitydate,"
                            . "f.District_Name from tbl_user_master as a INNER JOIN tbl_organization_detail as b "
                            . "on a.User_Code = b.Organization_User Inner Join tbl_Organization_Detail as c "
                            . "on a.User_Rsp = c.Organization_User Inner Join tbl_tehsil_master as e "
                            . "on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f "
                            . "on f.District_Code =  b.Organization_District Inner Join block as g "
                            . "on a.User_LoginId=g.centercode where a.User_Status=1 ";
                }
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	}