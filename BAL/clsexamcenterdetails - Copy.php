<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsexamcenterdetails {
	
	
	public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_examcenterdetailmaster as a inner join tbl_district_master as b on a.district=b.District_Code inner join tbl_tehsil_master as c on a.examtehsil=c.Tehsil_Code  inner join tbl_events as d on a.exameventname=d.Event_Id inner join tbl_exammaster as e on d.Event_Id=e.Affilate_Event where e.Event_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    //put your code here
     public function Add($_District,$_Center,$_Tehsil,$_Name,$_Address,$_Mobile,$_Email,$_Capacity,$_Rooms,$_Coordinator,$_Event,$_txtdate ,$_examid) 
       {
         //echo "org call";
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "Insert Into tbl_examcenterdetailmaster(district,examcentercode,examcentername,examcenteradd,
			examcentermobile,examcenteremail,examcentercapacity,
			 noofrooms,examcentercoordinator,examtehsil,exameventname,date,examid) 
												 VALUES ('" .$_District. "',
												 '" .$_Center. "',
												 '".$_Name ."',
												 '". $_Address ."',
												 '".$_Mobile."',
												 '". $_Email ."',
												 '". $_Capacity."',
												 '". $_Rooms ."',
												  '". $_Coordinator ."',
												 '". $_Tehsil ."',
												 '". $_Event ."',
												 '". $_txtdate ."',
												 '". $_examid ."')";
            $_DuplicateQuery = "Select * From tbl_examcenterdetailmaster Where examcentercode='" . $_Center . "' and examid='" . $_examid . "' ";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
       //print_r($_Response);
        return $_Response;
    }
	
	
	public function GetDatabyCode($_examcentercode,$_examid)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select *  From tbl_examcenterdetailmaster  Where examcentercode='" . $_examcentercode . "' and examid='" . $_examid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_examcentercode,$_examid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_DeleteQuery = "Delete From tbl_examcenterdetailmaster Where examcentercode='" . $_examcentercode . "' and examid='" . $_examid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	 public function Update($_Code,$_District,$_Center,$_Tehsil,$_Name,$_Address,$_Mobile,$_Email,$_Capacity,$_Rooms,$_Coordinator,$_Event,$_txtdate,$_examid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_examcenterdetailmaster set district='" . $_District . "',
		  
			". "examcentername='" . $_Name . "',
			". "examcenteradd='" . $_Address . "',
			". "examcentermobile='" . $_Mobile . "',
			". "examcenteremail='" . $_Email . "',
			". "examcentercapacity='" . $_Capacity . "',
			". "noofrooms='" . $_Rooms . "',
			". "examcentercoordinator='" . $_Coordinator . "',
			". "exameventname='" . $_Event . "',
			". "date='" . $_txtdate . "',
			". "examtehsil='" . $_Tehsil . "'
			
			Where examcentercode='" . $_Code . "' and  examid='" . $_examid . "'";
			
			
			
			
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
					
              
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
   
	public function Getcenter($district) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
                $_SelectQuery = "Select examcentercode From examcodemapping as a inner join tbl_district_master as b on a.district=b.District_Name WHERE b.District_Code='" . $district . "' ";
                
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
