<?php

/**
 * Description of clsgetpassword
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();

$_Response = array();

class clsgetpassword {

    //put your code here

    public function getpass($usercode) {
		global $_ObjConnection;                
        $_ObjConnection->Connect();
        try {
           $_SelectQuery = "Select User_LoginId,User_Password,User_EmailId,User_MobileNo From tbl_user_master
								WHERE User_LoginId = '".$usercode."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function getdecryptpass() {
        
        //global $_ObjConnection,$aut;
        global $_ObjConnection;
                
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select User_Code, User_LoginId, User_Password From tbl_user_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);

        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function decryptpassupdate($User_LoginId,$originalpassword) 
            {
        
        //global $_ObjConnection,$aut;
        global $_ObjConnection;
                
        $_ObjConnection->Connect();
        try {
            $query = "UPDATE tbl_user_master SET User_Password = '" . $originalpassword . "' WHERE User_LoginId = '" . $User_LoginId . "'";
            $_Response=$_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);

        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
