<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

date_default_timezone_set("Asia/Kolkata");
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

class clsWcdlearnercount {

    //put your code here

    public function GetAllPref($_course, $_batch, $_category) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginUserRole = $_SESSION['User_UserRoll'];
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(), $_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(), $_batch);

            if ($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4' || $_LoginUserRole == '8') {
                $_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, Wcd_Intake_Available_SC, Wcd_Intake_Available_ST,
                                            sum(case when Oasis_Admission_LearnerStatus in ('Pending','Approved','Rejected') then 1 else 0 end) AS pref,
                                            sum(case when Oasis_Admission_LearnerStatus = 'Approved' then 1 else 0 end) Confirmed_Learner
                                            from tbl_user_master as a
                                            inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.user_loginid inner join tbl_district_master as c on 
                                            b.Oasis_Admission_District = c.District_Code WHERE a.User_UserRoll='7' and b.Oasis_Admission_Course='" . $_course . "' AND 
                                            b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility LIKE('eligible')
                                            AND Oasis_Admission_SpecialCat_Flag='" . $_category . "'
                                            group by b.Oasis_Admission_District";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else if ($_LoginUserRole == '17') {
                //print_r($_SESSION);
                /* $_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, Count(b.Oasis_Admission_LearnerCode) 
                  AS pref from tbl_user_master as a
                  inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.User_LoginId inner join tbl_district_master as c on
                  b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='3' AND b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible' AND b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "'
                  AND Oasis_Admission_LearnerStatus in ('Pending','Approved') group by b.Oasis_Admission_Final_Preference"; */

                $_SelectQuery1 = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference,
                                            Wcd_Intake_Available_SC, Wcd_Intake_Available_ST,
                                            sum(case when Oasis_Admission_LearnerStatus in ('Pending','Approved','Rejected')
                                            then 1 else 0 end) AS pref,
                                            sum(case when Oasis_Admission_LearnerStatus = 'Approved' then 1 else 0 end) Confirmed_Learner, 
                                            sum(case when Oasis_Admission_Category in ('2','3') then 1 else 0 end) ResCount, 
                                            sum(case when Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Category in('1','4','5','6','7') 
then 1 else 0 end ) as apgen,
sum(case when Oasis_Admission_LearnerStatus = 'Pending' and Oasis_Admission_Category='2' then 1 else 0 end) as psc,
sum(case when Oasis_Admission_LearnerStatus = 'Pending' and Oasis_Admission_Category='3' then 1 else 0 end) as pst,
sum(case when Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Category='2' then 1 else 0 end) as apsc,
sum(case when Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Category='3' then 1 else 0 end) as apst,
                                            wim.Wcd_Intake_Available_General, wim.Wcd_Intake_Consumed from tbl_user_master as a
                                            inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.User_LoginId
                                            inner join tbl_district_master as c on  b.Oasis_Admission_District = c.District_Code 
                                            LEFT JOIN tbl_wcd_intake_master wim ON wim.Wcd_Intake_Center = 
                                            b.Oasis_Admission_Final_Preference AND wim.Wcd_Intake_Batch = b.Oasis_Admission_Batch
                                            WHERE b.Oasis_Admission_Course='" . $_course . "' AND b.Oasis_Admission_Batch='" . $_batch . "' 
                                            AND b.Oasis_Admission_Eligibility='eligible' AND
                                            b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "'
                                            AND Oasis_Admission_SpecialCat_Flag='" . $_category . "'
                                             group by b.Oasis_Admission_Final_Preference";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(), $_CourseCode);

            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "' and Batch_Code>293";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerList($_mode, $_batch, $_districtcode, $_Itgk, $_spcategory, $category = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_mode = mysqli_real_escape_string($_ObjConnection->Connect(), $_mode);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(), $_batch);
            $_districtcode = mysqli_real_escape_string($_ObjConnection->Connect(), $_districtcode);

            if ($_mode == 'one') {
                $filter = ($category) ? " AND b.Oasis_Admission_Category = '" . $category . "' AND 
				b.Oasis_Admission_LearnerStatus in ('Pending')" : "";
                $_SelectQuery3 = "SELECT b.*, c.Wcd_Intake_Available, c.Wcd_Intake_Consumed, c.Wcd_Intake_Available_General,
                                            c.Wcd_Intake_Available_SC, c.Wcd_Intake_Available_ST, d.Category_Name,e.Qualification_Name
                                            FROM tbl_oasis_admission as b inner join tbl_wcd_intake_master as c on
                                            b.Oasis_Admission_Final_Preference = c.Wcd_Intake_Center
                                            INNER JOIN tbl_category_master as d on b.Oasis_Admission_Category = d.Category_Code
                                            LEFT JOIN tbl_qualification_master as e on
                                            b.Oasis_Admission_Qualification = e.Qualification_Code
                                            WHERE b.Oasis_Admission_Batch = '" . $_batch . "' AND
                                            b.Oasis_Admission_District='" . $_districtcode . "'
                                            AND b.Oasis_Admission_Final_Preference='" . $_Itgk . "' AND 
                                            b.Oasis_Admission_Eligibility='eligible' 
                                            AND b.Oasis_Admission_LearnerStatus in ('Pending','Approved') 
                                            AND Oasis_Admission_SpecialCat_Flag='" . $_spcategory . "' and
                                            c.Wcd_Intake_Batch='" . $_batch . "' $filter Order by Oasis_Admission_Final_Priority";
            } else if ($_mode == 'two') {
                $filter = ($category) ? " AND b.Oasis_Admission_Category = '" . $category . "'" : "";
                $_SelectQuery3 = "SELECT b.*,c.Wcd_Intake_Available, c.Wcd_Intake_Available_General, c.Wcd_Intake_Available_SC, c.Wcd_Intake_Available_ST, d.Category_Name,e.Qualification_Name FROM tbl_oasis_admission as b
                                            inner join tbl_wcd_intake_master as c on b.Oasis_Admission_Final_Preference = c.Wcd_Intake_Center
                                            INNER JOIN tbl_category_master as d on b.Oasis_Admission_Category = d.Category_Code
                                            LEFT JOIN tbl_qualification_master as e on b.Oasis_Admission_Qualification = e.Qualification_Code
                                            WHERE b.Oasis_Admission_Batch = '" . $_batch . "' AND b.Oasis_Admission_District='" . $_districtcode . "'
                                            AND b.Oasis_Admission_Final_Preference='" . $_Itgk . "' AND b.Oasis_Admission_Eligibility='eligible' 
                                            AND b.Oasis_Admission_LearnerStatus in ('Approved') 
                                            AND Oasis_Admission_SpecialCat_Flag='" . $_spcategory . "' and
                                            c.Wcd_Intake_Batch='" . $_batch . "' $filter Order by Oasis_Admission_Final_Priority";
            }
            else if ($_mode == 'three') {
                $_Consume_Query = "select count(Oasis_Admission_Code) as tc from tbl_oasis_admission where 
                    Oasis_Admission_LearnerStatus='Approved' and Oasis_Admission_Batch =  '" . $_batch . "'"
                        . "  AND Oasis_Admission_District='" . $_districtcode . "' AND Oasis_Admission_Final_Preference='" . $_Itgk . "' 
                            AND Oasis_Admission_Eligibility='eligible' AND Oasis_Admission_SpecialCat_Flag='0' ";
                $_ResponseConsume_Query = $_ObjConnection->ExecuteQuery($_Consume_Query, Message::SelectStatement);
                $_Row = mysqli_fetch_array($_ResponseConsume_Query[2]);
                $_TotalConsumed = $_Row['tc'];
                
                $_SelectQuery3 = "SELECT b.*, c.Wcd_Intake_Available, c.Wcd_Intake_Consumed, c.Wcd_Intake_Available_General,
                                            c.Wcd_Intake_Available_SC, c.Wcd_Intake_Available_ST, d.Category_Name,e.Qualification_Name,
                                            '".$_TotalConsumed."' as TotalConsumed 
                                            FROM tbl_oasis_admission as b inner join tbl_wcd_intake_master as c on
                                            b.Oasis_Admission_Final_Preference = c.Wcd_Intake_Center
                                            INNER JOIN tbl_category_master as d on b.Oasis_Admission_Category = d.Category_Code
                                            LEFT JOIN tbl_qualification_master as e on
                                            b.Oasis_Admission_Qualification = e.Qualification_Code
                                            WHERE b.Oasis_Admission_Batch = '" . $_batch . "' AND
                                            b.Oasis_Admission_District='" . $_districtcode . "'
                                            AND b.Oasis_Admission_Final_Preference='" . $_Itgk . "' AND 
                                            b.Oasis_Admission_Eligibility='eligible' 
                                            AND b.Oasis_Admission_LearnerStatus in ('Pending') 
                                            AND Oasis_Admission_SpecialCat_Flag='" . $_spcategory . "' and
                                            c.Wcd_Intake_Batch='" . $_batch . "' Order by Oasis_Admission_Final_Priority";
            }
//          echo   $_SelectQuery3 ;
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_Batch, $_Lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(), $_Batch);
            $_Lcode = mysqli_real_escape_string($_ObjConnection->Connect(), $_Lcode);

            $_SelectQuery = "Select a.*,b.Category_Name,c.Qualification_Name From tbl_oasis_admission as a INNER JOIN tbl_category_master as b 
                                        on a.Oasis_Admission_Category = b.Category_Code LEFT JOIN tbl_qualification_master as c
                                        on a.Oasis_Admission_Qualification = c.Qualification_Code Where Oasis_Admission_Batch='" . $_Batch . "' 
                                        AND Oasis_Admission_LearnerCode='" . $_Lcode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateWcdRejected($_LCode, $_ProcessStatus, $_Remark) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LCode = mysqli_real_escape_string($_ObjConnection->Connect(), $_LCode);

            $_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_LearnerStatus='" . $_ProcessStatus . "', Oasis_Admission_Reason='" . $_Remark . "'"
                    . " Where Oasis_Admission_LearnerCode='" . $_LCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateWcdReservedAppoved($formData) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $codes = (isset($formData['shortlisted'])) ? implode(',', $formData['shortlisted']) : 0;
        $_ITGK = $formData['itgkcode'];
        $_Batch = $formData['batch'];
        $category = ($formData['category'] == '2') ? 'Wcd_Intake_Available_SC' : 'Wcd_Intake_Available_ST';
        if (!$codes)
            return ['No record found to approve.'];
        try {
            $_SelectQuery2 = "SELECT Oasis_Admission_LearnerStatus from tbl_oasis_admission where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Batch='" . $_Batch . "' AND Oasis_Admission_Code IN ($codes)";
            $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            $_count = mysqli_num_rows($_Response2[2]);
            //$_Row1=mysqli_fetch_array($_Response2[2]);
            if ($_count) {
                echo "Already Approved";
                return;
            } else {
                $_UpdateQuery = "UPDATE tbl_oasis_admission set Oasis_Admission_LearnerStatus='Approved' Where
                                            Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $_Batch . "' AND
                                            Oasis_Admission_LearnerStatus LIKE('Pending') AND Oasis_Admission_Eligibility='eligible' AND
                                            Oasis_Admission_Code IN ($codes) ";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

                $_UpdateIntakeQuery = "UPDATE tbl_wcd_intake_master SET $category = '-1' WHERE
					Wcd_Intake_Center = '" . $_ITGK . "' AND Wcd_Intake_Batch = '" . $_Batch . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateIntakeQuery, Message::UpdateStatement);
            }
//print_r($_Response);		   
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateWcdAppoved($_Batch, $_ITGK, $_SpCategory) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(), $_Batch);
            $_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(), $_ITGK);

            $_SelectQuery2 = "SELECT Oasis_Admission_LearnerStatus from tbl_oasis_admission where 
                                        Oasis_Admission_Final_Preference='" . $_ITGK . "'
                                        AND Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Batch='" . $_Batch . "'
                                        and Oasis_Admission_SpecialCat_Flag='" . $_SpCategory . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            $_count = mysqli_num_rows($_Response2[2]);
            //$_Row1=mysqli_fetch_array($_Response2[2]);
            if ($_count) {
                echo "Already Approved";
                return;
            } else {
                if ($_SpCategory == '1') {
                    $_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_LearnerStatus='Approved'
                                                Where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND
                                                Oasis_Admission_Batch='" . $_Batch . "' AND Oasis_Admission_LearnerStatus = 'Pending'
                                                AND Oasis_Admission_Eligibility='eligible' AND Oasis_Admission_SpecialCat_Flag='1'";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                } else {
                    $_SelectQuery3 = "SELECT Wcd_Intake_Available_General, Wcd_Intake_Available, Wcd_Intake_Available_SC, Wcd_Intake_Available_ST from tbl_wcd_intake_master where Wcd_Intake_Center ='" . $_ITGK . "' and Wcd_Intake_Batch='" . $_Batch . "'";
                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                    $_Row = mysqli_fetch_array($_Response3[2]);
                    $wcdIntakeAvailable = $_Row['Wcd_Intake_Available'];
                    if (empty($_Row['Wcd_Intake_Available_SC']) || empty($_Row['Wcd_Intake_Available_ST'])) {
                        $per18 = ($wcdIntakeAvailable * 0.18);
                        $per14 = ($wcdIntakeAvailable * 0.14);
                        $remainingSC = round($per18, 0);
                        $remainingST = round($per14, 0);
                    } else {
                        $remainingSC = $_Row['Wcd_Intake_Available_SC'];
                        $remainingST = $_Row['Wcd_Intake_Available_ST'];
                    }

                    $per70 = round(($wcdIntakeAvailable * .70), 0);
                    $remain = ($wcdIntakeAvailable - ($remainingSC + $remainingST));
                    $intake = (!empty($_Row['Wcd_Intake_Available_General'])) ? $_Row['Wcd_Intake_Available_General'] :
                            (($remain < $per70) ? $remain : $per70);

                    $_SelectQuery4 = "SELECT * FROM tbl_oasis_admission WHERE Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $_Batch . "' AND Oasis_Admission_LearnerStatus = 'Pending' AND Oasis_Admission_Eligibility='eligible' AND Oasis_Admission_SpecialCat_Flag='0' order by Oasis_Admission_Final_Priority LIMIT " . $intake;
                    $_Response4 = $_ObjConnection->ExecuteQuery($_SelectQuery4, Message::SelectStatement);
                    $effected = mysqli_num_rows($_Response4[2]);


                    $_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_LearnerStatus='Approved'"
                            . " Where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $_Batch . "'"
                            . " AND Oasis_Admission_LearnerStatus = 'Pending' AND Oasis_Admission_Eligibility='eligible'"
                            . "	AND Oasis_Admission_SpecialCat_Flag='0' order by Oasis_Admission_Final_Priority LIMIT " . $intake . " ";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

                    $_UpdateIntakeQuery = "UPDATE tbl_wcd_intake_master SET Wcd_Intake_Available_General = '" . $intake . "', Wcd_Intake_Consumed = '" . $effected . "', Wcd_Intake_Available_SC = '" . $remainingSC . "', Wcd_Intake_Available_ST = '" . $remainingST . "' WHERE Wcd_Intake_Center = '" . $_ITGK . "' AND Wcd_Intake_Batch = '" . $_Batch . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateIntakeQuery, Message::UpdateStatement);
                }
            }
//print_r($_Response);		   
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function UpdateWcdAppovedWL($_Batch, $_ITGK, $_SpCategory) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(), $_Batch);
            $_ITGK = mysqli_real_escape_string($_ObjConnection->Connect(), $_ITGK);

                    $_SelectQuery3 = "select sum(case when Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Category in('1','4','5','6','7') 
then 1 else 0 end ) as apgen,
sum(case when Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Category='2' then 1 else 0 end) as apsc,
sum(case when Oasis_Admission_LearnerStatus = 'Approved' and Oasis_Admission_Category='3' then 1 else 0 end) as apst
from tbl_oasis_admission where  Oasis_Admission_Final_Preference ='" . $_ITGK . "' and Oasis_Admission_Batch='" . $_Batch . "' and Oasis_Admission_SpecialCat_Flag='0'";
                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                    $_Row = mysqli_fetch_array($_Response3[2]);
                    $intake =  9 - ($_Row['apgen']  + $_Row['apsc'] + $_Row['apst']) ;


                    $_SelectQuery4 = "SELECT * FROM tbl_oasis_admission WHERE Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $_Batch . "' AND Oasis_Admission_LearnerStatus = 'Pending' AND Oasis_Admission_Eligibility='eligible' AND Oasis_Admission_SpecialCat_Flag='0' order by Oasis_Admission_Final_Priority LIMIT " . $intake;
                    $_Response4 = $_ObjConnection->ExecuteQuery($_SelectQuery4, Message::SelectStatement);
                    $effected = mysqli_num_rows($_Response4[2]);


                    $_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_LearnerStatus='Approved'"
                            . " Where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $_Batch . "'"
                            . " AND Oasis_Admission_LearnerStatus = 'Pending' AND Oasis_Admission_Eligibility='eligible'"
                            . "	AND Oasis_Admission_SpecialCat_Flag='0' order by Oasis_Admission_Final_Priority LIMIT " . $intake . " ";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

//                    $_UpdateIntakeQuery = "UPDATE tbl_wcd_intake_master SET Wcd_Intake_Available_General = '" . $intake . "', Wcd_Intake_Consumed = '" . $effected . "', Wcd_Intake_Available_SC = '" . $remainingSC . "', Wcd_Intake_Available_ST = '" . $remainingST . "' WHERE Wcd_Intake_Center = '" . $_ITGK . "' AND Wcd_Intake_Batch = '" . $_Batch . "'";
//                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateIntakeQuery, Message::UpdateStatement);
                
            
//print_r($_Response);		   
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function processWCDMerit($batchId, $action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $maxInTake = 10;
        $ageOn = '2018-01-01';

        //$meritQry = "CALL generateWCDMerit($batchId, $maxInTake, '" . $ageOn . "', '" . $action . "')";
        $_ObjConnection->ExecuteQuery($meritQry, Message::MultipleStatement);

        return '1';
    }

    public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
