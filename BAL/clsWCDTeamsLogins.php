
<?php

/**
 * Description of clsWCDTeamsLogins
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWCDTeamsLogins {

    //put your code here
    public function GetAllLearner($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(), $batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(), $course);

        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '28') {

               $_SelectQuery = "Select * from tbl_wcd_teams_logins_learner where Course_Code='".$course."' 
                and Batch_Code='".$batch."'";
                
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "Select * from tbl_wcd_teams_logins_learner where Course_Code='".$course."' 
                and Batch_Code='".$batch."' and ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            }
            elseif ($_LoginRole == '14') {
                $_SelectQuery = "Select * from tbl_wcd_teams_logins_learner where Course_Code='".$course."' 
                and Batch_Code='".$batch."' and RSP_Code = '" . $_SESSION['User_LoginId'] . "'";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAllITGK($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(), $batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(), $course);

        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '28') {

               $_SelectQuery = "Select * from tbl_wcd_teams_login_itgk where Course_Code='".$course."' 
                and Batch_Code='".$batch."'";
                
            } elseif ($_LoginRole == '7') {
                $_SelectQuery = "Select * from tbl_wcd_teams_login_itgk where Course_Code='".$course."' 
                and Batch_Code='".$batch."' and ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            }
            elseif ($_LoginRole == '14') {
                $_SelectQuery = "Select * from tbl_wcd_teams_login_itgk where Course_Code='".$course."' 
                and Batch_Code='".$batch."' and SP_Code = '" . $_SESSION['User_LoginId'] . "'";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
 public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('3','24') ORDER BY Course_Code ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLEventBatch($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $_Course . "' AND Now() >= Event_Startdate AND
			Now() <= Event_Enddate AND Event_Name='13'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}