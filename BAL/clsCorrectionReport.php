<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionReport {
    //put your code here
    public function GetAll($_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				
				if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258' || $_SESSION['User_UserRoll'] == 8){
					$_SelectQuery = "Select a.*, b.lotname FROM `tbl_correction_copy` as a INNER JOIN tbl_clot as b on a.lot = b.lotid where dispatchstatus='" . $_Status . "'";
				}
				else{
					$_SelectQuery = "Select a.*, b.lotname FROM `tbl_correction_copy` as a INNER JOIN tbl_clot as b on a.lot = b.lotid where Correction_ITGK_Code = '".$_SESSION['User_LoginId']."' AND dispatchstatus='" . $_Status . "'";
				}				
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function FILLCorrectionLot() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetCorrectionApprovalStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
 
 
}
