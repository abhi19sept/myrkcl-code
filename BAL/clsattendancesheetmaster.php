<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response1 = array();

class clsattendancesheetmaster {
	
	
	public function GetAll($examid, $center2) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

         $_SelectQuery = "SELECT * FROM tbl_finalexammapping  WHERE examcentercode = '" . $center2 . "' AND examid = '" . $examid . "' ORDER BY examcentercode, rollno";
         $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			
        }
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

    public function GetCenters($requestArgs) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (!empty($requestArgs['job'])) {
                $job = $requestArgs['job'];
                $start = ($job - 1) * 100;
                $end = 100;
                $limit = " LIMIT $start, $end";
            }
            if (!empty($requestArgs['centercode']) && $requestArgs['centercode'] != 'All') {
                $centercodes = explode(',', $requestArgs['centercode']);
                $filter .= " AND fm.examcentercode IN ('" . implode("','", $centercodes) . "')";
            }
            if (!empty($requestArgs['district'])) {
                $filter .= " AND dm.District_Code = '" . $requestArgs['district'] . "'";
            }
            //$filter = " AND fm.centercode IN ('11290273', '12290085', '12290085', '12290125', '13290100', '14290008', '15290114', '16290037', '21290606', '21290669', '23290022', '23290082', '24290089', '25290035', '25290035', '25290035', '25290035', '25290035', '25290035', '25290035', '27290098', '31290031', '31290097', '33290061', '33290071', '36290115', '36290216', '37290013', '45290186', '45290254', '45290327', '45290383', '46290049', '51290087', '51290087', '51290087', '51290087', '51290087', '51290087', '57290009', '71290080', '71290080', '71290080', '71290080')";
            $_SelectQuery = "SELECT DISTINCT fm.examcentercode, fm.examcenterdistrict, REPLACE(fm.eventname, 'RS-CIT', '') AS eventname, examdate, examtime FROM tbl_finalexammapping fm INNER JOIN tbl_district_master dm ON fm.examcenterdistrict = dm.District_Name WHERE fm.examid='" . $requestArgs["code"] . "' " . $filter . " ORDER BY fm.examcentercode ASC " . $limit;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
        
        return $_Response;
    }
    
}
