<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBatchWiseLearnerAdmission.php
 *
 * @author Yogendra soni
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsBatchWiseLearnerAdmission 
{
    //put your code here
  
   public function GetAll($course,$batch) 
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$_SelectQuery = "select a.Admission_LearnerCode as learnercode, a.Admission_Name as name,
				a.Admission_Fname as fathername, a.Admission_DOB as dob, a.Admission_Mobile as mobile,
				a.Admission_ITGK_Code as itgkcode, ITGK_Name, District_Name, Tehsil_Name,
				g.Course_Name as course, h.Batch_Name as batch
				from tbl_admission as a inner join vw_itgkname_distict_rsp as b on a.Admission_ITGK_Code =b.ITGKCODE 
				inner join tbl_course_master as g on  a.Admission_Course=g.Course_Code
				inner join tbl_batch_master as h on  a.Admission_Batch=h.Batch_Code
				where a.Admission_Course='".$course."' and a.Admission_Batch='".$batch."' and Admission_Payment_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
}
