<?php

/**
 * Description of clsManageEvent
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsManageEvent {

    public function FILLCategory() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_event_category";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLEvent($_EventCat) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventCat = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventCat);
				
            $_SelectQuery = "Select * From tbl_event_type WHERE Event_Type_Category = '" . $_EventCat . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLEventDDmode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_event_type WHERE Event_Type_ID IN (3,5,7,8)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLEventIDddmode($course, $batch, $event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
				
            $_SelectQuery = "Select Event_ID,Event_Name From tbl_event_management WHERE Event_Course='" . $course . "' AND 
							Event_Batch='" . $batch . "' AND Event_Name='" . $event . "' AND Event_Payment='2'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Addlistddmodeitgk($_EventID, $_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventID = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventID);
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            $_Insert = "Insert Into tbl_ddmode_center (Ddmode_Center_EventId,Ddmode_Center_ITGK) Values
						('" . $_EventID . "', '" . $_CenterCode . "')";
            $_Response = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLPaymentMode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_payment_mode";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

   
    public function Add($_EventCat, $_EventName, $_PayMode, $_Course, $_CourseEOI, $_BatchName, $_ReexamEvent, $_CorrectionEvent, $_Event_LearnerAttendance, $_Event_Coreectionbfrexamevent, $_Event_Rscfacertificatepayment, $_Sdate, $_Edate) {
        global $_ObjConnection;
        $_Response1 = array();
        $_ObjConnection->Connect();
        try {
				$_EventCat = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventCat);
				$_EventName = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventName);
				$_PayMode = mysqli_real_escape_string($_ObjConnection->Connect(),$_PayMode);
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_CourseEOI = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseEOI);
				$_BatchName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchName);
				$_ReexamEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_ReexamEvent);
				$_CorrectionEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_CorrectionEvent);
				$_Event_LearnerAttendance = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event_LearnerAttendance);
				$_Event_Coreectionbfrexamevent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event_Coreectionbfrexamevent);
				$_Event_Rscfacertificatepayment = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event_Rscfacertificatepayment);
				$_Sdate = mysqli_real_escape_string($_ObjConnection->Connect(),$_Sdate);
				$_Edate = mysqli_real_escape_string($_ObjConnection->Connect(),$_Edate);
				
            $_Insert = "Insert Into tbl_event_management (Event_Category, Event_Name,Event_Payment, Event_Course, Event_CourseEOI, Event_Batch, Event_ReexamEvent, Event_CorrectionEvent,Event_LearnerAttendance,Event_AadharUpdate,Event_CertificatePayment, Event_Startdate, Event_Enddate,Event_User) VALUES (" . $_EventCat . "," . $_EventName . "," . $_PayMode . "," . $_Course . "," . $_CourseEOI . "," . $_BatchName . ", '" . $_ReexamEvent . "', '" . $_CorrectionEvent . "','" . $_Event_LearnerAttendance . "','" . $_Event_Coreectionbfrexamevent . "','".$_Event_Rscfacertificatepayment."','" . $_Sdate . "','" . $_Edate . "'," . $_SESSION['User_Code'] . ")";

            $_DuplicateQuery = "Select * From tbl_event_management Where Event_Category='" . $_EventCat . "' AND Event_Name='" . $_EventName . "' AND Event_Payment='" . $_PayMode . "' AND Event_Course='" . $_Course . "' AND Event_CourseEOI = '" . $_CourseEOI . "' AND Event_Batch='" . $_BatchName . "' AND Event_ReexamEvent='" . $_ReexamEvent . "' AND Event_CorrectionEvent='" . $_CorrectionEvent . "' AND Event_LearnerAttendance='" . $_Event_LearnerAttendance . "' AND Event_AadharUpdate='" . $_Event_Coreectionbfrexamevent . "' AND Event_CertificatePayment='".$_Event_Rscfacertificatepayment."'";

            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {

                $_Response = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll($_EventCat, $_EventName, $_PayMode, $_Course, $_CourseEoi, $_BatchName, $_ReexamEvent, $_CorrectionEvent, $_NameAddressEvent, $_NcrPaymentEvent, $_OwnerChangeEvent, $_RenewalPenaltyEvent, $_NameAadharUpdEvent, $_RscfaCertificateEvent) {
        //echo $_EventCat;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventCat = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventCat);
				$_EventName = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventName);
				$_PayMode = mysqli_real_escape_string($_ObjConnection->Connect(),$_PayMode);
				$_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
				$_CourseEoi = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseEoi);
				$_BatchName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BatchName);
				$_ReexamEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_ReexamEvent);
				$_CorrectionEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_CorrectionEvent);
				$_NameAddressEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_NameAddressEvent);
				$_NcrPaymentEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_NcrPaymentEvent);
				$_OwnerChangeEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_OwnerChangeEvent);
				$_RenewalPenaltyEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_RenewalPenaltyEvent);
				$_NameAadharUpdEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_NameAadharUpdEvent);
				$_RscfaCertificateEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_RscfaCertificateEvent);
				
             $_SelectQuery = "Select Event_ID, Event_Category, Event_Name, Event_Startdate, Event_Enddate FROM tbl_event_management"
            . " WHERE  Event_Category = '" . $_EventCat . "' AND  Event_Name = '" . $_EventName . "'"
            . " AND  Event_Payment = '" . $_PayMode . "' AND  Event_Course = '" . $_Course . "'"
            . " AND  Event_Batch = '" . $_BatchName . "' AND  Event_ReexamEvent = '" . $_ReexamEvent . "' " 
            . " AND  Event_CorrectionEvent = '" . $_CorrectionEvent . "'  AND (Event_CourseEOI is NULL OR Event_CourseEOI= '" . $_CourseEoi . "') "
            . " AND  Event_NameAddressPayment = '" . $_NameAddressEvent . "' and Event_NCRPayment = '" . $_NcrPaymentEvent . "'"
            . "  AND  Event_OwnershipChangePayment = '" . $_OwnerChangeEvent . "' and Event_RenewalPenalty = '" . $_RenewalPenaltyEvent . "'"
            . " AND  Event_AadharUpdate = '" . $_NameAadharUpdEvent . "' AND Event_CertificatePayment='".$_RscfaCertificateEvent."'";
            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
            //WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetEvents($_EventCat, $_EventName) {
        //echo $_EventCat;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventCat = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventCat);
				$_EventName = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventName);
				
            $_SelectQuery = "Select a.*, b.Event_Category_Name AS EventCat, c.Event_Type_Name AS EventName FROM tbl_event_management AS a INNER JOIN tbl_event_category AS b ON a.Event_Category = b.Event_Category_ID INNER JOIN tbl_event_type AS c ON a.Event_Name = c.Event_Type_ID"
                    . " WHERE  Event_Category = '" . $_EventCat . "' AND  Event_Name = '" . $_EventName . "'";

            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
            //WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_txtEventID, $_txtEndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_txtEventID = mysqli_real_escape_string($_ObjConnection->Connect(),$_txtEventID);
				$_txtEndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_txtEndDate);
				
            $_UpdateQuery = "Update tbl_event_management set "
                    . " Event_Enddate='" . $_txtEndDate . "'"
                    . " Where Event_ID='" . $_txtEventID . "'";


            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllEvents() {
        //echo $_EventCat;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select b.Event_Category_Name AS EventCat, c.Event_Type_Name AS EventName,d.Course_Name,e.Batch_Name,
                                        f.Event_Name as ExamEvent,g.EOI_Name, a.Event_Startdate,a.Event_Enddate,a.Timestamp,a.Event_LearnerAttendance 
                                        FROM tbl_event_management AS a 
                                        INNER JOIN tbl_event_category AS b ON a.Event_Category = b.Event_Category_ID INNER JOIN tbl_event_type AS c
                                         ON a.Event_Name = c.Event_Type_ID left join tbl_course_master as d on a.Event_Course=d.Course_Code 
                                        left join tbl_batch_master as e on a.Event_Batch = e.Batch_Code left join tbl_events as f on 
                                        a.Event_ReexamEvent= f.Event_Id left join tbl_eoi_master as g on a.Event_CourseEOI=g.EOI_Code 
                                        order by a.Timestamp desc";

            //$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
            //WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}

?>