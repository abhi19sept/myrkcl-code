<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFeeTypeMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsFeeTypeMaster {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select FeeType_Code,FeeType_Name,"
                    . "Status_Name From tbl_feetype_master as a inner join tbl_status_master as b "
                    . "on a.FeeType_Status"
                    . "=b.Status_Code";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_FeeType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_FeeType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_FeeType_Code);
            $_SelectQuery = "Select FeeType_Code,FeeType_Name,"
                    . "FeeType_Status From tbl_feetype_master Where FeeType_Code='" . $_FeeType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_FeeType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_FeeType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_FeeType_Code);
            $_DeleteQuery = "Delete From tbl_feetype_master Where FeeType_Code='" . $_FeeType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_FeeTypeName,$_FeeTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_FeeTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_FeeTypeName);
            $_InsertQuery = "Insert Into tbl_feetype_master(FeeType_Code,FeeType_Name,"
                    . "FeeType_Status) "
                    . "Select Case When Max(FeeType_Code) Is Null Then 1 Else Max(FeeType_Code)+1 End as FeeType_Code,"
                    . "'" . $_FeeTypeName . "' as FeeType_Name,'" . $_FeeTypeStatus . "' as FeeType_Status"
                    . " From tbl_feetype_master";
            $_DuplicateQuery = "Select * From tbl_feetype_master Where FeeType_Name='" . $_FeeTypeName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_FeeTypeCode,$_FeeTypeName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_FeeTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_FeeTypeName);
            $_FeeTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_FeeTypeCode);
            $_UpdateQuery = "Update tbl_feetype_master set FeeType_Name='" . $_FeeTypeName . "',"
                    . "FeeType_Status='" . $_FunctionStatus . "' Where FeeType_Code='" . $_FeeTypeCode . "'";
            $_DuplicateQuery = "Select * From tbl_feetype_master Where FeeType_Name='" . $_FeeTypeName . "' "
                    . "and FeeType_Code <> '" . $_FeeTypeCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
