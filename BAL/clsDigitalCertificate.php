<?php

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsDigitalCertificate {

    public function getSignAuthoriser($designation, $department) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$designation = mysqli_real_escape_string($_ObjConnection->Connect(),$designation);
				$department = mysqli_real_escape_string($_ObjConnection->Connect(),$department);
				
            $_SelectQuery = "Select Sign_Id,Sign_Sno,Sign_Name From tbl_esign_master where Sign_Designation='".$designation."'
							and Sign_Department='".$department."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCourseCertificate() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','4','3','20')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllBatchCertificate($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Code,Batch_Name From tbl_batch_master where Course_Code ='" . $_CourseCode . "'  and 
							 Batch_Code>111 order by Batch_Code DESC ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerListITGK($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					
                if ($_SESSION['User_UserRoll'] == '1') {
                    $_SelectQuery3 = "SELECT a.Admission_Aadhar_Status,a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_DOB,a.Admission_UID,
                                        a.Admission_Mobile FROM tbl_admission as a inner join tbl_final_result as b on a.Admission_LearnerCode=b.scol_no WHERE
                                        Admission_Course = '" . $_course . "' AND Admission_Batch = '" . $_batch . "' and result LIKE('%pass%')";
                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                    return $_Response3;
                } else if ($_SESSION['User_UserRoll'] == '7') {
                    $_SelectQuery3 = "SELECT a.Admission_Aadhar_Status,a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_DOB,a.Admission_UID,
                                        a.Admission_Mobile FROM tbl_admission as a inner join tbl_final_result as b on a.Admission_LearnerCode=b.scol_no WHERE
                                        Admission_ITGK_Code ='" . $_SESSION['User_LoginId'] . "' AND
                                        Admission_Course = '" . $_course . "' AND Admission_Batch = '" . $_batch . "' and result LIKE('%pass%')";
                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                    return $_Response3;
                } else if ($_SESSION['User_UserRoll'] == '19') {
                    $_SelectQuery3 = "SELECT a.Admission_Aadhar_Status,a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_DOB,a.Admission_UID,
                                    a.Admission_Mobile FROM tbl_admission as a inner join tbl_final_result as b on a.Admission_LearnerCode=b.scol_no WHERE
                                    Admission_LearnerCode ='" . $_SESSION['User_LearnerCode'] . "' AND
                                    Admission_Course = '" . $_course . "' AND Admission_Batch = '" . $_batch . "' and result LIKE('%pass%')";
                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                    return $_Response3;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function getLearnerResults($Lcode, $signedNameReg) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                //$filter = " AND tr.scol_no IN ('127170810110841789')";
                $curdate = date("d-m-Y h:i:s");
				$Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$Lcode);
				$signedNameReg = mysqli_real_escape_string($_ObjConnection->Connect(),$signedNameReg);
				
                $_SelectQuery = "SELECT tr.scol_no AS learnercode, tr.result_date, tr.name, tr.study_cen AS centercode, tr.percentage,
			 tr.fname, tr.exameventnameID, REPLACE(REPLACE(em.Event_Name, 'RS-CIT ', ''), ' ', '-') AS eventname, tr.vmou_ref_num,
			 tr.exam_date, '" . $curdate . "' AS istdt, '" . $signedNameReg . "' as registrar_name, '' as reg_no FROM tbl_final_result tr INNER JOIN tbl_events em ON 
			 tr.exameventnameID = em.Event_Id INNER JOIN tbl_user_master um ON um.User_LoginId = tr.study_cen 
			 INNER JOIN tbl_organization_detail om ON om.Organization_User = um.User_Code 
			 WHERE scol_no = '" . $Lcode . "' AND result LIKE('%pass%')";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getData($learnercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $result = [];
		$learnercode = mysqli_real_escape_string($_ObjConnection->Connect(),$learnercode);
		
        $_SelectQuery = "SELECT bm.Batch_Name AS batchname, cm.Course_Name AS coursename FROM tbl_admission ta INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course INNER JOIN tbl_batch_master bm ON ta.Admission_Batch = bm.Batch_Code WHERE ta.Admission_LearnerCode = '" . $learnercode . "'";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2]) > 0) {
            $result = mysqli_fetch_assoc($_Response[2]);
        }

        return $result;
    }

    public function ShowLearnerEcertificate($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            $_SelectQuery = "Select * From tbl_final_result where result LIKE('%pass%') and scol_no='" . $lcode . "' and status='0'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ShowLearnerEcertificateForUpdate($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            $_SelectQuery = "Select * From tbl_final_result where result LIKE('%pass%') and scol_no='" . $lcode . "' and status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateEcertificateStatus($lcode, $eventid, $itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
                $_UpdateQuery = "update tbl_final_result set status='1' where scol_no ='" . $lcode . "' and exameventnameID='" . $eventid . "' and
							 study_cen='" . $itgk . "'";
                $_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

                $_UpdateQuery1 = "Update tbl_admission set Admission_Aadhar_Status='2'
							Where Admission_ITGK_Code='" . $itgk . "' AND Admission_LearnerCode='" . $lcode . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateEcertificateStatusForUpdateCertificate($lcode,$eventid,$itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
            $_UpdateQuery = "update tbl_final_result set status='2' where scol_no ='".$lcode."' and exameventnameID='".$eventid."' 
							 and study_cen='".$itgk."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			$_UpdateQuery1 = "Update tbl_admission set Admission_Aadhar_Status='4'
							Where Admission_ITGK_Code='" . $itgk . "' AND Admission_LearnerCode='". $lcode ."'";
            $_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }	
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
}
