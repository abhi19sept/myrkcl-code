<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPrintRecpNCR
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response2 = array();

class clsPrintRecpNCR {

    //put your code here

    public function GetDataAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "SELECT b.User_Ack, a.Organization_Name,b.User_LoginId,a.Organization_AreaType from tbl_organization_detail "
                    . " as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_ncr_invoice as c "
                    . " on b.User_Ack = c.Ao_Ack_No where "
                    . "b.User_UserRoll='15' and b.User_Code='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);



            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetGSTIN($id) {
        
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
				
            $_SelectQuery = "SELECT Bank_Gstin_No from tbl_bank_account where Bank_User_Code='" . $id . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllPrintRecpDwnld($eid) { /// for new invoice system GST
        // echo $eid;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$eid = mysqli_real_escape_string($_ObjConnection->Connect(),$eid);
            if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                if ($eid == "Rural") {
                    $Organization_AreaType = "NCR_Rural";
                } elseif ($eid == "Urban") {
                    $Organization_AreaType = "NCR_Urban";
                }
                  
 $_SelectQuery = "SELECT a.Organization_Ack, a.Organization_Name,d.District_Name,b.User_LoginId,c.invoice_no,c.addtime, "
                        . "b.User_MobileNo, e.Gst_Invoce_SAC,e.Gst_Invoce_BaseFee,e.Gst_Invoce_CGST,e.Gst_Invoce_SGST,e.Gst_Invoce_TotalFee "
                        . " from tbl_organization_detail "
                        . " as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_ncr_invoice as c "
                        . " on b.User_Ack = c.Ao_Ack_No inner join tbl_district_master as d on a.Organization_District = d.District_Code "
                        . " inner join tbl_gst_invoice_master as e where 
                         CASE WHEN c.addtime > '2019-07-31' THEN e.Gst_Invoce_Category_Type='NCR_Rural_New'  
ELSE e.Gst_Invoce_Category_Type='" . $Organization_AreaType . "' END and "
                        . "b.User_UserRoll='15' and b.User_Code='" . $_SESSION['User_Code'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function addPhotoInPDF($newURL, $coursecode) {//echo $a;
        require_once('fpdi/fpdf/fpdf.php');
        require_once('fpdi/fpdi.php');
        require_once('fpdi/fpdf_tpl.php');
        $pdf = new FPDI();
        $pdf->AddPage();
        $path = $newURL;
        $pdf->setSourceFile($path);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        if ($coursecode == '5') {
            $Imagepath = '../upload/DownloadPrintRecpt/rscfa.jpg';
        } else {
            $Imagepath = '../upload/DownloadPrintRecpt/rscit.jpg';
        }

        $pdf->Image($Imagepath, 8, 106, 50, 25);
        $pdf->Image($Imagepath, 8, 253, 50, 25);

        $pdf->Output($path, "F");
    }

    public function convert_number_to_words($number) {

        $hyphen = ' ';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'Zero',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Fourty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100 => 'Hundred',
            1000 => 'Thousand',
            1000000 => 'Million',
            1000000000 => 'Billion',
            1000000000000 => 'Trillion',
            1000000000000000 => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

}
