<?php

/**
 * Description of clsSpKyc
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');
require 'DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();
$_ObjConnection = new _Connection();
$_Response = array();

class clsSpKyc {
    //put your code here
    public function CheckExistence($_Type) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_SP_Code = $_SESSION['User_LoginId'];
				$status="";
				if($_Type=='1'){
					$_chkDuplicate = "Select * From tbl_spkyc_director_details Where director_details_sp_code='" . $_SP_Code . "'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
						if ($_Response2[0] == Message::NoRecordFound){
							$status="yes";
						}else{
							$status="no";
						}
				}
				else if($_Type=='2'){
					$_chkDuplicate = "Select * From tbl_spkyc_director_interest Where director_interest_sp_code='" . $_SP_Code . "'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
						if ($_Response2[0] == Message::NoRecordFound){
							$status="yes";
						}else{
							$status="no";
						}
				}
				else if($_Type=='3'){
					$_chkDuplicate = "Select * From tbl_spkyc_owned_itgk Where owned_itgk_sp_code='" . $_SP_Code . "'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
						if ($_Response2[0] == Message::NoRecordFound){
							$status="yes";
						}else{
							$status="no";
						}
				}
				else if($_Type=='4'){
					$_chkDuplicate = "Select * From tbl_spkyc_authorized_person Where authorized_person_sp_code='" . $_SP_Code . "'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
						if ($_Response2[0] == Message::NoRecordFound){
							$status="yes";
						}else{
							$status="no";
						}
				}
				else if($_Type=='5'){
					$_chkDuplicate = "Select * From tbl_spkyc_office_details Where office_details_sp_code='" . $_SP_Code . "'";
                    $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
						if ($_Response2[0] == Message::NoRecordFound){
							$status="yes";
						}else{
							$status="no";
						}
				}
				else{
					$status="wrong";
				}            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $status;
    }
	
	public function GetCompType() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Org_Type_Code,Org_Type_Name,"
                    . "Status_Name From tbl_org_type_master as a inner join tbl_status_master as b "
                    . "on a.Org_Type_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	public function AddDirectorDetails($Name, $Email, $Mobile, $Designation, $Address, $Appointment, $PAN, $ShareHolding, $Type)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_SP_Code = $_SESSION['User_LoginId'];
					
								$_InsertQuery = "insert into 
									tbl_spkyc_director_details(director_details_type,director_details_name,director_details_email,
									director_details_mobile,director_details_address,director_details_designation,
									director_details_appointment_date,director_details_pan_no,
									director_details_shareholding,director_details_sp_code )values
									('".$Type."','".$Name."','".$Email."','".$Mobile."','".$Designation."','".$Address."',"
										. "'".$Appointment."','".$PAN."','".$ShareHolding."','".$_SP_Code."')";
								$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);					
												
				}else{
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}          
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
		public function AddDirectorInterest($Name, $Relation, $Interested, $Interested_Name, $Nature, $Remark)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_SP_Code = $_SESSION['User_LoginId'];
						$_InsertQuery = "insert into 
									tbl_spkyc_director_interest(director_interest_name,director_interest_relation,
									director_interest_with,director_interest_with_name,director_interest_nature,
									director_interest_remark,director_interest_sp_code )values
									('".$Name."','".$Relation."','".$Interested."','".$Interested_Name."','".$Nature."',"
										. "'".$Remark."','".$_SP_Code."')";
								$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);					
											
				}else{
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}          
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
	public function AddOwnedITGK($ITGK_Code, $ITGK_Name, $Remark)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_SP_Code = $_SESSION['User_LoginId'];
						$_InsertQuery = "insert into 
									tbl_spkyc_owned_itgk(owned_itgk_code,owned_itgk_name,
									owned_itgk_remark,owned_itgk_sp_code )values
									('".$ITGK_Code."','".$ITGK_Name."','".$Remark."','".$_SP_Code."')";
								$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);					
											
				}else{
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}          
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
	public function AddAuthorizePerson($Name, $Mobile, $Designation, $Appointment, $District, $StationAt)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$_SP_Code = $_SESSION['User_LoginId'];
						$_InsertQuery = "insert into 
							tbl_spkyc_authorized_person(authorized_person_name,authorized_person_mobile,authorized_person_designation,
							authorized_person_appointment_date,authorized_person_district,authorized_person_stationed,
							authorized_person_sp_code)values
									('".$Name."','".$Mobile."','".$Designation."','".$Appointment."','".$District."','".$StationAt."',
									'".$_SP_Code."')";
								$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);					
											
				}else{
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}          
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
	public function AddOfficeDetails($Name, $Email, $Mobile, $Designation, $Address, $ContactPerson, $img_name)
    {
        global $_ObjConnection;
		global $_ObjFTPConnection;
        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					$ftpaddress = $_ObjFTPConnection->ftpPathIp();
					
					$_SP_Code = $_SESSION['User_LoginId'];
					
					$frontofficedoc = $ftpaddress.'SP_Front_Office/' . '/' . $img_name;
					$interiordoc = $ftpaddress.'SP_Interior_office/' . '/' . $img_name;
					$deeddoc = $ftpaddress.'SP_Deed/' . '/' . $img_name;
					
				
				$ch = 	curl_init($frontofficedoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responsefrontDoc = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
				
				$ch = 	curl_init($interiordoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseinteriorDoc = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
						
				$ch = 	curl_init($deeddoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responsedeedDoc = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
				
					
						
						$_InsertQuery = "insert into 
							tbl_spkyc_office_details(office_details_spname,office_details_email,office_details_mobile,
							office_details_designation,office_details_address,office_details_contact_person,office_details_sp_code,
                                                        office_details_file1,office_details_file2,office_details_file3)
							values
							('".$Name."','".$Email."','".$Mobile."','".$Designation."','".$Address."','".$ContactPerson."',
									'".$_SP_Code."','".$img_name."','".$img_name."','".$img_name."')";
						if($responsefrontDoc == 200 && $responseinteriorDoc == 200 && $responsedeedDoc == 200){
							$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
						}
						else
						{
							echo "Please Attach Necessary Document.";
							return;
						}							
											
				}else{
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}          
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
	public function VerifyOTP($_otp,$_mobile)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_spkyc_otp_verify WHERE SP_Mobile = '" . $_mobile . "' AND SP_OTP = '" . $_otp . "' 
						AND SP_Status = '0' AND SP_Time >= NOW() - INTERVAL 10 MINUTE";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response[0]);
            if ($_Response1[0] == Message::NoRecordFound) {
                //echo "Invalid OTP. Please Try Again";
                $er='InvalidOTP';
                return $er;
            } else {
                $_UpdateQuery = "Update tbl_spkyc_otp_verify set SP_Status='1' WHERE SP_Mobile='" . $_mobile . "' AND
								SP_OTP = '" . $_otp . "' AND SP_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_spkyc_otp_verify WHERE SP_Mobile='" . $_mobile . "' AND SP_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
                return $_Response;
            }
           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
    public function GenerateOTP($mobile)
    {
        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = "Dear SP, Your OTP " . $_OTP ." for verfiying your mobile number is dated on ".date('Y-m-d h:i:s')." .OTP is valid for 10 mins.
		Please do not share with anyone.RKCL";

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "insert into tbl_spkyc_otp_verify(SP_Mobile,SP_OTP,SP_Device)"
                    . "values('".$mobile."','".$_OTP."','Web')";
            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement); 
            SendSMS($mobile, $_SMS);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
	public function VerifyEmailOTP($_otp,$_email)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_spkyc_otp_verify WHERE SP_Email = '" . $_email . "' AND SP_OTP = '" . $_otp . "' 
						AND SP_Status = '0' AND SP_Time >= NOW() - INTERVAL 10 MINUTE";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response[0]);
            if ($_Response1[0] == Message::NoRecordFound) {
                //echo "Invalid OTP. Please Try Again";
                $er='InvalidOTP';
                return $er;
            } else {
                $_UpdateQuery = "Update tbl_spkyc_otp_verify set SP_Status='1' WHERE SP_Email='" . $_email . "' AND
								SP_OTP = '" . $_otp . "' AND SP_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_spkyc_otp_verify WHERE SP_Email='" . $_email . "' AND SP_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
                return $_Response;
            }
           
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
	public function GenerateEmailOTP($email)
    {
        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = "Your OTP " . $_OTP ." for verfiying your email id is dated on ".date('Y-m-d h:i:s')." .OTP is valid for 10 mins.
		Please do not share with anyone.RKCL";
		
			$Admin_Name='RKCL';
			$mail_data = array();
			$mail_data['email'] = $email;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_SMS;
			$subject = "OTP for Email Verifying is";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "insert into tbl_spkyc_otp_verify(SP_Email,SP_OTP,SP_Device)"
                    . "values('".$email."','".$_OTP."','Web')";
            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement); 
            
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
         return $_Response;
    }
	
	
    
}
