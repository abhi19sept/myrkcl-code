<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBankMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsBankMaster {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Bank_Code,Bank_Name,"
                    . "Status_Name From tbl_bank_master as a inner join tbl_status_master as b "
                    . "on a.Bank_Status"
                    . "=b.Status_Code";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Bank_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Bank_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Bank_Code);
            $_SelectQuery = "Select Bank_Code,Bank_Name,"
                    . "Bank_Status From tbl_bank_master Where Bank_Code='" . $_Bank_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Bank_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_Bank_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Bank_Code);
            $_DeleteQuery = "Delete From tbl_Bank_master Where Bank_Code='" . $_Bank_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_BankName,$_BankStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_BankName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BankName);
            $_InsertQuery = "Insert Into tbl_bank_master(Bank_Code,Bank_Name,"
                    . "Bank_Status) "
                    . "Select Case When Max(Bank_Code) Is Null Then 1 Else Max(Bank_Code)+1 End as Bank_Code,"
                    . "'" . $_BankName . "' as Bank_Name,'" . $_BankStatus . "' as Bank_Status"
                    . " From tbl_bank_master";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_bank_master Where Bank_Name='" . $_BankName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_BankCode,$_BankName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_BankCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_BankCode);
            $_UpdateQuery = "Update tbl_bank_master set Bank_Name='" . $_BankName . "',"
                    . "Bank_Status='" . $_FunctionStatus . "' Where Bank_Code='" . $_BankCode . "'";
            $_DuplicateQuery = "Select * From tbl_bank_master Where Bank_Name='" . $_BankName . "' "
                    . "and Bank_Code <> '" . $_BankCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
