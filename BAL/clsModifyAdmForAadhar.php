<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsModifyAdmForAadhar
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsModifyAdmForAadhar {

//put your code here

    public function GetAll($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            $checkbatchinevent = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN"
                    . " tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $course . "' AND"
                    . " a.Event_Batch = '" . $batch . "' AND CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate "
                    . "AND Event_Name='17'";
            $_Response1 = $_ObjConnection->ExecuteQuery($checkbatchinevent, Message::SelectStatement);
            // print_r($_Response1);

            if ($_Response1[0] == 'Success') {

                $_SelectQuery = "Select c.Batch_Name,Admission_Code,Admission_LearnerCode,Admission_Mobile, Admission_Name, Admission_Batch, Admission_Fname, Admission_DOB, Admission_Photo, Admission_Sign, Admission_Fee
                                        FROM tbl_admission as a INNER JOIN tbl_batch_master AS c ON a.Admission_Batch = c.Batch_Code WHERE  Admission_Batch = '" . $batch . "' AND  Admission_Course = '" . $course . "' AND Admission_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' and Admission_Payment_Status='1'";
//$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code 
//WHERE b.Course_Name = '" . $course . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                echo "Something went wrong.";
                return;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerDetails($code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
				
            $_SelectQuery = "SELECT a.Admission_Code, a.Admission_LearnerCode, a.Admission_Name, a.Admission_Fname, a.Admission_DOB, a.Admission_Photo, a.Admission_Sign, b.Course_Name,c.Batch_Name,a.Admission_ITGK_Code,
        a.Admission_UID, a.Admission_Course, a.Admission_Batch FROM tbl_admission AS a INNER JOIN tbl_course_master AS b ON a.Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.Admission_Batch = c.Batch_Code 
                                    WHERE  a.Admission_Code = '" . $code . "'";
//$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
//WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerDetailsExist($code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
				
            $_SelectQuery = "SELECT a.* FROM tbl_adm_log_foraadhar AS a WHERE  a.Adm_Log_AdmissionCode = '" . $code . "'";
//$_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.Course_Code
//WHERE b.Course_Name = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Addtolog($_learnercode, $_NewName, $_NewFname, $_NewDob, $_Lcode, $_oldName, $_oldFname, $_oldDob, $_itgkcode, $_oldUid, $_newUid, $course, $batch, $n, $fn, $d, $u, $docprooftype, $docprooffilename, $lphotofilename, $pic) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User_Code = $_SESSION['User_Code'];
			
			$_learnercode = mysqli_real_escape_string($_ObjConnection->Connect(),$_learnercode);
			$_itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_itgkcode);
			$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
			$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
			
            $checklearnerexam = "Select * from tbl_finalexammapping where learnercode='" . $_learnercode . "' and
				centercode='" . $_itgkcode . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($checklearnerexam, Message::SelectStatement);
            // print_r($_Response1);

            if ($_Response1[0] == 'Success') {

                echo "This Learner has already scheduled in Final Exam, can not be edited";
                return;
            } else {
                $checkbatchinevent = "Select a.Event_Batch, b.Batch_Name From tbl_event_management AS a INNER JOIN"
                        . " tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '" . $course . "' AND"
                        . " a.Event_Batch = '" . $batch . "' AND CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate "
                        . "AND Event_Name='17'";
                $_Response2 = $_ObjConnection->ExecuteQuery($checkbatchinevent, Message::SelectStatement);
                // print_r($_Response1);

                if ($_Response2[0] == 'Success') {

                    $_InsertQuery = "insert into tbl_adm_log_foraadhar (Adm_Log_AdmissionLcode, Adm_Log_AdmissionCode,
                Adm_Log_ITGK_Code, Adm_Log_Lname_New,Adm_Log_Fname_New, Adm_Log_Dob_New,
                 Adm_Log_Apply_User, Adm_Log_ApproveStatus, Adm_Log_Lname_Old, Adm_Log_Fname_Old,
                 Adm_Log_Dob_Old,Adm_Log_Uid_Old,Adm_Log_Uid_New,Adm_Log_Course, Adm_Log_Batch,
                 Adm_Log_IsUpd_Name,Adm_Log_IsUpd_Fname,Adm_Log_IsUpd_Dob,Adm_Log_IsUpd_Uid,Adm_Log_Doc_ProofType,Adm_Log_Doc_Proof,Adm_Log_Photo,Adm_Log_IsUpd_Photo)
    values('" . $_learnercode . "','" . $_Lcode . "','" . $_itgkcode . "','" . $_NewName . "','" . $_NewFname . "','" . $_NewDob . "','" . $_User_Code . "','0','" . $_oldName . "','" . $_oldFname . "','" . $_oldDob . "','" . $_oldUid . "','" . $_newUid . "','" . $course . "','" . $batch . "','" . $n . "','" . $fn . "','" . $d . "','" . $u . "','" . $docprooftype . "','" . $docprooffilename . "','" . $lphotofilename . "','" . $pic . "')";
                    $_DuplicateQuery = "Select Adm_Log_AdmissionCode From tbl_adm_log_foraadhar Where Adm_Log_AdmissionCode='" . $_Lcode . "' AND Adm_Log_ApproveStatus in ('0','2')";

                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                    if ($_Response[0] == Message::NoRecordFound) {
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    } else {

                        // $_UpdateQuery = "update tbl_adm_log_foraadhar set Adm_Log_Lname_New='" . $_NewName . "', Adm_Log_Fname_New="
                        //         . "'" . $_NewFname . "', Adm_Log_Dob_New='" . $_NewDob . "', Adm_Log_Uid_New='" . $_newUid . "', Adm_Log_Doc_ProofType='".$docprooftype."', Adm_Log_Doc_Proof='".$docprooffilename."', Adm_Log_Photo='".$lphotofilename."',"
                        //         . " Adm_Log_IsUpd_Name = IF(Adm_Log_IsUpd_Name = '0','" . $n . "', '1') ,Adm_Log_IsUpd_Fname="
                        //         . "IF(Adm_Log_IsUpd_Fname = '0','" . $fn . "', '1'),Adm_Log_IsUpd_Dob=IF(Adm_Log_IsUpd_Dob = '0','" . $d . "', '1'),"
                        //         . "Adm_Log_IsUpd_Uid = IF(Adm_Log_IsUpd_Uid = '0','" . $u . "', '1'), Adm_Log_IsUpd_Photo = IF(Adm_Log_IsUpd_Photo = '0','" . $pic . "', '1') where "
                        //         . "Adm_Log_AdmissionCode='" . $_Lcode . "' and Adm_Log_ITGK_Code = '" . $_itgkcode . "'";
                        date_default_timezone_set('Asia/Kolkata');
                        $_Date = date("Y-m-d h:i:s");
                        $_UpdateQuery = "update tbl_adm_log_foraadhar set Adm_Log_Lname_New='" . $_NewName . "', Adm_Log_Fname_New="
                                . "'" . $_NewFname . "', Adm_Log_Dob_New='" . $_NewDob . "', Adm_Log_Uid_New='" . $_newUid . "', Adm_Log_Doc_ProofType='" . $docprooftype . "', Adm_Log_Doc_Proof='" . $docprooffilename . "', Adm_Log_Photo='" . $lphotofilename . "',"
                                . " Adm_Log_IsUpd_Name = '" . $n . "' ,Adm_Log_IsUpd_Fname="
                                . "'" . $fn . "', Adm_Log_IsUpd_Dob='" . $d . "',"
                                . "Adm_Log_IsUpd_Uid = '" . $u . "', Adm_Log_IsUpd_Photo = '" . $pic . "', Adm_log_ReApplyDate='" . $_Date . "',Adm_Log_ApproveStatus='0' where "
                                . "Adm_Log_AdmissionCode='" . $_Lcode . "' and Adm_Log_ITGK_Code = '" . $_itgkcode . "' and 
                                Adm_Log_ApproveStatus in ('0','2')";

                        $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    }
                    $_Mobile = $this->GetMobileNumberLeraner($_Lcode, $_itgkcode);
                    $row_mobile = mysqli_fetch_array($_Mobile[2]);
                    $_SMS = "Dear Learner (" . $row_mobile["Admission_LearnerCode"] . "), your Learner Detail Correction Request (Before Final Exam) is Submitted. Please contact IT-GK to Pay Application Fees";
                    SendSMS($row_mobile["Admission_Mobile"], $_SMS);
                    $_Mobile2 = $this->GetMobileNumberITGK($_itgkcode);
                    $row_mobile2 = mysqli_fetch_array($_Mobile2[2]);
                    $_SMS2 = "Dear IT-GK, your Learner Detail Correction Request (Before Final Exam) for Learner (" . $row_mobile["Admission_LearnerCode"] . ")  is Submitted. Please Pay Application Fees in your MYRKCL Login";
                    SendSMS($row_mobile2["User_MobileNo"], $_SMS2);
                }
                else {
                echo "Something went wrong.";
                return;
                }
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllPref($batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User_Code = $_SESSION['User_Code'];
				$batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
				
            if ($_User_Code == '1' || $_User_Code == '14' || $_User_Code == '4258' || $_User_Code == '4257') {
                $_SelectQuery = "select Adm_Log_ITGK_Code,count(Adm_Log_AdmissionCode) as cnt from tbl_adm_log_foraadhar where Adm_Log_ApproveStatus='0' and Adm_Log_PayStatus='1' and Adm_Log_Batch='" . $batchcode . "' group by Adm_Log_ITGK_Code order by Adm_Log_ApplyDate";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                $_Response[0] = '0';
                $_Response[1] = 'error';
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerList($_Itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User_Code = $_SESSION['User_Code'];
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
            if ($_User_Code == '1' || $_User_Code == '14' || $_User_Code == '4258' || $_User_Code == '4257') {
                $_SelectQuery3 = "SELECT a.*, b.Batch_Name from tbl_adm_log_foraadhar as a inner join tbl_batch_master as b on a.Adm_Log_Batch=b.Batch_Code where Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_PayStatus='1' order by Adm_Log_ApplyDate, Adm_Log_ApproveStatus";

                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            } else {
                $_Response3[0] = '0';
                $_Response3[1] = 'error';
            }
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCorrectionCount($_Itgk, $lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            $_SelectQuery3 = "select count(Adm_Log_AdmissionLcode) as appcount from tbl_adm_log_foraadhar where Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_AdmissionLcode='" . $lcode . "' group by Adm_Log_AdmissionLcode order by Adm_Log_ApplyDate";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerAdmEditHistory($lcode, $_Itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
            $_SelectQuery3 = "select a.*, b.Batch_Name from tbl_adm_log_foraadhar as a inner join tbl_batch_master as b on a.Adm_Log_Batch=b.Batch_Code where Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_AdmissionLcode='" . $lcode . "' and Adm_Log_PayStatus='1' order by Adm_Log_ApplyDate";

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateApproveLearner($_LCode, $_Itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        date_default_timezone_set('Asia/Kolkata');
        $_LoginUserCode = $_SESSION['User_LoginId'];
        $date = date("Y-m-d H:i:s");
        try {
				$_LCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LCode);
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
            $_UpdateQuery = "Update tbl_adm_log_foraadhar set Adm_Log_ApproveStatus='1', Adm_Log_ApproveUser='" . $_LoginUserCode . "',"
                    . "Adm_log_ApproveDate= '" . $date . "' Where Adm_Log_AdmissionCode='" . $_LCode . "' and Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_ApproveStatus in ('0','2') and Adm_Log_PayStatus='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetMobileNumberLeraner($code, $_Itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				$code = mysqli_real_escape_string($_ObjConnection->Connect(),$code);
				
            $_SelectQuery = "SELECT Admission_LearnerCode,Admission_Mobile,Admission_Course,Batch_Name FROM tbl_admission as a inner join tbl_batch_master as b on a.Admission_Batch=b.Batch_Code WHERE Admission_Code = '" . $code . "' and Admission_ITGK_Code= '" . $_Itgk . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetMobileNumberITGK($_Itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
            $_SelectQuery = "SELECT User_LoginId,User_MobileNo FROM tbl_user_master WHERE  User_LoginId= '" . $_Itgk . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateRejectLearner($_LCode, $_Itgk, $_onholdrmk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        date_default_timezone_set('Asia/Kolkata');
        $_LoginUserCode = $_SESSION['User_LoginId'];
        $date = date("Y-m-d H:i:s");
        try {
				$_LCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LCode);
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
            $_Mobile = $this->GetMobileNumberLeraner($_LCode, $_Itgk);
            $row_mobile = mysqli_fetch_array($_Mobile[2]);
            $_SMS = "Dear Learner (" . $row_mobile["Admission_LearnerCode"] . "), your Learner Detail Correction Request (Before Final Exam) is Not Approved. Please contact your IT-GK";
            SendSMS($row_mobile["Admission_Mobile"], $_SMS);
            $_Mobile2 = $this->GetMobileNumberITGK($_Itgk);
            $row_mobile2 = mysqli_fetch_array($_Mobile2[2]);
            $_SMS2 = "Dear IT-GK, your Learner Detail Correction Request (Before Final Exam) for Learner (" . $row_mobile["Admission_LearnerCode"] . ")  is Not Approved. Please Update request as Remarks shown in your MYRKCL Login";
            SendSMS($row_mobile2["User_MobileNo"], $_SMS2);
            $_UpdateQuery = "Update tbl_adm_log_foraadhar set Adm_Log_ApproveStatus='2', Adm_Log_ApproveUser='" . $_LoginUserCode . "',"
                    . "Adm_log_ApproveDate= '" . $date . "', Adm_Log_ApproveRemark = '" . $_onholdrmk . "' Where Adm_Log_AdmissionCode='" . $_LCode . "' and Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_ApproveStatus in ('0','2') and Adm_Log_PayStatus='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);


            $_InsertQuery_log = "insert into tbl_adm_log_foraadhar_LOG (Adm_Log_AdmissionLcode, Adm_Log_AdmissionCode, Adm_Log_ITGK_Code, Adm_Log_Lname_New,Adm_Log_Fname_New, Adm_Log_Dob_New, Adm_Log_Apply_User, Adm_Log_ApproveStatus, Adm_Log_Lname_Old, Adm_Log_Fname_Old, Adm_Log_Dob_Old,Adm_Log_Uid_Old,Adm_Log_Uid_New,Adm_Log_Course, Adm_Log_Batch, Adm_Log_IsUpd_Name,Adm_Log_IsUpd_Fname,Adm_Log_IsUpd_Dob,Adm_Log_IsUpd_Uid,Adm_Log_Doc_ProofType,Adm_Log_Doc_Proof,Adm_Log_Photo,Adm_Log_IsUpd_Photo,Adm_Log_ApproveRemark,Adm_log_ApproveDate,Adm_Log_Logdate) select Adm_Log_AdmissionLcode, Adm_Log_AdmissionCode, Adm_Log_ITGK_Code, Adm_Log_Lname_New,Adm_Log_Fname_New, Adm_Log_Dob_New, Adm_Log_Apply_User, Adm_Log_ApproveStatus, Adm_Log_Lname_Old, Adm_Log_Fname_Old, Adm_Log_Dob_Old,Adm_Log_Uid_Old,Adm_Log_Uid_New,Adm_Log_Course, Adm_Log_Batch, Adm_Log_IsUpd_Name,Adm_Log_IsUpd_Fname,Adm_Log_IsUpd_Dob,Adm_Log_IsUpd_Uid,Adm_Log_Doc_ProofType,Adm_Log_Doc_Proof,Adm_Log_Photo,Adm_Log_IsUpd_Photo,'" . $_onholdrmk . "','" . $date . "','" . $date . "'  from tbl_adm_log_foraadhar where Adm_Log_AdmissionCode='" . $_LCode . "' and Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_ApproveStatus in ('0','2') and Adm_Log_PayStatus='1' order by Adm_Log_Code DESC LIMIT 1";
            if ($_Response[0] == 'Successfully Updated') {
                $_Response_InsertQuery_log = $_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
public function FinalRejectLearner($_LCode, $_Itgk, $rejectrmk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        date_default_timezone_set('Asia/Kolkata');
        $_LoginUserCode = $_SESSION['User_LoginId'];
        $date = date("Y-m-d H:i:s");
        try {
				$_LCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LCode);
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
            $_UpdateQuery = "Update tbl_adm_log_foraadhar set Adm_Log_ApproveStatus='3', Adm_Log_ApproveUser='" . $_LoginUserCode . "',"
                    . "Adm_log_ApproveDate= '" . $date . "', Adm_Log_ApproveRemark = '" . $rejectrmk . "' Where Adm_Log_AdmissionCode='" . $_LCode . "' and Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_ApproveStatus in ('0','2') and Adm_Log_PayStatus='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);


            $_InsertQuery_log = "insert into tbl_adm_log_foraadhar_LOG (Adm_Log_AdmissionLcode, Adm_Log_AdmissionCode, Adm_Log_ITGK_Code, Adm_Log_Lname_New,Adm_Log_Fname_New, Adm_Log_Dob_New, Adm_Log_Apply_User, Adm_Log_ApproveStatus, Adm_Log_Lname_Old, Adm_Log_Fname_Old, Adm_Log_Dob_Old,Adm_Log_Uid_Old,Adm_Log_Uid_New,Adm_Log_Course, Adm_Log_Batch, Adm_Log_IsUpd_Name,Adm_Log_IsUpd_Fname,Adm_Log_IsUpd_Dob,Adm_Log_IsUpd_Uid,Adm_Log_Doc_ProofType,Adm_Log_Doc_Proof,Adm_Log_Photo,Adm_Log_IsUpd_Photo,Adm_Log_ApproveRemark,Adm_log_ApproveDate,Adm_Log_Logdate) select Adm_Log_AdmissionLcode, Adm_Log_AdmissionCode, Adm_Log_ITGK_Code, Adm_Log_Lname_New,Adm_Log_Fname_New, Adm_Log_Dob_New, Adm_Log_Apply_User, Adm_Log_ApproveStatus, Adm_Log_Lname_Old, Adm_Log_Fname_Old, Adm_Log_Dob_Old,Adm_Log_Uid_Old,Adm_Log_Uid_New,Adm_Log_Course, Adm_Log_Batch, Adm_Log_IsUpd_Name,Adm_Log_IsUpd_Fname,Adm_Log_IsUpd_Dob,Adm_Log_IsUpd_Uid,Adm_Log_Doc_ProofType,Adm_Log_Doc_Proof,Adm_Log_Photo,Adm_Log_IsUpd_Photo,'" . $rejectrmk . "','" . $date . "','" . $date . "'  from tbl_adm_log_foraadhar where Adm_Log_AdmissionCode='" . $_LCode . "' and Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_ApproveStatus in ('0','2','3') and Adm_Log_PayStatus='1' order by Adm_Log_Code DESC LIMIT 1";
            if ($_Response[0] == 'Successfully Updated') {
                $_Response_InsertQuery_log = $_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);
            }
            $_Mobile = $this->GetMobileNumberLeraner($_LCode, $_Itgk);
            $row_mobile = mysqli_fetch_array($_Mobile[2]);
            $_SMS = "Dear Learner (" . $row_mobile["Admission_LearnerCode"] . "), your Learner Detail Correction Request (Before Final Exam) is Rejected. Remarks Entered are:- ".$rejectrmk;
            SendSMS($row_mobile["Admission_Mobile"], $_SMS);
            $_Mobile2 = $this->GetMobileNumberITGK($_Itgk);
            $row_mobile2 = mysqli_fetch_array($_Mobile2[2]);
            $_SMS2 = "Dear IT-GK, your Learner Detail Correction Request (Before Final Exam) for Learner (" . $row_mobile["Admission_LearnerCode"] . ")  is Rejected. Remarks Entered are:- ". $rejectrmk;
            SendSMS($row_mobile2["User_MobileNo"], $_SMS2);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function UpdateApproveAdmission($_LCode, $_Itgk) {
        global $_ObjConnection;
        global $_ObjFTPConnection;
        $_ObjConnection->Connect();
        date_default_timezone_set('Asia/Kolkata');

        $date = date("Y-m-d H:i:s");
        try {
				$_LCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LCode);
				$_Itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$_Itgk);
				
            $_Mobile = $this->GetMobileNumberLeraner($_LCode, $_Itgk);
            $row_mobile = mysqli_fetch_array($_Mobile[2]);
            $_SMS = "Dear Learner (" . $row_mobile["Admission_LearnerCode"] . "), your Learner Detail Correction Request is Approved.";
            SendSMS($row_mobile["Admission_Mobile"], $_SMS);
            $_Mobile2 = $this->GetMobileNumberITGK($_Itgk);
            $row_mobile2 = mysqli_fetch_array($_Mobile2[2]);
            $_SMS2 = "Dear IT-GK, your Learner Detail Correction Request for Learner (" . $row_mobile["Admission_LearnerCode"] . ")  is Approved.";
            SendSMS($row_mobile2["User_MobileNo"], $_SMS2);
            $_SelectQuery = "select * from tbl_adm_log_foraadhar where Adm_Log_AdmissionCode='" . $_LCode . "' and Adm_Log_ITGK_Code='" . $_Itgk . "' and Adm_Log_ApproveStatus='1' and Adm_Log_PayStatus='1' order by Adm_Log_Code DESC LIMIT 1";

            $_Response_SelectQuery = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            $row_SelectQuery = mysqli_fetch_array($_Response_SelectQuery[2]);

            $_UpdateQuery = "Update tbl_admission as a set a.Admission_Name='" . $row_SelectQuery['Adm_Log_Lname_New'] . "',"
                    . "a.Admission_Fname='" . $row_SelectQuery['Adm_Log_Fname_New'] . "', a.Admission_DOB='" . $row_SelectQuery['Adm_Log_Dob_New'] . "',"
                    . " a.Admission_UID='" . $row_SelectQuery['Adm_Log_Uid_New'] . "', a.IsNewRecord='Y' where"
                    . " a.Admission_Code='" . $_LCode . "' and a.Admission_ITGK_Code='" . $_Itgk . "'";

            // if (file_exists("../upload/AdmissionCorrectionAfterPayment/" . $row_mobile["Admission_LearnerCode"] . "_photo.png")) {
            //     copy("../upload/AdmissionCorrectionAfterPayment/" . $row_mobile["Admission_LearnerCode"] . "_photo.png", "../upload/admission_photo/" . $row_mobile["Admission_LearnerCode"] . "_photo.png");
            // }
                $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($row_mobile['Admission_Course'],$row_mobile['Batch_Name']);
                $oldpic = "/AdmissionCorrectionAfterPayment/".$row_mobile["Admission_LearnerCode"] . "_photo.png";
                $newpicture = $row_mobile["Admission_LearnerCode"] . "_photo.png";
                $_UploadDirectory1 = "/admission_photo/".$_LearnerBatchNameFTP;
                $newpic = $_UploadDirectory1.'/'.$row_mobile["Admission_LearnerCode"] . "_photo.png";
                ftp_copy($_UploadDirectory1,$newpic,$oldpic,$newpicture);

            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	 public function GetLearnerAdmEditHistoryAll($sdate, $edate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $User_UserRoll = $_SESSION['User_UserRoll'];
            if ($User_UserRoll == '1' || $User_UserRoll == '4' || $User_UserRoll == '8' || $User_UserRoll == '28') {
                 $_SelectQuery3 = "select a.*, b.Batch_Name from tbl_adm_log_foraadhar as a inner join tbl_batch_master as b on a.Adm_Log_Batch=b.Batch_Code where Adm_Log_ApplyDate >='" . $sdate . "' and (Adm_Log_ApplyDate or Adm_log_ApproveDate or Adm_log_ReApplyDate) <= '".$edate."' order by Adm_Log_ApplyDate";

                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            }
elseif($User_UserRoll == '11') {
                 $_SelectQuery3 = "select a.*, b.Batch_Name from tbl_adm_log_foraadhar as a inner join tbl_batch_master as b on a.Adm_Log_Batch=b.Batch_Code where Adm_Log_ApplyDate >='" . $sdate . "' and (Adm_Log_ApplyDate or Adm_log_ApproveDate or Adm_log_ReApplyDate) <= '".$edate."' AND Adm_Log_PayStatus = '1' order by Adm_Log_ApplyDate";

                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            }

			else {
                $_Response3[0] = '0';
                $_Response3[1] = 'error';
            }

           

            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
    public function getBatchName($_BatchCode){
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $querybatch = "select Batch_Name from tbl_batch_master where Batch_Code='".$_BatchCode."'";
        $_Response = $_ObjConnection->ExecuteQuery($querybatch, Message::SelectStatement);
        return $_Response;
    }

}
