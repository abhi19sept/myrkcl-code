<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

require 'DAL/classconnection.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$_ObjConnection = new _Connection();
$_Response = array();

require 'DAL/sendsms.php';

class clsCopyDbTables {
    //put your code here
    
	public function GetTables() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_copytables where cpy_status='Active'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping
							 as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name 
							 WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' 
							 ORDER BY `Courseitgk_EOI` ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetBatch($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master 
								WHERE Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLSTATUSFORUPDATE() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where govrole='Yes' AND capprovalid in ('3','0','4','5','8','9','6','7','10') ORDER BY 
								FIELD(capprovalid,'3','0','4','5','8','9','10','7','6')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLEoi() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select EOI_Code,EOI_Name From tbl_eoi_master";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function UpdateAdmission($_tablename,$_course,$_batch) {
       global $_ObjConnection;
        $_ObjConnection->Connect();	
					$table='tbl_admission';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					$result = "select * from $table where 
											Admission_Course='" . $_course ."' AND Admission_Batch='". $_batch ."'";
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where 
											Admission_Course='" . $_course ."' AND Admission_Batch='". $_batch ."'",$dblink2);
					
					 // select all content		
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES
						('".implode("', '",	array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
				
		}
		
		public function UpdateAdmissionTransaction($_tablename,$_course,$_batch) {
			global $_ObjConnection;
			$_ObjConnection->Connect();	
					$table='tbl_admission_transaction';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					$result = "select * from $table where 
									Admission_Transaction_Course='" . $_course ."' AND Admission_Transaction_Batch='". $_batch ."'";
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where 
							Admission_Transaction_Course='" . $_course ."' AND Admission_Transaction_Batch='". $_batch ."'",$dblink2);
					
					 // select all content		
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES
						('".implode("', '",	array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
				
		}
	
	
	public function UpdateBatchMaster($_tablename,$_sdate,$_edate) {
       global $_ObjConnection;
        $_ObjConnection->Connect();				
				$table='tbl_batch_master';
				$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					$result = "select * from $table where Timestamp >= '" . $_sdate ."' AND Timestamp <= '" . $_edate ."'";
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where 
											Timestamp >= '" . $_sdate ."' AND Timestamp <= '" . $_edate ."'",$dblink2);
					
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES
						('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
			}
	
	public function UpdateCorrectionCopy($_tablename,$_sdate,$_edate) {
       global $_ObjConnection;
        $_ObjConnection->Connect();				
				
					$table='tbl_correction_copy';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					$result = "select * from $table where applicationdate >= '" . $_sdate ."' AND 
								applicationdate <= '" . $_edate ."'";
											// select all content
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where 
											applicationdate >= '" . $_sdate ."' AND applicationdate <= '" . $_edate ."'",$dblink2);
											
							
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES 
							('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
		}
		
		public function UpdateOrganizationDetail($srange,$erange) {
			global $_ObjConnection;
			$_ObjConnection->Connect();				
				
					$table='tbl_organization_detail';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					$result = "select * from $table where Organization_Code >= '" . $srange ."' AND 
								Organization_Code <= '" . $erange ."'";
											// select all content
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where 
											Organization_Code >= '" . $srange ."' AND Organization_Code <= '" . $erange ."'",$dblink2);
											
							
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES 
							('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
		}
		
		public function UpdateCourseITGKMapping($_tablename,$_course) {
			global $_ObjConnection;
			$_ObjConnection->Connect();				
				
					$table='tbl_courseitgk_mapping';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					$result = "select a.* from $table as a , tbl_course_master as b where 
								a.Courseitgk_Course=b.Course_Name AND b.Course_Code='". $_course ."'";
											// select all content
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete a FROM $table as a, tbl_course_master as b where 
								a.Courseitgk_Course=b.Course_Name AND b.Course_Code='" . $_course . "'",$dblink2);
						
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES 
							('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
		}
		
		public function UpdateUserMaster($srange,$erange) {
			global $_ObjConnection;
			$_ObjConnection->Connect();				
				
					$table='tbl_user_master';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					$result = "select * from $table where User_Code >= '" . $srange ."' AND 
								User_Code <= '" . $erange ."'";
											// select all content
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where 
											User_Code >= '" . $srange ."' AND User_Code <= '" . $erange ."'",$dblink2);
											
							
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES 
							('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
		}
		
		public function UpdateGovtEmp($_tablename,$_ProcessStatus) {
			global $_ObjConnection;
			$_ObjConnection->Connect();				
				
					$table='tbl_govempentryform';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					 $result = "select * from $table where trnpending='". $_ProcessStatus ."'";
											// select all content
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where trnpending='". $_ProcessStatus ."'",$dblink2);
						
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES 
							('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
		}
		
		public function UpdateEoiCenterList($_tablename,$_EoiCode) {
			global $_ObjConnection;
			$_ObjConnection->Connect();		
				
					$table='tbl_eoi_centerlist';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1
					 $result = "select * from $table where EOI_Code='". $_EoiCode ."'";
											// select all content
					 $_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2
					mysql_query(" $tableinfo[1] ",$dblink2);
					$deleteTable=mysql_query("Delete FROM $table where EOI_Code='". $_EoiCode ."'",$dblink2);
						
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES 
							('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
		}
		
		public function UpdateBiometricRegister($srange,$erange) {
			global $_ObjConnection;
			$_ObjConnection->Connect();				
				
					$table='tbl_biomatric_register';
					$tableinfo = mysql_fetch_array(mysql_query("SHOW CREATE TABLE $table")); // get structure from table on server 1

					$result = "select * from $table where BioMatric_Register_Code >= '" . $srange ."' AND 
								BioMatric_Register_Code <= '" . $erange ."'";
											// select all content
					$_Response = $_ObjConnection->ExecuteQuery($result, Message::SelectStatement);
					
					$dblink2=mysql_connect('117.239.28.94', 'rkclcoin', 'naresh@123'); // connect server 2	
					mysql_select_db('database_rkclcoin_test',$dblink2); // select database 2				
					
					mysql_query(" $tableinfo[1] ",$dblink2);
					
					$deleteTable=mysql_query("Delete FROM $table where BioMatric_Register_Code >= '" . $srange ."' AND 
												BioMatric_Register_Code <= '" . $erange ."'",$dblink2);									
							
					while ($row = mysql_fetch_array($_Response[2], MYSQL_ASSOC) ) {		
						mysql_query("INSERT INTO $table (".implode(", ",array_keys($row)).") VALUES 
							('".implode("', '",array_values($row))."')",$dblink2); // insert one row into new table
					}
					mysql_close($dblink2);
					echo "1";
		}
}