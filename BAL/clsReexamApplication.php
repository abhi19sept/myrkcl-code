<?php

/**
 * Description of clsReexamApplication
 *
 * @author Abhishek
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsReexamApplication {

    //put your code here
    public function GetExamEvent($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " INNER JOIN tbl_event_management as c ON b.Event_Id = c.Event_ReexamEvent WHERE "
                    . " c.Event_Name='6' AND NOW() >= c.Event_Startdate AND NOW() <= c.Event_Enddate";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetExamEventManage($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " WHERE NOW() >= Affilate_Astart AND NOW() <= Affilate_End";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetReexamBatch($_ExamEvent, $_CourseCode, $batchId = 0) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ExamEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExamEvent);
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelBatch = "Select Affilate_ReexamBatches from tbl_exammaster where Affilate_Event = '" . $_ExamEvent . "'";
            $_a = $_ObjConnection->ExecuteQuery($_SelBatch, Message::SelectStatement);
            $_b = mysqli_fetch_array($_a[2]);
            $_Batch = $_b['Affilate_ReexamBatches'];

            $filter = ($batchId) ? " AND b.Batch_Code = '" . $batchId . "'" : '';

            $_SelectQuery = "Select a.Affilate_ReexamBatches, b.Batch_Name, b.Batch_Code, c.Course_Code FROM tbl_exammaster"
                    . " as a INNER JOIN tbl_batch_master as b ON b.Batch_Code IN ($_Batch) inner join tbl_course_master"
                    . " as c on c.Course_Code= b.Course_Code  where c.Course_Code = '" . $_CourseCode . "' and"
                    . " a.Affilate_Event = '" . $_ExamEvent . "' " . $filter;

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_Batch = '" . $batch . "' AND
				Admission_Course = '" . $course . "' AND Admission_Payment_Status = '1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    /* Added By: Sunil for single learner */

    public function GetAllReexam($batch, $course, $examevent, $learnerCode = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
					$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
					$examevent = mysqli_real_escape_string($_ObjConnection->Connect(),$examevent);
					
                $SelectCourse = "Select Course_Name from tbl_course_master where Course_Code='" . $course . "'";
                $_Response2 = $_ObjConnection->ExecuteQuery($SelectCourse, Message::SelectStatement);
                $row = mysqli_fetch_array($_Response2[2]);
                $courseName = $row['Course_Name'];

                $_ITGK_Code = $_SESSION['User_LoginId'];
                $filter = ($learnerCode) ? " AND a.Admission_LearnerCode = '" . $learnerCode . "'" : " AND a.Admission_LearnerCode NOT IN(SELECT learnercode FROM examdata WHERE examid = '" . $examevent . "')";
                $_SelectQuery = "SELECT a.*, b.Course_Name as coursename, $examevent AS exameventid FROM tbl_admission AS a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code INNER JOIN tbl_learner_score ls ON ls.Learner_Code = a.Admission_LearnerCode WHERE a.Admission_Batch = '" . $batch . "' AND Admission_ITGK_Code='" . $_ITGK_Code . "' AND a.Admission_Payment_Status = '1' AND ls.Score >= 11 AND a.Admission_LearnerCode IN (SELECT distinct scol_no FROM tbl_result WHERE study_cen = '" . $_ITGK_Code . "' AND scol_no NOT IN (SELECT scol_no FROM tbl_result WHERE result LIKE ('%pass%')) AND (result NOT LIKE('%pass%') AND (result LIKE('%abs%') OR result LIKE('%fail%') OR result LIKE('%ufm%') OR result LIKE('%um%')))) " . $filter . " ORDER BY a.Admission_Name";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function CheckIfAlreadyAppliedForReExam($learnerCode, $ExamEvent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$learnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$learnerCode);
				$ExamEvent = mysqli_real_escape_string($_ObjConnection->Connect(),$ExamEvent);
				
            $_SelectQuery_Reexam = "SELECT * FROM examdata WHERE examid = '" . $ExamEvent . "' AND learnercode = '" . $learnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery_Reexam, Message::SelectStatement);
        }  catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function addexamdata($Admission_Codes, $_EventID) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventID = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventID);
				
            $_InsertQuery = "";
            foreach ($Admission_Codes as $code) {
                $_SelectQuery_Admission = "SELECT Admission_LearnerCode, Admission_Name, Admission_Fname, Admission_DOB, Admission_Course, Admission_Batch, Admission_ITGK_Code, Admission_District, Admission_Tehsil FROM tbl_admission WHERE Admission_Code = '" . $code . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery_Admission, Message::SelectStatement);
                if (mysqli_num_rows($_Response1[2])) {
                    $row = mysqli_fetch_array($_Response1[2]);
                    $_InsertQuery = "INSERT INTO examdata (examid, learnercode, learnername, fathername, dob, coursename, batchname, itgkcode, itgkname, itgkdistrict, itgktehsil, remark, status, paymentstatus, learnertype, reexamapplyby) VALUES ('" . $_EventID . "', '" . $row['Admission_LearnerCode'] . "', '" . $row['Admission_Name'] . "', '" . $row['Admission_Fname'] . "', '" . $row['Admission_DOB'] . "', '" . $row['Admission_Course'] . "', '" . $row['Admission_Batch'] . "', '" . $row['Admission_ITGK_Code'] . "', '1', '" . $row['Admission_District'] . "', '" . $row['Admission_Tehsil'] . "', 'Active', '1', '0', 'reexam', '" . $_SESSION['User_UserRoll'] . "')";
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                }
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    function GetLearnerCourseBatch($learnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
				$learnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$learnerCode);
				
            $_SelBatch = "SELECT Admission_Course, Admission_Batch FROM tbl_admission WHERE
				Admission_LearnerCode = '" . $learnerCode . "'";
            $result = $_ObjConnection->ExecuteQuery($_SelBatch, Message::SelectStatement);
            $row = mysqli_fetch_array($result[2]);
        }  catch (Exception $_e) {
            $row = 0;
        }

        return $row;
    }
    
}
