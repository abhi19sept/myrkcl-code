<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGovtEntryReport {
    //put your code here
   public function GetAll($_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
			if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == '11' || $_SESSION['User_UserRoll'] == '18' || $_SESSION['User_UserRoll'] == '30'){
				//$_SelectQuery = "Select a.*, b.lotname FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid where trnpending='" . $_Status . "'";
				$_SelectQuery = "Select * from (Select a.Govemp_ITGK_Code,a.learnercode,a.fname,a.faname,a.gdname,a.designation,a.officeaddress,a.trnamount, b.lotname FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid where trnpending='" . $_Status . "') as a ,(Select c.User_LoginId, d.Organization_Name from tbl_user_master as c Inner join tbl_organization_detail as d on d.Organization_User = c.User_Code where User_UserRoll = 7) as b where b.User_LoginId = a.Govemp_ITGK_Code";
			}
			else{
				$_SelectQuery = "Select a.*, b.lotname FROM `tbl_govempentryform` as a INNER JOIN tbl_clot as b on a.lot = b.lotid where Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND trnpending='" . $_Status . "'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function FILLGovLot() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where govrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetGovtApprovalStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where govrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function ShowReimbursement($_Lotid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Lotid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lotid);
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select * FROM `tbl_govempentryform` where lot='" . $_Lotid . "' AND trnpending='0'";
			}
			else{
				$_SelectQuery = "Select * FROM `tbl_govempentryform` where lot='" . $_Lotid . "' AND Govemp_ITGK_Code = '".$_SESSION['User_LoginId']."' AND trnpending='0'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
 
}
