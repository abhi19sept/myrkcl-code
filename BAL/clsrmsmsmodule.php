<?php


/**
 * Description of clsOrgDetail
 *
 * @author  yogendra soni
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsrmsmsmodule {
	
	 
		
     public function ADD($_mobile,$_Msg) 
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$countno=explode(',', $_mobile);
			
			$countno1 = count($countno);
			$_ITGK_Code =   $_SESSION['User_LoginId'];
			$_InsertQuery = "Insert Into tbl_sms(id,Mobile,sms,status,centercode,count) 
			 Select Case When Max(id) Is Null Then 1 Else Max(id)+1 End as id,
			
			 '" . $_mobile. "' as Mobile,
			 '" .$_Msg. "' as sms,
			 '1' as status,
			 '" .$_ITGK_Code. "' as centercode,
			 '" .$countno1. "' as count
			 From tbl_sms";
			 /* $arr_length = count($_mobile); 
			 echo $arr_length; */
			 SendSMS($_mobile, $_Msg);
			 
			
			 
			 $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					
				
               
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	
	
	public function GetMsg() 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK_Code =   $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='".$_ITGK_Code."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAll($_ITGK_Code) 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
				
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$totalpackage=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$totalpackage = $totalpackage + $_Row1['package'];
		      } 
			
			
			$temp=0;
			$_SelectQuery1 = "Select count from tbl_sms where centercode='$_ITGK_Code'";
            $_Response1=$_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			while ($_data = mysqli_fetch_array($_Response1[2])) 
			{
				$temp=$temp+$_data['count'];
				
				
		    }
			//print_r($temp);
			$_Response[2]=$totalpackage-$temp;
			$_Response[3]=$temp;
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function GetTotalpackage($_ITGK_Code) 
	 {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ITGK_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ITGK_Code);
				
		    $_SelectQuery = "Select package from tbl_package_transections where centercode='$_ITGK_Code' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			$tot=0;
			while($_Row1 = mysqli_fetch_array($_Response[2])){
			$tot = $tot + $_Row1['package'];
		      } 
			  $_Response[2]=$tot;
			
			
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	public function GetDistrict() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
		    $_SelectQuery = "Select * from tbl_district_master order by District_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    } 
	
	
	public function Getentityname() {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			
			$_SelectQuery = "Select distinct User_LoginId from tbl_user_master where User_LoginId='".$_SESSION['User_LoginId']."'";  
							
			$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	public function GetcenterList($_district, $_tehsil) 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
				$_district = mysqli_real_escape_string($_ObjConnection->Connect(),$_district);
				$_tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_tehsil);
				
				 $_SelectQuery = "Select a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
									  inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK where b.User_UserRoll='7' AND Courseitgk_Course='RS-CIT' AND  a.Organization_District=".$_district." AND a.Organization_Tehsil IN (".$_tehsil.") AND b.User_UserRoll='7' order by Organization_Name";
					    
					
			$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	
	
	public function GetMobilenumber($_activate) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
			
		    $_SelectQuery = "Select distinct User_MobileNo from tbl_user_master where User_LoginId IN (".$_activate.")";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetCentermobile($_district, $_tehsil) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_district = mysqli_real_escape_string($_ObjConnection->Connect(),$_district);
			$_tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_tehsil);
			
		   $_SelectQuery = "Select distinct b.User_MobileNo from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='7'  AND Organization_District='" . $_district . "'
											AND Organization_Tehsil IN (" . $_tehsil . ") AND Courseitgk_Course='RS-CIT' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function Getstatus() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_ITGK_Code = $_SESSION['User_LoginId'];
		    $_SelectQuery = "Select Payment_Status from tbl_package_transections where  centercode='".$_ITGK_Code."'  ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	public function GetCenterbytehsil($_tehsil) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//print_r($_activate);
			$_tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_tehsil);
			
		   $_SelectQuery = "Select b.* from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='7'  AND Organization_Tehsil='" . $_tehsil . "'
											 AND Courseitgk_Course='RS-CIT' ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	public function GetalltehsilRspCenter($_district,$_tehsil) 
	{
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
			$_district = mysqli_real_escape_string($_ObjConnection->Connect(),$_district);
			$_tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_tehsil);
			
					$_SelectQuery3 = "Select distinct a.Organization_Name,b.User_LoginId from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='7' AND Organization_District='" . $_district . "' AND Organization_Tehsil IN(" . $_tehsil . ") AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock'
											order by a.Organization_Name";
					    
						
						
			$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	
	
	
}
