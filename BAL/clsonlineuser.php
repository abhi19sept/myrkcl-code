<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsMessageMaster
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response1 = array();
$_Response2 = array();
$_Response3 = array();
$_Response4 = array();

class clsonlineuser {

    //put your code here

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $active_sessions = 0;
            $minutes = 160;




            //$_SelectQuery1 = "SELECT * FROM `tbl_active_sessions`";
            if ($sid = session_id()) { # if there is an active session
                $ip = $_SERVER['REMOTE_ADDR'];





                $_DeleteQuery = "delete FROM tbl_active_sessions WHERE date < DATE_SUB(NOW(),INTERVAL $minutes MINUTE)";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);


                $_SelectQuery = "SELECT * FROM tbl_active_sessions WHERE session='$sid'";

                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                $row = mysqli_fetch_array($_Response2[2]);

                if (!$row) {


                    $_InsertQuery = "INSERT INTO tbl_active_sessions (ip, session, date) 
            VALUES ('$ip', '$sid', NOW()) ON DUPLICATE KEY UPDATE `date` = NOW()";
                    //echo $_InsertQuery;

                    $_Response3 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                }

                $_SelectQuery1 = "SELECT * FROM `tbl_active_sessions`";

                $_Response4 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);

                echo $active_sessions = mysqli_num_rows($_Response4[2]);

                //print_r($active_sessions);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response4;
    }

    public function GetAll1($values) {
        /// echo $values;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $active_sessions = 0;
            $minutes = 60;
            if ($sid = session_id()) { # if there is an active session
                $ip = $_SERVER['REMOTE_ADDR'];
               $_InsertQuery1 = "INSERT INTO tbl_visited_user (ip, session, date, usercode, usercity) 
            VALUES ('$ip', '$sid', NOW(),'" . $_SESSION['User_LoginId'] . "','" . $values . "')";
                //echo $_InsertQuery;

                $_Response4 = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
                //print_r($active_sessions);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response4;
    }
public function getinformation() {

        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
		mysqli_set_charset('utf8');
            $_SelectQuery = "Select * From tbl_important_information WHERE Status =1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function getFixedPopUpContent($role, $userCode = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        mysqli_set_charset('utf8');
        $condition = (!empty($userCode)) ? " AND (fp.usercode='" . $userCode . "' OR fp.usercode = 0)" : '';
        $query = "SELECT fp.* FROM tbl_fixed_popup_content fp WHERE fp.status = 1 AND fp.Role = '" . $role . "' $condition AND fp.id NOT IN (SELECT popupid FROM tbl_fixed_popup_viewedby WHERE usercode='" . $userCode . "' AND status = 1) ORDER BY fp.id DESC LIMIT 1";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        }  catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function setFixedPopUpContentInactive($id, $userCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $query = "INSERT IGNORE INTO tbl_fixed_popup_viewedby SET 
            usercode = '" . $userCode . "',
            popupid = '" . $id . "'";
        $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
    }
}
