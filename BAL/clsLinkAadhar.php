<?php

/**
 * Description of clsLinkAadhar
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsLinkAadhar {

    //put your code here
	    public function GetAllLinkAadharCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','4','3','20')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAllAdmissionBatch($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
            $_SelectQuery = "Select Batch_Code,Batch_Name From tbl_batch_master where Course_Code ='". $_CourseCode ."'  and 
							 Batch_Code>111 order by Batch_Code DESC ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function ShowDetailForEdit($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            $_SelectQuery = "Select Admission_LearnerCode,Admission_Course,Admission_Batch,Admission_PIN,Admission_UID From 
							tbl_admission where Admission_LearnerCode ='". $lcode ."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function GetLearnerListTech($_course, $_batch, $learner) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
					$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					$learner = mysqli_real_escape_string($_ObjConnection->Connect(),$learner);
					
				if ($_SESSION['User_UserRoll'] == '28' || $_SESSION['User_UserRoll'] == '1') {
                                    $_SelectQuery3 = "SELECT a.Admission_Aadhar_Status,a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_DOB,a.Admission_UID,
                                                        a.Admission_Mobile FROM tbl_admission as a inner join tbl_final_result as b on a.Admission_LearnerCode=b.scol_no WHERE
                                                        Admission_Course = '" . $_course . "' AND Admission_Batch = '" . $_batch . "' and Admission_LearnerCode='".$learner."' and result LIKE('%pass%')";
                                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                                    return $_Response3;
                                } 
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetLearnerListITGK($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
					$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
					
				if ($_SESSION['User_UserRoll'] == '7') {
                                    $_SelectQuery3 = "SELECT a.Admission_Aadhar_Status,a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_DOB,a.Admission_UID,
                                                        a.Admission_Mobile FROM tbl_admission as a inner join tbl_final_result as b on a.Admission_LearnerCode=b.scol_no WHERE
                                                        Admission_ITGK_Code ='" . $_SESSION['User_LoginId'] . "' AND
                                                        Admission_Course = '" . $_course . "' AND Admission_Batch = '" . $_batch . "' and result LIKE('%pass%')";
                                    $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                                    return $_Response3;
                                } 
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetVerificationAadhar($txtLcode,$txtBatch,$txtCourse) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$txtLcode = mysqli_real_escape_string($_ObjConnection->Connect(),$txtLcode);
				$txtBatch = mysqli_real_escape_string($_ObjConnection->Connect(),$txtBatch);
				$txtCourse = mysqli_real_escape_string($_ObjConnection->Connect(),$txtCourse);
				
             $_SelectQuery3 = "SELECT Admission_Name,Admission_Fname,Admission_DOB FROM tbl_admission where
								Admission_LearnerCode='".$txtLcode."' and Admission_Course='".$txtCourse."' and
								Admission_Batch='".$txtBatch."'";
            $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
    
	/* Insert learner details into aadhar detail table after successfully aadhar linked AND update flag status and aadhar number of
		learner in admission and final result tabe  */
    public function InsertAadharData($txtLcode,$txtBatch,$txtCourse,$namew,$fnamew,$newDOB,$Aadharno,$Gender,$District,$Tehsil,$Pincode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$txtCourse = mysqli_real_escape_string($_ObjConnection->Connect(),$txtCourse);
				$txtBatch = mysqli_real_escape_string($_ObjConnection->Connect(),$txtBatch);
				$txtLcode = mysqli_real_escape_string($_ObjConnection->Connect(),$txtLcode);
				
			$_UpdateQuery = "Update tbl_final_result as a,tbl_batch_master as b set 
								a.learner_aadhar='" . $Aadharno . "', a.batch_name=b.Batch_Name
								where a.scol_no='". $txtLcode ."' and b.Batch_Code='" . $txtBatch . "'";
			
          
           $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if($_Response[0]=='Successfully Updated'){
			
				 $_UpdateQuery = "Update tbl_admission set Admission_UID='" . $Aadharno . "', Admission_Aadhar_Status='1'
							Where Admission_Course='" . $txtCourse . "' AND Admission_Batch='" . $txtBatch . "'
							AND Admission_LearnerCode='". $txtLcode ."'";
				
				$_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		   
                $_InsertQuery="insert into tbl_aadhar_details (Aadhar_Detail_Lcode,Aadhar_Detail_Number,Aadhar_Detail_Course,"
                        . "Aadhar_Detail_Batch,Aadhar_Detail_Name,Aadhar_Detail_FName,Aadhar_Detail_Gender,Aadhar_Detail_DOB,"
                        . "Aadhar_Detail_District,Aadhar_Detail_Tehsil,Aadhar_Detail_PinCode)"
                        . "values('".$txtLcode."','".$Aadharno."','".$txtCourse."','".$txtBatch."','".$namew."','".$fnamew."','".$Gender."'"
                        . ",'".$newDOB."','".$District."','".$Tehsil."','".$Pincode."')";
                $_Response1 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
           } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
	
	/* select learner details from final result table according to learner code that we have passed in function.  */
	 public function getLearnerResults($Lcode,$signedNameReg) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$Lcode);
				$signedNameReg = mysqli_real_escape_string($_ObjConnection->Connect(),$signedNameReg);
				
            //$filter = " AND tr.scol_no IN ('127170810110841789')";
            $curdate = date("d-m-Y h:i:s");
             $_SelectQuery = "SELECT tr.scol_no AS learnercode, tr.result_date, upper(tr.name) as name, tr.study_cen AS centercode, tr.percentage,
			 upper(tr.fname) as fname, tr.exameventnameID, tr.vmou_ref_num,
			 tr.exam_date, '" . $curdate . "' AS istdt, '".$signedNameReg."' as registrar_name, '' as reg_no FROM tbl_final_result tr INNER JOIN tbl_events em ON 
			 tr.exameventnameID = em.Event_Id INNER JOIN tbl_user_master um ON um.User_LoginId = tr.study_cen 
			 INNER JOIN tbl_organization_detail om ON om.Organization_User = um.User_Code 
			 WHERE scol_no = '" . $Lcode . "' AND result LIKE('%pass%')";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }        
        return $_Response;
    }

    function getCleanedlearnerCodes($learnerCode) {
        $learnerCodes = explode(',', $learnerCode);
        $codes = [];
        foreach ($learnerCodes as $code) {
            $codes[] = trim($code);
        }

        return !empty($codes) ? implode("','", $codes) : '';
    }

	/* select learner course batch name from admission table according to learner code that we have passed in function.  */
	public function getData($learnercode) {
       global $_ObjConnection;
        $_ObjConnection->Connect();
        $result = [];
		$learnercode = mysqli_real_escape_string($_ObjConnection->Connect(),$learnercode);
        $_SelectQuery = "SELECT bm.Batch_Name AS batchname, cm.Course_Name AS coursename FROM tbl_admission ta INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course INNER JOIN tbl_batch_master bm ON ta.Admission_Batch = bm.Batch_Code WHERE ta.Admission_LearnerCode = '" . $learnercode . "'";
        $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2]) > 0) {
            $result = mysqli_fetch_assoc($_Response[2]);
        }

        return $result;
    }
	
	public function ShowLearnerEcertificate($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            $_SelectQuery = "Select * From tbl_final_result where result LIKE('%pass%') and scol_no='".$lcode."' and status='0'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	/* update admission and final result table status after certificate added to rajevault  */
	public function UpdateEcertificateStatus($lcode,$eventid,$itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
					$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
					$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
					
            $_UpdateQuery = "update tbl_final_result set status='1' where scol_no ='".$lcode."' and exameventnameID='".$eventid."' and
							 study_cen='".$itgk."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			$_UpdateQuery1 = "Update tbl_admission set Admission_Aadhar_Status='2'
							Where Admission_ITGK_Code='" . $itgk . "' AND Admission_LearnerCode='". $lcode ."'";
            $_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
		   } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	/* select learner details from final result table according to learner code that we have passed in function.  */
	public function ShowLearnerEcertificateUpdate($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {    
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
			
            $_SelectQuery = "Select * From tbl_final_result where result LIKE('%pass%') and scol_no='".$lcode."' and status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	/* update admission and final result table status after updated certificate sent to rajevault  */
	 public function UpdateEcertificateStatusForUpdateCertificate($lcode,$eventid,$itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
            $_UpdateQuery = "update tbl_final_result set status='2' where scol_no ='".$lcode."' and exameventnameID='".$eventid."' 
							 and study_cen='".$itgk."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			$_UpdateQuery1 = "Update tbl_admission set Admission_Aadhar_Status='4'
							Where Admission_ITGK_Code='" . $itgk . "' AND Admission_LearnerCode='". $lcode ."'";
            $_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }	
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
