$("#form").validate({
        rules: {
            ddlstatus: { required: true },	ddlLot: { required: true },		txtBatchId: { required: true },		txtFee: { required: true },
			txtIncentive: { required: true },	txtTotalAmt: { required: true }, txtRemark: { required: true }, txtEmpDOB: { required: true }, txtAddress: { required: true }
        },			
        messages: {				
			ddlstatus: 				{ required: '<span style="color:red; font-size: 12px;">Please select Process Status</span>' },
			ddlLot:		 			{ required: '<span style="color:red; font-size: 12px;">Please select Lot No.</span>' },
			txtBatchId: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Batch Id</span>' },
			txtFee: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your Fees</span>' },			
			txtIncentive:			{ required: '<span style="color:red; font-size: 12px;">Please enter incentive amount</span>' },
			txtTotalAmt: 			{ required: '<span style="color:red; font-size: 12px;">Please enter total amount</span>' },
			txtRemark: 			{ required: '<span style="color:red; font-size: 12px;">Please enter remark</span>' },
			txtEmpDOB: 			{ required: '<span style="color:red; font-size: 12px;">Please enter D.O.B</span>' },
			txtAddress:				{ required: '<span style="color:red; font-size: 12px;">Please enter office address</span>' }
        },
	});