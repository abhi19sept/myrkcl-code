<?php

/**
 * Description of clsLotteryResultReport
 *
 // * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsLotteryResultReport {

    //put your code here
	public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code='1'
							order by Course_Code ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function GetAdmissionBatchcode($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
				$_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $course . "'
								and Batch_Code>='270' order by Batch_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
	
    public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
				$_LoginRole = $_SESSION['User_UserRoll'];
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				
				if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' 
					|| $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258' || $_SESSION['User_Code'] == '9063') {
//					$_SelectQuery = "select lottery_scheme_lcode,lottery_scheme_itgk_code,lottery_scheme_lname,lottery_scheme_fname,
//							lottery_scheme_dob,Discount,lottery_scheme_reservation_type,c.District_Name,
//							ITGK_Name,RSP_Code,RSP_Name,Admission_Payment_Status as payment from
//							tbl_admission_lottery_scheme as a inner join tbl_admission_lottery_master as b 
//							on a.lottery_scheme_master_id=b.level_id inner join tbl_district_master as c 
//							on a.lottery_scheme_district=c.District_Code inner join vw_itgkname_distict_rsp as d
//							on a.lottery_scheme_itgk_code=d.ITGKCODE inner join tbl_admission as e
//                                                        on a.lottery_scheme_lcode=e.Admission_LearnerCode where lottery_scheme_batch='".$_batch."'";
					$_SelectQuery = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,
									a.Admission_DOB,a.admitcardfilename,b.RSP_Code,b.RSP_Name,b.District_Name,
									Discount,d.Admission_Payment_Status,d.Admission_LearnerCode as adm_lcode,remark
									FROM tbl_admission_fee_waiver_scheme AS a
									inner join tbl_admission_lottery_master as c on a.Lottery_master_id=c.level_id 
									left join tbl_admission as d on a.Admission_LearnerCode=d.Admission_LearnerCode
									inner join vw_itgkname_distict_rsp as b on a.Admission_ITGK_Code=b.ITGKCODE
									WHERE  a.Admission_Course = '".$_course."' AND a.Admission_Batch = '".$_batch."' and a.status = '1'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
				elseif ($_LoginRole == '7') {
					$_SelectQuery = "select lottery_scheme_lcode,lottery_scheme_itgk_code,lottery_scheme_lname,lottery_scheme_fname,
							lottery_scheme_dob,Discount,lottery_scheme_reservation_type,c.District_Name,
							ITGK_Name,RSP_Code,RSP_Name from
							tbl_admission_lottery_scheme as a inner join tbl_admission_lottery_master as b 
							on a.lottery_scheme_master_id=b.level_id inner join tbl_district_master as c 
							on a.lottery_scheme_district=c.District_Code inner join vw_itgkname_distict_rsp as d
							on a.lottery_scheme_itgk_code=d.ITGKCODE where lottery_scheme_batch='".$_batch."'
							and lottery_scheme_itgk_code='".$_SESSION['User_LoginId']."'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				} 
				elseif ($_LoginRole == '14') {
					$_SelectQuery = "select Admission_LearnerCode as lottery_scheme_lcode,Admission_ITGK_Code as lottery_scheme_itgk_code,
									Admission_Name as lottery_scheme_lname,Admission_Fname as lottery_scheme_fname,
									Admission_DOB as lottery_scheme_dob,Discount,
									Lottery_reservation_type as lottery_scheme_reservation_type,District_Name,
									ITGK_Name,RSP_Code,RSP_Name from
									tbl_admission_fee_waiver_scheme as a inner join tbl_admission_lottery_master as b 
									on a.Lottery_master_id=b.level_id inner join vw_itgkname_distict_rsp as d
									on a.Admission_ITGK_Code=d.ITGKCODE where Admission_Batch='".$_batch."' and a.status='1'
									AND a.Admission_RspName='".$_SESSION['User_Code']."'";
					$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				} 
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
    
    
    public function AddRemark($lcode,$remarktext) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$remarktext = mysqli_real_escape_string($_ObjConnection->Connect(),$remarktext);
				
            $_UpdateQuery = "Update tbl_admission_fee_waiver_scheme set remark = '".$remarktext."' where
							Admission_LearnerCode = '".$lcode."'";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function CheckRemark($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            $_SelectQuery = "select remark from tbl_admission_fee_waiver_scheme where Admission_LearnerCode = '".$lcode."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
}
