<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOwnershipChangeReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOwnershipChangeReport {
    //put your code here
    
       
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 8) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code";
            } elseif ($_SESSION['User_UserRoll'] == 14) {
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code 
                            INNER JOIN tbl_user_master as c on a.Org_Itgk_Code = c.User_LoginId 
                            and c.User_Rsp= '" . $_SESSION['User_Code'] . "'";
            }
            else{
            $_SelectQuery = "";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
