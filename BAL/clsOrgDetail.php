<?php


/**
 * Description of clsOrgDetail
 *
 * @author 
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsOrgDetail {
	
	 public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		$_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

              if ($_LoginRole == '1') {

                $_LoginUserType = "1";
            } elseif ($_LoginRole == '3') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '4') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '9') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } 
			elseif ($_LoginRole == '11') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } 
			elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
            } else {
                echo "hello";
            }
           // $_SESSION['UserType'] = $_LoginUserType;
            $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_user_master AS b ON a.Centercode = b.User_LoginId WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_i = 0;
            $CenterCode2 = '';
            while ($_Row = mysqli_fetch_array($_Response1[2])) {

                $CenterCode2.=$_Row['Centercode'] . ",";
                $_i = $_i + 1;
            }
            $CenterCode3 = rtrim($CenterCode2, ",");

            if ($CenterCode3) {
			$_SelectQuery2 = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_org_type_master as c on a.Organization_Type=c.Org_Type_Code where b.User_LoginId IN ($CenterCode3)";
               
				//$_SelectQuery2 = "Select * from tbl_staff_detail where Staff_User IN ($CenterCode3)";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response2;
            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response1;
    }
	
	
	
	
		/* public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			
           $_SelectQuery = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_org_type_master as c on a.Organization_Type=c.Org_Type_Code inner join tbl_state_master as d on a.Organization_State=d.State_Code inner join tbl_region_master as e on a.Organization_Region=e.Region_Code inner join tbl_district_master as f on a.Organization_District=f.District_Code inner join tbl_tehsil_master as g on a.Organization_Tehsil=g.Tehsil_Code inner join tbl_country_master as h on a.Organization_Country=h.Country_Code inner join tbl_area_master as i on a.Organization_Area=i.Area_Code where b.User_LoginId='".$_SESSION['User_LoginId']."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    } */
    //put your code here
     public function UPDATE($_OrgCode,$_OrgName,$_OrgType,$_OrgRegno,$_Landmark,$_Road,$_Street,$_HouseNo,$_OrgEstDate,$_address,$_docproof) 
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
			
		  $_fileBirth1   =	$_SESSION['User_LoginId'].'_'.$_docproof.'.jpg';	
		  
		 $_UpdateQuery = "Update tbl_organization_detail set 
						
						Organization_RegistrationNo='" . $_OrgRegno . "',
						Organization_FoundedDate='" . $_OrgEstDate . "',
						Organization_Type='" . $_OrgType . "',
						Organization_DocType='" . $_docproof . "',
						Organization_ScanDoc='" . $_fileBirth1 . "',
						Organization_Landmark='" . $_Landmark . "',
						Organization_Road='" . $_Road . "',
						Organization_Street='" . $_Street . "',
						Organization_HouseNo='" . $_HouseNo . "',
						Organization_Address='" . $_address . "'
						Where Organization_Code='" . $_OrgCode . "'";
            
			
				$birth = $_SERVER['DOCUMENT_ROOT'] . '/upload/orgdetails_birth/'.$_fileBirth1;
				
				if ( file_exists($birth) )
				{
					
					 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
					
				}
					else
					{
						echo "Please Re-Upload Documents Again.";
						return;
					}		
            
            
               
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	
	public function GetDatabyCode($_Org_Code)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Org_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Org_Code);
				
            $_SelectQuery = "Select *  From tbl_organization_detail Where Organization_Code='" . $_Org_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Org_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Org_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Org_Code);
				
            $_DeleteQuery = "Delete From tbl_organization_detail Where Organization_Code='" . $_Org_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	public function Getdoctype() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select * from tbl_cdoccategory_master";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function GetDetails()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.Organization_Code as Organization_Code,a.Organization_Name as Organization_Name,a.Organization_RegistrationNo as Organization_RegistrationNo,a.Organization_FoundedDate as Organization_FoundedDate from  tbl_organization_detail as a inner join  tbl_user_master as b on a.Organization_User=b.User_Code Where b.User_LoginId= '" . $_SESSION['User_LoginId'] . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	 public function GetDobdocs() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select cdocname From tbl_cdocorganization_master";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }     
    
}
