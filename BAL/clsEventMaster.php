<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of clsStateMaster
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsEventMaster {
    //put your code here
   
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
            $_SelectQuery = "Select * from tbl_events ORDER BY Event_Id DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetDatabyCode($_Event_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Event_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event_Code);
				
            $_SelectQuery = "Select Event_Id,Event_Name,Event_Status"
                    . " From tbl_events Where Event_Id='" . $_Event_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Event_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Event_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event_Code);
				
            $_DeleteQuery = "Delete From tbl_events Where Event_Id='" . $_Event_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_EventName,$_StateStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventName = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventName);
				$_StateStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_StateStatus);
				
            $_InsertQuery = "Insert Into tbl_events(Event_Id,Event_Name,"
                    . "Event_Status) "
                    . "Select Case When Max(Event_Id) Is Null Then 1 Else Max(Event_Id)+1 End as Event_Id,"
                    . "'" . $_EventName . "' as Event_Name,"
                    . "'" . $_StateStatus . "' as Event_Status"
                    . " From tbl_events";
            $_DuplicateQuery = "Select * From tbl_events Where Event_Name='" . $_EventName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_EventName,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_EventName = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventName);
				
            $_UpdateQuery = "Update tbl_events set Event_Name='" . $_EventName . "',"
                    
                    . "Event_Status='" . $_Status . "' Where Event_Id='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_events Where Event_Name='" . $_EventName . "' "
                    . "and Event_Id <> '" . $_Code . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
