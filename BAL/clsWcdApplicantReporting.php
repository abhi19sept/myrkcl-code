<?php

/**
 * Description of clsWcdApplicantReporting
 *
 * @author Mayank
 */
require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdApplicantReporting {

    //put your code here

	public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'
								and Batch_Code > '293'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function getwcdlearnerdetails($_course, $_batch) {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
            $_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4'){
				 $_SelectQuery = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference, 
									sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS pref,
									sum(case when Oasis_Admission_ReportedStatus = 'Reported' then 1 else 0 end) As 
									Reported_Learner,
									sum(case when Oasis_Admission_ReportedStatus = 'Not Reported' then 1 else 0 end) 
									NotReported_Learner
									from tbl_user_master as a
									inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.user_loginid 
									inner join tbl_district_master as c on 
									b.Oasis_Admission_District = c.District_Code WHERE a.User_UserRoll='7' and
									b.Oasis_Admission_Course='".$_course."' AND 
									b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible'
									group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_LoginUserRole == '17') {
				//print_r($_SESSION);
				   $_SelectQuery1 = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference,
								sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS pref,
								sum(case when Oasis_Admission_ReportedStatus = 'Reported' then 1 else 0 end) Reported_Learner,
								sum(case when Oasis_Admission_ReportedStatus = 'Not Reported' then 1 else 0 end) NotReported_Learner
								from tbl_user_master as a
								inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.User_LoginId inner join tbl_district_master as c on
								b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND 
								b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible' AND
								b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "'
								 group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			}
			else if ($_LoginUserRole == '14') {
				//print_r($_SESSION);
				   $_SelectQuery1 = "select c.District_Name, c.District_Code, b.Oasis_Admission_Final_Preference,
								sum(case when Oasis_Admission_LearnerStatus in ('Approved') then 1 else 0 end) AS pref,
								sum(case when Oasis_Admission_ReportedStatus = 'Reported' then 1 else 0 end) Reported_Learner,
								sum(case when Oasis_Admission_ReportedStatus = 'Not Reported' then 1 else 0 end) NotReported_Learner
								from tbl_user_master as a
								inner join tbl_oasis_admission as b on b.Oasis_Admission_Final_Preference = a.User_LoginId inner join tbl_district_master as c on
								b.Oasis_Admission_District = c.District_Code WHERE b.Oasis_Admission_Course='".$_course."' AND 
								b.Oasis_Admission_Batch='" . $_batch . "' AND b.Oasis_Admission_Eligibility='eligible' AND
								a.User_Rsp='" . $_SESSION['User_Code'] . "'
								 group by b.Oasis_Admission_Final_Preference";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			}
			else{
				$_SelectQuery1 = "SELECT Oasis_Admission_LearnerCode,Oasis_Admission_Name,Oasis_Admission_Fname,
								Oasis_Admission_Subcategory,Oasis_Admission_MaritalStatus,Oasis_Admission_Mobile,
								Oasis_Admission_Caste, Oasis_Admission_Final_Preference,d.Category_Name,
								Oasis_Admission_ReportedStatus FROM
									tbl_oasis_admission as b INNER JOIN tbl_category_master as d on
									b.Oasis_Admission_Category = d.Category_Code
									WHERE b.Oasis_Admission_Batch = '" . $_batch . "' AND 
									b.Oasis_Admission_Final_Preference='" . $_SESSION['User_LoginId'] . "' AND
									b.Oasis_Admission_Eligibility='eligible' AND b.Oasis_Admission_LearnerStatus='Approved'
									Order by Oasis_Admission_Final_Priority";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
			}
           } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	
public function UpdateWcdRSCITReported($_Lcode,$docname,$status,$dtstamp,$batch,$_ITGK,$_allottime) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_InsertQuery = "insert into tbl_wcd_allocate_batch_time (Batch_Time_lcode, Batch_Time_ITGK_Code,
								Batch_Time_Learner_Name, Batch_Time_Father_Name, Batch_Time_dob, Batch_Time_course,
								Batch_Time_batch, Batch_Time_mobile,Batch_Time_Reported_Doc, 
								Batch_Time_time) select Oasis_Admission_LearnerCode, Oasis_Admission_Final_Preference,
								Oasis_Admission_Name, Oasis_Admission_Fname, Oasis_Admission_DOB, Oasis_Admission_Course,
								Oasis_Admission_Batch, Oasis_Admission_Mobile,'".$docname."', '".$_allottime."'
								from tbl_oasis_admission
								where Oasis_Admission_LearnerCode='".$_Lcode."' and Oasis_Admission_Batch='".$batch."'
					and Oasis_Admission_LearnerStatus='Approved' and Oasis_Admission_Final_Preference='".$_ITGK."'";
                $_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				
				$_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_ReportedStatus='Reported'                   
						 Where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $batch . "'
						 AND Oasis_Admission_LearnerStatus = 'Approved' AND Oasis_Admission_Eligibility='eligible'
						 AND Oasis_Admission_LearnerCode='".$_Lcode."'";
				   $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement); 
							   
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
	public function UpdateWcdRSCFAReported($_Lcode,$docname,$status,$dtstamp,$batch,$_ITGK,$_allottime,$_email) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
		
        try {
				
              $_chkDuplicate = "Select Batch_Time_lcode From tbl_wcd_allocate_batch_time Where
									Batch_Time_email='" . $_email . "' and Batch_Time_batch='".$batch."'";
              $_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
				if ($_Response2[0] == Message::NoRecordFound){							
					$_InsertQuery = "insert into tbl_wcd_allocate_batch_time (Batch_Time_lcode, Batch_Time_ITGK_Code,
								Batch_Time_Learner_Name, Batch_Time_Father_Name, Batch_Time_dob, Batch_Time_course,
								Batch_Time_batch, Batch_Time_mobile,Batch_Time_time,Batch_Time_email,
								Batch_Time_Reported_Doc)
								select Oasis_Admission_LearnerCode, Oasis_Admission_Final_Preference,
								Oasis_Admission_Name, Oasis_Admission_Fname, Oasis_Admission_DOB, Oasis_Admission_Course,
								Oasis_Admission_Batch, Oasis_Admission_Mobile, '".$_allottime."', '".$_email."',
								'".$docname."' from tbl_oasis_admission
								where Oasis_Admission_LearnerCode='".$_Lcode."' and Oasis_Admission_Batch='".$batch."'
					and Oasis_Admission_LearnerStatus='Approved' and Oasis_Admission_Final_Preference='".$_ITGK."'";
				$_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				
				$_UpdateQuery = "Update tbl_oasis_admission set Oasis_Admission_ReportedStatus='Reported'                   
						 Where Oasis_Admission_Final_Preference='" . $_ITGK . "' AND Oasis_Admission_Batch='" . $batch . "'
						 AND Oasis_Admission_LearnerStatus = 'Approved' AND Oasis_Admission_Eligibility='eligible'
						 AND Oasis_Admission_LearnerCode='".$_Lcode."'";
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement); 
				   
				}
				else {
					$_Response[0] = Message::DuplicateRecord;
					$_Response[1] = Message::Error;
				}          
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
}
