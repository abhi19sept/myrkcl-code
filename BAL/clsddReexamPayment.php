<?php

/**
 * Description of clsddReexamPayment
 *
 * @author Abhishek
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
class clsddReexamPayment {
    //put your code here
    
    public function Update($_Code,$_PayTypeCode,$_TranRefNo,$_firstname,$_phone,$_email,$_amount,$_ddno,$_dddate,$_txtMicrNo,$_ddlBankDistrict,$_ddlBankName,$_txtBranchName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_InsertQuery = "INSERT INTO tbl_reexam_ddpay (reexam_ddpay_code, dd_Transaction_Status, dd_Transaction_Name, dd_amount,"
								. "dd_Transaction_Txtid, dd_no, dd_date,"
								. "dd_centercode,dd_bank,dd_branch,dd_Transaction_Email) "
								. "Select Case When Max(reexam_ddpay_code) Is Null Then 1 Else Max(reexam_ddpay_code)+1 End as reexam_ddpay_code,"
								. "'Intermediate' as dd_Transaction_Status,'" .$_firstname. "' as dd_Transaction_Name,'" .$_amount. "' as dd_amount,"
								. "'" .$_TranRefNo. "' as dd_Transaction_Txtid,"
								. "'" .$_ddno. "' as dd_no,'" .$_dddate. "' as dd_date,'" .$_Code. "' as dd_centercode,'" .$_ddlBankName. "' as dd_bank,'" .$_txtBranchName. "' as dd_branch,'" .$_email. "' as dd_Transaction_Email"
								. " From tbl_reexam_ddpay";
		  $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
		 
		   
		   
		   $_InsertQuery1 = "INSERT INTO tbl_reexam_transaction (Reexam_Transaction_Code, Reexam_Transaction_Status, Reexam_Transaction_Fname, Reexam_Transaction_Amount,"
								. "Reexam_Transaction_Txtid,Reexam_Payment_Mode, Reexam_Transaction_ProdInfo, Reexam_Transaction_Email,"
								. "Reexam_Transaction_CenterCode) "
								. "Select Case When Max(Reexam_Transaction_Code) Is Null Then 1 Else Max(Reexam_Transaction_Code)+1 End as Reexam_Transaction_Code,"
								. "'Intermediate' as Reexam_Transaction_Status,'" .$_firstname. "' as Reexam_Transaction_Fname,'" .$_amount. "' as Reexam_Transaction_Amount,"
								. "'" .$_TranRefNo. "' as Reexam_Transaction_Txtid,'DD Mode' as Reexam_Payment_Mode,"
								. "'" .$_PayTypeCode. "' as Reexam_Transaction_ProdInfo,'" .$_email. "' as Reexam_Transaction_Email,'" .$_Code. "' as Reexam_Transaction_CenterCode"
								. " From tbl_reexam_transaction";
		  $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
		  
		  $_UpdateQuery = "Update examdata set paymentstatus = 8, reexxam_TranRefNo='" . $_TranRefNo . "' "			
								. "Where itgkcode='" . $_Code . "' AND examdata_code IN (" . $_SESSION['ExamDataCode'] . ")";
		  $_Response2=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
									
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAdmissionFee($_batchcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select RKCL_Share FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetCenterCode($_batchcode,$_coursecode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select Admission_ITGK_Code,Admission_TranRefNo FROM tbl_admission WHERE Admission_Batch = '" . $_batchcode . "' AND Admission_Course = '" . $_coursecode . "' AND Admission_Payment_Status = '8'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAllDdData($_refno)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
           $_SelectQuery="Select * FROM tbl_admission_ddpay WHERE dd_Transaction_Txtid = '" . $_refno . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function UpdateDDPaymentStatus()
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
			$Transactionid = $_SESSION['LearnerAdmissionCodes'];
			
          $_UpdateQuery = "Update tbl_admission_ddpay set dd_Transaction_Status = 'Success'"
                      . " Where dd_Transaction_Txtid IN ($Transactionid) "; 
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		  
		  $_UpdateQuery1 = "Update tbl_admission_transaction set Admission_Transaction_Status = 'Success'"
                      . " Where Admission_Transaction_Txtid IN ($Transactionid) "; 
              $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
			  
		  $_UpdateQuery2 = "Update tbl_admission set Admission_Payment_Status = '1'"
                      . " Where Admission_TranRefNo IN ($Transactionid) "; 
              $_Response2=$_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
