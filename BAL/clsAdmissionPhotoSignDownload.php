<?php


/**
 * Description of clsAdmissionPhotoSignDownload
 *
 * @author 
 */

require 'DAL/classconnection.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsAdmissionPhotoSignDownload {	
	
    public function gentxtfile($_Course,$_Batch)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
            $_SelectQuery = "select Admission_Photo from tbl_admission where Admission_Course='".$_Course."' and Admission_Batch='".$_Batch."'";            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	public function GetDetails($_Course,$_Batch)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_Batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch);
			$_SelectQuery = "select Admission_Photo from tbl_admission where Admission_Course='".$_Course."' and Admission_Batch='".$_Batch."'";			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','3','4','5','22','23','25')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAllBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $course . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
}
