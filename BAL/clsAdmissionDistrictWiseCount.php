<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionDistrictWiseCount {

    //put your code here   
	public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11'){
			$_SelectQuery =	"select District_Name, sum(case when Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' then 1 else 0 end) Total_Learner,
							 sum(case when Admission_Course='" . $_course . "' AND Admission_Batch='" . $_batch . "' AND Admission_Payment_Status = '1' then 1 else 0 end)
							 Confirmed_Learner from tbl_admission as a inner join tbl_district_master as b where a.Admission_District = b.District_Code group by Admission_District";			
			}
			 $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
