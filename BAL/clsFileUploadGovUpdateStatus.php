<?php

/**
 * Description of clsFileUploadGovUpdateStatus
 *
 * @author Mayank
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsFileUploadGovUpdateStatus {

    //put your code here
    //print_r($_SESSION['DataArrayExamResult']);
    public function Addgovstatusdata($lcode, $itgkcode, $remark, $status, $_fname) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {  
            $lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
            $itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkcode);
				 $_UpdateQuery = "Update tbl_govempentryform set trnpending='" . $status . "', Remarks='" . $remark . "' WHERE learnercode= '" . $lcode . "' AND Govemp_ITGK_Code= '" . $itgkcode . "'"; 
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				//print_r($_Response);
				$_Row=mysqli_affected_rows();				
				 if($_Row == "1") {
					 
					 date_default_timezone_set('Asia/Kolkata');
					 $_Date = date("Y-m-d h:i:s");					 
					  $_InsertQuery = "INSERT INTO tbl_govstatusupdatelog (Govemp_id, Govemp_ITGK_Code, Govemp_Learnercode, Govemp_Status, Govemp_Process_Status,"
                    . "Govemp_Filename, Govemp_Process_Remark, Govemp_Datetime) "
                    . "Select Case When Max(Govemp_id) Is Null Then 1 Else Max(Govemp_id)+1 End as Govemp_id,"
                    . "'" . $itgkcode . "' as Govemp_ITGK_Code,'" . $lcode . "' as Govemp_Learnercode,'RecordUpdated' as Govemp_Status,'" . $status . "' as Govemp_Process_Status,"
                    . "'" . $_fname . "' as Govemp_Filename, '" . $remark . "' as Govemp_Process_Remark, '" . $_Date . "' as Govemp_Datetime"
                    . " From tbl_govstatusupdatelog";
					$_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				 }
				 else {
					 $_Date = date("Y-m-d h:i:s");					 
					  $_InsertQuery = "INSERT INTO tbl_govstatusupdatelog (Govemp_id, Govemp_ITGK_Code, Govemp_Learnercode, Govemp_Status, Govemp_Process_Status,"
                    . "Govemp_Filename, Govemp_Process_Remark, Govemp_Datetime) "
                    . "Select Case When Max(Govemp_id) Is Null Then 1 Else Max(Govemp_id)+1 End as Govemp_id,"
                    . "'" . $itgkcode . "' as Govemp_ITGK_Code,'" . $lcode . "' as Govemp_Learnercode,'RecordNotUpdated' as Govemp_Status, '" . $status . "' as Govemp_Process_Status,"
                    . "'" . $_fname . "' as Govemp_Filename, '" . $remark . "' as Govemp_Process_Remark, '" . $_Date . "' as Govemp_Datetime"
                    . " From tbl_govstatusupdatelog";
					$_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				 }
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
