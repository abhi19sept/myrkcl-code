<?php

/**
 * Description of clsgetpassword
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsusermasterdetails {

    //put your code here

     public function getschedule() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		$_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

              if ($_LoginRole == '1') {

                $_LoginUserType = "1";
            } elseif ($_LoginRole == '3') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '4') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } elseif ($_LoginRole == '9') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } 
			elseif ($_LoginRole == '11') {
               
                $_LoginUserType = $_SESSION['User_Code'];
            } 
			elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
            }
			 elseif ($_LoginRole == '14') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "8";
            } 

			else {
                echo "hello";
            }
           // $_SESSION['UserType'] = $_LoginUserType;
            $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_user_master AS b ON a.Centercode = b.User_LoginId WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_i = 0;
            $CenterCode2 = '';
            while ($_Row = mysqli_fetch_array($_Response1[2])) {

                $CenterCode2.=$_Row['Centercode'] . ",";
                $_i = $_i + 1;
            }
            $CenterCode3 = rtrim($CenterCode2, ",");

            if ($CenterCode3) {
			$_SelectQuery2 = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_district_master as c on a.Organization_District=c.District_Code 
			inner join tbl_tehsil_master as d on a.Organization_Tehsil=d.Tehsil_Code 
			where b.User_LoginId IN ($CenterCode3)";
               
				//$_SelectQuery2 = "Select * from tbl_staff_detail where Staff_User IN ($CenterCode3)";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response2;
            }
			
			else if($_loginflag == "8")
			{
				 $_SelectQuery2 = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_district_master as c on a.Organization_District=c.District_Code 
				inner join tbl_tehsil_master as d on a.Organization_Tehsil=d.Tehsil_Code 
				where b.User_UserRoll='7' AND b.User_Rsp='".$_SESSION['User_Code']."'";
				$_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response2;
				
			}
			
			else if ($_LoginRole == '7') {
				$_SelectQuery2 = "Select * from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code inner join tbl_district_master as c on a.Organization_District=c.District_Code 
									inner join tbl_tehsil_master as d on a.Organization_Tehsil=d.Tehsil_Code 
									where b.User_LoginId = '" . $_SESSION['User_LoginId'] . "'";
               
				//$_SelectQuery2 = "Select * from tbl_staff_detail where Staff_User IN ($CenterCode3)";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response2;
            }
			
			
			
			
			
			
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response1;
    }


    public function CheckITGKExpireDate() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_SelectQuery = "SELECT CourseITGK_ExpireDate, Courseitgk_EOI FROM tbl_courseitgk_mapping WHERE Courseitgk_ITGK = '" . $_SESSION['User_LoginId'] . "' AND Courseitgk_EOI IN (1,29,30) AND EOI_Fee_Confirm = 1 ORDER BY Courseitgk_EOI ASC";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        $result = '';
        $dates = [];
        if ($_Response[0] != Message::NoRecordFound) {
            while($row = mysqli_fetch_array($_Response[2])) {
                $type = ($row['Courseitgk_EOI'] == 1) ? ' (RS-CIT)' : ' (RS-CFA)';
                $dates[] = date("jS F, Y", strtotime($row['CourseITGK_ExpireDate'])) . $type;
            }
            $result = implode(', ', $dates);
        } else {
            $result = 'Not registred, as you have not completed EOI yet.';
        }

        return $result;
    }

    public function reNewITGK() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $eoiCodes = $this->getRenewalEOICodes();

        $month = date("m");
        $cfaNextRenewalDate = ($month > 3 && $month <= 12) ? (date("Y") + 1) : date("Y");
        $cfaNextRenewalDate .= '-03-31 00:00:00';
        
        $insertSql = "INSERT INTO tbl_courseitgk_mapping_log SELECT *, now() FROM tbl_courseitgk_mapping WHERE Courseitgk_ITGK = '" . $_SESSION['User_LoginId'] . "' AND Courseitgk_EOI IN (" . $eoiCodes . ") AND CourseITGK_Renewal_Status = 'Y' AND EOI_Fee_Confirm = 1";
        $_ObjConnection->ExecuteQuery($insertSql, Message::InsertStatement);

        $UpdateSql = "UPDATE tbl_courseitgk_mapping SET CourseITGK_Renewal_Status = 'N', CourseITGK_ExpireDate = IF(Courseitgk_EOI IN (5,6,7,14,15,16), '" . $cfaNextRenewalDate . "', CONCAT(DATE_FORMAT(DATE_ADD(DATE_ADD(CourseITGK_ExpireDate, INTERVAL 1 YEAR), INTERVAL 1 MONTH), '%Y-%m-'), '10 00:00:00')) WHERE Courseitgk_ITGK = '" . $_SESSION['User_LoginId'] . "' AND Courseitgk_EOI IN (" . $eoiCodes . ") AND CourseITGK_Renewal_Status = 'Y' AND EOI_Fee_Confirm = 1";
        $_ObjConnection->ExecuteQuery($UpdateSql, Message::UpdateStatement);

        $UpdateSql = "UPDATE tbl_eoi_centerlist SET EOI_Status_Flag = 'N' WHERE EOI_Status_Flag = 'Y' AND EOI_ECL = '" . $_SESSION['User_LoginId'] . "' AND EOI_Code IN (" . $eoiCodes . ")";
        $_ObjConnection->ExecuteQuery($UpdateSql, Message::UpdateStatement);

        unset($_SESSION['ITGK_Expire_Days']);
        unset($_SESSION['ITGK_Expire_For']);
    }

    private function getRenewalEOICodes() {
        $btnName = (isset($_SESSION['ITGK_Expire_For'])) ? $_SESSION['ITGK_Expire_For'] : 'RS-CIT';
        $iscfa = stripos($btnName, 'CFA');
        $eoiCodes = ($iscfa) ? '5,6,7,11,14,15,16' : '1,2';

        return $eoiCodes;
    }

}
