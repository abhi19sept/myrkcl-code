<?php

/**
 * Description of clsGstInvoiceRpt
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGstInvoiceRpt {

    //put your code here
    
        public function GetBatchName($BatchCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT  Batch_Name FROM tbl_batch_master WHERE Batch_Code = '" . $BatchCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Learner_fee($startdate, $enddate, $bcodearray) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ai.invoice_no as Invoice_No, ai.amount as Invoice_Amount, Date_Format(ai.addtime,'%d-%m-%Y') as Invoice_Date,
                                         if(ad.Admission_Payment_Status = 1, 'Success', 'Cancelled') as Invoice_Status FROM tbl_admission_invoice ai
                                         INNER join tbl_admission ad ON ai.invoice_ref_id = ad.Admission_Code and ad.Admission_Batch in ($bcodearray) 
                                         AND Date_Format(ai.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(ai.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	    public function Learner_fee_admin($startdate, $enddate, $bcodearray) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ai.invoice_no as Invoice_No, ai.amount as Invoice_Amount, Date_Format(ai.addtime,'%d-%m-%Y') as Invoice_Date,
                                         if(ad.Admission_Payment_Status = 1, 'Success', 'Cancelled') as Invoice_Status,ad.Admission_Code,ad.Admission_TranRefNo FROM tbl_admission_invoice ai
                                         INNER join tbl_admission ad ON ai.invoice_ref_id = ad.Admission_Code and ad.Admission_Batch in ($bcodearray) 
                                         AND Date_Format(ai.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND Date_Format(ai.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function reexam_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ri.invoice_no as Invoice_No, ri.amount as Invoice_Amount, Date_Format(ri.addtime,'%d-%m-%Y') as Invoice_Date, 
                                    if(ed.paymentstatus = 1, 'Success', 'Cancelled') as Invoice_Status,reexam_TranRefNo FROM tbl_reexam_invoice ri INNER join examdata ed 
                                    ON ri.exam_data_code = ed.examdata_code AND Date_Format(ri.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND 
                                    ed.learnertype='reexam' AND Date_Format(ri.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function NCR_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ni.invoice_no as Invoice_No, ni.amount as Invoice_Amount, Date_Format(ni.addtime,'%d-%m-%Y') as Invoice_Date, 
                                        if(um.User_UserRoll in (15,7), 'Success', 'Cancelled') as Invoice_Status,om.Org_TranRefNo  FROM tbl_ncr_invoice ni INNER join tbl_user_master um 
                                        ON ni.Ao_Itgkcode = um.User_LoginId inner join tbl_org_master as om on ni.Ao_Ack_No = om.Org_Ack AND Date_Format(ni.addtime,'%Y-%m-%d') >= '" . $startdate . "' AND
                                        um.User_PaperLess='Yes' AND Date_Format(ni.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function correction_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ci.invoice_no as Invoice_No, ci.amount as Invoice_Amount, Date_Format(ci.addtime,'%d-%m-%Y') as Invoice_Date, 
                            if(cc.Correction_Payment_Status =1, 'Success', 'Cancelled') as Invoice_Status,Correction_TranRefNo FROM tbl_correction_invoice ci INNER join 
                            tbl_correction_copy cc ON ci.transaction_ref_id = cc.cid AND Date_Format(ci.addtime,'%Y-%m-%d') >= '" . $startdate . "'  AND
                             Date_Format(ci.addtime,'%Y-%m-%d') <= '" . $enddate . "' ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function eoi_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ei.invoice_no as Invoice_No, ei.amount as Invoice_Amount, Date_Format(ei.addtime,'%d-%m-%Y') as Invoice_Date, 
                                    if(cim.EOI_Fee_Confirm =1, 'Success', 'Cancelled') as Invoice_Status,Courseitgk_TranRefNo FROM tbl_eoi_invoice ei 
                                    INNER join tbl_courseitgk_mapping as cim ON ei.Itgkcode = cim.Courseitgk_ITGK and
                                    cim.Courseitgk_EOI = ei.eoi_code  AND Date_Format(ei.addtime,'%Y-%m-%d') >= '" . $startdate . "'  AND
                                     Date_Format(ei.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function name_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ai.invoice_no as Invoice_No, ai.amount as Invoice_Amount, Date_Format(ai.addtime,'%d-%m-%Y') as Invoice_Date,
                                        if(ad.fld_status in('5','2'), 'Success', 'Cancelled') as Invoice_Status,fld_transactionID FROM tbl_change_address_itgk ad INNER JOIN 
                                        tbl_nameaddress_invoice ai ON ai.invoice_ref_id = ad.fld_ref_no inner join tbl_address_name_transaction as c on ad.fld_ref_no=c.fld_ref_no WHERE 
                                        Date_Format(ai.addtime,'%Y-%m-%d') >=  '" . $startdate . "'  AND Date_Format(ai.addtime,'%Y-%m-%d') <=  '" . $enddate . "' 
                                        ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	    public function RenewalPenalty_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ei.invoice_no as Invoice_No, ei.amount as Invoice_Amount, Date_Format(ei.addtime,'%d-%m-%Y') as Invoice_Date, 
                                    if(cim.RenewalPenalty_Payment_Status =1, 'Success', 'Cancelled') as Invoice_Status, RenewalPenalty_Trans_Id FROM tbl_renewal_penalty_invoice ei 
                                    INNER join tbl_renewal_penalty as cim ON ei.panelty_code = cim.RenewalPenalty_Code  and
                                    Date_Format(ei.addtime,'%Y-%m-%d') >= '" . $startdate . "'  AND
                                     Date_Format(ei.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function OwnerChange_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ai.invoice_no as Invoice_No, ai.amount as Invoice_Amount, Date_Format(ai.addtime,'%d-%m-%Y') as Invoice_Date, 
if(ad.Org_PayStatus = '1', 'Success', 'Cancelled') as Invoice_Status,Org_TranRefNo FROM tbl_ownership_change ad LEFT JOIN 
tbl_ownershipchange_invoice ai ON ai.Ao_Ack_No = ad.Org_Ack WHERE 
Date_Format(ai.addtime,'%Y-%m-%d') >= '" . $startdate . "'  AND
                                     Date_Format(ai.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function CorrectionBfrExam_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ai.invoice_no as Invoice_No, ai.amount as Invoice_Amount, Date_Format(ai.addtime,'%d-%m-%Y') as Invoice_Date, if(ad.Adm_Log_PayStatus = '1', 'Success', 'Cancelled') as Invoice_Status,Adm_Log_TranRefNo FROM tbl_adm_log_foraadhar ad INNER JOIN 
tbl_aadhar_upd_invoice ai ON ad.Adm_Log_Code = ai.invoice_ref_id WHERE 
Date_Format(ai.addtime,'%Y-%m-%d') >= '" . $startdate . "'  AND
                                     Date_Format(ai.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }    
    public function RSCFACert_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ai.invoice_no as Invoice_No, ai.amount as Invoice_Amount, Date_Format(ai.addtime,'%d-%m-%Y') as Invoice_Date, if(ad.apply_rscfa_cert_payment_status = '1', 'Success', 'Cancelled') as Invoice_Status, apply_rscfa_cert_txnid FROM tbl_apply_rscfa_certificate ad INNER JOIN 
tbl_apply_rscfa_exam_invoice ai ON ad.apply_rscfa_cert_id = ai.transaction_ref_id WHERE 
Date_Format(ai.addtime,'%Y-%m-%d') >= '" . $startdate . "'  AND
                                     Date_Format(ai.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
public function RSCFAReexam_fee($startdate, $enddate) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT ai.invoice_no as Invoice_No, ai.amount as Invoice_Amount, Date_Format(ai.addtime,'%d-%m-%Y') as Invoice_Date, if(ad.rscfa_reexam_paymentstatus = '1', 'Success', 'Cancelled') as Invoice_Status, rscfa_reexam_TranRefNo FROM tbl_rscfa_reexam_application ad INNER JOIN 
tbl_rscfa_reexam_invoice ai ON ad.rscfa_reexam_code = ai.transaction_ref_id WHERE 
Date_Format(ai.addtime,'%Y-%m-%d') >= '" . $startdate . "'  AND
                                     Date_Format(ai.addtime,'%Y-%m-%d') <= '" . $enddate . "'  ORDER BY Invoice_No ASC";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}
