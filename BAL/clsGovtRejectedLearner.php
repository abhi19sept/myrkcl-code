<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGovtRejectedLearner {
    //put your code here
    
    public function GetAllRejectedList() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_CenterCode = $_SESSION['User_LoginId'];
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select * FROM tbl_govempentryform where trnpending='4'";
			}
			else{	
				$_SelectQuery = "Select * FROM tbl_govempentryform WHERE Govemp_ITGK_Code = '" . $_CenterCode . "' AND trnpending = '4'";
			}
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	public function GetDatabyLearnerCode($_LearnerCode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
            $_SelectQuery = "Select * From tbl_govempentryform Where learnercode='" . $_LearnerCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}