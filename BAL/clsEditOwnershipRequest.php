<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsEditOwnershipRequest
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsEditOwnershipRequest {
    //put your code here
    
    public function GetOrgDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type FROM tbl_ownership_change as a 
                            INNER JOIN tbl_org_type_master as b ON a.Organization_Type=b.Org_Type_Code
                            where Org_Itgk_Code='" . $_SESSION['User_LoginId'] . "'
                            AND a.Org_Application_Approval_Status = 'OnHold' and Org_Final_Approval_Status='Pending' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
