<?php

/*
 * @author Abhi

 */

/**
 * Description of clsDistrictMaster
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsDistrictMaster {
    //put your code here
     public function GetAll($region) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$region = mysqli_real_escape_string($_ObjConnection->Connect(),$region);
				
            if(!$region)
            {
            $_SelectQuery = "Select District_Code,District_Name,Region_name,State_Name, Status_Name,Country_Name From tbl_district_master as a inner join tbl_region_master as b on a.District_Region=b.Region_Code inner join tbl_state_master as c on b.Region_State=c.State_Code inner join tbl_status_master as d on a.District_Status=d.Status_Code inner join tbl_country_master as e on c.State_Country=e.Country_Code where District_StateCode='29' order by District_Name";
            }
            else {
                $_SelectQuery = "Select District_Code,District_Name,Region_name,State_Name,"
                    . "Status_Name,Country_Name From tbl_district_master as a inner join tbl_region_master as b "
                    . "on a.District_Region=b.Region_Code inner join tbl_state_master as c "
                    . "on b.Region_State=c.State_Code inner join tbl_status_master as d "
                    . "on a.District_Status=d.Status_Code inner join tbl_country_master as e "
                    . "on c.State_Country=e.Country_Code and a.District_Region='" . $region . "' order by District_Name";
                
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_District_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_District_Code);
				
            $_SelectQuery = "Select District_Code,District_Name,District_Region,District_Status "
                    . "From tbl_district_master Where District_Code='" . $_District_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_District_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_District_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_District_Code);
				
            $_DeleteQuery = "Delete From tbl_district_master Where District_Code='" . $_District_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_DistrictName,$_Parent,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DistrictName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DistrictName);
				$_Parent = mysqli_real_escape_string($_ObjConnection->Connect(),$_Parent);
				
            $_InsertQuery = "Insert Into tbl_district_master(District_Code,District_Name,"
                    . "District_Region,District_Status) "
                    . "Select Case When Max(District_Code) Is Null Then 1 Else Max(District_Code)+1 End as District_Code,"
                    . "'" . $_DistrictName . "' as District_Name,"
                    . "'" . $_Parent . "' as District_Region,'" . $_Status . "' as District_Status"
                    . " From tbl_district_master";
            $_DuplicateQuery = "Select * From tbl_district_master Where District_Name='" . $_DistrictName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_DistrictName,$_Region,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_DistrictName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DistrictName);
				
            $_UpdateQuery = "Update tbl_district_master set District_Name='" . $_DistrictName . "',"
                    . "District_Region='" . $_Region . "',"
                    . "District_Status='" . $_Status . "' Where District_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_district_master Where District_Name='" . $_DistrictName . "' "
                    . "and District_Code <> '" . $_Code . "'";
           
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
