<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOwnershipChangeRequest
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');

$_ObjConnection = new _Connection();
$_Response = array();


class clsOwnershipChangeRequest {
    //put your code here
    
    public function GetITGK_Details() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '7' ) {
                
                $_SelectQuery = "Select DISTINCT a.*, c.Organization_Name as ITGK_Name, c.*, d.District_Name,
                                e.Tehsil_Name,f.User_LoginId as RSP_Code,
                                g.Organization_Name as RSP_Name, 
                                j.Municipality_Type_Name, k.Municipality_Name, l.Ward_Name, 
                                m.Block_Name, n.GP_Name, o.Village_Name, q.Org_Type_Name From tbl_user_master as a 
                                LEFT join tbl_organization_detail as c on a.User_Code=c.Organization_User
                                LEFT join tbl_org_type_master as q on c.Organization_Type=q.Org_Type_Code
                                LEFT join tbl_district_master as d on c.Organization_District=d.District_Code
                                LEFT join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
                                LEFT join tbl_municipality_type as j on c.Organization_Municipal_Type=j.Municipality_Type_Code
                                LEFT join tbl_municipality_master as k on c.Organization_Municipal=k.Municipality_Code
                                LEFT join tbl_ward_master as l on c.Organization_WardNo=l.Ward_Code
                                LEFT join tbl_panchayat_samiti as m on c.Organization_Panchayat=m.Block_Code
                                LEFT join tbl_gram_panchayat as n on c.Organization_Gram=n.GP_Code
                                LEFT join tbl_village_master as o on c.Organization_Village=o.Village_Code
                                LEFT join tbl_user_master as f on a.User_Rsp=f.User_Code
                                LEFT join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_courseitgk_mapping as h on h.Courseitgk_ITGK=a.User_LoginId
                                WHERE a.User_UserRoll = '7' and a.User_LoginId='" .$_SESSION['User_LoginId']. "'
                                and CURDATE() < h.CourseITGK_ExpireDate and h.Courseitgk_Course='RS-CIT'";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }  else {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
    public function Add($_GeneratedId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_OrgEstDate, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename, $_doctype, $_PanNo, $_AADHARNo, 
            $_IFSC_Code, $_AccountName, $_AccountNumber, $_BankName, $_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof, $BankPANfilename, $bankdocfilename) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_CenterCode = $_SESSION['User_LoginId'];
        
        date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d h:i:s");
        
        $_SMSOLD = "Dear ITGK, Your Application for Ownership Change has been Submitted on MYRKCL. Kindly proceed to payment for approval from RKCL. RKCL";
        $_SMSNEW = "Dear Applicant, Your Application for Ownership Change with ITGK name " . $_OrgName . " has been Submitted on MYRKCL. Kindly proceed to payment for approval from RKCL.";
        $_SMSRSP = "Dear SP, IT-GK " . $_CenterCode . " has submitted an Ownership Change Request on MYRKCL. RKCL";
        
        $_EmailOLD = "Dear ITGK, Your Application for Ownership Change has been Submitted on MYRKCL. You are required to make payment from your login in myrkcl for further application process.";
        $_EmailNEW = "Dear Applicant, Your Application for Ownership Change with ITGK name " . $_OrgName . " has been Submitted on MYRKCL.";
        $_EmailRSP = "Dear SP, IT-GK " . $_CenterCode . " has submitted an Ownership Change Request on MYRKCL.";
        
        try {

            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_OrgMobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgMobile);
					
                $_SelectQuery = "Select User_MobileNo, User_EmailId, User_Rsp FROM tbl_user_master WHERE
					User_LoginId = '" . $_CenterCode . "'";
                $_Response10 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Row = mysqli_fetch_array($_Response10[2]);                
                $_OLDMobile = $_Row['User_MobileNo'];
                $_OLDEmail = $_Row['User_EmailId']; 
                $_RspCode = $_Row['User_Rsp']; 
                
                $_SelectQuery1 = "Select User_MobileNo, User_EmailId FROM tbl_user_master WHERE User_Code = '" . $_RspCode . "'";
                $_Response20 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_Row = mysqli_fetch_array($_Response20[2]);                
                $_RspMobile = $_Row['User_MobileNo'];
                $_RspEmail = $_Row['User_EmailId']; 

                $_InsertQuery = "Insert Into tbl_ownership_change(Organization_Code,Org_Ack,Organization_Name,"
                        . "Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,"
                        . "Organization_DocType,Organization_ScanDoc,Organization_UID,Organization_AddProof,Organization_AppForm,Organization_TypeDocId,"
                        . "Org_App_Date,Org_Email,Org_Mobile,Org_Itgk_Code,Org_PAN,Org_AADHAR,"
                        
                        . "Bank_Account_Name,Bank_Account_Number,Bank_Account_Type,Bank_Ifsc_code,"
                        . "Bank_Name,Bank_Micr_Code,Bank_Branch_Name,Bank_Pan_No,Bank_Pan_Name,Bank_Id_Proof,Bank_Pan_Document, Bank_Id_Doc"
                        . ") "
                        . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                        . " Case When Max(Org_Ack) Is Null Then 4000 Else Max(Org_Ack)+1 End as Org_Ack,"
                        . "'" . $_OrgName . "' as Organization_Name,'" . $_OrgRegno . "' as Organization_RegistrationNo,"
                        . "'" . $_OrgEstDate . "' as Organization_FoundedDate,  '" . $_OrgType . "' as Organization_Type,"
                        . "'" . $_doctype . "' as Organization_DocType,'" . $PANfilename . "' as Organization_ScanDoc,"
                        . "'" . $UIDfilename . "' as Organization_UID,'" . $addprooffilename . "' as Organization_AddProof,"
                        . "'" . $appformfilename . "' as Organization_AppForm,'" . $_GeneratedId . "' as Organization_TypeDocId,"
                                              
                        . "'" . $_Date . "' as Org_App_Date,'" . $_OrgEmail . "' as Org_Email,'" . $_OrgMobile . "' as Org_Mobile,"
                        . "'" . $_SESSION['User_LoginId'] . "' as Org_Itgk_Code,'" . $_PanNo . "' as Org_PAN,'". $_AADHARNo . "' as Org_AADHAR,"
                        
                        . "'" . $_AccountName . "' as Bank_Account_Name,  '" . $_AccountNumber . "' as Bank_Account_Number,"
                        . "'" . $_AccountType . "' as Bank_Account_Type,'" . $_IFSC_Code . "' as Bank_Ifsc_code,"
                        . "'" . $_BankName . "' as Bank_Name,'" . $_MicrCode . "' as Bank_Micr_Code,"
                        . "'" . $_BranchName . "' as Bank_Branch_Name,'" . $_PanNo1 . "' as Bank_Pan_No,"
                        . "'" . $_PanName . "' as Bank_Pan_Name,'" . $_IdProof . "' as Bank_Id_Proof,"
                        . "'" .$BankPANfilename."' as Bank_Pan_Document,'" . $bankdocfilename . "' as Bank_Id_Doc"
                        . " From tbl_ownership_change";
                
                $_DuplicateQuery = "Select * From tbl_ownership_change Where Org_Mobile='" . $_OrgMobile . "' OR Org_Email = '" . $_OrgEmail . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
//
                $_DuplicateQuery1 = "Select * From tbl_ownership_change Where Org_Itgk_Code='" . $_SESSION['User_LoginId'] . "' and  Org_Application_Approval_Status = 'Pending'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);
//

                if ($_Response[0] == Message::NoRecordFound ){
                    if ($_Response1[0] == Message::NoRecordFound) {
					
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                        SendSMS($_OLDMobile, $_SMSOLD);
                        SendSMS($_OrgMobile, $_SMSNEW);
                        SendSMS($_RspMobile, $_SMSRSP);
                        
                        $Admin_Name='RKCL';
			$mail_data = array();
			$mail_data['email'] = $_OLDEmail;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_EmailOLD;
			$subject = "Ownership change Applicaition submitted on MYRKCL";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
                        
                        $Admin_Name='RKCL';
			$mail_data = array();
			$mail_data['email'] = $_OrgEmail;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_EmailNEW;
			$subject = "Ownership change Applicaition submitted on MYRKCL";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
                        
                        $Admin_Name='RKCL';
			$mail_data = array();
			$mail_data['email'] = $_RspEmail;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_EmailRSP;
			$subject = "Ownership change Applicaition submitted on MYRKCL";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
                                        
                } 
                else {
                    echo " Ownership Change application is still in Process.";
                    return;                     
                }
                }else {
                    echo " Owner Details Already Exists.";
                    return;                     
                }
                } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
    
    public function GenerateOTP($_EmailId,$_MobNo) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		
		$_MobNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_MobNo);
		
        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTPSMS = OTP();
        //$_SMS = "OTP for Validating Mobile Number on MYRKCL is " . $_OTPSMS;
      $_SMS = "For validating Mobile Number your OTP is '" . $_OTPSMS ."' in MYRKCL. Rajasthan Knowledge Corporation Limited";
        
        $_OTPEMAIL = OTP();
      //$_EmailMsg = "OTP for Validating Email on MYRKCL is " . $_OTPEMAIL;	
$_EmailMsg = "For validating Mobile Number your OTP is '" . $_OTPEMAIL ."' in MYRKCL. Rajasthan Knowledge Corporation Limited";	
        
        date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d H:i:s");

        try {
           
            $_InsertQuery = "Insert Into tbl_ownershipotp_verify(OWNERSHIP_Code,OWNERSHIP_Mobile,OWNERSHIP_Email,OWNERSHIP_Start_Date,OWNERSHIP_OTPSMS,OWNERSHIP_OTPEmail)"
                    . "Select Case When Max(OWNERSHIP_Code) Is Null Then 1 Else Max(OWNERSHIP_Code)+1 End as OWNERSHIP_Code,"
                    . "'" .  $_MobNo . "' as OWNERSHIP_Mobile,'" . $_EmailId . "' as OWNERSHIP_Email,'" . $_Date . "' as OWNERSHIP_Start_Date,"
                    . "'" . $_OTPSMS . "' as OWNERSHIP_OTPSMS,'" . $_OTPEMAIL . "' as OWNERSHIP_OTPEmail"
                    . " From tbl_ownershipotp_verify";
            //echo $_InsertQuery;
            
            $_DuplicateQuery = "Select * From tbl_ownership_change Where Org_Mobile='" . $_MobNo . "' OR Org_Email = '" . $_EmailId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            
            
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_MobNo, $_SMS);
			   $Admin_Name='RKCL';
			   	$mail_data = array();
			$mail_data['email'] = $_EmailId;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_EmailMsg;
			$subject = "OTP for Validating Email on MYRKCL";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
				//sendMail("".$_Email."", "".$Admin_Name."", $_SMS, $_SMS, "RKCL Support");
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Verify($_OTPEmail, $_OTPMobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OTPMobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_OTPMobile);
				
            $_SelectQuery = "Select * FROM tbl_ownershipotp_verify WHERE OWNERSHIP_OTPEmail='" . $_OTPEmail . "' AND OWNERSHIP_OTPSMS = '" . $_OTPMobile . "' "
                    . "AND OWNERSHIP_Status = '0' and NOW() <= DATE_ADD(OWNERSHIP_Start_Date, INTERVAL 30 MINUTE)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid Verification Details. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_ownershipotp_verify set OWNERSHIP_Status='1' WHERE OWNERSHIP_OTPEmail='" . $_OTPEmail . "' AND OWNERSHIP_OTPSMS = '" . $_OTPMobile . "' AND OWNERSHIP_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetDetails($_actionvalue)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select * from tbl_rajbank_master Where ifsccode= '" . $_actionvalue . "' Limit 1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

    
}
