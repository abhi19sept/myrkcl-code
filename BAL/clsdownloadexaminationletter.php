<?php

/**
 * Description of clsdownloadexaminationletter
 *
 * @author Anoop
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response2 = array();

class clsdownloadexaminationletter {

    //put your code here
    public function FillExamEvent() {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Event_Name,Event_Id From tbl_events as a inner join tbl_exammaster as b 
							on a.Event_Id=b.Affilate_Event where b.Event_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
