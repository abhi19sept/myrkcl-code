<?php

/**
 * Description of clsPaymentSummary
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response1 = array();
$_Response = array();

class clsPaymentSummary {

    //put your code here
    public function Show($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
				$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
				
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {

                //echo $_SelectQuery2 = "Select sum(Trans_Summary_Amount) as totalamount, Trans_Summary_ProdInfo From tbl_pay_trans_summary  WHERE Trans_Summary_Status like 'success' AND Trans_Summary_Timestamp >= '" . $_StartDate . "' AND Trans_Summary_Timestamp <= '" . $_EndDate . "' group by Trans_Summary_ProdInfo";

                $_SelectQuery2 = "SELECT (select SUM(`Admission_Transaction_Amount`) from tbl_admission_transaction where Admission_Transaction_Status like 'success' AND
                Admission_Transaction_timestamp >= '" . $_StartDate . "' AND Admission_Transaction_timestamp <= '" . $_EndDate . "' AND (
                Admission_Payment_Mode in ('','online','VerifyApi') or Admission_Payment_Mode IS NULL)) AS Total_Online_Learner_Payment,
                (select SUM(`Admission_Transaction_Amount`) from tbl_admission_transaction where Admission_Transaction_Status like 'success' AND
                Admission_Transaction_timestamp >= '" . $_StartDate . "' AND Admission_Transaction_timestamp <= '" . $_EndDate . "' AND
                Admission_Payment_Mode='DD Mode') AS Total_DD_Learner_Payment,

                (select SUM(`Correction_Transaction_Amount`) from tbl_correction_transaction where Correction_Transaction_Status like 'success' AND
                 Correction_Transaction_timestamp >= '" . $_StartDate . "' AND Correction_Transaction_timestamp <= '" . $_EndDate . "' AND (
                 Correction_Payment_Mode in ('','online','VerifyApi') or Correction_Payment_Mode IS NULL)) AS Total_Online_Correction_Payment,
                (select SUM(`Correction_Transaction_Amount`) from tbl_correction_transaction where Correction_Transaction_Status like 'success' AND
                 Correction_Transaction_timestamp >= '" . $_StartDate . "' AND Correction_Transaction_timestamp <= '" . $_EndDate . "' AND
                 Correction_Payment_Mode='DD Mode') AS Total_DD_Correction_Payment,

                (select SUM(`EOI_Transaction_Amount`) from tbl_eoi_transaction where EOI_Transaction_Status like 'success' AND
                       EOI_Transaction_timestamp >= '" . $_StartDate . "' AND EOI_Transaction_timestamp <= '" . $_EndDate . "' AND (
                       EOI_Payment_Mode in ('','online') or EOI_Payment_Mode IS NULL)) AS Total_Online_EOI_Payment,
                (select SUM(`EOI_Transaction_Amount`) from tbl_eoi_transaction where EOI_Transaction_Status like 'success' AND
                       EOI_Transaction_timestamp >= '" . $_StartDate . "' AND EOI_Transaction_timestamp <= '" . $_EndDate . "' AND
                       EOI_Payment_Mode ='DD Mode') AS Total_DD_EOI_Payment, 

                (select SUM(`Reexam_Transaction_Amount`) from tbl_reexam_transaction where Reexam_Transaction_Status like 'success' AND
                       Reexam_Transaction_timestamp >= '" . $_StartDate . "' AND Reexam_Transaction_timestamp <= '" . $_EndDate . "' AND (
                       Reexam_Payment_Mode in ('','online','VerifyApi') or Reexam_Payment_Mode IS NULL)) AS Total_Online_Reexam_Payment,
                (select SUM(`Reexam_Transaction_Amount`) from tbl_reexam_transaction where Reexam_Transaction_Status like 'success' AND
                       Reexam_Transaction_timestamp >= '" . $_StartDate . "' AND Reexam_Transaction_timestamp <= '" . $_EndDate . "' AND
                       Reexam_Payment_Mode ='DD Mode') AS Total_DD_Reexam_Payment, 

                (select SUM(`Ncr_Transaction_Amount`) from tbl_ncr_transaction where Ncr_Transaction_Status like 'success' AND
                       Ncr_Transaction_timestamp >= '" . $_StartDate . "' AND Ncr_Transaction_timestamp <= '" . $_EndDate . "' AND (
                       Ncr_Payment_Mode in ('','online') or Ncr_Payment_Mode IS NULL)) AS Total_Online_Ncr_Payment,
                (select SUM(`Ncr_Transaction_Amount`) from tbl_ncr_transaction where Ncr_Transaction_Status like 'success' AND
                       Ncr_Transaction_timestamp >= '" . $_StartDate . "' AND Ncr_Transaction_timestamp <= '" . $_EndDate . "' AND
                       Ncr_Payment_Mode='DD Mode') AS Total_DD_Ncr_Payment,

                (select SUM(`fld_amount`) from tbl_address_name_transaction where fld_Transaction_Status like 'success' AND
                       fld_updatedOn >= '" . $_StartDate . "' AND fld_updatedOn <= '" . $_EndDate . "' ) AS Total_Online_address_name_Payment,
                       '0' AS Total_DD_address_name_Payment,
                                                   
(select SUM(`RP_Transaction_Amount`) from tbl_renewal_penalty_transaction where RP_Transaction_Status like 'success' AND RP_Transaction_timestamp >= '" . $_StartDate . "' AND RP_Transaction_timestamp <= '" . $_EndDate . "' AND (
                       RP_Payment_Mode in ('','online') or RP_Payment_Mode IS NULL)) AS Total_Online_RP_Payment,
                (select SUM(`RP_Transaction_Amount`) from tbl_renewal_penalty_transaction where RP_Transaction_Status like 'success' AND
                       RP_Transaction_timestamp >= '" . $_StartDate . "' AND RP_Transaction_timestamp <= '" . $_EndDate . "' AND
                       RP_Payment_Mode='DD Mode') AS Total_DD_RP_Payment,
                                                   
(select SUM(`Ownership_Transaction_Amount`) from tbl_ownershipchange_transaction where Ownership_Transaction_Status like 'success' AND Ownership_Transaction_timestamp >= '" . $_StartDate . "' AND Ownership_Transaction_timestamp <= '" . $_EndDate . "' AND (
             Ownership_Payment_Mode in ('','online') or Ownership_Payment_Mode IS NULL)) AS Total_Online_OC_Payment,
      (select SUM(`Ownership_Transaction_Amount`) from tbl_ownershipchange_transaction where Ownership_Transaction_Status like 'success' AND
             Ownership_Transaction_timestamp >= '" . $_StartDate . "' AND Ownership_Transaction_timestamp <= '" . $_EndDate . "' AND
             Ownership_Payment_Mode='DD Mode') AS Total_DD_OC_Payment,
                                                   
(select SUM(`Aadhar_Transaction_Amount`) from tbl_aadhar_upd_transaction where Aadhar_Transaction_Status like 'success' AND  Aadhar_Transaction_timestamp >= '" . $_StartDate . "' AND Aadhar_Transaction_timestamp <= '" . $_EndDate . "' AND (
           Aadhar_Payment_Mode in ('','online') or Aadhar_Payment_Mode IS NULL)) AS Total_Online_Aadhar_Payment,
    (select SUM(`Aadhar_Transaction_Amount`) from tbl_aadhar_upd_transaction where Aadhar_Transaction_Status like 'success' AND
           Aadhar_Transaction_timestamp >= '" . $_StartDate . "' AND Aadhar_Transaction_timestamp <= '" . $_EndDate . "' AND
           Aadhar_Payment_Mode='DD Mode') AS Total_DD_Aadhar_Payment,
                                                   
(select SUM(`ExpCenter_Transaction_Amount`) from tbl_redhat_expcenter_transaction where ExpCenter_Transaction_Status like 'success' AND ExpCenter_Transaction_timestamp >= '" . $_StartDate . "' AND ExpCenter_Transaction_timestamp <= '" . $_EndDate . "' AND (
             ExpCenter_Payment_Mode in ('','online') or ExpCenter_Payment_Mode IS NULL)) AS Total_Online_RHExpCenter_Payment,
      (select SUM(`ExpCenter_Transaction_Amount`) from tbl_redhat_expcenter_transaction where ExpCenter_Transaction_Status like 'success' AND
             ExpCenter_Transaction_timestamp >= '" . $_StartDate . "' AND ExpCenter_Transaction_timestamp <= '" . $_EndDate . "' AND
             ExpCenter_Payment_Mode='DD Mode') AS Total_DD_RHExpCenter_Payment,

             (select SUM(`Rscfa_Certificate_Transaction_Amount`) from tbl_apply_rscfa_certificate_transaction where Rscfa_Certificate_Transaction_Status like 'success' AND Rscfa_Certificate_Transaction_timestamp >= '" . $_StartDate . "' AND Rscfa_Certificate_Transaction_timestamp <= '" . $_EndDate . "' AND (
             Rscfa_Certificate_Payment_Mode in ('','online') or Rscfa_Certificate_Payment_Mode IS NULL)) AS Total_Online_RSCFACert_Payment,
      (select SUM(`Rscfa_Certificate_Transaction_Amount`) from tbl_apply_rscfa_certificate_transaction where Rscfa_Certificate_Transaction_Status like 'success' AND
             Rscfa_Certificate_Transaction_timestamp >= '" . $_StartDate . "' AND Rscfa_Certificate_Transaction_timestamp <= '" . $_EndDate . "' AND
             Rscfa_Certificate_Payment_Mode='DD Mode') AS Total_DD_RSCFACert_Payment,

             (select SUM(`Rscfa_ReExam_Transaction_Amount`) from tbl_rscfa_reexam_transaction where Rscfa_ReExam_Transaction_Status like 'success' AND Rscfa_ReExam_Transaction_timestamp >= '" . $_StartDate . "' AND Rscfa_ReExam_Transaction_timestamp <= '" . $_EndDate . "' AND (
             Rscfa_ReExam_Payment_Mode in ('','online') or Rscfa_ReExam_Payment_Mode IS NULL)) AS Total_Online_RSCFAreexam_Payment,
      (select SUM(`Rscfa_ReExam_Transaction_Amount`) from tbl_rscfa_reexam_transaction where Rscfa_ReExam_Transaction_Status like 'success' AND
             Rscfa_ReExam_Transaction_timestamp >= '" . $_StartDate . "' AND Rscfa_ReExam_Transaction_timestamp <= '" . $_EndDate . "' AND
             Rscfa_ReExam_Payment_Mode='DD Mode') AS Total_DD_RSCFAreexam_Payment";
            }
//            else {
//
//                echo $_SelectQuery2 = "Select sum(Trans_Summary_Amount), Trans_Summary_ProdInfo From tbl_pay_trans_summary  WHERE Trans_Summary_Status like 'success' AND Trans_Summary_Timestamp >= '" . $_StartDate . "' AND Trans_Summary_Timestamp <= '" . $_EndDate . "' group by Trans_Summary_ProdInfo";
//            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

    public function ShowCategory($sdate, $edate, $headname, $mode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_LoginRole = $_SESSION['User_UserRoll'];
				$sdate = mysqli_real_escape_string($_ObjConnection->Connect(),$sdate);
				$edate = mysqli_real_escape_string($_ObjConnection->Connect(),$edate);
				$headname = mysqli_real_escape_string($_ObjConnection->Connect(),$headname);
				$mode = mysqli_real_escape_string($_ObjConnection->Connect(),$mode);
            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
                $_loginflag = "5";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
                $_loginflag = "6";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {

                //echo $_SelectQuery2 = "Select sum(Trans_Summary_Amount) as totalamount, Trans_Summary_ProdInfo From tbl_pay_trans_summary  WHERE Trans_Summary_Status like 'success' AND Trans_Summary_Timestamp >= '" . $_StartDate . "' AND Trans_Summary_Timestamp <= '" . $_EndDate . "' group by Trans_Summary_ProdInfo";
                if ($headname == "Admission_Payment") {
                    if ($mode == 'ShowOnline') {
                        $_SelectQuery2 = "select d.Course_Name as coursename, c.Batch_Name as batchname, sum(b.Admission_Transaction_Amount)  as totalamount from tbl_admission_transaction as b inner join tbl_course_master as d on d.Course_Code = b.Admission_Transaction_Course inner join tbl_batch_master as c on c.Batch_Code = b.Admission_Transaction_Batch where 
                          b.Admission_Transaction_Status like 'success' AND b.Admission_Transaction_timestamp >= '" . $sdate . "' AND 
                          b.Admission_Transaction_timestamp <= '" . $edate . "' AND ( Admission_Payment_Mode in ('','online','VerifyApi') or Admission_Payment_Mode IS NULL)
                          group by b.Admission_Transaction_Course,b.Admission_Transaction_Batch";
                    } else if ($mode == 'ShowDD') {
                        $_SelectQuery2 = "select d.Course_Name as coursename, c.Batch_Name as batchname, sum(b.Admission_Transaction_Amount)
                          as totalamount from tbl_admission_transaction as b inner join tbl_course_master as d on d.Course_Code = b.Admission_Transaction_Course 
                          inner join tbl_batch_master as c on c.Batch_Code = b.Admission_Transaction_Batch where 
                          b.Admission_Transaction_Status like 'success' AND b.Admission_Transaction_timestamp >= '" . $sdate . "' AND 
                          b.Admission_Transaction_timestamp <= '" . $edate . "' AND Admission_Payment_Mode='DD Mode'
                          group by b.Admission_Transaction_Course,b.Admission_Transaction_Batch";
                    }
                } elseif ($headname == "EOI_Payment") {
                    $_SelectQuery2 = "select EOI_Name, sum(EOI_Transaction_Amount) as totaleoiamount from tbl_eoi_transaction 
                                    as a inner join tbl_eoi_master as b on a.EOI_Transaction_ProdInfo=b.EOI_Code where EOI_Transaction_Status like 'success' and
                                    EOI_Transaction_timestamp >= '" . $sdate . "' AND EOI_Transaction_timestamp <= '" . $edate . "' group by EOI_Transaction_ProdInfo";
                } elseif ($headname == "Correction_Payment") {
                    $_SelectQuery2 = "select d.Course_Name as coursename, c.Batch_Name as batchname, sum(b.Admission_Transaction_Amount) as totalamount from tbl_admission as a inner join 
                                    tbl_admission_transaction as b on a.Admission_TranRefNo = b.Admission_Transaction_Txtid inner join tbl_course_master as d
                                    on d.Course_Code = a.Admission_Course inner join tbl_batch_master as c on c.Batch_Code = a.Admission_Batch where 
                                    b.Admission_Transaction_Status like 'success' AND b.Admission_Transaction_timestamp >= '" . $sdate . "' AND 
                                    b.Admission_Transaction_timestamp <= '" . $edate . "' group by a.Admission_Course, a.Admission_Batch ";
                } elseif ($headname == "Reexam_Payment") {
                    $_SelectQuery2 = "select Reexam_Transaction_ProdInfo, sum(Reexam_Transaction_Amount) as totalreexamamount from tbl_reexam_transaction where Reexam_Transaction_Status like 'success' and
                                    Reexam_Transaction_timestamp >= '" . $sdate . "' AND Reexam_Transaction_timestamp <= '" . $edate . "' group by Reexam_Transaction_ProdInfo";
                } elseif ($headname == "NCR_Payment") {
                    $_SelectQuery2 = "select Ncr_Transaction_CenterCode, sum(Ncr_Transaction_Amount) as totalncramount from tbl_ncr_transaction where 
                                        Ncr_Transaction_Status like 'success' and Ncr_Transaction_timestamp >= '" . $sdate . "' AND 
                                        Ncr_Transaction_timestamp <= '" . $edate . "' group by Ncr_Transaction_CenterCode";
                } elseif ($headname == "address_name_Payment") {
                    $_SelectQuery2 = "select fld_ITGK_Code, sum(fld_amount) as totaladdress_name_Payment from tbl_address_name_transaction where 
                                        fld_Transaction_Status like 'success' and fld_updatedOn >= '" . $sdate . "' AND 
                                        fld_updatedOn <= '" . $edate . "' group by fld_ITGK_Code";
                } elseif ($headname == "renewal_penalty_Payment") {
                    $_SelectQuery2 = "select RP_Transaction_CenterCode, sum(RP_Transaction_Amount) as totalamount from tbl_renewal_penalty_transaction where 
                                        RP_Transaction_Status like 'success' and RP_Transaction_timestamp >= '" . $sdate . "' AND 
                                        RP_Transaction_timestamp <= '" . $edate . "' group by RP_Transaction_CenterCode";
                } elseif ($headname == "owner_change_Payment") {
                    $_SelectQuery2 = "select Ownership_Transaction_CenterCode, sum(Ownership_Transaction_Amount) as totalamount from tbl_ownershipchange_transaction where 
                                        Ownership_Transaction_Status like 'success' and Ownership_Transaction_timestamp >= '" . $sdate . "' AND 
                                        Ownership_Transaction_timestamp <= '" . $edate . "' group by Ownership_Transaction_CenterCode";
                } elseif ($headname == "adm_correction_aadhar_Payment") {
                    $_SelectQuery2 = "select Aadhar_Transaction_CenterCode, sum(Aadhar_Transaction_Amount) as totalamount from tbl_aadhar_upd_transaction where 
                                        Aadhar_Transaction_Status like 'success' and Aadhar_Transaction_timestamp >= '" . $sdate . "' AND 
                                        Aadhar_Transaction_timestamp <= '" . $edate . "' group by Aadhar_Transaction_CenterCode";
                } elseif ($headname == "RHExpCenter_Payment") {
                    $_SelectQuery2 = "Select a.*, b.* from tbl_redhat_expcenter_transaction as a inner join tbl_redhat_exp_center as b "
                            . " on a.ExpCenter_Transaction_Txtid=b.exp_center_txnid WHERE "
                            . "a.ExpCenter_Transaction_timestamp >= '" . $sdate . "' AND a.ExpCenter_Transaction_timestamp <= '" . $edate . "' AND "
                            . " a.ExpCenter_Transaction_Status = 'Success'";
                }
                elseif ($headname == "RSCFACert_Payment") {
                    $_SelectQuery2 = "Select a.*, b.* from tbl_apply_rscfa_certificate_transaction as a inner join tbl_apply_rscfa_certificate as b "
                            . " on a.Rscfa_Certificate_Transaction_Txtid=b.apply_rscfa_cert_txnid WHERE "
                            . "a.Rscfa_Certificate_Transaction_timestamp >= '" . $sdate . "' AND a.Rscfa_Certificate_Transaction_timestamp <= '" . $edate . "' AND "
                            . " a.Rscfa_Certificate_Transaction_Status = 'Success'";
                }
                elseif ($headname == "RSCFAReexam_Payment") {
                    $_SelectQuery2 = "Select a.*, b.* from tbl_rscfa_reexam_transaction as a inner join tbl_rscfa_reexam_application as b "
                            . " on a.Rscfa_ReExam_Transaction_Txtid=b.rscfa_reexam_TranRefNo WHERE "
                            . "a.Rscfa_ReExam_Transaction_timestamp >= '" . $sdate . "' AND a.Rscfa_ReExam_Transaction_timestamp <= '" . $edate . "' AND "
                            . " a.Rscfa_ReExam_Transaction_Status = 'Success'";
                }
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }

}
