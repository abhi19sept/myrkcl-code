<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNewCenterRegistration
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');

$_ObjConnection = new _Connection();
$_Response = array();

date_default_timezone_set('Asia/Calcutta');

class clsNewCenterRegistration {
    //put your code here
    
    public function Add($_GeneratedId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_State, $_Region,
            $_District, $_Tehsil, $_Landmark, $_Road, $_PinCode, $_OrgEstDate, $_OrgCountry, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename, $_doctype, 
            $_PanNo, $_AADHARNo, $_AreaType, $_MunicipalType, $_WardNo, $_Police, $_MunicipalName, $_Village, $_Gram, $_Panchayat) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_RspLoginId = $_SESSION['User_LoginId'];
        $_RspCode = $_SESSION['User_Code'];
        $_Date = date("Y-m-d h:i:s");
		$_OrgMobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgMobile);
		
        $_OrgAOType = '22';
        if ($_OrgAOType == '14') {
            $type = 'RKCL Service Provider';
        } elseif ($_OrgAOType == '22') {
            $type = 'IT-GK';
        }
        $_SMS = "Dear Applicant, " . $_OrgName . " Your Application for " . $type . " has been Submitted to RKCL. Once your Submitted Information is Verified, you will get your MYRKCL temporary login Details on your Registered Mobile Number.";
        $_SMSRSP = "Dear SP, IT-GK " . $_OrgName . " has been registered on MYRKCL. Kindly Verify and Approve Submitted Information. MYRKCL temporary login Details will be sent to IT-GK Registered Mobile Number.";
        
        $_Email = "Dear Applicant, " . $_OrgName . " Your Application for " . $type . " has been Submitted to RKCL. Once your Submitted Information is Verified, you will get your MYRKCL temporary login Details on your Registered Mobile Number.";
        $_EmailRSP = "Dear SP, IT-GK " . $_OrgName . " has been registered on MYRKCL. Kindly Verify and Approve Submitted Information. MYRKCL temporary login Details will be sent to IT-GK Registered Mobile Number.";
        
        try {

            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {    
                $_SelectQuery = "Select * FROM tbl_user_master WHERE User_LoginId = '" . $_RspLoginId . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Row = mysqli_fetch_array($_Response1[2]);                
                $_RspMobile = $_Row['User_MobileNo'];
                $_RspEmail = $_Row['User_EmailId'];                

                $_InsertQuery = "Insert Into tbl_org_master(Organization_Code,Org_Ack,Org_RspCode,Org_RspLoginId,Organization_Name,"
                        . "Organization_RegistrationNo,Organization_FoundedDate,Organization_Type,"
                        . "Organization_DocType,Organization_ScanDoc,Organization_UID,Organization_AddProof,Organization_AppForm,Organization_TypeDocId,"
                        . "Organization_State,Organization_Region,Organization_District,"
                        . "Organization_Tehsil,Organization_Landmark,Organization_Road,"
                        . "Organization_Country,"
                        . "Org_NCR_App_Date,Org_Role,Org_Email,Org_Mobile,Org_PAN,Org_AADHAR,Org_AreaType,Org_Municipal_Type,Org_WardNo,Org_Police,"
                        . "Org_Municipal,Org_Village,Org_Gram,Org_Panchayat,Org_PinCode) "
                        . "Select Case When Max(Organization_Code) Is Null Then 1 Else Max(Organization_Code)+1 End as Organization_Code,"
                        . " Case When Max(Org_Ack) Is Null Then 4000 Else Max(Org_Ack)+1 End as Org_Ack,"
                        . "'" . $_RspCode . "' as Org_RspCode,'" . $_RspLoginId . "' as Org_RspLoginId,'" . $_OrgName . "' as Organization_Name,'" . $_OrgRegno . "' as Organization_RegistrationNo,"
                        . "'" . $_OrgEstDate . "' as Organization_FoundedDate,  '" . $_OrgType . "' as Organization_Type,"
                        . "'" . $_doctype . "' as Organization_DocType,'" . $PANfilename . "' as Organization_ScanDoc,"
                        . "'" . $UIDfilename . "' as Organization_UID,'" . $addprooffilename . "' as Organization_AddProof,"
                        . "'" . $appformfilename . "' as Organization_AppForm,'" . $_GeneratedId . "' as Organization_TypeDocId,"
                        . "'" . $_State . "' as Organization_State,'" . $_Region . "' as Organization_Region,'" . $_District . "' as Organization_District,"
                        . "'" . $_Tehsil . "' as Organization_Tehsil,"
                        . "'" . $_Landmark . "' as Organization_Landmark,'" . $_Road . "' as Organization_Road,"                        
                        . "'" . $_OrgCountry . "' as Organization_Country,"                       
                        . "'" . $_Date . "' as Org_NCR_App_Date,'". $_OrgAOType . "' as Org_Role,'" . $_OrgEmail . "' as Org_Email,'" . $_OrgMobile . "' as Org_Mobile,"
                        . "'" . $_PanNo . "' as Org_PAN,'". $_AADHARNo . "' as Org_AADHAR,'" . $_AreaType . "' as Org_AreaType,"
                        . "'" . $_MunicipalType . "' as Org_Municipal_Type,'" . $_WardNo . "' as Org_WardNo,"
                        . "'" . $_Police . "' as Org_Police,"
                        . "'" . $_MunicipalName . "' as Org_Municipal,'" . $_Village . "' as Org_Village,"
                        . "'" . $_Gram . "' as Org_Gram,'" . $_Panchayat . "' as Org_Panchayat,'" . $_PinCode . "' as Org_PinCode"
                        . " From tbl_org_master";

                $_DuplicateQuery = "Select * From tbl_org_master Where Org_Mobile='" . $_OrgMobile . "' OR Org_Email = '" . $_OrgEmail . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                $_DuplicateQuery1 = "Select * From tbl_user_master Where User_MobileNo='" . $_OrgMobile . "' OR User_EmailId = '" . $_OrgEmail . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);


                if ($_Response[0] == Message::NoRecordFound && $_Response1[0] == Message::NoRecordFound) {
                        if($_AreaType == 'Urban') {
					if($_MunicipalName == '0' || $_MunicipalType == '0' || $_WardNo == '0') {
						 echo " Please select all fields";
						 return;
					}
                        }
                        elseif($_AreaType == 'Rural') {
                         if($_Gram =='00' || $_Panchayat =='0' || $_Village =='0') {
						 echo " Please select all fields";
						 return;
					}
                        }
					
                        $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                        SendSMS($_OrgMobile, $_SMS);
                        SendSMS($_RspMobile, $_SMSRSP);
                        
                        $Admin_Name='RKCL';
			$mail_data = array();
			$mail_data['email'] = $_OrgEmail;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_Email;
			$subject = "Registration Successfulll on MYRKCL";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
                        
                        $Admin_Name='RKCL';
			$mail_data = array();
			$mail_data['email'] = $_RspEmail;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_EmailRSP;
			$subject = "Registration Successfulll on MYRKCL";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
                                        
                } else {
                    echo " Mobile Number or Email Id Already Exists.";
                    return;                     
                }
                } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
    
    public function GenerateOTP($_EmailId,$_MobNo) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
		$_MobNo = mysqli_real_escape_string($_ObjConnection->Connect(),$_MobNo);
		
        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTPSMS = OTP();
        $_SMS = "For validating Mobile Number your OTP is $_OTPSMS in MYRKCL. Rajasthan Knowledge Corporation Limited ";
        
        $_OTPEMAIL = OTP();
        $_EmailMsg = "OTP for Validating Email on MYRKCL is " . $_OTPEMAIL;		

        try {
           
            $_InsertQuery = "Insert Into tbl_ncrotp_verify(NCROTP_Code,NCROTP_Mobile,NCROTP_Email,NCROTP_OTPSMS,NCROTP_OTPEmail)"
                    . "Select Case When Max(NCROTP_Code) Is Null Then 1 Else Max(NCROTP_Code)+1 End as NCROTP_Code,"
                    . "'" .  $_MobNo . "' as NCROTP_Mobile,'" . $_EmailId . "' as NCROTP_Email,"
                    . "" . $_OTPSMS . " as NCROTP_OTPSMS,'" . $_OTPEMAIL . "' as NCROTP_OTPEmail"
                    . " From tbl_ncrotp_verify";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_org_master Where Org_Mobile='" . $_MobNo . "' OR Org_Email = '" . $_EmailId . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            
            $_DuplicateQuery1 = "Select * From tbl_user_master Where User_MobileNo='" . $_MobNo . "' OR User_EmailId = '" . $_EmailId . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound && $_Response1[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_MobNo, $_SMS);
			   $Admin_Name='RKCL';
			   	$mail_data = array();
			$mail_data['email'] = $_EmailId;
			$mail_data['admin'] = $Admin_Name;
						
			$mail_data['Msg'] = $_EmailMsg;
			$subject = "OTP for Validating Email on MYRKCL";						
			$sendMail = new sendMail();
			$sendMail->sendEmail($mail_data['email'], $mail_data['admin'], $subject, $mail_data['Msg']);
				//sendMail("".$_Email."", "".$Admin_Name."", $_SMS, $_SMS, "RKCL Support");
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Verify($_OTPEmail, $_OTPMobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_OTPMobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_OTPMobile);
				
            $_SelectQuery = "Select * FROM tbl_ncrotp_verify WHERE NCROTP_OTPEmail='" . $_OTPEmail . "' AND NCROTP_OTPSMS = '" . $_OTPMobile . "' AND NCROTP_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid Verification Details. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_ncrotp_verify set NCROTP_Status='1' WHERE NCROTP_OTPEmail='" . $_OTPEmail . "' AND NCROTP_OTPSMS = '" . $_OTPMobile . "' AND NCROTP_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}
