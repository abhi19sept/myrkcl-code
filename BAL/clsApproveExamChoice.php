<?php
/**
 * Description of clsApproveExamChoice
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsApproveExamChoice {
    //put your code here
    
    public function Updatetemp() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				
           $_UpdateQuery = "UPDATE tbl_examchoicemaster_temp SET status = 'Approve' WHERE status = 'pending'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	 public function Updatechoice() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				
           $_UpdateQuery = "UPDATE tbl_examchoicemaster SET status = 'Approve' WHERE status = 'pending'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
