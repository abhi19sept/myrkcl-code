<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCorrectionUpdateStatus {
    //put your code here  
	
	public function FILLOTFORUPDATESTATUS() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT lotid, lotname FROM tbl_clot where correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function FILLSTATUSFORUPDATE() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_capcategory where correctionrole='Yes'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function ShowDetailsToUpdate($_Status,$_Lotid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				$_Lotid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lotid);
				
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select * FROM `tbl_correction_copy` where lot='" . $_Lotid . "' AND dispatchstatus='" . $_Status . "' AND Correction_Payment_Status='1'";
			}
			else{
				$_SelectQuery = "Select * FROM `tbl_correction_copy` where lot='" . $_Lotid . "' AND dispatchstatus='" . $_Status . "' AND Correction_Payment_Status='1'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function ShowDetailsToDownload($_Status,$_Lotid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				$_Lotid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lotid);
				
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select * FROM `tbl_correction_copy` where lot='" . $_Lotid . "' AND dispatchstatus='" . $_Status . "'";
			}
			else{
				$_SelectQuery = "Select * FROM `tbl_correction_copy` where lot='" . $_Lotid . "' AND dispatchstatus='" . $_Status . "'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function Update_LearnerStatus($_CorrectionId, $_Update_Status) {
         //print_r($_LearnerCode);
        global $_ObjConnection;
        $_ObjConnection->Connect();
         try { 
					$_CorrectionId = mysqli_real_escape_string($_ObjConnection->Connect(),$_CorrectionId);
					$_Update_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Update_Status);
					
              $_UpdateQuery = "Update tbl_correction_copy set dispatchstatus='" . $_Update_Status . "' WHERE cid IN ($_CorrectionId)"; 
			  $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);                     
        }        
        catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	public function GetLearnerPhoto($_Status,$_Lotid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				$_Lotid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Lotid);
				
			if($_SESSION['User_UserRoll'] == 1){
				$_SelectQuery = "Select photo FROM `tbl_correction_copy` where lot='" . $_Lotid . "' AND
								dispatchstatus='" . $_Status . "'";
			}
			else{
				$_SelectQuery = "Select photo FROM `tbl_correction_copy` where lot='" . $_Lotid . "' AND
				dispatchstatus='" . $_Status . "'";
			}		
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
 
}
