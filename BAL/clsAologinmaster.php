<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPremisesDetail
 *
 *  author Viveks
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response1 = array();

class clsAologinmaster {

    //put your code here


    public function Add($_Email, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
     //   $_SMS = "OTP for your AO registration is " . $_OTP;
		$_SMS = $_OTP. " is the OTP for your AO registration.";


        try {
            $_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email);
            $_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
            $_InsertQuery = "Insert Into tbl_ao_register(AO_Email,AO_Mobile,AO_OTP) VALUES('" . $_Email . "', '" . $_Mobile . "', '" . $_OTP . "')";
            //echo $_InsertQuery;            
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            SendSMS($_Mobile, $_SMS);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function AddWomen($_Email, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        //$_SMS = "OTP for your AO registration is " . $_OTP;
        $_SMS = $_OTP. " is the OTP for your application in Indira Mahila Shakti Prashikshan Evam Koushal Sanvardhan Yojana. ";


        try {
            $_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
            $_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email);
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Mobile = '" . $_Mobile . "' AND AO_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response1);
            if ($_Response1[0] == 'Success') 
                {
                $_Row = mysqli_fetch_array($_Response1[2]);
                $old_OTP=$_Row['AO_OTP'];
                $_OLD_SMS = $old_OTP. " is the OTP for your application in Indira Mahila Shakti Prashikshan Evam Koushal Sanvardhan Yojana. ";
                SendSMS($_Mobile, $_OLD_SMS);
                return $_Response1;
                }
            else{
                $_InsertQuery = "Insert Into tbl_ao_register(AO_Email,AO_Mobile,AO_OTP) VALUES('" . $_Email . "', '" . $_Mobile . "', '" . $_OTP . "')";
            //echo $_InsertQuery;            
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
                }
            
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

	 public function verifymobilenumber($_mobile) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
            $_mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_mobile);
            $_SelectQuery = "Select * FROM tbl_oasis_admission WHERE Oasis_Admission_Mobile = '" . $_mobile . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                       
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	

    public function verifyotp($_mobile, $_otp) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
            $_mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_mobile);
            $_otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_otp);
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Mobile = '" . $_mobile . "' AND AO_OTP = '" . $_otp . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response[0]);
            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid OTP. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Mobile='" . $_mobile . "' AND AO_OTP = '" . $_otp . "' AND AO_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_ao_register WHERE AO_Mobile='" . $_mobile . "' AND AO_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
				$_SESSION['OTP_Mobile'] = $_mobile;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Unlink() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select id,status From tbl_ncrstatus";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
