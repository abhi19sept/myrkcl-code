<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */
require 'DAL/classconnectionNEW.php';
require 'common/payments.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAadharUpdFee extends paymentFunctions {

    //put your code here
    public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //   $_SelectQuery = "Select a.Event_Batch AS Batch_Code, b.Batch_Name From tbl_event_management AS a INNER JOIN tbl_batch_master AS b ON a.Event_Batch = b.Batch_Code WHERE a.Event_Course = '".$course."' AND CURDATE() >= Event_Startdate AND CURDATE() <= Event_Enddate AND Event_Name='3' AND Event_Payment='1'";
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "' ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($_SESSION['User_UserRoll'] == '7') {
                $_SelectQuery = "Select distinct a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' AND `Courseitgk_ITGK` = '" . $_SESSION['User_LoginId'] . "' ORDER BY `CourseITGK_StartDate` ASC";
            } else {
                $_SelectQuery = "Select distinct a.Courseitgk_Course, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' ORDER BY `CourseITGK_StartDate` ASC";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function ChkPayEvent($ccode,$bcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $ccode = mysqli_real_escape_string($_ObjConnection->Connect(),$ccode);
            $bcode = mysqli_real_escape_string($_ObjConnection->Connect(),$bcode);
            $_SelectQuery = "Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id WHERE Event_Name='15' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate and Event_Course='".$ccode."' AND Event_Batch='".$bcode."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
    public function GetAll($batch, $course, $paymode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
            $course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
            $paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$paymode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_ITGK = $_SESSION['User_LoginId'];
                $_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE  Event_Payment = '" . $paymode . "' AND Event_Name = '15'  AND Event_Course='".$course."' AND Event_Batch='".$batch."' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
                $_ResponseGetEvent1 = $_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
                $_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
                if ($_getEvent['Event_Payment'] == '1') {
                    $_SelectQuery = "SELECT * FROM tbl_adm_log_foraadhar WHERE Adm_Log_ITGK_Code = '" . $_ITGK . "' AND Adm_Log_Batch='".$batch."' and Adm_Log_Course='".$course."' and Adm_Log_ApproveStatus = '0' AND Adm_Log_PayStatus='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }  else {
                    //echo "Invalid User Input";
                    return;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Aadhar_Fee_Amount FROM tbl_aadhar_upd_fee_master WHERE Aadhar_Fee_Status = 'Active'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function AddPayTran($postValues, $productinfo) {
        $return = 0;
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $return = parent::insertPaymentTransaction($productinfo, $_SESSION['User_LoginId'], $postValues['amounts'], $postValues['Adm_Log_AdmissionCode'], $postValues['ddlCourse'], 0, $postValues['ddlBatch']);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }

        return $return;
    }

    public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
