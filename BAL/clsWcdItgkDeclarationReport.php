<?php

/**
 * Description of clsWcdItgkDeclarationReport
 *
 // * @author Mayank
 */
require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsWcdItgkDeclarationReport {

    //put your code here
	public function GetAdmissionCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('3','24')
							order by Course_Code ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
    public function GetAdmissionBatchcode($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
				$_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $course . "'
								and Batch_Code>'293' order by Batch_Code";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
	
    public function GetDataAll($_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
				$_LoginRole = $_SESSION['User_UserRoll'];
				
				$_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
				$_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
				
				if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8') {
					if($_course=='3'){
						$_SelectQuery = "select wcd_itgk_declare_id,wcd_itgk_declare_course_name as Courseitgk_Course,
								wcd_itgk_declare_itgkcode as Courseitgk_ITGK,ITGKMOBILE,District_Name,
								ITGK_Name,RSP_Code,RSP_Name,
								(case when a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk 
								and wcd_declare_course='3' then 'Submitted' else 'Not Submitted' end) as status
								from tbl_wcd_itgk_declaration_master as a inner join vw_itgkname_distict_rsp as b
								on a.wcd_itgk_declare_itgkcode=b.ITGKCODE left join tbl_wcd_itgk_declaration as c
								on a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk and c.wcd_declare_eoi=a.wcd_itgk_declare_eoi
								where wcd_itgk_declare_eoi='32' and wcd_itgk_declare_course_name='RS-CIT Women'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					}
					else{
						$_SelectQuery = "select wcd_itgk_declare_id,wcd_itgk_declare_course_name as Courseitgk_Course,
								wcd_itgk_declare_itgkcode as Courseitgk_ITGK,ITGKMOBILE,District_Name,
								ITGK_Name,RSP_Code,RSP_Name,
								(case when a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk and wcd_declare_course='24'
								then 'Submitted' else 'Not Submitted' end) as status
								from tbl_wcd_itgk_declaration_master as a inner join vw_itgkname_distict_rsp as b
								on a.wcd_itgk_declare_itgkcode=b.ITGKCODE left join tbl_wcd_itgk_declaration as c
								on a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk and c.wcd_declare_eoi=a.wcd_itgk_declare_eoi
								where wcd_itgk_declare_eoi='33' and wcd_itgk_declare_course_name='RS-CFA Women'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					}					
				}
				
				elseif ($_LoginRole == '14') {
					if($_course=='3'){
						$_SelectQuery = "select wcd_itgk_declare_id,wcd_itgk_declare_course_name as Courseitgk_Course,
								wcd_itgk_declare_itgkcode as Courseitgk_ITGK,ITGKMOBILE,District_Name,
								ITGK_Name,RSP_Code,RSP_Name,
								(case when a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk 
								and wcd_declare_course='3' then 'Submitted' else 'Not Submitted' end) as status
								from tbl_wcd_itgk_declaration_master as a inner join vw_itgkname_distict_rsp as b
								on a.wcd_itgk_declare_itgkcode=b.ITGKCODE left join tbl_wcd_itgk_declaration as c
								on a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk and c.wcd_declare_eoi=a.wcd_itgk_declare_eoi
								where wcd_itgk_declare_eoi='32' and wcd_itgk_declare_course_name='RS-CIT Women'
								and RSP_Code='".$_SESSION['User_LoginId']."'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					}
					else{
						$_SelectQuery = "select wcd_itgk_declare_id,wcd_itgk_declare_course_name as Courseitgk_Course,
								wcd_itgk_declare_itgkcode as Courseitgk_ITGK,ITGKMOBILE,District_Name,
								ITGK_Name,RSP_Code,RSP_Name,
								(case when a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk and wcd_declare_course='24'
								then 'Submitted' else 'Not Submitted' end) as status
								from tbl_wcd_itgk_declaration_master as a inner join vw_itgkname_distict_rsp as b
								on a.wcd_itgk_declare_itgkcode=b.ITGKCODE left join tbl_wcd_itgk_declaration as c
								on a.wcd_itgk_declare_itgkcode=c.wcd_declare_itgk and c.wcd_declare_eoi=a.wcd_itgk_declare_eoi
								where wcd_itgk_declare_eoi='33' and wcd_itgk_declare_course_name='RS-CFA Women'
								and RSP_Code='".$_SESSION['User_LoginId']."'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					}		
				} 
			} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
}
