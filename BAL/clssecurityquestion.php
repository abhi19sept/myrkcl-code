<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clssecurityquestion {

    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_security_q ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_CourseType_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseType_Code);
				
            $_SelectQuery = "Select * From tbl_security_q Where SQ_ID='" . $_CourseType_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteRecord($_CourseType_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseType_Code);
				
            $_DeleteQuery = "Delete From tbl_security_q Where SQ_ID='" . $_CourseType_Code . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add($Security_Question,$Security_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$Security_Question = mysqli_real_escape_string($_ObjConnection->Connect(),$Security_Question);
				
            $_InsertQuery = "Insert Into tbl_security_q(`Security_Question`,`Security_Status`) values('" . $Security_Question . "', '".$Security_Status."')";
            $_DuplicateQuery = "Select * From tbl_security_q Where Security_Question='" . $Security_Question . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($SQ_ID, $Security_Question, $Security_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$SQ_ID = mysqli_real_escape_string($_ObjConnection->Connect(),$SQ_ID);
				
            $_UpdateQuery = "Update tbl_security_q set Security_Question='" . $Security_Question . "',Security_Status='" . $Security_Status . "' Where SQ_ID='" . $SQ_ID . "'";
            //$_DuplicateQuery = "Select * From tbl_security_q Where Security_Question='" . $Security_Question . "' and SQ_ID <> '" . $SQ_ID . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            //$_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //if ($_Response[0] == Message::NoRecordFound) {
             $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
//            } else {
//                $_Response[0] = Message::DuplicateRecord;
//                $_Response[1] = Message::Error;
//            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
?>
