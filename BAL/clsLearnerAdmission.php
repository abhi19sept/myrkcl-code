<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsLearnerAdmission
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsLearnerAdmission {
    //put your code here
    
    public function Add($_Email, $_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        // $_SMS = "OTP for your RS-CIT registration is " . $_OTP;
        $_SMS = "OTP is ".$_OTP." for your RS-CIT registration. Rajasthan Knowledge Corporation Limited";
        date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d H:i:s");

        try {
				$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
            $_InsertQuery = "Insert Into tbl_ao_register(AO_Code,AO_Email,AO_Mobile,AO_OTP,Otp_CreatedOn)"
                    . "Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,"
                    . "'" . $_Email . "' as AO_Email,'" . $_Mobile . "' as AO_Mobile,"
                    . "'" . $_OTP . "' as AO_OTP, '" . $_Date . "' as Otp_CreatedOn"
                    . " From tbl_ao_register";
            //echo $_InsertQuery;            
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            SendSMS($_Mobile, $_SMS);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function verifyotp($_mobile, $_otp) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
				$_mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_mobile);
				$_otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_otp);
				
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Mobile = '" . $_mobile . "' AND AO_OTP = '" . $_otp . "' and AO_Status='0' and NOW() <= DATE_ADD(Otp_CreatedOn, INTERVAL 5 MINUTE)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response[0]); 
            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid OTP. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Mobile='" . $_mobile . "' AND AO_OTP = '" . $_otp . "' AND AO_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_ao_register WHERE AO_Mobile='" . $_mobile . "' AND AO_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Unlink() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select id,status From tbl_ncrstatus";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
        public function FILLCenter($_Tehsil, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_Tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_Tehsil);
			$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
			
            $_SelectQuery = "Select DISTINCT a.Courseitgk_ITGK, c.Organization_Name, c.Organization_User, c.Organization_AreaType, c.Organization_Address 
                    From tbl_courseitgk_mapping as a INNER JOIN tbl_user_master as b 
                    ON a.Courseitgk_ITGK=b.User_LoginId INNER JOIN tbl_organization_detail as c 
                    ON b.User_Code=c.Organization_User  INNER JOIN tbl_course_master as d
                    ON a.Courseitgk_Course=d.Course_Name WHERE Organization_Tehsil= '" . $_Tehsil . "' 
                    AND CourseITGK_BlockStatus='unblock' AND CURDATE() <= `CourseITGK_ExpireDate` 
                    AND a.EOI_Fee_Confirm='1'AND d.Course_Code='" . $course . "' 
                    AND b.User_UserRoll = 7 ORDER BY a.Courseitgk_ITGK";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCenterDetails($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_User = $_SESSION['User_LoginId'];
			$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, a.Organization_Address, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.*"
                    . " FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code INNER JOIN tbl_tehsil_master as d"                    
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e"
                    . " ON a.Organization_User=e.User_Code WHERE Organization_User = '" . $_CenterCode . "' AND e.User_UserRoll = 7";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
