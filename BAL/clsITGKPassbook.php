<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsITGKPassbook
 *
 * @author Abhishek
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsITGKPassbook {
    //put your code here
    
    public function ShowByDate($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
            $_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '14') {

                $_LoginUserType = "RSPUserCode";
                $_loginflag = "14";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {

                // $_SelectQuery2 = "Select * From tbl_payment_refund WHERE Payment_Refund_Timestamp >= '" . $_StartDate . "' AND Payment_Refund_Timestamp <= '" . $_EndDate . "'";
            } 
            elseif ($_loginflag == "7") {


             $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Debit' as type, '' as rmk from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between if('" . $_StartDate . "' >= '2019-01-01 00:00:00', '" . $_StartDate . "', '2019-01-01 00:00:00') and '" . $_EndDate . "'
                           union 
             select User_Credit_Tran_UserLoginId, User_Credit_Tran_Timestamp  as paydate, User_Credit_Tran_ProdInfo, User_Credit_Tran_PGid, User_Credit_Tran_Amount, 'Credit' as type, User_Credit_Tran_Remark as rmk from tbl_user_credit_transaction where User_Credit_Tran_UserLoginId= '" . $_SESSION['User_LoginId'] . "' and  User_Credit_Tran_Timestamp between if('" . $_StartDate . "' >= '2019-01-01 00:00:00', '" . $_StartDate . "', '2019-01-01 00:00:00') and '" . $_EndDate . "' 
                order by paydate DESC";
            } 
            elseif ($_loginflag == "14") {

                // $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Credit' from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between '" . $_StartDate . "' and '" . $_EndDate . "' order by `timestamp` DESC";
            }


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            $date1=(date_create($_StartDate) >= date_create('2019-01-01 00:00:00') ? date_create($_StartDate) : date_create('2019-01-01 00:00:00'));
            $date2=date_create($_EndDate);
            $_Response[3] = date_format($date1, 'Y-m-d H:i:s');
            $_Response[4] = date_format($date2, 'Y-m-d H:i:s');
          
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
public function ShowByMonth($_Month, $_Year) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Month = mysqli_real_escape_string($_ObjConnection->Connect(),$_Month);
            $_Year = mysqli_real_escape_string($_ObjConnection->Connect(),$_Year);
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '14') {

                $_LoginUserType = "RSPUserCode";
                $_loginflag = "14";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {
// STR_TO_DATE($_Year.'-'.$_Month.'-01 00:00:00', '%Y-%m-%d H:i:s')
                // $_SelectQuery2 = "Select * From tbl_payment_refund WHERE Payment_Refund_Timestamp >= STR_TO_DATE('$_Year-$_Month-01 00:00:00', '%Y-%m-%d %H:%i:%s')  AND Payment_Refund_Timestamp <=  STR_TO_DATE('$_Year-$_Month-31 24:59:59', '%Y-%m-%d %H:%i:%s')";
            } 
            elseif ($_loginflag == "7") {

                $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Debit' as type, '' as rmk from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between if(STR_TO_DATE('$_Year-$_Month-01 00:00:00', '%Y-%m-%d %H:%i:%s') >= '2019-01-01 00:00:00', STR_TO_DATE('$_Year-$_Month-01 00:00:00', '%Y-%m-%d %H:%i:%s'), '2019-01-01 00:00:00') and  STR_TO_DATE('$_Year-$_Month-31 23:59:59', '%Y-%m-%d %H:%i:%s') 
                           union 
             select User_Credit_Tran_UserLoginId, User_Credit_Tran_Timestamp  as paydate, User_Credit_Tran_ProdInfo, User_Credit_Tran_PGid, User_Credit_Tran_Amount, 'Credit' as type, User_Credit_Tran_Remark as rmk from tbl_user_credit_transaction where User_Credit_Tran_UserLoginId= '" . $_SESSION['User_LoginId'] . "' and  User_Credit_Tran_Timestamp between if(STR_TO_DATE('$_Year-$_Month-01 00:00:00', '%Y-%m-%d %H:%i:%s') >= '2019-01-01 00:00:00', STR_TO_DATE('$_Year-$_Month-01 00:00:00', '%Y-%m-%d %H:%i:%s'), '2019-01-01 00:00:00') and  STR_TO_DATE('$_Year-$_Month-31 23:59:59', '%Y-%m-%d %H:%i:%s')  
                order by paydate DESC";
                
            } 
            elseif ($_loginflag == "14") {

                // $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Credit' from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between '" . $_StartDate . "' and '" . $_EndDate . "' order by `timestamp` DESC";
            }


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            $date1=(date_create($_Year.'-'.$_Month.'-01 00:00:00') >= date_create('2019-01-01 00:00:00') ? date_create($_Year.'-'.$_Month.'-01 00:00:00') : date_create('2019-01-01 00:00:00'));
            //$date1=date_create($_Year.'-'.$_Month.'-01 00:00:00');
            $date2=date_create($_Year.'-'.$_Month.' 23:59:59');
            $_Response[3] = date_format($date1, 'Y-m-d H:i:s');
            $_Response[4] = (date_format($date2, 'Y-m-t H:i:s') < date_format($date1, 'Y-m-t H:i:s') ? date_format($date1, 'Y-m-t H:i:s') : date_format($date2, 'Y-m-t H:i:s'));
         
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function ShowByLastSixM() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '14') {

                $_LoginUserType = "RSPUserCode";
                $_loginflag = "14";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;
for ($i = 1; $i < 6; $i++) {
  $d= date('Y-m-01 00:00:00', strtotime("-$i month"));
  
}

            if ($_loginflag == "1") {

             //    $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Debit' from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and  `timestamp` between STR_TO_DATE('$d', '%Y-%m-%d %H:%i:%s') and  CURDATE() 
             // union 
             // Select Payment_Refund_ITGK,Payment_Refund_Timestamp as paydate,Payment_Refund_ProdInfo,Payment_Refund_Txnid,
             // Payment_Refund_Amount,'Credit' From tbl_payment_refund WHERE  Payment_Refund_Timestamp between STR_TO_DATE('$d', '%Y-%m-%d %H:%i:%s') and  CURDATE()
             //    order by paydate";
            } 
            elseif ($_loginflag == "7") {
               $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Debit' as type, '' as rmk from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between if(STR_TO_DATE('$d', '%Y-%m-%d %H:%i:%s') >= '2019-01-01 00:00:00', STR_TO_DATE('$d', '%Y-%m-%d %H:%i:%s'), '2019-01-01 00:00:00') and  CURDATE() 
             union 
             select User_Credit_Tran_UserLoginId, User_Credit_Tran_Timestamp  as paydate, User_Credit_Tran_ProdInfo, User_Credit_Tran_PGid, User_Credit_Tran_Amount, 'Credit' as type, User_Credit_Tran_Remark as rmk from tbl_user_credit_transaction where User_Credit_Tran_UserLoginId= '" . $_SESSION['User_LoginId'] . "' and  User_Credit_Tran_Timestamp between if(STR_TO_DATE('$d', '%Y-%m-%d %H:%i:%s') >= '2019-01-01 00:00:00', STR_TO_DATE('$d', '%Y-%m-%d %H:%i:%s'), '2019-01-01 00:00:00') and  CURDATE() 
                order by paydate DESC";
            } 
            elseif ($_loginflag == "14") {

                // $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Credit' from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between '" . $_StartDate . "' and '" . $_EndDate . "' order by `timestamp` DESC";
            }


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            //$date1=date_create($d);
            $date1=(date_create($d) >= date_create('2019-01-01 00:00:00') ? date_create($d) : date_create('2019-01-01 00:00:00'));
            $date2=date_create(date("Y-m-d H:i:s"));
            $_Response[3] = date_format($date1, 'Y-m-d H:i:s');
            //$_Response[4] = date_format($date2, 'Y-m-d H:i:s');
            $_Response[4] = (date_format($date2, 'Y-m-t H:i:s') < date_format($date1, 'Y-m-t H:i:s') ? date_format($date1, 'Y-m-t H:i:s') : date_format($date2, 'Y-m-t H:i:s'));
          
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function ShowByFyear($_Fyear) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Fyear = mysqli_real_escape_string($_ObjConnection->Connect(),$_Fyear);
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '14') {

                $_LoginUserType = "RSPUserCode";
                $_loginflag = "14";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;

            $yeararr = explode('-', $_Fyear);

            if ($_loginflag == "1") {

            // $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Debit' as type from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and  `timestamp` between STR_TO_DATE('$yeararr[0]-04-01 00:00:00', '%Y-%m-%d %H:%i:%s') and  STR_TO_DATE('$yeararr[1]-03-31 23:59:59', '%Y-%m-%d %H:%i:%s')  
            //  union 
            //  Select Payment_Refund_ITGK,Payment_Refund_Timestamp as paydate,Payment_Refund_ProdInfo,Payment_Refund_Txnid,
            //  Payment_Refund_Amount,'Credit' as type From tbl_payment_refund WHERE Payment_Refund_Timestamp between STR_TO_DATE('$yeararr[0]-04-01 00:00:00', '%Y-%m-%d %H:%i:%s') and  STR_TO_DATE('$yeararr[1]-03-31 23:59:59', '%Y-%m-%d %H:%i:%s') 
            //  union 
            //  select User_Credit_Tran_UserLoginId, User_Credit_Tran_Timestamp  as paydate, User_Credit_Tran_ProdInfo, User_Credit_Tran_PGid, User_Credit_Tran_Amount, 'Credit' as type from tbl_user_credit_transaction where  User_Credit_Tran_Timestamp between STR_TO_DATE('$yeararr[0]-04-01 00:00:00', '%Y-%m-%d %H:%i:%s') and  STR_TO_DATE('$yeararr[1]-03-31 23:59:59', '%Y-%m-%d %H:%i:%s')  
            //     order by paydate";
            } 
            elseif ($_loginflag == "7") {
                 $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Debit'  as type, '' as rmk from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between if(STR_TO_DATE('$yeararr[0]-04-01 00:00:00', '%Y-%m-%d %H:%i:%s') >= '2019-01-01 00:00:00', STR_TO_DATE('$yeararr[0]-04-01 00:00:00', '%Y-%m-%d %H:%i:%s'), '2019-01-01 00:00:00') and  STR_TO_DATE('$yeararr[1]-03-31 23:59:59', '%Y-%m-%d %H:%i:%s')  
             union 
             select User_Credit_Tran_UserLoginId, User_Credit_Tran_Timestamp  as paydate, User_Credit_Tran_ProdInfo, User_Credit_Tran_PGid, User_Credit_Tran_Amount, 'Credit' as type, User_Credit_Tran_Remark as rmk from tbl_user_credit_transaction where User_Credit_Tran_UserLoginId= '" . $_SESSION['User_LoginId'] . "' and  User_Credit_Tran_Timestamp between if(STR_TO_DATE('$yeararr[0]-04-01 00:00:00', '%Y-%m-%d %H:%i:%s') >= '2019-01-01 00:00:00', STR_TO_DATE('$yeararr[0]-04-01 00:00:00', '%Y-%m-%d %H:%i:%s'), '2019-01-01 00:00:00') and  STR_TO_DATE('$yeararr[1]-03-31 23:59:59', '%Y-%m-%d %H:%i:%s')  
                order by paydate DESC";
          
            } 
            elseif ($_loginflag == "14") {

                // $_SelectQuery2 = "select Pay_Tran_ITGK,`timestamp` as paydate,Pay_Tran_ProdInfo,Pay_Tran_PG_Trnid,Pay_Tran_Amount,'Credit' from tbl_payment_transaction where Pay_Tran_Status='PaymentReceive' and Pay_Tran_ITGK='" . $_SESSION['User_LoginId'] . "' and  `timestamp` between '" . $_StartDate . "' and '" . $_EndDate . "' order by `timestamp` DESC";
            }


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
            $date1=date_create($yeararr[0].'-04-01 00:00:00');
            $date2=date_create($yeararr[1].'-03-31 23:59:59');
            $_Response[3] = date_format($date1, 'Y-m-d H:i:s');
            $_Response[4] = date_format($date2, 'Y-m-d H:i:s');
          
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function SHOWBANKDATA($usercode = NULL) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $usercode = mysqli_real_escape_string($_ObjConnection->Connect(),$usercode);
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {

                    $_SelectQuery = "Select a.*,b.* from tbl_bank_account as a inner join vw_itgkname_distict_rsp as b on "
                            . "a.Bank_User_Code=b.ITGKCODE where Bank_User_Code ='" . $usercode . "'";
                } else {

                    $_SelectQuery = "Select a.*,b.* from tbl_bank_account as a inner join vw_itgkname_distict_rsp as b on "
                            . " a.Bank_User_Code=b.ITGKCODE where Bank_User_Code ='" . $_SESSION['User_LoginId'] . "'";
                }

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
    public function Addfinalexamresult($rows) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $UNIX_DATE = ($b - 25569) * 86400;
//            $b = gmdate("M-y", $UNIX_DATE);
//            $UNIX_DATE = ($c - 25569) * 86400;
//            $c = gmdate("Y-m-d H:i:s", $UNIX_DATE);
//            $UNIX_DATE = ($g - 25569) * 86400;
//            $g = gmdate("d-m-Y", $UNIX_DATE);
            $trun = "DELETE from tbl_user_credit_transaction_temp where User_Credit_Tran_UpdUser='".$_SESSION['User_Code']."'";
            $_ObjConnection->ExecuteQuery($trun, Message::OnUpdateStatement);
            $_InsertQuery = "INSERT INTO tbl_user_credit_transaction_temp (User_Credit_Tran_UserLoginId, 
    User_Credit_Tran_ProdInfo,  
    User_Credit_Tran_Amount, 
    User_Credit_Tran_Timestamp,
    User_Credit_Tran_PGid,
    User_Credit_Tran_Remark,
    User_Credit_Tran_UpdUser,
    User_Credit_Tran_UserRoll,      
    User_Credit_Tran_Status, 
    User_Credit_Tran_Type) "
                    . "VALUES " . $rows;
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        // print_r($_Response);
        // die;
        return $_Response;
    }
    public function ShowGrid() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

             $_SelectQuery3 = "select l.*,cnt from tbl_user_credit_transaction_temp l
inner join (select count(distinct User_Credit_Tran_UserLoginId) as cnt,User_Credit_Tran_UserLoginId from
 tbl_user_credit_transaction_temp) r where User_Credit_Tran_UpdUser= '".$_SESSION['User_Code']."'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function transferexamresult($uploadfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $uploadfor = mysqli_real_escape_string($_ObjConnection->Connect(),$uploadfor);
            $_InsertQuery = "INSERT INTO tbl_user_credit_transaction (User_Credit_Tran_UserLoginId, User_Credit_Tran_UserRoll, 
    User_Credit_Tran_ProdInfo, User_Credit_Tran_PGid, User_Credit_Tran_Amount,  User_Credit_Tran_Timestamp, User_Credit_Tran_UpdUser, User_Credit_Tran_Status, 
    User_Credit_Tran_Type, User_Credit_Tran_Remark, User_Credit_Tran_UpdDate) select User_Credit_Tran_UserLoginId, User_Credit_Tran_UserRoll,    User_Credit_Tran_ProdInfo, User_Credit_Tran_PGid, User_Credit_Tran_Amount, 
    User_Credit_Tran_Timestamp, User_Credit_Tran_UpdUser, User_Credit_Tran_Status, 
    User_Credit_Tran_Type, User_Credit_Tran_Remark, User_Credit_Tran_UpdDate  from tbl_user_credit_transaction_temp where User_Credit_Tran_UpdUser= '".$_SESSION['User_Code']."' and User_Credit_Tran_UserRoll='".$uploadfor."'";
                    
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
             $trun = "DELETE from tbl_user_credit_transaction_temp where User_Credit_Tran_UpdUser='".$_SESSION['User_Code']."'";
             if($_Response['0'] == "Successfully Done"){
                $_ObjConnection->ExecuteQuery($trun, Message::OnUpdateStatement);
             }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function chkduplicateutr($uploadfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $uploadfor = mysqli_real_escape_string($_ObjConnection->Connect(),$uploadfor);
            $_InsertQuery = "SELECT User_Credit_Tran_PGid FROM tbl_user_credit_transaction_temp group by User_Credit_Tran_PGid having count(*) >= 2";
                    
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
       public function chkduplicateutrfinal($uploadfor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $uploadfor = mysqli_real_escape_string($_ObjConnection->Connect(),$uploadfor);
            // $_InsertQuery = "SELECT a.User_Credit_Tran_PGid,b.User_Credit_Tran_Timestamp FROM tbl_user_credit_transaction_temp as a inner join tbl_user_credit_transaction as b on a.User_Credit_Tran_PGid=b.User_Credit_Tran_PGid";

            $_InsertQuery = "SELECT a.User_Credit_Tran_PGid FROM tbl_user_credit_transaction as a where  a.User_Credit_Tran_PGid IN
                (select b.User_Credit_Tran_PGid from tbl_user_credit_transaction_temp as b)";
                    
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function chkpaymentdesc() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "SELECT * from tbl_payment_description where  Pay_Des_Status='1'";
                    
             $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
  public function SHOWrpt($_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
            $_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '14') {

                $_LoginUserType = "RSPUserCode";
                $_loginflag = "14";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {

                $_SelectQuery2 = "Select * From tbl_user_credit_transaction WHERE User_Credit_Tran_UpdDate >= '" . $_StartDate . "' AND User_Credit_Tran_UpdDate <= '" . $_EndDate . "'";
            } 


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
          
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function AddPayDes($_txtPayDes){
                global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_InsertQuery = "INSERT IGNORE INTO tbl_payment_description (Pay_Des_Value, Pay_Des_Status) VALUES ('".$_txtPayDes."','1')";
                    
             $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::OnUpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function SHOWrptCenter($_txtCenter,$_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_txtCenter = mysqli_real_escape_string($_ObjConnection->Connect(),$_txtCenter);
            $_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
            $_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11') {

                $_LoginUserType = "1";
                $_loginflag = "1";
            } elseif ($_LoginRole == '14') {

                $_LoginUserType = "RSPUserCode";
                $_loginflag = "14";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
                $_loginflag = "7";
            } else {
                echo "hello";
            }

            //$_SESSION['UserType'] = $_LoginUserType;


            if ($_loginflag == "1") {

                $_SelectQuery2 = "Select * From tbl_user_credit_transaction WHERE User_Credit_Tran_UpdDate >= '" . $_StartDate . "' AND User_Credit_Tran_UpdDate <= '" . $_EndDate . "' AND User_Credit_Tran_UserLoginId='".$_txtCenter."'";
            } 


            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
          
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
