<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
 
class clsLearnerCorrectionFee {
    //put your code here
    public function GetAll($_application, $_correctioncode, $paymode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
				$_ITGK_Code = $_SESSION['User_LoginId'];
				$_learnercode = $_SESSION['User_LearnerCode'];
				
				$_application = mysqli_real_escape_string($_ObjConnection->Connect(),$_application);
				$_correctioncode = mysqli_real_escape_string($_ObjConnection->Connect(),$_correctioncode);
				$paymode = mysqli_real_escape_string($_ObjConnection->Connect(),$paymode);
				
				$_SelectQueryGetEvent1 = "SELECT Event_Payment FROM tbl_event_management WHERE Event_CorrectionEvent = '" . $_correctioncode . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '8' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
				$_ResponseGetEvent1=$_ObjConnection->ExecuteQuery($_SelectQueryGetEvent1, Message::SelectStatement);
				$_getEvent = mysqli_fetch_array($_ResponseGetEvent1[2]);
				if($_getEvent['Event_Payment'] == '1')
				{
					$_SelectQuery = "Select * FROM  `tbl_correction_copy` WHERE Correction_Payment_Status = '0' AND applicationfor = '" . $_application . "' AND Correction_ITGK_Code='" . $_ITGK_Code . "' AND lcode='" . $_learnercode . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
				else if($_getEvent['Event_Payment'] == '2')
				{
					$_SelectQuery = "Select * FROM  `tbl_correction_copy` WHERE Correction_Payment_Status = '0' AND applicationfor = '" . $_application . "' AND Correction_ITGK_Code='" . $_ITGK_Code . "' AND lcode='" . $_learnercode . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				}
				else
				{
					//echo "Invalid User Input";
						return;
				}
	   } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
	
	public function GetCorrectionName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select DISTINCT a.applicationfor, a.docid FROM tbl_cdoccategory_master AS a INNER JOIN tbl_correction_copy AS b ON a.applicationfor = b.applicationfor WHERE a.active = 'Y'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetCorrectionFee($_doccode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
			$_doccode = mysqli_real_escape_string($_ObjConnection->Connect(),$_doccode);
			
           $_SelectQuery="Select RKCL_Share FROM tbl_cdoccategory_master WHERE docid = '" . $_doccode . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetApplicationName($_application)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try
        {
			$_application = mysqli_real_escape_string($_ObjConnection->Connect(),$_application);
			
           $_SelectQuery="Select applicationfor FROM tbl_cdoccategory_master WHERE docid = '" . $_application . "'";
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }	
	
	 public function AddLearnerCorrectionPayTran($postValues) {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        try {
				$_RKCL_Id = $txtGenerateId . '_RKCLtranid';
				$_ITGK_Code = $_SESSION['User_LoginId'];           

           $_InsertQuery = "INSERT INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray,Pay_Tran_LCount,"
                    . "Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_PG_Trnid) "
                    . "values('" . $_ITGK_Code . "' ,'" . $_RKCL_Id . "','" . $_CorrectionId . "','" . $Lcount . "','" . $amount1 . "','PaymentInProcess',
								'" . $_txtapplicationfor . "','" . $txtGeneratePayUId . "')";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            $_UpdateQuery = "Update tbl_correction_copy set Correction_RKCL_Trnid = '" . $_RKCL_Id . "', Correction_TranRefNo='" . $txtGeneratePayUId . "' 
								Where cid IN ($_CorrectionId)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }
	
 public function FillNetBankingName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From tbl_netbanking order by BankName ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
 public function GetDatabyCode() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code=$_SESSION['User_LoginId'];
				$_SelectQuery = "Select a.UserProfile_FirstName,a.UserProfile_Mobile,a.UserProfile_Email From tbl_userprofile as a inner join tbl_user_master as b
									on a.UserProfile_User=b.User_Code Where b.User_LoginId='" . $_Code . "'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				//print_r($_Response);
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;           
			 }
         return $_Response;
    }
}
