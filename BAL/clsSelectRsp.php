<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsSelectRsp
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';


$_ObjConnection = new _Connection();
$_Response = array();

class clsSelectRsp {

    //put your code here

    public function Add($_Rspcode, $_Rspname, $_Rspregno, $_Rspestdate, $_Rsptype, $_Rspdistrict, $_Rsptehsil, $_Rspstreet, $_Rsproad) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_User = $_SESSION['User_LoginId'];
                $_SelectQuery1 = "SELECT User_MobileNo FROM tbl_user_master WHERE User_LoginId='" . $_User . "'";
                $_Response4 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
                $_Row1 = mysqli_fetch_array($_Response4[2]);
                $_MobileITGK = $_Row1['User_MobileNo'];
                $_Date = date("Y-m-d h:i:s");
                $_InsertQuery = "Insert Into tbl_rspitgk_mapping(Rspitgk_Code,Rspitgk_ItgkCode,Rspitgk_Rspcode,Rspitgk_Rspname,"
                        . "Rspitgk_Rspregno,Rspitgk_Rspestdate,Rspitgk_Rsporgtype,Rspitgk_Rspdistrict,"
                        . "Rspitgk_Rsptehsil,Rspitgk_Rspstreet,Rspitgk_Rsproad,Rspitgk_Date) "
                        . "Select Case When Max(Rspitgk_Code) Is Null Then 1 Else Max(Rspitgk_Code)+1 End as Rspitgk_Code,"
                        . "'" . $_User . "' as Rspitgk_ItgkCode,'" . $_Rspcode . "' as Rspitgk_Rspcode,'" . $_Rspname . "' as Rspitgk_Rspname,"
                        . "'" . $_Rspregno . "' as Rspitgk_Rspregno,'" . $_Rspestdate . "' as Rspitgk_Rspestdate,"
                        . "'" . $_Rsptype . "' as Rspitgk_Rsporgtype,'" . $_Rspdistrict . "' as Rspitgk_Rspdistrict,"
                        . "'" . $_Rsptehsil . "' as Rspitgk_Rsptehsil,'" . $_Rspstreet . "' as Rspitgk_Rspstreet,"
                        . "'" . $_Rsproad . "' as Rspitgk_Rsproad,'" . $_Date . "' as Rspitgk_Date"
                        . " From tbl_rspitgk_mapping";
                //echo $_InsertQuery;
                $_SelectQuery = "Select User_Rsp From tbl_user_master Where User_LoginId='" . $_User . "'";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if ($_Response3[0] == 'Success') {
                    $_Row = mysqli_fetch_array($_Response3[2]);
                    $_RSP = $_Row['User_Rsp'];
                } else {
                    $_RSP = '0';
                }


                //echo $_Response[0];
                if ($_RSP == $_Rspcode) {
                    echo "This is Your Current RSP. Please Select Another RSP.";
                    return;
                } else {
                    $_DuplicateQuery = "Select * From tbl_rspitgk_mapping Where Rspitgk_ItgkCode='" . $_User . "'";
                    $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                    //echo $_Response[0];
                    if ($_Response[0] == Message::NoRecordFound) {
                        $_Response1 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                        $_UpdateQuery = "Update tbl_user_master set User_Rsp='" . $_Rspcode . "'"
                                . " Where User_LoginId='" . $_User . "'";
                        $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                        $_SMSITGK = "Dear IT-GK" . $_User . ", Congratulations! You have selected" . $_Rspname . " as your Service Provider. Regards, RKCL Team.";
                        $_SMSRSP = "Dear Service Provider, Congratulations! IT-GK " . $_User . " has selected you as Service Provider. Regards, RKCL Team.";
                        SendSMS($_MobileITGK, $_SMSITGK);
                        SendSMS($_Rspregno, $_SMSRSP);
                    } else {
                        $_Response[0] = Message::DuplicateRecord;
                        $_Response[1] = Message::Error;
                    }
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User = $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT b.Organization_District FROM tbl_user_master as a INNER JOIN tbl_organization_detail as b on a.User_Code=b.Organization_User WHERE a.User_LoginId='" . $_User . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_District = $_Row1['Organization_District'];

            $_SelectQuery = "SELECT DISTINCT b.Organization_Name, b.Organization_User FROM tbl_user_master as a INNER JOIN"
                    . " tbl_organization_detail as b on a.User_Code=b.Organization_User INNER JOIN"
                    . " tbl_rsptarget as c ON c.Rsptarget_User=a.User_LoginId " 
                    . " WHERE Rsptarget_Status='Approved' AND c.Rsptarget_District='" . $_District . "' ORDER BY Organization_Name ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDatabyCode($_OrgCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
				$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
				
            $_SelectQuery = "Select a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_organization_detail as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d "
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e"
                    . " ON a.Organization_User=e.User_Code WHERE Organization_User = '" . $_OrgCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SendOTP($_OrgCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
       // $_SMS = "OTP for RSP Selection is " . $_OTP;
 $_SMS = "OTP for Verification of Authentication is " . $_OTP;

        try {
				$_OrgCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_OrgCode);
				
            $_UserM = $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_LoginId='" . $_UserM . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_Mobile = $_Row1['User_MobileNo'];
            $_Email = $_Row1['User_EmailId'];


            $_InsertQuery = "Insert Into tbl_ao_register(AO_Code,AO_Email,AO_Mobile,AO_OTP)"
                    . "Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,"
                    . "'" . $_Email . "' as AO_Email,'" . $_Mobile . "' as AO_Mobile,'"
                    . "" . $_OTP . " as AO_OTP '"
                    . " From tbl_ao_register";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_ao_register Where AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_OTP = '" . $_OTP . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Verify($_Email, $_Mobile, $_Otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Email = mysqli_real_escape_string($_ObjConnection->Connect(),$_Email);
				$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
				$_Otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_Otp);
				
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "'
							AND AO_OTP = '" . $_Otp . "' AND AO_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid Verification Details. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_OTP = '" . $_Otp . "' AND AO_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_ao_register WHERE AO_Mobile='" . $_Mobile . "' AND AO_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
