<?php

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsSpaddressupdate {

    public function Getaddress() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Tehsil_Name,District_Name,User_Code,User_LoginId,User_MobileNo,User_EmailId,
            Org_formatted_address,Organization_Name,Organization_Code,Organization_State,State_Name from tbl_user_master as a
            inner join tbl_organization_detail as b on a.User_Code=b.Organization_User
            inner join tbl_district_master as c on b.Organization_District=c.District_Code 
            inner join tbl_state_master as e on b.Organization_State=e.State_Code
            inner join tbl_tehsil_master as d on b.Organization_Tehsil=d.Tehsil_Code
            where User_UserRoll='14'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
     public function ShowDetailForEdit($_editid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_editid = mysqli_real_escape_string($_ObjConnection->Connect(),$_editid);
				
             $_SelectQuery = "select District_Code,Tehsil_Code,Tehsil_Name,District_Name,User_Code,User_LoginId,User_MobileNo,User_EmailId,
                    Org_formatted_address,Organization_Name,Organization_Code,Organization_State,State_Name from tbl_user_master as a
                    inner join tbl_organization_detail as b on a.User_Code=b.Organization_User
                    inner join tbl_district_master as c on b.Organization_District=c.District_Code 
                    inner join tbl_state_master as e on b.Organization_State=e.State_Code
                    inner join tbl_tehsil_master as d on b.Organization_Tehsil=d.Tehsil_Code
                    where User_UserRoll='14' and Organization_Code = '" . $_editid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetAllState() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select State_Code,State_Name from tbl_state_master where State_Country='1' and State_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetAllDistrict($state) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$state = mysqli_real_escape_string($_ObjConnection->Connect(),$state);
				
            $_SelectQuery = "Select District_Code,District_Name From tbl_district_master Where District_Status='1'
				and District_StateCode = '".$state."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetAllTehshil($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$district = mysqli_real_escape_string($_ObjConnection->Connect(),$district);
				
            $_SelectQuery = "select Tehsil_Code,Tehsil_Name from tbl_tehsil_master where Tehsil_District='".$district."' 
				and Tehsil_Status='1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function updateaddress($_code,$_State,$_address,$_district,$_tehsil) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
				
                $_SelectQuery = "Select * from tbl_organization_detail where Organization_Code = '" . $_code . "' ";
                $_ResponseOldDetails = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if($_ResponseOldDetails[0] == 'Success')
                {
                    $_Row = mysqli_fetch_array($_ResponseOldDetails[2]);
                    $_old_state = $_Row['Organization_State'];
                    $_old_address = $_Row['Org_formatted_address'];
                    $_oldDistrict = $_Row['Organization_District'];
                    $_Tehsil = $_Row['Organization_Tehsil'];
                    $_code = $_Row['Organization_Code'];
                    $_InsertQuery = "Insert Into tbl_org_detail_log_rspaddressupdate(Organization_Code,Organization_State,Organization_District,Organization_Tehsil,
                             Org_formatted_address,Old_Organization_State,Old_Organization_District,Old_Organization_Tehsil,
                             Old_Org_formatted_address) 
                            VALUES ('". $_code ."','". $_State ."','".$_district."','". $_tehsil ."','". $_address."','". $_old_state ."','".$_oldDistrict."','". $_Tehsil ."','". $_old_address."')";
                    $_ResponseInts=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    if($_ResponseInts[0] == 'Successfully Inserted')
                    {
                        $_UpdateQuery = "Update tbl_organization_detail set Org_formatted_address='".$_address."',Organization_State='".$_State."',Organization_District='".$_district."',Organization_Tehsil='".$_tehsil."'
                                            WHERE  Organization_Code = '" . $_code . "'";
                        $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                    }
                }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
}