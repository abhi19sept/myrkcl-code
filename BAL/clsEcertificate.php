<?php
/**
 * Description of clsLinkAadhar
 *
 * @author Mayank and Anoop
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsEcertificate {
    
    public function FillEventName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct exam_event_name,exameventnameID From tbl_result";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function ShowLearnerEcertificateGet($event,$lot) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
				$lot = mysqli_real_escape_string($_ObjConnection->Connect(),$lot);
				
            $_SelectQuery = "Select * From tbl_result where result LIKE('%pass%') and exameventnameID='".$event."' and
								scol_no = '".$lot."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ShowLearnerEcertificate($event,$lot) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
				$lot = mysqli_real_escape_string($_ObjConnection->Connect(),$lot);
				
            $_SelectQuery = "Select * From tbl_result where result LIKE('%pass%') and exameventnameID='".$event."' and status='0'
							order by id ASC limit $lot";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ShowLearnerEcertificateUpdate($event,$lot) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
					$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
					$lot = mysqli_real_escape_string($_ObjConnection->Connect(),$lot);
					
            $_SelectQuery = "Select * From tbl_result where result LIKE('%pass%') and exameventnameID='".$event."' and status='1'
							order by id ASC limit $lot";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateEcertificateStatus($lcode,$eventid,$itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
            $_UpdateQuery = "update tbl_result set status='1' where scol_no ='".$lcode."' and exameventnameID='".$eventid."'
								and study_cen='".$itgk."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateEcertificateStatusForUpdateCertificate($lcode,$eventid,$itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
            $_UpdateQuery = "update tbl_result set status='2' where scol_no ='".$lcode."' and exameventnameID='".$eventid."'
								and study_cen='".$itgk."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}