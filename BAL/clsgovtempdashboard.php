<?php
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGovtEmpDashboard {

    public function GetApproved_by_RKCL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=0";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetLearnerDetails($_status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select learnercode,Govemp_ITGK_Code,fname,faname,empdob,gdname,designation,exammarks,applicationdate from 
							tbl_govempentryform where trnpending='" . $_status ."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetOfflineLearnerDetails($_status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select learnercode,Govemp_ITGK_Code,fname,faname,empdob,gdname,designation,exammarks,applicationdate from 
							tbl_govempentryform where trnpending='" . $_status ."' and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
		public function GETDeoDetails($_status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$todate = date('Y-m-d 00:00:00', strtotime('-30 days'));
            $_SelectQuery = "select count(distinct a.learnercode) as deocnt, Govemp_Datetime as applicationdate from 
							tbl_govempentryform as a, tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode
							and Govemp_Datetime >= '".$todate."' and deo='".$_status."' and Govemp_Process_Status in (0,4)
							group by DATE(Govemp_Datetime)  order by Govemp_Datetime";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetpaperlessLearnerDetails($_status) {
        global $_ObjConnection;
        $_ObjConnection->Connect(); 
		
        try {
			if($_status=='12'){
				$_SelectQuery = "select distinct a.learnercode,a.Govemp_ITGK_Code,fname,faname,empdob,gdname,designation,exammarks,
								applicationdate
								from tbl_govempentryform as a, tbl_govstatusupdatelog as b where 
								b.Govemp_Learnercode=a.learnercode and	applicationtype='paperless' and 
								Govemp_Process_Status='" . $_status ."'";
			}
			else if($_status=='30'){
				$todate = date('Y-m-d 00:00:00', strtotime('-30 days'));
				$_SelectQuery = "select distinct a.learnercode,a.Govemp_ITGK_Code,fname,faname,empdob,gdname,designation,exammarks,
								applicationdate
								from tbl_govempentryform as a, tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode and
								Govemp_Datetime >= '".$todate."' and applicationtype_log='paperless' and Govemp_Process_Status='12'";
			}
			else if($_status=='14'){
				$todate = date('Y-m-d');
				$_SelectQuery = "select distinct a.learnercode,a.Govemp_ITGK_Code,fname,faname,empdob,gdname,designation,exammarks,
								applicationdate
								from tbl_govempentryform as a, tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode and
								Govemp_Datetime like '".$todate."%' and applicationtype_log='paperless'";
			}
			else if($_status=='15'){				
				$todate = date('Y-m-d 00:00:00', strtotime('-30 days'));
				$_SelectQuery = "select count(distinct a.learnercode) as deocnt, deo from tbl_govempentryform as a,
									tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode and
									Govemp_Datetime >= '".$todate."' and deo NOT IN ('NULL' ,'') and
									Govemp_Process_Status in (0,4) group by deo";
			}
			else {
				$_SelectQuery = "select distinct a.learnercode,a.Govemp_ITGK_Code,fname,faname,empdob,gdname,designation,exammarks,
								applicationdate
								from tbl_govempentryform as a, tbl_govstatusupdatelog as b where 
								b.Govemp_Learnercode=a.learnercode and	applicationtype='paperless' and trnpending='" . $_status ."'";
			}
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetPending_for_Processing() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=3";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRejected_by_RKCL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=4";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetReceipt_Not_Submited() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=12";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetSent_to_DoIT() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=5";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAmount_Reimbursed() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=6";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAmount_Bounced() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=7";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAwaiting_Budget_Approval() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=8";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetPending_With_DoIT() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=9";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRejected_by_DoIT() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=10";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetNo_Status_Found() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=11";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
    
    public function GetOnline_Start_Process_Date() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Govemp_Datetime from tbl_govstatusupdatelog where Govemp_Process_Status=12 order by
							Govemp_Datetime asc limit 1,1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
     public function GetTotal_Applied_Paper_Less() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select distinct(Govemp_Learnercode) from tbl_govstatusupdatelog where applicationtype_log='paperless'
							 and Govemp_Process_Status='12'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }	
    
    
    
    public function GetApplied_From_Last_30_Days($todate) {		
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery="select count(distinct Govemp_Learnercode) as countN from tbl_govstatusupdatelog where 
							Govemp_Process_Status='12'
							and Govemp_Datetime >= '".$todate."' and (Govemp_Status is Null OR Govemp_Status='RecordUpdated')
							and applicationtype_log='paperless'";
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetFill_Today_Applied_Application($todate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(distinct Govemp_Learnercode) as countN  from tbl_govstatusupdatelog 
                            where Govemp_Datetime like '".$todate."%' 
                            and (Govemp_Status is Null OR Govemp_Status='RecordUpdated') and applicationtype_log='paperless'
							and Govemp_Process_Status='12'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }    
   
    public function GetApproved_by_RKCL_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select count(distinct Govemp_Learnercode) as countN from tbl_govempentryform as a, 
							 tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode and
							 trnpending='0' and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetPending_for_Processing_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a, 
							tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=3 
							and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRejected_by_RKCL_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a, 
							tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=4 
							and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetReceipt_Not_Submited_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a, 
								tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=12
								and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetSent_to_DoIT_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a, 
							tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=5
							and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAmount_Reimbursed_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a,
							tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=6 
							and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAmount_Bounced_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a, 
								tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=7
								and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
           
    public function GetAwaiting_Budget_Approval_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {           
             $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a,
								tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=8
								and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
           
    public function GetPending_With_DoIT_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a,
								tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=9
								and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRejected_by_DoIT_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a, 
							  tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=10
							  and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetNo_Status_Found_PL() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "select count(distinct b.Govemp_Learnercode) as countN from tbl_govempentryform as a, 
							  tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode AND a.trnpending=11
							  and applicationtype='paperless'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
           
    
	
	public function GetProcess_deo_Last_30_Days($_date) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "select count(distinct a.learnercode) as countN from tbl_govempentryform as a, 
								tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode and
								Govemp_Datetime >= '" . $_date ."' and deo NOT IN ('NULL' ,'') and
								Govemp_Process_Status in (0,4)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	public function GetApproved_by_RKCL_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=0 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetPending_for_Processing_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=3 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRejected_by_RKCL_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=4 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetReceipt_Not_Submited_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=12 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetSent_to_DoIT_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=5 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAmount_Reimbursed_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=6 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAmount_Bounced_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=7 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAwaiting_Budget_Approval_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=8 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetPending_With_DoIT_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=9 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRejected_by_DoIT_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=10 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetNo_Status_Found_Offline() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select count(*) as countN from tbl_govempentryform where trnpending=11 and applicationtype='offline'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GETLearnerDeoDetails($ldate,$deo) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
				 $_SelectQuery = "select a.learnercode,a.Govemp_ITGK_Code,a.fname,faname,empdob,gdname,designation,exammarks,
							Govemp_Datetime as applicationdate,Govemp_Process_Status from tbl_govempentryform as a, 
							tbl_govstatusupdatelog as b where b.Govemp_Learnercode=a.learnercode and deo='". $deo . "'
							and Govemp_Datetime like('%$ldate%') and Govemp_Process_Status in (0,4)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}




?>