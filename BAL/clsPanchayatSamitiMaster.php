<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPanchayatSamitiMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPanchayatSamitiMaster {
    //put your code here
    
    public function GetAll($country) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
       //echo $country;
        try {
				$country = mysqli_real_escape_string($_ObjConnection->Connect(),$country);
				
            if(!$country)
            {
               $_SelectQuery = "Select Block_Code,Block_Name,District_Name,"
                    . "Status_Name From tbl_panchayat_samiti as a inner join tbl_district_master as b "
                    . "on a.Block_District=b.District_Code inner join tbl_status_master as c "
                    . "on a.Block_Status=c.Status_Code"; 
            }
            else
            {
            $_SelectQuery = "Select Block_Code,Block_Name,District_Name,"
                    . "Status_Name From tbl_panchayat_samiti as a inner join tbl_district_master as b "
                    . "on a.Block_District=b.District_Code inner join tbl_status_master as c "
                    . "on a.Block_Status=c.Status_Code and a.Block_District='" . $country . "'";
            }
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Block_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Block_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Block_Code);
				
            $_SelectQuery = "Select Block_Code,Block_Name,Block_District,Block_Status"
                    . " From tbl_panchayat_samiti Where Block_Code='" . $_Block_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Block_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Block_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Block_Code);
            $_DeleteQuery = "Delete From tbl_panchayat_samiti Where Block_Code='" . $_Block_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_BlockName,$_BlockParent,$_BlockStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_BlockName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BlockName);
            $_InsertQuery = "Insert Into tbl_panchayat_samiti(Block_Code,Block_Name,"
                    . "Block_District,Block_Status) "
                    . "Select Case When Max(Block_Code) Is Null Then 1 Else Max(Block_Code)+1 End as Block_Code,"
                    . "'" . $_BlockName . "' as Block_Name,"
                    . "'" . $_BlockParent . "' as Block_District,'" . $_BlockStatus . "' as Block_Status"
                    . " From tbl_panchayat_samiti";
            $_DuplicateQuery = "Select * From tbl_panchayat_samiti Where Block_Name='" . $_BlockName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_BlockCode,$_BlockName,$_BlockParent,$_BlockStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_BlockName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BlockName);
				$_BlockCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_BlockCode);
				
            $_UpdateQuery = "Update tbl_panchayat_samiti set Block_Name='" . $_BlockName . "',"
                    . "Block_District='" . $_BlockParent . "',"
                    . "Block_Status='" . $_BlockStatus . "' Where Block_Code='" . $_BlockCode . "'";
            $_DuplicateQuery = "Select * From tbl_panchayat_samiti Where Block_Name='" . $_BlockName . "' "
                    . "and Block_Code <> '" . $_BlockCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
