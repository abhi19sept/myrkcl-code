<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsAuthorizationCertificate {

    //put your code here

public function GetItgkList($_district, $_tehsil) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_district = mysqli_real_escape_string($_ObjConnection->Connect(),$_district);
            $_tehsil = mysqli_real_escape_string($_ObjConnection->Connect(),$_tehsil);
			//print_r($_SESSION['User_Code']);
			$_LoginRole = $_SESSION['User_UserRoll'];
			if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '9' ||  $_LoginRole == '11') {
                $_LoginUserType = "1";
                $_loginflag = "1";
            } 
			else if ($_LoginRole == '14') {
                $_LoginUserType = "CenterUserCode";
                $_loginflag = "8";
            } 
			$date = date("Y-m-d");
				
				if ($_loginflag == "1") 
				{
					$_SelectQuery3 = "Select a.*,b.*,c.CourseITGK_ExpireDate from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK
											where User_UserRoll='7' AND Organization_District='" . $_district . "'
											AND Organization_Tehsil='" . $_tehsil . "' AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock' 
											group by b.user_loginid";
			} 
			elseif ($_loginflag == "8") 
			{
						 $_SelectQuery3 = "Select a.*,b.*,c.CourseITGK_ExpireDate from tbl_organization_detail as a inner join tbl_user_master as b on a.Organization_User=b.User_Code
											inner join tbl_courseitgk_mapping as c on b.user_loginid=c.Courseitgk_ITGK  
											where User_UserRoll='7' AND User_Rsp='".$_SESSION['User_Code']."' AND Organization_District='" . $_district . "'
											AND Organization_Tehsil='" . $_tehsil . "' AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock' 
											group by b.user_loginid ";
					    
						} 
						
			$_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
            return $_Response3;
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAll($_Code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			$date = date("Y-m-d");
             $_SelectQuery = "Select * From tbl_organization_detail as a inner join tbl_user_master as b 
							 on a.Organization_User=b.User_Code inner join tbl_district_master as c on a.Organization_District=c.District_Code inner join tbl_courseitgk_mapping as d on b.user_loginid=d.Courseitgk_ITGK	WHERE user_loginid = '" . $_Code . "'  AND d.Courseitgk_Course='RS-CIT' AND d.CourseITGK_ExpireDate>='" . $date . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
}
