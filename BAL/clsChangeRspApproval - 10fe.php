<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsChangeRspApproval
 *
 * @author VIVEK
 */

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsChangeRspApproval {
    //put your code here
    
    public function GetDatabyCode($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select * FROM tbl_rspitgk_mapping WHERE Rspitgk_ItgkCode = '" . $_CenterCode . "' AND Rspitgk_Status ='Pending'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCenterDetails($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select a.User_MobileNo, b.Organization_Name, c.District_Name, d.Tehsil_Name "
                    . " FROM tbl_user_master AS a INNER JOIN tbl_organization_detail AS b "
                    . " ON a.User_Code=b.Organization_User INNER JOIN tbl_district_master as c "
                    . " ON b.Organization_District=c.District_Code INNER JOIN tbl_tehsil_master as d "
                    . " ON b.Organization_Tehsil=d.Tehsil_Code WHERE a.User_LoginId = '" . $_CenterCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetRSPDetails($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select b.Organization_FoundedDate, b.Organization_Name, c.District_Name, d.Tehsil_Name "
                    . " FROM tbl_user_master AS a INNER JOIN tbl_organization_detail AS b "
                    . " ON a.User_Rsp=b.Organization_User INNER JOIN tbl_district_master as c "
                    . " ON b.Organization_District=c.District_Code INNER JOIN tbl_tehsil_master as d "
                    . " ON b.Organization_Tehsil=d.Tehsil_Code WHERE a.User_LoginId = '" . $_CenterCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAllHistory($_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select * FROM tbl_rspitgk_mapping WHERE Rspitgk_Status ='Approved' and Rspitgk_ItgkCode = '" . $_CenterCode . "' ORDER BY Rspitgk_Date";
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "Select * FROM tbl_rspitgk_mapping WHERE Rspitgk_Status ='Pending' ORDER BY Rspitgk_Date";
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Update($_CenterCode,$_Status,$_Remark,$_RspCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_Status == 'Approved'){    
            $_SelectQuery = "Select MAX(Rspitgk_Code) as Rspitgk_Code FROM tbl_rspitgk_mapping Where Rspitgk_ItgkCode='" . $_CenterCode . "' AND Rspitgk_Status='Approved'";            
            $_Response4 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response4[2]);
            $_RSP = $_Row['Rspitgk_Code'];
            $_Date = date("Y-m-d h:i:s");
            
            $_UpdateQuery1 = "Update tbl_rspitgk_mapping set Rspitgk_End_Date='" . $_Date . "'"
                    . "Where Rspitgk_Code='" . $_RSP . "'";            
            $_Response1=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
            
            $_UpdateQuery2 = "Update tbl_rspitgk_mapping set Rspitgk_Status='" . $_Status . "'"
                    . "Where Rspitgk_ItgkCode='" . $_CenterCode . "' AND Rspitgk_Status='Pending'";            
            $_Response2=$_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
            
            $_UpdateQuery = "Update tbl_user_master set User_Rsp='" . $_RspCode . "'"
                    . "Where User_LoginId='" . $_CenterCode . "'";            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);  
            } else{
                $_UpdateQuery = "Update tbl_rspitgk_mapping set Rspitgk_Status='" . $_Status . "',Rspitgk_Remark='" . $_Remark . "'"
                    . "Where Rspitgk_ItgkCode='" . $_CenterCode . "' AND Rspitgk_Status='Pending'";            
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);  
            }
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
