<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Yogendra
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsInnaugrationcount {

    //put your code here

public function GetAll($_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4'){
				 $_SelectQuery = "select  a.*,b.*,count(b.centercode) as centercount from vw_wcd_dd_itgk as a inner join tbl_inauguration_details as b on b.centercode = a.ITGKLogin AND b.Batchcode=".$_batch."  group by a.DDLogin";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_LoginUserRole == '17') {
				 $_SelectQuery = "select b.* from tbl_user_master as a
inner join tbl_inauguration_details as b on b.centercode = a.User_LoginId
inner join tbl_organization_detail as c on a.User_Code=c.Organization_User  WHERE 
 c.Organization_District='" . $_SESSION['Organization_District'] . "' AND b.Batchcode=".$_batch." group by b.centercode";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


	
public function GetCenterList($_po,$_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
            $_po = mysqli_real_escape_string($_ObjConnection->Connect(),$_po);
			   
			 $_SelectQuery3 = "select  *  from vw_wcd_dd_itgk as a inner join tbl_inauguration_details as b on b.centercode = a.ITGKLogin 
				WHERE a.DDLogin='".$_po."' AND  Batchcode=".$_batch."";   
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
   

}
