<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsItPeripherals
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsItPeripherals {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
          
		$_SelectQuery = "Select IT_Peripherals_Code,IT_UserCode,IT_Peripherals_Device_Type,IT_Peripherals_Availablity,"
                ."IT_Peripherals_Make,IT_Peripherals_Model,IT_Peripherals_Quantity,IT_Peripherals_Detail,"
                . "Device_Name From tbl_it_peripherals as a inner join tbl_device_master as b "
                . "on a.IT_Peripherals_Device_Type=b.Device_Code";
                
                } else {
                            
                $_SelectQuery = "Select IT_Peripherals_Code,IT_UserCode,IT_Peripherals_Device_Type,IT_Peripherals_Availablity,"
                ."IT_Peripherals_Make,IT_Peripherals_Model,IT_Peripherals_Quantity,IT_Peripherals_Detail,"
                . "Device_Name From tbl_it_peripherals as a inner join tbl_device_master as b "
                . "on a.IT_Peripherals_Device_Type=b.Device_Code where IT_UserCode = '" .$_SESSION['User_LoginId'] . "'";
                        }
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response2;
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response1;
    }
    public function GetDatabyCode($_IT_Peripherals_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_IT_Peripherals_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_IT_Peripherals_Code);
            $_SelectQuery = "Select IT_Peripherals_Code,IT_Peripherals_Device_Type,IT_Peripherals_Availablity,"
                    ."IT_Peripherals_Make,IT_Peripherals_Model,IT_Peripherals_Quantity,IT_Peripherals_Detail"
                    . " From tbl_it_peripherals Where IT_Peripherals_Code= '" . $_IT_Peripherals_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_IT_Peripherals_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_IT_Peripherals_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_IT_Peripherals_Code);
            $_DeleteQuery = "Delete From tbl_it_peripherals Where IT_Peripherals_Code='" . $_IT_Peripherals_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_DeviceName,$_Available,$_Make,$_Model,$_Quantity,$_Detail) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_DeviceName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DeviceName);
            $_mobile=$_SESSION['User_MobileNo'];
            $_SMS = "Dear  Applicant, Your IT Infra Details has been submitted to RKCL. Kindly complete entry of all other details for reaching next stage of NCR.";
            $_InsertQuery = "Insert Into tbl_it_peripherals(IT_Peripherals_Code,IT_Peripherals_Device_Type,IT_Peripherals_Availablity,"
                    ."IT_Peripherals_Make,IT_Peripherals_Model,IT_Peripherals_Quantity,IT_Peripherals_Detail,IT_UserCode) "
                    . "Select Case When Max(IT_Peripherals_Code) Is Null Then 1 Else Max(IT_Peripherals_Code)+1 End as IT_Peripherals_Code,"
                    . "'" . $_DeviceName . "' as IT_Peripherals_Device_Type,"
                    . "'" .$_Available . "' as IT_Peripherals_Availablity,"
                    . "'" .$_Make . "' as IT_Peripherals_Make,"
                    . "'" .$_Model . "' as IT_Peripherals_Model,"
                    . "'" .$_Quantity . "' as IT_Peripherals_Quantity,"
                    . "'" .$_Detail . "' as IT_Peripherals_Detail ,"
					 . "'" .$_SESSION['User_LoginId'] . "' as IT_UserCode"
                    . " From tbl_it_peripherals";
            $_DuplicateQuery = "Select * From tbl_it_peripherals Where IT_Peripherals_Device_Type='" . $_DeviceName . "' AND IT_UserCode='" .$_SESSION['User_LoginId'] . "'";
			$_Affilate='1';
				$_InsertQuery1 = "Insert Into tbl_ncrstatus(status) 
												 VALUES ('" .$_Affilate. "')";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				
            
            $_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
		SendSMS($_mobile, $_SMS);		
				
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_IT_Peripherals_Code,$_DeviceName,$_Available,$_Make,$_Model,$_Quantity,$_Detail) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_DeviceName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DeviceName);
            $_IT_Peripherals_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_IT_Peripherals_Code);
            $_UpdateQuery = "Update tbl_it_peripherals set IT_Peripherals_Device_Type='" . $_DeviceName . "',"
                    . "IT_Peripherals_Availablity='" . $_Available ."',"
                    . "IT_Peripherals_Make='" . $_Make ."',"
                    . "IT_Peripherals_Model='" . $_Model ."',"
                    . "IT_Peripherals_Quantity='" . $_Quantity ."',"
                    . "IT_Peripherals_Detail='" . $_Detail ."'"
                    . " Where IT_Peripherals_Code='" . $_IT_Peripherals_Code . "'";
            $_DuplicateQuery = "Select * From tbl_it_peripherals Where IT_Peripherals_Device_Type='" . $_DeviceName . "' "
                    . "and IT_Peripherals_Code <> '" . $_IT_Peripherals_Code . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                //echo $_UpdateQuery;
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                //print_r($_Response);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
