<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of clsStateMaster
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsStateMaster {
    //put your code here
     public function GetAll($country) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
       //echo $country;
        try {
				$country = mysqli_real_escape_string($_ObjConnection->Connect(),$country);
				
            if(!$country)
            {
               $_SelectQuery = "Select State_Code,State_Name,Country_Name,"
                    . "Status_Name From tbl_state_master as a inner join tbl_country_master as b "
                    . "on a.State_Country=b.Country_Code inner join tbl_status_master as c "
                    . "on a.State_Status=c.Status_Code ORDER BY State_Name"; 
            }
            else
            {
            $_SelectQuery = "Select State_Code,State_Name,Country_Name,"
                    . "Status_Name From tbl_state_master as a inner join tbl_country_master as b "
                    . "on a.State_Country=b.Country_Code inner join tbl_status_master as c "
                    . "on a.State_Status=c.Status_Code and a.State_Country='" . $country . "' ORDER BY State_Name";
            }
            //echo $_SelectQuery;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_State_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_State_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_State_Code);
				
            $_SelectQuery = "Select State_Code,State_Name,State_Country,State_Status"
                    . " From tbl_state_master Where State_Code='" . $_State_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_State_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_State_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_State_Code);
				
            $_DeleteQuery = "Delete From tbl_state_master Where State_Code='" . $_State_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_StateName,$_StateParent,$_StateStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StateName = mysqli_real_escape_string($_ObjConnection->Connect(),$_StateName);
				$_StateParent = mysqli_real_escape_string($_ObjConnection->Connect(),$_StateParent);
				$_StateStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_StateStatus);
				
            $_InsertQuery = "Insert Into tbl_state_master(State_Code,State_Name,"
                    . "State_Country,State_Status) "
                    . "Select Case When Max(State_Code) Is Null Then 1 Else Max(State_Code)+1 End as State_Code,"
                    . "'" . $_StateName . "' as State_Name,"
                    . "'" . $_StateParent . "' as State_Country,'" . $_StateStatus . "' as State_Status"
                    . " From tbl_state_master";
            $_DuplicateQuery = "Select * From tbl_state_master Where State_Name='" . $_StateName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_StateCode,$_StateName,$_StateParent,$_StateStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_StateCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_StateCode);
				$_StateName = mysqli_real_escape_string($_ObjConnection->Connect(),$_StateName);
				$_StateParent = mysqli_real_escape_string($_ObjConnection->Connect(),$_StateParent);
				
            $_UpdateQuery = "Update tbl_state_master set State_Name='" . $_StateName . "',"
                    . "State_Country='" . $_StateParent . "',"
                    . "State_Status='" . $_StateStatus . "' Where State_Code='" . $_StateCode . "'";
            $_DuplicateQuery = "Select * From tbl_state_master Where State_Name='" . $_StateName . "' "
                    . "and State_Code <> '" . $_StateCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
