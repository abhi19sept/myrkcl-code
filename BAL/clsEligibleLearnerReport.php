<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsEligibleLearnerReport.php
 *
 * @author Yogendra soni
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsEligibleLearnerReport 
{
    //put your code here
  
   public function GetAll($event) 
   {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{	
			$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
			
		    $_SelectQuery = "select el.learnercode as Learner_Code,upper(el.learnername)as Learner_Name, upper(el.fathername)as
								 Learners_FatherName, el.dob as Date_of_Birth, Admission_Mobile as Learner_MobileNo,  
								 el.examdate as Exam_Date, el.eventid as Exam_Event_Id, el.eventname as Exam_Event_Name, 
								 el.itgkcode as ITGK_Code, upper(el.itgkname)as ITGK_Name, el.Reason, el.status as Eligibility_Status ,
								 District_Name as ITGK_District,Tehsil_Name as ITGK_Tehsil from tbl_eligiblelearners el
								 left join tbl_admission on learnercode=Admission_LearnerCode
								 left join tbl_district_master on District_Code=districtId
								 left join tbl_tehsil_master on Tehsil_Code=thesilId
								where eventid='".$event."' and status like('eligible') order by el.itgkcode";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}