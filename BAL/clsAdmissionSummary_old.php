<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsAdmissionSummary {

    //put your code here

    public function GetDataAll($_Role, $_course, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_UserRole = $_SESSION['User_UserRoll'];
            $_SelectedRole = $_Role;
            $_LoginRole = $_SESSION['User_UserRoll'];

            // echo $_LoginRole;

            if ($_LoginRole == '1' || '2' || '3' || '4' || '8' || '9' || '10' || '11') {

                $_LoginUserType = $_LoginRole;
            } elseif ($_LoginRole == '5') {

                $_LoginUserType = "PSAUserCode";
            } elseif ($_LoginRole == '6') {

                $_LoginUserType = "DLCUserCode";
            } elseif ($_LoginRole == '7') {

                $_LoginUserType = "CenterUserCode";
            } else {
                echo "hello";
            }
            $_SESSION['UserType'] = $_LoginUserType;

            if ($_LoginRole == '1' || '2' || '3' || '4' || '8' || '9' || '10' || '11') {

                $_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code";
            } else {

                $_SelectQuery = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE " . $_LoginUserType . " = '" . $_SESSION['User_Code'] . "'";
            }

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);


            $_i = 0;
            $CenterCode2 = '';
            while ($_Row = mysqli_fetch_array($_Response[2])) {

                $CenterCode2.=$_Row['Centercode'] . ",";
                $_i = $_i + 1;
            }
            $CenterCode3 = rtrim($CenterCode2, ",");
            $_SESSION['AdmSummaryCenter'] = $CenterCode3;

            if ($_SelectedRole == '1') {
                
            } elseif ($_SelectedRole == '4') {
                
            } elseif ($_SelectedRole == '5') {
                $_RoleFlter = "PSAName";
                $_RoleFlterCode = "PSAUserCode";
            } elseif ($_SelectedRole == '6') {
                $_RoleFlter = "DLCName";
                $_RoleFlterCode = "DLCUserCode";
            } elseif ($_SelectedRole == '7') {
                $_RoleFlter = "Centercode";
                $_RoleFlterCode = "Centercode";
            } else {
                echo "hello";
            }
            $_SESSION['$_RoleFlterCode'] = $_RoleFlterCode;

            if ($CenterCode3) {

                if ($_batch == '0') {

                    $_SelectQuery3 = "SELECT DISTINCT vwcenterwisedlcpsaname." . $_RoleFlter . " AS RoleName,vwcenterwisedlcpsaname." . $_RoleFlterCode . " AS RoleCode, Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM vwcenterwisedlcpsaname CROSS JOIN tbl_admission ON vwcenterwisedlcpsaname.Centercode = tbl_admission.Admission_ITGK_Code WHERE vwcenterwisedlcpsaname.Centercode IN ($CenterCode3) AND tbl_admission.Admission_Course = '" . $_course . "' group by vwcenterwisedlcpsaname." . $_RoleFlter . "";
                } else {

                    $_SelectQuery3 = "SELECT DISTINCT vwcenterwisedlcpsaname." . $_RoleFlter . " AS RoleName,vwcenterwisedlcpsaname." . $_RoleFlterCode . " AS RoleCode, Count(tbl_admission.Admission_Code) AS admissioncount, '" . $_course . "' AS Course, '" . $_batch . "' AS Batch FROM vwcenterwisedlcpsaname CROSS JOIN tbl_admission ON vwcenterwisedlcpsaname.Centercode = tbl_admission.Admission_ITGK_Code WHERE vwcenterwisedlcpsaname.Centercode IN ($CenterCode3) AND tbl_admission.Admission_Course = '" . $_course . "' AND tbl_admission.Admission_Batch = '" . $_batch . "' group by vwcenterwisedlcpsaname." . $_RoleFlter . "";
                }
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerList($_course, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $CenterCode3 = $_SESSION['AdmSummaryCenter'];
            $_LoginRole = $_SESSION['$_RoleFlterCode'];
            $_LoginType = $_SESSION['UserType'];
            // echo $_LoginRole;


            $_SelectQuery1 = "SELECT DISTINCT a.Centercode FROM VwCenterWiseDLCPSA AS a INNER JOIN tbl_admission AS b ON a.Centercode = b.Admission_ITGK_Code WHERE a." . $_LoginRole . " = '" . $_rolecode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            //print_r($_Response);


            $_i = 0;
            $CenterCode2 = '';
            while ($_Row = mysqli_fetch_array($_Response[2])) {

                $CenterCode2.=$_Row['Centercode'] . ",";
                $_i = $_i + 1;
            }
            $CenterCode3 = rtrim($CenterCode2, ",");
            if ($CenterCode3) {

                if ($_LoginType == '1' || '2' || '3' || '4' || '8' || '9' || '10' || '11') {

                    $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a INNER JOIN vwcenterwisedlcpsaname AS b ON a.Admission_ITGK_Code = b.Centercode WHERE a.Admission_ITGK_Code IN ($CenterCode3) AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "'";
                } else {

                    $_SelectQuery3 = "SELECT a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.Admission_Fname,a.Admission_Photo,a.Admission_Sign FROM tbl_admission AS a INNER JOIN vwcenterwisedlcpsaname AS b ON a.Admission_ITGK_Code = b.Centercode WHERE a.Admission_ITGK_Code IN ($CenterCode3) AND a.Admission_Course = '" . $_course . "' AND a.Admission_Batch = '" . $_batch . "' AND " . $_LoginType . "='" . $_SESSION['User_Code'] . "'";
                }


                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

}
