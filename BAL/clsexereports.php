<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

class clsexecutesqls {

    //put your code here

    public function Add($formData) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $values = unserialize($formData);

        $replace = ['&gt;' => '>', '&lt;' => '<'];

        $return = '<span class="small star">Unable to process, please check your all input details.</span>';
        $sql = trim(addslashes($values['sql']));
        $subject = trim(addslashes($values['subject']));
        $createSql = "CREATE TABLE IF NOT EXISTS tbl_reconcile_reports (
             id int(11) NOT NULL AUTO_INCREMENT,
             subject varchar(255) DEFAULT NULL,                         
             qry text,                                                  
             status tinyint(1) DEFAULT '1',                             
             adddate datetime DEFAULT CURRENT_TIMESTAMP,                
             lastupdated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  
             PRIMARY KEY (`id`)                                           
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
        $_ObjConnection->ExecuteQuery($createSql, Message::MultipleStatement);

        foreach ($replace as $key => $val) {
            $sql = str_replace($key, $val, $sql);
            $subject = str_replace($key, $val, $subject);
        }

        if ($values['id']) {
            $updateQuery = "UPDATE tbl_reconcile_reports SET 
                subject = '" . $subject . "', 
                qry = '" . $sql . "' WHERE id = '" . $values['id'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);

            if ($_Response[0] == Message::SuccessfullyUpdate) {
                $return = '<span class="small">Sql has been updated successfully.</span>';
            }
        } else {
            $_InsertQuery = "INSERT INTO tbl_reconcile_reports SET 
                subject = '" . addslashes($values['subject']) . "', 
                qry = '" . addslashes($values['sql']) . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

            if ($_Response[0] == Message::SuccessfullyInsert) {
                $return = '<span class="small">Sql has been registred successfully.</span>';
            }
        }

        return $return;
    }

    public function getall() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $return = [];
        $query = "SELECT * FROM tbl_reconcile_reports ORDER BY id DESC";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            while ($row = mysqli_fetch_assoc($_Response[2])) {
                $return[] = $row;
            }
        }

        return $return;
    }

    public function deleteSQl($id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
			$id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
			
        $query = "DELETE FROM tbl_reconcile_reports WHERE id = '" . $id . "'";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
    }

    public function executeSQl($id) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $return = [];
		
		$id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
		
        $query = "SELECT * FROM tbl_reconcile_reports WHERE id = '" . $id . "'";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2])) {
            $row = mysqli_fetch_assoc($_Response[2]);
            echo '<span class="small"><b>' . $row['subject'] . '</b><br /><b>Query:</b> ' . $row['qry'] . '</span><p>&nbsp;</p>';
            $sql = trim(stripcslashes($row['qry']));
            $str = 'select ';
            $len = strlen($str);
            $subStr = strtolower(substr($sql, 0, $len));
            $statementType = ($subStr == $str) ? Message::SelectStatement : Message::MultipleStatement;
            $_Response = $_ObjConnection->ExecuteQuery($sql, $statementType);
            if ($_Response[1] = Message::Success && isset($_Response[2])) {
                if (mysqli_num_rows($_Response[2])) {
                    $row = mysqli_fetch_assoc($_Response[2]);
                    $return['cols'] = array_keys($row);
                    $return['data'][] = $row;
                    while ($row = mysqli_fetch_assoc($_Response[2])) {
                        $return['data'][] = $row;
                    }
                }
            } else {
                echo '<center>Query executed successfully.</center>';
            }
        }

        return $return;
    }

}