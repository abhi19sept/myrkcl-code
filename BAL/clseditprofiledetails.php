<?php


/**
 * Description of clsOrgDetail
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';


$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clseditprofiledetails {
	
	 public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		$_LoginRole = $_SESSION['User_UserRoll'];

			
			if($_LoginRole == '11' || $_LoginRole == '1') {
					$_SelectQuery2 = "Select * from tbl_userprofile as a inner join tbl_user_master as b on a.UserProfile_User=b.User_Code inner join tbl_courseitgk_mapping as c
				    on b.user_loginid=c.Courseitgk_ITGK where b.User_UserRoll='7' AND Courseitgk_Course='RS-CIT'";
             
			}
               
				//$_SelectQuery2 = "Select * from tbl_staff_detail where Staff_User IN ($CenterCode3)";
                $_Response2 = $_ObjConnection->ExecuteQuery($_SelectQuery2, Message::SelectStatement);
                return $_Response2;
            
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response1;
    }
	
		
     public function UPDATE($_Code,$_username,$_usermobile,$_useremail) 
       {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_username = mysqli_real_escape_string($_ObjConnection->Connect(),$_username);
				
		  $_UpdateQuery = "Update tbl_userprofile set 
						
						UserProfile_FirstName='" . $_username . "',
						UserProfile_Mobile='" . $_usermobile . "',
						UserProfile_Email='" . $_useremail . "',
						IsNewRecord='Y'
						Where  	UserProfile_User='" . $_Code . "'";
            
					 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
					
                      $_UpdateQuery1 = "Update tbl_user_master set 
						
						
						User_MobileNo='" . $_usermobile . "',
						User_EmailId='" . $_useremail . "' ,IsNewRecord='Y',IsNewCRecord='Y',IsNewSRecord='Y',IsNewOnlineLMSRecord='Y'
						Where User_Code='" . $_Code . "'";
            
					 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
			  
			  
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
	
	
	public function GetDatabyCode($_activate)
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_activate = mysqli_real_escape_string($_ObjConnection->Connect(),$_activate);
				
           $_SelectQuery = "Select UserProfile_FirstName,UserProfile_Mobile,UserProfile_Email  From tbl_userprofile 
							Where  	UserProfile_User='" . $_activate . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
	
	
	
	
	
	
}
