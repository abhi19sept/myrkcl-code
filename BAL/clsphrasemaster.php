<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of clsStateMaster
 *
 * @author Sunil Kumar Baindara
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsphrasemaster {
    //put your code here
     public function GetAll($country) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
       //echo $country;
        try {
            
            $_SelectQuery = "select * from tbl_phrase_ke order by Phrase_ID desc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    
    public function DeleteRecord($_State_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_State_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_State_Code);
				
            $_DuplicateQuery = "Select * From tbl_phrase_ke Where Phrase_ID='" . $_State_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            $_Row = mysqli_fetch_array($_Response[2],true);
            //echo "<pre>"; print_r($_Row);
            //echo $_Row['Phrase_Status'];die;
            if($_Row['Phrase_Status']==0)
            {
                $_DeleteQuery = "Delete From tbl_phrase_ke Where Phrase_ID='" . $_State_Code . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            }else{
                $_Response = array();
                $_Response[0]='DONTDOTHIS';
            }
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function AddPhrase_Ke($_Phrase_Ke) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Phrase_Ke = mysqli_real_escape_string($_ObjConnection->Connect(),$_Phrase_Ke);
				
            $_InsertQuery = "Insert Into tbl_phrase_ke(Phrase_Ke,Phrase_Status) VALUES('" . strtoupper($_Phrase_Ke) . "',1)";
            $_DuplicateQuery = "Select * From tbl_phrase_ke Where Phrase_Ke='" . strtoupper($_Phrase_Ke) . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_UpdateQuery = "Update tbl_phrase_ke set Phrase_Status=0";
                $_Response2=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                if($_Response2[0]=='Successfully Updated'){
                    $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                }
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
   
    
}
