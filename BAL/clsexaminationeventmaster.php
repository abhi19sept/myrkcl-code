<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogi
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsexaminationeventmaster {
	/*Added By Sunil: Dated: 7-02-2017
        For checking the event status is reday for download permission latter*/
        public function getPermissionletter($_actionvalue,$center2) {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {

             $_SelectQuery = "SELECT * from tbl_finalexammapping  where learnercode='" . $_SESSION['User_LearnerCode'] . "' AND centercode='" .$center2. "' AND examid='".$_actionvalue."'";
             $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            }
                    catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
             return $_Response;
        }
    
        /*Added By Sunil: Dated: 7-02-2017
        For checking the event status is reday for download permission latter*/
	
	
	public function GetAll($_actionvalue, $center2, $learnerCode = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        if (empty($center2)) {
            return false;
        }
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				$center2 = mysqli_real_escape_string($_ObjConnection->Connect(),$center2);
				
          $filter = !empty($learnerCode) ? " AND learnercode IN ('" . $this->getCleanedlearnerCodes($learnerCode) . "')" : '';
          $_SelectQuery = "SELECT examcentercode, learnercode,coursename,batchname,centercode,examcentername,examcenteraddress,examcentertehsil,examcenterdistrict,rollno,learnername,fathername,dob from tbl_finalexammapping  where centercode='" .$center2. "' AND examid='".$_actionvalue."' $filter ORDER BY rollno ASC, examcentercode ASC";
         $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }

        return $_Response;
    }
	
	public function GetAll1($_actionvalue,$center2)
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			$center2 = mysqli_real_escape_string($_ObjConnection->Connect(),$center2);
			
        $_SelectQuery = "SELECT learnercode,centercode,examcentername,examcenteraddress,examcentertehsil,examcenterdistrict,rollno,learnername,fathername,dob from tbl_finalexammapping  where centercode IN ($center2) AND examid='".$_actionvalue."'";
         $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			
        }
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    //put your code here
	
	
	 public function Getcenter($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select Distinct centercode from tbl_finalexammapping where examid='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
    public function GetcenterByDistrict($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "SELECT DISTINCT fm.centercode FROM tbl_finalexammapping fm INNER JOIN tbl_district_master dm ON fm.examcenterdistrict = dm.District_Name WHERE dm.District_Code = '" . $_actionvalue . "' ORDER BY fm.centercode ASC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetCenters($requestArgs) {
        global $_ObjConnection;
        $filter = $limit = '';
        $_ObjConnection->Connect();
        try {
            if (!empty($requestArgs['job'])) {
                $job = $requestArgs['job'];
                $start = ($job - 1) * 1000;
                $end = 1000;
                $limit = " LIMIT $start, $end";
            }
            if (!empty($requestArgs['centercode']) && $requestArgs['centercode'] != 'All') {
                $centercodes = explode(',', $requestArgs['centercode']);
                $filter .= " AND fm.centercode IN ('" . implode("','", $centercodes) . "')";
            }
            if (!empty($requestArgs['examCenterCode'])) {
                $examCenterCode = explode(',', $requestArgs['examCenterCode']);
                $filter .= " AND fm.examcentercode IN ('" . implode("','", $examCenterCode) . "')";
            }
            if (!empty($requestArgs['district'])) {
                $filter .= " AND dm.District_Code = '" . $requestArgs['district'] . "'";
            }
            if (!empty($requestArgs['learnerCode'])) {
                $filter .= " AND (fm.learnercode IN ('" . $this->getCleanedlearnerCodes($requestArgs['learnerCode']) . "') OR fm.centercode IN ('" . $this->getCleanedlearnerCodes($requestArgs['learnerCode']) . "'))";
            }

            //$filter = " AND fm.centercode IN ('11290273', '12290085', '12290085', '12290125', '13290100', '14290008', '15290114', '16290037', '21290606', '21290669', '23290022', '23290082', '24290089', '25290035', '25290035', '25290035', '25290035', '25290035', '25290035', '25290035', '27290098', '31290031', '31290097', '33290061', '33290071', '36290115', '36290216', '37290013', '45290186', '45290254', '45290327', '45290383', '46290049', '51290087', '51290087', '51290087', '51290087', '51290087', '51290087', '57290009', '71290080', '71290080', '71290080', '71290080', '51290033')";
			
			//$filter = " AND fm.learnercode IN ('127170810110841789')";

            print $_SelectQuery = "SELECT DISTINCT fm.centercode, fm.examcenterdistrict, REPLACE(fm.eventname, 'RS-CIT', '') AS eventname, examdate, examtime FROM tbl_finalexammapping fm INNER JOIN tbl_district_master dm ON fm.examcenterdistrict = dm.District_Name WHERE fm.examid = '" . $requestArgs['code'] . "'" . $filter . " ORDER BY fm.centercode ASC " . $limit;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
        
        return $_Response;
    }

    function GetAllMappedLearner($learnerCodes, $examId) {
        global $_ObjConnection;
        $filter = $limit = '';
        $_ObjConnection->Connect();
        try {
				$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
				
            $filter .= " AND fm.learnercode IN ('" . implode("', '", $learnerCodes) . "')";
            $_SelectQuery = "SELECT DISTINCT fm.centercode AS Admission_ITGK_Code, fm.learnercode AS Admission_LearnerCode, fm.batchname AS Batch_Name, fm.coursename AS Course_Name FROM tbl_finalexammapping fm WHERE fm.examid = '" . $examId . "'" . $filter . " ORDER BY fm.centercode ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }

        return $_Response;
    }
    
    function getCleanedlearnerCodes($learnerCode) {
        $learnerCodes = explode(',', $learnerCode);
        $codes = [];
        foreach ($learnerCodes as $code) {
            $codes[] = trim($code);
        }

        return !empty($codes) ? implode("','", $codes) : '';
    }
	
	
}



