<?php


/**
 * Description of clsOrgDetail
 *
 * @author Mayank
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsexamcertificates {

    private function dbConnection() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        return $_ObjConnection;
    }

    public function GetResultEvents() {
        $_ObjConnection = $this->dbConnection();
        try {
            $_SelectQuery = "SELECT exam_event_name AS Event_Name, exameventnameID AS Event_Id, exam_held_date AS Event_Date FROM tbl_result GROUP BY exameventnameID ORDER BY exameventnameID DESC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function GetcenterByDistrict($_actionvalue) {
        $_ObjConnection = $this->dbConnection();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
             $_SelectQuery = "SELECT DISTINCT tr.study_cen AS centercode FROM tbl_result tr INNER JOIN tbl_user_master um ON um.User_LoginId = tr.study_cen INNER JOIN tbl_organization_detail om ON om.Organization_User = um.User_Code INNER JOIN tbl_district_master dm ON om.Organization_District = dm.District_Code WHERE dm.District_Code = '" . $_actionvalue . "' ORDER BY tr.study_cen ASC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

    public function getLearnerResults($requestArgs) {
        $filter = $limit = '';
        $_ObjConnection = $this->dbConnection();
        try {
            if (!empty($requestArgs['job'])) {
                $job = $requestArgs['job'];
                $start = ($job - 1) * 50000;
                $end = 50000;
                $limit = " LIMIT $start, $end";
            }
            if (!empty($requestArgs['centercode']) && $requestArgs['centercode'] != 'All') {
                $centercodes = explode(',', $requestArgs['centercode']);
                $filter .= " AND tr.study_cen IN ('" . implode("','", $centercodes) . "')";
            }
            if (!empty($requestArgs['district'])) {
                $filter .= " AND om.Organization_District = '" . $requestArgs['district'] . "'";
            }
            if (!empty($requestArgs['learnerCode'])) {
                $filter .= " AND tr.scol_no IN ('" . $this->getCleanedlearnerCodes($requestArgs['learnerCode']) . "')";
            }

            //$filter = " AND fm.centercode IN ('11290273', '12290085', '12290085', '12290125', '13290100', '14290008', '15290114', '16290037', '21290606', '21290669', '23290022', '23290082', '24290089', '25290035', '25290035', '25290035', '25290035', '25290035', '25290035', '25290035', '27290098', '31290031', '31290097', '33290061', '33290071', '36290115', '36290216', '37290013', '45290186', '45290254', '45290327', '45290383', '46290049', '51290087', '51290087', '51290087', '51290087', '51290087', '51290087', '57290009', '71290080', '71290080', '71290080', '71290080', '51290033')";
            
            //$filter = " AND tr.scol_no IN ('466170417084145789')";
            $curdate = date("d-m-Y h:i:s");
            $_SelectQuery = "SELECT tr.id, tr.scol_no AS learnercode, tr.result_date, tr.name, tr.study_cen AS centercode, tr.percentage, tr.fname, tr.exameventnameID, REPLACE(REPLACE(em.Event_Name, 'RS-CIT ', ''), ' ', '-') AS eventname, tr.vmou_ref_num, tr.exam_date, '" . $curdate . "' AS istdt FROM tbl_result tr INNER JOIN tbl_events em ON tr.exameventnameID = em.Event_Id INNER JOIN tbl_user_master um ON um.User_LoginId = tr.study_cen INNER JOIN tbl_organization_detail om ON om.Organization_User = um.User_Code WHERE exameventnameID = '" . $requestArgs['code'] . "' AND result LIKE('%pass%') " . $filter . " ORDER BY tr.study_cen ASC " . $limit;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
        
        return $_Response;
    }

    function getCleanedlearnerCodes($learnerCode) {
        $learnerCodes = explode(',', $learnerCode);
        $codes = [];
        foreach ($learnerCodes as $code) {
            $codes[] = trim($code);
        }

        return !empty($codes) ? implode("','", $codes) : '';
    }

    public function getData($learnercode) {
        $_ObjConnection = $this->dbConnection();
        $result = [];
		$learnercode = mysqli_real_escape_string($_ObjConnection->Connect(),$learnercode);
		
        $_SelectQuery = "SELECT bm.Batch_Name AS batchname, cm.Course_Name AS coursename FROM tbl_admission ta INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course INNER JOIN tbl_batch_master bm ON ta.Admission_Batch = bm.Batch_Code WHERE ta.Admission_LearnerCode = '" . $learnercode . "'";
        $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        if (mysqli_num_rows($_Response[2]) > 0) {
            $result = mysqli_fetch_assoc($_Response[2]);
        }

        return $result;
    }

	/*Added By Sunil: Dated: 7-02-2017
        For checking the event status is reday for download permission latter*/
        public function getPermissionletter($_actionvalue,$center2) {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
				
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				$center2 = mysqli_real_escape_string($_ObjConnection->Connect(),$center2);
				
             $_SelectQuery = "SELECT * from tbl_finalexammapping  where learnercode='" . $_SESSION['User_LearnerCode'] . "' AND centercode='" .$center2. "' AND examid='".$_actionvalue."'";
             $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            }
                    catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;

            }
             return $_Response;
        }
    
        /*Added By Sunil: Dated: 7-02-2017
        For checking the event status is reday for download permission latter*/
	
	
	public function GetAll($_actionvalue, $center2, $learnerCode = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        if (empty($center2)) {
            return false;
        }
        try {
          $filter = !empty($learnerCode) ? " AND learnercode IN ('" . $this->getCleanedlearnerCodes($learnerCode) . "')" : '';
          $_SelectQuery = "SELECT examcentercode, learnercode,coursename,batchname,centercode,examcentername,examcenteraddress,examcentertehsil,examcenterdistrict,rollno,learnername,fathername,dob from tbl_finalexammapping  where centercode='" .$center2. "' AND examid='".$_actionvalue."' $filter ORDER BY rollno ASC, examcentercode ASC";
         $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        }
        catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }

        return $_Response;
    }
	
	public function GetAll1($_actionvalue,$center2)
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			
        $_SelectQuery = "SELECT learnercode,centercode,examcentername,examcenteraddress,examcentertehsil,examcenterdistrict,rollno,learnername,fathername,dob from tbl_finalexammapping  where centercode IN ($center2) AND examid='".$_actionvalue."'";
         $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			
        }
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    //put your code here
	
	
	 public function Getcenter($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select Distinct centercode from tbl_finalexammapping where examid='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    

    function GetAllMappedLearner($learnerCodes, $examId) {
        global $_ObjConnection;
        $filter = $limit = '';
        $_ObjConnection->Connect();
        try {
				$examId = mysqli_real_escape_string($_ObjConnection->Connect(),$examId);
				
            $filter .= " AND fm.learnercode IN ('" . implode("', '", $learnerCodes) . "')";
            $_SelectQuery = "SELECT DISTINCT fm.centercode AS Admission_ITGK_Code, fm.learnercode AS Admission_LearnerCode, fm.batchname AS Batch_Name, fm.coursename AS Course_Name FROM tbl_finalexammapping fm WHERE fm.examid = '" . $examId . "'" . $filter . " ORDER BY fm.centercode ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }

        return $_Response;
    }
	

    
    function UpdateLearnerAadhar($Lcode,$itgk) {
        global $_ObjConnection;        
        $_ObjConnection->Connect();
        try {
				$Lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$Lcode);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
			$_SelectQuery = "SELECT Admission_UID FROM tbl_admission fm WHERE fm.Admission_ITGK_Code = '" . $itgk . "' and
									fm.Admission_LearnerCode='".$Lcode."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			
				if($_Response[0]=='Success'){
					$_Row = mysqli_fetch_array($_Response[2]);
					$Aadharno = trim($_Row["Admission_UID"]);
					$_UpdateQuery = "Update tbl_result set learner_aadhar='" . $Aadharno . "' Where scol_no='".$Lcode."' and study_cen='".$itgk."'";
					$_response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				}            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
        return $_Response;
    }
}