<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsCenterStatusReport
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCenterStatusReport {
    //put your code here
    public function GetCenterDetails($CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CenterCode);
            
            $_SelectQuery = "select a.User_Code, a.User_LoginId as CenterCode, a.User_EmailId,a.User_MobileNo, "
                    . "b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, "
                    . "c.Organization_Name as RSPName, e.Tehsil_Name, h.CourseITGK_BlockStatus, h.CourseITGK_ExpireDate, "
                    . "f.District_Name from tbl_user_master as a INNER JOIN tbl_organization_detail as b "
                    . "on a.User_Code = b.Organization_User Inner Join tbl_organization_detail as c "
                    . "on a.User_Rsp = c.Organization_User Inner Join tbl_tehsil_master as e "
                    . "on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f "
                    . "on f.District_Code =  b.Organization_District Inner Join tbl_courseitgk_mapping as h "
					. "on a.User_LoginId=h.Courseitgk_ITGK where a.User_LoginId = " . $CenterCode . " and "
					. "h.Courseitgk_Course='RS-CIT' and h.Courseitgk_EOI='1'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
}