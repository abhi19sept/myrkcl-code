<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response3 = array();

class clsCenterPreferenceCount {

    //put your code here

public function GetAllPref1($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			//print_r($_SESSION['User_Code']);
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4'){
				//$_SelectQuery = "select b.Oasis_Admission_ITGK_Code as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref from tbl_oasis_admission b WHERE b.Oasis_Admission_Course='".$_course."' AND b.Oasis_Admission_Batch='".$_batch."' group by b.Oasis_Admission_ITGK_Code";
				$_SelectQuery = "select a.user_loginid as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_oasis_admission as b, tbl_user_master as a  
								WHERE b.Oasis_Admission_ITGK_Code = a.user_loginid AND a.User_UserRoll='7' and b.Oasis_Admission_Course='".$_course."' AND b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_ITGK_Code > 0 AND b.User_Code1 <> '4332'
								group by a.user_loginid";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_LoginUserRole == 17) {
                $_SelectQuery = "select a.user_loginid as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_user_master as a inner join tbl_oasis_admission as b on b.Oasis_Admission_ITGK_Code = a.user_loginid
								WHERE a.User_UserRoll='7' and b.Oasis_Admission_Course='".$_course."' AND
								b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "' 
								AND b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_ITGK_Code > 0 AND b.User_Code1 <> '4332' group by a.user_loginid";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
               } 
			else if ($_LoginUserRole == 14) {
                $_SelectQuery = "select a.user_loginid as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_user_master as a inner join tbl_oasis_admission as b on b.Oasis_Admission_ITGK_Code = a.user_loginid
								WHERE a.User_UserRoll='7' and b.Oasis_Admission_Course='".$_course."' AND
								a.User_Rsp='" . $_SESSION['User_Code'] . "'  
								AND b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_ITGK_Code > 0 AND b.User_Code1 <> '4332' group by a.user_loginid";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
               } 
			else {				
				$_SelectQuery = "select '" . $_SESSION['User_LoginId'] . "' as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_user_master as a inner join tbl_oasis_admission as b on b.Oasis_Admission_ITGK_Code = a.user_loginid
								WHERE b.Oasis_Admission_Course='".$_course."' AND b.Oasis_Admission_Batch='".$_batch."' AND a.user_loginid='" . $_SESSION['User_LoginId'] . "' AND b.Oasis_Admission_ITGK_Code > 0 AND b.User_Code1 <> '4332'";
			     $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}				
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetAllPref2($_course, $_batch) {
	global $_ObjConnection;
	$_ObjConnection->Connect();
	try {
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			$_LoginUserRole = $_SESSION['User_UserRoll'];
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4'){
				$_SelectQuery = "select a.user_loginid as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_user_master as a inner join tbl_oasis_admission as b on b.Oasis_Admission_ITGK_Code2 = a.user_loginid
								WHERE a.User_UserRoll='7' and b.Oasis_Admission_Course='".$_course."' AND b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_ITGK_Code2 > 0 AND b.User_Code2 <> '4332'
								group by a.user_loginid";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}
			else if ($_LoginUserRole == 17) {
               $_SelectQuery = "select a.user_loginid as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_user_master as a inner join tbl_oasis_admission as b on b.Oasis_Admission_ITGK_Code2 = a.user_loginid
								WHERE a.User_UserRoll='7' and b.Oasis_Admission_Course='".$_course."' AND
								b.Oasis_Admission_District='" . $_SESSION['Organization_District'] . "' 
								AND b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_ITGK_Code2 > 0 AND b.User_Code2 <> '4332' group by a.user_loginid";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
               } 
			else if ($_LoginUserRole == 14) {
               $_SelectQuery = "select a.user_loginid as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_user_master as a inner join tbl_oasis_admission as b on b.Oasis_Admission_ITGK_Code2 = a.user_loginid
								WHERE a.User_UserRoll='7' and b.Oasis_Admission_Course='".$_course."' AND
								a.User_Rsp='" . $_SESSION['User_Code'] . "' 
								AND b.Oasis_Admission_Batch='".$_batch."' AND b.Oasis_Admission_ITGK_Code2 > 0 AND b.User_Code2 <> '4332' group by a.user_loginid";
				$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
               } 
			else {				
				$_SelectQuery = "select '" . $_SESSION['User_LoginId'] . "' as OasisAdmissionITGKCode, Count(b.Oasis_Admission_LearnerCode) AS pref
								from tbl_user_master as a inner join tbl_oasis_admission as b on b.Oasis_Admission_ITGK_Code2 = a.user_loginid
								WHERE b.Oasis_Admission_Course='".$_course."' AND b.Oasis_Admission_Batch='".$_batch."' AND a.user_loginid='" . $_SESSION['User_LoginId'] . "' AND b.Oasis_Admission_ITGK_Code2 > 0 AND b.User_Code2 <> '4332'";
			     $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			}				
		}
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }


public function GetCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Course_Code,Course_Name from tbl_course_master where Course_Code in ('3','24')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

public function FILLBatchName($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
            $_SelectQuery = "Select Batch_Name, Batch_Code From tbl_batch_master WHERE Course_Code = '" . $_CourseCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	public function GetLearnerList($_mode, $_batch, $_rolecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_mode = mysqli_real_escape_string($_ObjConnection->Connect(),$_mode);
            $_rolecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_rolecode);
            $_batch = mysqli_real_escape_string($_ObjConnection->Connect(),$_batch);
			   if($_mode == 'one') {
				$_SelectQuery3 = "SELECT * FROM tbl_oasis_admission WHERE Oasis_Admission_Batch = '" . $_batch . "' AND Oasis_Admission_ITGK_Code='" . $_rolecode . "'";   
			   }
			   else {
				 $_SelectQuery3 = "SELECT * FROM tbl_oasis_admission WHERE Oasis_Admission_Batch = '" . $_batch . "' AND Oasis_Admission_ITGK_Code2='" . $_rolecode . "'";             
			   }
               $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
   

}
