<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgModify
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOrgModify {

    //put your code here

    public function GetDatabyCode($_AckCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$_AckCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_AckCode);
				
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9) {
                    $_SelectQuery = "Select * FROM tbl_org_master WHERE Org_Ack = '" . $_AckCode . "' AND Org_Status = 'Pending'";
                } else {
                    $_SelectQuery = "Select * FROM tbl_org_master WHERE Org_Ack = '" . $_AckCode . "' AND Org_RspLoginId = '" . $_SESSION['User_LoginId'] . "' AND Org_Status = 'Pending'";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Update($_Ackno, $_RspLoginId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_State, $_Region, $_District, $_Tehsil, $_Landmark, $_Road, $_PinCode, $_OrgEstDate, $_OrgCountry, $_docproof, $_doctype, $_PanNo, $_AreaType, $_Mohalla, $_WardNo, $_Police, $_Municipal, $_Village, $_Gram, $_Panchayat) {
        //print_r($_OrgEmail);
        global $_ObjConnection;
        $_ObjConnection->Connect();
			
			$_RspLoginId = mysqli_real_escape_string($_ObjConnection->Connect(),$_RspLoginId);
			
        $_SMS = "Dear Applicant, " . $_OrgName . " Your Application for IT-GK has been Updated on MYRKCL. Once your Submitted Information is Verified, you will get your MYRKCL login Details on your Registered Mobile Number.";
        $_SMSRSP = "Dear SP, IT-GK " . $_OrgName . " details has been updated thru your MYRKCL login. Once your Submitted Information is Verified, MYRKCL login Details will be sent to IT-GK Registered Mobile Number.";
        try {

            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $_photoname = $_docproof . '_orgdoc' . '.png';

                $_SelectQuery = "Select User_MobileNo FROM tbl_user_master WHERE User_LoginId = '" . $_RspLoginId . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                $_Row = mysqli_fetch_array($_Response1[2]);
                $_RspMobile = $_Row['User_MobileNo'];

                $_UpdateQuery = "Update tbl_org_master set Organization_Name='" . $_OrgName . "',Organization_Type='" . $_OrgType . "',"
                        . " Organization_RegistrationNo='" . $_OrgRegno . "',Organization_FoundedDate='" . $_OrgEstDate . "',"
                        . " Organization_DocType='" . $_doctype . "',"
                        . " Organization_ScanDoc='" . $_photoname . "',Organization_State='" . $_State . "',"
                        . " Organization_Region='" . $_Region . "',Organization_District='" . $_District . "',"
                        . " Organization_Tehsil='" . $_Tehsil . "',Organization_Landmark='" . $_Landmark . "',"
                        . " Organization_Road='" . $_Road . "',Organization_Country='" . $_OrgCountry . "',"
                        . " Org_Email='" . $_OrgEmail . "',Org_Mobile='" . $_OrgMobile . "',"
                        . " Org_PAN='" . $_PanNo . "',Org_AreaType='" . $_AreaType . "',"
                        . " Org_Mohalla='" . $_Mohalla . "',Org_WardNo='" . $_WardNo . "',"
                        . " Org_Police='" . $_Police . "',Org_Municipal='" . $_Municipal . "',"
                        . " Org_Village='" . $_Village . "',Org_Gram='" . $_Gram . "',"
                        . " Org_Panchayat='" . $_Panchayat . "',Org_PinCode='" . $_PinCode . "'"
                        . " WHERE Org_Ack = '" . $_Ackno . "'";

                $_DuplicateQuery = "Select * From tbl_org_master Where Org_Ack <> '" . $_Ackno . "' AND (Org_Mobile='" . $_OrgMobile . "' OR Org_Email = '" . $_OrgEmail . "')";
                $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

                $_DuplicateQuery1 = "Select * From tbl_user_master Where User_MobileNo='" . $_OrgMobile . "' OR User_EmailId = '" . $_OrgEmail . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);
               
                    if ($_Response[0] == Message::NoRecordFound && $_Response1[0] == Message::NoRecordFound) {
                        $lphotodoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/orgdocupload/' . $_photoname;
//                $lsigndoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/' . $_signname;


                        if (file_exists($lphotodoc)) {
                            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                            SendSMS($_OrgMobile, $_SMS);
                            SendSMS($_RspMobile, $_SMSRSP);
                        } else {
                            echo "Document not attached successfully, Please Re-Upload Document";
                            return;
                        }
                    } else {
//                    $_Response[0] = Message::DuplicateRecord;
//                    $_Response[1] = Message::Error;
                        echo " Mobile Number or Email Id Already Exists.";
                        return;
                    }
                
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
//print_r($_Response);
        return $_Response;
    }

}
