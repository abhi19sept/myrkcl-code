<?php
require 'DAL/classconnectionWebRKCL.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsRHLSReport {
    
    public function ShowRHLSReport($ddlstartdate,$ddlenddate)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$ddlstartdate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlstartdate);
				$ddlenddate = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlenddate);
				
               $_SelectQuery = "select Subs_User_Code,Subs_User_Company_Name,Subs_User_Gstin_no,Subs_User_First_Name,Subs_User_Email,Subs_User_Phone,Subs_User_Address,
                Subs_User_City,Subs_User_Postal_Code,Subs_User_Amount,Subs_User_Applied_Time,Subs_User_Payment_Time,Subs_User_Pay_Status,Subs_User_Del_Status,
                Subs_User_Del_Time,State_Name,District_Name from tbl_subscription_user as a inner join tbl_district_master as b
                on a.Subs_User_District=b.District_Code inner join tbl_state_master as c on a.Subs_User_State=c.State_Code 
                where (Subs_User_Applied_Time BETWEEN '".$ddlstartdate."' AND '".$ddlenddate."') and Subs_User_Pay_Status='1'";
              $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
}

?>