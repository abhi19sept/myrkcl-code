<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsGuidesUpdates
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsGuidesUpdates {
    //put your code here
    
    public function ImageSliderNew($_imageTittle,$headingname,$_sliderStatus,$_newfilename)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "insert into tbl_guide_update (headingname,imagetittle,status,photo)values('".$headingname."','".$_imageTittle."','".$_sliderStatus."','".$_newfilename."')";
            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function ShowSlideImages($_Status)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_guide_update";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetRows($_Heading)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Heading = mysqli_real_escape_string($_ObjConnection->Connect(),$_Heading);
            $_SelectQuery = "select * from tbl_guide_update where headingname='".$_Heading."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteSliderImage($_deleteid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_deleteid = mysqli_real_escape_string($_ObjConnection->Connect(),$_deleteid);
            $_DeleteQuery = "Delete From tbl_guide_update Where id='" . $_deleteid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function ShowSlideImagesForEdit($_editid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_editid = mysqli_real_escape_string($_ObjConnection->Connect(),$_editid);
            $_SelectQuery = "select * from tbl_guide_update Where id='" . $_editid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function SliderImageUpdate($_sliderid,$_imageTittleupdate,$_sliderStatusupdate,$_newfilename)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_sliderid = mysqli_real_escape_string($_ObjConnection->Connect(),$_sliderid);
            $_UpdateQuery = "Update tbl_guide_update set imagetittle = '".$_imageTittleupdate."', status = '".$_sliderStatusupdate."', photo = '".$_newfilename."' where id = '".$_sliderid."'";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetHeading($_Status)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select DISTINCT headingname from tbl_guide_update ORDER BY id DESC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
