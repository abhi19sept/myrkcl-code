<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsDropTranStatus
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response1 = array();

class clsDropTranStatus {
    //put your code here
    
    public function getdetails($_TranId) {

        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
				$_TranId = mysqli_real_escape_string($_ObjConnection->Connect(),$_TranId);
				
            if( $_SESSION['User_UserRoll']=='1' OR $_SESSION['User_UserRoll']=='2' OR $_SESSION['User_UserRoll']=='3' OR $_SESSION['User_UserRoll']=='4' OR $_SESSION['User_UserRoll']=='11' OR $_SESSION['User_UserRoll']=='8' )
            {
             $_SelectQuery = "Select * From tbl_payment_transaction WHERE  Pay_Tran_Status = 'PaymentInProcess' AND
							Pay_Tran_PG_Trnid = '".$_TranId."'"; 
             $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
            elseif($_SESSION['User_UserRoll']=='7')
            {
                $_SelectQuery = "Select * From tbl_payment_transaction WHERE Pay_Tran_ITGK = '" . $_SESSION['User_LoginId'] . "'
								AND Pay_Tran_Status = 'PaymentInProcess' AND Pay_Tran_PG_Trnid = '".$_TranId."'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            }
            
            $_Row = mysqli_fetch_array($_Response[2]);
            $_Admission_Code = $_Row['Pay_Tran_AdmissionArray'];
            
            $_SelectQuery1 = "Select * From tbl_admission WHERE Admission_Code IN ($_Admission_Code)";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response1;
    }
}
