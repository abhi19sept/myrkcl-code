<?php

/**
 * Description of clsEligibleLearnerWithMobile
 *
 * @author Mayank
 */
require 'DAL/classconnection.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$_ObjConnection = new _Connection();
$_Response = array();

class clsEligibleLearnerWithMobile {

    //put your code here

    public function FillEvent() {
        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT eventname,eventid From tbl_eligiblelearners as a inner join tbl_exammaster as b 
							on a.eventid=b.Affilate_Event where b.Event_Status='1' order by id DESC LIMIT 3";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function geteligibleDetails($_id) {

        global $_ObjConnection;

        $_ObjConnection->Connect();
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$_id = mysqli_real_escape_string($_ObjConnection->Connect(),$_id);
					
					if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 4){
						$_SelectQuery = "SELECT el.eventname, el.examdate, IF(el.status = 'Eligible', dm.District_Name, 'N/A') AS District_Name, IF(el.status = 'Eligible', tm.Tehsil_Name, 'N/A') AS Tehsil_Name, el.status, el.Reason, el.learnercode, el.learnername, el.remark, el.coursename, el.batchname, el.fathername, el.dob, ad.Admission_Mobile, el.itgkcode, el.itgkname, el.itgkdistrict, el.itgktehsil FROM tbl_eligiblelearners el INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = el.location_rkcl INNER JOIN tbl_district_master dm ON dm.District_Code = tm.Tehsil_District LEFT JOIN tbl_admission as ad ON el.learnercode=ad.Admission_LearnerCode WHERE el.eventid = '" . $_id . "' ORDER BY el.itgkcode, el.status, el.learnername";
					}
					else{
						$_SelectQuery = "SELECT el.eventname, el.examdate, IF(el.status = 'Eligible', dm.District_Name, 'N/A') AS District_Name, IF(el.status = 'Eligible', tm.Tehsil_Name, 'N/A') AS Tehsil_Name, el.status, el.Reason, el.learnercode, el.learnername, el.remark, el.coursename, el.batchname, el.fathername, el.dob, ad.Admission_Mobile, el.itgkcode, el.itgkname, el.itgkdistrict, el.itgktehsil FROM tbl_eligiblelearners el INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = el.location_rkcl INNER JOIN tbl_district_master dm ON dm.District_Code = tm.Tehsil_District LEFT JOIN tbl_admission as ad ON el.learnercode=ad.Admission_LearnerCode WHERE el.itgkcode = '" . $_SESSION['User_LoginId'] . "' AND el.eventid = '" . $_id . "' ORDER BY el.itgkcode, el.status, el.learnername";
					}
                
		   
		    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
		} else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    
}
