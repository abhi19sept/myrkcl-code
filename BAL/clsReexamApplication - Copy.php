<?php

/**
 * Description of clsReexamApplication
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsReexamApplication {

    //put your code here
    public function GetExamEvent($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $_SelectQuery = "Select a.Course_Code b.Course_Name FROM tbl_exammaster as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.a.Course_Code WHERE a.";
            $_SelectQuery = "Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " INNER JOIN tbl_event_management as c ON b.Event_Id = c.Event_ReexamEvent WHERE "
                    . " c.Event_Name='6' AND NOW() >= c.Event_Startdate AND NOW() <= c.Event_Enddate";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetExamEventManage($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $_SelectQuery = "Select a.Course_Code b.Course_Name FROM tbl_exammaster as a INNER JOIN tbl_course_master as b ON a.Course_Code = b.a.Course_Code WHERE a.";
            $_SelectQuery = "Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " WHERE NOW() >= Affilate_Astart AND NOW() <= Affilate_End";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetReexamBatch($_ExamEvent, $_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelBatch = "Select Affilate_ReexamBatches from tbl_exammaster where Affilate_Event = '" . $_ExamEvent . "'";
            $_a = $_ObjConnection->ExecuteQuery($_SelBatch, Message::SelectStatement);
            $_b = mysqli_fetch_array($_a[2]);
            $_Batch = $_b['Affilate_ReexamBatches'];

            $_SelectQuery = "Select a.Affilate_ReexamBatches, b.Batch_Name, b.Batch_Code, c.Course_Code FROM tbl_exammaster"
                    . " as a INNER JOIN tbl_batch_master as b ON b.Batch_Code IN ($_Batch) inner join tbl_course_master"
                    . " as c on c.Course_Code= b.Course_Code  where c.Course_Code = '" . $_CourseCode . "' and"
                    . " a.Affilate_Event = '" . $_ExamEvent . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll($batch, $course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_admission WHERE Admission_Batch = '" . $batch . "' AND Admission_Course = '" . $course . "' AND Admission_Payment_Status = '1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllReexam($batch, $course, $examevent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                $SelectCourse = "Select Course_Name from tbl_course_master where Course_Code='" . $course . "'";
                $_Response2 = $_ObjConnection->ExecuteQuery($SelectCourse, Message::SelectStatement);
                $row = mysqli_fetch_array($_Response2[2]);
                $courseName = $row['Course_Name'];

                $_ITGK_Code = $_SESSION['User_LoginId'];
                // echo  $_SelectQuery = "SELECT a.* FROM tbl_admission AS a INNER JOIN examdata AS b ON a.Admission_LearnerCode NOT IN (b.learnercode) WHERE a.Admission_Batch = '" . $batch . "' AND a.Admission_Course = '" . $course . "' AND a.Admission_Payment_Status = '1'";
                //$_SelectQuery = "SELECT a.*, '".$courseName."' as coursename FROM tbl_admission AS a WHERE a.Admission_LearnerCode NOT IN(SELECT learnercode FROM examdata WHERE examid = '" . $examevent . "') AND a.Admission_Batch = '" . $batch . "' AND Admission_ITGK_Code='" . $_ITGK_Code . "' AND a.Admission_Payment_Status = '1'";
                $_SelectQuery = "SELECT a.*, b.Course_Name as coursename FROM tbl_admission AS a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code WHERE a.Admission_LearnerCode NOT IN(SELECT learnercode FROM examdata WHERE examid = '" . $examevent . "') AND a.Admission_Batch = '" . $batch . "' AND Admission_ITGK_Code='" . $_ITGK_Code . "' AND a.Admission_Payment_Status = '1'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function addexamdata($_UserPermissionArray, $_EventID) {
        //print_r($_UserPermissionArray);

        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "";
            $_CountLength = count($_UserPermissionArray);


            for ($i = 0; $i < $_CountLength; $i++) {
                // $_InsertQuery = "INSERT INTO examdata set Admission_PhotoProcessing_Status ='" . $_UserPermissionArray[$i]['Values'] . "' WHERE Admission_Code = '" . $_UserPermissionArray[$i]['Function'] . "'";
                $_InsertQuery = "INSERT INTO examdata (examid,learnercode,learnername,fathername,dob,coursename,batchname,itgkcode,itgkname,itgkdistrict,itgktehsil,remark,status,paymentstatus,learnertype)
                                 SELECT '" . $_EventID . "' as examid, Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB,Admission_Course,Admission_Batch,Admission_ITGK_Code,'1',Admission_District,Admission_Tehsil,'Active','1','0','reexam' FROM tbl_admission 
                                WHERE Admission_Code = '" . $_UserPermissionArray[$i]['Function'] . "'";

                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
