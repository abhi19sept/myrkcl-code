<?php

/*
 * @author Mayank

 */

 ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCheckAdmissionTxnStatus {
    //put your code here
    
	public function ValidateTxnId($_Txnid)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Txnid = mysqli_real_escape_string($_ObjConnection->Connect(),$_Txnid);
			 $_ITGKCODE = $_SESSION['User_LoginId']; 
			 $_LoginUserRole = $_SESSION['User_UserRoll'];
					if($_LoginUserRole == '1') {
						$_SelectQuery = "Select * From tbl_payment_transaction Where (Pay_Tran_Status='PaymentInProcess' OR Pay_Tran_Status='PaymentFailure') AND Pay_Tran_ProdInfo='LearnerFeePayment' AND Pay_Tran_PG_Trnid='" . $_Txnid . "'";
					}
					 else if($_LoginUserRole == '7') {
						$_SelectQuery = "Select * From tbl_payment_transaction Where (Pay_Tran_Status='PaymentInProcess' OR Pay_Tran_Status='PaymentFailure') AND Pay_Tran_ProdInfo='LearnerFeePayment' AND Pay_Tran_PG_Trnid='" . $_Txnid . "' AND Pay_Tran_ITGK='" . $_ITGKCODE . "'";
					 }
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					if($_Response[0] == 'Success') {
						$_Row = mysqli_fetch_array($_Response[2]);
							$itgk = $_Row['Pay_Tran_ITGK'];
							$amount = $_Row['Pay_Tran_Amount'];
							$txnid = $_Row['Pay_Tran_PG_Trnid'];
							$dtime = $_Row['timestamp'];
							$course = $_Row['Pay_Tran_Course'];
							$batch = $_Row['Pay_Tran_Batch'];
						
						$_SelectQuery1 = "Select *, '" . $amount . "' as amount, '" . $itgk . "' as itgk,'" . $dtime . "' as dtime From tbl_event_management where Event_Course = '" . $course . "' AND Event_Batch = '" . $batch . "' AND Event_Category='1' AND Event_Name='3' AND Event_Payment='1' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate";
						$_SelectResponse = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
						if ($_SelectResponse[0] == 'Success') {							
							return $_SelectResponse;
						}
						else {
							//echo "No Record Found";
							return;
						}
					}
					 
            //print_r($_Response);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }  
			return $_Response;
    }
	
	
	public function AdmissionMoneySettled($txnid,$amount) {		
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $txnid = mysqli_real_escape_string($_ObjConnection->Connect(),$txnid);
				$_SelectQuery = "Select * From tbl_payment_transaction Where Pay_Tran_ProdInfo='LearnerFeePayment' AND Pay_Tran_PG_Trnid='" . $txnid . "'";
				$_SelectResponse = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			
				
					$_Row = mysqli_fetch_array($_SelectResponse[2]);					   
					$Admission_code = $_Row['Pay_Tran_AdmissionArray'];
					$_Txnid = $_Row['Pay_Tran_PG_Trnid'];					
					$firstname = $_Row['Pay_Tran_Fname'];
					$amount = $_Row['Pay_Tran_Amount'];
					$udf2 = $_Row['Pay_Tran_RKCL_Trnid'];
					$productinfo = $_Row['Pay_Tran_ProdInfo'];
					$udf1 = $_Row['Pay_Tran_ITGK'];
					
					$_SelectQuery1 = "Select Count(Admission_TranRefNo) AS AdmissionTxnid, Admission_Course, Admission_Batch FROM tbl_admission Where Admission_TranRefNo='" . $txnid . "' AND Admission_ITGK_Code='" . $udf1 . "' group by Admission_TranRefNo,Admission_Course,Admission_Batch";					
					$_SelectResponse1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);				
					$num_rows = mysqli_num_rows($_SelectResponse1[2]);
					
					$_SelectMobile = "Select User_MobileNo from tbl_user_master where User_LoginId= '".$udf1."'";
					$_SelectMobileResponse1 = $_ObjConnection->ExecuteQuery($_SelectMobile, Message::SelectStatement);
					$_Mobile = mysqli_fetch_array($_SelectMobileResponse1[2]);
					$mobileno = $_Mobile['User_MobileNo'];	
		
					if($_SelectResponse1[0] == 'Success' && $num_rows = '1') {
						//echo "if";
						$_Row1 = mysqli_fetch_array($_SelectResponse1[2]);
							$_PayTxnid=$_Row1['AdmissionTxnid'];
							  $_Course=$_Row1['Admission_Course'];
								if($_Course == '1' || $_Course == '4') {
									$admission_amount = ($_PayTxnid * 1000);
								}
								else if($_Course == '5') {
									$admission_amount = ($_PayTxnid * 3000);
								}
						if($admission_amount == $amount) {
							$_UpdateAdmission = "Update tbl_admission set Admission_Payment_Status = '1' Where Admission_ITGK_Code = '" . $udf1 . "'  AND Admission_TranRefNo = '" . $txnid . "'";
							$_AdmissionResponse = $_ObjConnection->ExecuteQuery($_UpdateAdmission, Message::UpdateStatement);
							
							$_SelectAdmissiontxn = "Select * From tbl_admission_transaction Where Admission_Transaction_Txtid='" . $txnid . "'";
							$_SelectResponse2 = $_ObjConnection->ExecuteQuery($_SelectAdmissiontxn, Message::SelectStatement);			
								if($_SelectResponse2[0]==Message::NoRecordFound) {
									$_InsertQuery = "INSERT INTO tbl_admission_transaction (Admission_Transaction_Code, Admission_Transaction_Status, Admission_Transaction_Fname, Admission_Transaction_Amount,"
										. "Admission_Transaction_Txtid, Admission_Transaction_ProdInfo,"
										. "Admission_Transaction_CenterCode,Admission_Transaction_RKCL_Txid) "
										. "Select Case When Max(Admission_Transaction_Code) Is Null Then 1 Else Max(Admission_Transaction_Code)+1 End as Admission_Transaction_Code,"
										. "'Success' as Admission_Transaction_Status,'" .$firstname. "' as Admission_Transaction_Fname,'" .$amount. "' as Admission_Transaction_Amount,"
										. "'" .$txnid. "' as Admission_Transaction_Txtid,"
										. "'" .$productinfo. "' as Admission_Transaction_ProdInfo,'" .$udf1. "' as Admission_Transaction_CenterCode, '" .$udf2. "' as Admission_Transaction_RKCL_Txid"
										. " From tbl_admission_transaction";
									$_Response1=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);						
								}
								else {
										$_UpdateAdmissionTranQuery = "Update tbl_admission_transaction set Admission_Transaction_Status = 'Success', Admission_Payment_Mode='VerifyApi', Admission_Transaction_Amount='" . $amount . "'"			
										. " Where Admission_Transaction_Txtid='" . $txnid . "' AND Admission_Transaction_CenterCode='" . $udf1 . "'";								
										$_Response4=$_ObjConnection->ExecuteQuery($_UpdateAdmissionTranQuery, Message::UpdateStatement);				
									}
							$_UpdatePayTxn = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndConfirmed', Pay_Tran_Status='PaymentReceive', Pay_Tran_Reconcile_status='Reconciled'"			
									. " Where Pay_Tran_PG_Trnid = '" . $txnid . "' AND Pay_Tran_ITGK='" . $udf1 . "'";								
							$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTxn, Message::UpdateStatement);
								
							
							$_SMS = "Dear ITGK your Payment for Trans. Ref. No. '".$_Txnid."' and Amount '".$amount."' is successful and Learner will be Confirmed .";
							SendSMS($mobileno, $_SMS);
						}
						else {
									//echo "if-else";
							$_InsertQuery = "INSERT INTO tbl_payment_refund (Payment_Refund_Id, Payment_Refund_Txnid, Payment_Refund_Amount, Payment_Refund_Status,"
								. "Payment_Refund_ProdInfo,"
								. "Payment_Refund_ITGK,Payment_Refund_RKCL_Txid) "
								. "Select Case When Max(Payment_Refund_Id) Is Null Then 1 Else Max(Payment_Refund_Id)+1 End as Payment_Refund_Id,"
								. "'" . $txnid . "' as Payment_Refund_Txnid,'" .$amount. "' as Payment_Refund_Amount, 'Refund' as Payment_Refund_Status,"
								. "'" .$productinfo. "' as Payment_Refund_ProdInfo,"
								. "'" .$udf1. "' as Payment_Refund_ITGK, '" .$udf2. "' as Payment_Refund_RKCL_Txid"
								. " From tbl_payment_refund";
								
							$_DuplicateQuery = "Select * From tbl_payment_refund Where Payment_Refund_Txnid='" . $txnid . "'";
							$_DuplicateResponse = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
								if ($_DuplicateResponse[0] == Message::NoRecordFound) {
									$_Response4=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								}
							
							$_UpdatePayTran = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndRefund', Pay_Tran_Status='PaymentToRefund', Pay_Tran_Reconcile_status='Reconciled'"			
											. " Where Pay_Tran_PG_Trnid = '" . $txnid . "' ";								
							$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTran, Message::UpdateStatement);
							
							$_SelectAdmissiontxn = "Select * From tbl_admission_transaction Where Admission_Transaction_Txtid='" . $txnid . "'";
							$_SelectResponse22 = $_ObjConnection->ExecuteQuery($_SelectAdmissiontxn, Message::SelectStatement);			
								if($_SelectResponse22[0]==Message::NoRecordFound) {
									$_InsertQuery1 = "INSERT INTO tbl_admission_transaction (Admission_Transaction_Code, Admission_Transaction_Status, Admission_Transaction_Fname, Admission_Transaction_Amount,"
										. "Admission_Transaction_Txtid, Admission_Transaction_ProdInfo,"
										. "Admission_Transaction_CenterCode,Admission_Transaction_RKCL_Txid) "
										. "Select Case When Max(Admission_Transaction_Code) Is Null Then 1 Else Max(Admission_Transaction_Code)+1 End as Admission_Transaction_Code,"
										. "'Refund' as Admission_Transaction_Status,'" .$firstname. "' as Admission_Transaction_Fname,'" .$amount. "' as Admission_Transaction_Amount,"
										. "'" .$txnid. "' as Admission_Transaction_Txtid,"
										. "'" .$productinfo. "' as Admission_Transaction_ProdInfo,'" .$udf1. "' as Admission_Transaction_CenterCode, '" .$udf2. "' as Admission_Transaction_RKCL_Txid"
										. " From tbl_admission_transaction";
									$_Response11=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);						
								}
								else {
										$_UpdateAdmissionTranQuery = "Update tbl_admission_transaction set Admission_Transaction_Status = 'Refund', Admission_Payment_Mode='VerifyAndRefund', Admission_Transaction_Amount='" . $amount . "'"			
										. " Where Admission_Transaction_Txtid='" . $txnid . "' AND Admission_Transaction_CenterCode='" . $udf1 . "'";								
										$_Response4=$_ObjConnection->ExecuteQuery($_UpdateAdmissionTranQuery, Message::UpdateStatement);				
									}
									
							$_UpdateAdmission1 = "Update tbl_admission set Admission_Payment_Status = '0' Where Admission_ITGK_Code = '" . $udf1 . "'  AND Admission_TranRefNo = '" . $txnid . "'";
							$_AdmissionResponse1 = $_ObjConnection->ExecuteQuery($_UpdateAdmission1, Message::UpdateStatement);	

							$_SMS = "Dear ITGK your Payment for Trans. Ref. No. '".$_Txnid."' and Amount '".$amount."' will be shortly refunded to you.";
							SendSMS($mobileno, $_SMS);
							
							echo "Amount for Trans. Ref. No. ".$_Txnid." and Amount ".$amount." will be shortly refunded to you";
							return; 
						}
					}
					
					else{
							//echo "else";
							$_InsertQuery = "INSERT INTO tbl_payment_refund (Payment_Refund_Id, Payment_Refund_Txnid, Payment_Refund_Amount, Payment_Refund_Status,"
								. "Payment_Refund_ProdInfo,"
								. "Payment_Refund_ITGK,Payment_Refund_RKCL_Txid) "
								. "Select Case When Max(Payment_Refund_Id) Is Null Then 1 Else Max(Payment_Refund_Id)+1 End as Payment_Refund_Id,"
								. "'" . $txnid . "' as Payment_Refund_Txnid,'" .$amount. "' as Payment_Refund_Amount, 'Refund' as Payment_Refund_Status,"
								. "'" .$productinfo. "' as Payment_Refund_ProdInfo,"
								. "'" .$udf1. "' as Payment_Refund_ITGK, '" .$udf2. "' as Payment_Refund_RKCL_Txid"
								. " From tbl_payment_refund";
								
							$_DuplicateQuery = "Select * From tbl_payment_refund Where Payment_Refund_Txnid='" . $txnid . "'";
							$_DuplicateResponse = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
								if ($_DuplicateResponse[0] == Message::NoRecordFound) {
									$_Response4=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
								}
							
							$_UpdatePayTran = "Update tbl_payment_transaction set pay_verifyapi_status='VerifiedAndRefund', Pay_Tran_Status='PaymentToRefund', Pay_Tran_Reconcile_status='Reconciled'"			
											. " Where Pay_Tran_PG_Trnid = '" . $txnid . "' ";								
							$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTran, Message::UpdateStatement);
							
							$_SelectAdmissiontxn = "Select * From tbl_admission_transaction Where Admission_Transaction_Txtid='" . $txnid . "'";
							$_SelectResponse22 = $_ObjConnection->ExecuteQuery($_SelectAdmissiontxn, Message::SelectStatement);			
								if($_SelectResponse22[0]==Message::NoRecordFound) {
									$_InsertQuery1 = "INSERT INTO tbl_admission_transaction (Admission_Transaction_Code, Admission_Transaction_Status, Admission_Transaction_Fname, Admission_Transaction_Amount,"
										. "Admission_Transaction_Txtid, Admission_Transaction_ProdInfo,"
										. "Admission_Transaction_CenterCode,Admission_Transaction_RKCL_Txid) "
										. "Select Case When Max(Admission_Transaction_Code) Is Null Then 1 Else Max(Admission_Transaction_Code)+1 End as Admission_Transaction_Code,"
										. "'Refund' as Admission_Transaction_Status,'" .$firstname. "' as Admission_Transaction_Fname,'" .$amount. "' as Admission_Transaction_Amount,"
										. "'" .$txnid. "' as Admission_Transaction_Txtid,"
										. "'" .$productinfo. "' as Admission_Transaction_ProdInfo,'" .$udf1. "' as Admission_Transaction_CenterCode, '" .$udf2. "' as Admission_Transaction_RKCL_Txid"
										. " From tbl_admission_transaction";
									$_Response11=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);						
								}
								else {
										$_UpdateAdmissionTranQuery = "Update tbl_admission_transaction set Admission_Transaction_Status = 'Refund', Admission_Payment_Mode='VerifyAndRefund', Admission_Transaction_Amount='" . $amount . "'"			
										. " Where Admission_Transaction_Txtid='" . $txnid . "' AND Admission_Transaction_CenterCode='" . $udf1 . "'";								
										$_Response4=$_ObjConnection->ExecuteQuery($_UpdateAdmissionTranQuery, Message::UpdateStatement);				
									}
									
							$_UpdateAdmission1 = "Update tbl_admission set Admission_Payment_Status = '0' Where Admission_ITGK_Code = '" . $udf1 . "'  AND Admission_TranRefNo = '" . $txnid . "'";
							$_AdmissionResponse1 = $_ObjConnection->ExecuteQuery($_UpdateAdmission1, Message::UpdateStatement);	

							$_SMS = "Dear ITGK your Payment for Trans. Ref. No. '".$_Txnid."' and Amount '".$amount."' will be shortly refunded to you.";
							SendSMS($mobileno, $_SMS);
							
							echo "Amount for Trans. Ref. No. ".$_Txnid." and Amount ".$amount." will be shortly refunded to you";
							return; 
					}
					
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response3;        
    }
	
}