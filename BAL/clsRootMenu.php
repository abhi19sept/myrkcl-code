<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of clsParentFunctionMaster
 *
 *  author Mayank
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRootMenu {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Root_Menu_Code,Root_Menu_Name,"
                    . "Status_Name,Root_Menu_Display From tbl_root_menu as a inner join tbl_status_master as b "
                    . "on a.Root_Menu_Status"
                    . "=b.Status_Code Order By Root_Menu_Display";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Root_Menu_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Root_Menu_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Root_Menu_Code);
				
            $_SelectQuery = "Select Root_Menu_Code,Root_Menu_Name,"
                    . "Root_Menu_Status,Root_Menu_Display From tbl_root_menu Where Root_Menu_Code='" . $_Root_Menu_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Root_Menu_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Root_Menu_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Root_Menu_Code);
				
            $_DeleteQuery = "Delete From tbl_root_menu Where Root_Menu_Code='" . $_Root_Menu_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_RootName,$_RootStatus,$_RootDisplay) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootName);
				$_RootStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootStatus);
				$_RootDisplay = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootDisplay);
				
            $_InsertQuery = "Insert Into tbl_root_menu(Root_Menu_Name,"  . "Root_Menu_Status, Root_Menu_Display) "
                    . "Select '" . $_RootName . "' as Root_Menu_Name,'" . $_RootStatus . "' as Root_Menu_Status,'" . $_RootDisplay . "' as Root_Menu_Display";
                   
            $_DuplicateQuery = "Select * From tbl_root_menu Where Root_Menu_Name='" . $_RootName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_RootCode,$_RootName,$_RootStatus,$_RootDisplay) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RootCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RootCode);
				
            $_UpdateQuery = "Update tbl_root_menu set Root_Menu_Name='" . $_RootName . "',"
                    . "Root_Menu_Status='" . $_RootStatus . "',Root_Menu_Display='" . $_RootDisplay . "' Where 
						Root_Menu_Code='" . $_RootCode . "'";
            $_DuplicateQuery = "Select * From tbl_parent_function_master Where Parent_Function_Name='" . $_RootName . "' "
                    . "and Parent_Function_Code <> '" . $_RootCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
