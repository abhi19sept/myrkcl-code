<?php

/**
 * Description of clsWcdSchemeDeclaration
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';
//require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdSchemeDeclaration {

public function Add($qps1, $qps2) {
		global $_ObjConnection;
        $_ObjConnection->Connect();
		
		try {
		if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
			if ($_SESSION['User_UserRoll'] == '7'){
				$_ITGKCode=$_SESSION['User_LoginId'];
				$_User_Rsp = $_SESSION['User_Rsp'];
			
				$_chkDuplicate = "select * from wcd_scheme_declaration where wcd_scheme_Itgk_code='" . $_ITGKCode ."'";
				$_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate, Message::SelectStatement);
					if ($_Response2[0] == Message::NoRecordFound){
						$_InsertQuery = "Insert Into wcd_scheme_declaration(wcd_scheme_id,wcd_scheme_Itgk_code,wcd_scheme_rsp_code,wcd_scheme_Ques_1,
						wcd_scheme_Ques_2)"
						. "Select Case When Max(wcd_scheme_id) Is Null Then 1 Else Max(wcd_scheme_id)+1 End as wcd_scheme_id,
						'" . $_ITGKCode . "' as wcd_scheme_Itgk_code, '" . $_User_Rsp . "' as wcd_scheme_rsp_code,
						'" . $qps1 . "' as wcd_scheme_Ques_1, '" . $qps2 . "' as wcd_scheme_Ques_2"
						. " From wcd_scheme_declaration";           
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				}
				else {
					$_Response[0] = Message::DuplicateRecord;
					$_Response[1] = Message::Error;
				}
			}
			else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php

				}
		}
		else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

			}
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
