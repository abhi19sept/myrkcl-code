<?php

/**
 * Description of clsddeoipayment
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsddeoipayment {

    //put your code here

    public function Update($_Code, $_PayTypeCode, $_PayType, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_eoi_ddpay (eoi_ddpay_code, dd_Transaction_Status, dd_Transaction_Name, dd_amount,"
                    . "dd_Transaction_Txtid, dd_no, dd_date,"
                    . "dd_centercode,dd_bank,dd_branch,dd_Transaction_Email) "
                    . "Select Case When Max(eoi_ddpay_code) Is Null Then 1 Else Max(eoi_ddpay_code)+1 End as eoi_ddpay_code,"
                    . "'Intermediate' as dd_Transaction_Status,'" . $_firstname . "' as dd_Transaction_Name,'" . $_amount . "' as dd_amount,"
                    . "'" . $_TranRefNo . "' as dd_Transaction_Txtid,"
                    . "'" . $_ddno . "' as dd_no,'" . $_dddate . "' as dd_date,'" . $_Code . "' as dd_centercode,'" . $_ddlBankName . "' as dd_bank,'" . $_txtBranchName . "' as dd_branch,'" . $_email . "' as dd_Transaction_Email"
                    . " From tbl_eoi_ddpay";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);



            $_InsertQuery1 = "INSERT INTO tbl_eoi_transaction (EOI_Transaction_Code, EOI_Transaction_Status, EOI_Transaction_Fname, EOI_Transaction_Amount,"
                    . "EOI_Transaction_Txtid,EOI_Payment_Mode, EOI_Transaction_ProdInfo, EOI_Transaction_Email,"
                    . "EOI_Transaction_CenterCode) "
                    . "Select Case When Max(EOI_Transaction_Code) Is Null Then 1 Else Max(EOI_Transaction_Code)+1 End as EOI_Transaction_Code,"
                    . "'Intermediate' as EOI_Transaction_Status,'" . $_firstname . "' as EOI_Transaction_Fname,'" . $_amount . "' as EOI_Transaction_Amount,"
                    . "'" . $_TranRefNo . "' as EOI_Transaction_Txtid,'DD Mode' as EOI_Payment_Mode,"
                    . "'" . $_PayTypeCode . "' as EOI_Transaction_ProdInfo,'" . $_email . "' as EOI_Transaction_Email,'" . $_Code . "' as EOI_Transaction_CenterCode"
                    . " From tbl_eoi_transaction";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);

            $_UpdateQuery = "Update tbl_courseitgk_mapping set EOI_Fee_Confirm = 8, Courseitgk_TranRefNo='" . $_TranRefNo . "' "
                    . "Where Courseitgk_ITGK='" . $_Code . "' AND Courseitgk_EOI = '" . $_PayType . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAdmissionFee($_batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select RKCL_Share FROM tbl_batch_master WHERE Batch_Code = '" . $_batchcode . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetCenterCode($_batchcode, $_coursecode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.Courseitgk_ITGK, a.Courseitgk_TranRefNo FROM tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE b.Course_Code = '" . $_coursecode . "' AND EOI_Fee_Confirm = '8'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAllDdData($_refno) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * FROM tbl_eoi_ddpay WHERE dd_Transaction_Txtid = '" . $_refno . "'";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateDDPaymentStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $Transactionid = $_SESSION['EOITrnxId'];

            $_UpdateQuery = "Update tbl_eoi_ddpay set dd_Transaction_Status = 'Success'"
                    . " Where dd_Transaction_Txtid IN ($Transactionid) ";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

            $_UpdateQuery1 = "Update tbl_eoi_transaction set EOI_Transaction_Status = 'Success'"
                    . " Where EOI_Transaction_Txtid IN ($Transactionid) ";
            $_Response1 = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);

            $_UpdateQuery2 = "Update tbl_courseitgk_mapping set EOI_Fee_Confirm = '1'"
                    . " Where Courseitgk_TranRefNo IN ($Transactionid) ";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery2, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
