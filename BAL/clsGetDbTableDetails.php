<?php

require 'DAL/classconnection.php';
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$_ObjConnection = new _Connection();
$_Response = array();

require 'DAL/sendsms.php';

class clsGetDbTableDetails {

    public function GetTables() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_copytables where cpy_status='Active'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateEoiCenterList($rows) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "insert into tbl_eoi_centerlist(EOI_Code,EOI_ECL,EOI_Year,EOI_Status_Flag,EOI_DateTime,EOI_Filename,timestamp,IsNewCRecord)
                                values " . $rows . " ON DUPLICATE KEY UPDATE EOI_Code = VALUES(EOI_Code), EOI_ECL = VALUES(EOI_ECL)";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
