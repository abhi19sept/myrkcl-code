<?php

/**
 * Description of clsAreaMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();


ini_set("memory_limit", "-1");
ini_set('max_execution_time', 0);

class clsSentSMS {

    public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //    $_SelectQuery = "Select Course_Code,Course_Name From tbl_course_master";
            $_SelectQuery = "Select distinct a.Courseitgk_Course as Course_Name, b.Course_Code From tbl_courseitgk_mapping as a INNER JOIN tbl_course_master as b ON a.Courseitgk_Course = b.Course_Name WHERE `EOI_Fee_Confirm` = 1 AND `CourseITGK_BlockStatus` = 'unblock' ORDER BY Course_Code ASC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetBatch($CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CourseCode);
				
            $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code = '" . $CourseCode . "'
                            order by Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SendMsgLearner($coursecode, $batchcode, $messagetype, $examevent) {

        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {
					
					$coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$coursecode);
					$batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
					$examevent = mysqli_real_escape_string($_ObjConnection->Connect(),$examevent);
					$messagetype = mysqli_real_escape_string($_ObjConnection->Connect(),$messagetype);
					
                   if ($messagetype == 1) {
                        $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.BioMatric_Status,a.Admission_Mobile,
                            a.Admission_Phone FROM `tbl_admission` as a  where BioMatric_Status = '0' AND Admission_Payment_Status='1' and 
                            Admission_Course='" . $coursecode . "'  AND Admission_Batch in ($batchcode)";
                    } elseif ($messagetype == 2) {
                        $_SelectQuery = "select count(t) as attcount , Admission_LearnerCode,Admission_ITGK_Code,Attendance_Admission_Code,Admission_Mobile,
                        Attendance_ITGK_Code, Admission_Batch,Admission_Course,
                        Admission_Name,Admission_Fname,BioMatric_Status from 
                        (select distinct date(Attendance_In_Time) as t,Admission_LearnerCode,Admission_ITGK_Code,
                        Attendance_Admission_Code,Attendance_ITGK_Code,Admission_Mobile,
                        Admission_Batch,Admission_Course,Admission_Name,Admission_Fname , BioMatric_Status
                        from tbl_learner_attendance as a right join tbl_admission as b on a.Attendance_Admission_Code=b.Admission_LearnerCode where Admission_Payment_Status='1' 
                        and Admission_Batch in ($batchcode) and Admission_Course='" . $coursecode . "') as tb group by Admission_LearnerCode";
                    } elseif ($messagetype == 3) {
                        $_SelectQuery = "select a.Fresult_learner,b.Admission_LearnerCode,b.Admission_Phone,b.Admission_ITGK_Code,b.Admission_Mobile, b.Admission_Name,b.Admission_Fname from tbl_assessementfinalresult as a Right join tbl_admission as b on a.Fresult_learner=b.Admission_LearnerCode and a.Fresult_itgk=b.Admission_ITGK_Code and b.Admission_Payment_Status='1' where Fresult_course='" . $coursecode . "' and Fresult_batch in ($batchcode) and Fresult_obtainmarks <= 11";
                    } elseif ($messagetype == 4) {
                        $_SelectQuery = "select a.learnercode,b.Admission_LearnerCode,b.Admission_Phone,b.Admission_ITGK_Code,b.Admission_Mobile,
                    b.Admission_Name,b.Admission_Fname from tbl_eligiblelearners as a Right join tbl_admission as b 
                    on a.learnercode=b.Admission_LearnerCode and a.itgkcode=b.Admission_ITGK_Code and 
                    b.Admission_Payment_Status='1' where eventid='" . $examevent . "' ";
                    } elseif ($messagetype == 5) {
                        $_SelectQuery = "Select a.learnercode,b.Admission_LearnerCode,b.Admission_Phone,b.Admission_ITGK_Code,b.Admission_Mobile,
                    b.Admission_Name,b.Admission_Fname From tbl_finalexammapping as a Right join tbl_admission as b 
                    on a.learnercode=b.Admission_LearnerCode and a.centercode=b.Admission_ITGK_Code and 
                    b.Admission_Payment_Status='1' WHERE examid = '" . $examevent . "' ";
                    } elseif ($messagetype == 6) {
                        $_SelectQuery = "Select a.learnercode,b.Admission_LearnerCode,b.Admission_Phone,b.Admission_ITGK_Code,b.Admission_Mobile,
                    b.Admission_Name,b.Admission_Fname From tbl_finalexammapping as a Right join tbl_admission as b 
                    on a.learnercode=b.Admission_LearnerCode and a.centercode=b.Admission_ITGK_Code and 
                    b.Admission_Payment_Status='1' WHERE examid = '" . $examevent . "' ";
                    } elseif ($messagetype == 7) {
                         $_SelectQuery = "Select a.scol_no,b.Admission_LearnerCode,b.Admission_Phone,b.Admission_ITGK_Code,b.Admission_Mobile,
                    b.Admission_Name,b.Admission_Fname From tbl_result as a Right join tbl_admission as b 
                    on a.scol_no=b.Admission_LearnerCode and a.study_cen=b.Admission_ITGK_Code and 
                    b.Admission_Payment_Status='1' WHERE exameventnameID = '" . $examevent . "' ";
                    }
                    elseif ($messagetype == 8) {
                         $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.BioMatric_Status,a.Admission_Mobile,
                            a.Admission_Phone FROM `tbl_admission` as a  where  Admission_Payment_Status='0' and 
                            Admission_Course='" . $coursecode . "'  AND Admission_Batch in ($batchcode)";
                    }
                    elseif ($messagetype == 9) {
                         $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.BioMatric_Status,a.Admission_Mobile,
                            a.Admission_Phone FROM `tbl_admission` as a  where  Admission_Payment_Status='1' and 
                            Admission_Course='" . $coursecode . "'  AND Admission_Batch in ($batchcode)";
                    }
                    elseif ($messagetype == 10) {
                         $_SelectQuery = "Select a.Admission_ITGK_Code,a.Admission_LearnerCode,a.Admission_Name,a.BioMatric_Status,a.Admission_Mobile,
                            a.Admission_Phone FROM `tbl_admission` as a  where  Admission_Payment_Status='0' and 
                            Admission_Course='" . $coursecode . "'  AND Admission_Batch in ($batchcode)";
                    }
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    //session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetExamEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT Event_Id,Event_Name From tbl_events as a inner join tbl_exammaster as b on a.Event_Id=b.Affilate_Event where b.Event_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function GetNotEnrollAdmItgk($coursecode, $batchcode, $itgkcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$coursecode);
				$batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
				$itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkcode);
				
            $_SelectQuery = "select count(Admission_LearnerCode) as ttadm from tbl_admission where 
            Admission_Course='" . $coursecode . "'  AND Admission_Batch='" . $batchcode . "' and Admission_ITGK_Code='" . $itgkcode . "' and Admission_Payment_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    public function SendMsgItgk($coursecode, $batchcode, $messagetype, $examevent) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {
					$coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$coursecode);
					$batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
					$messagetype = mysqli_real_escape_string($_ObjConnection->Connect(),$messagetype);
					$examevent = mysqli_real_escape_string($_ObjConnection->Connect(),$examevent);
					
                    if ($messagetype == 1) {
                        $_SelectQuery = "Select a.Admission_ITGK_Code,count(a.Admission_LearnerCode) as lcount, b.ITGKMOBILE  FROM `tbl_admission` as a inner join vw_itgkname_distict_rsp as b on a.Admission_ITGK_Code=b.ITGKCODE  where BioMatric_Status = '0' AND Admission_Payment_Status='1' and Admission_Course='" . $coursecode . "'  AND Admission_Batch in ($batchcode) group by a.Admission_LearnerCode ";
                    } elseif ($messagetype == 2) {
                        $_SelectQuery = "select Attendance_ITGK_Code, count(attendance_count) as attn_count,ITGKMOBILE from (select Attendance_Admission_Code,  Attendance_ITGK_Code, COUNT(DISTINCT cast(Attendance_In_Time as date)) as
                             attendance_count from tbl_learner_attendance as a inner join tbl_admission as b on 
                            a.Attendance_Admission_Code=b.Admission_LearnerCode and a.Attendance_ITGK_Code=b.Admission_ITGK_Code  
                            where b.Admission_Course='" . $coursecode . "' and b.Admission_Batch in ($batchcode) and Admission_Payment_Status='1' 
                            group by Attendance_Admission_Code having attendance_count < '14' limit 100) as T1 inner join vw_itgkname_distict_rsp as T2 on T1.Attendance_ITGK_Code = T2.ITGKCODE group by Attendance_ITGK_Code ";
                    } elseif ($messagetype == 3) {
                        $_SelectQuery = "select Admission_ITGK_Code,Admission_Batch, c.ITGKMOBILE, 
                            sum(case when b.Fresult_obtainmarks > 11 AND b.Fresult_course='" . $coursecode . "' and b.Fresult_batch = '" . $batchcode . "' then 1 else 0 end) as eligible, count(Admission_LearnerCode) AS lcount from tbl_admission as a 
                            left join tbl_assessementfinalresult as b on b.Fresult_learner=a.Admission_LearnerCode  
                            left join vw_itgkname_distict_rsp as c on a.Admission_ITGK_Code=c.ITGKCODE
                            where a.Admission_Batch in ($batchcode) AND a.Admission_Course = '" . $coursecode . "' group by a.Admission_ITGK_Code ";
                    } elseif ($messagetype == 4) {
                        $_SelectQuery = "select a.itgkcode, b.ITGKMOBILE, count(a.learnercode) lcount from tbl_eligiblelearners as a inner join vw_itgkname_distict_rsp as b 
                    on  a.itgkcode=b.ITGKCODE where eventid='" . $examevent . "' group  by a.itgkcode ";
                    } elseif ($messagetype == 5) {
                        $_SelectQuery = "select a.centercode, b.ITGKMOBILE, count(a.learnercode) lcount from tbl_finalexammapping as a inner join vw_itgkname_distict_rsp as b 
                    on  a.centercode=b.ITGKCODE where eventid='" . $examevent . "' group  by a.centercode ";
                    } elseif ($messagetype == 6) {
                        $_SelectQuery = "select a.centercode, b.ITGKMOBILE, count(a.learnercode) lcount from tbl_finalexammapping as a inner join vw_itgkname_distict_rsp as b 
                    on  a.centercode=b.ITGKCODE where eventid='" . $examevent . "' group  by a.centercode ";
                    } elseif ($messagetype == 7) {
                        $_SelectQuery = "select a.study_cen, b.ITGKMOBILE, count(a.scol_no) lcount from tbl_finalexammapping as a inner join vw_itgkname_distict_rsp as b 
                    on  a.study_cen=b.ITGKCODE where exameventnameID='" . $examevent . "' group  by a.study_cen ";
                    }
                    elseif ($messagetype == 8) {
                        if(date("d") <= '5'){
                             $dd = date("Y-m"). "-05";
                        }
                        else{
                            $dd = date(date("Y") ."-". (date('m') + 1) . "-05");
                        }
                        $_SelectQuery = "Select DISTINCT b.Courseitgk_Course, b.Courseitgk_ITGK AS ITGKCODE, a.User_MobileNo, 
b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate  From tbl_user_master as a 
INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
WHERE b.Courseitgk_Course='RS-CIT' AND STR_TO_DATE('".$dd."', '%Y-%m-%d') > b.CourseITGK_ExpireDate";
                    }
                    elseif ($messagetype == 9) {
                        $_SelectQuery = "select a.EOI_ECL,c.CourseITGK_Code, b.User_MobileNo, c.CourseITGK_ExpireDate from tbl_eoi_centerlist as a inner join tbl_user_master as b on a.EOI_ECL=b.User_LoginId inner join tbl_courseitgk_mapping as c on a.EOI_ECL=c.Courseitgk_ITGK  where EOI_Year >= (YEAR(curdate())-1) and EOI_Status_Flag='Y' and Courseitgk_Course='RS-CIT' AND TIMESTAMPDIFF(MONTH,  c.CourseITGK_ExpireDate, curdate()) <= 10";
                    }
                    elseif ($messagetype == 10) {
                        $_SelectQuery = "Select DISTINCT a.User_MobileNo, b.Courseitgk_ITGK AS ITGKCODE, b.CourseITGK_ExpireDate,
 c.Organization_AreaType, j.Block_Name, k.GP_Name From tbl_user_master as a 
INNER JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
INNER join tbl_organization_detail as c on a.User_Code=c.Organization_User
INNER join tbl_user_master as f on a.User_Rsp=f.User_Code
INNER join tbl_organization_detail as g on f.User_Code=g.Organization_User
LEFT JOIN tbl_panchayat_samiti as j on c.Organization_Panchayat=j.Block_Code
LEFT JOIN tbl_gram_panchayat as k on c.Organization_Gram=k.GP_Code
WHERE b.Courseitgk_Course='RS-CIT' AND CURDATE() > b.CourseITGK_ExpireDate AND TIMESTAMPDIFF(MONTH,  b.CourseITGK_ExpireDate, curdate()) <= 10";
                    }
                 elseif ($messagetype == 11) {
                         $_SelectQuery = "SELECT Admission_ITGK_Code,User_MobileNo, Count(Admission_Code) AS uploadcount,
sum(case when Admission_Payment_Status = '1' then 1 else 0 end) confirmcount 
FROM tbl_admission as a inner join tbl_user_master as b on 
a.Admission_ITGK_Code=b.User_LoginId WHERE Admission_Course = '" . $coursecode . "' 
AND Admission_Batch in ($batchcode) group by Admission_ITGK_Code";
                    }
                 elseif ($messagetype == 12) {
                         $_SelectQuery = "select a.Courseitgk_ITGK,b.User_MobileNo from tbl_courseitgk_mapping as a left join tbl_user_master as b on a.Courseitgk_ITGK=b.User_LoginId where Courseitgk_Course='RS-CIT' and CourseITGK_ExpireDate>=CURDATE() 
and a.Courseitgk_ITGK not in (select distinct Admission_ITGK_Code from tbl_admission WHERE Admission_Course = '" . $coursecode . "' 
AND Admission_Batch in ($batchcode))";
                    }
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    //session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function SendMsgSP($coursecode, $batchcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {
						$coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$coursecode);
						$batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$batchcode);
						
                    $_SelectQuery = "select b.User_LoginId,b.User_MobileNo, sum(case when a.BioMatric_Status='0'  then 1 else 0 end) notenrollcount , 
                                        count(a.Admission_Code) as totalcount, b.User_UserRoll
                                        from  tbl_admission as a  inner join tbl_user_master as b on a.Admission_RspName=b.User_Code where 
                                        a.Admission_Payment_Status='1' and a.Admission_Course='" . $coursecode . "' and Admission_Batch='" . $batchcode . "'
                                        group by a.Admission_RspName ";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    //session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function StoreSmsLog($loginid, $mobile, $msg, $User_UserRoll, $SMS_Log_UpdateId, $SMS_Log_SmsText) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {

                    $_InsertQuery = "insert into tbl_sms_log (SMS_Log_UserRole, SMS_Log_UserLoginId, SMS_Log_Mobile, SMS_Log_SmsTpye, SMS_Log_SmsText, SMS_Log_UpdateId, SMS_Log_SenderLoginId)  
                                    values ('" . $User_UserRoll . "', '" . $loginid . "', '" . $mobile . "', '" . $SMS_Log_SmsText . "', '" . $msg . "','" . $SMS_Log_UpdateId . "','" . $userLogin . "')";
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                } else {
                    //session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
public function StoreSmsLogNEW($r) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {

                    $_InsertQuery = "insert into tbl_sms_log (SMS_Log_UserRole, SMS_Log_UserLoginId, SMS_Log_Mobile, SMS_Log_SmsTpye, SMS_Log_SmsText, SMS_Log_UpdateId, SMS_Log_SenderLoginId)  
                                    values " . $r . "";
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                } else {
                    //session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateSmsLog($SMS_Log_UpdateId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {
					$SMS_Log_UpdateId = mysqli_real_escape_string($_ObjConnection->Connect(),$SMS_Log_UpdateId);
					
                    $_UpdateQuery = "update tbl_sms_log set SMS_Log_SmsSentStaus='1' where SMS_Log_UpdateId = '" . $SMS_Log_UpdateId . "' and "
                            . "SMS_Log_SmsSentStaus='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                } else {
                    //session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function DeleteUnsendSmsLog() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $userLogin = $_SESSION['User_LoginId'];
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {

                    $_UpdateQuery = "Delete from tbl_sms_log where SMS_Log_SmsSentStaus='0'";
                    $_Response = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
                } else {
                    //session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            } else {
                //session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    /**
     * 
     * @global _Connection $_ObjConnection
     * @param type $smsdetails FOR COMEE VALES STRING
     * @return DATA INSERT AND UPDATE 
     */
    public function Add_final_result($smsdetails) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_InsertQuery = "INSERT INTO tbl_assessementfinalresult "
                    . "(Fresult_learner,Fresult_itgk,Fresult_rsp,Fresult_district,Fresult_batch,Fresult_course,Fresult_noofassessment,Fresult_obtainmarks,Fresult_timestamp,IsNewMRecord,Re_exam_status) "
                    . "VALUES " . $smsdetails . " ON DUPLICATE KEY UPDATE "
                    . "Fresult_learner = VALUES(Fresult_learner),"
                    . "Fresult_itgk = VALUES(Fresult_itgk),"
                    . "Fresult_rsp = VALUES(Fresult_rsp),"
                    . "Fresult_district = VALUES(Fresult_district),"
                    . "Fresult_batch = VALUES(Fresult_batch),"
                    . "Fresult_course = VALUES(Fresult_course),"
                    . "Fresult_noofassessment = VALUES(Fresult_noofassessment),"
                    . "Fresult_obtainmarks = VALUES(Fresult_obtainmarks),"
                    . "Fresult_timestamp = VALUES(Fresult_timestamp),"
                    . "Re_exam_status = VALUES(Re_exam_status)";


            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::MultipleStatement);
        } catch (Exception $_e) {

            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    public function GetAdmCntInYear($itgkcode,$_EndDate,$_StartDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
			$itgkcode = mysqli_real_escape_string($_ObjConnection->Connect(),$itgkcode);
			$_EndDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_EndDate);
			$_StartDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_StartDate);
			
        $SDate = $_StartDate.' 00:00:00';
            $EDate = $_EndDate.' 23:59:59';
        try {
            $_SelectQuery = "select count(Admission_Code) as Admission_Count from tbl_admission where
			Admission_ITGK_Code='" . $itgkcode . "' and Admission_Date_Payment>='" . $SDate . "' and
			Admission_Date_Payment<='" . $EDate . "' and Admission_Payment_Status='1' and Admission_Course in (1,4)";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }
    public function GetRenewalPenaltyAmount() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Renewal_Penalty_Amount from tbl_renewal_penalty_master where Renewal_Penalty_Status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
