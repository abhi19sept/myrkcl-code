<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionEnquiryReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionEnquiryReport {
    //put your code here
    
    public function GetITGK_NCR_Details() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '7') {
                 
                $_SelectQuery = "SELECT a.*,b.Course_Name FROM tbl_admission_enquiry as a inner join tbl_course_master as b"
                        . " on a.AdmissionEnquiry_Course=b.Course_Code where AdmissionEnquiry_ITGK='" . $_SESSION['User_LoginId'] . "'";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '14') {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else {
                $_SelectQuery = "";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
 public function GetDatabyCode($_CenterCode,$_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "select Admission_Code,Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB 
                from tbl_admission where Admission_Code in ($_LearnerCode) and Admission_ITGK_Code='" . $_CenterCode . "'";
                    //. " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
