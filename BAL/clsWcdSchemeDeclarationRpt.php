<?php

/**
 * Description of clsSpChangeRpt
 *
 * @author Mayank
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWcdSchemeDeclarationRpt {

//put your code here


    public function GetDetails() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])){
					if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' 
						|| $_SESSION['User_UserRoll'] == '28' || $_SESSION['User_UserRoll'] == '8'){
							$_SelectQuery3 = "select wcd_scheme_Itgk_code,c.Organization_Name as ITGK_Name,
											d.Organization_Name as RSP_Name,e.User_LoginId as RSP_Code,
											f.District_Name,g.Tehsil_Name,wcd_scheme_Ques_1,wcd_scheme_Ques_2,datetime
											from wcd_scheme_declaration as a inner join tbl_user_master as b
											on a.wcd_scheme_Itgk_code=b.User_LoginId inner join tbl_organization_detail as c
											on b.User_Code=c.Organization_User inner join tbl_organization_detail as d
											on a.wcd_scheme_rsp_code=d.Organization_User inner join tbl_user_master as e
											on a.wcd_scheme_rsp_code=e.User_Code inner join tbl_district_master as f
											on c.Organization_District=f.District_Code inner join tbl_tehsil_master as g
											on c.Organization_Tehsil=g.Tehsil_Code order by wcd_scheme_Itgk_code";
							$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
						}
					else if($_SESSION['User_UserRoll'] == '14'){
						$_SelectQuery3 = "select wcd_scheme_Itgk_code,c.Organization_Name as ITGK_Name,
											d.Organization_Name as RSP_Name,e.User_LoginId as RSP_Code,
											f.District_Name,g.Tehsil_Name,wcd_scheme_Ques_1,wcd_scheme_Ques_2,datetime
											from wcd_scheme_declaration as a inner join tbl_user_master as b
											on a.wcd_scheme_Itgk_code=b.User_LoginId inner join tbl_organization_detail as c
											on b.User_Code=c.Organization_User inner join tbl_organization_detail as d
											on a.wcd_scheme_rsp_code=d.Organization_User inner join tbl_user_master as e
											on a.wcd_scheme_rsp_code=e.User_Code inner join tbl_district_master as f
											on c.Organization_District=f.District_Code inner join tbl_tehsil_master as g
											on c.Organization_Tehsil=g.Tehsil_Code
											where wcd_scheme_rsp_code='".$_SESSION['User_Code']."'
											order by wcd_scheme_Itgk_code";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
					}
					else if($_SESSION['User_UserRoll'] == '7'){
						$_SelectQuery3 = "select wcd_scheme_Itgk_code,c.Organization_Name as ITGK_Name,
											d.Organization_Name as RSP_Name,e.User_LoginId as RSP_Code,
											f.District_Name,g.Tehsil_Name,wcd_scheme_Ques_1,wcd_scheme_Ques_2,datetime
											from wcd_scheme_declaration as a inner join tbl_user_master as b
											on a.wcd_scheme_Itgk_code=b.User_LoginId inner join tbl_organization_detail as c
											on b.User_Code=c.Organization_User inner join tbl_organization_detail as d
											on a.wcd_scheme_rsp_code=d.Organization_User inner join tbl_user_master as e
											on a.wcd_scheme_rsp_code=e.User_Code inner join tbl_district_master as f
											on c.Organization_District=f.District_Code inner join tbl_tehsil_master as g
											on c.Organization_Tehsil=g.Tehsil_Code
											where wcd_scheme_Itgk_code='".$_SESSION['User_LoginId']."'";
						$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
					}
				}
				else {
					session_destroy();
					?>
					<script> window.location.href = "logout.php";</script> 
					<?php
				}
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       return $_Response;
    }

    public function GetDataSP($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '14'  || $_LoginUserRole == '13') {
                $_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अधिक' then 1 else 0 end) adhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अत्यधिक' then 1 else 0 end) atyadhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'सामान्य' then 1 else 0 end) samanya,
                                            sum(case when Wcd_Feedback_Ques_13 = 'कम' then 1 else 0 end) kam 
                                            from wcd_feedback_form as a  inner join vw_itgkname_distict_rsp as b on 
                                            a.Wcd_Feedback_Itgk_Code = b.ITGKCODE where Wcd_Feedback_Itgk_Code !='' and
                                            RSP_Code='" . $_SESSION['User_LoginId'] . "' and Wcd_Feedback_Batch='" . $batch . "' 
                                            group by Wcd_Feedback_Itgk_Code";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
       // return $_Response3;
    }

    public function GetDataPO($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
            if ($_LoginUserRole == '17') {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
                $_SelectQuery3 = "select Wcd_Feedback_Itgk_Code, count(Wcd_Feedback_lcode) as lcount,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अधिक' then 1 else 0 end) adhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'अत्यधिक' then 1 else 0 end) atyadhik,
                                            sum(case when Wcd_Feedback_Ques_13 = 'सामान्य' then 1 else 0 end) samanya,
                                            sum(case when Wcd_Feedback_Ques_13 = 'कम' then 1 else 0 end) kam 
                                           from wcd_feedback_form as a  inner join vw_itgkname_distict_rsp as b on 
                                            a.Wcd_Feedback_Itgk_Code = b.ITGKCODE where Wcd_Feedback_Itgk_Code !='' and 
                                            District_Code='" . $_SESSION['Organization_District'] . "'  and Wcd_Feedback_Batch='" . $batch . "' 
                                            group by Wcd_Feedback_Itgk_Code";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        //return $_Response;
    }

    
    public function GetDatafirstLearner($course, $batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				$batch = mysqli_real_escape_string($_ObjConnection->Connect(),$batch);
				
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11' || $_LoginUserRole == '28') {
                $_SelectQuery3 = "select a.*, b.District_Name from wcd_feedback_form as a inner join vw_itgkname_distict_rsp as b
                                                on a.Wcd_Feedback_Itgk_Code=b.ITGKCODE where Wcd_Feedback_Itgk_Code !='' and 
                                                Wcd_Feedback_Batch='" . $batch . "' group by Wcd_Feedback_Itgk_Code";
                $_Response3 = $_ObjConnection->ExecuteQuery($_SelectQuery3, Message::SelectStatement);
                return $_Response3;
            }
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }
}
