<?php
/**
 * Description of clsBoardMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsBoardMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Board_Code,Board_Name,"
                    . "Status_Name From tbl_board_master as a inner join tbl_status_master as b "
                    . "on a.Board_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAllHighBoard() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select HighBoard_Code,HighBoard_Name,"
                    . "Status_Name From tbl_highboard_master as a inner join tbl_status_master as b "
                    . "on a.HighBoard_Status=b.Status_Code ORDER BY HighBoard_Name";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
    public function GetDatabyCode($_Board_Code)
    {   //echo $_Board_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Board_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Board_Code);
            $_SelectQuery = "Select Board_Code,Board_Name,Board_Status"
                    . " From tbl_board_master Where Board_Code='" . $_Board_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Board_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Board_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Board_Code);
            $_DeleteQuery = "Delete From tbl_board_master Where Board_Code='" . $_Board_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_BoardName,$_BoardStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_BoardName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BoardName);
            $_InsertQuery = "Insert Into tbl_board_master(Board_Code,Board_Name,"
                    . "Board_Status) "
                    . "Select Case When Max(Board_Code) Is Null Then 1 Else Max(Board_Code)+1 End as Board_Code,"
                    . "'" . $_BoardName . "' as Board_Name,"
                    . "'" . $_BoardStatus . "' as Board_Status"
                    . " From tbl_board_master";
            $_DuplicateQuery = "Select * From tbl_board_master Where Board_Name='" . $_BoardName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_BoardCode,$_BoardName,$_BoardStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_BoardName = mysqli_real_escape_string($_ObjConnection->Connect(),$_BoardName);
            $_BoardCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_BoardCode);
            $_UpdateQuery = "Update tbl_board_master set Board_Name='" . $_BoardName . "',"
                    . "Board_Status='" . $_BoardStatus . "' Where Board_Code='" . $_BoardCode . "'";
            $_DuplicateQuery = "Select * From tbl_board_master Where Board_Name='" . $_BoardName . "' "
                    . "and Board_Code <> '" . $_BoardCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
