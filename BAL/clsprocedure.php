<?php


/**
 * Description of clsOrgDetail
 *
 * @author yogi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsprocedure {
	
	public function Add($ddlEvent)
	{	global $_ObjConnection;
		$_Response1 = array();
        $_ObjConnection->Connect();
        try {			
			$_Response=$_ObjConnection->ExecuteQuery("call proc_updateexamcode(". $ddlEvent .")", Message::SelectStatement);
				
		}
		
		catch (Exception $_e) {
										$_Response[0] = $_e->getTraceAsString();
										$_Response[1] = Message::Error;
            
								}
        return $_Response;
	}
	public function Getexception() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT  Event_Id,Event_Name ,Affilate_Event From tbl_exammaster as a inner join tbl_events as b on a.Affilate_Event=b.Event_Id ";
			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	public function Getexambatch()
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
				$_SelectQuery = "Select DISTINCT Affilate_FreshBaches FROM tbl_exammaster";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			   
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 return $_Response;
		}
		
		
		public function GeteselectedEvents($_Event)
		{
			global $_ObjConnection;
			$_ObjConnection->Connect();
			try {
					$_Event = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event);
				$_SelectQuery = "Select DISTINCT Affilate_FreshBaches,Affilate_Code  FROM tbl_exammaster where
						Affilate_Event='".$_Event."'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);			   
			} catch (Exception $_ex) {
				$_Response[0] = $_ex->getLine() . $_ex->getTrace();
				$_Response[1] = Message::Error;			   
			}
			 return $_Response;
		}
		
		
		

		public function Getexambatchname($_Batch_Code)
      {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
          echo  $_SelectQuery = "Select DISTINCT Event_Id,Event_Name,Affilate_Code From tbl_exammaster as a INNER JOIN tbl_events as b ON a. Affilate_Event = b.Event_Id inner join tbl_admission as c where a.Affilate_FreshBaches IN (" . $_Batch_Code . ") and c.Admission_Batch IN (" . $_Batch_Code . ") ";            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }		

	
	
	
	
	
}
