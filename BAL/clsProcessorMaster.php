<?php
/**
 * Description of clsProcessorMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsProcessorMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Processor_Code,Processor_Name,"
                    . "Status_Name From tbl_processor_master as a inner join tbl_status_master as b "
                    . "on a.Processor_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Processor_Code)
    {   //echo $_Processor_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Processor_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Processor_Code);
				
            $_SelectQuery = "Select Processor_Code,Processor_Name,Processor_Status"
                    . " From tbl_processor_master Where Processor_Code='" . $_Processor_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Processor_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Processor_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Processor_Code);
				
            $_DeleteQuery = "Delete From tbl_processor_master Where Processor_Code='" . $_Processor_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ProcessorName,$_ProcessorStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ProcessorName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ProcessorName);
				$_ProcessorStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_ProcessorStatus);
				
            $_InsertQuery = "Insert Into tbl_processor_master(Processor_Code,Processor_Name,"
                    . "Processor_Status) "
                    . "Select Case When Max(Processor_Code) Is Null Then 1 Else Max(Processor_Code)+1 End as Processor_Code,"
                    . "'" . $_ProcessorName . "' as Processor_Name,"
                    . "'" . $_ProcessorStatus . "' as Processor_Status"
                    . " From tbl_processor_master";
            $_DuplicateQuery = "Select * From tbl_processor_master Where Processor_Name='" . $_ProcessorName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_ProcessorCode,$_ProcessorName,$_ProcessorStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ProcessorCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ProcessorCode);
				$_ProcessorName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ProcessorName);
				$_ProcessorStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_ProcessorStatus);
				
            $_UpdateQuery = "Update tbl_processor_master set Processor_Name='" . $_ProcessorName . "',"
                    . "Processor_Status='" . $_ProcessorStatus . "' Where Processor_Code='" . $_ProcessorCode . "'";
            $_DuplicateQuery = "Select * From tbl_processor_master Where Processor_Name='" . $_ProcessorName . "' "
                    . "and Processor_Code <> '" . $_ProcessorCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
