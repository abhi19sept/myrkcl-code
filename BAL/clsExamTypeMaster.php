<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsExamTypeMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsExamTypeMaster {
    //put your code here
     //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select ExaminationType_Code,ExaminationType_Name,"
                    . "Status_Name From tbl_examinationtype_master as a inner join tbl_status_master as b "
                    . "on a.ExaminationType_Status"
                    . "=b.Status_Code";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
          //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_ExaminationType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ExaminationType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExaminationType_Code);
				
            $_SelectQuery = "Select ExaminationType_Code,ExaminationType_Name,"
                    . "ExaminationType_Status From tbl_examinationtype_master Where
					ExaminationType_Code='" . $_ExaminationType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_ExaminationType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ExaminationType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExaminationType_Code);
				
            $_DeleteQuery = "Delete From tbl_examinationtype_master Where ExaminationType_Code='" . $_ExaminationType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ExaminationTypeName,$_ExaminationTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ExaminationTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExaminationTypeName);
				$_ExaminationTypeStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExaminationTypeStatus);
				
            $_InsertQuery = "Insert Into tbl_examinationtype_master(ExaminationType_Code,ExaminationType_Name,"
                    . "ExaminationType_Status) "
                    . "Select Case When Max(ExaminationType_Code) Is Null Then 1 Else Max(ExaminationType_Code)+1 End as ExaminationType_Code,"
                    . "'" . $_ExaminationTypeName . "' as ExaminationType_Name,'" . $_ExaminationTypeStatus . "' as ExaminationType_Status"
                    . " From tbl_examinationtype_master";
            $_DuplicateQuery = "Select * From tbl_examinationtype_master Where ExaminationType_Name='" . $_ExaminationTypeName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_ExaminationTypeCode,$_ExaminationTypeName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ExaminationTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExaminationTypeCode);
				$_ExaminationTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExaminationTypeName);
				$_FunctionStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_FunctionStatus);
				
            $_UpdateQuery = "Update tbl_examinationtype_master set ExaminationType_Name='" . $_ExaminationTypeName . "',"
                    . "ExaminationType_Status='" . $_FunctionStatus . "' Where ExaminationType_Code='" . $_ExaminationTypeCode . "'";
            $_DuplicateQuery = "Select * From tbl_examinationtype_master Where ExaminationType_Name='" . $_ExaminationTypeName . "' "
                    . "and ExaminationType_Code <> '" . $_ExaminationTypeCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }

}
