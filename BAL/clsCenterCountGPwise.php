<?php

/**
 * Description of clsCenterCountGPwise
 *
 * @author Abhishek
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCenterCountGPwise {

    //put your code here
    public function GetDataAll($_district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_district = mysqli_real_escape_string($_ObjConnection->Connect(),$_district);
            $_LoginUserRole = $_SESSION['User_UserRoll'];
            if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11') {
                $_SelectQuery = "select d.District_Name,c.Block_Name, a.GP_Name, count(b.Organization_User) as cnt from tbl_gram_panchayat as a left join tbl_organization_detail as b 
on a.GP_Code=b.Organization_Gram inner join tbl_panchayat_samiti as c on a.GP_Block=c.Block_Code inner join tbl_district_master as d 
on c.Block_District=d.District_Code where d.District_Code='".$_district."' group by a.GP_Code order by d.District_Name, c.Block_Name,cnt";
            }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
