<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsDeviceMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsDeviceMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Device_Code,Device_Name,"
                    . "Status_Name From tbl_device_master as a inner join tbl_status_master as b "
                    . "on a.Device_Status"
                    . "=b.Status_Code";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Device_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Device_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Device_Code);
				
            $_SelectQuery = "Select Device_Code,Device_Name,"
                    . "Device_Status From tbl_device_master Where Device_Code='" . $_Device_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Device_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Device_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Device_Code);
				
            $_DeleteQuery = "Delete From tbl_device_master Where Device_Code='" . $_Device_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_DeviceName,$_DeviceStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DeviceName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DeviceName);
				$_DeviceStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_DeviceStatus);
				
            $_InsertQuery = "Insert Into tbl_device_master(Device_Code,Device_Name,"
                    . "Device_Status) "
                    . "Select Case When Max(Device_Code) Is Null Then 1 Else Max(Device_Code)+1 End as Device_Code,"
                    . "'" . $_DeviceName . "' as Device_Name,'" . $_DeviceStatus . "' as Device_Status"
                    . " From tbl_device_master";
            $_DuplicateQuery = "Select * From tbl_device_master Where Device_Name='" . $_DeviceName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_DeviceCode,$_DeviceName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_DeviceCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_DeviceCode);
				$_DeviceName = mysqli_real_escape_string($_ObjConnection->Connect(),$_DeviceName);
				
            $_UpdateQuery = "Update tbl_device_master set Device_Name='" . $_DeviceName . "',"
                    . "Device_Status='" . $_FunctionStatus . "' Where Device_Code='" . $_DeviceCode . "'";
            $_DuplicateQuery = "Select * From tbl_device_master Where Device_Name='" . $_DeviceName . "' "
                    . "and Device_Code <> '" . $_DeviceCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
