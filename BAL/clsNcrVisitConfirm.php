<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNcrVisitConfirm
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
require 'DAL/smtp_class.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNcrVisitConfirm {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '15'){
            $_SelectQuery = "Select a.*, b.* FROM tbl_ncr_visit as a inner join tbl_user_master as b "
                    . "on a.NCRVisit_UserCode=b.User_Code where NCRVisit_Center_Code = '" . $_SESSION['User_LoginId'] . "'";
                } elseif($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '23') {
                  $_SelectQuery = "Select a.*, b.* FROM tbl_ncr_visit as a inner join tbl_user_master as b "
                    . "on a.NCRVisit_UserCode=b.User_Code where NCRVisit_LoginId = '" . $_SESSION['User_LoginId'] . "'";  
                } elseif ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8') {
                    $_SelectQuery = "Select a.*, b.* FROM tbl_ncr_visit as a inner join tbl_user_master as b "
                    . "on a.NCRVisit_UserCode=b.User_Code";  
                } else{
                    $_SelectQuery = "";
                }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetFeedback($CenterCode, $Visitor) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
				$CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$CenterCode);
				$Visitor = mysqli_real_escape_string($_ObjConnection->Connect(),$Visitor);
				
            $_SelectQuery = "Select * from tbl_ncr_feedback where NCRFeedback_AOCode = '" . $CenterCode . "'
				and NCRFeedback_User = '" . $Visitor . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
            public function GetLearnerByPIN($biopin,$params,$id)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$biopin = mysqli_real_escape_string($_ObjConnection->Connect(),$biopin);
					$params = mysqli_real_escape_string($_ObjConnection->Connect(),$params);
					$id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
					
                    if($params == 'ITGK'){
                     $_SelectQuery = "select * from tbl_biomatric_enrollments WHERE userId='" . $_SESSION['User_Code'] . "' and status='1' and id='" . $id . "'";    
                    } else {
                    $_SelectQuery = "select a.* from tbl_biomatric_enrollments as a inner join tbl_ncr_visit as b "
                            . "on a.userId=b.NCRVisit_UserCode WHERE b.NCRVisit_Code='" . $biopin . "' and status='1' and a.id='" . $id . "'";
                    //$_SelectQuery = "SELECT a.*, b.Course_Name, c.Batch_Name FROM tbl_biomatric_registration as a INNER JOIN tbl_course_master as b ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master as c ON a.BioMatric_Admission_Batch = c.Batch_Code WHERE BioMatric_Admission_BioPIN='" . $biopin . "'";
                    }
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
        
        public function GetVisitor($biopin)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$biopin = mysqli_real_escape_string($_ObjConnection->Connect(),$biopin);
					
                    $_SelectQuery = "select a.* from tbl_biomatric_enrollments as a inner join tbl_ncr_visit as b "
                            . "on a.userId=b.NCRVisit_UserCode WHERE b.NCRVisit_Code='" . $biopin . "' and status='1'";
                    
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
        
        public function GetITGK($biopin)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
					$biopin = mysqli_real_escape_string($_ObjConnection->Connect(),$biopin);
					
                   $_SelectQuery = "select * from tbl_biomatric_enrollments WHERE userId='" . $_SESSION['User_Code'] . "' and status='1'";
                    
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
        
        public function ConfirmVisit($_VisitId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                date_default_timezone_set('Asia/Kolkata');
                $_Date = date("Y-m-d h:i:s");
				$_VisitId = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitId);
				
                $_UpdateQuery = "Update tbl_ncr_visit set NCRVisit_Status='1', confirm_by_itgk='" . $_SESSION['User_LoginId'] . "', NCRVisit_Confirm_date='" . $_Date . "'"
                    . "Where NCRVisit_Code='" . $_VisitId . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
