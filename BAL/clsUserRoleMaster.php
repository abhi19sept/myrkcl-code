<?php


require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsUserRoleMaster {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select UserRoll_Code,UserRoll_Name,Status_Name,Entity_Name From tbl_userroll_master as a inner join tbl_status_master as b "
                    . "on a.UserRoll_Status=b.Status_Code Inner Join tbl_Entity_Master as C on A.UserRoll_Entity=C.Entity_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetByEntity($_Entity) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Entity = mysqli_real_escape_string($_ObjConnection->Connect(),$_Entity);
				
            $_SelectQuery = "Select UserRoll_Code,UserRoll_Name From tbl_userroll_master Where UserRoll_Entity='" . $_Entity . "'
				and UserRoll_Status=1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetEntity() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Entity_Code,Entity_Name From tbl_Entity_Master Where Entity_Status=1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Role_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Role_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role_Code);
				
            $_SelectQuery = "Select UserRoll_Code,UserRoll_Name,UserRoll_Status,UserRoll_Entity From tbl_userroll_master Where 
							UserRoll_Code='" . $_Role_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Role_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Role_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Role_Code);
				
            $_DeleteQuery = "Delete From tbl_userroll_master Where UserRoll_Code='" . $_Role_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_RoleName, $_RoleStatus,$_RoleEntity) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				
				$_RoleName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleName);
				$_RoleEntity = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleEntity);
				
            $_InsertQuery = "Insert Into tbl_userroll_master(UserRoll_Code,UserRoll_Name,UserRoll_Status,UserRoll_Entity) "
                    . "Select Case When Max(UserRoll_Code) Is Null Then 1 Else Max(UserRoll_Code)+1 End as UserRoll_Code,"
                    . "'" . $_RoleName . "' as UserRoll_Name,'" . $_RoleStatus . "' as UserRoll_Status,"
                    . "'" . $_RoleEntity . "' as UserRoll_Entity From tbl_userroll_master";
            $_DuplicateQuery = "Select * From tbl_userroll_master Where UserRoll_Name='" . $_RoleName . "' and
								UserRoll_Entity='" . $_RoleEntity . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_RoleCode,$_RoleName, $_RoleStatus,$_RoleEntity) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RoleName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleName);
				$_RoleCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleCode);
				$_RoleEntity = mysqli_real_escape_string($_ObjConnection->Connect(),$_RoleEntity);
				
            $_UpdateQuery = "Update tbl_userroll_master set UserRoll_Name='" . $_RoleName . "',UserRoll_Status='" . $_RoleStatus . "',UserRoll_Entity='" . $_RoleEntity . "' "
                    . "Where UserRoll_Code='" . $_RoleCode . "'";
            $_DuplicateQuery = "Select * From tbl_userroll_master Where UserRoll_Name='" . $_RoleName . "' and UserRoll_Code <> '" . $_RoleCode . "' And UserRoll_Entity='" . $_RoleEntity . "'";
            $_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }

}

?>