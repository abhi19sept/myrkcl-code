<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsLearnerTypeMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsLearnerTypeMaster {
    //put your code here
    
    public function GetAll($_CourseCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_CourseCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CourseCode);
				
			if($_CourseCode=='4') {
				$_SelectQuery = "Select LearnerType_Code,LearnerType_Name,"
                    . "Status_Name From tbl_learnertype_master as a inner join tbl_status_master as b "
                    . "on a.LearnerType_Status=b.Status_Code where LearnerType_Code in ('31','32','33','34')";
			}
			else {
				$_SelectQuery = "Select LearnerType_Code,LearnerType_Name,"
                    . "Status_Name From tbl_learnertype_master as a inner join tbl_status_master as b "
                    . "on a.LearnerType_Status=b.Status_Code where LearnerType_Code NOT in ('31','32','33','34')";
			}           
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_LearnerType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LearnerType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerType_Code);
				
            $_SelectQuery = "Select LearnerType_Code,LearnerType_Name,"
                    . "LearnerType_Status From tbl_learnertype_master Where LearnerType_Code='" . $_LearnerType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_LearnerType_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LearnerType_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerType_Code);
				
            $_DeleteQuery = "Delete From tbl_learnertype_master Where LearnerType_Code='" . $_LearnerType_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_LearnerTypeName,$_LearnerTypeStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LearnerTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerTypeName);
				$_LearnerTypeStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerTypeStatus);
				
            $_InsertQuery = "Insert Into tbl_learnertype_master(LearnerType_Code,LearnerType_Name,"
                    . "LearnerType_Status) "
                    . "Select Case When Max(LearnerType_Code) Is Null Then 1 Else Max(LearnerType_Code)+1 End as LearnerType_Code,"
                    . "'" . $_LearnerTypeName . "' as LearnerType_Name,'" . $_LearnerTypeStatus . "' as LearnerType_Status"
                    . " From tbl_learnertype_master";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_LearnerType_master Where LearnerType_Name='" . $_LearnerTypeName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_LearnerTypeCode,$_LearnerTypeName,$_FunctionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_LearnerTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerTypeCode);
				$_LearnerTypeName = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerTypeName);
				$_FunctionStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_FunctionStatus);
				
            $_UpdateQuery = "Update tbl_learnertype_master set LearnerType_Name='" . $_LearnerTypeName . "',"
                    . "LearnerType_Status='" . $_FunctionStatus . "' Where LearnerType_Code='" . $_LearnerTypeCode . "'";
            $_DuplicateQuery = "Select * From tbl_learnertype_master Where LearnerType_Name='" . $_LearnerTypeName . "' "
                    . "and LearnerType_Code <> '" . $_LearnerTypeCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
