<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBankAccount
 *
 *  author Yogendra
 */
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsbankaccountforaccountlogin {

    //put your code here


    public function SHOWDATA() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {

                    $_SelectQuery = "Select Bank_Account_Code,
					Bank_Account_Name,
					Bank_Account_Number,
					Bank_Account_Type,
					Bank_Ifsc_code,
					Bank_Name,
					Bank_Micr_Code,
					Bank_Branch_Name,
					Pan_No,Pan_Name,Bank_Id_Proof,Pan_Document,Bank_Document,Bank_User_Code from tbl_bank_account";
                } else {

                    $_SelectQuery = "Select Bank_Account_Code,
				Bank_Account_Name,
				Bank_Account_Number,
				Bank_Account_Type,
				Bank_Ifsc_code,
				Bank_Name,
				Bank_Micr_Code,
				Bank_Branch_Name,
				Pan_No,Pan_Name,Bank_Id_Proof,Pan_Document,Bank_Document,Bank_User_Code from tbl_bank_account";
                }

                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct bankname from tbl_rajbank_master";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetBranch() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct branchname from tbl_rajbank_master ";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function Add($_AccountName, $_AccountNumber, $_AccountType, $_IfscCode, $_BankName, $_BranchName, $_MicrCode, $_PanNo, $_PanName, $_IdProof, $_Genid, $_Centercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_docname = $_Centercode . '_' . $_AccountNumber . '_' . $_IdProof . '.jpg';
            $_pancard = $_Centercode . '_' . $_AccountNumber . '_pancard' . '.jpg';


            $_InsertQuery = "Insert Into tbl_bank_account(Bank_Account_Code,Bank_Account_Name,Bank_Account_Number,Bank_Account_Type,"
                    . "Bank_Ifsc_code,Bank_Name,Bank_Branch_Name,Bank_Micr_Code,Pan_No,Pan_Name,Bank_Id_Proof,Bank_Document,Pan_Document,Bank_User_Code) "
                    . "Select Case When Max(Bank_Account_Code) Is Null Then 1 Else Max(Bank_Account_Code)+1 End as Bank_Account_Code,"
                    . "'" . $_AccountName . "' as Bank_Account_Name, "
                    . "'" . $_AccountNumber . "' as Bank_Account_Number,'" . $_AccountType . "' as Bank_Account_Type, "
                    . "'" . $_IfscCode . "' as Bank_Ifsc_code,'" . $_BankName . "' as Bank_Name, "
                    . "'" . $_BranchName . "' as Bank_Branch_Name,'" . $_MicrCode . "' as Bank_Micr_Code, "
                    . "'" . $_PanNo . "' as Pan_No, '" . $_PanName . "' as Pan_Name, '" . $_IdProof . "' as Bank_Id_Proof, "
                    . "'" . $_docname . "' as Bank_Document, '" . $_pancard . "' as Pan_Document, '" . $_Centercode . "' as Bank_User_Code "
                    . " From tbl_bank_account";


            $_InsertQuery1 = "Insert Into tbl_bank_account_log(Bank_Account_Code,Bank_Account_Name,Bank_Account_Number,Bank_Account_Type,"
                    . "Bank_Ifsc_code,Bank_Name,Bank_Branch_Name,Bank_Micr_Code,Pan_No,Pan_Name,Bank_Id_Proof,Bank_Document,Pan_Document,Bank_User_Code) "
                    . "Select Case When Max(Bank_Account_Code) Is Null Then 1 Else Max(Bank_Account_Code)+1 End as Bank_Account_Code,"
                    . "'" . $_AccountName . "' as Bank_Account_Name, "
                    . "'" . $_AccountNumber . "' as Bank_Account_Number,'" . $_AccountType . "' as Bank_Account_Type, "
                    . "'" . $_IfscCode . "' as Bank_Ifsc_code,'" . $_BankName . "' as Bank_Name, "
                    . "'" . $_BranchName . "' as Bank_Branch_Name,'" . $_MicrCode . "' as Bank_Micr_Code, "
                    . "'" . $_PanNo . "' as Pan_No, '" . $_PanName . "' as Pan_Name, '" . $_IdProof . "' as Bank_Id_Proof, "
                    . "'" . $_docname . "' as Bank_Document, '" . $_pancard . "' as Pan_Document, '" . $_Centercode . "' as Bank_User_Code "
                    . " From tbl_bank_account_log";
            $_DuplicateQuery = "Select * From tbl_bank_account Where Bank_User_Code='" . $_Centercode . "'";


            //$_DuplicateQuery1 = "Select * From tbl_bank_account_log Where Bank_Account_Number='" . $_AccountNumber . "' AND  //Bank_User_Code='".$_SESSION['User_LoginId']."' AND Bank_Account_Name='" . $_AccountName . "' ";

            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);

            if ($_Response[0] == Message::NoRecordFound) {
                $bankdoc = '../upload/Bankdocs/' . $_docname;
                $pandoc = '../upload/pancard/' . $_pancard;

                if (file_exists($bankdoc) && file_exists($pandoc)) {
                    $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                    $_Response1 = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
                } else {
                    echo "Please Attach Necessary Documents on first page ";
                    return;
                }
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetDetails($_actionvalue, $_Centercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery1 = "Select * from tbl_user_master Where User_LoginId= '" . $_Centercode . "'";
            $_Responses1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            if ($_Responses1[0] == 'Success') {
                $_SelectQuery = "Select * from tbl_bank_account Where Bank_User_Code= '" . $_Centercode . "'";
                $_Responses = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

                if ($_Responses[0] == 'Success') {
                    echo "Your Account Details is Already Exist. Please Use Edit Bank Details Link to Edit Bank Details.";
                    return;
                } else {
                    $_SelectQuery = "Select a.*,b.* from tbl_rajbank_master as a inner join vw_itgkname_distict_rsp as b on "
                            . "b.ITGKCODE='" . $_Centercode . "'  Where ifsccode= '" . $_actionvalue . "' Limit 1";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                }
            } else {

                echo "Invalid Center Code.";
                return;
            }



            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

}
