<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsOrgDDModify
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsOrgDDModify {
    //put your code here
    
    public function GetDatabyCode($_AckCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9) {
                    $_SelectQuery = "Select a.dd_no,a.dd_date,a.dd_branch,a.dd_Transaction_Txtid,a.dd_amount FROM tbl_ncr_ddpay AS a INNER JOIN tbl_org_master AS b "
                            . "ON a.dd_Transaction_Txtid=b.Org_TranRefNo WHERE b.Org_Ack = '" . $_AckCode . "' AND b.Org_PayStatus = '8'";
                } else {
                    $_SelectQuery = "Select * FROM tbl_org_master WHERE Org_Ack = '" . $_AckCode . "' AND Org_RspLoginId = '" . $_SESSION['User_LoginId'] . "' AND Org_PayStatus = '8'";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Update($_AckCode, $_Code, $_TranRefNo, $_ddno, $_dddate, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if(isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
		if(isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
                    
            $_ddimage = $_TranRefNo.'_ddpayment.png';
            $_UpdateQuery = "Update tbl_ncr_ddpay set dd_no='" . $_ddno . "',"
                        . " dd_date='" . $_dddate . "',dd_bank='" . $_ddlBankName . "',"
                        . " dd_branch	='" . $_txtBranchName . "',"
                        . " dd_image='" . $_ddimage . "'"                        
                        . " WHERE dd_Transaction_Txtid = '" . $_Code . "'";
            
            $ddscan = $_SERVER['DOCUMENT_ROOT'] . '/myrkcllive3/upload/Ncr_dd_payment/' . $_ddimage;
            
                if (file_exists($ddscan)) {
                  $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                                       
                } else {
                    echo "DD Image not attached successfully ,Please Re-Upload DD image";
                    return;
                }
            
                }else {
			  session_destroy();
				?>
				<script> window.location.href = "index.php"; </script> 
		<?php 
		  }
	
            }  else {
			  session_destroy();
				?>
				<script> window.location.href = "index.php"; </script> 
		<?php 
		  }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
