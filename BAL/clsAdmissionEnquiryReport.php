<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAdmissionEnquiryReport
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAdmissionEnquiryReport {
    //put your code here
    
    public function GetITGK_NCR_Details($_CourseCode, $_batch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '7') {
                 
                $_SelectQuery = "SELECT * FROM tbl_admission_enquiry as b inner join tbl_batch_master as c on c.Batch_Code='" . $_batch . "' where AdmissionEnquiry_ITGK='" . $_SESSION['User_LoginId'] . "' and AdmissionEnquiry_Course='" . $_CourseCode . "' and MONTH(b.AdmissionEnquiry_Timestamp) = MONTH(c.Timestamp) and YEAR(b.AdmissionEnquiry_Timestamp)=YEAR(c.Timestamp) order by AdmissionEnquiry_Status, AdmissionEnquiry_Timestamp";
            //die;
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '28') {
                $_SelectQuery = "SELECT * FROM tbl_admission_enquiry where  AdmissionEnquiry_Course='" . $_CourseCode . "' order by AdmissionEnquiry_Status, AdmissionEnquiry_Timestamp";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '14'){
                $_SelectQuery = "SELECT a.* FROM tbl_admission_enquiry as a inner join vw_itgkname_distict_rsp as vw on a.AdmissionEnquiry_ITGK=vw.ITGCODE 
                where  vw.RSP_Code = '" . $_SESSION['User_LoginId'] . "' and AdmissionEnquiry_Course='" . $_CourseCode . "' order by AdmissionEnquiry_Status, AdmissionEnquiry_Timestamp";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }
    
 public function GetDatabyCode($_CenterCode,$_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            $_SelectQuery = "select Admission_Code,Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB 
                from tbl_admission where Admission_Code in ($_LearnerCode) and Admission_ITGK_Code='" . $_CenterCode . "'";
                    //. " AND e.User_Rsp ='" . $_SESSION['User_Code'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

           
           $_SelectQuery = "SELECT DISTINCT Batch_Name, Batch_Code FROM tbl_batch_master WHERE Course_Code='" . $course . "'  ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}