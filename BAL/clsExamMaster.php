<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of clsStateMaster
 *
 * @author yogi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsExamMaster {
    //put your code here
	public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Affiliate_Name,Event_Name,Affilate_Code
                     From tbl_exammaster as a inner join tbl_affiliate_master as b 
                    on a.Affilate_authority=b.Affiliate_Code inner join tbl_events as c
                    on a.Affilate_Event=c.Event_Id";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
	public function GetEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from  tbl_events";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function GetpublishEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {                                    
			 $date=date("Y-m-d");
            $_SelectQuery = "Select distinct  Event_Id,Event_Name 
                     From tbl_exammaster as a inner join tbl_events as b 
                     on a.Affilate_Event=b.Event_Id where a.Event_Status='1' and a.Event_Start<='".$date."' AND a.Event_End>='".$date."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function GetEventid($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$date=date("Y-m-d");
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			
            $_SelectQuery = "Select distinct  Affilate_Code,Affilate_Event,Event_Id,Event_Name 
                     From tbl_exammaster as a inner join tbl_events as b 
                     on a.Affilate_Event=b.Event_Id where a.Event_Status='1' and a.Event_Start<='".$date."' and a.Affilate_Event='".$_actionvalue."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function GetpublishDate($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$date=date("Y-m-d");
			$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
			
            $_SelectQuery = "Select distinct  Affilate_Code,Event_Id,Event_Name ,Event_Start
                     From tbl_exammaster as a inner join tbl_events as b 
                     on a.Affilate_Event=b.Event_Id where a.Event_Status='1' and a.Event_Start<='".$date."' and a.Affilate_Event='".$_actionvalue."'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetexamFee() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from  tbl_reexamfeemaster";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	public function Getexception() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select DISTINCT  Event_Id,Event_Name ,Affilate_Event From tbl_exammaster as a inner join tbl_events as b on a.Affilate_Event=b.Event_Id ";
			
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
    public function GetDatabyCode($_examcode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_examcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_examcode);
				
            $_SelectQuery = "Select * from tbl_exammaster Where Affilate_Code='" . $_examcode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Affilate)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Affilate = mysqli_real_escape_string($_ObjConnection->Connect(),$_Affilate);
				
            $_DeleteQuery = "Delete From tbl_exammaster Where Affilate_Code='" . $_Affilate . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_Affilate,$_Event,$_Course,$_Assessment,$_fee,$_start,$_end,$_description,$freshexam,$reexam,$ddlEvents,$ddlBaches,$EventAstart,$EventAEnd,$_ddlStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Event = mysqli_real_escape_string($_ObjConnection->Connect(),$_Event);
				
             $_InsertQuery = "Insert Into tbl_exammaster(Affilate_authority,Affilate_Event,Course_Code,Affilate_Assessment,Affilate_Fee,Affilate_Astart,
			 Affilate_End,Affilate_Description,Affilate_FreshBaches,Affilate_ReexamBatches,Affilate_cases, Affilate_baches,Event_Start,Event_End,Event_Status) 
												 VALUES ('" .$_Affilate. "','" .$_Event. "',
												 '". $_Course ."',
												 '". $_Assessment ."',
												 '".$_fee."',
												 '". $_start ."',
												 '". $_end ."',
												 '". $_description."',
												 '". $freshexam ."',
												 '". $reexam ."',
												  '". $ddlEvents ."',
												  '". $ddlBaches ."',
												  '". $EventAstart ."',
												  '". $EventAEnd ."',
												'".$_ddlStatus."')";
            $_DuplicateQuery = "Select * From tbl_exammaster Where Affilate_Event='" . $_Event . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				
				// GST Insert Code
				// Old Fees :- $Gst_Invoce_BaseFee = '165.00';
                    //$Gst_Invoce_VMOU = '135';
                    //$Gst_Invoce_TutionFee = '165';
                    //$Gst_Invoce_TotalFee = '300';
					
					//$Gst_Invoce_BaseFee = '125.00';
                    //$Gst_Invoce_VMOU = '175';
                    //$Gst_Invoce_TutionFee = '125';
					$Gst_Invoce_BaseFee = '109';
                    $Gst_Invoce_VMOU = '191';
                    $Gst_Invoce_TutionFee = '109';
                 //   $Gst_Invoce_TotalFee = '300';
                                    //from 1 Aug 2019 fees changes :-
                     $Gst_Invoce_TotalFee = '350';

                $Gst_Invoce_CGST = ($Gst_Invoce_BaseFee) * 0.09;
                $Gst_Invoce_SGST = ($Gst_Invoce_BaseFee) * 0.09;
                
                $_InsertQuery_Invoice = "insert into tbl_gst_invoice_master 
										(Gst_Invoce_Category_Id, 
										Gst_Invoce_Category_Type, 
										Gst_Invoce_SAC, 
										Gst_Invoce_BaseFee, 
										Gst_Invoce_CGST, 
										Gst_Invoce_SGST, 
										Gst_Invoce_VMOU, 
										Gst_Invoce_TutionFee, 
										Gst_Invoce_TotalFee
										
										)
										values
										('" . $_Event . "', 
										'Reexam', 
										'999294', 
										'" . $Gst_Invoce_BaseFee . "', 
										'" . $Gst_Invoce_CGST . "', 
										'" . $Gst_Invoce_SGST . "', 
										'" . $Gst_Invoce_VMOU . "', 
										'" . $Gst_Invoce_TutionFee . "', 
										'" . $Gst_Invoce_TotalFee . "'
										)";
				$_Response1 = $_ObjConnection->ExecuteQuery($_InsertQuery_Invoice, Message::InsertStatement);
				
				// GST CODE Finish
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_Affilate,$_Event,$_Course,$_Assessment,$_fee,$_start,$_end,$_description,$freshexam,$reexam,$ddlEvents,$ddlBaches,$EventAstart,$EventAEnd,$_ddlStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				
            $_UpdateQuery = "Update tbl_exammaster set Affilate_authority='" . $_Affilate . "',
			". "Affilate_Event='" . $_Event . "',
					". "Course_Code='" . $_Course . "',
					". "Affilate_Assessment='" . $_Assessment . "',
					". "Affilate_Fee='" . $_fee . "',
					". "Affilate_Astart='" . $_start . "',
					". "Affilate_End='" . $_end . "',
					". "Affilate_Description='" . $_description . "',
					". "Affilate_FreshBaches='" . $freshexam . "',
					". "Affilate_ReexamBatches='" . $reexam . "',
					". "Affilate_cases='" . $ddlEvents . "',
					". "Affilate_baches='" . $ddlBaches . "',
					". "Event_Start='" . $EventAstart . "',
					". "Event_End='" . $EventAEnd . "',
					". "Event_Status='" . $_ddlStatus . "'
				Where  	Affilate_Code='" . $_Code . "'";
           
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	
	 public function Getexambatch($_Exam_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Exam_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Exam_Code);
            $_SelectQuery = "Select Affilate_FreshBaches from tbl_exammaster where Affilate_Event='".$_Exam_Code."' ";           
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function Getexambatchname($_Batch_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Batch_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Batch_Code);
				
            $_SelectQuery = "Select Batch_Code ,Batch_Name From tbl_batch_master where Batch_Code IN (" . $_Batch_Code . ")  ";            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	public function FILLAdmissionBatch($course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$course = mysqli_real_escape_string($_ObjConnection->Connect(),$course);
				
            $_SelectQuery = "SELECT DISTINCT Course_Name, Batch_Name, Batch_Code FROM tbl_batch_master as a inner join tbl_course_master as b on a.course_code=b.course_code  WHERE a.Course_Code IN ($course) ORDER BY Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
