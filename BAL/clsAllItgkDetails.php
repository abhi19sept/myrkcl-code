<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsAllItgkDetails
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsAllItgkDetails {

    //put your code here

    public function GetAllCourse() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Distinct Courseitgk_Course From tbl_courseitgk_mapping order by Courseitgk_EOI";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetITGKDetails($_Course) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Course = mysqli_real_escape_string($_ObjConnection->Connect(),$_Course);
            $_LoginRole = $_SESSION['User_UserRoll'];
            if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '11') {
                $_SelectQuery = "Select distinct a.*, b.Courseitgk_Course, b.Courseitgk_ITGK, b.Courseitgk_EOI, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
				c.Organization_Name as ITGK_Name, c.*, d.District_Name,e.Tehsil_Name,f.User_LoginId as RSP_Code,
				g.Organization_Name as RSP_Name, Ncr_Transaction_Amount, 
                                j.Municipality_Type_Name, k.Municipality_Name, l.Ward_Name, 
                                m.Block_Name, n.GP_Name, o.Village_Name From tbl_user_master as a 
                                LEFT JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
                                LEFT join tbl_organization_detail as c on a.User_Code=c.Organization_User
				LEFT join tbl_district_master as d on c.Organization_District=d.District_Code
				LEFT join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
                                LEFT join tbl_municipality_type as j on c.Organization_Municipal_Type=j.Municipality_Type_Code
                                LEFT join tbl_municipality_master as k on c.Organization_Municipal=k.Municipality_Raj_Code
                                LEFT join tbl_ward_master as l on c.Organization_WardNo=l.Ward_Code
                                LEFT join tbl_panchayat_samiti as m on c.Organization_Panchayat=m.Block_Code
                                LEFT join tbl_gram_panchayat as n on c.Organization_Gram=n.GP_Code
                                LEFT join tbl_village_master as o on c.Organization_Village=o.Village_Code
				LEFT join tbl_user_master as f on a.User_Rsp=f.User_Code
				LEFT join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_org_master as h on a.User_Ack=h.Org_Ack
                                LEFT JOIN tbl_ncr_transaction as i on h.Org_TranRefNo=i.Ncr_Transaction_Txtid
				WHERE a.User_UserRoll = '7' and b.Courseitgk_Course='" . $_Course . "' AND CURDATE() <= `CourseITGK_ExpireDate` AND b.EOI_Fee_Confirm='1'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            } else if ($_LoginRole == '14') {
                $_SelectQuery = "Select distinct a.*, b.Courseitgk_Course, b.Courseitgk_ITGK, b.Courseitgk_EOI, b.CourseITGK_UserCreatedDate, b.CourseITGK_ExpireDate,
				c.Organization_Name as ITGK_Name, c.*, d.District_Name,e.Tehsil_Name,f.User_LoginId as RSP_Code,
				g.Organization_Name as RSP_Name, Ncr_Transaction_Amount, 
                                j.Municipality_Type_Name, k.Municipality_Name, l.Ward_Name, 
                                m.Block_Name, n.GP_Name, o.Village_Name From tbl_user_master as a 
                                LEFT JOIN tbl_courseitgk_mapping as b ON a.User_LoginId = b.Courseitgk_ITGK 
                                LEFT join tbl_organization_detail as c on a.User_Code=c.Organization_User
				LEFT join tbl_district_master as d on c.Organization_District=d.District_Code
				LEFT join tbl_tehsil_master as e on c.Organization_Tehsil=e.Tehsil_Code
                                LEFT join tbl_municipality_type as j on c.Organization_Municipal_Type=j.Municipality_Type_Code
                                LEFT join tbl_municipality_master as k on c.Organization_Municipal=k.Municipality_Raj_Code
                                LEFT join tbl_ward_master as l on c.Organization_WardNo=l.Ward_Code
                                LEFT join tbl_panchayat_samiti as m on c.Organization_Panchayat=m.Block_Code
                                LEFT join tbl_gram_panchayat as n on c.Organization_Gram=n.GP_Code
                                LEFT join tbl_village_master as o on c.Organization_Village=o.Village_Code
				LEFT join tbl_user_master as f on a.User_Rsp=f.User_Code
				LEFT join tbl_organization_detail as g on f.User_Code=g.Organization_User
                                LEFT JOIN tbl_org_master as h on a.User_Ack=h.Org_Ack
                                LEFT JOIN tbl_ncr_transaction as i on h.Org_TranRefNo=i.Ncr_Transaction_Txtid
				WHERE a.User_UserRoll = '7' and b.Courseitgk_Course='" . $_Course . "' AND "
                                . "CURDATE() <= `CourseITGK_ExpireDate` AND b.EOI_Fee_Confirm='1' AND f.User_LoginId='" . $_SESSION['User_LoginId'] . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
    }

}
