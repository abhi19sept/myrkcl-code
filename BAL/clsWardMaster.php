<?php

/*
 * @author Abhi

 */

/**
 * Description of clsWardMaster
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsWardMaster {
    //put your code here
    public function GetAll($district) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		
			if ($district && is_array($district)) {
                $filter = '';
                foreach($district as $field => $val) {
                    $filter .= ' AND ' . $field . ' ' . $val;
                }
                $_SelectQuery = "Select Ward_Code, Ward_Name, Municipality_Name,District_Name, Region_Name, State_Name, Country_Name, Status_Name From tbl_ward_master as a inner join tbl_municipality_master as tm on tm.Municipality_Code = a.Ward_Municipality_Code inner join tbl_district_master as b on tm.Municipality_District = b.District_Code inner join tbl_region_master as c on b.District_Region = c.Region_Code inner join tbl_state_master as d on c.Region_State = d.State_Code inner join tbl_country_master as f on d.State_Country=f.Country_Code inner join tbl_status_master as e on a.Ward_Status=e.Status_Code $filter ORDER BY State_Name, Region_Name, Municipality_Name,District_Name, Ward_Name";
			} elseif($district) {
                 $_SelectQuery = "Select Ward_Code, Ward_Name, Municipality_Name, District_Name, Region_Name, State_Name, Status_Name, Country_Name From tbl_ward_master as a inner join tbl_municipality_master as tm on tm.Municipality_Code = a.Ward_Municipality_Code inner join tbl_district_master as b on tm.Municipality_District = b.District_Code inner join tbl_region_master as c on b.District_Region = c.Region_Code inner join tbl_state_master as d on c.Region_State = d.State_Code inner join tbl_status_master as e on a.Ward_Status=e.Status_Code inner join tbl_country_master as f on d.State_Country = f.Country_Code and a.Ward_Municipality_Code = '" . $district . "' ORDER BY State_Name, Region_Name, Municipality_Name, District_Name, Ward_Name";
            } else {
				$_SelectQuery = "Select Ward_Code, Ward_Name, Municipality_Name,District_Name, Region_Name, State_Name, Country_Name, Status_Name From tbl_ward_master as a inner join tbl_municipality_master as tm on tm.Municipality_Code = a.Ward_Municipality_Code inner join tbl_district_master as b on tm.Municipality_District = b.District_Code inner join tbl_region_master as c on b.District_Region = c.Region_Code inner join tbl_state_master as d on c.Region_State = d.State_Code inner join tbl_country_master as f on d.State_Country=f.Country_Code inner join tbl_status_master as e on a.Ward_Status=e.Status_Code ORDER BY State_Name, Region_Name, Municipality_Name,District_Name, Ward_Name";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Ward_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ward_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ward_Code);
				
            $_SelectQuery = "Select Ward_Code,Ward_Name,Ward_Municipality_Code,Ward_Status"
                    . " From tbl_ward_master Where Ward_Code='" . $_Ward_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Ward_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Ward_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Ward_Code);
				
            $_DeleteQuery = "Delete From tbl_ward_master Where Ward_Code='" . $_Ward_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_WardName,$_District,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_WardName = mysqli_real_escape_string($_ObjConnection->Connect(),$_WardName);
				$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				
            $_InsertQuery = "Insert Into tbl_ward_master(Ward_Code,Ward_Name,"
                    . "Ward_Municipality_Code,Ward_Status) "
                    . "Select Case When Max(Ward_Code) Is Null Then 1 Else Max(Ward_Code)+1 End as Ward_Code,"
                    . "'" . $_WardName . "' as Ward_Name,"
                    . "'" . $_District . "' as Ward_Municipality_Code,'" . $_Status . "' as Ward_Status"
                    . " From tbl_ward_master";
            $_DuplicateQuery = "Select * From tbl_ward_master Where Ward_Name='" . $_WardName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_WardName,$_District,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_WardName = mysqli_real_escape_string($_ObjConnection->Connect(),$_WardName);
				$_District = mysqli_real_escape_string($_ObjConnection->Connect(),$_District);
				$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
				
            $_UpdateQuery = "Update tbl_ward_master set Ward_Name='" . $_WardName . "',"
                    . "Ward_Municipality_Code='" . $_District . "',"
                    . "Ward_Status='" . $_Status . "' Where Ward_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_ward_master Where Ward_Name='" . $_WardName . "' "
                    . "and Ward_Code <> '" . $_Code . "'";
           
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
