<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsFunctionMaster
 *
 * @author Lalit
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsFunctionMaster {
    //put your code here
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Function_Code,Function_Name,Function_URL,Parent_Function_Name,"
                    . "Status_Name,Function_Display,Root_Menu_Name From tbl_function_master as a inner join tbl_parent_function_master as b "
                    . "on a.function_parent=b.Parent_Function_Code inner join tbl_status_master as c "
                    . "on a.Function_Status=c.Status_Code Inner Join tbl_root_menu as d on b.Parent_Function_Root=d.Root_Menu_Code"
                    . " Order By D.Root_Menu_Display,b.Parent_Function_Display,a.Function_Display";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Function_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Function_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Function_Code);
            $_SelectQuery = "Select Function_Code,Function_Name,Function_Parent,Function_Status,"
                    . "Function_URL,Function_Display,Root_Menu_Code From tbl_function_master as a inner join "
                    . "tbl_parent_function_master as b on a.function_parent=b.Parent_Function_Code "
                    . "Inner Join tbl_root_menu as c on b.parent_function_root=c.root_menu_code"
                    . " Where Function_Code='" . $_Function_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Function_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_Function_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Function_Code);
            $_DeleteQuery = "Delete From tbl_function_master Where Function_Code='" . $_Function_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_FunctionName, $_FunctionURL,$_FunctionParent,$_FunctionStatus,$_FunctionDisplay) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_FunctionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_FunctionName);
            $_InsertQuery = "Insert Into tbl_function_master(Function_Code,Function_Name,Function_URL,"
                    . "Function_Parent,Function_Status,Function_Display) "
                    . "Select Case When Max(Function_Code) Is Null Then 1 Else Max(Function_Code)+1 End as Function_Code,"
                    . "'" . $_FunctionName . "' as Function_Name,'" . $_FunctionURL . "' as Function_URL,"
                    . "'" . $_FunctionParent . "' as Function_Parent,'" . $_FunctionStatus . "' as Function_Status,'" . $_FunctionDisplay . "' as Function_Display"
                    . " From tbl_function_master";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_function_master Where Function_Name='" . $_FunctionName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if($_Response[0]==Message::NoRecordFound)
            {
               $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_FunctionCode,$_FunctionName, $_FunctionURL,$_FunctionParent,$_FunctionStatus,$_FunctionDisplay) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_FunctionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_FunctionName);
            $_UpdateQuery = "Update tbl_function_master set Function_Name='" . $_FunctionName . "',"
                    . "Function_URL='" . $_FunctionURL . "',Function_Parent='" . $_FunctionParent . "',"
                    . "Function_Status='" . $_FunctionStatus . "',Function_Display='" . $_FunctionDisplay . "' Where Function_Code='" . $_FunctionCode . "'";
            $_DuplicateQuery = "Select * From tbl_function_master Where Function_Name='" . $_FunctionName . "' "
                    . "and Function_Code <> '" . $_FunctionCode . "'";
            //$_DuplicateCheck = $_ObjConnection->ExecuteQuery($_DuplicateQuery);
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
