<?php
/**
 * Description of clsUpdateResultDate
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsUpdateResultDate {
    //put your code here
    public function GetExamEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select Event_Id,Event_Name from tbl_events where Result_Date=''";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	public function GetAllDetails() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_events where Result_Date!=''";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }  
  
    public function Add($_ExamID,$_ResultDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				date_default_timezone_set("Asia/Kolkata");
				$updatedate = date("Y-m-d H:i:s");
				$_ITGK_Code 	=   $_SESSION['User_LoginId'];
				
				$_ExamID = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExamID);
				$_ResultDate = mysqli_real_escape_string($_ObjConnection->Connect(),$_ResultDate);
				
           $_UpdateQuery = "Update tbl_events set Result_Date='" . $_ResultDate . "', Updated_By='".$_ITGK_Code."',
							Update_Datetime='".$updatedate."' Where Event_Id='" . $_ExamID . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_ExamIdModal,$_ResultDateModal) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				date_default_timezone_set("Asia/Kolkata");
				$updatedate = date("Y-m-d H:i:s");
				$_ITGK_Code 	=   $_SESSION['User_LoginId'];
				
				$_ExamIdModal = mysqli_real_escape_string($_ObjConnection->Connect(),$_ExamIdModal);
				$_ResultDateModal = mysqli_real_escape_string($_ObjConnection->Connect(),$_ResultDateModal);
				
           $_UpdateQuery = "Update tbl_events set Result_Date='" . $_ResultDateModal . "', Updated_By='".$_ITGK_Code."',
							Update_Datetime='".$updatedate."' Where Event_Id='" . $_ExamIdModal . "'";
           $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
