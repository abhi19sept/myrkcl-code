<?php
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsgoventryform {
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {	$_SelectQuery = "Select District_Name From tbl_district_master where District_Status='1' and District_StateCode='29'";
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);          
			}	 
		catch (Exception $_ex) {	$_Response[0] = $_ex->getLine() . $_ex->getTrace();
									$_Response[1] = Message::Error;            
								}
        return $_Response;
    } 
	
	public function GetLearnerBatch($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select a.Admission_Batch,b.Batch_Name,b.Batch_StartDate,b.Govt_Fee,b.Govt_Incentive From tbl_admission a
                                left join tbl_batch_master b on a. Admission_Batch=b.Batch_Code 
                                Where a.Admission_LearnerCode='".$_action."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	
	
	
	
    
    public function GetEmpId() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select id From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }	
	
    public function GetEmpdepartment() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select gdname From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }    
    
    public function GetEmpdesignation() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct designationname From tbl_designationname_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetBankdistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct District_Name From tbl_district_master where District_Status='1' and District_StateCode='29'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetBankname($districtid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select distinct bankname From tbl_rajbank_master where bankdistrict='" . $districtid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetDobProof() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {        
            $_SelectQuery = "Select cdocname, docid From tbl_cdoccategory_master where active = 'Yes' AND type = 'govreimbursement'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }     
    
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Status_Code,Status_Name,Status_Description From "
                    . "tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
	
    public function DeleteRecord($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_DeleteQuery = "Delete From tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;           
        }
         return $_Response;
    }
	
    public function Add($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName,
						$_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
						$_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_fileReceipt, $_fileBirth, $_EmpDobProof) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_fileReceipt1 	=	$_fileReceipt .'_payment' . '.jpg';
			$_fileBirth1   	=	$_fileBirth .'_birth' . '.jpg';				
			$_ITGK_Code 	=   $_SESSION['User_LoginId'];
			$_User_Code		=   $_SESSION['User_Code'];
			
            $_InsertQuery = "Insert Into tbl_govempentryform(learnercode, fname, faname, ldistrictname, lmobileno, lemailid, officeaddress, gdname, 
							 empid, empgpfno, designation, exammarks, aattempt, panno, empaccountno, bankdistrict, gbankname, gbranchname, gifsccode,
							 gmicrcode, attach1, attach2, userid, Govemp_ITGK_Code, dobtype)
								VALUES
							('" . $_LearnerCode . "', '" . $_EmpName . "', '" . $_FatherName . "', '" . $_EmpDistrict . "',
							 '" . $_EmpMobile . "', '" . $_EmpEmail . "', '" . $_EmpAddress . "', '" . $_EmpDeptName . "', '" . $_EmpId . "',
							 '" . $_EmpGPF . "', '" . $_EmpDesignation . "', '" . $_EmpMarks . "', '" . $_EmpExamAttempt . "', '" . $_EmpPan . "',
							 '" . $_EmpBankAccount . "', '" . $_EmpBankDistrict . "','" . $_EmpBankName . "', '" . $_EmpBranchName . "',
							 '" . $_EmpIFSC . "', '" . $_EmpMICR . "', '" . $_fileReceipt1 . "', '" . $_fileBirth1 . "', '" . $_User_Code . "', '" . $_ITGK_Code . "', '" . $_EmpDobProof ."')";
            $_DuplicateQuery = "Select * From tbl_govempentryform Where learnercode='" . $_LearnerCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
				$receiptdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_payment/'.$_fileReceipt1;
				$birthdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_birth/'.$_fileBirth1;
				
				if ( file_exists($receiptdoc) && file_exists($birthdoc) ) {					
					$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Government Reimbursement is Successfully Applied to RKCL";
					$_Mobile=$_EmpMobile;
					SendSMS($_Mobile, $_SMS);				
				   }
					else
					{
						echo "Please Re-Upload Documents Again.";
						return;
					}               
            }
			else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function Update($_Status_Code,$_StatusName, $_Status_Description) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_status_master set Status_Name='" . $_StatusName . "',"	. "Status_Description='" . $_Status_Description . "' "
							. "Where Status_Code='" . $_Status_Code . "'";
            $_DuplicateQuery = "Select * From tbl_status_master Where Status_Name='" . $_StatusName . "' and "	. "Status_Code <> '" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;        
    }
	
	public function GetAllDETAILS($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_ITGK=$_SESSION['User_LoginId'];
		    $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_action."' AND Admission_ITGK_Code='" . $_ITGK . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
      
    /* Added By SUNIL: 2-2-2017 for checking the course of learner */
    public function checkLearnerCourse($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_ITGK=$_SESSION['User_LoginId'];
            $_DuplicateQuery = "Select * From tbl_govempentryform Where learnercode='" . $_action . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
              $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_action."' AND Admission_ITGK_Code='" . $_ITGK . "' AND Admission_Course in (1,4,2)";


			  //$_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_action."' AND Admission_ITGK_Code='" . $_ITGK . "' AND Admission_Course='4'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } 
            else 
            {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    /*if the Amount Reimbursed to Beneficiary's Account*/
    public function checkLearnerEMPid($_action, $_empid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_ITGK=$_SESSION['User_LoginId'];
                $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND Govemp_ITGK_Code='" . $_ITGK . "'  AND empid='" . $_empid . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    public function checkLearnerStatus($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_ITGK=$_SESSION['User_LoginId'];
                $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND Govemp_ITGK_Code='" . $_ITGK . "' AND trnpending='6'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    public function checkLearnerStatusZero($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_ITGK=$_SESSION['User_LoginId'];
                $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND Govemp_ITGK_Code='" . $_ITGK . "' AND trnpending='0'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    
    // for checking the validity of learner code
    public function checkLearnerCode($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $_ITGK=$_SESSION['User_LoginId'];
                $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_action."' AND Admission_ITGK_Code='" . $_ITGK . "'";
                $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    /* Added By SUNIL: 2-2-2017 for checking the course of learner */
    
    /* Added By SUNIL: 15-3-2017 for Adding EMployee Detail in tbl_govempentryform table  */
     public function EditEmp($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName,
						$_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpFee, $_EmpIncentive, $_EmpTotalAmt, $_Empbatchid, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
						$_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_EmpDob, $_EmpDobApp, $_fileReceipt, $_fileDPLetter, $_fileRSCITCer)
             { //echo "hi";die;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
            {
                    $_fileReceipt1      =	$_fileReceipt .'_cancelcheck' . '.jpg';
                    $_fileDPLetter1     =	$_fileDPLetter .'_dpletter' . '.jpg';				
                    $_fileRSCITCer1     =	$_fileRSCITCer .'_rscitcer' . '.jpg';				
                    $_ITGK_Code         =   $_SESSION['User_LoginId'];
                    $_User_Code         =   $_SESSION['User_Code'];

                    $_UpdateQuery = "Update tbl_govempentryform set 
                            fname='" . $_EmpName . "',
                            faname='" . $_FatherName . "',
                            ldistrictname='" . $_EmpDistrict . "',
                            lmobileno='" . $_EmpMobile . "',
                            lemailid='" . $_EmpEmail . "',
                            officeaddress='" . $_EmpAddress . "',
                            gdname='" . $_EmpDeptName . "',
                            empid='" . $_EmpId . "',
                            empgpfno='" . $_EmpGPF . "',
                            designation='" . $_EmpDesignation . "',
                            exammarks='" . $_EmpMarks . "',
                            aattempt='" . $_EmpExamAttempt . "',
                            fee='" . $_EmpFee . "',
                            incentive='" . $_EmpIncentive . "',
                            trnamount='" . $_EmpTotalAmt . "',
                            batchid='" . $_Empbatchid . "',
                            panno='" . $_EmpPan . "',
                            empaccountno='" . $_EmpBankAccount . "',
                            bankdistrict='" . $_EmpBankDistrict . "',
                            gbankname='" . $_EmpBankName . "',
                            gbranchname='" . $_EmpBranchName . "',
                            gifsccode='" . $_EmpIFSC . "',
                            gmicrcode='" . $_EmpMICR . "',
                            attach4='" . $_fileReceipt1 . "',
                            attach1='',
                            attach5='" . $_fileDPLetter1 . "',
                            attach3='" . $_fileRSCITCer1 . "',
                            empdob='" . $_EmpDob . "',
                            empdobapp='" . $_EmpDobApp . "',
                            userid='" . $_User_Code . "',
                            trnpending='12',
                            Govemp_ITGK_Code='" . $_ITGK_Code . "'

                            Where learnercode='" . $_LearnerCode . "'";//die;

         //dobtype='" . $_EmpDobProof . "'
                            $cancelcheck = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_cancelcheck/'.$_fileReceipt1;
                            $dpletter = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_dpletter/'.$_fileDPLetter1;
                            $rscitcer = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_rscitcer/'.$_fileRSCITCer1;


                            if ( file_exists($cancelcheck) && file_exists($dpletter) && file_exists($rscitcer) ) 
                               {					
                                    $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

                                    $_Date = date("Y-m-d h:i:s");	
                                    $_InsertQuery_log = "INSERT INTO tbl_govstatusupdatelog (Govemp_id, Govemp_ITGK_Code, Govemp_Learnercode, Govemp_Process_Status,"
                                    . "Govemp_Datetime) "
                                    . "Select Case When Max(Govemp_id) Is Null Then 1 Else Max(Govemp_id)+1 End as Govemp_id,"
                            . "'" . $_SESSION['User_LoginId'] . "' as Govemp_ITGK_Code,'" . $_LearnerCode . "' as Govemp_Learnercode,'12' as Govemp_Process_Status,"
                            . "'" . $_Date . "' as Govemp_Datetime"
                                    . " From tbl_govstatusupdatelog";
                                    $_Response_log=$_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);

                                    //$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Government Reimbursement Process is Successfully Updated.";
                                    $_SMS = "Dear Learner " . $_LearnerCode . " Your application for RS-CIT fee Reimbursement is still not completed. Kindly print acknowledgement receipt (Step2), sign it and upload it (Step3) on myrkcl.com ";
                                    $_Mobile=$_EmpMobile;
                                    SendSMS($_Mobile, $_SMS);				
                               }
                            else
                                {
                                    echo "Please Re-Upload Documents Again.";
                                    return;
                                } 
                                    
            }                  
             else 
                {
                    session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }                        
                                    
                                    
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    
    
    
     public function EditEmpReSub($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName,
						$_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpFee, $_EmpIncentive, $_EmpTotalAmt, $_Empbatchid, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
						$_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_EmpDob, $_EmpDobApp, $_fileReceipt, $_fileDPLetter, $_fileRSCITCer)
             {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
             {
			$_fileReceipt1      =	$_fileReceipt .'_cancelcheck' . '.jpg';
			$_fileDPLetter1     =	$_fileDPLetter .'_dpletter' . '.jpg';				
			$_fileRSCITCer1     =	$_fileRSCITCer .'_rscitcer' . '.jpg';				
			$_ITGK_Code         =   $_SESSION['User_LoginId'];
			$_User_Code         =   $_SESSION['User_Code'];
            
                        $_UpdateQuery = "Update tbl_govempentryform set 
                                fname='" . $_EmpName . "',
                                faname='" . $_FatherName . "',
                                ldistrictname='" . $_EmpDistrict . "',
                                lmobileno='" . $_EmpMobile . "',
                                lemailid='" . $_EmpEmail . "',
                                officeaddress='" . $_EmpAddress . "',
                                gdname='" . $_EmpDeptName . "',
                                empid='" . $_EmpId . "',
                                empgpfno='" . $_EmpGPF . "',
                                designation='" . $_EmpDesignation . "',
                                exammarks='" . $_EmpMarks . "',
                                aattempt='" . $_EmpExamAttempt . "',
                                fee='" . $_EmpFee . "',
                                incentive='" . $_EmpIncentive . "',
                                trnamount='" . $_EmpTotalAmt . "',
                                batchid='" . $_Empbatchid . "',
                                panno='" . $_EmpPan . "',
                                empaccountno='" . $_EmpBankAccount . "',
                                bankdistrict='" . $_EmpBankDistrict . "',
                                gbankname='" . $_EmpBankName . "',
                                gbranchname='" . $_EmpBranchName . "',
                                gifsccode='" . $_EmpIFSC . "',
                                gmicrcode='" . $_EmpMICR . "',
                                attach4='" . $_fileReceipt1 . "',
                                attach5='" . $_fileDPLetter1 . "',
                                attach3='" . $_fileRSCITCer1 . "',
                                attach1='',
                                empdob='" . $_EmpDob . "',
                                empdobapp='" . $_EmpDobApp . "',
                                userid='" . $_User_Code . "',
                                trnpending='12',
                                Govemp_ITGK_Code='" . $_ITGK_Code . "'
                               
                                Where learnercode='" . $_LearnerCode . "'";
                                
             //dobtype='" . $_EmpDobProof . "'
                                $cancelcheck = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_cancelcheck/'.$_fileReceipt1;
                                $dpletter = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_dpletter/'.$_fileDPLetter1;
                                $rscitcer = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_rscitcer/'.$_fileRSCITCer1;
                
				
				if ( file_exists($cancelcheck) && file_exists($dpletter) && file_exists($rscitcer) ) 
                                   {					
					$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                                        
                                        
                                        $_Date = date("Y-m-d h:i:s");	
				        $_InsertQuery_log = "INSERT INTO tbl_govstatusupdatelog (Govemp_id, Govemp_ITGK_Code, Govemp_Learnercode, Govemp_Process_Status,"
                                        . "Govemp_Datetime) "
                                        . "Select Case When Max(Govemp_id) Is Null Then 1 Else Max(Govemp_id)+1 End as Govemp_id,"
                                . "'" . $_SESSION['User_LoginId'] . "' as Govemp_ITGK_Code,'" . $_LearnerCode . "' as Govemp_Learnercode,'12' as Govemp_Process_Status,"
                                . "'" . $_Date . "' as Govemp_Datetime"
                                        . " From tbl_govstatusupdatelog";
                                        $_Response_log=$_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);
                                        
                                        
                                        
                                        
					//$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Government Reimbursement Process is Successfully Updated.";
                                        $_SMS = "Dear Learner " . $_LearnerCode . " Your application for RS-CIT fee Reimbursement is still not completed. Kindly print acknowledgement receipt (Step2), sign it and upload it (Step3) on myrkcl.com ";
					$_Mobile=$_EmpMobile;
					SendSMS($_Mobile, $_SMS);				
				   }
                                else
                                    {
					echo "Please Re-Upload Documents Again.";
					return;
                                    } 
                                    
             }
             else 
             {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    
    
    
    
    public function AddEmp($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName,
						$_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpFee, $_EmpIncentive, $_EmpTotalAmt, $_Empbatchid, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
						$_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_EmpDob, $_EmpDobApp, $_fileReceipt, $_fileDPLetter, $_fileRSCITCer) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
                {
                        $_fileReceipt1      =	$_fileReceipt .'_cancelcheck' . '.jpg';
			$_fileDPLetter1     =	$_fileDPLetter .'_dpletter' . '.jpg';				
			$_fileRSCITCer1     =	$_fileRSCITCer .'_rscitcer' . '.jpg';				
			$_ITGK_Code         =   $_SESSION['User_LoginId'];
			$_User_Code         =   $_SESSION['User_Code'];
			
                        $_InsertQuery = "Insert Into tbl_govempentryform(learnercode, fname, faname, ldistrictname, lmobileno, lemailid, officeaddress, gdname, 
							 empid, empgpfno, designation, exammarks, aattempt, fee, incentive, trnamount, batchid, panno, empaccountno, bankdistrict, gbankname, gbranchname, gifsccode,
							 gmicrcode, attach4, attach5, attach3, empdob, empdobapp, userid, Govemp_ITGK_Code, dobtype, appliedby)
								VALUES
							('" . $_LearnerCode . "', '" . $_EmpName . "', '" . $_FatherName . "', '" . $_EmpDistrict . "',
							 '" . $_EmpMobile . "', '" . $_EmpEmail . "', '" . $_EmpAddress . "', '" . $_EmpDeptName . "', '" . $_EmpId . "',
							 '" . $_EmpGPF . "', '" . $_EmpDesignation . "', '" . $_EmpMarks . "', '" . $_EmpExamAttempt . "', '" . $_EmpFee . "', "
                     . "                                 '" . $_EmpIncentive . "', '" . $_EmpTotalAmt . "', '" . $_Empbatchid . "', '" . $_EmpPan . "',
							 '" . $_EmpBankAccount . "', '" . $_EmpBankDistrict . "','" . $_EmpBankName . "', '" . $_EmpBranchName . "',
							 '" . $_EmpIFSC . "', '" . $_EmpMICR . "', '" . $_fileReceipt1 . "', '" . $_fileDPLetter1 . "',  '" . $_fileRSCITCer1 . "', "
                     . "                                 '" . $_EmpDob . "', '" . $_EmpDobApp . "', '" . $_User_Code . "', '" . $_ITGK_Code . "', '" . $_EmpDobProof ."', '" . $_SESSION['User_UserRoll'] ."')";
             
                        $_DuplicateQuery = "Select * From tbl_govempentryform Where learnercode='" . $_LearnerCode . "'";
                        $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
                        if($_Response[0]==Message::NoRecordFound)
                            {
                                    //$receiptdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_payment/'.$_fileReceipt1;
                                    //$birthdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_birth/'.$_fileBirth1;
                                    $cancelcheck = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_cancelcheck/'.$_fileReceipt1;
                                    $dpletter = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_dpletter/'.$_fileDPLetter1;
                                    $rscitcer = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_rscitcer/'.$_fileRSCITCer1;


                                    if ( file_exists($cancelcheck) && file_exists($dpletter) && file_exists($rscitcer) ) 
                                       {					
                                            $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);

                                            $_Date = date("Y-m-d h:i:s");	
                                            $_InsertQuery_log = "INSERT INTO tbl_govstatusupdatelog (Govemp_id, Govemp_ITGK_Code, Govemp_Learnercode, Govemp_Process_Status,"
                                            . "Govemp_Datetime) "
                                            . "Select Case When Max(Govemp_id) Is Null Then 1 Else Max(Govemp_id)+1 End as Govemp_id,"
                                    . "'" . $_SESSION['User_LoginId'] . "' as Govemp_ITGK_Code,'" . $_LearnerCode . "' as Govemp_Learnercode,'12' as Govemp_Process_Status,"
                                    . "'" . $_Date . "' as Govemp_Datetime"
                                            . " From tbl_govstatusupdatelog";
                                            $_Response_log=$_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);



                                            //$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Government Reimbursement Process is Successfully Submitted.";
                                            $_SMS = "Dear Learner " . $_LearnerCode . " Your application for RS-CIT fee Reimbursement is still not completed. Kindly print acknowledgement receipt (Step2), sign it and upload it (Step3) on myrkcl.com ";
                                            $_Mobile=$_EmpMobile;
                                            SendSMS($_Mobile, $_SMS);				
                                       }
                                    else
                                        {
                                            echo "Please Re-Upload Documents Again.";
                                            return;
                                        }               
                            }
                        else 
                            {
                                $_Response[0] = Message::DuplicateRecord;
                                $_Response[1] = Message::Error;
                            }

            
                }
            
            else 
                {
                    session_destroy();
                    ?>
                    <script> window.location.href = "logout.php";</script> 
                    <?php

                }
            
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;
    }
    
    public function GetEMPACKDETAILS($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			//$_ITGK=$_SESSION['User_LoginId'];
                      
             $_SelectQuery = "select a.*,b.*,c.cstatus 
                                    from tbl_admission a
                                    left join tbl_govempentryform b on a.Admission_LearnerCode=b.learnercode
                                    left join tbl_capcategory c on b.trnpending = c.capprovalid 
                                    where b.learnercode='".$_action."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetCHECKEMPRECIPT($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND attach1=''";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function CHECKEMPRECIPT($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND empid!=''";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetImageAckReceipt($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select attach1 from tbl_govempentryform where learnercode='".$_action."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function ActMobValidate($lcode,$empmobile,$empbankaccount) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select * from tbl_govempentryform where learnercode!='".$lcode."' and (lmobileno='".$empmobile."' or empaccountno='".$empbankaccount."')";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function AddLernerMob($_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = $_OTP." is the OTP for validating mobile number for your Reimbursement Process.";
        

        try {
            /* Here it checking that the OTP in which we are sending that is allredy in our database or not.*/
            $_SelectQuery = "Select * FROM tbl_ao_learner_register WHERE AO_Mobile = '" . $_Mobile . "' AND AO_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response1);
            if ($_Response1[0] == 'Success') 
                {
                $_Row = mysqli_fetch_array($_Response1[2]);
                //print_r($_Row);
                $old_OTP=$_Row['AO_OTP'];
                //$_OLD_SMS = "OTP for your Learner Login in MYRKCL Application is  " . $old_OTP;
                $_OLD_SMS = $old_OTP." is the OTP for your Reimbursement Application Form in MYRKCL Application.";

                SendSMS($_Mobile, $_OLD_SMS);
                return $_Response1;
            }
            else{
                $_InsertQuery = "Insert Into tbl_ao_learner_register(AO_Code,AO_Mobile,AO_OTP)"
                    . "Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,"
                    . "'" . $_Mobile . "' as AO_Mobile,'"
                    . "" . $_OTP . " as AO_OTP '"
                    . " From tbl_ao_learner_register";
                //echo $_InsertQuery;die;            
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
                return $_Response;
            }
            //die;
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    public function verifyLotp($_mobile, $_otp) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select * FROM tbl_ao_learner_register WHERE AO_Mobile = '" . $_mobile . "' AND AO_OTP = '" . $_otp . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response[0]);
            if ($_Response1[0] == Message::NoRecordFound) {
                //echo "Invalid OTP. Please Try Again";
                $er='InvalidOTP';
                return $er;
            } else {
                $_UpdateQuery = "Update tbl_ao_learner_register set AO_Status='1' WHERE AO_Mobile='" . $_mobile . "' AND AO_OTP = '" . $_otp . "' AND AO_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_ao_learner_register WHERE AO_Mobile='" . $_mobile . "' AND AO_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
                return $_Response;
            }
        } catch (Exception $_ex) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
     public function UpdateEmpRecipt($_EmpId,$_learnercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                 $_UpdateQuery = "Update tbl_govempentryform set empid='" . $_EmpId . "' Where learnercode='" . $_learnercode . "'";
                 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;        
    }
    
    public function UpdateEmpAkcRecipt($_fileReceipt,$_learnercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                 
            $_fileReceipt1      =	$_fileReceipt .'_payment' . '.jpg';
            
            $_UpdateQuery = "Update tbl_govempentryform set attach1='" . $_fileReceipt1 . "' Where learnercode='" . $_learnercode . "'";
           
            $receiptdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_payment/'.$_fileReceipt1;
				
            if ( file_exists($receiptdoc) ) 
                        {					
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                                
                                
                                $_Date = date("Y-m-d h:i:s");	
				        $_InsertQuery_log = "INSERT INTO tbl_govstatusupdatelog (Govemp_id, Govemp_ITGK_Code, Govemp_Learnercode, Govemp_Process_Status,"
                                        . "Govemp_Datetime) "
                                        . "Select Case When Max(Govemp_id) Is Null Then 1 Else Max(Govemp_id)+1 End as Govemp_id,"
                                . "'" . $_SESSION['User_LoginId'] . "' as Govemp_ITGK_Code,'" . $_learnercode . "' as Govemp_Learnercode,'3' as Govemp_Process_Status,"
                                . "'" . $_Date . "' as Govemp_Datetime"
                                        . " From tbl_govstatusupdatelog";
                                $_Response_log=$_ObjConnection->ExecuteQuery($_InsertQuery_log, Message::InsertStatement);
                                        
                                $_trnpending="Update tbl_govempentryform  
                                                set trnpending='3' 
                                                Where learnercode='" . $_learnercode . "'";
                                $_trnpending=$_ObjConnection->ExecuteQuery($_trnpending, Message::UpdateStatement);
                                
                                
				$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Government Reimbursement Receipt is Successfully Uploded to RKCL";
				$_Mobile=$_EmpMobile;
				SendSMS($_Mobile, $_SMS);				
			}
            else
                        {
				echo "Please Re-Upload Documents Again.";
				return;
                        }               
           
          
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
        return $_Response;        
    }
    
    
    public function CheckLearnerCodeStatus($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function FILLGovFee($_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT Admission_Fee,Admission_Batch FROM tbl_admission where Admission_LearnerCode='" . $_LearnerCode . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCheckEditEMPStatus($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND trnpending='3'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetCheckEditEMPStatus12($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND trnpending='12'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function GetCheckRejEMPStatus($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select * from tbl_govempentryform where learnercode='".$_action."' AND trnpending='4'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    public function GetUserRollName($_action) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
	
            $_SelectQuery = "Select UserRoll_Name from tbl_userroll_master where UserRoll_Code='".$_action."' and UserRoll_Status=1 ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    /* Added By SUNIL: 15-3-2017 for  Adding EMployee Detail in tbl_govempentryform table */
    
    
}
?>