<?php
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clswebservicescoresync {

    public function GetWebserviceTableName() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Id,WTableName From tbl_webservice_tablename where Tstatus=1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function getAdmissionCode($Admission_LearnerCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$Admission_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$Admission_LearnerCode);
				
            $_SelectQuery = "Select Admission_Code From tbl_admission where Admission_LearnerCode='".$Admission_LearnerCode."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    /* Show Table Count Detail*/
    public function GetTableCount($tablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select count(*) as totalcount From ".$tablename." ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetTableCountLearnerScore($ddlCourse, $ddlBatch) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$ddlCourse = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlCourse);
				$ddlBatch = mysqli_real_escape_string($_ObjConnection->Connect(),$ddlBatch);
				
            $_SelectQuery = "Select count(*) as totalcount, c.Course_Name, d.Batch_Name from tbl_learner_score a 
            inner join tbl_admission b on a.Learner_Code=b.Admission_LearnerCode
            inner JOIN tbl_course_master c on c.Course_Code=b.Admission_Course 
            inner JOIN tbl_batch_master d on d.Batch_Code=b.Admission_Batch
            where b.Admission_Course='".$ddlCourse."' and b.Admission_Batch='".$ddlBatch."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    /* Show Table Count Detail*/
    
    // 1for Biomatric Register
    
    public function AddLearnerScore($Admission_Code, $Learner_Code, $Score, $Score_Per, $ITGK_code, $AddEditTime)
            {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
					$Admission_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$Admission_Code);
					$Learner_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$Learner_Code);
					
            $_InsertQuery = "Insert Into tbl_learner_score(Admission_Code,Learner_Code,Score,Score_Per,ITGK_code,AddEditTime) 
				VALUES ('" . $Admission_Code . "', '" . $Learner_Code . "', '" . $Score . "', '" . $Score_Per . "','" . $ITGK_code . "','" . $AddEditTime . "')";
            $_DuplicateQuery = "Select * From tbl_learner_score Where Learner_Code='" . $Learner_Code . "' ";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
   
    public function UpdateLearnerScore($Learner_Score_Id_old, $Admission_Code, $Learner_Code, $Score, $Score_Per, $ITGK_code, $AddEditTime)
            {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$Learner_Score_Id_old = mysqli_real_escape_string($_ObjConnection->Connect(),$Learner_Score_Id_old);
            
            $_UpdateQuery = "Update tbl_learner_score set                                 
                                Admission_Code='" . $Admission_Code . "',
                                Learner_Code='" . $Learner_Code . "',
                                Score='" . $Score . "',
                                Score_Per='" . $Score_Per . "',
                                ITGK_code='" . $ITGK_code . "',
                                AddEditTime='" . $AddEditTime . "'
                                Where Learner_Score_Id='" . $Learner_Score_Id_old . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
           
           
    
}




?>