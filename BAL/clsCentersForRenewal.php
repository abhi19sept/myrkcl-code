<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsLearnerTypeMaster
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsCentersForRenewal {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
			{
			mysqli_set_charset('utf8');
			$_LoginRole = $_SESSION['User_UserRoll'];
				/*$_SelectQuery = "select b.User_LoginId , a.Courseitgk_Course, a.Courseitgk_ITGK, b.User_MobileNo, b.User_EmailId, 
				b.User_CreatedDate, c.Organization_Name, d.District_Name, e.Tehsil_Name , g.User_LoginId as SP_Code,
				h.Organization_Name as SP_Name,g.User_MobileNo as SP_Phone, g.User_EmailId as SP_Email, a.CourseITGK_StartDate, a.CourseITGK_ExpireDate, CONCAT(DATE_FORMAT(DATE_ADD(DATE_ADD(b.User_CreatedDate, INTERVAL 1 YEAR), INTERVAL 1 MONTH), '%Y-%m-'), '05 00:00:00') AS optional_expire_date
				from tbl_user_master as b  left join  tbl_courseitgk_mapping as a on b.User_LoginId = a.Courseitgk_ITGK join tbl_organization_detail as c 
				on  b.User_Code = c.Organization_User join tbl_district_master as d on d.District_Code = c.Organization_District join 
				tbl_tehsil_master as e on e.Tehsil_Code = c.Organization_Tehsil join tbl_user_master as g on g.User_Code = b.User_Rsp join 
				tbl_organization_detail as h on  g.User_Code = h.Organization_User where (a.Courseitgk_EOI = 1 or ( b.User_Type like 'new' and a.Courseitgk_EOI is null)) order by a.CourseITGK_ExpireDate ASC";  */
				
		 
				if ($_LoginRole == '14'){
							$_SelectQuery = "select b.User_LoginId , a.Courseitgk_Course, a.Courseitgk_ITGK, b.User_MobileNo,
											b.User_EmailId, b.User_CreatedDate, c.Organization_Name, d.District_Name, e.Tehsil_Name,
											g.User_LoginId as SP_Code,h.Organization_Name as SP_Name,g.User_MobileNo as SP_Phone, 
											g.User_EmailId as SP_Email, a.CourseITGK_StartDate,a.CourseITGK_ExpireDate AS 
											optional_expire_date from tbl_user_master as b left join  tbl_courseitgk_mapping as a 
											on b.User_LoginId = a.Courseitgk_ITGK join tbl_organization_detail as c 
											on  b.User_Code = c.Organization_User join tbl_district_master as d on
											d.District_Code = c.Organization_District join tbl_tehsil_master as e on
											e.Tehsil_Code = c.Organization_Tehsil join tbl_user_master as g on g.User_Code = b.User_Rsp
											join tbl_organization_detail as h on  g.User_Code = h.Organization_User
											where (a.Courseitgk_EOI = 1 or ( b.User_Type like 'new' and a.Courseitgk_EOI is null))
											AND b.User_Rsp='" . $_SESSION['User_Code'] . "' order by a.CourseITGK_ExpireDate ASC";
											
				}else if ($_LoginRole == '1' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '11') {
							$_SelectQuery = "select b.User_LoginId , a.Courseitgk_Course, a.Courseitgk_ITGK, b.User_MobileNo, 
											b.User_EmailId, b.User_CreatedDate, c.Organization_Name, d.District_Name, e.Tehsil_Name, 
											g.User_LoginId as SP_Code,h.Organization_Name as SP_Name,g.User_MobileNo as SP_Phone, 
											g.User_EmailId as SP_Email, a.CourseITGK_StartDate,a.CourseITGK_ExpireDate AS
											optional_expire_date from tbl_user_master as b left join tbl_courseitgk_mapping as a
											on b.User_LoginId = a.Courseitgk_ITGK join tbl_organization_detail as c 
											on  b.User_Code = c.Organization_User join tbl_district_master as d on 
											d.District_Code = c.Organization_District join tbl_tehsil_master as e on
											e.Tehsil_Code = c.Organization_Tehsil join tbl_user_master as g on g.User_Code = b.User_Rsp
											join tbl_organization_detail as h on  g.User_Code = h.Organization_User
											where (a.Courseitgk_EOI = 1 or ( b.User_Type like 'new' and a.Courseitgk_EOI is null))
											order by a.CourseITGK_ExpireDate ASC";
				}
				else {
					?>
						<script> window.location.href = "logout.php";</script> 
                <?php
				}
				
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
		   }
		   else 
		   {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
        return $_Response;    
	}
    
}
