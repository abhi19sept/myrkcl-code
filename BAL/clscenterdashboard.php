<?php
/* 
 * Created By Sunil Kuamr Baindara 
 * Dated: 19-7-2017
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clscenterdashboard {
	
	public function GetAllRSP() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_SelectQuery = "select * from tbl_user_master where User_UserRoll='14'";
            $_SelectQuery="select a.User_Code, a.User_LoginId, b.Organization_Name from tbl_user_master a 
                            LEFT JOIN tbl_organization_detail b on a.User_Code=b.Organization_User 
                            where a.User_UserRoll='14' order by a.User_Code asc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    } 
    
	public function GetRSPNAME($user_code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select User_LoginId from tbl_user_master where User_UserRoll='14' AND User_Code='".$user_code."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    } 
    
	public function GetAllITGK($user_code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            //$_SelectQuery = "select * from tbl_user_master where User_Rsp='".$user_code."' order by User_LoginId asc";
            $_SelectQuery = "select a.*,b.* from tbl_user_master a 
                            LEFT JOIN tbl_organization_detail b on a.User_Code=b.Organization_User 
                            where a.User_Rsp='".$user_code."' order by a.User_LoginId asc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    } 
    
	public function checkITGK($user_code) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_user_master where User_LoginId='".$user_code."' and User_Rsp='".$_SESSION['User_Code']."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function CourseDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select a.Courseitgk_Code,a.Courseitgk_Course,b.User_CreatedDate,a.CourseITGK_UserFinalApprovalDate,a.CourseITGK_ExpireDate,c.EOI_StartDate 
                                    from tbl_courseitgk_mapping a
                                    LEFT JOIN tbl_user_master b on a.Courseitgk_ITGK=b.User_LoginId 
                                    LEFT JOIN tbl_eoi_master c on a.Courseitgk_EOI=c.EOI_Code 
                                    where a.Courseitgk_ITGK='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SLAAdmisiionDate($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select a.Courseitgk_Code,a.Courseitgk_Course,a.CourseITGK_ExpireDate
                            from tbl_courseitgk_mapping a
                            where a.Courseitgk_ITGK='".$_actionvalue."' and a.Courseitgk_Course='RS-CIT'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SLADetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select count(*) as confirmcount, a.coursename, a.batchname, a.eventname, 
                                a.examid, a.examcentercapacity
                                from tbl_finalexammapping a 
                                where a.centercode='".$_actionvalue."'
                                group by a.examid
                                order by coursename,id asc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SLADetailsResult_old($_actionvalue,$_eventid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

           $_SelectQuery = "select count(*) Allresult,b. result from tbl_result b 
                            where study_cen='".$_actionvalue."' and exameventnameID='".$_eventid."'
                            group by result order by result desc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SLADetailsResult($_actionvalue,$_eventid,$_result) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

           $_SelectQuery = "select count(*) Allresult from tbl_result b 
                            where study_cen='".$_actionvalue."' and exameventnameID='".$_eventid."' 
                            and result='".$_result."'
                            group by result";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SLAUDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select count(*) as confirmcount, a.coursename, a.batchname, a.eventname, 
                                a.examid, a.examcentercapacity
                                from tbl_finalexammapping_collective a 
                                group by a.examid
                                order by a.coursename,a.id  asc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
        public function SLAUDetailsResult($_actionvalue,$_eventid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

           $_SelectQuery = "select count(*) Allresult,b. result from tbl_result b 
                            where exameventnameID='".$_eventid."'
                            group by result order by result desc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function PremisesDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select * from tbl_premises_details where Premises_User='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function ComputerRecourseDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select * from tbl_system_info where System_User='".$_actionvalue."'  order by System_Type DESC";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function EmailMobileUpdateDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select * from tbl_user_master_log where centercode='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function HRDetails($_actionvalue, $div_name) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

             $_SelectQuery = "select a.*,b.Designation_Name,c.Qualification_Name from tbl_staff_detail a
                            left join tbl_designation_master b on b.Designation_Code=a.Staff_Designation
                            left join tbl_qualification_master c on c.Qualification_Code=a.Staff_Qualification1
                            where a.Staff_User='".$_actionvalue."' and a.Staff_Designation='".$div_name."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
        public function HRDetailsCount($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select count(*) as countf,b.Designation_Name 
                            from tbl_staff_detail a
                            left join tbl_designation_master b on b.Designation_Code=a.Staff_Designation
                            where a.Staff_User='".$_actionvalue."'
                            group by Staff_Designation order by Staff_Designation Desc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
        //Addres Name Change Log
        public function ANCDetail($_actionvalue, $div_name) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select *, CASE fld_status WHEN '0' THEN 'Open' WHEN '1' THEN 'Submitted' WHEN '2' THEN 'Approved By SP' WHEN '3' THEN 'Approved By RKCL/Payment Pending' WHEN '4' THEN 'Rejected by RKCL' WHEN '5' THEN 'Paid' WHEN '6' THEN 'Rejected By ITGK' END AS 'fld_status'
                            from tbl_change_address_itgk where fld_ITGK_Code='".$_actionvalue."' and fld_requestChangeType='".$div_name."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function ANCDetailCount($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "select count(*) as countf, fld_requestChangeType 
                            from tbl_change_address_itgk where fld_ITGK_Code='".$_actionvalue."' group by fld_requestChangeType order by fld_requestChangeType Desc";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
        public function SPDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
//            $_SelectQuery = "Select a.* , b.*,c.* from tbl_admission a 
//                            LEFT JOIN tbl_organization_detail b on a.Admission_RspName=b.Organization_User 
//                            LEFT JOIN tbl_user_master c on a.Admission_RspName=c.User_Code
//                            where a.Admission_LearnerCode='".$_actionvalue."'";
            $_SelectQuery = "select * from tbl_organization_detail where Organization_User='".$_actionvalue."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    
    public function CenterDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.* , b.*
                        from tbl_user_master a
                        LEFT JOIN tbl_organization_detail b on a.User_Code=b.Organization_User
                        where  a.User_LoginId='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function BankDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * from tbl_bank_account where Bank_User_Code='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function LocationDetails($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.* , b.*,d.State_Name,e.District_Name,f.Region_Name,g.Tehsil_Name,
                h.Municipality_Type_Name,i.Ward_Name,j.Municipality_Name,k.Block_Name,l.GP_Name,m.Village_Name
                        from tbl_user_master a
                        LEFT JOIN tbl_organization_detail b on a.User_Code=b.Organization_User
                        LEFT JOIN tbl_state_master d on d.State_Code=b.Organization_State
                        LEFT JOIN tbl_district_master e on e.District_Code=b.Organization_District
                        LEFT JOIN tbl_region_master f on f.Region_Code=b.Organization_Region
                        LEFT JOIN tbl_tehsil_master g on g.Tehsil_Code=b.Organization_Tehsil
                        LEFT JOIN tbl_municipality_type h on h.Municipality_Type_Code=b.Organization_Municipal_Type
                        LEFT JOIN tbl_ward_master i on i.Ward_Code=b.Organization_WardNo
                        LEFT JOIN tbl_municipality_master j on j.Municipality_Raj_Code=b.Organization_Municipal
                        LEFT JOIN tbl_panchayat_samiti k on k.Block_Code=b.Organization_Panchayat
                        LEFT JOIN tbl_gram_panchayat l on l.GP_Code=b.Organization_Gram 
                        LEFT JOIN tbl_village_master m on m.Village_Code=b.Organization_Village
                        where  a.User_LoginId='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function AdmissionDetails($_actionvalue,$year,$typeyear,$data) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
           
        if($typeyear=='Cal'){
            $_SelectQuery = "SELECT b.Course_Name, count(*) as confirmcount,  a.*  
                            FROM tbl_admission a
                            LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
                            LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
                            WHERE a.Admission_ITGK_Code='" . $_actionvalue . "' AND a.Admission_Payment_Status = '1'
                            and right(c.Batch_Name,4) = '" . $year . "'
                            group by right(c.Batch_Name,4), b.Course_Name
                            order by b.Course_Name";
        }
        
        else if($typeyear=='SLA')
        {
            if(is_array($data)){
                $batch_nameIN=implode(", ",$data);
            }else{
                $batch_nameIN="'".$data."'";
            }
            
            
             $_SelectQuery = "SELECT b.Course_Name, count(*) as confirmcount,  a.*  
                            FROM tbl_admission a
                            LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
                            LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
                            WHERE a.Admission_ITGK_Code='" . $_actionvalue . "' AND a.Admission_Payment_Status = '1'
                            and c.Batch_Name in (".$batch_nameIN.")
                            group by b.Course_Name
                            order by b.Course_Name";
        }
        else{   
            //$year_1=$year+1;
            //$yearN=$year.'-'.$year_1;
            $_SelectQuery = "SELECT b.Course_Name, count(*) as confirmcount,  a.*  
                            FROM tbl_admission a
                            LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
                            LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
                            WHERE a.Admission_ITGK_Code='" . $_actionvalue . "' AND a.Admission_Payment_Status = '1'
                            and c.Financial_Year = '" . $year . "'
                            group by right(c.Batch_Name,4), b.Course_Name
                            order by b.Course_Name";
            } 
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    /* In this function we getting SLA report that direct taking detail form expire date. Update Date: 8-8-2018*/
    public function GetITGK_Admission_Details($_CenterCode, $_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $SDate = $_StartDate.' 00:00:00';
            $EDate = $_EndDate.' 00:00:00';
//            $_SelectQuery = "select count(Admission_Code) as Admission_Count from tbl_admission where Admission_ITGK_Code='" . $_CenterCode . "' 
//                                and Admission_Date>'" . $SDate . "' and Admission_Date<='" . $EDate . "' and Admission_Payment_Status='1'"
//                    . " and Admission_Course in (1,4)";
            $_SelectQuery = "select b.Course_Name, count(*) as confirmcount,  a.*  
                from tbl_admission a 
                LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
                LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
                where a.Admission_ITGK_Code='" . $_CenterCode . "' and a.Admission_Date>'" . $SDate . "' 
                and a.Admission_Date<='" . $EDate . "' and a.Admission_Payment_Status='1' 
                and a.Admission_Course in (1,4)
                group by right(c.Batch_Name,4), b.Course_Name
                order by b.Course_Name";
        
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetITGK_Admission_DetailsInMonths($_CenterCode, $_StartDate, $_EndDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $SDate = $_StartDate.' 00:00:00';
            $EDate = $_EndDate.' 00:00:00';
            $_SelectQuery = "select b.Course_Name, count(*) as confirmcount, c.Batch_Name, a.*  
            from tbl_admission a 
            LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
            LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
            where a.Admission_ITGK_Code='" . $_CenterCode . "' and a.Admission_Date>'" . $SDate . "' 
            and a.Admission_Date<='" . $EDate . "' and a.Admission_Payment_Status='1'  
            and a.Admission_Course in (1,4)
            group by a.Admission_Batch, b.Course_Name
            order by b.Course_Name, c.Batch_Code";
        
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    /* In this function we getting SLA report that direct taking detail form expire date. Update Date: 8-8-2018*/
    
    public function AdmissionDetailsInMonths($_actionvalue,$year,$typeyear,$data) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        if($typeyear=='Cal'){
            $_SelectQuery = "SELECT b.Course_Name, count(*) as confirmcount, c.Batch_Name,  a.*  
                            FROM tbl_admission a
                            LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
                            LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
                            WHERE a.Admission_ITGK_Code='" . $_actionvalue . "' AND a.Admission_Payment_Status = '1'
                            and right(c.Batch_Name,4) = '" . $year . "'
                            group by a.Admission_Batch, b.Course_Name
                            order by b.Course_Name, c.Batch_Code";
        }
        else if($typeyear=='SLA')
        {
            if(is_array($data)){
                $batch_nameIN=implode(", ",$data);
            }else{
                $batch_nameIN="'".$data."'";
            }
            
            
              $_SelectQuery = "SELECT b.Course_Name, count(*) as confirmcount, c.Batch_Name, a.*  
                            FROM tbl_admission a
                            LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
                            LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
                            WHERE a.Admission_ITGK_Code='" . $_actionvalue . "' AND a.Admission_Payment_Status = '1'
                            and c.Batch_Name in (".$batch_nameIN.")
                            group by a.Admission_Batch, b.Course_Name
                            order by b.Course_Name, c.Batch_Code";
        }
        
        else{
            
                            //$year_1=$year+1;
                            //$yearN=$year.'-'.$year_1;
            $_SelectQuery = "SELECT b.Course_Name, count(*) as confirmcount,  c.Batch_Name,  a.*  
                            FROM tbl_admission a
                            LEFT JOIN tbl_course_master b on b.Course_Code=a.Admission_Course 
                            LEFT JOIN tbl_batch_master c on c.Batch_Code=a.Admission_Batch
                            WHERE a.Admission_ITGK_Code='" . $_actionvalue . "' AND a.Admission_Payment_Status = '1'
                            and c.Financial_Year = '".$year."'
                            group by a.Admission_Batch, b.Course_Name
                            order by b.Course_Name, c.Batch_Code";
        }
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCenterDetailReport($CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select a.User_Code, a.User_LoginId as CenterCode, a.User_EmailId,a.User_MobileNo, b.Organization_Tehsil, b.Organization_District, b.Organization_Name as CenterName, c.Organization_Name as PSAName, a.DLC, d.Organization_Name as DLCName, e.Tehsil_Name, f.District_Name from ( select user_code,User_MobileNo,User_EmailId,user_loginid, SUBSTRING_INDEX(user_parentid,'|',-1) as PSA,SUBSTRING_INDEX(user_parentid,'|',1) as DLC from tbl_user_master where user_userroll=7) as a INNER JOIN tbl_organization_detail as b on a.User_Code = b.Organization_User Inner Join tbl_Organization_Detail as c on a.PSA = c.Organization_User inner join tbl_Organization_Detail as D on a.DLC = D.Organization_User Inner Join tbl_tehsil_master as e on e.Tehsil_Code = b.Organization_Tehsil Inner Join tbl_district_master as f on f.District_Code =  b.Organization_District where a.User_LoginId = " . $CenterCode . " ";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCenterWiseReport($CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From block Where CenterCode='" . $CenterCode . "' Order By Id Desc";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    ///////////////////////////////////////////////////////////////////////////
        public function CenterDetails_ALl($_actionvalue) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select a.* , b.*,c.*,d.State_Name,e.District_Name,f.Region_Name,g.Tehsil_Name,
                h.Municipality_Type_Name,i.Ward_Name,j.Municipality_Name,k.Block_Name,l.GP_Name,m.Village_Name
                        from tbl_user_master a
                        LEFT JOIN tbl_organization_detail b on a.User_Code=b.Organization_User
                        LEFT JOIN tbl_bank_account c on a.User_LoginId=c.Bank_User_Code
                        LEFT JOIN tbl_state_master d on d.State_Code=b.Organization_State
                        LEFT JOIN tbl_district_master e on e.District_Code=b.Organization_District
                        LEFT JOIN tbl_region_master f on f.Region_Code=b.Organization_Region
                        LEFT JOIN tbl_tehsil_master g on g.Tehsil_Code=b.Organization_Tehsil
                        LEFT JOIN tbl_municipality_type h on h.Municipality_Type_Code=b.Organization_Municipal_Type
                        LEFT JOIN tbl_ward_master i on i.Ward_Code=b.Organization_WardNo
                        LEFT JOIN tbl_municipality_master j on j.Municipality_Raj_Code=b.Organization_Municipal
                        LEFT JOIN tbl_panchayat_samiti k on k.Block_Code=b.Organization_Panchayat
                        LEFT JOIN tbl_gram_panchayat l on l.GP_Code=b.Organization_Gram 
                        LEFT JOIN tbl_village_master m on m.Village_Code=b.Organization_Village
                        where a.User_UserRoll=7 and User_LoginId='" . $_actionvalue . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       

    //put your code here
    
}
