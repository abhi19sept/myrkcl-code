<?php
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsscholarform {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select District_Name From tbl_district_master where District_Status=1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
	
	public function GetEmpId() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select id From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	
	
     public function GetEmpdepartment() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select gdname From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
    
    
    public function GetEmpdesignation() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select distinct designationname From tbl_designationname_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetBankdistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select distinct District_Name From tbl_district_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetBankname($districtid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$districtid = mysqli_real_escape_string($_ObjConnection->Connect(),$districtid);
				
            $_SelectQuery = "Select distinct bankname From tbl_rajbank_master where bankdistrict='" . $districtid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetDobProof() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select cdocname From tbl_cdoccategory_master where active = 'Yes' AND type = 'govreimbursement'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    } 
    
    
    
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_SelectQuery = "Select Status_Code,Status_Name,Status_Description From "
                    . "tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function DeleteRecord($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_DeleteQuery = "Delete From tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Add($_LearnerCode, $_LearnerName, $_FatherName, $_MotherName, $_Address, $_Mobile, $_Email, $_Batch, 
						$_District , $_Tehsil, $_PinNo, $_Qualification, $_RollNo, $_PermissionLetter, $_ScholarForm) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {			
				$_LearnerCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_LearnerCode);
				
            $_InsertQuery = "Insert Into tbl_scholarship(lcode, lname, fname, lmname, laddress, lmobile, lemailid, lbatch, tehsil, district,
								pincode, qualification, rollno )
					 VALUES ('" . $_LearnerCode . "', '" . $_LearnerName . "', '" . $_FatherName . "', '" . $_MotherName . "',
							 '" . $_Address . "', '" . $_Mobile . "', '" . $_Email . "', '" . $_Batch . "', '" . $_Tehsil . "',
							 '" . $_District . "', '" . $_PinNo . "', '" . $_Qualification . "', '" . $_RollNo . "')";
            $_DuplicateQuery = "Select * From tbl_scholarship Where lcode='" . $_LearnerCode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Status_Code,$_StatusName, $_Status_Description) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				$_StatusName = mysqli_real_escape_string($_ObjConnection->Connect(),$_StatusName);
				
            $_UpdateQuery = "Update tbl_status_master set Status_Name='" . $_StatusName . "',"
                    . "Status_Description='" . $_Status_Description . "' "
                    . "Where Status_Code='" . $_Status_Code . "'";
            $_DuplicateQuery = "Select * From tbl_status_master Where Status_Name='" . $_StatusName . "' and "
                    . "Status_Code <> '" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
        
    }
    

}

?>