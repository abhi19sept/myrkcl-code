<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsBankAccount
 *
 *  author Yogendra
 */

require 'DAL/classconnectionNEW.php';

require 'DAL/sendsms.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clseditbankaccountdetails {
    //put your code here
    
	
	public function SHOWDATA() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {

		$_SelectQuery = "Select * from tbl_bank_account";
                
                } else {
                    
                $_SelectQuery = "Select * from tbl_bank_account where Bank_User_Code ='" .$_SESSION['User_LoginId'] . "'";
                            
                }
                
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);                
            
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response2[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response2[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct bankname from tbl_rajbank_master";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	
	
	 public function GetBranch() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select distinct branchname from tbl_rajbank_master ";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
  
    

   public function Update($_code,$_AccountName,$_AccountNumber,$_AccountType,$_IfscCode,$_BankName,$_BranchName,$_MicrCode,$_PanNo,$_PanName,$_IdProof,$_Genid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
					$_code = mysqli_real_escape_string($_ObjConnection->Connect(),$_code);
					
			 $_docname =	$_SESSION['User_LoginId'].'_'.$_AccountNumber.'_'.$_IdProof.'.jpg';
			 $_pancard =	$_SESSION['User_LoginId'].'_'.$_AccountNumber .'_pancard'.'.jpg';
            $_UpdateQuery = "Update tbl_bank_account set Bank_Account_Name='" . $_AccountName . "',"
                    . "Bank_Account_Number='" . $_AccountNumber ."',"
                    . "Bank_Account_Type='" . $_AccountType ."',"
                    . "Bank_Ifsc_code='" . $_IfscCode ."',"
					. "Bank_Micr_Code='" . $_MicrCode ."',"
					. "Bank_Name='" . $_BankName ."',"
					
					. "Bank_Branch_Name='" . $_BranchName ."',"
					. "Pan_No='" . $_PanNo ."',"
					. "Pan_Name='" . $_PanName ."',"
					. "Bank_Document='" . $_docname ."',"
					. "Pan_Document='" . $_pancard ."',"
					//. "Bank_User_Code='" . $_SESSION['User_LoginId'] ."',"
					
					. "Bank_Id_Proof='" . $_IdProof ."'"
                    . " Where  	Bank_Account_Code='" . $_code . "' AND Bank_User_Code='".$_SESSION['User_LoginId']."'";
            //$_DuplicateQuery = "Select * From tbl_bank_account Where Bank_Account_Number='" . $_AccountNumber . "' ";
			//$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
			
			
			
			
			
			 $_InsertQuery1 = "Insert Into tbl_bank_account_log(Bank_Account_Code,Bank_Account_Name,Bank_Account_Number,Bank_Account_Type,"
                    ."Bank_Ifsc_code,Bank_Name,Bank_Branch_Name,Bank_Micr_Code,Pan_No,Pan_Name,Bank_Id_Proof,Bank_Document,Pan_Document,Bank_User_Code) "
                    . "Select Case When Max(Bank_Account_Code) Is Null Then 1 Else Max(Bank_Account_Code)+1 End as Bank_Account_Code,"
                    . "'" . $_AccountName . "' as Bank_Account_Name, "
                    . "'" . $_AccountNumber . "' as Bank_Account_Number,'" . $_AccountType . "' as Bank_Account_Type, "
                    . "'" . $_IfscCode . "' as Bank_Ifsc_code,'" . $_BankName . "' as Bank_Name, "
                    . "'" . $_BranchName . "' as Bank_Branch_Name,'" . $_MicrCode . "' as Bank_Micr_Code, "
					. "'" . $_PanNo . "' as Pan_No, '" . $_PanName . "' as Pan_Name, '" . $_IdProof . "' as Bank_Id_Proof, "
					. "'" . $_docname . "' as Bank_Document, '" . $_pancard . "' as Pan_Document, '" . $_SESSION['User_LoginId'] . "' as Bank_User_Code "
					
                    . " From tbl_bank_account_log";
			
				
				$bankdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/Bankdocs/'.$_docname;
				$pandoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/pancard/'.$_pancard;
				
				if ( file_exists($bankdoc) && file_exists($pandoc) ) {					
					$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);
				$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
				   }
					else
					{
						echo "Please Attach Necessary Documents on first page ";
						return;
					}				
           
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
          
	
	
	
	public function DeleteRecord($_actionvalue)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_DeleteQuery = "Delete From tbl_bank_account Where Bank_Account_Code='" . $_actionvalue . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	public function GetDatabyCode($_actionvalue)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select * from tbl_bank_account Where Bank_Account_Code= '" . $_actionvalue . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	
	 public function SendOTP() {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }

        $_OTP = OTP();
        $_SMS = "OTP for Verification of Authentication is " . $_OTP;

        try {
            $_UserM = $_SESSION['User_LoginId'];
            $_SelectQuery1 = "SELECT * FROM tbl_user_master WHERE User_LoginId='" . $_UserM . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery1, Message::SelectStatement);
            $_Row1 = mysqli_fetch_array($_Response1[2]);
            $_Mobile = $_Row1['User_MobileNo'];
            $_Email = $_Row1['User_EmailId'];


            $_InsertQuery = "Insert Into tbl_ao_register(AO_Code,AO_Email,AO_Mobile,AO_OTP)"
                    . "Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code,"
                    . "'" . $_Email . "' as AO_Email,'" . $_Mobile . "' as AO_Mobile,'"
                    . "" . $_OTP . " as AO_OTP '"
                    . " From tbl_ao_register";
            //echo $_InsertQuery;
            $_DuplicateQuery = "Select * From tbl_ao_register Where AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_OTP = '" . $_OTP . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            //echo $_Response[0];
            if ($_Response[0] == Message::NoRecordFound) {
                $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                SendSMS($_Mobile, $_SMS);
            } else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	public function Verify($_Email, $_Mobile, $_Otp) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
				$_Otp = mysqli_real_escape_string($_ObjConnection->Connect(),$_Otp);
				
            $_SelectQuery = "Select * FROM tbl_ao_register WHERE AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_OTP = '" . $_Otp . "' AND AO_Status = '0'";
            $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);

            if ($_Response1[0] == Message::NoRecordFound) {
                echo "Invalid Verification Details. Please Try Again";
                return;
            } else {
                $_UpdateQuery = "Update tbl_ao_register set AO_Status='1' WHERE AO_Mobile='" . $_Mobile . "' AND AO_Email = '" . $_Email . "' AND AO_OTP = '" . $_Otp . "' AND AO_Status = '0'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                $_DeleteQuery = "Delete From tbl_ao_register WHERE AO_Mobile='" . $_Mobile . "' AND AO_Status = '0'";
                $_Response2 = $_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	
	public function checkifsccode($_actionvalue)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_actionvalue = mysqli_real_escape_string($_ObjConnection->Connect(),$_actionvalue);
				
            $_SelectQuery = "Select * from tbl_rajbank_master Where ifsccode= '" . $_actionvalue . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
	

}
