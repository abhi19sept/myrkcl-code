<?php

/**
 * Description of clsPrintReceipt
 *
 * @author Abhi
 */
//echo "abhi";
require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPrintReceipt {
    //put your code here
     
    public function GetDatabyCode($userid)
    { 
        //echo $userid;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$userid = mysqli_real_escape_string($_ObjConnection->Connect(),$userid);
				
			$_SelectQuery = "Select a.Admission_Code, a.Admission_LearnerCode, a.Admission_Name, a.Admission_Fname, a.Admission_DOB, a.Admission_Medium,"
                    . "a.Admission_Photo, a.Admission_Sign, a.Admission_ITGK_Code, Course_Name, Batch_Name, a.Admission_Date,a.Admission_Mobile,"
                    . "a.Admission_Email,a.Timestamp,District_Name, Organization_Name, Batch_StartDate, Course_Fee, RKCL_Share, VMOU_Share "
                    . "From tbl_Admission as a inner join tbl_user_master as g "
                    . "on a.Admission_ITGK_Code=g.User_LoginId inner join tbl_organization_detail as b "
                    . "on g.User_Code=b.Organization_User inner join tbl_district_master as c "
                    . "on b.Organization_District=c.District_Code inner join tbl_batch_master as d "
                    . "on a.Admission_Batch=d.Batch_Code inner join tbl_course_master as e "
                    . "on a.Admission_Course=e.Course_Code "
                    . " WHERE Admission_Code='" . $userid . "'";

     
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
}
