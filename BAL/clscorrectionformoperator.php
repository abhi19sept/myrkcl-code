<?php
require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clscorrectionformoperator {

    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select District_Name From tbl_district_master where District_Status=1";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
     public function GetEmpdepartment() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select gdname From tbl_department_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
    
    
    public function GetEmpdesignation() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select distinct designationname From tbl_designationname_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetBankdistrict() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select distinct District_Name From tbl_district_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetBankname() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select distinct bankname From tbl_rajbank_master ";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetDobProof() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select cdocname From tbl_cdoccategory_master where active = 'Yes' AND type = 'govreimbursement'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    } 
        
    public function GetDatabyCode($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_SelectQuery = "Select Status_Code,Status_Name,Status_Description From "
                    . "tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
    public function DeleteRecord($_Status_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_DeleteQuery = "Delete From tbl_status_master Where Status_Code='" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Add($_LearnerCode, $_CenterCode, $_LearnerName, $_FatherName, $_LApplication, $_ExamName, $_TotalMarks, $_Email, $_MobileNo, $_DOB, $_GENERATEDID) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			  //print_r($_SESSION['CorrectionProvisional']);
			$cphoto			=	$_LearnerCode .'_photo'.'.jpg';
			$cmarksheet		=	$_LearnerCode .'_marksheet'.'.jpg';	
			$ccertificate	=	$_LearnerCode .'_certificate' .'.jpg';
			
			if(isset($_SESSION['CorrectionProvisional']) && !empty($_SESSION['CorrectionProvisional'])){
				$cprovisional = $_LearnerCode .'_provisional' . '.jpg';
			}
			else{
				$cprovisional	=	"";
			}
			
			//$_ITGK_Code 	=   $_SESSION['User_LoginId'];
			$_User_Code 	=   $_SESSION['User_Code'];
			
			if($_LApplication == 'Correction Certificate') {
            $_InsertQuery = "Insert Into tbl_correction_copy(lcode, cfname, cfaname, applicationfor, exameventname, totalmarks, emailid, mobile, 
							 dob, photo, attach1, attach2, attach4, userid, Correction_ITGK_Code ) 
							 VALUES ('" . $_LearnerCode . "', '" . $_LearnerName . "', '" . $_FatherName . "', '" . $_LApplication . "',
							 '" . $_ExamName . "', '" . $_TotalMarks . "', '" . $_Email . "', '" . $_MobileNo . "', '" . $_DOB . "',							
							 '" . $cphoto . "',  '" . $cmarksheet . "',  '" . $ccertificate . "',
							 '" . $cprovisional . "', '" . $_User_Code . "', '" . $_CenterCode . "')";
         
                $photodoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_photo/'.$cphoto;
				$marksheetdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_marksheet/'.$cmarksheet;
				$certificatedoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_certificate/'.$ccertificate;
				
				if ( file_exists($photodoc) && file_exists($marksheetdoc) && file_exists($certificatedoc) ) {
				$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Certificate Name Correction/Duplicate Certificate is Successfully Applied to RKCL";
				$_Mobile=$_MobileNo;
				SendSMS($_Mobile, $_SMS);
				}
					else {
						echo "Please Re-Upload Documents Again.";
						return;
					}          
		}
		  else {
					$_InsertQuery = "Insert Into tbl_correction_copy(lcode, cfname, cfaname, applicationfor, exameventname, totalmarks, emailid, mobile, 
							 dob, photo, attach1, attach2, attach4, userid, Correction_ITGK_Code ) 
							 VALUES ('" . $_LearnerCode . "', '" . $_LearnerName . "', '" . $_FatherName . "', '" . $_LApplication . "',
							 '" . $_ExamName . "', '" . $_TotalMarks . "', '" . $_Email . "', '" . $_MobileNo . "', '" . $_DOB . "',							
							 '" . $cphoto . "',  '" . $cmarksheet . "',  '" . $ccertificate . "',
							 '" . $cprovisional . "', '" . $_User_Code . "', '" . $_CenterCode . "')";
							 
							 $photodoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_photo/'.$cphoto;
				$marksheetdoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_marksheet/'.$cmarksheet;
				$certificatedoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_certificate/'.$ccertificate;
				
				if ( file_exists($photodoc) && file_exists($marksheetdoc) && file_exists($certificatedoc) ) {
				$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				$_SMS = "Dear Learner " . $_LearnerCode . " Your Application for Certificate Name Correction/Duplicate Certificate is Successfully Applied to RKCL";
				$_Mobile=$_MobileNo;
				SendSMS($_Mobile, $_SMS);
				}
					else {
						echo "Please Re-Upload Documents Again.";
						return;
					} 
			 }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Status_Code,$_StatusName, $_Status_Description) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Status_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status_Code);
				
            $_UpdateQuery = "Update tbl_status_master set Status_Name='" . $_StatusName . "',"
                    . "Status_Description='" . $_Status_Description . "' "
                    . "Where Status_Code='" . $_Status_Code . "'";
            $_DuplicateQuery = "Select * From tbl_status_master Where Status_Name='" . $_StatusName . "' and "
                    . "Status_Code <> '" . $_Status_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
			else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;        
    }
    
	public function GetFillApplicationFor() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select * From tbl_cdoccategory_master where active = 'Y'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    } 
	
	public function GetExamEvent() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
        
            $_SelectQuery = "Select * From tbl_events where Event_Status = '1'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    } 
	
	public function FillEventById($_EventId) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EventId = mysqli_real_escape_string($_ObjConnection->Connect(),$_EventId);
				
            $_SelectQuery = "Select Event_Name From tbl_events where Event_Id = '" . $_EventId . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    } 
	
	public function GetAllDETAILS($_action, $_Centercode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_action = mysqli_real_escape_string($_ObjConnection->Connect(),$_action);
				$_Centercode = mysqli_real_escape_string($_ObjConnection->Connect(),$_Centercode);
				
			$_ITGK=$_SESSION['User_LoginId'];
		    $_SelectQuery = "Select * from tbl_admission where Admission_LearnerCode='".$_action."' AND
								Admission_ITGK_Code='" . $_Centercode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	
}

?>