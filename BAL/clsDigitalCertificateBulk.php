<?php

require 'DAL/classconnectionNEW.php';
$_ObjConnection = new _Connection();
$_Response = array();

class clsDigitalCertificate {

    public function getSignAuthoriser($designation, $department) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$designation = mysqli_real_escape_string($_ObjConnection->Connect(),$designation);
				$department = mysqli_real_escape_string($_ObjConnection->Connect(),$department);
				
            $_SelectQuery = "Select Sign_Id,Sign_Sno,Sign_Name From tbl_esign_master where Sign_Designation='".$designation."' 
							and Sign_Department='".$department."'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetCourseCertificate() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_events where Event_Status='1' order by Event_Id DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function GetLearnerListITGK($_event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_event = mysqli_real_escape_string($_ObjConnection->Connect(),$_event);
				
                    $_SelectQuery = "select 
                        sum(case when status=0 then 1 else 0 end) as noofcertificatepending,
                        sum(case when status in(1,2) then 1 else 0 end) as noofcertificategenterated,
                        sum(case when status=2 then 1 else 0 end) as signedcertificate
                        from tbl_final_result where exameventnameID='".$_event."' and result LIKE('%pass%')";
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response3[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response3[1] = Message::Error;
        }
        return $_Response;
    }

    public function getLearnerResults($event, $signedNameReg) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
                $curdate = date("d-m-Y h:i:s");
				$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
				$signedNameReg = mysqli_real_escape_string($_ObjConnection->Connect(),$signedNameReg);
				
                $_SelectQuery = "SELECT scol_no AS learnercode, result_date, name, study_cen AS centercode, percentage,
                                    fname, exameventnameID, exam_event_name AS eventname, vmou_ref_num,
                                    exam_date, '" . $curdate . "' AS istdt, '" . $signedNameReg . "' as registrar_name, '' as reg_no, RIGHT(exam_date,4) as exam_year, batch_name as batchname, 'RS-CIT' as coursename FROM tbl_final_result
                                    WHERE exameventnameID = '" . $event . "' AND result LIKE('%pass%') and status='0' limit 1";
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ShowLearnerEcertificate($event) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
				
            $_SelectQuery = "Select * From tbl_final_result where result LIKE('%pass%') and exameventnameID='" . $event . "'
								and status='1' limit 1";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function ShowLearnerEcertificateForUpdate($lcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				
            $_SelectQuery = "Select * From tbl_final_result where result LIKE('%pass%') and scol_no='" . $lcode . "' and status='1'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }

    public function UpdateEcertificateStatus($lcode, $eventid, $itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
                $_UpdateQuery = "update tbl_final_result set status='2' where scol_no ='" . $lcode . "' and exameventnameID='" . $eventid . "' and
							 study_cen='" . $itgk . "'";
                $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateStatus($event, $learner) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$event = mysqli_real_escape_string($_ObjConnection->Connect(),$event);
				$learner = mysqli_real_escape_string($_ObjConnection->Connect(),$learner);
				
            $_UpdateQuery = "update tbl_final_result set status='1' where scol_no ='".$learner."' and exameventnameID='".$event."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);	
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function UpdateEcertificateStatusForUpdateCertificate($lcode,$eventid,$itgk) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$lcode = mysqli_real_escape_string($_ObjConnection->Connect(),$lcode);
				$eventid = mysqli_real_escape_string($_ObjConnection->Connect(),$eventid);
				$itgk = mysqli_real_escape_string($_ObjConnection->Connect(),$itgk);
				
            $_UpdateQuery = "update tbl_final_result set status='2' where scol_no ='".$lcode."' and exameventnameID='".$eventid."' 
							 and study_cen='".$itgk."'";
            $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
			
			$_UpdateQuery1 = "Update tbl_admission set Admission_Aadhar_Status='4'
							Where Admission_ITGK_Code='" . $itgk . "' AND Admission_LearnerCode='". $lcode ."'";
            $_Responses = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
}
