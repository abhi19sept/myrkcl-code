<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsPremisesArea
 *
 * @author VIVEK
 */


require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsPremisesArea {
    //put your code here
    
       public function GetArea() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])  && $_SESSION['User_UserRoll'] == '7') {
            $_SelectQuery = "Select * FROM tbl_premises_details WHERE Premises_User = '" . $_SESSION['User_LoginId'] . "'";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
   public function UpgradePremisesArea($_AreaType, $_TheoryArea, $_LabArea, $_TotalArea) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId']) && $_SESSION['User_UserRoll'] == '7') {
                date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
           $_UpdateQuery = "Update tbl_premises_details set Permises_Separate='" . $_AreaType . "', Premises_Theory_area='" . $_TheoryArea . "',"
                   . " Premises_Lab_area='" . $_LabArea . "',Premises_Total_area='" . $_TotalArea . "'"
                    . " Where Premises_User='" . $_SESSION['User_LoginId'] . "'";
            
            
            $_DuplicateQuery = "Select * From tbl_premises_details Where Premises_User='" . $_SESSION['User_LoginId'] . "' AND Premises_Total_area != '0'";
            $_Response = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);

            if ($_Response[0] == Message::NoRecordFound) {
                
            $_InsertQuery = "INSERT INTO tbl_premises_details_log
                            (Premises_Code,Premises_User,Premises_Receptiontype,Premises_seatingcapacity,Premises_area,Permises_Separate,
                            Premises_Theory_area,Premises_Lab_area,Premises_Total_area,Premises_details,Premises_parkingfacility,
                            Premises_ownership,Premises_parkingfor,Premises_fwcapacity,Premises_twcapacity,Premises_toiletavailable,
                            Premises_ownershiptoilet,Premises_toilet_type,Premises_panatry,Premises_panatry_capacity,Premises_library_available,
                            Premises_library_capacity,Premises_Staff_available,Premises_Staff_capacity,Premises_Log_Activity)
                            SELECT Premises_Code,Premises_User,Premises_Receptiontype,Premises_seatingcapacity,Premises_area,Permises_Separate,
                            Premises_Theory_area,Premises_Lab_area,Premises_Total_area,Premises_details,Premises_parkingfacility,
                            Premises_ownership,Premises_parkingfor,Premises_fwcapacity,Premises_twcapacity,Premises_toiletavailable,
                            Premises_ownershiptoilet,Premises_toilet_type,Premises_panatry,Premises_panatry_capacity,Premises_library_available,
                            Premises_library_capacity,Premises_Staff_available,Premises_Staff_capacity, 'Update_Log' FROM tbl_premises_details 
                            WHERE Premises_User='" . $_SESSION['User_LoginId'] . "'";    	
            $_ResponseI=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                
            } else {
                $_Response[0] = "You have Already Submitted Premises Area"; //Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
            } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
