<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsProcessVisitClose
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';
include('DAL/smtp_class.php');

$_ObjConnection = new _Connection();
$_Response = array();

class clsProcessVisitClose {
    //put your code here
    
    public function Add($_Reason,$_VisitCode,$_CenterCode,$_VisitId,$_Lat,$_Long,$_spid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            date_default_timezone_set('Asia/Calcutta');
        $_Date = date("Y-m-d H:i:s");
            $_VisitId = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitId);
			
            if($_VisitCode == '1'){
             $_UpdateQuery = "update tbl_visit_requests set status='2', close_reason='" . $_Reason . "',"
                     . "close_date='" . $_Date . "',close_Lat='" . $_Lat . "', close_Long='" . $_Long . "', spid='" . $_spid . "'
					 where id='" . $_VisitId . "'";   
        
            } else {
            $_UpdateQuery = "";
            }
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function GetITGKCodes($_VisitTypeCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
			$_VisitTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitTypeCode);
			
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if($_VisitTypeCode =='1'){
                  $_SelectQuery = "select a.itgk_code as ITGKCODE,a.visit_date ,a.status from tbl_visit_requests as a 
                                where a.status='0' and a.request_by='" . $_SESSION['User_Code'] . "'";  
                } else if ($_VisitTypeCode =='2'){
                   $_SelectQuery = "select a.NCRVisit_Center_Code as ITGKCODE,a.NCRVisit_Selected_Date,a.NCRVisit_Status,b.ITGK_Name 
                                from tbl_ncr_visit as a inner join vw_itgkname_distict_rsp as b 
                                on a.NCRVisit_Center_Code=b.ITGKCODE 
                                where a.NCRVisit_Status='0' and a.NCRVisit_UserCode='" . $_SESSION['User_Code'] . "'";  
                }
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
       public function GetVisitCloseReason() {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                
                  $_SelectQuery = "select * from tbl_visitclosereason_master where VisitClose_Status='1'";  
               
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    
         public function GetDatabyCode($_VisitTypeCode,$_CenterCode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();


        try {
				$_VisitTypeCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_VisitTypeCode);
				$_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
				
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                if($_VisitTypeCode =='1'){
                  $_SelectQuery = "select a.id as visitid,a.itgk_code as ITGKCODE,a.visit_date AS VISITDATE,a.status AS STATUS,b.ITGK_Name from tbl_visit_requests as a 
                                inner join vw_itgkname_distict_rsp as b on a.itgk_code=b.ITGKCODE 
                                where a.status='0' and a.itgk_code='" . $_CenterCode . "' and a.request_by='" . $_SESSION['User_Code'] . "'";  
                } else if ($_VisitTypeCode =='2'){
                   $_SelectQuery = "select a.NCRFeedback_Code,a.NCRVisit_Center_Code as ITGKCODE,a.NCRVisit_Selected_Date AS VISITDATE,a.NCRVisit_Status AS STATUS,b.ITGK_Name 
                                from tbl_ncr_visit as a inner join vw_itgkname_distict_rsp as b 
                                on a.NCRVisit_Center_Code=b.ITGKCODE 
                                where a.NCRVisit_Status='0' and a.NCRVisit_Center_Code='" . $_CenterCode . "' and a.NCRVisit_UserCode='" . $_SESSION['User_Code'] . "'";  
                }
            
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
            
            } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetVisitor()
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                    $_SelectQuery = "select * from tbl_biomatric_enrollments WHERE userId='" . $_SESSION['User_Code'] . "' and status='1'";
                    
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
        
          public function GetLearnerByPIN($id)
        {
            global $_ObjConnection;
            $_ObjConnection->Connect();
            try {
					$id = mysqli_real_escape_string($_ObjConnection->Connect(),$id);
					
                if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
                    
                    $_SelectQuery = "select * from tbl_biomatric_enrollments WHERE status='1' and id='" . $id . "'";
                    
                    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                } else {
                    session_destroy();
                    ?>
                    <script> window.location.href = "index.php";</script>
                    <?php

                }
            } catch (Exception $_ex) {

                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }
            return $_Response;
        }
        
}
