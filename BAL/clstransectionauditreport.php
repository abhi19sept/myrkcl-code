<?php

/**
 * Description of clsAdmissionSummary
 *
 * @author Abhi
 */
require 'DAL/classconnectionNEW.php';
date_default_timezone_set("Asia/Kolkata");

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$_ObjConnection = new _Connection();
$_ObjConnection->Connect();

$_Response = array();
$_Response3 = array();

class clstransectionauditreport {

    //put your code here
    function dbConnection() {
        global $_ObjConnection;

        return $_ObjConnection;
    }

    public function GetAll($type, $dateFrom, $dateTo) {
        $_ObjConnection = $this->dbConnection();
        try {
            $report = $this->getPaymentTransactions($type, $dateFrom, $dateTo);
            switch ($type) {
                case 'LearnerFeePayment':
                    $report = $this->getLearnerFeeTransactions($report, $type, $dateFrom, $dateTo);
                    $report = $this->getAdmissionData($report, $type, $dateFrom, $dateTo);
                    $report = $this->getAdmissionInvoices($report, $type);
                    break;
                case 'ReexamPayment':
                    $report = $this->getReExamTransactions($report);
                    $report = $this->getReExamData($report);
                    $report = $this->getReExamInvoices($report, $type);
                    break;
                case 'Correction Certificate':
                case 'Duplicate Certificate':
                    $report = $this->getCorrectionTransactions($report);
                    $report = $this->getCorrectionData($report);
                    $report = $this->getCorrectionInvoices($report, $type);
                    break;
                case 'NcrFeePayment':
                    $report = $this->getNCRTransactions($report);
                    $report = $this->getNCRData($report);
                    $report = $this->getNCRInvoices($report, $type);
                    break;
                case 'EOI Fee Payment':
                    $report = $this->getEOITransactions($report);
                    $report = $this->getEOIData($report);
                    $report = $this->getEOIInvoices($report, $type);
                    break;
                case 'NameAddressFeePayment':
                    $report = $this->getNameAddressChangeTransactions($report);
                    $report = $this->getNameAddressChangeData($report);
                    $report = $this->getNameAddressChangeInvoices($report, $type);
                    break;
            }
            $report = $this->getRefunds($report);
            $_Response[0] = $report;
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }

        return $_Response;
    }

    private function getSubTypes($type) {
        $types = [$type];
        switch ($type) {
            case 'ReexamPayment':
                $types[] = 'Re Exam Event Name';
                break;
            case 'NcrFeePayment':
                $types[] = 'Ncr Fee Payment';
                $types[] = 'NcrRSCFAFeePayment';
                break;
            case 'NameAddressFeePayment':
                $types[] = 'Name Change Fee Payment';
                $types[] = 'Address Change Fee Payment';
                break;
        }

        return $types;
    }
    
    public function getPaymentTransactions($type, $dateFrom, $dateTo) {
        $_ObjConnection = $this->dbConnection();
         
        $from = explode('-', $dateFrom);
        $fromTimestamp = mktime(0, 0, 0, $from[1], $from[0], $from[2]);
        $fromTime = date("Y-m-d H:i:s", $fromTimestamp);

        $to = explode('-', $dateTo);
        $toTimestamp = mktime(23, 59, 59, $to[1], $to[0], $to[2]);
        $toTime = date("Y-m-d H:i:s", $toTimestamp);

        $filter = " AND timestamp >= '" . $fromTime . "' AND timestamp <= '" . $toTime . "'";

        $types = $this->getSubTypes($type);

        $query = "SELECT Pay_Tran_Status, Pay_Tran_Amount, Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE Pay_Tran_ProdInfo IN ('". implode("','", $types) ."') " . $filter;
        $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        $report = [];
        while ($row = mysqli_fetch_array($response[2])) {
            $report[$row['Pay_Tran_Status']]['Payment_Transactions'][$row['Pay_Tran_PG_Trnid']]['Amount'][] = $row['Pay_Tran_Amount'];
        }

        $report = $this->getPaymentTransactionsCount($report, $type, $fromTime, $toTime);
        //$report = $this->getPayUTransactions($report, $type, $fromTime, $toTime);

        return $report;
    }

    public function getPaymentTransactionsCount($report, $type, $fromTime, $toTime) {
        $_ObjConnection = $this->dbConnection();
        $filter = " AND timestamp >= '" . $fromTime . "' AND timestamp <= '" . $toTime . "'";

        $types = $this->getSubTypes($type);

        $query = "
        (
            SELECT Pay_Tran_Status, SUM(Pay_Tran_Amount) AS amount, COUNT(*) AS TotalRows FROM tbl_payment_transaction WHERE Pay_Tran_ProdInfo IN ('". implode("','", $types) ."')  $filter GROUP BY Pay_Tran_Status
        )
            UNION
        (
            SELECT 'RKCL_Total', SUM(pt.Pay_Tran_Amount), COUNT(*) FROM tbl_payment_transaction pt WHERE pt.Pay_Tran_ProdInfo IN ('". implode("','", $types) ."') $filter
        )
        ";
        $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        while ($row = mysqli_fetch_array($response[2])) {
            $report[$row['Pay_Tran_Status']]['Payment_Transactions']['count'] = $row['TotalRows'];
            $report[$row['Pay_Tran_Status']]['Payment_Transactions']['Amount'] = $row['amount'];
        }

        return $report;
    }

    public function getRefunds($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(*) AS TotalRows, SUM(Payment_Refund_Amount) AS Amount FROM tbl_payment_refund WHERE Payment_Refund_Txnid IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['Refunds'] = [
               'count' =>  $row['TotalRows'],
               'Amount' =>  $row['Amount'],
            ];
        }

        return $report;
    }

    public function getReExamTransactions($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(*) AS TotalRows, SUM(Reexam_Transaction_Amount) AS Amount FROM tbl_reexam_transaction WHERE Reexam_Transaction_Txtid IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table1_transactions'] = [
               'count' =>  $row['TotalRows'],
               'Amount' =>  $row['Amount'],
            ];
        }

        return $report;
    }

    public function getReExamData($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $payTransactions = array_keys($transactions['Payment_Transactions']);
            $trnsactionsIds = "'" . implode("','", $payTransactions) . "'";
            $query = "SELECT COUNT(distinct reexam_TranRefNo) AS TotalRows, COUNT(reexam_TranRefNo) AS LearnersCount FROM examdata WHERE reexam_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table2_transactions'] = [
               'count' =>  $row['TotalRows'],
               'LearnersCount' => $row['LearnersCount'],
            ];

            $paymentTransactions = $transactions['Payment_Transactions'];
            $query = "SELECT distinct reexam_TranRefNo FROM examdata WHERE reexam_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            while ($row = mysqli_fetch_array($response[2])) {
                unset($paymentTransactions[$row['reexam_TranRefNo']]);
            }
            unset($paymentTransactions['count']);
            unset($paymentTransactions['Amount']);

            if (!empty($paymentTransactions['Payment_Transactions'])) {
                $report[$type]['table2_transactions']['missings'] = implode(", ", array_keys($paymentTransactions['Payment_Transactions']));
            }
        }

        return $report;
    }

    public function getLearnerFeeTransactions($report, $type, $dateFrom, $dateTo) {
        $_ObjConnection = $this->dbConnection();

        $from = explode('-', $dateFrom);
        $fromTimestamp = mktime(0, 0, 0, $from[1], $from[0], $from[2]);
        $fromTime = date("Y-m-d H:i:s", $fromTimestamp);

        $to = explode('-', $dateTo);
        $toTimestamp = mktime(23, 59, 59, $to[1], $to[0], $to[2]);
        $toTime = date("Y-m-d H:i:s", $toTimestamp);

        foreach ($report as $type => $trans) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;

            $query = "SELECT COUNT(*) AS TotalRows, SUM(adt.Admission_Transaction_Amount) AS Amount FROM tbl_admission_transaction adt INNER JOIN tbl_payment_transaction pt ON adt.Admission_Transaction_Txtid = pt.Pay_Tran_PG_Trnid WHERE pt.Pay_Tran_Status LIKE ('%" . trim($type) . "%') AND pt.timestamp >= '" . $fromTime . "' AND pt.timestamp <= '" . $toTime . "'";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table1_transactions'] = [
               'count' =>  $row['TotalRows'],
               'Amount' =>  $row['Amount'],
            ];
        }

        return $report;
    }

    public function getAdmissionData($report, $type, $dateFrom, $dateTo) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(distinct Admission_TranRefNo) AS TotalRows, COUNT(Admission_TranRefNo) AS LearnersCount FROM tbl_admission WHERE Admission_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table2_transactions'] = [
               'count' =>  $row['TotalRows'],
               'LearnersCount' => $row['LearnersCount'],
            ];
        }

        return $report;
    }

    public function getCorrectionTransactions($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(*) AS TotalRows, SUM(Correction_Transaction_Amount) AS Amount FROM tbl_correction_transaction WHERE Correction_Transaction_Txtid IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table1_transactions'] = [
               'count' =>  $row['TotalRows'],
               'Amount' =>  $row['Amount'],
            ];
        }

        return $report;
    }

    public function getCorrectionData($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(distinct Correction_TranRefNo) AS TotalRows, COUNT(Correction_TranRefNo) AS LearnersCount FROM tbl_correction_copy WHERE Correction_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table2_transactions'] = [
               'count' =>  $row['TotalRows'],
               'LearnersCount' => $row['LearnersCount'],
            ];
        }

        return $report;
    }

    public function getNCRTransactions($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(*) AS TotalRows, SUM(Ncr_Transaction_Amount) AS Amount FROM tbl_ncr_transaction WHERE Ncr_Transaction_Txtid IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table1_transactions'] = [
               'count' =>  $row['TotalRows'],
               'Amount' =>  $row['Amount'],
            ];
        }

        return $report;
    }

    public function getNCRData($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(distinct Org_TranRefNo) AS TotalRows, COUNT(Org_TranRefNo) AS LearnersCount FROM tbl_org_master WHERE Org_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table2_transactions'] = [
               'count' =>  $row['TotalRows'],
               'LearnersCount' => $row['LearnersCount'],
            ];
        }

        return $report;
    }

    public function getEOITransactions($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(*) AS TotalRows, SUM(EOI_Transaction_Amount) AS Amount FROM tbl_eoi_transaction WHERE EOI_Transaction_Txtid IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table1_transactions'] = [
               'count' =>  $row['TotalRows'],
               'Amount' =>  $row['Amount'],
            ];
        }

        return $report;
    }

    public function getEOIData($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(distinct Courseitgk_TranRefNo) AS TotalRows, COUNT(Courseitgk_TranRefNo) AS LearnersCount FROM tbl_courseitgk_mapping WHERE Courseitgk_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table2_transactions'] = [
               'count' =>  $row['TotalRows'],
               'LearnersCount' => $row['LearnersCount'],
            ];
        }

        return $report;
    }

    public function getNameAddressChangeTransactions($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(*) AS TotalRows, SUM(fld_amount) AS Amount FROM tbl_address_name_transaction WHERE fld_transactionID IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table1_transactions'] = [
               'count' =>  $row['TotalRows'],
               'Amount' =>  $row['Amount'],
            ];
        }

        return $report;
    }

    public function getNameAddressChangeData($report) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(distinct ca.fld_ref_no) AS TotalRows, COUNT(ca.fld_ref_no) AS LearnersCount FROM tbl_change_address_itgk ca INNER JOIN tbl_address_name_transaction at ON ca.fld_ref_no = at.fld_ref_no WHERE at.fld_transactionID IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table2_transactions'] = [
               'count' =>  $row['TotalRows'],
               'LearnersCount' => $row['LearnersCount'],
            ];
        }

        return $report;
    }

    public function getPayUTransactions($report, $type, $fromTime, $toTime) {
        $_ObjConnection = $this->dbConnection();
        $filter = " AND payuresponsetime >= '" . $fromTime . "' AND payuresponsetime <= '" . $toTime . "'";
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(*) AS TotalRows, SUM(amount) AS Amount, status FROM tbl_payutransactionresponse WHERE txnid IN (" . $trnsactionsIds . ") $filter GROUP BY status";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            while ($row = mysqli_fetch_array($response[2])) {
                $report['PayU'][$row['status']][$type] = [
                   'count' =>  $row['TotalRows'],
                   'Amount' =>  $row['Amount'],
                ];
            }
        }

        return $report;
    }

    public function optimizeTransactionsRecords($filters) {

        $from = explode('-', $filters['dateFrom']);
        $fromTimestamp = mktime(0, 0, 0, $from[1], $from[0], $from[2]);
        $fromTime = date("Y-m-d H:i:s", $fromTimestamp);

        $to = explode('-', $filters['dateTo']);
        $toTimestamp = mktime(23, 59, 59, $to[1], $to[0], $to[2]);
        $toTime = date("Y-m-d H:i:s", $toTimestamp);

        $this->checkRefundedTransactions($filters['transaction_type'], $fromTime, $toTime);
        $this->checkForMissingTransactions($filters['transaction_type'], $fromTime, $toTime);
		$this->checkForMissingInvoices($filters['transaction_type'], $fromTime, $toTime);
        return;
    }

    private function checkRefundedTransactions($type, $fromTime, $toTime) {
        $_ObjConnection = $this->dbConnection();
        $query = "UPDATE tbl_payment_transaction pt INNER JOIN tbl_payment_refund pr ON pr.Payment_Refund_Txnid = pt.Pay_Tran_PG_Trnid SET pt.Pay_Tran_Status = 'PaymentInProcess', pt.Pay_Tran_Reconcile_status = 'Not Reconciled' WHERE pt.Pay_Tran_ProdInfo = '" . $type . "' AND pt.Pay_Tran_Status NOT LIKE ('%ref%') AND pt.timestamp >= '" . $fromTime . "' AND pt.timestamp <= '" . $toTime . "'";
        $response = $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
    }
	

    private function checkForMissingTransactions($proInfo, $fromTime, $toTime) {
        $_ObjConnection = $this->dbConnection();
        switch ($proInfo) {
            case 'LearnerFeePayment':
                $table = 'tbl_admission_transaction';
                $refField = 'Admission_Transaction_Txtid';
				$statusField = 'Admission_Transaction_Status';
                break;
            case 'ReexamPayment':
                $table = 'tbl_reexam_transaction';
                $refField = 'Reexam_Transaction_Txtid';
				$statusField = 'Reexam_Transaction_Status';
                break;
            case 'Correction Certificate':
            case 'Duplicate Certificate':
                $table = 'tbl_correction_transaction';
                $refField = 'Correction_Transaction_Txtid';
				$statusField = 'Correction_Transaction_Status';
                break;
        }

        $query = "UPDATE tbl_payment_transaction SET Pay_Tran_Status = 'PaymentInProcess', Pay_Tran_Reconcile_status = 'Not Reconciled' WHERE Pay_Tran_Status LIKE ('%receive%') AND Pay_Tran_ProdInfo = '" . $proInfo . "' AND timestamp >= '" . $fromTime . "' AND timestamp <= '" . $toTime . "' AND Pay_Tran_PG_Trnid NOT IN (SELECT " . $refField . " FROM " . $table . "  WHERE " . $statusField . " LIKE('%success%'))";
        $response = $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
    }
	
	private function checkForMissingInvoices($proInfo, $fromTime, $toTime) {
        $_ObjConnection = $this->dbConnection();
        switch ($proInfo) {
            case 'LearnerFeePayment':
                $table = 'tbl_admission';
                $tableInvoice = 'tbl_admission_invoice';
                $trnxField = 'Admission_TranRefNo';
                $refField = 'Admission_Code';
                break;
            case 'ReexamPayment':
                $table = 'examdata';
                $tableInvoice = 'tbl_reexam_invoice';
                $trnxField = 'reexam_TranRefNo';
                $refField = 'examdata_code';
                break;
            case 'Correction Certificate':
            case 'Duplicate Certificate':
                $table = 'tbl_correction_copy';
                $tableInvoice = 'tbl_correction_invoice';
                $trnxField = 'Correction_TranRefNo';
                $refField = 'cid';
                break;
        }

        $refFieldInvoice = ($tableInvoice == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($tableInvoice == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');

        $query = "UPDATE tbl_payment_transaction pt INNER JOIN " . $table . " ta ON ta." . $trnxField . " = pt.Pay_Tran_PG_Trnid SET pt.Pay_Tran_Status = 'PaymentInProcess', Pay_Tran_Reconcile_status = 'Not Reconciled' WHERE pt.Pay_Tran_ProdInfo LIKE('" . $proInfo . "') AND pt.timestamp >= '" . $fromTime . "' AND pt.timestamp <= '" . $toTime . "' AND pt.Pay_Tran_Status LIKE('%receive%') AND ta." . $refField . " NOT IN (SELECT " . $refFieldInvoice . " FROM " . $tableInvoice . " WHERE addtime >= '" . $fromTime . "')";
        $response = $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
    }

    public function getAdmissionInvoices($report, $type) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(ai.invoice_no) AS InvoiceCount FROM tbl_admission_invoice ai INNER JOIN tbl_admission ad ON ad.Admission_Code = ai.invoice_ref_id WHERE ad.Admission_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table3_transaction_invoices'] = [
               'count' =>  $row['InvoiceCount'],
            ];
        }

        return $report;
    }

    public function getCorrectionInvoices($report, $type) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(ci.invoice_no) AS InvoiceCount FROM tbl_correction_invoice ci INNER JOIN tbl_correction_copy cc ON cc.cid = ci.transaction_ref_id WHERE cc.Correction_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table3_transaction_invoices'] = [
               'count' =>  $row['InvoiceCount'],
            ];
        }

        return $report;
    }

    public function getReExamInvoices($report, $type) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(ri.invoice_no) AS InvoiceCount FROM tbl_reexam_invoice ri INNER JOIN examdata ed ON ed.examdata_code = ri.exam_data_code WHERE ed.reexam_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table3_transaction_invoices'] = [
               'count' =>  $row['InvoiceCount'],
            ];
        }

        return $report;
    }

    public function getNCRInvoices($report, $type) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(ni.invoice_no) AS InvoiceCount FROM tbl_ncr_invoice ni INNER JOIN tbl_org_master om ON om.Org_Ack = ni.Ao_Ack_No WHERE om.Org_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table3_transaction_invoices'] = [
               'count' =>  $row['InvoiceCount'],
            ];
        }

        return $report;
    }

    public function getEOIInvoices($report, $type) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
            $query = "SELECT COUNT(ei.invoice_no) AS InvoiceCount FROM tbl_eoi_invoice ei INNER JOIN tbl_courseitgk_mapping cm ON cm.Courseitgk_EOI = ei.eoi_code WHERE cm.Courseitgk_TranRefNo IN (" . $trnsactionsIds . ")";
            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table3_transaction_invoices'] = [
               'count' =>  $row['InvoiceCount'],
            ];
        }

        return $report;
    }

    public function getNameAddressChangeInvoices($report, $type) {
        $_ObjConnection = $this->dbConnection();
        foreach ($report as $type => $transactions) {
            if ($type == 'RKCL_Total' || $type == 'PayU') continue;
            $trnsactionsIds = "'" . implode("','", array_keys($transactions['Payment_Transactions'])) . "'";
           $query = "SELECT COUNT(ai.invoice_no) AS InvoiceCount FROM tbl_nameaddress_invoice ai INNER JOIN tbl_address_name_transaction ad ON ad.fld_ref_no = ai.invoice_ref_id WHERE ad.fld_transactionID IN (" . $trnsactionsIds . ")";

            $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            $row = mysqli_fetch_array($response[2]);
            $report[$type]['table3_transaction_invoices'] = [
               'count' =>  $row['InvoiceCount'],
            ];
        }

        return $report;
    }
}
