<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsNCRCenterVisit
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsNCRCenterVisit {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
//            if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 || $_SESSION['User_UserRoll'] == 4) {
            $_SelectQuery = "Select DISTINCT a.*, b.Org_Type_Name as Organization_Type, c.District_Name as Organization_District_Name,"
                    . " d.Tehsil_Name as Organization_Tehsil, e.* FROM tbl_org_master as a INNER JOIN tbl_org_type_master as b"
                    . " ON a.Organization_Type=b.Org_Type_Code INNER JOIN tbl_district_master as c "
                    . " ON a.Organization_District=c.District_Code"
                    . " INNER JOIN tbl_tehsil_master as d"
                    . " ON a.Organization_Tehsil=d.Tehsil_Code INNER JOIN tbl_user_master as e "
                    . " ON a.Org_Ack=e.User_Ack INNER JOIN tbl_intake_master as f "
                    . " ON e.User_LoginId=f.Intake_Center INNER JOIN tbl_staff_detail as g "
                    . " ON e.User_LoginId=g.Staff_User INNER JOIN tbl_bank_account as h "
                    . " ON e.User_LoginId=h.Bank_User_Code INNER JOIN tbl_it_peripherals as i "
                    . " ON e.User_LoginId=i.IT_UserCode INNER JOIN tbl_premises_details as j "
                    . " ON e.User_LoginId=j.Premises_User INNER JOIN tbl_userprofile as k "
                    . " ON e.User_Code=k.UserProfile_User INNER JOIN tbl_rspitgk_mapping as l"
                    . " ON e.User_LoginId=l.Rspitgk_ItgkCode INNER JOIN tbl_payment_transaction as m "
                    . " ON e.User_LoginId=m.Pay_Tran_ITGK "
                    . " WHERE a.Org_Visit='Yes' AND m.Pay_Tran_Status = 'PaymentReceive' AND m.Pay_Tran_ProdInfo = 'NcrFeePayment' "
                    . " AND e.User_PaperLess = 'Yes' AND e.User_Rsp = '" . $_SESSION['User_Code'] . "'";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function Add($_CenterCode, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
            $_CenterCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_CenterCode);
			
                 $_InsertQuery = "Insert Into tbl_ncrvisit_photo"
                    . "(NCRVisit_LoginId,NCRVisit_CenterCode,NCRVisit_TheoryRoom,NCRVisit_Reception,NCRVisit_Exterior,"
                    . " NCRVisit_Other,NCRVisit_Date) values "                   
                    . "('" . $_SESSION['User_LoginId'] . "','" . $_CenterCode . "','" . $PANfilename . "',"
                    . "'" . $UIDfilename . "','" . $addprooffilename . "','" . $appformfilename . "','" . $_Date . "')";
            
                $_DuplicateQuery1 = "Select * From tbl_ncrvisit_photo Where NCRVisit_LoginId='" . $_SESSION['User_LoginId'] . "' "
                        . " AND NCRVisit_CenterCode = '" . $_CenterCode . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);

                if ($_Response1[0] == Message::NoRecordFound) {
                 	
                        $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                                         
                } else {
                    echo " NCR Visit Photos Already uploaded for this Center.";
                    return;                     
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "logout.php";</script> 
                <?php

            }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
