<?php
/**
 * Description of clsReligionMaster
 *
 * @author VIVEK
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsReligionMaster {
    //put your code here
    
    public function GetAll() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select Religion_Code,Religion_Name,"
                    . "Status_Name From tbl_religion_master as a inner join tbl_status_master as b "
                    . "on a.Religion_Status=b.Status_Code";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Religion_Code)
    {   //echo $_Religion_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Religion_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Religion_Code);
				
            $_SelectQuery = "Select Religion_Code,Religion_Name,Religion_Status"
                    . " From tbl_religion_master Where Religion_Code='" . $_Religion_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Religion_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Religion_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Religion_Code);
				
            $_DeleteQuery = "Delete From tbl_religion_master Where Religion_Code='" . $_Religion_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_ReligionName,$_ReligionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ReligionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ReligionName);
				$_ReligionStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_ReligionStatus);
				
            $_InsertQuery = "Insert Into tbl_religion_master(Religion_Code,Religion_Name,"
                    . "Religion_Status) "
                    . "Select Case When Max(Religion_Code) Is Null Then 1 Else Max(Religion_Code)+1 End as Religion_Code,"
                    . "'" . $_ReligionName . "' as Religion_Name,"
                    . "'" . $_ReligionStatus . "' as Religion_Status"
                    . " From tbl_religion_master";
            $_DuplicateQuery = "Select * From tbl_religion_master Where Religion_Name='" . $_ReligionName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_ReligionCode,$_ReligionName,$_ReligionStatus) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_ReligionCode = mysqli_real_escape_string($_ObjConnection->Connect(),$_ReligionCode);
				$_ReligionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_ReligionName);
				$_ReligionStatus = mysqli_real_escape_string($_ObjConnection->Connect(),$_ReligionStatus);
				
            $_UpdateQuery = "Update tbl_religion_master set Religion_Name='" . $_ReligionName . "',"
                    . "Religion_Status='" . $_ReligionStatus . "' Where Religion_Code='" . $_ReligionCode . "'";
            $_DuplicateQuery = "Select * From tbl_Religion_master Where Religion_Name='" . $_ReligionName . "' "
                    . "and Religion_Code <> '" . $_ReligionCode . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
