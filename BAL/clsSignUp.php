<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsSignUp
 *
 *  author Viveks
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsSignUp {
    //put your code here
    
    public function Add($_EmailId,$_Mobile) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_EmailId = mysqli_real_escape_string($_ObjConnection->Connect(),$_EmailId);
				$_Mobile = mysqli_real_escape_string($_ObjConnection->Connect(),$_Mobile);
				
            $_InsertQuery = "Insert Into tbl_user_master(User_Code,User_EmailId,"
                    . "User_MobileNo) "
                    . "Select Case When Max(User_Code) Is Null Then 1 Else Max(User_Code)+1 End as User_Code,"
                    . "'" . $_EmailId . "' as User_EmailId,'" . $_Mobile . "' as User_MobileNo"
                    . " From tbl_user_master";
            $_DuplicateQuery = "Select * From tbl_user_master Where User_EmailId='" . $_EmailId . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_DLCName,$_PSAName) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = "Update tbl_user_master set User_ParentId='" . $_DLCName . "|" . $_PSAName . "'"
                    . " Where User_Code='" . $_Code . "'";

                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
 
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
}
