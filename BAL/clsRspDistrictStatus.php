<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsRspDistrictStatus
 *
 * @author VIVEK
 */
require 'DAL/classconnectionNEW.php';


$_ObjConnection = new _Connection();
$_Response = array();

class clsRspDistrictStatus {

    //put your code here

    public function FILLStatus() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT DISTINCT Rsptarget_Status FROM tbl_rsptarget";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
    
    public function GetAll($_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_User = $_SESSION['User_LoginId'];
			$_Status = mysqli_real_escape_string($_ObjConnection->Connect(),$_Status);
			
            $_SelectQuery = "Select a.*,b.District_Name"
                    . " From tbl_rsptarget as a inner join tbl_district_master as b "
                    . "on a.Rsptarget_District"
                    . "=b.District_Code WHERE Rsptarget_User='" . $_User . "' AND Rsptarget_Status='" . $_Status . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }

}
