<?php


/**
 * Description of Innaugrationdetails
 *
 * @author Yogendra Soni
 */

require 'DAL/classconnectionNEW.php';
require 'DAL/sendsms.php';

$_ObjConnection = new _Connection();
$_Response = array();
$_Response2 = array();
$_Response3 = array();

class clsInnaugrationdetails {
	/* for show function */
	public function GetAll()
    {   //echo $_Country_Code;
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			
			if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
			{
			//mysqli_set_charset('utf8');
			
              $_SelectQuery = "Select a.Innaug_janpratinidhi_name,a.Innaug_designation,a.Innaug_area_details,a.Innaug_end_date,a.Innaug_other_officer
             ,a.Innaug_image1,a.Innaug_image2,a.centercode,b.Batch_Name,c.Course_Name  From tbl_Inauguration_details as a inner join tbl_batch_master as b on a.Batchcode=b.Batch_Code inner join tbl_course_master as c on a.Coursecode=c.Course_Code where  a.centercode=".$_SESSION['User_LoginId']." ";
             $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
			// print_r($_Response);
		    }
		    else 
		    {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
	/* for Designation function */  
	public function Getdesignation()
    {   
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
			//mysqli_set_charset('utf8');
            $_SelectQuery = "Select *  From tbl_Inauguration_designation order by Id";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           // print_r($_Response);
        } 
		catch (Exception $_ex) 
		{

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
	/*for add function */
	public function Add($_Name,$_desig,$_txtarea,$_enddate,$_otherofficer,$_batchcode,$_coursecode) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
            $_coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_coursecode);
			 if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) 
			 {
				 $_image1 =	$_SESSION['User_LoginId'].'_'.$_batchcode.'_image1.jpg';
				 $_image2 =	$_SESSION['User_LoginId'].'_'.$_batchcode.'_image2.jpg';
			
			
			
			    $_SelectQuery = "select * from tbl_wcd_intake_master where Wcd_Intake_Course='".$_coursecode."' and Wcd_Intake_Batch='".$_batchcode."' AND Wcd_Intake_Center ='" .$_SESSION['User_LoginId'] . "'";
			    $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				//print_r($_Response[0]);
				if($_Response[0]=='Success')
		        {
					 $_InsertQuery = "Insert Into tbl_Inauguration_details(id
					,Innaug_janpratinidhi_name,Innaug_designation,Innaug_area_details,"
						."Innaug_end_date,Innaug_other_officer,centercode,Innaug_image1,Innaug_image2,Batchcode,Coursecode) "
						. "Select Case When Max(id) Is Null Then 1 Else Max(id)+1 End as id,"
						. "'" . $_Name . "' as Innaug_janpratinidhi_name, "
						. "'" . $_desig . "' as Innaug_designation,'" . $_txtarea . "' as Innaug_area_details, "
						. "'" . $_enddate . "' as Innaug_end_date,'" . $_otherofficer . "' as Innaug_other_officer, "
						. "'" . $_SESSION['User_LoginId'] . "' as centercode, "
					    . "'" . $_image1 . "' as Innaug_image1,'"
						. $_image2 . "' as Innaug_image2, '" 
						. $_batchcode . "' as Batchcode, '" 
						. $_coursecode . "' as Coursecode"
						. " From tbl_Inauguration_details";
					
					
					$_DuplicateQuery = "Select * From tbl_Inauguration_details Where centercode=".$_SESSION['User_LoginId']." AND Batchcode=".$_batchcode." AND Coursecode= '" . $_coursecode . "'";
					$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
					//$_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery1, Message::SelectStatement);
	
					if($_Response[0]==Message::NoRecordFound)
					{
				
						$_image11 = '../upload/Inauguration/'.$_image1;
						$_image22 = '../upload/Inauguration/'.$_image2;
						
						if (file_exists($_image11) && file_exists($_image22) ) 
						{				
							$_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					
						}
						else
						{
								
							echo "Please Attach Necessary Documents on first page ";
							return;
						}				
					}
					else 
					{
						$_Response[0] = Message::DuplicateRecord;
						$_Response[1] = Message::Error;
					}
				}
				else
				{
					 echo "You are not authorised for this course.";
					 return;
				}
			}
			 else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php

            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	/* for get Details using batchcode and course code */
	
	public function GetDetails($_batchcode,$_coursecode)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
                $_batchcode = mysqli_real_escape_string($_ObjConnection->Connect(),$_batchcode);
                $_coursecode = mysqli_real_escape_string($_ObjConnection->Connect(),$_coursecode);
			
				$_SelectQuery = "select * from tbl_wcd_intake_master where Wcd_Intake_Course='".$_coursecode."' and Wcd_Intake_Batch='".$_batchcode."' AND Wcd_Intake_Center ='" .$_SESSION['User_LoginId'] . "'";
			    $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
				//print_r($_Response[0]);
				if($_Response1[0]=='Success')
		        {
			
					//echo $_SESSION['User_LoginId'];
					$_SelectQuery = "Select * from tbl_Inauguration_details Where Batchcode= '" . $_batchcode . "' AND Coursecode= '" . $_coursecode . "' AND centercode=".$_SESSION['User_LoginId']."";
					$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					if($_Response[0]=='Success')
					{
						echo "You have already submitted details.";
						return;						
					}
					else
					{
						return $_Response;
					}
					//print_r($_Response);
				}
				else
				{
					 echo "You are not authorized for this course.";
					 return;
				}
        } 
		catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         
    }
	
	/* for course function */
	
	public function Getcourse() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
			$_SelectQuery = "Select Course_Code, Course_Name From tbl_course_master where Course_Code='3' or Course_Code='24' ";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	/* for Batch function */
	
	public function Getbatch($_course) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $_course = mysqli_real_escape_string($_ObjConnection->Connect(),$_course);
		    $_SelectQuery = "Select Batch_Code,Batch_Name From tbl_batch_master where Course_Code='".$_course ."' order by Batch_Code DESC";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
 
    
}
