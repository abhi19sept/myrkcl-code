<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clsupdateseourl
 *
 * @author yogendra 
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsupdateseourl 
{
    //put your code here
    public function GetAll() 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
            $_SelectQuery = "Select Function_Code,Function_Name,Function_URL,Function_SEO,Parent_Function_Name,"
                    . "Status_Name,Function_Display,Root_Menu_Name From tbl_function_master as a inner join tbl_parent_function_master as b "
                    . "on a.function_parent=b.Parent_Function_Code inner join tbl_status_master as c "
                    . "on a.Function_Status=c.Status_Code Inner Join tbl_root_menu as d on b.Parent_Function_Root=d.Root_Menu_Code"
                    . " Order By D.Root_Menu_Display,b.Parent_Function_Display,a.Function_Display";
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } 
		catch (Exception $_ex) 
		{

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function Update($_Code,$_SeoURL) 
	{
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try 
		{
            $_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
			$_SeoURL = mysqli_real_escape_string($_ObjConnection->Connect(),$_SeoURL);
			
			
            $_UpdateQuery = "Update tbl_function_master set Function_SEO='" . $_SeoURL ."'
			Where Function_Code='" . $_Code . "' ";
			//die;
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
        } 
		catch (Exception $_e) 
		{
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
	
	
	public function GetChild($_Parent) 
	{
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select DISTINCT  FunctionCode,FunctionName,FunctionURL  From vw_userrolewisefunction where Parent='" . $_Parent . "' Order by Display";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	
	public function Getfunctionurl($_child) 
	{
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {

             $_SelectQuery = "Select DISTINCT  FunctionSEO,FunctionURL From vw_userrolewisefunction where FunctionCode='" . $_child . "' Order by Display";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
}
