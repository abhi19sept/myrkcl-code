<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of clsStateMaster
 *
 * @author Abhi
 */

require 'DAL/classconnectionNEW.php';

$_ObjConnection = new _Connection();
$_Response = array();

class clsRegionMaster {
    //put your code here
     public function GetAll($state) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$state = mysqli_real_escape_string($_ObjConnection->Connect(),$state);
				
            if(!$state)
            {
            $_SelectQuery = "Select Region_Code,Region_Name,State_Name,"
                    . "Status_Name,Country_Name From tbl_region_master as a inner join tbl_state_master as b "
                    . "on a.Region_State=b.State_Code inner join tbl_status_master as c "
                    . "on a.Region_Status=c.Status_Code inner join tbl_country_master as d "
                    . "on b.State_Country=d.Country_Code ORDER BY State_Name, Region_Name";
            }
            else {
                $_SelectQuery = "Select Region_Code,Region_Name,State_Name,"
                    . "Status_Name,Country_Name From tbl_region_master as a inner join tbl_state_master as b "
                    . "on a.Region_State=b.State_Code inner join tbl_status_master as c "
                    . "on a.Region_Status=c.Status_Code inner join tbl_country_master as d "
                    . "on b.State_Country=d.Country_Code and a.Region_State='" . $state . "' ORDER BY State_Name, Region_Name";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            //print_r($_Response);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function GetDatabyCode($_Region_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Region_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Region_Code);
				
            $_SelectQuery = "Select Region_Code,Region_Name,Region_State,Region_Status "
                    . "From tbl_region_master Where Region_Code='" . $_Region_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
           
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    public function DeleteRecord($_Region_Code)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Region_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Region_Code);
				
            $_DeleteQuery = "Delete From tbl_region_master Where Region_Code='" . $_Region_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DeleteQuery, Message::DeleteStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    public function Add($_RegionName,$_Parent,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_RegionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RegionName);
				
            $_InsertQuery = "Insert Into tbl_region_master(Region_Code,Region_Name,"
                    . "Region_State,Region_Status) "
                    . "Select Case When Max(Region_Code) Is Null Then 1 Else Max(Region_Code)+1 End as Region_Code,"
                    . "'" . $_RegionName . "' as Region_Name,"
                    . "'" . $_Parent . "' as Region_State,'" . $_Status . "' as Region_Status"
                    . " From tbl_region_master";
            $_DuplicateQuery = "Select * From tbl_region_master Where Region_Name='" . $_RegionName . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            // print_r($_Response);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function Update($_Code,$_RegionName,$_State,$_Status) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				$_Code = mysqli_real_escape_string($_ObjConnection->Connect(),$_Code);
				$_RegionName = mysqli_real_escape_string($_ObjConnection->Connect(),$_RegionName);
				
            $_UpdateQuery = "Update tbl_region_master set Region_Name='" . $_RegionName . "',"
                    . "Region_State='" . $_State . "',"
                    . "Region_Status='" . $_Status . "' Where Region_Code='" . $_Code . "'";
            $_DuplicateQuery = "Select * From tbl_region_master Where Region_Name='" . $_RegionName . "' "
                    . "and Region_Code <> '" . $_Code . "'";
            
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                $_Response[0] = Message::DuplicateRecord;
                $_Response[1] = Message::Error;
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
}
