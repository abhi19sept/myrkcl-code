﻿<?php  ob_start(); 
$title="Online Faculty Exam";
include ('header.php'); 
include ('root_menu.php'); 
if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 23 ) {
    // you are in
} else {
    header('Location: logout.php');
    exit;
}
//if (isset($_REQUEST['code']) && !empty($_REQUEST['code'])) {
    
 ?>
<script>
var lat = "";
var long = "";
var startmode = "";
</script>
<style>
    .bxdesign{
      background-color: #dfdfdf;
    font-weight: bold;  
    } 
    .col-sm-3:hover {
    background-color: white !important;
}
</style>
<link href="css/onlineexam.css" rel="stylesheet" type="text/css">
<div style="min-height:350px !important;">	
    <div class="container">
    <div class="row">
    <div class="col-md-12" style="margin-top: 35px;">
    <div class="panel panel-primary packagepanelbdr">
        <div class="panel-heading packagepanel">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-bookmark"></span> Online Faculty Exam </h3>
        </div>
        <div class="panel-body" style="min-height: 398px;">
            <div class="col-md-12 bhoechie-tab-container">
                <div class="col-md-10 bhoechie-tab" style="width: 100%;">
                  <div class="bhoechie-tab-content active">
                    <center>
                        <div class="row">
                            <form class="form-horizontal"  name="startexamFormlist" id="startexamFormlist" enctype="multipart/form-data" style="text-align:left;">
                               <div class="col-md-3"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type='hidden' name="action" id="action" value="SHOWITGKFACULTY">
                                        <label for="inputEmail3" class="col-sm-3 control-label" style="text-align: left;">Enter Center Code</label>
                                        <div class="col-sm-3">
                                            <input type="text" id="itgkCode" name="itgkCode" class="form-control" required="required" onkeypress="javascript:return allownumbers(event);" maxlength="8" />
                                        </div>
                                        <div class="col-sm-3">
                                            <button type="button" name="btn-viewcenter"  id="btn-viewcenter" class="btnyogi btn btn-success btn-sm">Search</button>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-8" id="response"></div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class='col-md-12' id="CenterDetails">
                                    
                                </div>
                            </form>
                        </div>
                    </center>
                  </div>
                 </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
</div>
<div id="myModaldescinst" class="modal" style="padding-top: 5px;">
    <div class="modal-content" style="width: 90%;">
        <div class="modal-header" style="text-align: center;">
      <span class="close closeinst">&times;</span>
      <h6>Faculty Exam Instructions</h6>
    </div>
    <div class="modal-body modelscroll" style="height: 570px;overflow: auto;">
        <form name="startexamform" id="startexamform" method="POST">
            <input type='hidden' name='action' id='action' value='QuestionPaper'>
          <div class="row">
            <div style="text-align: center;" id="instresponse"></div>
            <div id="readinstruction">
               
            </div>
          </div>
        </form>
    </div>
<!--    <div class="modal-footer" style="margin-top:80px;">
      <h6>Online Assessment Exam Instructions</h6>
    </div>-->
    </div>
</div>
<div id="myModaldesc" class="modal" style="padding-top: 0;background-color: rgb(58, 75, 83);">
    <div class="modal-content" style="width: 100%;">
        <div class="modal-header" style="min-height: 75px !important;">
<!--      <span class="close closeres">&times;</span>-->
        <div class="col-md-6"><div class="user-info">
        <div class="image">
            <img id="myimg" src="" width="48" height="48" alt="User">
            <img id="mynoimg" src="images/user_learner.png" width="48" height="48" alt="User" style="display: none;">
        </div>
        <script>
            document.getElementById("myimg").onerror = function() {
                this.style.display = "none";
                document.getElementById("mynoimg").style.display = "block";
            }
        </script>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['UserRoll_Name'];?></div>
            <div class="email"><?php echo $_SESSION['Organization_Name'];?></div>
        </div>
    </div></div>
            <div class="col-md-6"><h6 style="float:right;">Faculty Exam</h6></div>
    </div>
        <div class="modal-body" id="scrollescp" style="height: 700px;overflow: auto;">
        <div class='col-sm-offset-3 col-sm-6' id='responsepopexamAns' style='min-height: 30px;'></div>
        <div class="" style="background-color: white;">
            <div id="Add" class="tabcontent">
                <form class="form-horizontal" method="POST" name="onlineexamform" id="onlineexamform" style="margin-top: 20px;min-height: 400px;">
                  <input type="hidden" name="action" id="action" value="SubmitQuestion"/>
                  <input type="hidden" name="durationexit" id="durationexit" value="00:00"/>
                  <div id="elementId" style="font-weight: bold;font-size: 18px;float: right;margin: -25px 95px 0 0; display: none;">
                      Remaining Exam Time<br>
                      <div id='clockdiv'>
                      <div>
                        <span class="minutes">00</span>
                        <div class="smalltext">Minutes</div>
                      </div>
                      <div>
                        <span class="seconds">00</span>
                        <div class="smalltext">Seconds</div>
                      </div>
                          
                      </div>
                  </div>
                  
                  <div style="margin-left: 20px;text-align: left;color: #3a4b53;min-height: 300px;" id="questionview">
                  
                  
                  
                </form> 
            </div>
        </div>
    </div>
<!--        <div class="modal-footer" style="margin-top:80px;">
      <h6>Online Exam</h6>
    </div>-->
  </div>
</div></div>
<button id="exitscreen" onclick="fullscreen()"></button>
<div id="myModalresult" class="modal" style="padding-top: 110px;">
    <div class="modal-content" style="width: 87%;">
    <div class="modal-header" style="text-align: center;">
      <span class="close closeres">×</span>
      <h6>Faculty Exam Result</h6>
    </div>
    <div class="modal-body" style="height: 420px;overflow: auto;">
        <div class="" style="background-color: white;">
            <div id="responsesexamRes"></div>
    <div class="row">  
        <div class="col-sm-12">
            <h2 class='res-header' id="panelresult" style="display:none;font-weight: bold;font-size: 19px;">Total Score: <span id="getmarks"></span> / <span id="totalmarks"></span> <span id="resmassage" style="font-size: 15px;color: tomato;font-weight: bold;"></span></h2>
            <div id="result" class="quiz-body result">
            
            </div>
        </div> <!-- End of col-sm-12 -->
        <div class="col-sm-12" style="text-align: center;">
            <span class="closeres btn btn-success">Close</span>
        </div>
    </div>
    <style>
        .left-title { color:#FFF; font-size:18px; float:left;     width: 110px;     margin: 12px 0 0 20px;}
        .right-title { width:150px; text-align:right; float:right; color:#FFF; margin: 12px 25px 0px 0px; }
        .quiz-body { margin-top:15px; padding-bottom:50px; }
        .option-block-container { margin-top:20px; max-width:420px; }
        .option-block { padding:10px; background:aliceblue; border:1px solid #84c5fe; margin-bottom:10px; cursor:pointer; }
        .result-question { font-weight:bold; }
        .c-wrong { margin-left:20px; color:#FF0000; }
        .c-correct {  margin-left:20px; color:green; }
        .last-row { border-bottom:1px solid #ccc; padding-bottom:25px; margin-bottom:25px; }
        .res-header { border-bottom:1px solid #ccc; margin-bottom:15px; padding-bottom:15px; }
    </style>
        </div>
    </div>
<!--        <div class="modal-footer" style="margin-top:80px;">
      <h6>Online Exam</h6>
    </div>-->
  </div>
</div>
<div id="divLargerImage"></div>

<div id="divOverlay"></div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

$(document).ready(function(){
    $(".closeres").click(function(){
        var modal = document.getElementById('myModalresult');
        modal.style.display = "none";
        window.location.href = "frmfacultyexam.php";
    });
});

var interav;
function startTimer(duration, display, display1) {
    var timer = duration, minutes, seconds;
    interav = setInterval(function () {
    minutes = parseInt(timer / 60, 10)
    seconds = parseInt(timer % 60, 10);
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    display.textContent = minutes;
    display1.textContent = seconds;
    if (--timer < 0) { 
        timer = duration;
        if(timer == duration){
            clearTimeout(interav);
            SubmitExamCompletionTimer();
        }
    }
    }, 1000);
}
function SubmitExamCompletionTimer() { 
    if((window.fullScreen) ||
        (window.innerWidth == screen.width && window.innerHeight == screen.height)) {
     $('#exitscreen').click();
     window.onbeforeunload = null;
     }
     window.onbeforeunload = null;
     document.onkeydown = function (e) {
        return true;
     }
     clearTimeout(interav);
    var examidt = $('#examid').val();
    var realattemptt = $('#realattemptforres').val();
    var resExamnamet = $('#resExamname').val();
    var staffcodet = $('#staffcode').val();
    var itgkcodet = $('#itgkcode').val();
    var rspcodet = $('#rspcode').val();
    var districtcodet = $('#districtcode').val();
     $('#responsepopexamAns').empty();
     $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
     $.ajax({
         type: "post",
         url: "common/cffacultyexam.php",
         data: "action=ShowLearnerResult&examid=" + examidt +"&realattempt="+ realattemptt +"&resExamname="+ resExamnamet +"&staffcode="+ staffcodet +"&itgkcode="+ itgkcodet +"&rspcode="+ rspcodet +"&districtcode="+ districtcodet +"&lat="+ lat +"&long="+ long,
         success: function (data) { 
              if(data === "Success"){
                    $('#responsepopexamAns').empty();
                    $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                    $.ajax({
                       type: "post",
                       url: "common/cffacultyexam.php",
                       data: "action=SubmitQuestionCompletion&examid=" + examidt +"&staffcode="+ staffcodet +"&realattempt="+ realattemptt,
                       success: function (data) {
                          $('#responsepopexamAns').empty();
                           if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                           {  
                               BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Exam completed successfully please wait...</span>", callback:function(result){
                               if(result) {
                                    window.location.href = "frmfacultyexam.php";
                               }}});
                           }else{
                               BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Error to complete exam.</span>", callback:function(result){
                               if(result) {
                                    window.location.href = "frmfacultyexam.php";
                               }}});
                           }
                       }
                   });
                }else{
                   BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Error to complete exam.</span>", callback:function(result){
                   if(result) {
                       // window.location.href = "frmfacultyexam.php";
                   }}});
                }
                   
         }
     });

}
$(document).ready(function () { 
    
//    function ShowFaculty() { 
//    var url = "common/cffacultyexam.php"; 
//    var data;
//    data = "action=SHOWFACULTY"; 			 
//    $.ajax({
//        type: "POST",
//        url: url,
//        data: data,
//        success: function (data)
//        { 
//            $("#facultyexamgrid").html(data);
//        }
//    });
//    }
    //ShowFaculty();
   
    $("#btn-viewcenter").click(function () {
        if ($("#startexamFormlist").valid())
          {
          $('#response').empty();
          $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
          $.ajax({ 
              url: "common/cffacultyexam.php",
              type: "POST",
              data: $("#startexamFormlist").serialize(),
              success: function(data)
                { 
                   // alert(data);
                   if(data === "norecord"){
                      $('#response').empty();
                      $('#response').html("<div class='alert-success' style='background-color: transparent;'><span><img src=images/warning.jpg width=10px /></span><span style='font-weight: bold;color: tomato;'>&nbsp; No data found.</span></div>"); 
                   }else{
                     $('#response').empty();
                     $("#CenterDetails").html(data);  
                   }
                    
                },
                  error: function(e) 
                  {
                     $("#errmsg").html(e).fadeIn();
                  } 	        
          });
          }
          return false;
    });

    $("#CenterDetails").on("click",".fun_start_exam",function(){
        var edit_id = $(this).attr("id");
        var mybtnid = "myBtn_"+edit_id;
        var staff = $(this).attr("name");
        $('.resloc'+staff).html("<img src=images/ajax-loader.gif width=22 />");
        $(this).css("display", "none");
        getLocation();
        window.setTimeout(function () {
            $('.resloc').html("");
        var modal = document.getElementById('myModaldescinst');
        var btn = document.getElementById(mybtnid);
        var span = document.getElementsByClassName("closeinst")[0];
        modal.style.display = "block";
        span.onclick = function() { 
            window.location.href = "frmfacultyexam.php";
            modal.style.display = "none";
        }
        window.onclick = function(event) {
            if (event.target == modal) {
                window.location.href = "frmfacultyexam.php";
                modal.style.display = "none";
            }
        }
        var data;
         data = "action=EXAMSINST&examid=" + edit_id +"&staff=" + staff +"&attempt=1";
        $('#instresponse').empty();
        $('#instresponse').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
        $.ajax({ 
            url: "common/cffacultyexam.php",
            type: "POST",
            data: data,
            success: function(data)
              {   //alert(data);
                if(data == 'eroorStatus'){
                    $('#instresponse').empty();
                    $('.closeinst').click();
                     BootstrapDialog.alert({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span><img src=images/warning.jpg width=10px /></span><span style='font-weight: bold;color: tomato;'>&nbsp; You have already attempted this exam</span>"});
                }
                else if(data == 'errorstatusOneDay'){ 
                    $('#instresponse').empty();
                    $('.closeinst').click();
                     BootstrapDialog.alert({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span><img src=images/warning.jpg width=10px /></span><span style='font-weight: bold;color: tomato;'>&nbsp; You have already attempted one internal exam today hence not allow to attempt another one today. Please try later.</span>"});
                }
                else{ 
                    $("#readinstruction").html(data);
                    $('#instresponse').empty();
                }            
              }	        
        });
         }, 3000);
     });
    $("#CenterDetails").on("click",".fun_start_exam_2",function(){
        var edit_id = $(this).attr("id");
        var mybtnid = "myBtn_"+edit_id;
        var staff = $(this).attr("name");
        $('.resloc_2'+staff).html("<img src=images/ajax-loader.gif width=22 />");
        $(this).css("display", "none");
        getLocation();
        window.setTimeout(function () {
        var modal = document.getElementById('myModaldescinst');
        var btn = document.getElementById(mybtnid);
        var span = document.getElementsByClassName("closeinst")[0];
        modal.style.display = "block";
        span.onclick = function() { 
            window.location.href = "frmfacultyexam.php";
            modal.style.display = "none";
        }
        window.onclick = function(event) {
            if (event.target == modal) {
                window.location.href = "frmfacultyexam.php";
                modal.style.display = "none";
            }
        }
        var data;
         data = "action=EXAMSINST&examid=" + edit_id +"&staff=" + staff +"&attempt=2";
        $('#instresponse').empty();
        $('#instresponse').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
        $.ajax({ 
            url: "common/cffacultyexam.php",
            type: "POST",
            data: data,
            success: function(data)
              {   //alert(data);
                if(data == 'eroorStatus'){
                    $('#instresponse').empty();
                    $('.closeinst').click();
                     BootstrapDialog.alert({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span><img src=images/warning.jpg width=10px /></span><span style='font-weight: bold;color: tomato;'>&nbsp; You have already attempted this exam</span>"});
                }
                else if(data == 'errorstatusOneDay'){ 
                    $('#instresponse').empty();
                    $('.closeinst').click();
                     BootstrapDialog.alert({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span><img src=images/warning.jpg width=10px /></span><span style='font-weight: bold;color: tomato;'>&nbsp; You have already attempted one internal exam today hence not allow to attempt another one today. Please try later.</span>"});
                }
                else{ 
                    $("#readinstruction").html(data);
                    $('#instresponse').empty();
                }            
              }	        
        });
        }, 3000);
     });
     
     
     
     $("#examgrid").on("click",".examperday",function(){
       BootstrapDialog.alert({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span><img src=images/warning.jpg width=10px /></span><span style='font-weight: bold;color: tomato;'>&nbsp; You have already attempted one internal exam today hence not allow to attempt another one today. Please try later.</span>"});
     });
    
    $("#readinstruction").on("click","#examidinst",function(){
        if(!$('input[type=checkbox]:checked').length) {
            alert("Agree Terms & Instructions To Start Exam.");
            return false;
        }else{
            var examds = $('#examId').val();
            var modalInst = document.getElementById('myModaldescinst');
            modalInst.style.display = "none";
            var modal = document.getElementById('myModaldesc');
            var btn = document.getElementById(examds);
            var span = document.getElementsByClassName("closeexam")[0];
            modal.style.display = "block";
            window.onbeforeunload = function(){
                return 'Are you sure you want to leave?';
              };
            document.onkeydown = function (e) {
                return false;
              } 
            $('#responsepopexamAns').empty();
            $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Exam starting please wait.....</span></div>");
            $.ajax({
                type: "post",
                url: "common/cffacultyexam.php",
                data: $("#startexamform").serialize(),
                success: function (data) {
                    $("#elementId").css({ 'display': "block" });
                    $("#questionview").html(data);
                        var posts = $('.Quepost');
                        posts.hide();
                        var customType = 'Fillter1';
                        $("#presubmit").prop('disabled', true);
                        $("#presubmit").css("cursor", "no-drop");
                        console.log(customType);
                        console.log(posts.length); 
                        posts
                            .hide()
                            .filter(function () {
                                return $(this).data('cat') === customType;
                            })
                            .show();
                    var uls = document.querySelectorAll('ul');
                    for (var j = 0; j < uls.length; j++) {
                      var ul = uls.item(j);
                      for (var i = ul.children.length; i >= 0; i--) {
                        ul.appendChild(ul.children[Math.random() * i | 0]);
                      }
                    }
                    $('#responsepopexamAns').empty();
                    var timet = $("#duration").val();
                    var fiveMinutes = 60 * timet,
                    display = document.querySelector('.minutes');
                    display1 = document.querySelector('.seconds');
                    startTimer(fiveMinutes, display, display1); 
                    
                   
                }
            });
        }    
    });  
    
    $("#questionview").on("click",".QueNumberString",function(e){
    e.preventDefault();
        var Que = $(this).attr("id");
        var QueNext = (parseInt(Que) + 1);
        var QueN = "Fillter"+QueNext;
        $('#nextubmit').data("filter",QueN);
        $('.savenextubmit').data("filter",QueN);
        
        var QuePre = (parseInt(Que) - 1);
        var QueP = "Fillter"+QuePre;
        $('#presubmit').data("filter",QueP);
        
        var Qcount = $('#disableCount').val();
        var QueC = (parseInt(Qcount) - 1);
        if(QueC==Que){ 
           $("#nextubmit").prop('disabled', true); 
           $("#nextubmit").css("cursor", "no-drop");
           $("#presubmit").prop('disabled', false);
           $("#presubmit").css("cursor", "pointer");
        }
        else if(Que=='1'){ 
           $("#presubmit").prop('disabled', true); 
           $("#presubmit").css("cursor", "no-drop");
           $("#nextubmit").prop('disabled', false);
           $("#nextubmit").css("cursor", "pointer");
        }
        else{
            $("#nextubmit").prop('disabled', false);
            $("#nextubmit").css("cursor", "pointer");
            $("#presubmit").prop('disabled', false);
            $("#presubmit").css("cursor", "pointer");
        }
        
        var posts = $('.Quepost');
        posts.hide();
            var customType = $( this ).data('filter');
            console.log(customType);
            console.log(posts.length); 
            posts
                .hide()
                .filter(function () {
                    return $(this).data('cat') === customType;
                })
                .show();
    });
    
    $("#questionview").on("click","#nextubmit",function(){
        var Que = $(this).data('filter');
        var Qstr = Que.match(/\d+/);
        var Qcount = $('#disableCount').val();
        var QueC = (parseInt(Qcount) - 1);
        if(QueC==Qstr){ 
           $("#nextubmit").prop('disabled', true); 
           $("#nextubmit").css("cursor", "no-drop");
           $("#presubmit").prop('disabled', false);
           $("#presubmit").css("cursor", "pointer");
        }
        else if(Qstr=='1'){ 
           $("#presubmit").prop('disabled', true); 
           $("#presubmit").css("cursor", "no-drop");
           $("#nextubmit").prop('disabled', false);
           $("#nextubmit").css("cursor", "pointer");
        }
        else{
            $("#nextubmit").prop('disabled', false);
            $("#nextubmit").css("cursor", "pointer");
            $("#presubmit").prop('disabled', false);
            $("#presubmit").css("cursor", "pointer");
        }
         var Qcount = $('#disableCount').val();
            var posts = $('.Quepost');
            posts.hide();
                var customType = $( this ).data('filter');
                console.log(customType);
                console.log(posts.length); 
                posts
                    .hide()
                    .filter(function () {
                        return $(this).data('cat') === customType;
                    })
                    .show();
            var QueNext = (parseInt(Qstr) + 1);
            var QueN = "Fillter"+QueNext;
            $('#nextubmit').data("filter",QueN);
            $('.savenextubmit').data("filter",QueN);
            var QuePre = (parseInt(Qstr) - 1);
            var QueP = "Fillter"+QuePre;
            $('#presubmit').data("filter",QueP);
        });
        
    $("#questionview").on("click",".savenextubmit",function(){
        var Que = $(this).data('filter');
        var Qstr = Que.match(/\d+/);
        var Qcount = $('#disableCount').val();
        var QueC = (parseInt(Qcount) - 1);
        var lastque = (Qstr - 1);
        if(QueC == Qstr){ 
           $("#nextubmit").prop('disabled', true); 
           $("#nextubmit").css("cursor", "no-drop");
           $("#presubmit").prop('disabled', false);
           $("#presubmit").css("cursor", "pointer");
           var Qcount = $('#disableCount').val();
            var posts = $('.Quepost');
            posts.hide();
                var customType = $( this ).data('filter');
                console.log(customType);
                console.log(posts.length); 
                posts
                    .hide()
                    .filter(function () {
                        return $(this).data('cat') === customType;
                    })
                    .show();
            var QueNext = (parseInt(Qstr) + 1);
            var QueN = "Fillter"+QueNext;
            $('.savenextubmit').data("filter",QueN);
            $('#nextubmit').data("filter",QueN);
            var QuePre = (parseInt(Qstr) - 1);
            var QueP = "Fillter"+QuePre;
            $('#presubmit').data("filter",QueP);
        }
        else if(Qstr=='1'){
           $("#presubmit").prop('disabled', true); 
           $("#presubmit").css("cursor", "no-drop");
           $("#nextubmit").prop('disabled', false);
           $("#nextubmit").css("cursor", "pointer");
           var Qcount = $('#disableCount').val();
            var posts = $('.Quepost');
            posts.hide();
                var customType = $( this ).data('filter');
                console.log(customType);
                console.log(posts.length); 
                posts
                    .hide()
                    .filter(function () {
                        return $(this).data('cat') === customType;
                    })
                    .show();
            var QueNext = (parseInt(Qstr) + 1);
            var QueN = "Fillter"+QueNext;
            $('.savenextubmit').data("filter",QueN);
            $('#nextubmit').data("filter",QueN);
            var QuePre = (parseInt(Qstr) - 1);
            var QueP = "Fillter"+QuePre;
            $('#presubmit').data("filter",QueP);
        }
        else if(QueC == lastque){
            
        }
        else{ 
            $(".savenextubmit").prop('disabled', false);
            $(".savenextubmit").css("cursor", "pointer");
            $("#presubmit").prop('disabled', false);
            $("#presubmit").css("cursor", "pointer");
            var Qcount = $('#disableCount').val();
            var posts = $('.Quepost');
            posts.hide();
                var customType = $( this ).data('filter');
                console.log(customType);
                console.log(posts.length); 
                posts
                    .hide()
                    .filter(function () {
                        return $(this).data('cat') === customType;
                    })
                    .show();
            var QueNext = (parseInt(Qstr) + 1);
            var QueN = "Fillter"+QueNext;
            $('.savenextubmit').data("filter",QueN);
            $('#nextubmit').data("filter",QueN);
            var QuePre = (parseInt(Qstr) - 1);
            var QueP = "Fillter"+QuePre;
            $('#presubmit').data("filter",QueP);
        }
         
        });
        
    $("#questionview").on("click","#presubmit",function(){
        var Que = $(this).data('filter');
        var Qstr = Que.match(/\d+/);
        var QueNext = (parseInt(Qstr) + 1);
        var QueN = "Fillter"+QueNext;
        $('#nextubmit').data("filter",QueN);
        $('.savenextubmit').data("filter",QueN);
        var Qcount = $('#disableCount').val();
        var QueC = (parseInt(Qcount) - 1);
            if(QueC==Qstr){ 
                $("#nextubmit").prop('disabled', true); 
                $("#nextubmit").css("cursor", "no-drop");
                $("#presubmit").prop('disabled', false);
                $("#presubmit").css("cursor", "pointer");
             }
             else if(Qstr=='1'){ 
                $("#presubmit").prop('disabled', true);
                $("#presubmit").css("cursor", "no-drop");
                $("#nextubmit").prop('disabled', false);
                $("#nextubmit").css("cursor", "pointer");
             }
             else{
                 $("#nextubmit").prop('disabled', false);
                 $("#nextubmit").css("cursor", "pointer");
                 $("#presubmit").prop('disabled', false);
                 $("#presubmit").css("cursor", "pointer");
             }
            var posts = $('.Quepost');
            posts.hide();
                var customType = $( this ).data('filter');
                console.log(customType);
                console.log(posts.length); 
                posts
                    .hide()
                    .filter(function () {
                        return $(this).data('cat') === customType;
                    })
                    .show();
            
            var QuePre = (parseInt(Qstr) - 1);
            var QueP = "Fillter"+QuePre;
            $('#presubmit').data("filter",QueP);
        });
        
    $("#questionview").on("click",".examonlinesubmit",function(){
        var queid = $(this).attr("id");
        var tval =  $('.minutes').text();
        var tval1 =  $('.seconds').text();
        var res2 = tval+"."+tval1;
        $('#durationexit').val(res2);
        SubmitQuestionAns(queid);
     });
     $("#questionview").on("click","#completeexamsubmit",function(){
        BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Once submitted you will not be able to allow to take this test again. Please make sure you have completed your test.</span>", callback:function(result){
            if(result) {
                BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Are you sure you want to submit this test?</span>", callback:function(result){
                    if(result) {
                        if((window.fullScreen) ||
                            (window.innerWidth == screen.width && window.innerHeight == screen.height)) {
                         $('#exitscreen').click();
                         window.onbeforeunload = null;
                         }
                         window.onbeforeunload = null;
                         document.onkeydown = function (e) {
                            return true;
                         }
                         clearTimeout(interav);
                         function SubmitExamCompletion() {
                            var examid = $('#examid').val();
                            var realattempt = $('#realattemptforres').val();
                            var resExamname = $('#resExamname').val();
                            var staffcode = $('#staffcode').val();
                            var itgkcode = $('#itgkcode').val();
                            var rspcode = $('#rspcode').val();
                            var districtcode = $('#districtcode').val();
                            $('#responsepopexamAns').empty();
                            $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                            $('#responsepopexamAns').empty();
                            //window.location.href = "frmassessmentexam.php";
//                             var modal = document.getElementById('myModalresult');
//                             var span = document.getElementsByClassName("closeres")[0];
//                             modal.style.display = "block";
//                             span.onclick = function() { 
//                                 modal.style.display = "none";
//                                 window.location.href = "frmfacultyexam.php";
//                             }
//                             window.onclick = function(event) {
//                                 if (event.target == modal) {
//                                     modal.style.display = "none";
//                                     window.location.href = "frmfacultyexam.php";
//                                 }
//                             }
//
//                             var modalExam = document.getElementById('myModaldesc');
//                             modalExam.style.display = "none";
                             
                             $('#responsepopexamAns').empty();
                             $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                             
                             $.ajax({
                                 type: "post",
                                 url: "common/cffacultyexam.php",
                                 data: "action=ShowLearnerResult&examid=" + examid +"&realattempt="+ realattempt +"&resExamname="+ resExamname +"&staffcode="+staffcode+"&itgkcode="+itgkcode+"&rspcode="+rspcode+"&districtcode="+districtcode+"&lat="+lat+"&long="+long+"",
                                 success: function (data) { 
                                     $('#responsesexamRes').empty();
                                     //$('#result').html(data);
                                     $('#responsepopexamAns').empty();
                                     $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                                     if(data === "Success"){
                                         $('#responsepopexamAns').empty();
                                         $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                                     
                                         $.ajax({
                                            type: "post",
                                            url: "common/cffacultyexam.php",
                                            data: "action=SubmitQuestionCompletion&examid=" + examid +"&staffcode="+staffcode+"&realattempt="+ realattempt,
                                            success: function (data) {
                                               // alert(data);
                                               $('#responsepopexamAns').empty();
                                                if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                                {  
                                                    BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Exam completed successfully please wait...</span>", callback:function(result){
                                                    if(result) {
                                                         window.location.href = "frmfacultyexam.php";
                                                    }}});
                                                }else{
                                                    BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Error to complete exam.</span>", callback:function(result){
                                                    if(result) {
                                                         window.location.href = "frmfacultyexam.php";
                                                    }}});
                                                }
                                            }
                                        });
                                     }else{
                                        BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>Error to complete exam.</span>", callback:function(result){
                                        if(result) {
                                            // window.location.href = "frmfacultyexam.php";
                                        }}});
                                     }
                                     
                                 }
                             });

                        }
                        SubmitExamCompletion();
                    }else {
                        //return false;
                    }
                }});
            }else {
               // dialog.close();
               // return false;
            }
        }});
          
     });
    function SubmitQuestionAns(queid) {
        $('#responsepopexamAns').empty();
        $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
        $.ajax({
            type: "post",
            url: "common/cffacultyexam.php",
            data: $('#onlineexamform').serialize() + "&queid=" + queid,
            success: function (data) { 
                if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                {
                    $('#responsepopexamAns').empty();
                    $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Answer saved successfully</span></div>");
                    window.setTimeout(function () {
                      $('#responsepopexamAns').empty();
                    }, 1000);
                    var grqid = "#Green_"+queid;
                     $(grqid).each(function () {
                        this.style.setProperty('background-color', '#45ca45', 'important');
                    });
                    
                }
                else{
                    $('#responsepopexamAns').empty();
                    $('#responsepopexamAns').append("<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>Error to save answer please try again</span></div>");
                    window.setTimeout(function () {
                      $('#responsepopexamAns').empty();
                    }, 5000);
                }
            }
        });
    }
    
$("#questionview").on("click","p img",function(){ 
    var $img = $(this);
    $('#divLargerImage').html($img.clone()).add($('#divOverlay')).fadeIn();
});

$('#divLargerImage').add($('#divOverlay')).click(function () {
    $('#divLargerImage').add($('#divOverlay')).fadeOut(function () {
        $('#divLargerImage').empty();
    });
});
    
});
</script>
<!-- Location capture script -->
<script>
function getLocation() {
    var run = 0;
    navigator.geolocation.watchPosition(function(position) {
         lat = position.coords.latitude; 
         long = position.coords.longitude;
        run++;
        if(run == 1){
            lat = lat;
            long = long;
            startmode = 1;
        }
    },
    function(error) {
        if (error.code == error.PERMISSION_DENIED)
        BootstrapDialog.confirm({title: "Alert", message:"<div class='alert-success' style='background-color: transparent;'><span></span><span style='font-weight: bold;color: tomato;'>To start exam allow location.</span>", callback:function(result){
        if(result) {
             window.location.href = "frmfacultyexam.php";
        }else{
             window.location.href = "frmfacultyexam.php";
        }
        }});
    });
}
</script>
<script type="text/javascript">
function fullscreen() { 
    var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
        (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
        (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
        (document.msFullscreenElement && document.msFullscreenElement !== null);

    var docElm = document.documentElement;
    if (!isInFullScreen) { 
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        } else if (docElm.msRequestFullscreen) {
            docElm.msRequestFullscreen();
        }
    } else { 
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}
</script>
<script type="text/javascript" src="bootcss/js/jquery.validate.min.js"></script>
<script>
$("#startexamFormlist").validate({
    rules: {
      startexamName: "required",
      examAttemptLearner: "required",
      
    },
    messages: {
      startexamName: "Please Select Assessment",
      examAttemptLearner: "Please Select Attempt",
    },  
});
</script>
</html>
<?php
//} else { ?>
    <script>
       // window.location.href = "frmstartexam.php";
    </script>
    <?php
//}
?>