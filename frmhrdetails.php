<?php
$title="HR DETAILS";
include ('header.php'); 
include ('root_menu.php'); 
    if (isset($_REQUEST['code'])) {
                echo "<script>var HRCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var HRCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
        <div class="container"> 
			

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">HR Details
				 
				</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmHRDetail" id="frmHRDetail" class="form-inline" action=""> 
					
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
							
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Designation:<span class="star">*</span></label>
                                <select id="ddlDesignation" name="ddlDesignation" class="form-control" >
								 
                                </select>    
                            </div>
							
							
							 <div class="col-sm-4 form-group">     
                                <label for="address">Full Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="25" name="txtName" 
									id="txtName" placeholder=" Full Name" style="text-transform:uppercase"  onkeypress="javascript:return validAddress(event);" >
                            </div>
							
							
							<div class="col-sm-4 form-group" style="margin-top:30px"> 
                                <label for="edistrict">Gender:<span class="star">*</span></label>
                                <input type="radio" name="usergender" value="yes" id="usergender1" checked="checked">Male</input> &nbsp;&nbsp;&nbsp;&nbsp; 
                            <input type="radio" name="usergender" value="no" id="usergender1">Female</input>
				
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="address">Date of Birth:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtDob" id="txtDob" placeholder="DD-MM-YY" readonly='true'>    
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Mobile No.<span class="star">*</span></label>
                                <input type="text" class="form-control" id="txtMobile" name="txtMobile" placeholder="Mobile No." onkeypress="javascript:return allownumbers(event);" maxlength="10">

                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Email ID:<span class="star">*</span></label>
                                <input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="Email ID."  >

                            </div>
							
							
							
							
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address"> Max. Qualification :<span class="star">*</span></label>
                               <select id="ddlQualification1" name="ddlQualification1" class="form-control" >
								 
                                </select>
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address"> Total Work Experience  :<span class="star">*</span></label>
                             <input type="text" class="form-control" id="txtExperience" name="txtExperience" placeholder="Total Work Experience" onkeypress="javascript:return allownumbers(event);" maxlength="2">
                            </div>
							
							
							
                            
						</div>
						
						
						<div class="container">
						<div class="col-sm-4">     
                                <label for="address"> Certification Details :<span class="star">*</span></label>
                            <textarea class="form-control" rows="4" id="txtQualification2" name="txtQualification2" placeholder="Certification Details" ></textarea>
							  
                            </div>
							
							
							<div class="col-sm-4 ">     
                                <label for="address"> Other Qualifications :<span class="star">*</span></label>
								 <textarea class="form-control" rows="4" id="txtQualification3" name="txtQualification3" placeholder="Other Qualifications" ></textarea>
                             
                            </div>
						</div>

						<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-top:10px;margin-left:10px"/>    
                        </div>
						
						
						<div id="gird"></div>
                </div>
            </div>   
        </div>
    </form>

</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox
{	color:#F00;	 } 
</style>

<script type="text/javascript"> 
 $('#txtDob').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	});  
	
	
	
	</script>
<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>




<script>
$('#txtConfirmaccountNumber').on('keyup', function () {
    if ($(this).val() == $('#txtaccountNumber').val()) {
        $('#message').html('confirmed').css('color', 'green');
    } else $('#message').html('confirm account no. should be same as account no').css('color', 'red');
});
</script>

<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>
 <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
        
		
		
		
		if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
			
			function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfHRDetail.php",
                    data: "action=DELETE&values=" + HRCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmHRDetails.php";
                           }, 1000);
                            
                            Mode="Add";
                            resetForm("frmHRDetail");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }
			
			
			
			
			function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfHRDetail.php",
                    data: "action=EDIT&values=" + HRCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Make']);
                        //alert(data);
                        data = $.parseJSON(data);
                        ddlDesignation.value = data[0].Staff_Designation;
                        txtName.value = data[0].Staff_Name;
                        txtDob.value = data[0].Staff_Dob;
                        usergender1.value = data[0].staff_Gender;
                        txtMobile.value = data[0].Staff_Mobile;
						 txtExperience.value = data[0].Staff_Experience;
						 txtEmail.value = data[0].Staff_Email_Id;
						 txtQualification2.value = data[0].Staff_Qualification2;
						 txtQualification3.value = data[0].Staff_Qualification3;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfHRDetail.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }

            showData();
			
			
			
          

            function FillDesignation() {
                $.ajax({
                    type: "post",
                    url: "common/cfDesignationMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlDesignation").html(data);
                    }
                });
            }

            FillDesignation();

            function FillQyalification() {
                $.ajax({
                    type: "post",
                    url: "common/cfQualificationMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlQualification1").html(data);
                    }
                });
            }
            FillQyalification();


            $("#btnSubmit").click(function () {
				
				if ($("#frmHRDetail").valid())
                {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfHRDetail.php"; // the script where you handle the form input.

                if (document.getElementById('usergender1').checked) //for radio button
                {
                    var gender_type = 'Male';
                }
                else {
                    gender_type = 'Female';
                }
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name="+txtName.value+"&designation="+ddlDesignation.value+
                            "&gender="+gender_type+"&dob="+txtDob.value+
                            "&mobile="+txtMobile.value+"&email="+txtEmail.value+
                            "&qualification1="+ddlQualification1.value+"&experience="+txtExperience.value+
                            "&qualification2="+txtQualification2.value+"&qualification3="+txtQualification3.value+ ""; // serializes the form's elements.
                    // alert(data);
                }
                else
                {
                    data = "action=UPDATE&code=" + HRCode +
                            "&name="+txtName.value+"&designation="+ddlDesignation.value+
                            "&gender="+gender_type+"&dob="+txtDob.value+
                            "&mobile="+txtMobile.value+"&email="+txtEmail.value+
                            "&qualification1="+ddlQualification1.value+"&experience="+txtExperience.value+
                            "&qualification2="+txtQualification2.value+"&qualification3="+txtQualification3.value+ ""; // serializes the form's elements.
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmhrdetails.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("frmHRDetail");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
				}
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
	
	<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmhrvalidation.js" type="text/javascript"></script>
</html>