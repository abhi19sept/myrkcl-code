<?php
$title = "WCD Visit Feedback Report";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> WCD Visit Feedback Report

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmNcrReport" id="frmNcrReport" class="form-inline" action=""> 

                    <br>

                    <div id="gird"></div>
                </form>

            </div>

        </div>   
    </div>

</div>
<div class="modal" id="myModal">
    <div class="modal-content">
        <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal">×</button>
            <h3 id="heading-tittle" class="modal-title">Heading</h3>
        </div>
        <div class="modal-body">
            <div class="container">
            <div id="responses"></div>

            </div>        
            <div id="errorBox"></div>

             <div id="grid2" style="margin-top:5px; width:94%;"> </div>
        </div>
        </div>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
            var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                    $(document).ready(function () {


                        function showData() {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                            $.ajax({
                                type: "post",                 url: "common/cfWcdVisitFeedbackReport.php",
                                data: "action=DETAILS",
                                success: function (data) {
                                    $('#response').empty();
                                    $("#gird").html(data);
                                    $('#example').DataTable({
                                        dom: 'Bfrtip',
                                        buttons: [
                                        'copy', 'csv', 'excel', 'pdf', 'print'                         ]
                                    });



                                }
                            });
                        }

                        showData();
                        
                    $("#gird").on('click', '.updcount',function(){
                    var itgkcode = $(this).attr('itgkcode');
                    var learnercode = $(this).attr('learnercode');

                    
                    var modal = document.getElementById('myModal');
                    var span = document.getElementsByClassName("close")[0];
                    modal.style.display = "block";
                    span.onclick = function() {
                       
                        modal.style.display = "none";                       
                    }

                    $("#heading-tittle").html('Learners Details');
                    $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
              
            var url = "common/cfWcdVisitFeedbackReport.php"; // the script where you handle the form input.
            var data;
            
            data = "action=GETLEARNERLIST&itgkcode=" + itgkcode + "&learnercode=" + learnercode + "&rolecode=" + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                   
                    $('#response').empty();
                    $("#grid2").html(data);
                    $('#example2').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
                });

                        function resetForm(formid) {
                            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                        }

    });
                    
</script>
</html>