<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of frm Rkcl Apis
 *
 *  Author Name:  Sunil Kumar Baindara
 */
$title="RKCl Apis Clients Functions Details";
include ('header.php'); 
include ('root_menu.php'); 
//include './rkcltheme/include/menu.php';
		if (isset($_REQUEST['clientapiID'])) {
			echo "<script>var ClientApiId=" . $_REQUEST['clientapiID'] . "</script>";
                        if (isset($_REQUEST['Mode'])){
                            echo "<script>var Mode='".$_REQUEST['Mode']."'</script>";
                            echo "<script>var delId=" . $_REQUEST['code'] . "</script>";
                        }else{
                            echo "<script>var Mode='Add'</script>";
                        }
			
		} else {
			echo "<script>var ClientApiId=0</script>";
			echo "<script>var Mode='Add'</script>";
		}
if ($_SESSION['User_Code'] == '1') {		
		?>		
<style type='text/css'>
.selectmulti {
    width:200px;
    height:50px;
    overflow:auto
}
</style>			
        <div class="container"> 			  

            <div class="panel panel-primary" style="margin-top:36px;">

                <div class="panel-heading">RKCl Apis Function Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmRootMenu" id="frmRootMenu" class="form-inline" role="form" action="">     
                      <div id="addFrm" style="display: none;">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>
                        </div>  
                        
                        <div class="container">

                            <div class="col-sm-10 form-group"> 
                                <label for="status">Table Name:</label>
                                <select id="ddlFunctionName" name="ddlFunctionName" class="form-control">
                                  <option value="">Please Select</option>          
                                </select>
                            </div>                            
                        </div>
                        <div class="container">

                            <div class="col-sm-10 form-group"> 
                                <label for="status">Field Name:</label>
                                <select id="ddlFtable" name="ddlFtable" class="form-control selectmulti" multiple>
                                  <option value="">Please Select</option>          
                                </select>
                            </div>
                            
                            <div class="col-sm-10 form-group"> 
                                <label for="status">Selected Field Name:</label>
                                <select id="ddlFtable2" name="ddlFtable2" class="form-control selectmulti" multiple>
<!--                                  <option value="">Please Select</option>          -->
                                </select>
                            </div>                              
                        </div>

                        <div class="container">
                            
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" onclick="add_FunDetail();" value="Submit"/> 
                            <input type="button" name="btnCancle" id="btnCancle" class="btn btn-primary" onclick="cancleadd(<?php echo $_REQUEST['clientapiID'];?>);" value="Cancel"/> 
                        </div>
                            
                        </div>
                        
                        
                        <div class="container"  id="btnShow">
                            <input type="button" name="btnShowFrm" id="btnShowFrm"  class="btn btn-primary" value="Add Functions For Clients "/> 
                            <input type="button" name="btnCancle" id="btnCancle" class="btn btn-primary" onclick="cancleback();" value="Back"/> 
                        </div>
						
			<div id="gird" style="margin-top:35px;"> </div>		
						
                </div>
            </div>   
        </div>
    </form>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
</body>
                <script type="text/javascript">
                    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                    $(document).ready(function () {
//alert(Mode);
                        if (Mode == 'Delete')
                        {
                            if (confirm("Do You Want To Delete This Item ?"))
                            {
                                deleteRecord();
                            }
                        }
                        else if (Mode == 'Edit')
                        {
                            $("#gird").hide();
                            $("#btnShow").hide();
                            $("#addFrm").show();                
                            fillForm();
                        }

                       
                        /* get all function name in dropdown boxes*/
                        
                        function FillFname() {
                            $.ajax({
                                type: "post",
                                url: "common/cfRkclApiFDetail.php",
                                data: "action=FILLFNAME",
                                success: function (data) {
                                    $("#ddlFunctionName").html(data);
                                }
                            });
                        }

                        FillFname();
                        
                       
                        /* get all function name in dropdown boxes*/



                        function deleteRecord()
                        { //alert('hello');
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                            $.ajax({
                                type: "post",
                                url: "common/cfRkclApiFDetail.php",
                                data: "action=DELETE&values=" + delId + "",
                                success: function (data) {
                                    //alert(data);
                                    if (data == SuccessfullyDelete)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                        window.setTimeout(function () {
                                            window.location.href = "frmRkclApiFDetail.php?clientapiID="+ClientApiId;;
                                        }, 1000);
                                        Mode = "Add";
                                        //resetForm("frmRootMenu");
                                    }
                                    else
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                    }
                                    showData();
                                }
                            });
                        }


                        function fillForm()
                        {
							//alert("HII");
                            $.ajax({
                                type: "post",
                                url: "common/cfRkclApiFDetail.php",
                                data: "action=EDIT&values=" + ClientApiId + "&code=" + delId + "",
                                success: function (data) {
									alert(data);
                                    //alert($.parseJSON(data)[0]['Status']);
                                    data = $.parseJSON(data);
                                    ddlFunctionName.value = data[0].Functionname;
                                    //ddlFtable2.value = data[0].Functionfileds;
                                    //ddlStatus.value = data[0].Status;
                                }
                            });
                        }

                        function showData() {

                            $.ajax({
                                type: "post",
                                url: "common/cfRkclApiFDetail.php",
                                data: 'action=SHOW&ClientApiId='+ClientApiId,
                                success: function (data) {
                                    //alert(data);
                                    $("#gird").html(data);

                                }
                            });
                        }
							
                        showData();
						
                    $("#btnShowFrm").click(function () {

                            $("#gird").hide();
                            $("#btnShow").hide();
                            $("#addFrm").show();
                        });
                        
                      
                        
                    function resetForm(formid) {
                            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                        }

                    });
                    function cancleadd(clientapiID)
                    {
                        window.location.href = "frmRkclApiFDetail.php?clientapiID="+ClientApiId;
                    }
                    function cancleback()
                    {
                        window.location.href = "frmRkclApi.php";
                    }
                    function add_FunDetail()
                        {
                            
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                        var strtxtFtable = "";
                        $( "#ddlFtable2 option:selected" ).each(function() {
                              strtxtFtable += $( this ).text() + ", ";
                            });
                       // alert("You have selected the country - " + strtxtFtable);
                        
                          $.ajax({
                                  url: "common/cfRkclApiFDetail.php",
                                  type: "POST",
                                  data:'action=ADD&ClientApiId='+ClientApiId+'&txtFunctionName='+ddlFunctionName.value+'&txtddlFtable2='+strtxtFtable,
                                  success: function(data){
                                  //alert(data);   
                                  if (data == SuccessfullyInsert)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                        window.setTimeout(function () {
                                            window.location.href = "frmRkclApiFDetail.php?clientapiID="+ClientApiId;
                                        }, 1000);

                                    }
                                    else
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                    }
                                    showData();

                                }        
                          });


                        }
   
                   

                    $("#ddlFunctionName").change(function() {
                        //get the selected value
                        var selectedValue = this.value;
                        //alert(selectedValue);
                        //make the ajax call
                        $.ajax({
                            url: "common/cfRkclApiFDetail.php",
                            type: 'POST',
                            data: "action=FILLTBFIELDNAME&ftablename="+selectedValue,
                            success: function(data) {
                                //console.log("Data sent!");
                                //alert(data);
                                $("#ddlFtable").html(data);
                            }
                        });
                    });
                    /* for add to left select box values*/
                    $(window).load(function(){
        
                    $('#ddlFtable').click(function(){
                        $('#ddlFtable option:selected').appendTo('#ddlFtable2');
                    });

                    $('#ddlFtable2').click(function(){
                        $('#ddlFtable2 option:selected').appendTo('#ddlFtable');
                    });

                    

                });
                    /* for add to left select box values*/

                </script>
                </html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>