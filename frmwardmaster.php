<?php
$title = "Government Entry Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<div class="container"> 


    <div class="panel panel-primary" style="margin-top:36px !important;">

        <div class="panel-heading">Ward Master</div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmWardMaster" id="frmWardMaster" class="form-inline" role="form" >     

                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Ward Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtWardName" id="txtWardName" placeholder="Ward Name">
                    </div>



                </div> 

                <div class="container">


                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">District Name:</label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control" >

                        </select>    
                    </div>


                </div>
				
				<div class="container">


                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Municipality Name:</label>
                        <select id="ddlMunicipality" name="ddlMunicipality" class="form-control" >

                        </select>    
                    </div>


                </div>



                <div class="container">


                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Ward  Status:</label>
                        <select id="ddlStatus" name="ddlStatus" class="form-control" >

                        </select>    
                    </div>
                </div>

                <div class="container">

<div class="col-sm-4 form-group"> 
						<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/> 
                        <input type="button" name="btnSearch" id="btnSearch" class="btn btn-primary" value="Search"/>
                        </div>					
                </div>
				
        </div>
        <div id="gird"></div>
    </div>   
</div>
</form>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }

</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
		$("#btnSearch").click(function () {
            searchData();
        });

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        function FillStatus() {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlStatus").html(data);
                }
            });
        }

        FillStatus();
		
		function FillMunicipal(districtId) {
            $.ajax({
                type: "post",
                url: "common/cfMunicipalMaster.php",
                data: "action=FILL&values=" + districtId,
                success: function (data) {
                    $("#ddlMunicipality").html(data);
                }
            });
        }

        $("#ddlDistrict").change(function () {
            FillMunicipal(this.value);
        });

        function FillDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }

        FillDistrict();


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfWardMaster.php",
                data: "action=DELETE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmWardmaster.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmWardMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    searchData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfWardMaster.php",
                data: "action=EDIT&values=" + Code + "",
                success: function (data) {

                    //alert($.parseJSON(data)[0]['Status']);
                    //alert(data);
                    data = $.parseJSON(data);
                    txtWardName.value = data[0].WardName;
                    ddlMunicipality.value = data[0].District;
                    ddlStatus.value = data[0].Status;

                }
            });
        }

        function searchData() {
            $('#response').empty();
			$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			var url = "common/cfWardMaster.php"; // the script where you handle the form input.
			var data = "action=Search&name=" + $('#txtWardName').val() + "&districtId=" + $('#ddlDistrict').val() + "&parent=" + $('#ddlMunicipality').val() + "&status=" + $('#ddlStatus').val() + ""; // serializes the form's elements.
//			alert(data); return;
			$.ajax({
				type: "post",
				url: url,
				data: data,
				success: function (data) {

					$("#gird").html(data);
					$('#example').DataTable({
						dom: 'Bfrtip',
						buttons: [
							'copy', 'csv', 'excel', 'pdf', 'print'
						]
					});
					$('#response').empty();
				}
			});
        }

        $("#btnSubmit").click(function () {

            if ($("#frmWardMaster").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfWardMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtWardName.value
                            + "&Municipality_Code=" + ddlMunicipality.value
                            + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + Code
                            + "&name=" + txtWardName.value
                            + "&Municipality_Code=" + ddlMunicipality.value
                            + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);

                            Mode = "Add";
                            resetForm("frmWardmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        searchData();
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmWardMastervalidation.js"></script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>