<?php
    $title = "Update Learner Email";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';

    echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";


    if ($_SESSION["User_UserRoll"] <> 19) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }

?>

<style>

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #575c5f;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -16px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    #errorBox {
        color: #F00;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover,
    textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .hidden-xs {
        display: inline-block !important;
    }

    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
        color: #468847;
        background-color: #DFF0D8;
        border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
        color: #B94A48;
        background-color: #F2DEDE;
        border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
        margin: 2px 0 3px;
        padding: 0;
        list-style-type: none;
        font-size: 0.9em;
        line-height: 0.9em;
        opacity: 0;
        color: tomato !important;

        transition: all .3s ease-in;
        -o-transition: all .3s ease-in;
        -moz-transition: all .3s ease-in;
        -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
        opacity: 1;
    }

    .parsley-required {
        color: tomato;
        font-family: Calibri;
        margin-top: 4px;
        font-size: 15px;
    }

    select[disabled] {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
    }
</style>

<script src="bootcss/js/parsley.min.js"></script>

<div class="container" id="showdata" style="display: none;">


    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Learner Details</div>
        <div class="panel-body">

            <div style="padding-bottom: 5px;font-size: 15px;color: #575c5f;">
                Fields marked with <span class="asterisk">*</span> are mandatory
            </div>

            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateLearnerDetails" name="updateLearnerDetails">

                <div class="division_heading">
                    Learner Details
                </div>

                <div class="alert alert-success text-center" role="alert" id="success_email" style="display: none;"></div>

                <div class="alert alert-danger text-center" role="alert" id="error_email" style="display: none;"></div>

                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Learner's Email ID</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" name="Admission_Email" id="Admission_Email" placeholder="Please enter your Email ID" data-parsley-required="true" data-parsley-type="email" data-parsley-trigger="change">
                        </div>
                    </div>
                </div>

                <input type="hidden" name="action" value="updateEmailLearner" id="action">
                <!-- /.box-body -->
                <div class="box-footer extra-footer-class">
                    <button type="submit" class="btn btn-lg btn-danger">Submit</button>
                </div>
                <!-- /.box-footer -->
            </form>

        </div>
    </div>
</div>


</body>
<?php
    include 'common/message.php';
    include 'footer.php';
?>

<script src="bootcss/js/frmupdateEmailLearner.js"></script>

</html>