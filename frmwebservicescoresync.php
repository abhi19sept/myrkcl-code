<?php  ob_start(); 
$title="Webservice";
include ('header.php'); 
include ('root_menu.php'); 
if($_SESSION['User_UserRoll']!=1 && $_SESSION['User_UserRoll']!=8){
    session_destroy();
    header('Location: index.php');
    exit;
}

?>
 

<link rel="stylesheet" href="css/profile_style.css">
<link rel="stylesheet" href="css/webservice.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
	
             <div class="panel panel-primary" style="margin-top:20px !important;">

                <div class="panel-heading">Sync Webservice For Learner Score</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
                            <div id="errorBox"></div>

                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 

                        
							
                            <div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow"  class="btn btn-primary" value="Sync Table" style=" display: none; margin-top:25px"/>    
                            </div>
			</div>
						
                        <!-- Widgets /* Show Table Count Detail*/-->
                        <div id ="tatalcount"  class="row clearfix" style="padding-top: 20px;padding-left: 27px;">
                             <div class="container">
                                <div id="responsecount"></div>
                            </div>
                        <div id="CLICKDiv"></div>
                        
                        <div id="MYRKCLDiv" style="width: 100%;float: left;"></div>
                        
                        

                        </div>
                        <!-- #END# Widgets -->
                   
                </div>
            </div> 
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        
       function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);

                }
            });

        });
        
        $("#ddlBatch").change(function () {
            $('#responsecount').show();
            $('#responsecount').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Please wait, While we are fetching the records......</span></p>");		
             
            var ddlCourse = $("#ddlCourse").val();
            var ddlBatch = $(this).val();
            //alert(ddlCourse);
            //alert(ddlBatch);
            $.ajax({
                
                type: "post",
                url: "common/cfwebservicescoresync.php",
                data: "action=getTotalByBatchCourse&ddlCourse=" + ddlCourse + "&ddlBatch="+ ddlBatch,
                success: function (data) {
                   $('#responsecount').empty();
                   $("#MYRKCLDiv").html(data);
                   $("#btnShow").show();

                }
            });

        }); 
       

        function FillTableName() {
            /* Show Table Count Detail*/
            $('#responsecount').show();
            $('#responsecount').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Please wait, While we are fetching the records......</span></p>");		
                $.ajax({
                        type: "post",
                        url: "common/cfwebservicescoresync.php",
                        data: "action=COUNTTABLE",
                        success: function (data){
                            $('#responsecount').empty();
                            $("#CLICKDiv").html(data);
                            
                        }
                 });
           /* Show Table Count Detail*/
        }
        //FillTableName();
	
        

	$("#btnShow").click(function () {//alert("hello");
              
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Please wait, While we are fetching the records......</span></p>");		
		$.ajax({
			type: "post",
                        url: "common/cfwebservicescoresync.php",
			data: "action=SINKTABLE&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "",
			success: function (data)
				{
				//alert(data);
				$('#response').empty();
                                if(data == 'DONE')
                                    {
                                      $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + "   Sucessfullly Inserted and Updated Also." + "</span></p>");
                                      //$("#iLEARNDiv").html(data);
                                      setTimeout(function () {
                                   window.location.href = "frmwebservicescoresync.php";
                                },3000)
                                                                         
                                    }
                               else if(data == 'NONEWRF')
                                    {
                                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   No New Record Found." + "</span></p>");
                                    }
                                else if(data == 'CYHPDNM')
                                    {
                                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Credentials You have provided, Does not matched." + "</span></p>");
                                    }
                                else if(data == 'APIERR')
                                    {
                                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   It Seems Some Api Error." + "</span></p>");
                                    }
                                else if(data == 'COUBAT')
                                    {
                                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please select Course and Batch Both." + "</span></p>");
                                    }
                                else{
                                    
                                    $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + "   Sucessfullly Inserted and Updated Also." + "</span></p>");
                                    $("#iLEARNDiv").html(data);
                                    setTimeout(function () {
                                    window.location.href = "frmwebservicescoresync.php";
                                    },5000)
                                    }
                                }
                });	
                
          
             //}	
          });
		  
	        
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>


</html>