<!DOCTYPE html>

<html lang="en" class="no-js">
	<head>
		
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>MyRKCL Web Portal</title>
		<meta name="description" content="Blueprint: Horizontal Drop-Down Menu" />
		<meta name="keywords" content="horizontal menu, microsoft menu, drop-down menu, mega menu, javascript, jquery, simple menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" href="bootcss/css/bootstrap.min.css">	
                <script src="js/jquery.min.js"></script>
                <script src="bootcss/js/bootstrap.min.js"></script>	
		<link rel="stylesheet" href="assets/header-search.css">
		<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
		<link href="/assets/font-awesome.min.css" rel="stylesheet">
                <link rel="stylesheet" href="css/style.css">
                
                <!--For all grid export and paging operations-->
                <script src="bootcss/js/bootstrap-dialog.js"></script>
                <script src="bootcss/js/bootstrap-dialog.min.js"></script>
                <script src="bootcss/js/jquery.dataTables.min.js"></script>	
                <script src="bootcss/js/dataTables.bootstrap.min.js"></script>			
                <link rel="stylesheet" href="bootcss/css/dataTables.bootstrap.min.css">	
                <script src="bootcss/js/dataTables.buttons.min.js"></script>
                <script src="bootcss/js/buttons.flash.min.js"></script>
                <script src="bootcss/js/jszip.min.js"></script>
                <script src="bootcss/js/pdfmake.min.js"></script>
                <script src="bootcss/js/vfs_fonts.js"></script>
                <script src="bootcss/js/buttons.html5.min.js"></script>
                <script src="bootcss/js/buttons.print.min.js"></script>
                <link rel="stylesheet" href="bootcss/css/jquery.dataTables.min.css">	
                <link rel="stylesheet" href="bootcss/css/buttons.dataTables.min.css">
                <link rel="stylesheet" href="bootcss/css/bootstrap-dialog.css">	
                <link rel="stylesheet" href="bootcss/css/bootstrap-dialog.min.css">		
                <!--For all grid export and paging operations-->
        
                <link rel="stylesheet" href="css/bootstrap.min_online.css">
                <link rel="stylesheet" href="css/bootstrap-theme.min_online.css">
                <link rel="stylesheet" href="css/forgot.css">

                <link href="bootcss/css/select2.min.css" rel="stylesheet" />
                <script src="bootcss/js/select2.min.js"></script>
                <style>
                    .toplablel{
                        display: inline-block;
                        width: 25%;
                        text-align: center;
                        padding-right: 15px;
                        vertical-align: calc;
                        font-size: 12px;
                        background-color: #439943;
                        padding: 8px;
                        font-weight: normal;
                        color: #fff;
                    }
                    .homecontrol {
                        width: 42%; font-size: 12px; display: inline-block;
                    }
                    .footer-distributed{padding: 15px 50px 0px 64px; margin-top: 0px;}
                </style>
	</head>
	
	<body  style="background-image:url('images/myrkclbgenquiry.png');height: 100%;background-position: center;  background-repeat: no-repeat;  background-size: cover;">  
            <div id="main_content">
            <header class="header-search" style="color: #000">
			 <div class="header-limiter row">   
                             <div class="col-md-8">
                                <h1> <a href="index.php"><img src="images/myrkcl_logo.png" style="width:175px; height:75px;"></a></h1>
                             </div>
                          
			</div>
                
		</header>
	