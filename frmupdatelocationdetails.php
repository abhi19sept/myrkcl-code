<?php
$title = "Update Location Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Update Location Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmUpdateLocationDetails" id="frmUpdateLocationDetails" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="cc">Enter Center Code:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="javascript:return allownumbers(event);" name="txtcc" id="txtcc" placeholder="Enter Center Code">
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>
                    </div>


                    <div id="main-content" style="display:none">

                        <!--                        <div class="container">
                        
                                                    <div id="errorBox"></div>
                                                    <div class="col-sm-4 form-group">     
                                                        <label for="learnercode">Name of Organization/Center:<span class="star">*</span></label>
                                                        <input type="text" class="form-control" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                                    </div>
                        
                        
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="ename">Registration No:<span class="star">*</span></label>
                                                        <input type="text" class="form-control" maxlength="30" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                                                    </div>
                        
                        
                                                    <div class="col-sm-4 form-group">     
                                                        <label for="faname">Date of Establishment:<span class="star">*</span></label>
                                                        <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                                                    </div>
                        
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="edistrict">Type of Organization:<span class="star">*</span></label>
                                                        <select id="txtType" name="txtType" class="form-control" >
                        
                                                        </select>    
                                                    </div>
                                                </div>    -->

                        <!--                        <div class="container">
                        
                        
                        
                        
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="edistrict">Document Type:<span class="star">*</span></label>
                                                        <select id="txtDoctype" name="txtDoctype" class="form-control" >
                                                            <option value="">Select</option>
                                                            <option value="PAN Card">PAN Card</option>
                                                                                        <option value="Voter ID Card">Voter ID Card</option>
                                                                                        <option value="Driving License">Driving License</option>
                                                                                        <option value="Passport">Passport</option>
                                                                                        <option value="Employer ID card">Employer ID card</option>
                                                                                        <option value="Government ID Card">Governments ID Card</option>
                                                                                        <option value="College ID Card">College ID Card</option>
                                                                                        <option value="School ID Card">School ID Card</option>
                                                        </select>    
                                                    </div>
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="panno">PAN Card Number:<span class="star">*</span></label>
                                                        <input type="text" class="form-control" maxlength="10" name="txtPanNo" id="txtPanNo" placeholder="PAN Card Number">     
                                                    </div>
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="payreceipt">Upload Proof:<span class="star">*</span></label>
                                                        <input type="file" class="form-control"  name="orgdocproof" id="orgdocproof" onchange="checkScanForm(this)">
                                                    </div>
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="edistrict">Country:<span class="star">*</span></label>
                                                        <select id="ddlCountry" name="ddlCountry" class="form-control" >
                        
                                                        </select>    
                                                    </div>
                        
                        
                                                </div>	-->
                        <div class="panel panel-info" id="impreg">
                            <div class="panel-heading">Registerede IT-GK Details</div>
                            <div class="panel-body">

                                <div class="container">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">IT-GK Name:<span class="star">*</span></label>
                                        <input type="text" maxlength="10" class="form-control" name="txtCenterName" id="txtCenterName" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtOrgCode" id="txtOrgCode"/>
                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="edistrict">District:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtDistrictName" id="txtDistrictName" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtDistrictCode" id="txtDistrictCode"/>
                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="edistrict">Current Tehsil:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtTehsilName" id="txtTehsilName" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtTehsilCode" id="txtTehsilCode"/>
                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="edistrict">Current Area Type:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtAreaType" id="txtAreaType" readonly="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel panel-info">
                            <div class="panel-heading">Select IT-GK Details to Update</div>
                            <div class="panel-body">

                                <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Select Tehsil:<span class="star">*</span></label>
                                        <select id="ddlTehsil" name="ddlTehsil" class="form-control" >

                                        </select>    
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Select Area Type:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="areaUrban" name="area"/> Urban </label>
                                        <label class="radio-inline"> <input type="radio" id="areaRural" name="area"/> Rural </label>
                                    </div>
                            </div>
                        </div>

                        <!--                        <div class="container">	
                        
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="address">Pin Code:<span class="star">*</span></label>
                                                        <input type="text" class="form-control" name="txtPinCode" id="txtPinCode" maxlength="6" onkeypress="javascript:return allownumbers(event);" placeholder="Pin Code">    
                                                    </div>
                        
                        
                                                    <div class="col-sm-4 form-group">     
                                                        <label for="address">Address:<span class="star">*</span></label>
                                                        <textarea class="form-control" id="txtRoad" name="txtRoad" placeholder="Address"></textarea>
                        
                                                    </div>
                        
                                                    <div class="col-sm-4 form-group">     
                                                        <label for="address">Nearest Landmark:<span class="star">*</span></label>
                                                        <input type="text" class="form-control" maxlength="12" name="txtLandmark" 
                                                               id="txtLandmark" placeholder="Nearest Landmark" >
                        
                                                    </div>
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="edistrict">Police Station:</label>
                                                        <input type="text" class="form-control" name="txtPoliceStation" id="txtPoliceStation" value="NA" placeholder="Police Station">       
                        
                                                    </div>
                                                </div>    -->
                        <!--                        <div class="container">
                                                    <div class="col-sm-4 form-group">     
                                                        <label for="area">Area Type:<span class="star">*</span></label> <br/>                               
                                                        <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" onChange="findselected()"/> Urban </label>
                                                        <label class="radio-inline"> <input type="radio" id="areaRural" name="area" onChange="findselected1()"/> Rural </label>
                                                    </div>
                        
                                                    <div class="container" id="Urban" style="display:none">
                                                        <div class="col-sm-4 form-group"> 
                                                            <label for="edistrict">Mohalla:</label>
                                                            <input type="text" class="form-control" name="txtMohalla" id="txtMohalla" value="NA" placeholder="Mohalla">      
                                                        </div>
                        
                        
                                                        <div class="col-sm-4 form-group"> 
                                                            <label for="edistrict">Ward No.:</label>
                                                            <input type="text" class="form-control" name="txtWard" id="txtWard" value="NA" placeholder="Ward No">     
                                                        </div>
                        
                                                        <div class="col-sm-4 form-group"> 
                                                            <label for="edistrict">Municipal Town:</label>
                                                            <input type="text" class="form-control" name="txtMunicipal" id="txtMunicipal" value="NA" placeholder="Municipal Town">       
                                                        </div>
                        
                        
                                                    </div>
                                                    <div class="container" id="Rural" style="display:none">
                                                        <div class="col-sm-4 form-group"> 
                                                            <label for="edistrict">Panchayat Samiti/Block:</label>
                                                            <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >
                        
                                                            </select>
                                                        </div>
                        
                                                        <div class="col-sm-4 form-group"> 
                                                            <label for="edistrict">Gram Panchayat:</label>
                                                            <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >
                        
                                                            </select>
                                                        </div>
                        
                                                        <div class="col-sm-4 form-group"> 
                                                            <label for="edistrict">Village:</label>
                                                            <input type="text" class="form-control" name="txtVillage" id="txtVillage" value="NA" placeholder="Village">                            
                                                        </div>
                        
                        
                                                    </div>
                                                </div> -->

                        <!--                        <div class="container">
                                                    <input type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload"/>
                                                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>
                                                </div>-->
                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Click here to Update Details"/>    
                        </div>
                    </div>
                </form>
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function filltehsil() {
            var selDistrict = txtDistrictCode.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        }
//
//        $("#ddlDistrict").change(function () {
//            var selDistrict = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfCenterRegistration.php',
//                type: "post",
//                data: "action=FILLPanchayat&values=" + selDistrict + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlPanchayat').html(data);
//                }
//            });
//        });
//
//
//
//        function findselected() {
//
//            var UrbanDiv = document.getElementById("areaUrban");
//            var category = document.getElementById("Urban");
//
//            if (UrbanDiv) {
//                Urban.style.display = areaUrban.checked ? "block" : "none";
//                Rural.style.display = "none";
//            }
//            else
//            {
//                Urban.style.display = "none";
//            }
//        }
//        function findselected1() {
//
//
//            var RuralDiv = document.getElementById("areaRural");
//            var category1 = document.getElementById("Rural");
//
//            if (RuralDiv) {
//                Rural.style.display = areaRural.checked ? "block" : "none";
//                Urban.style.display = "none";
//            }
//            else
//            {
//                Rural.style.display = "none";
//            }
//        }
//        $("#areaUrban").change(function () {
//            findselected();
//        });
//        $("#areaRural").change(function () {
//            findselected1();
//        });

        $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            if(txtcc.value == ''){
              BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Please Enter Center Code to Proceed.</span>");  
            }
            $.ajax({
                type: "post",
                url: "common/cfUpdateLocationDetails.php",
                data: "action=DETAILS&values=" + txtcc.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Center Code" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtCenterName.value = data[0].orgname;
                        txtOrgCode.value = data[0].orgcode;
                        txtDistrictCode.value = data[0].districtcode;
                        txtTehsilCode.value = data[0].tehsilcode;

                        txtDistrictName.value = data[0].districtname;
                        txtTehsilName.value = data[0].tehsilname;
                        txtAreaType.value = data[0].areatype;

                        filltehsil();

                        $("#main-content").show();
                        $('#txtcc').attr('readonly', true);
                        $("#btnShow").hide();

                    }

                }
            });
        });

        $("#btnSubmit").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfUpdateLocationDetails.php"; // the script where you handle the form input.
                if (document.getElementById('areaUrban').checked) //for radio button
                {
                    var area_type = 'Urban';
                } 
                else if (document.getElementById('areaRural').checked) {
                    area_type = 'Rural';
                }
                else {
                    area_type = txtAreaType.value;
                }
                //alert(filename);    

                var data;
                // alert(Mode);
                if (Mode == 'Add')
                {

                    data = "action=ADD&&tehsil=" + ddlTehsil.value + "&orgcode=" + txtOrgCode.value +
                            "&areatype=" + area_type + ""; // serializes the form's elements.
                    //alert(data);
                } else
                {
                    data = "action=UPDATE&tehsil=" + ddlTehsil.value + "&orgcode=" + txtOrgCode.value +
                            "&areatype=" + area_type + ""; // serializes the form's elements.
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Details Updated Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmUpdateLocationDetails.php";
                            }, 2000);

                            Mode = "Add";
                            resetForm("frmUpdateLocationDetails");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }

                    }
                });
            

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

</html>