<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$title = "NCR Application Status";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>


<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:450px !important;max-height:1500px !important">

    <div class="container" > 

        <div class="panel panel-primary" style="margin-top:46px !important;" >

            <div class="panel-heading" id="non-printable">NCR Application Status</div>
            <div class="panel-body">


                <!-- <div class="jumbotron"> -->
                <form name="frmncrstatus" id="frmncrstatus" action="" class="form-inline">     

                    <div class="container"  >



                        <div id="response"> </div>
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group" id="hideicondiv">

                        </div>

                        <div class="col-sm-6 form-group"   style="display: none" id="showicondiv">
                            <div><i class="fa fa-forward" style="font-size:48px;color:green"></i> Step Completed</div>

                            <div><i class="fa fa-play" style="font-size:48px;color:orange"></i> Current Step</div>

                            <div><i class="fa fa-stop" style="font-size:48px;color:red"></i> Step Not Completed</div>

                        </div>
                        <div class="col-sm-2 form-group"   >

                        </div>
                        <div class="col-sm-4 form-group"   > 
                            <label for="learnercode">AO IT-GK Code:<span class="star">*</span></label>
                            <input type="text" maxlength="18" class="form-control"   name="AOTTGKCODE" id="AOTTGKCODE" onkeypress="javascript:return allownumbers(event);"  placeholder="AO IT-GK Code">
                        </div>


                        <div class="container" >
                            <label for="learnercode"></label>
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary btn-md" value="show Details" style="margin-top:25px"/>    
                        </div>

                        <div  id="maincontent" style="display: none">
                            <div class="col-lg-3 col-md-12 W20">
                                <div class="stepwizard">
                                    <div class="stepwizard-row">
                                        <div class="stepwizard-step">
                                            <button type="button" id="step1"  class="down-arrow redstep step">Step-1 AO IT-GK Registration By SP</button>
                                        </div>
                                    </div>

                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step2"  class="down-arrow redstep step">Step-2 AO Login Approve By SP</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step3"  class="down-arrow redstep step">Step-3 NCR Fee Payment BY AO</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step4"  class="down-arrow redstep step">Step-4 Bank Account Detail Submit</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step5"  class="down-arrow redstep step">Step-5 Owner Detail Submit</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step6"  class="down-arrow redstep step">Step-6 HR Detail Submit</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step7"  class="down-arrow redstep step">Step-7 Add System Details</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step8"  class="down-arrow redstep step">Step-8 Generate Intake</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step9"  class="down-arrow redstep step">Step-9 Premises Detail Submit</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step10"  class="down-arrow redstep step">Step-10 IT Peripherals Detail Submit</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step11"  class="down-arrow redstep step">Step-11 SP Detail Submit</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step12"  class="down-arrow redstep step">Step-12 NCR Center Visit Photo Submit</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step13"  class="down-arrow redstep step">Step-13 SP Visit Feedback</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step14"  class="down-arrow redstep step">Step-14 Final Approval By SP</button>
                                        </div>
                                    </div>
                                    
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step15"  class="down-arrow redstep step">Step-15 SP IT-GK Agreement Upload</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step16"  class="down-arrow redstep step">Step-16 DPO Visit Feedback</button>
                                        </div>
                                    </div>
                                    <div class="stepwizard-row">   
                                        <div class="stepwizard-step">
                                            <button type="button" id="step17"  class="btn btn-default btn-circle redstep step">Step-17 Course Authorization By RKCL</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class='col-lg-9 col-md-12'  id='tabs-1' aria-hidden='true'  style="display:none; margin-top: 15px;">

                                <div  id="ITGKDivNodata1">

                                    <div class="maindiv" id="ITGKDiv">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
            </div> 

            <!--<div id="grid"  ></div>-->	
            </form> 

        </div>

    </div>   
</div>
</div>

</body>					
<?php include ('footer.php'); ?>				
<?php include'common/message.php'; ?>
<style>

    .stepwizard { /* весь блок */
        display: table;
        width: 30px; /* ограничимся шириной .btn-square */
        height: 400px;  /* высота */
        position: relative; }

    .stepwizard-row { display: table-row; }

    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important; }

    .stepwizard-row:before {
        position: absolute;
        top: 10px;
        content: " ";
        width: 5px;
        height: 97%;
        background-color: #fff;
        left: 90px;
        border-radius: 10px;
    }

    .stepwizard-step {   /* каждый из 3-ёх */
        display: grid;
        text-align: center;
        position: relative; left: -15px;
        z-index: 1;  padding: 10px;}

    .btn-square {
        width: auto; height: 30px;
        text-align: center;  font-size: 12px; line-height: 1.428571429; /* цифры внутри */ }

    .btn-circle {
        width: auto;
        height: auto;
        text-align: left;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
        padding-left: 10px;
        padding-right: 10px;
    }
    .arrow-left {
        width: 0; 
        height: 0; 
        border-top: 30px solid transparent;
        border-bottom: 30px solid transparent;  
        border-right:30px solid black; 
    }
</style>
<style>
    .orangestep{
        background-color: orange !important;
        font-weight: bold;
        color: #fff;
        font-size: 12px;
    }
    .redstep{
        background-color: #f00 !important;
        font-weight: bold;
        color: #fff;
        font-size: 12px;
    }
    .greenstep{
        background-color: green !important;
        font-weight: bold;
        color: #fff;
        font-size: 12px;
    }
</style> 

<style>
    .maindiv {
        width: 98%;
        float: left;
        border: 1px solid #399CFF;
        border-top: none;
        min-height: auto;
        padding: 0px 0px 10px 0px;
    }
    .persdetablog {
        padding: 15px 0 0 20px;
    }

    .bdrtop {
        border-top: 1px solid #399CFF; border-bottom: 1px solid #399CFF; background-color: #f1f1f1; font-size: 17px;
    }

    .persdetablog {
        width: 100%;
        float: left;
        padding: 15px 0px 10px 5px;
    }
    .persdetacont {
        width: 50%;
        float: left;
    }
    .persdetacont span {
        width: 37%;
    }
    .persdetacont span {
        margin-top: 8px;
        width: 33%;
        float: left;
        font-size: 14px;
        font-family: Verdana;
    }
    .persdetacont p {
        width: 50%;
    }

    .persdetacont p {
        width: 65%;
        min-height: 32px;
        float: left;
        background-color: #f1f1f1;
        font-size: 12px;
        font-family: Verdana;
        border: 1px solid #dddddd;
        border-radius: 5px;
        padding: 7px 5px 7px 10px;
        margin: 0px;
    }
    .W20{ width: 23%;}

</style>
<style>
    .down-arrow {
        display: inline-block;
        position: relative;
        background: darkcyan;
        padding: 2px 7px;
        width: 245px;
        text-align: left;
        border-radius: 15px;
    }
    .down-arrow::after {
        content: '';
        display: block;
        position: absolute;
        left: 40%;
        top: 100%;
        width: 0;
        height: 0;
        border-top: 20px solid darkcyan;
        border-right: 20px solid transparent;
        border-bottom: 0 solid transparent;
        border-left: 20px solid transparent; 
    }
</style>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfNCRStatus.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmncrstatus").serialize();

            data = "action=DETAILS&" + forminput; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $(".step").addClass("redstep");
                    $(".step").removeClass("greenstep");
                    $("#tabs-1").hide();
                    $("#hideicondiv").hide();
                    $("#showicondiv").show();
                    //data = $.parseJSON(data);
                    //alert(data);
                    if (data == 0 || data == "invalidcenter") {

                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Please Enter Valid Center Code </span></p>");
                        $("#maincontent").hide();
                        return false;
                    }



                    $("#maincontent").show();

                    $(".step").prop("disabled", true);
                    data = $.parseJSON(data);

                    $('#response').empty();

                    //    if (data[0].step1 == "1") {
//                            $("#process").show();
//                            $("#processs").show();
                    $("#step1").addClass("greenstep");
                    $("#step1").removeClass("redstep");
                    //  }

                    if (data[0].step2 == "2") {
                        document.getElementById("step2").classList.add('greenstep');
                        document.getElementById("step2").classList.remove('redstep');
                        $("#step2").prop("disabled", false);

                    }
                    if (data[0].step3 == '3') {
                        $("#step3").addClass("greenstep");
                        $("#step3").removeClass("redstep");
                        $("#step3").prop("disabled", false);
                    }
                    if (data[0].step4 == "4") {
                        document.getElementById("step4").classList.add('greenstep');
                        document.getElementById("step4").classList.remove('redstep');
                        $("#step4").prop("disabled", false);
                    }
                    if (data[0].step5 == "5") {
                        document.getElementById("step5").classList.add('greenstep');
                        document.getElementById("step5").classList.remove('redstep');
                        $("#step5").prop("disabled", false);
                    }
                    if (data[0].step6 == "6") {
                        document.getElementById("step6").classList.add('greenstep');
                        document.getElementById("step6").classList.remove('redstep');
                        $("#step6").prop("disabled", false);
                    }
                    if (data[0].step7 == "7") {
                        document.getElementById("step7").classList.add('greenstep');
                        document.getElementById("step7").classList.remove('redstep');
                        $("#step7").prop("disabled", false);
                    }
                    if (data[0].step8 == "8") {
                        document.getElementById("step8").classList.add('greenstep');
                        document.getElementById("step8").classList.remove('redstep');
                        $("#step8").prop("disabled", false);
                    }
                    if (data[0].step9 == "9") {
                        document.getElementById("step9").classList.add('greenstep');
                        document.getElementById("step9").classList.remove('redstep');
                        $("#step9").prop("disabled", false);
                    }
                    if (data[0].step10 == "10") {
                        document.getElementById("step10").classList.add('greenstep');
                        document.getElementById("step10").classList.remove('redstep');
                        $("#step10").prop("disabled", false);
                    }
                    if (data[0].step11 == "11") {
                        document.getElementById("step11").classList.add('greenstep');
                        document.getElementById("step11").classList.remove('redstep');
                        $("#step11").prop("disabled", false);
                    }
                    if (data[0].step12 == "12") {
                        document.getElementById("step12").classList.add('greenstep');
                        document.getElementById("step12").classList.remove('redstep');
                        $("#step12").prop("disabled", false);
                    }
                    if (data[0].step13 == "13") {
                        document.getElementById("step13").classList.add('greenstep');
                        document.getElementById("step13").classList.remove('redstep');
                        $("#step13").prop("disabled", false);
                    }
                    if (data[0].step14 == "14") {
                        document.getElementById("step14").classList.add('greenstep');
                        document.getElementById("step14").classList.remove('redstep');
                        $("#step14").prop("disabled", false);
                    }
                    if (data[0].step15 == "15") {
                        document.getElementById("step15").classList.add('greenstep');
                        document.getElementById("step15").classList.remove('redstep');
                        $("#step15").prop("disabled", false);
                    }
                    if (data[0].step16 == "16") {
                        document.getElementById("step16").classList.add('greenstep');
                        document.getElementById("step16").classList.remove('redstep');
                        $("#step16").prop("disabled", false);
                    }
                    if (data[0].step17 == "17") {
                        document.getElementById("step17").classList.add('greenstep');
                        document.getElementById("step17").classList.remove('redstep');
                        $("#step17").prop("disabled", false);
                    }
                    var flag = data[0].flag;

                    document.getElementById("step" + flag).classList.add('orangestep');
                    document.getElementById("step" + flag).classList.remove('redstep');
                    document.getElementById("step" + flag).classList.remove('greenstep');
                      if(flag == "18"){
                            document.getElementById("step17").classList.add('greenstep');
                            document.getElementById("step17").classList.remove('redstep');
                            document.getElementById("step17").classList.remove('orangestep');
                     }

                }
            });

            return false; // avoid to execute the actual submit of the form.
        });


        $(".step").click(function () {


            var mybtn = $(this).attr("id");
            var url = "common/cfNCRStatus.php"; // the script where you handle the form input.
            var data;
            //var forminput = $("#frmncrstatus").serialize();     
            data = "action=ShowGrid&btnid=" + mybtn + "&ccode=" + AOTTGKCODE.value + ""; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $("#tabs-1").show();
                    $("#ITGKDiv").show();
                    $("#ITGKDivNodata1").show();
                    $("#ITGKDiv").html(data);
                }
            });

        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
    td {
        font-family: times new roman;
        font-size: 15px;
    }
    legend {
        font-size: 17px !important;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border: 1px solid #dddddd;
        line-height: 1.42857;
        padding: 8px;
        vertical-align: top;
    }
</style>

</html>