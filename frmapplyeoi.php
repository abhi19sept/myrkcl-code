<?php
$title = "Apply On EOI";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var EoiCCode='0'</script>";
} else {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='Add'</script>";
    echo "<script>var EoiCCode='0'</script>";
}
?>

<div class="container"> 

    <div style="min-height:430px !important;max-height:1500px !important;">
        <div class="panel panel-primary" style="margin-top:36px;">

            <div class="panel-heading">Apply EOI</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmApplyEoi" id="frmApplyEoi" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Select EOI:<span class="star">*</span></label>
                            <select id="ddlEOI" name="ddlEOI" class="form-control" >
                            </select>    
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Start Date:</label>
                            <div class="form-control" maxlength="50"   name="txtstartdate" id="txtstartdate">

                            </div>
                            <!--<input type="text" class="form-control" maxlength="50"   name="txtstartdate" id="txtstartdate" placeholder="Start Date">-->
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">End Date:</label>
                            <div class="form-control" maxlength="50"   name="txtenddate" id="txtenddate">

                            </div>
                            <!--<input type="text" class="form-control" maxlength="50" name="txtenddate" id="txtenddate" placeholder="End Date">-->     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="faname">Processing Fee:</label>
                            <div class="form-control" maxlength="50"   name="txtPFees" id="txtPFees">

                            </div>
                            <!--<input type="text" class="form-control" maxlength="50" name="txtPFees"   id="txtPFees"  placeholder="Processing Fee">-->
                        </div>





                    </div>  

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="faname">Course Fee:</label>

                            <div class="form-control" maxlength="50"   name="txtCFees" id="txtCFees">

                            </div>
                            <!--<input type="text" class="form-control"   maxlength="50" name="txtCFees" id="txtCFees"  placeholder="Course Fee">-->
                        </div>
                        <div class="col-sm-5">     
                            <label for="faname">Course Name:</label>
                            <div class="form-control"    name="txtCName" id="txtCName">

                            </div>
                            <!--<input type="text" class="form-control"   maxlength="50" name="txtCName" id="txtCName"  placeholder="Course Name">-->
                        </div>


                    </div>

                    <div class="container" id="others" style="display:none;">  
                        <div class="col-md-4 form-group"> 							
                            <label for="learnercode">Terms and conditions :<span class="star">*</span></label></br>                              
                            <label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="" >
                                <a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix">I Accept Terms & Conditions</span> </a>
                            </label>								
                        </div>
                    </div>
					<div class="container" id="wcdeoi" style="display:none;">  
                        <div class="col-md-12 form-group"> 							
                            <label for="learnercode">Terms and conditions :<span class="star">*</span></label></br>                              
                            <label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="" >
                                <a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><div id="fix" style="width:950px;">मैं यह शपथ पूर्वक घोषणा करता/करती हूँ की मैंने नि:शुल्क महिला कंप्यूटर प्रशिक्षण योजना की EOI की सभी शर्तें पढने के उपरांत स्वीकार की है तथा मेरे ज्ञान केंद्र के परिसर में प्रशिक्षनार्थियों/स्टाफ हेतु पीने का पानी तथा टॉयलेट्स की सुविधा उपलब्ध है|</div> </a>
                            </label>								
                        </div>
                    </div>
					
                    <div tabindex="-1" class="modal fade" id="myModal" role="dialog" style="padding-top:0;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button class="close" type="button" data-dismiss="modal">×</button>
                                    <h3 class="modal-title">Terms and Conditions</h3>
                                </div>
                                <div class="modal-body">
                                    <iframe id="EOItnc" style="width: 785px;height: 450px;"> </iframe>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <!--<label id="LblCCode" name="LblCCode" style="display:block"> </label>-->
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>
            </div>
        </div>   
    </div>
</div>

</form>


</body>
<?php include'footer.php'; ?>
<?php include'common/message.php'; ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
  .modal-dialog {width:1050px;}
.thumbnail {margin-bottom:6px; width:1100px;}
  </style>
<script type="text/javascript">  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    var totalamount = "";
    $(document).ready(function () {

        $("#ddlEOI").change(function ()
        {
            var EoiConfigCode = $(this).val();
				if(EoiConfigCode=='25'){
					$("#others").hide();
					$("#wcdeoi").show();
				}else{
					$("#others").show();
					$("#wcdeoi").hide();
				}
            $.ajax({
                type: "post",
                url: "common/cfEoiConfig.php",
                data: "action=EDIT&values=" + EoiConfigCode + "",
                success: function (data) {
  
                    data = $.parseJSON(data);
   
                    document.getElementById('txtstartdate').innerHTML = data[0].SDate;
                    document.getElementById('txtenddate').innerHTML = data[0].EDate;
                    document.getElementById('txtPFees').innerHTML = data[0].PFees;
                    document.getElementById('txtCFees').innerHTML = data[0].CFees;
                    $("#EOItnc").attr('src',"upload/eoi_tnc/" + data[0].EOI_TNC + "#zoom=80");
         
                    document.getElementById('txtCName').innerHTML = data[0].CName;
                    EoiCCode = data[0].CCode;
                 


                }
            }).done(function () {

                totalamount = Number(document.getElementById('txtPFees').innerHTML) + Number(document.getElementById('txtCFees').innerHTML);
            
            });
        });


        function FillEOI() {
            $.ajax({
                type: "post",
                url: "common/cfApplyEoi.php",
                data: "action=FILL",
                success: function (data) {
					
                    $("#ddlEOI").html(data);
                }
            });
        }
        FillEOI();

        function payment() {
        
            $.ajax({
      
                type: "post",
                url: "frmPayment.php",
                data: "code=" + Code +
                        "&paytype=" + ddlEOI.value +
                        "&amount=" + totalamount + "",
                success: function (data) {
           
                }
            });

        }




        $("#btnSubmit").click(function () {
            if ($("#frmApplyEoi").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfApplyEoi.php"; // the script where you handle the form input.                            
                var data;
               
                if (Mode == 'Add')
                {

                    data = "action=ADD&cname=" + document.getElementById('txtCName').innerHTML +
                            "&itgkcode=" + Code +
                            "&eoiname=" + ddlEOI.value +
                            "&startdate=" + document.getElementById('txtstartdate').innerHTML +
                            "&enddate=" + document.getElementById('txtenddate').innerHTML + 
                            "&EoiCCode=" + EoiCCode + ""; // serializes the form's elements.
               
                }
                else
                {

                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmapplyeoi.php';
                            }, 3000);

                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }


                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmapplyeoi.js" type="text/javascript"></script>
</html>
